#!/bin/bash

onlyup_select (){
  local project=`find emotibot -type d -name ${1}*`
  #local project=${project#*/}
  if [[ $1 == infra ]]
    then
      
      servicelist=(`sed -n -E "/^[ ]{2}[a-zA-Z0-9_-]{1,30}[:][ ]{0,2}$/p" infra/.infra.yaml | tr -d " "| tr -d : | grep -v -E "default|^emotibot$|hostnet"`)
  else
      # 获取compose文件的服务名称
      #servicelist=(`docker-compose -f ${project}/.module.yaml images | awk 'NR>2{print}' | awk '{print $1}'`)
      servicelist=(`sed -n -E "/^[ ]{2}[a-zA-Z0-9_-]{1,30}[:][ ]{0,2}$/p" ${project}/.module.yaml | tr -d " "| tr -d : | grep -v -E "default|^emotibot$|hostnet"`)
  fi
  for i in `seq 0 ${#servicelist[@]}`
  do
    # 每行打印3列
    count=$((($i+1) % 3))
    if [[ $count -eq 0 ]]
      then
        log_format $i ${servicelist[$i]} line
    elif [[ $i -eq ${#servicelist[@]} ]]
      then
        echo
    else
        log_format $i ${servicelist[$i]}
    fi
  done
  echo
  read -p "To recreate the :" onlyupsvnum
  if [[ $onlyupsvnum -gt ${#servicelist[@]} ]] || ! grep '^[[:digit:]]*$' <<< "$onlyupsvnum"  ; then
    echo "input number error ......"
    exit 1
  fi
  onlyup pass ${servicelist[$onlyupsvnum]}
}

onlyup (){
  shift 1
  # 同时被交互模式传递，被直接调用的方式传递
  if [[ -n $@ ]]
    then
      for i in $@
        do
          # 如果带参数，则循环找到，并单独stop rm up
          onlysv=`grep -o "^  $i" -r emotibot --include module.yaml | grep -v "#"`
          # module 中找不到，则去infra中找，如果.infra.yaml不存在，则可能是想单独之启动一个，成全你
          if [[ ! $onlysv ]] && $ONLY_DEPLOY_BASE
            then
                docker-compose -f infra/infra.yaml -f infra/port.yaml config > infra/.infra.yaml || db_health_exit "yaml error ......"
                onlysv=`grep -o "^  $i" -r infra --include .infra.yaml | grep -v "#"`
          elif [[ ! $onlysv ]] && ! $ONLY_DEPLOY_BASE
            then
                docker-compose -f infra/infra.yaml -f infra/infra-distributed.yaml config > infra/.infra.yaml || db_health_exit "yaml error ......"
                onlysv=`grep -o "^  $i" -r infra --include .infra.yaml | grep -v "#"`
          fi
          # 实在找不到了，就退出
          if [[ ! $onlysv ]] ; then 
            echo $i not found
            exit 1
          fi
          onlysvname=${onlysv#* }
          onlysvfile=${onlysv%:*}
          # DB 则操作DB
          #if [[ $onlysvfile =~ infra ]] then; 
          #   onlysvfile=infra/.infra.yaml
          #fi
          # 模块则操作模块
          if [[ -s ${onlysvfile/"module.yaml"/".module.yaml"} ]]
            then
              onlysvfile=${onlysvfile/"module.yaml"/".module.yaml"}
          fi
          if $ONLY_DEPLOY_BASE || [[ $onlysvfile =~ infra ]]
            then
              docker-compose -f $onlysvfile stop $onlysvname
              docker-compose -f $onlysvfile rm $onlysvname
              docker-compose -f $onlysvfile up -d $onlysvname
          else
              echo "swarm not support only up service_task "
              exit 1
          fi
      done  
      exit 0
  # 如果有参数，并且是DB操作
  elif [[ -n $@ ]] && $isinfra
    then
      docker-compose -f infra/.infra.yaml stop $@
  else
    echo -e "\033[33m                      restart one server \033[0m"
    echo -e "[ -------- infra  infra DB                                   --------]"
    banner_list
    select_number "onlyup"
  fi
}

