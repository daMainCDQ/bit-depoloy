#!/bin/bash
##
##  bingjunx
##
mini_deploy_convert (){

  # 根据变量list ，选择不启动的模块
  skipmodule=""

  skipfaq=""
  skipskill=""
  skipchat=""
  skipintent=""
  skipkg=""
  skipparserLibrary=""

  if [[ $deploy_faq == "false" ]]
    then
      faq_group=`echo $faq_group | sed 's/,/ /g'`
      faq_group=($faq_group)
      echo
      for (( i=0;i<${#faq_group[@]};i++))
      do
        skipfaq+="--scale ${faq_group[i]}=0 "
      done
      source $project/dev-mini-faq.env
  fi

  if [[ $deploy_skill == "false" ]]
    then
      skill_group=`echo $skill_group | sed 's/,/ /g'`
      skill_group=($skill_group)
      echo
      for (( i=0;i<${#skill_group[@]};i++))
      do
        skipskill+="--scale ${skill_group[i]}=0 "
      done
      source $project/dev-mini-skill.env
  fi

  if [[ $deploy_chat == "false" ]]
    then
      chat_group=`echo $chat_group | sed 's/,/ /g'`
      chat_group=($chat_group)
      echo
      for (( i=0;i<${#chat_group[@]};i++))
      do
        skipchat+="--scale ${chat_group[i]}=0 "
      done
      source $project/dev-mini-chat.env
  fi

  if [[ $deploy_intent == "false" ]]
    then
      intent_group=`echo $intent_group | sed 's/,/ /g'`
      intent_group=($intent_group)
      echo
      for (( i=0;i<${#intent_group[@]};i++))
      do
        skipintent+="--scale ${intent_group[i]}=0 "
      done
      source $project/dev-mini-intent.env
  fi

  if [[ $deploy_kg == "false" ]]
    then
      kg_group=`echo $kg_group | sed 's/,/ /g'`
      kg_group=($kg_group)
      echo
      for (( i=0;i<${#kg_group[@]};i++))
      do
        skipkg+="--scale ${kg_group[i]}=0 "
      done
      source $project/dev-mini-kg.env
  fi

  if [[ $deploy_parserLibrary == "false" ]]
    then
      parserLibrary_group=`echo $parserLibrary_group | sed 's/,/ /g'`
      parserLibrary_group=($parserLibrary_group)
      echo
      for (( i=0;i<${#parserLibrary_group[@]};i++))
      do
        skipparserLibrary+="--scale ${parserLibrary_group[i]}=0 "
      done
  fi

  # 最终不启动的模块
  skipmodule="$skipfaq $skipskill $skipchat $skipintent $skipkg $skipparserLibrary"
}

#此函数作为 cut裁剪模块部署模式启动前的 预初始化动作，包括以下几个行为：
#1）针对计划不启动的后端模块，前端load-balance/admin-ui里upstream域名配置替换成ip地址，临时解决了其启动依赖的问题!
#2）调用gen_module.py脚本生成 .module_output.csv 和 .ui-mini.dev
module_cut_convert (){

    WORK_PATH=$(dirname "$0")
    MODULE_ENV=$WORK_PATH/.dev-cut.env
    cat $WORK_PATH/dev.env > $MODULE_ENV
    echo -e "\n" >>  $MODULE_ENV
    cat $project/dev.env >> $MODULE_ENV
    if [[ -f $project/tailoring/script/gen_module.py ]] ; then
      #截除参数中的mini和cut
      cut_args=`echo $mini_cut_all | sed 's/mini//g' | sed 's/cut//g'`
      #如果cut/裁剪部署模式下命令行带参数，则调用python脚本
      if [[ "$cut_args" =~ [A-Za-z0-9]+ ]] ; then
        gen_module_result=`python $project/tailoring/script/gen_module.py $cut_args 2>&1`
        #当调用python脚本，发现不支持的参数后打印错误提示
        if [[ "$gen_module_result" =~ "not supported" ]] ; then
          echo -e "\033[31m $gen_module_result \033[0m"
           exit 1
        elif [[ "$gen_module_result" =~ "done" ]] ; then
          sleep 1
        elif [[ "$gen_module_result" =~ "command not found" ]] ; then
          echo -e "\033[31m 当前服务器没有python命令，不支持裁剪功能，请先安装python \033[0m"
        else
          echo -e "\033[31m $gen_module_result \033[0m"
          echo -e "\033[31m 调用$project/tailoring/script/gen_module.py 失败 ,请检查\033[0m"
          exit 1
        fi
      fi
    fi
    if [[ -f $project/.ui-mini.dev ]]
    then
      echo -e "\n" >>  $MODULE_ENV
      cat $project/.ui-mini.dev >> $MODULE_ENV
    fi
    if [[ -f $project/.module_output.csv ]] && [[ `cat -b $project/.module_output.csv | grep 1 | wc -l` -ne 0 ]]
      then
      module_group=`cat $project/.module_output.csv | sed 's/,/ /g'`
      #获取精剪部署的服务列表
      module_list_cut=(`cat $project/.module_output.csv |awk -F "," '{for(i=1;i<=NF;i++) print $i}'`)
      #获取全量服务列表
      module_list_all=(`cat $project/module.yaml  | egrep -o  "^  [a-Z-]*:$" | grep -v "default:"`)

      #遍历全量服务列表
      for (( i=0;i<${#module_list_all[@]};i++))
      do
        #服务列表截短多余的冒号
        module_list_all[$i]=${module_list_all[i]%:*}
        #遍历精剪部署的服务列表
        for (( j=0;j<${#module_list_cut[@]};j++))
        do
          #判断精剪部署的服务列表是否遍历到最后一个服务
          if [[ ! ${module_list_cut[j]} == ${module_list_cut[${#module_list_cut[@]}-1]} ]] ; then
            #如果<全量服务列表>里面的服务名存在<精剪部署服务列表>里，则直接跳出遍历循环
            if [[ ${module_list_cut[j]} == ${module_list_all[i]} ]] ; then
              break
            fi
          else
            #如果<全量服务列表>里面的服务名不存在<精剪部署服务列表>里，则对.dev-cut.env里面的服务名地址进行ip替换
            if [[ ! ${module_list_cut[j]} == ${module_list_all[i]} ]] ; then

               sed -i "s/_HOST=${module_list_all[i]}$/_HOST=${DEFAULT_IP}/g" ${MODULE_ENV}
               set -o allexport
               source ${MODULE_ENV}
               set +o allexport
            fi
          fi
        done
      done
    fi
    #裁剪模式标志位
    echo "true" > $project/.cut.current


}

