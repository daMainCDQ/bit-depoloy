#!/bin/bash
deploy_befor (){
  if [[ $DB_HOST_1 == $DB_HOST_2 ]]
  then
      export ONLY_DEPLOY_BASE=true
  # 多机
  else
      export ONLY_DEPLOY_BASE=false
  fi
  # 配置网络
  if [[ ! "`docker network ls | grep emotibot`" =~ " emotibot " ]]
  then
    if $ONLY_DEPLOY_BASE
    then
      echo create network emotibot on bridge:
      docker network create -d bridge --subnet=192.168.222.0/20 --gateway=192.168.208.1 --attachable emotibot
    else
      if [[ `ip a` =~ "$DB_HOST_1" ]]
        then
          if [[ "$DB_SEPARATE" == "false" ]]
          then
            echo create network emotibot on overlay:
            docker network create -d overlay --subnet=192.168.176.0/20 --gateway=192.168.176.1 --attachable emotibot
          else
            echo create network emotibot on bridge:
            docker network create -d bridge --subnet=192.168.222.0/20 --gateway=192.168.208.1 --attachable emotibot
          fi
      fi
    fi
  fi
  # 配置目录，swarm不会自己创建目录

  volumelist=`grep -r VOLUME_PATH --include module.yaml emotibot -h | grep -v "#" |awk '{print $2}' |awk -F ":" '{print $1}'`
  for v in $volumelist
  do
    eval vv=$v
    vv=${vv//\/\///}
    if [[ ! -d $vv ]]
    then
      mkdir -p $vv || exit 1
    fi
   # if [[ ! -d /data ]]
   # then
   #   mkdir -p /data || exit 1
   # fi
  done

  # 配置elasticsearch-efk目录，初始化该目录的属组和属主均为deployer 或者 777权限

  infravolumelist=`grep -r VOLUME_PATH --include es-infra.yaml infra -h | grep -v "#" |awk '{print $2}' |awk -F ":" '{print $1}'`
  for v in $infravolumelist
  do
    eval vv=$v
    vv=${vv//\/\///}
    if [[ ! -d $vv ]]
    then
      mkdir -p $vv || exit 1
      chmod 777 $vv || exit 1
    fi
  done

  # 初始化system环境，需要使用sudo操作，并使用临时文件验证
  # sudo 或 root 触发
  
  userid=`id -u`
  if [[ $userid -eq 0 ]]
    then
      # increase fd socket 
      grep -q "soft    nofile          1000000" /etc/security/limits.conf || echo "*     soft    nofile          1000000"  | sudo tee --append /etc/security/limits.conf
      grep -q "hard    nofile          1000000" /etc/security/limits.conf || echo "*     hard    nofile          1000000"  | sudo tee --append /etc/security/limits.conf
      # sysctl
      grep -q "net.ipv4.ip_forward=1" /etc/sysctl.conf || echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf
      grep -q "vm.max_map_count=262144" /etc/sysctl.conf || echo "vm.max_map_count=262144" >> /etc/sysctl.conf
      grep -q "kernel.pid_max=4194303" /etc/sysctl.conf || echo "kernel.pid_max=4194303" >> /etc/sysctl.conf
      grep -q "fs.file-max=1000000" /etc/sysctl.conf || echo "fs.file-max=1000000" >> /etc/sysctl.conf
      grep -q "net.ipv4.tcp_max_tw_buckets=6000" /etc/sysctl.conf || echo "net.ipv4.tcp_max_tw_buckets=6000" >> /etc/sysctl.conf
      grep -q "net.netfilter.nf_conntrack_max=2097152" /etc/sysctl.conf || echo "net.netfilter.nf_conntrack_max=2097152" >> /etc/sysctl.conf
      # use sysctl.conf
      sysctl -p
      modprobe br_netfilter
      modprobe nf_conntrack
      # off selinux
      sed -i "s/SELINUX=enforcing/SELINUX=disabled/" /etc/selinux/config
      setenforce 0
      # 1. Disable Firewalld Service.
      systemctl mask firewalld
      # 2. Stop Firewalld Service.
      systemctl stop firewalld
      # 配置docker默认配置，仓库与日志
      if [[ ! -s /etc/docker/daemon.json ]]
        then
      cat << EOF > /etc/docker/daemon.json
{
  "insecure-registries": [
      "harbor.emotibot.com",
      "docker-reg.emotibot.com.cn:55688",
      "172.16.101.70"
    ],
  "max-concurrent-downloads": 10,
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m",
    "max-file": "10"
  }
}
EOF
      fi
      systemctl enable docker 
      echo -e "\033[33m"
      echo -e "init done"
      echo -e "Please execute again , \033[31mDon't use sudo or Root\033[0m"
      exit 0
      
  fi


}

#swarm在切换服务的dns模式(dnsrr/vip)时，会出现dns解析的bug
#所以此函数会识别出切换服务的dns模式时，先删除原有服务，然后进行创建，这样就由替换 修改为 删除 重建，可避免出现此dns解析的bug
swarm_dns_convert (){

    WORK_PATH=$(dirname "$0")
    SWARM_YAML=$project/swarm.yaml
    #获取vip模式的服务列表
    if [[ -f $SWARM_YAML ]] ; then
      module_list_vip=(`cat $SWARM_YAML | grep  -B 1 "ports:" | egrep -o  "^  [a-Z0-9-]*:$"`)
    fi
    #获取dnsrr模式的服务列表
    if [[ -f $SWARM_YAML ]] ; then
      module_list_dnsrr=(`cat $SWARM_YAML | grep  -B 2 "endpoint_mode: dnsrr" | egrep -o  "^  [a-Z0-9-]*:$"`)
    fi
    #遍历vip 模式的服务列表
    for (( i=0;i<${#module_list_vip[@]};i++ ))
    do
      #服务列表截短多余的冒号
      module_list_vip[$i]=${module_list_vip[i]%:*}
      #获取当前服务是否运行,如果不存在就直接继续下一个遍历
      service_exist=`docker service inspect ${run_project}_${module_list_vip[i]} 2> /dev/null | grep '\"Name\": \"${run_project}_${module_list_vip[i]}\"' | wc -l`
      if [[ ${service_exist} -eq 0 ]] ; then
         continue
      fi
      #获取当前服务是否运行在vip模式下，如果不是，表示需要切换dns模式，触发先行删除该服务
      vip_mode=`docker service inspect ${run_project}_${module_list_vip[i]} 2> /dev/null | grep '\"Mode\": \"vip\"' | wc -l`
      if [[ ${vip_mode} -eq 0 ]] ; then
        echo -e "  \033[33m  触发 ${run_project}_${module_list_vip[i]} 服务切换dns模式(dnsrr--->vip)  \033[0m "
        docker service rm ${run_project}_${module_list_vip[i]}
      fi
    done
    #遍历dnsrr 模式的服务列表
    for (( i=0;i<${#module_list_dnsrr[@]};i++ ))
    do
      #服务列表截短多余的冒号
      module_list_dnsrr[$i]=${module_list_dnsrr[i]%:*}
      #获取当前服务是否运行,如果不存在就直接继续下一个遍历
      service_exist=`docker service inspect ${run_project}_${module_list_dnsrr[i]} 2> /dev/null | grep '\"Name\": \"${run_project}_${module_list_dnsrr[i]}\"' | wc -l`
      if [[ ${service_exist} -eq 0 ]] ; then
         continue
      fi
      #获取当前服务是否运行在dnsrr模式下，如果不是，表示需要切换dns模式，触发先行删除该服务
      dnsrr_mode=`docker service inspect ${run_project}_${module_list_dnsrr[i]} 2> /dev/null | grep '\"Mode\": \"dnsrr\"' | wc -l`
      if [[ ${dnsrr_mode} -eq 0 ]] ; then
        echo -e "  \033[33m  触发 ${run_project}_${module_list_dnsrr[i]} 服务切换dns模式(vip--->dnsrr)  \033[0m "
        docker service rm ${run_project}_${module_list_dnsrr[i]}
      fi
    done

}
