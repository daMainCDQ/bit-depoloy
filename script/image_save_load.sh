#!/bin/bash
# 获取镜像list
saveimageslist (){
    grep -e image: $@ | grep -v '#' |awk -F 'image:' '{print $2}'| sort | uniq
}
# 保存镜像
saveimage (){
  # 做个记录，打包成功的镜像不再打第二次
  tmpdir=/tmp/.save`date "+%Y%m%d"`
  mkdir $tmpdir -p
  touch $tmpdir/saveimagetmp.file
  echo -e "\033[36m---------- save ${@} ----------\033[0m"
  saveproject=$1
  shift 1
  # 增加一个数量提示
  nownum=1
  imagesumlist=($(saveimageslist $@))
  for i in ${imagesumlist[@]}
  do
      savefilename=$saveproject-${filename}-${i##*/}
      echo -e "save \033[33m ${image_path//\/\///}/${savefilename//:/_}.tar.gz\033[0m  \033[34m共 ${#imagesumlist[@]} 个 / 第 $nownum 个\033[0m"
  # 日期+镜像名.tar
      if [[ ! `cat $tmpdir/saveimagetmp.file` =~ ${image_path//\/\///}/${savefilename//:/_}.tar.gz ]]
        then
          pullerror=false
          docker pull $i ||  pullerror=true
          eval docker save $i | gzip > ${image_path//\/\///}/${savefilename//:/_}.tar.gz
          if [[ $? -eq 0 ]] && ! $pullerror
            then
              echo ${image_path//\/\///}/${savefilename//:/_}.tar.gz >> $tmpdir/saveimagetmp.file
          fi
          docker load < ${image_path//\/\///}/${savefilename//:/_}.tar.gz > /dev/null
          if [[ ! $? -eq 0 ]]
            then
              for s in `seq 10`
                do
                  eval docker save $i | gzip > ${image_path//\/\///}/${savefilename//:/_}.tar.gz
                  docker load < ${image_path//\/\///}/${savefilename//:/_}.tar.gz > /dev/null
                  if [[ $? -eq 0 ]] ; then
                    break
                  fi
                  if [[ $s -eq 10 ]] ; then
                    echo "save $i  Error...... Please check"
                    exit 1
                  fi
                done
          fi
      else
        echo -e "${image_path//\/\///}/${savefilename//:/_}.tar.gz  \033[32mhave done, pass\033[0m"
      fi
   let nownum++ || true
  done
}
upkeep_loadimage (){
   if [[ -d $1 ]]
   then
     loadimagelist=`find $1 -name *.tar.gz`
   else
     read -t 180 -p $'请输入image path:  \n' loadimage_path
     loadimagelist=`find $loadimage_path -name *.tar.gz`
   fi
   echo -e "\033[33mload image ... ...  \033[0m"
   if [[ ${#loadimagelist} -eq 0 ]]
   then
     echo "not find tar.gz file"
     exit 1
   fi
   for l in ${loadimagelist[@]}
     do
       docker load < $l
       if [[ ! $? -eq 0 ]]
         then
           echo -n "$l"
           echo -e "\033[31m           load failure ...... \033[0m"
           exit 1
      fi
   done
   exit 0
}
upkeep_image (){
  loadcmd=$1
  shift 1
  # load 交给upkeep_loadimage 处理,自己只处理save
  if [[ "$loadcmd" =~ "loadimage" ]]
    then
      upkeep_loadimage $@
  fi
  # save比较麻烦，load很简单，所以load单独拿出来，save就不必考虑load
  # 两者的参数以及类型都不一样，不好兼容
  # 拿image list ，如果0，则全部
  local imagelist=$1
  if [[ "$imagelist" == "0" ]]
    then
      local prolist=(`ls emotibot| sort -g`)
      local upkeeplist=""
      for i in ${prolist[@]}
        do
          i=${i%-*}
          upkeeplist+="${i},"
        done
  fi
  imagelist=${imagelist//[,\\\/\- +_]/ }
  # 保存目标目录的images文件夹下，文件名以今天日期开头
  filename=`date "+%Y%m%d"`
  if [[ -d $2 ]]; then
    image_path=$2
    echo save path      $image_path
  else
    read -t 180 -p $'请输入image path : \n' image_path
    echo save path      $image_path
  fi
  if [ ! -d $image_path/images ]
    then
      image_path="$image_path/images"
      mkdir -p ${image_path}
      if [[ ! $? -eq -0 ]]
        then
          echo " mkdir -p $images_path/images   error ......  "
          exit 1
      fi
  else
    image_path="$image_path/images"
  fi

  # 获取路径，并提交给saveimage方法进行保存

  TMOUT=0
  export TMOUT
  for i in ${imagelist}
  do
    imagepath=`find emotibot/  -type d -name ${i%.*}-*`
    imagesavefile="${imagepath}/module.yaml"
    imagesavefile_2="${imagepath}/*/*.yaml"
    saveimage $i $imagesavefile $imagesavefile_2
    # save init image
    initimagename=${imagepath#*/}
    echo ${imagepath#*/}
    #grep -rn "  ${imagepath#*/}" infra/init-db.yaml  > /dev/null
    grep -rn "  ${imagepath#*/}" infra/init-db.yaml
    if [[ $? -eq 0 ]] ; then
      lineinit=`grep -rn "  ${imagepath#*/}" infra/init-db.yaml | awk -F: '{print $1}'`
      echo $lineinit
      for abc in `seq $lineinit 100`
        do
          linecheck=`sed -n "${abc}"p infra/init-db.yaml`
          if [[ $linecheck =~ "image" ]] && [[ ! $linecheck =~ "#" ]] ; then
            initimage=${linecheck#*:} 
            echo $initimage
            sleep 3
            break
          fi
        done
    fi

    # save initdb image
    docker pull $initimage
    echo $initimage
    if [[ ! -f $image_path/${imagepath#*/}-init.tar.gz ]] ; then
      docker save $initimage | gzip > $image_path/${imagepath#*/}-init.tar.gz
      docker load < $image_path/${imagepath#*/}-init.tar.gz > /dev/null
      if [[ ! $? -eq 0 ]]
        then
          for s in `seq 10`
            do
              eval docker save $initimage | gzip > $image_path/${imagepath#*/}-init.tar.gz
              docker load < $image_path/${imagepath#*/}-init.tar.gz > /dev/null
              if [[ $? -eq 0 ]] ; then
                break
              fi
              if [[ $s -eq 10 ]] ; then
                echo "save $initimage  Error...... Please check"
                exit 1
              fi
            done
      fi
    fi
  done

  saveimage infra infra/infra.yaml infra/infra-distributed.yaml 
  saveimage efk   infra/efk/*.yaml
  saveimage k8s-efk   infra/k8s-yamls/efk/*.yaml
  saveimage monitor   infra/monitor/*.yaml
  saveimage rdate     infra/rdate/*.yaml
  nowpath=`pwd`
  if [[ ! -f $image_path/emotibot_deploy.zip ]];then
    zip -qr $image_path/emotibot_deploy.zip $nowpath
  fi
  exit 0
}

savecommit (){
  if [[ ${#@} -eq 3 ]]
    then
      read -p "save path : " commitpath
      commitold=$2
      commitnew=$3
  elif [[ ${#@} -eq 1 ]]
    then
      read -p "old TAG : " commitold
      read -p "new TAG : " commitnew
      read -p "save path : " commitpath
  elif [[ ${#@} -eq 4 ]]
    then
      commitpath=$4
      commitold=$2
      commitnew=$3
  else
    exit 1
  fi
  if [[ ! -d $commitpath/images ]]
    then
      mkdir -p $commitpath/images
      if [[ ! $? -eq 0 ]]
        then
          echo "dir error ... ... "
          exit 1
      fi
  fi
  commitlist=`git diff $commitold $commitnew  | grep -E "\+[ ]+image:" | awk '{print $3}' | sort | uniq`
  commitlistwc=`git diff $commitold $commitnew | grep -E "\+[ ]+image:" | awk '{print $3}' | sort | uniq | wc -l`
  commitnu=$(($commitlistwc - 1 ))
  for i in $commitlist
  do 
    echo -e  "\033[33mimage 共 $commitlistwc 个, 正在打包$i ，还有$commitnu个 \033[0m"
    docker pull $i
    nameelement=`grep -r --include *.yaml --exclude .*.yaml "$i" -l | head -n 1`
    nameelement=`echo $nameelement | grep -Eo [0-9]`
    name=`echo ${i##*/} | tr ":" "_"`
    docker save $i | gzip > $commitpath/images/$nameelement-`date "+%Y%m%d"`-${name}.tar.gz
    let commitnu-- || true
  done
  exit 1
      
}
