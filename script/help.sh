#!/bin/bash
help (){
  # 脚本使用说明
  echo "Usage: ./deploy.sh [OPTION] [OPTION]  ...
Parameters of the empty:
                                       Deploy the project using interactive mode
                                       By default Zabbix is deployed using docker-compose
  sudo ./deploy.sh                     System init
The parameter is not empty:
OPTION
  1,2,3,4,5,6,7,8,0                    deploy project, 0=1,2,3,4,5,6,7,8
  skip infra                           skip deploy DB
  skip init                            skip init
  skip infra init                      skip deploy DB and init
  onlyinit 1 2                         Start only init 1 2
  onlyinit                             Start only init , interactive mode
  ps                                   check status , interactive mode
      ps 1                                  check 1-bf status
      ps 1,2                                check 1-bf 2-ccbot status
      ps infra                              check DB status
      ......                             ......
  stop                                 stop project , interactive mode
      stop 1                                stop project 1-bf
      stop 1,2                              stop project 1-bf 2-ccbot
      stop infra                            stop project DB
  rm                                   delete project , interactive mode
      rm 1                                  delete project 1-bf
      rm 1,2                                delete project 1-bf 2-ccbot
      rm infra                              delete project DB
  restart                              restart project , interactive mode
      restart 1                             restart project 1-bf
      restart 1,2                           restart project 1-bf 2-ccbot
      restart infra                         restart project DB
  start                                start project , interactive mode
      start 1                               start project 1-bf
      start 1,2                             start project 1-bf 2-ccbot
      start infra                           start project DB
  saveimage                            Save the project's image , interactive mode
      saveimage 1 [PATH]                    Save the project's image 1-bf
      saveimage 1,2 [PATH]                  Save the project's image 1-bf 2-ccbot
  savecommit                           save image ,diff oldtag newtag , interactive mode
      savecommit oldtag newtag [PATH]       save image ,diff oldtag newtag
  loadimage                            load images , interactive model
      loadimage PATH                        load PATH images , interactive mode
  expose                               expose swarm port,interactive mode
      expose 14101                          expose swarm port 14101
      expose 1-bf                           expose swarm 1-bf all port
  log                                  check server or DB log
  onlyup                               only stop rm up service or DB, interactive mode
  onlyup task-engine                        only stop rm up service task-engine
  onlyup task-engine nlu ...                only stop rm up task-engine nlu ...
  onlyup mysql                              only stop rm up mysql, base or distributed-3
  license                              Installation  license module
  zabbix-server up                    #only create zabbix-server
  zabbix-server rm                    #only stop && remove zabbix-server

  mini                                #mini deploy  (mem/cpu limit mode)
  cut                                 #module cut deploy (mem/cpu normal mode)
  cut ccbot qic emotion faq-lite faq-ft faq auth skill ner taskengine intent kg mlp
                                      #cut support arguments above
  mini cut                            #both mini/cut deploy

  k8s-infra rm                        #remove infra && delete volume
  k8s-infra up                        #create infra && create volume
  k8s-infra restart                   #only restart infra docker container

  k8s-module rm 1-bf                  #delete 1-bf module && delete volume
  k8s-module up 1-bf                  #create 1-bf module && create volume
  k8s-module update 1-bf              #update 1-bf module

  k8s-init-db rm 1-bf                 #delete 1-bf init-db
  k8s-init-db up 1-bf                 #create 1-bf init-db

  health                              check db status
  health db                                 check all infra
  health redis                              only check redis
  health mysql                              only check mysql
  health minio                              only check minio
  health consul                             only check consul
  health kafka                              only check kafka
  health es                                 only check es
"
  exit 0
}

