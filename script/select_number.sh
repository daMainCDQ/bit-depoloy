#!/bin/bash
# 对获取到的安装list，进行.moudle.yaml生成,等待最后启动
select_number (){
  # 如果是mini/cut部署，则获取操作命令给mini_cut1,mini_cut2
  if [[ "mini" == "$1" ]] || [[ "cut" == "$1" ]]; then
    mini_cut1=$1
    mini_cut2=$2
    mini_cut_all=$@
  fi
  # 如果是数字快捷部署，则不进入upkeep维护脚本识别与触发
  if [[ ! "$1" =~ ^[0-9] ]]
    then
    signal=$1
    shift 1
  fi
  # 如果是upkeep维护操作，则获取操作命令给upkeep_cmd
  if [[ "$signal" == upkeep ]]
  then
    upkeep_cmd=$1
    shift 1
  fi
  # 如果没有参数，则手动询问参数
  if [[ -n ${@} ]] && [[ "$1" =~ ^[0-9] ]]
  then
    numlist=$@
  else
    read -t 90 -p $'请输入操作序号，如 1,2.1,4,7 \n' numlist 
    if [[ ! $? -eq 0 ]]
      then
        echo "input timeout"
        exit 1
    fi
  fi
  if [ "$numlist" == "q" -o "$numlist" == "quit" -o "$numlist" == "exit" -o "$numlist" == "bye" ]; then
      exit 0
  fi
  # 如果不输入，则默认只部署bfop
  numlist=${numlist:-1}
  # 如果输入0，则表示所有
  if [[ "$numlist" == 0 ]]
    then
      local prolist=(`ls emotibot| sort -g`)
      numlist=""
      for i in ${prolist[@]}
        do
          i=${i%-*}
          numlist+="${i},"
        done
  fi 
  echo input number  is  ${numlist//[,，\\\/\- +_]/ }
  if [[ "$signal" == "upkeep" ]]
    then
      upkeep $upkeep_cmd $numlist
  elif [[ "$signal" == "saveimage" ]]
    then
      upkeep_image saveimage $numlist
  elif [[ "$signal" == "expose" ]]
    then
      project_expose_port ${numlist//[,，\\\/\- +_]/ }
  elif [[ "$signal" == "checklog" ]]
    then
      checklog ${numlist//[,，\\\/\- +_]/ }
  elif [[ "$signal" == "onlyup" ]]
    then
      onlyup_select ${numlist//[,，\\\/\- +_]/ }
  fi

  for n in ${numlist//[,，\\\/\- +_]/ }
  do
    ########## 以下部署质检的特殊情况，请注意 ###################
    if [[ $n == "6" ]]
    then
      n=6.1
    fi
    ########## 以上部署质检的特殊情况，请注意 ###################
    if [[ ! $n =~ "." ]]
    then 
      project=`find emotibot/  -type d -name ${n}-*`
      # 如果是单机版，生成单机版yaml，
      # 如果是多机版，生成swarm yaml
      set -o allexport
      deploy_standard=""
      if [[ -f $project/dev.env ]]
      then
        source $project/dev.env
      fi
      use_efk=""
      if $EFK_DEPLOY
         then
         use_efk="-f $project/module_log_driver.yaml"
      fi
      set +o allexport
      if $ONLY_DEPLOY_BASE
        then
        module_group=""
        #第一个参数或第二个参数包含"cut"就触发裁剪模块
        if [[ "cut" == "$mini_cut1" ]] || [[ "cut" == "$mini_cut2" ]] ; then
          #cut裁剪模块部署模式启动前的 预初始化动作
          module_cut_convert
        #查看cut方式部署标志位是否为true，如果为true说明当前环境已经cut方式启动了module
        elif [[ -f $project/.cut.current ]] && [[ `cat $project/.cut.current` == "true" ]] ; then
          echo -e "  \033[33m  脚本发现当前以cut/裁剪模块 方式部署了$project 产品 \033[0m "
          read -t 180 -p $'默认继续使用该方式,直接敲回车即可，否则请敲 n \n' flag
          if [[ ${flag} == "y" ]] || [[ ${flag} == "Y" ]] || [[ -z ${flag} ]] ; then
            echo -e "  \033[33m  你已选择继续使用 cut/裁剪模块 方式部署$project 产品 \033[0m "
            #cut裁剪模块部署模式启动前的 预初始化动作
            module_cut_convert
          elif [[ ${flag} == "n" ]] || [[ ${flag} == "N" ]] || [[ ${flag} == "no" ]] ; then
            echo -e "  \033[33m  你已选择切换 非(cut/裁剪模块) 方式部署$project 产品 \033[0m "
            echo "false" > $project/.cut.current
          fi
        fi
        #第一个参数或第二个参数包含"mini"就触发降配启动模块
        if [[ "mini" == "$mini_cut1" ]] || [[ "mini" == "$mini_cut2" ]]
          then
          #降配模式下，模块日志输出到json格式的标准输出
          export use_efk=""
          #降配模式标志位
          echo "true" > $project/.mini.current
          if [[ -f $project/dev-mini.env ]]
          then
            source $project/dev-mini.env
            docker-compose -f $project/module.yaml -f $project/port.yaml  $use_efk config > $project/.module.yaml
          else
            docker-compose -f $project/module.yaml -f $project/port.yaml  $use_efk config > $project/.module.yaml
          fi
        #查看mini方式部署标志位是否为true，如果为true说明当前环境已经mini方式启动了module
        elif [[ -f $project/.mini.current ]] && [[ `cat $project/.mini.current` == "true" ]] ; then
          echo -e "  \033[33m  脚本发现当前以mini/降配内存 方式部署了$project 产品 \033[0m "
          read -t 180 -p $'默认继续使用该方式,直接敲回车即可，否则请敲 n \n' flag
          if [[ ${flag} == "y" ]] || [[ ${flag} == "Y" ]] || [[ -z ${flag} ]] ; then
            echo -e "  \033[33m  你已选择继续使用 mini/降配内存 方式部署$project 产品 \033[0m "
            #降配模式下，模块日志输出到json格式的标准输出
            export use_efk=""
            if [[ -f $project/dev-mini.env ]]
            then
              source $project/dev-mini.env
              docker-compose -f $project/module.yaml -f $project/port.yaml  $use_efk config > $project/.module.yaml
            else
              docker-compose -f $project/module.yaml -f $project/port.yaml  $use_efk config > $project/.module.yaml
            fi
          elif [[ ${flag} == "n" ]] || [[ ${flag} == "N" ]] || [[ ${flag} == "no" ]] ; then
            echo -e "  \033[33m  你已选择切换 非(mini/降配内存) 方式部署$project 产品 \033[0m "
            echo "false" > $project/.mini.current
            docker-compose -f $project/module.yaml -f $project/port.yaml  $use_efk config > $project/.module.yaml
          fi
        else
          docker-compose -f $project/module.yaml -f $project/port.yaml  $use_efk config > $project/.module.yaml
        fi
        if [[ -f $project/utils.sh ]]
          then
            source $project/utils.sh
            gen_yaml_project
        fi
      else
        docker-compose -f $project/module.yaml -f $project/swarm.yaml $use_efk config > $project/.module.yaml
      fi
      if $K8S_DEPLOY
        then
        cp $project/module.yaml $project/.module-k8s.yaml
        k8s_convert_load_balance $project
        if [[ "$K8S_DEPLOY_DB" == true ]]
          then
          source dev-k8s.env
        fi
        if [[ -f $project/dev-k8s.env ]]
        then
          source $project/dev-k8s.env
        fi
        docker-compose -f $project/.module-k8s.yaml -f $project/port.yaml config > $project/.module.yaml
        k8s_convert_before $project
        echo -en "  \033[33m 请等待1分钟，后脚本会开启yaml 转换动作 \033[0m "
        echo -e "\nkompose -f $project/.module.yaml convert -o $project/k8s-yamls/modules/origins/"
        kompose -f $project/.module.yaml convert -o $project/k8s-yamls/modules/origins/ > /dev/null 2>&1
        k8s_convert_post $project
        for i in `ls $project/k8s-yamls/modules/origins`
        do
          echo -e "\nkubectl convert -f $project/k8s-yamls/modules/origins/$i > $project/k8s-yamls/modules/$i"
          kubectl convert -f $project/k8s-yamls/modules/origins/$i > $project/k8s-yamls/modules/$i 2>/dev/null
        done
      fi
    elif [[ $n =~ "." ]]
    then
      pname=${n%.*}
      project=`find emotibot/  -type d -name ${pname}-*`
      # 如果是单机版，生成单机版yaml，
      # 如果是多机版，生成swarm yaml
      set -o allexport
      if [[ -f $project/dev.env ]]
      then
        source $project/dev.env
      fi
      use_efk=""
      if $EFK_DEPLOY
         then
         use_efk="-f $project/.module_log_driver.yaml"
      fi
      set +o allexport
      if $ONLY_DEPLOY_BASE
        then
        # 生成 合并后的yaml文档
        result=`bash $project/utils.sh $n genyaml`
        if [[ ! $result == "succeed" ]]
          then
            echo "$project project yaml config error ...... "
            exit 1
        fi
        mv $project/.module.yaml $project/.module.yaml.origin
        docker-compose -f $project/.module.yaml.origin $use_efk config > $project/.module.yaml
      else
        # 生成 合并后的swarm yaml文档
        result=`bash $project/utils.sh $n genyaml swarm`
        if [[ ! $result == "succeed" ]]
          then
          echo "$project project swarm yaml config error ...... "
          exit 1
        fi
        mv $project/.module.yaml $project/.module.yaml.origin
        docker-compose -f $project/.module.yaml.origin $use_efk config > $project/.module.yaml
      fi
      if $K8S_DEPLOY
        then
        if [[ "$K8S_DEPLOY_DB" == true ]]
          then
          source dev-k8s.env
        fi
        if [[ -f $project/dev-k8s.env ]]
        then
          source $project/dev-k8s.env
        fi
        # 生成 合并后的yaml文档
        result=`bash $project/utils.sh $n genyaml`
        if [[ ! $result == "succeed" ]]
          then
            echo "$project project yaml config error ...... "
            exit 1
        fi
        k8s_convert_before $project
        echo -en "  \033[33m 请等待1分钟，后脚本会开启yaml 转换动作 \033[0m "
        echo -e "\nkompose -f $project/.module.yaml convert -o $project/k8s-yamls/modules/origins/"
        kompose -f $project/.module.yaml convert -o $project/k8s-yamls/modules/origins/ > /dev/null 2>&1
        k8s_convert_post $project
        for i in `ls $project/k8s-yamls/modules/origins`
        do
          echo -e "\nkubectl convert -f $project/k8s-yamls/modules/origins/$i > $project/k8s-yamls/modules/$i"
          kubectl convert -f $project/k8s-yamls/modules/origins/$i > $project/k8s-yamls/modules/$i 2>/dev/null
        done
      fi
    fi
  done
}
run_services (){
  # 此时，已经生成好了.module.yaml文档
  # 检查是否需要init
  for i in ${numlist//[,，\\\/\- +_]/ }
  do
    local run_project=`find emotibot/  -type d -name ${i%.*}-*`
    # 判断是否需要初始化
    # 获取最新initdb image
    project_init_image=`grep -E -n "^[ ]+${run_project#*/}" infra/init-db.yaml`
    local linenum=${project_init_image%%:*}
    for line in `seq 100`
    do
      linetext=`sed -n "$linenum"p infra/init-db.yaml`
      if [[ "$linetext" =~ "image" ]]
        then
          new_init_image=${linetext#*:}
          break
      fi
      let linenum++ || true
    done
    # 获取执行过的initdb
    project_init_oldimage=`docker ps -a | grep ${run_project#*/}$ | awk '{print $2}'`
    # 新旧initdb  是否image 一样
    if [[ $@ =~ onlyinit ]] ; then new_init_image=ghy ; fi
    if [[ "${new_init_image// /}" != "${project_init_oldimage// /}" ]]
    then
      docker-compose -f infra/init-db.yaml config > infra/.init-db.yaml
      if [[ ! $? -eq 0 ]]
        then
          echo "initdb.yaml config Error ...... "
          exit 1
      fi
      echo
      if [[ "$K8S_DEPLOY" == true ]] && [[ ! -z $project_init_image ]]
        then
          #k8s_init_db_up ${run_project#*/}
          #echo -en "  \033[33m 待infra部分都running状态之后，执行【 ./deploy.sh k8s-init-db up <对应产品名称> 】进行初始化db\033[0m "
          pod_name_done=`kubectl get pod -n $NAMESPACE | grep ${run_project#*/} | grep Completed | awk '{print $1}'`
          project_init_oldimage_k8s=`kubectl describe pod $pod_name_done -n $NAMESPACE  | grep Image: | awk '{print $2}' | awk '{len=split($0,a,"/");print a[len]}'`
          if [[ ! "${new_init_image// /}" =~ "${project_init_oldimage_k8s// /}" ]]
            then
              if [[ "$K8S_DEPLOY_DB" == true ]]
                then
                source dev-k8s.env
              fi
              docker-compose -f infra/init-db.yaml config > infra/.init-db.yaml
              if [[ ! -z $pod_name_done ]]
              then
                k8s_init_db_remove ${run_project#*/}
              fi
              echo -en "  \033[33m 请等待2分钟，后脚本会自动开启初始化db动作 \033[0m "
              sleep 60
              k8s_init_db_up ${run_project#*/}
              sleep 60
              pod_name_up=`kubectl get pod -n $NAMESPACE | grep ${run_project#*/} | grep ContainerCreating | awk '{print $1}'`
              pod_name_ing=`kubectl get pod -n $NAMESPACE | grep ${run_project#*/} | grep Running | awk '{print $1}'`
              pod_name_done=`kubectl get pod -n $NAMESPACE | grep ${run_project#*/} | grep Completed | awk '{print $1}'`
              pod_name_error=`kubectl get pod -n $NAMESPACE | grep ${run_project#*/} | grep Error | awk '{print $1}'`
              while [[ ! -z $pod_name_up ]]
              do
                echo -e "  \033[33m  init ${run_project#*/}  pod 创建中，请等待 \033[0m "
                sleep 30
                pod_name_up=`kubectl get pod -n $NAMESPACE | grep ${run_project#*/} | grep ContainerCreating | awk '{print $1}'`
                pod_name_ing=`kubectl get pod -n $NAMESPACE | grep ${run_project#*/} | grep Running | awk '{print $1}'`
                pod_name_done=`kubectl get pod -n $NAMESPACE | grep ${run_project#*/} | grep Completed | awk '{print $1}'`
                pod_name_error=`kubectl get pod -n $NAMESPACE | grep ${run_project#*/} | grep Error | awk '{print $1}'`
              done
              while [[ ! -z $pod_name_ing ]]
              do
                echo -e "  \033[33m  init ${run_project#*/}  init 初始化数据中，请等待 \033[0m "
                kubectl logs -f $pod_name_ing -n $NAMESPACE
                sleep 5
                pod_name_ing=`kubectl get pod -n $NAMESPACE | grep ${run_project#*/} | grep Running | awk '{print $1}'`
                pod_name_done=`kubectl get pod -n $NAMESPACE | grep ${run_project#*/} | grep Completed | awk '{print $1}'`
                pod_name_error=`kubectl get pod -n $NAMESPACE | grep ${run_project#*/} | grep Error | awk '{print $1}'`
              done
              if [[ ! -z $pod_name_error ]] && [[ -z $pod_name_done ]]
              then
                echo -e "  \033[33m  init ${run_project#*/}  pod 启动失败 \033[0m "
                kubectl logs -f `echo $pod_name_error | awk '{print $1}'` -n $NAMESPACE
                k8s_init_db_remove ${run_project#*/}
                echo -e "  \033[33m  init ${run_project#*/} error . 请联系init image 的作者  \033[0m "
                exit 1
              fi
              if [[ -z $pod_name_done ]]
              then
                echo -e "  \033[33m  init ${run_project#*/}  pod 没有启动 \033[0m "
                echo -e "  \033[33m  请通过命令< kubectl get pod -n $NAMESPACE | grep ${run_project#*/} > 检查 \033[0m "
                exit 1
              else
                kubectl logs -f $pod_name_done -n $NAMESPACE
                echo -e "  \033[33m  init ${run_project#*/}  init 完成 \033[0m "
                if [[ ! -z $pod_name_error ]]
                then
                  kubectl delete pod $pod_name_error -n $NAMESPACE
                fi
              fi
              initlog=`kubectl logs -f $pod_name_done -n $NAMESPACE`
              if [[ "$initlog" =~ "Fail" ]]
              then
                k8s_init_db_remove ${run_project#*/}
                echo -e "  \033[33m  init ${run_project#*/} error . 请联系init image 的作者  \033[0m "
                exit 1
              fi
          fi
      elif [[ ! -z $project_init_image ]]
        then
          echo -en "  \033[33m init有变更或错误，需要重新init ${run_project#*/}  \033[0m "
          sleep 2
      fi
      # 是否需要跳过init
      if [[ ! "$@" =~ "skip init" ]] && [[ ! "$@" =~ "skip infra init" ]] && [[ "$K8S_DEPLOY" == false ]] || [[ "$@" =~ onlyinit ]]
        then
        if [[ ! -z $project_init_image ]]
          then
          echo -e "  \033[31m  init ${run_project#*/} 开始\033[0m "
          echo -e "  \033[31m  此过程耗时较长，请持续关注，如果中断请再次运行脚本  \033[0m "
          sleep 1
          docker-compose -f infra/.init-db.yaml up -d ${run_project#*/}
          if [ $? -ne  0 ];then 
               echo -e  "\033[31m ${run_project#*/}  init module create fail \033[0m"
                exit 1
            fi
          docker-compose -f infra/.init-db.yaml logs -f ${run_project#*/}
          initlog=`docker logs ${run_project#*/} --tail 4`
          if [[ "$initlog" =~ "Fail" ]]
            then
              docker rm -f ${run_project#*/}
              echo -e "  \033[33m  init ${run_project#*/} error . 请联系init image 的作者  \033[0m "
              exit 1
          fi
        else
          echo -e "  \033[33m  ${run_project#*/} 不需要init  \033[0m "
        fi
      elif [[ "$K8S_DEPLOY" == false ]]
        then
        echo -e "\033[31m skip          init ${run_project#*/}  \033[0m"
      fi
    else
      #如果initdb还在运行中，继续打印输出initdb容器日志
      initdb_running=`docker ps -f status=running | grep ${run_project#*/}$ | awk '{print $NF}'`
      if [[ ! -z $initdb_running ]]
        then
          echo -e "  \033[31m  init ${run_project#*/} 继续执行中。。。 \033[0m "
          echo -e "  \033[31m  此过程耗时较长，请持续关注，如果中断请也再次运行脚本  \033[0m "
          sleep 1
          docker-compose -f infra/.init-db.yaml logs -f ${run_project#*/}
          initlog=`docker logs ${run_project#*/} --tail 4`
          if [[ "$initlog" =~ "Fail" ]]
            then
              docker rm -f ${run_project#*/}
              echo -e "  \033[33m  init ${run_project#*/} error . 请联系init image 的作者  \033[0m "
              exit 1
          fi
      fi
    fi
    if $ONLY_DEPLOY_BASE && [[ "$K8S_DEPLOY" == false ]] && [[ ! "$@" =~ onlyinit ]]
      then
        echo "deploy ${run_project}"
        echo -e "\033[32m 部署 `cat ${run_project}/readme.md` \033[0m "
        echo docker-compose -f $run_project/.module.yaml up -d $module_group
        echo
        docker-compose -f $run_project/.module.yaml up -d $module_group
        echo -e "  \033[33m  模块启动中请稍候。。。  \033[0m "
        while true
        do
          sleep 10
          container_status=`docker-compose -f $run_project/.module.yaml ps | grep -vE "Ports|Up|----------" | grep -vE "^[ ]" | awk '{print $1}'`
          if [[ -z $container_status ]]
            then
              echo -e "  \033[32m  模块启动完成，请打开浏览器访问本机IP的${LB_PORT} 端口  \033[0m "
              break
          fi
        done
    elif [[ "$K8S_DEPLOY" == false ]] && [[ ! "$@" =~ onlyinit ]]
      then
        echo "deploy ${run_project}"
        echo -e "\033[32m 部署 `cat ${run_project}/readme.md` \033[0m "
        echo docker stack deploy -c $run_project/.module.yaml ${run_project#*/}
        echo
        #swarm_dns_convert 函数判断service是否需要切换swarm dns模式（dnsrr/vip）而做相应的动作
        swarm_dns_convert
        docker stack deploy --resolve-image never -c $run_project/.module.yaml ${run_project#*/}
        # update 检查
        updatecheck
        echo -e "  \033[33m  检查模块启动是否符合预期。。。  \033[0m "

        # 循环遍历 期望的副本数 与 实际的副本数 是否相等来判断启动完成与否
        while true
        do
          sleep 2
          # 实际的副本数
          action_replica=(`docker service ls | grep $run_project | awk '{print $4}' | awk -F "/" '{print $1}'`)
          # 期望的副本数
          desire_replica=(`docker service ls | grep $run_project | awk '{print $4}' | awk -F "/" '{print $2}'`)
          if [[ ${action_replica[@]} == ${desire_replica[@]} ]] 
            then
              echo -e "  \033[32m  模块启动完成，请打开浏览器访问本机IP的${LB_PORT} 端口  \033[0m "
              break
          else
            sleep 2
          fi
        done
    fi
    if [[ -f "$run_project/postjobs.sh" ]] && [[ "$K8S_DEPLOY" == false ]]
      then
      echo "Call post job script for $run_project"
      bash "$run_project/postjobs.sh"
    fi
    if [[ "$K8S_DEPLOY" == true ]]
      then
      echo "deploy ${run_project}"
      echo -e "\033[32m 部署 `cat ${run_project}/readme.md` \033[0m "
      k8s_module_up ${run_project#*/}
      echo -e "  \033[33m  模块启动中请稍候。。。  \033[0m "
      # 循环遍历 pod内容器 是否启动完成
      while true
      do
        sleep 10
        pod_status=`kubectl get pod -n $NAMESPACE | grep -v NAME |grep -v Completed | awk '{print $2}' | grep 0/`
        if [[ -z $pod_status ]]
          then
            echo -e "  \033[32m  模块启动完成，请打开浏览器访问本机IP的31080 或 自定义的前端端口  \033[0m "
            break
        fi
      done
    fi
  done
}

