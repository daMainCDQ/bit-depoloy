#!/bin/bash

log_format () {
  # 由 checklog 函数传递，负责处理打印格式化的log选择列表
  local length1=${#1}
  let length1-=3 || true
  local length2=${#2}
  let length2-=24 || true

  if [[ $3 == "line" ]]
  then
   printf "${1}%${length1#-}s ${2}%${length2#-}s \n"
  else
   printf "${1}%${length1#-}s ${2}%${length2#-}s"
  fi
  # 监视输入，并提交给日志打印
}

checklog (){
  # 打印log，或者打印swarm启动错误log，只有交互模式
  echo "project service list  "
  local project=`find emotibot -type d -name ${1}-*`
  #local project=${project#*/}
  infralog=false
  if [[ $1 == infra ]]
    then
      servicelist=(`sed -n -E "/^[ ]{2}[a-zA-Z0-9_-]{1,30}[:][ ]{0,2}$/p" infra/.infra.yaml | tr -d " "| tr -d : | grep -v -E "default|^emotibot$|hostnet"`)
      infralog=true
  else
      # 获取compose文件的服务名称
      #servicelist=(`docker-compose -f ${project}/.module.yaml images | awk 'NR>2{print}' | awk '{print $1}'`)
      servicelist=(`sed -n -E "/^[ ]{2}[a-zA-Z0-9_-]{1,30}[:][ ]{0,2}$/p" ${project}/.module.yaml | tr -d " "| tr -d : | grep -v -E "default|^emotibot$|hostnet"`)
  fi
  for i in `seq 0 ${#servicelist[@]}`
  do
    # 每行打印3列
    count=$((($i+1) % 3))
    if [[ $count -eq 0 ]]
      then
        log_format $i ${servicelist[$i]} line
    elif [[ $i -eq ${#servicelist[@]} ]]
      then
        echo
    else
        log_format $i ${servicelist[$i]}
    fi
  done
  # 准备打印日志
  read -p "check server log number : " svnumber
  if [[ ! "$svnumber" =~ ^[0-9] ]] || [[ $svnumber -ge ${#servicelist[@]} ]] 2> /dev/null
    then
      echo "input error ......"
      exit 0
  else
      if ! $infralog
        then
          if $ONLY_DEPLOY_BASE
            then
              docker-compose -f ${project}/.module.yaml logs -f --tail 200 ${servicelist[$svnumber]}
          # 处理多机版日志
          # swarm 日志分两种，容器正常启动，打印正常日志，否则打印启动错误原因
          else
              # 启动没有完成的情况，比如缺少volume image等
              swarmervicename=`docker service ls| grep ${servicelist[$svnumber]} | awk '{print $2}'`
              if [[ `docker service ls | grep ${servicelist[$svnumber]}` =~ " 0/" ]]
                then
                  echo "Up error or waited for Up"
                  echo $swarmervicename
                  docker inspect --format "{{.Status.Err}}" $(docker service ps $swarmervicename | awk '{print $1}' | head -n 3 | tail -n 1)
              else
                  echo docker service logs -f --tail 200 $swarmervicename
                  sleep 1
                  docker service logs -f --tail 200 $swarmervicename
              fi
          fi
      else
          docker-compose -f infra/.infra.yaml logs -f --tail 200 ${servicelist[$svnumber]}
      fi
      exit 0
  fi
}

