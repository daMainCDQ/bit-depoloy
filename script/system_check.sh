#/bin/bash

check1 (){
  dockercheck=`docker ps`
  if [[ ! $? -eq 0 ]]
    then
      echo "docker not installed  or permission denied"
  # 检查自己是不是docker环境
  elif [[ -f /.dockerenv ]]
    then
      echo "In-container deployment is not supported !!!"
  else
    echo true
  fi
}

check2 (){
# 机器配置检测
#	docker 版本检查
  dockerversion=`docker info --format "{{json .ServerVersion}}"`
  if [[ $dockerversion < 17.09 ]]
    then
      echo "docker version less than 17.09"
  else
    echo true
  fi
}
#	docker-compose  版本检查
check3 (){
  dcpath=`which docker-compose`
  if [[ $? -eq 0 ]]
    then
      dcversion=`$dcpath -v  | awk -F "[ ,]" '{print $3}'`
      if [[ $dcversion < 1.18 ]]
        then
          echo "docker-compose version less than 1.18"
      else
          echo true
      fi
  else
     echo "docker-compose not installed "
  fi
}
#	docker network 冲突检测
check4 (){
#    swarm_state=`docker info --format '{{json .Swarm.LocalNodeState}}'|sed -n 's@\"@@gp'`
    if [ $ONLY_DEPLOY_BASE == false ]; then
      swarm_state=active
    else
      swarm_state=inactive
    fi
    if [ $swarm_state == active ]; then
      local_type=swarm
    else
      local_type=single
    fi
    exist_network=`docker network ls|awk '$2 == "emotibot"{print $3}'`
    if [[ $local_type == single ]] && [[ $exist_network == overlay ]]
    then
      echo -e "\033[;31m network emotibot is not bridge ! \033[0m" && exit 1
    elif [[ $local_type == swarm ]] && [[ $exist_network == bridge ]]
    then
      echo -e "\033[;31m network emotibot is not overlay ! \033[0m" && exit 1
    fi
    echo true
}
#容器运行冲突检测
check5 (){
  #    检查所有module中定义的容器名称是否存在冲突
  name_clash=`grep -rn container_name --exclude *.*.yaml* emotibot | grep -v "#" | awk '{print $3}' | sort | uniq -d`
  #    检查所有yaml文件的 端口 开放是否存在冲突
  port_clash=`grep -E -rn "\-[\"'' ]+[0-9]+:[0-9]+" --include module.yaml --include port.yaml emotibot | grep -v "#" | awk '{print $3}' | sort | uniq -d`
  if [[ ! -n $port_clash ]] ; then port_clash=`grep -E -rn "\-[\"'' ]+[0-9]+:[0-9]+" --include module.yaml --include swarm.yaml emotibot | grep -v "#" | awk '{print $3}' | sort | uniq -d` ; fi
  if [[ -n $port_clash ]]
    then
      for i in $port_clash
      do
          echo "container port clash ,  $i "
      done
  fi
  #    dev.env 变量定义  子项目与总dev.env存在重复或者冲突检测
  env_clash=`grep -E -h  -r "[a-Z0-9]+=" --include dev.env emotibot dev.env  | grep -v "#" | sort | uniq -d`
  if [[ -n $name_clash ]]
    then
      echo "container name clash ,  $name_clash"
  elif [[ -n $port_clash ]]
    then
      echo "container port clash ,  $i "
  elif [[ -n $env_clash ]]
    then
      echo "container env clash ,  $env_clash"
  else
      echo true
  fi
#
}
check6 (){
    source dev.env
    # 找出ip地址
    ips_result=`grep -Er "(2[0-4][0-9]|25[0-5]|1[0-9][0-9]|[1-9]?[0-9])(\.(2[0-4][0-9]|25[0-5]|1[0-9][0-9]|[1-9]?[0-9])){3}" --include *yaml --exclude .module.yaml emotibot |  grep -vE "${DEFAULT_IP}|${DB_HOST_1}|${DB_HOST_2}|${DB_HOST_1}|0.0.0.0|\#" | grep -Eho "(2[0-4][0-9]|25[0-5]|1[0-9][0-9]|[1-9]?[0-9])(\.(2[0-4][0-9]|25[0-5]|1[0-9][0-9]|[1-9]?[0-9])){3}" | sort | uniq`
    if [[ -n $ips_result ]] ; then
        local spot=0
        echo -e "\033[;31mExternal ip addresses \033[0m"
        # 每行打印5 个 ip
        for ip in $ips_result ; do
          let spot++ || true
          if [[ $(($spot % 5)) -eq 0 ]] ; then
            echo  "$ip  "
          else
            echo -n "$ip  "
          fi
        done
    fi
}

run_check (){
str='#'
PER=0
for i in `seq 1 5`
do
    checkname=check${i}
    check_result=`$checkname`
    if [[ $check_result == true ]]
      then
      for n in `seq 1 10`
      do
        if [[ ${#str} -eq 50 ]]
        then
            result=ok
        fi
        printf "run_check: [%-50s] [%-3s%%]\033[;32m %s \033[0m\r" $str $(($PER*2)) "$result"
        str+='#'
        PER=${#str}
        sleep 0.05
      done
    else
      result=faild
      printf "\nrun_check: [%-50s] [%-3s%%]\033[;31m %s \033[0m %s\r" $str $(($PER*2)) "    $result"
      echo $check_result
      exit 1
    fi
done
#else
#  for i in  `seq 1 50`
#    do
#    printf "run_check: [%-50s] [%-3s%%]\033[;32m %s \033[0m\r" $str $(($PER*2)) "$result"
#    str+='#'
#    PER=${#str}
#    sleep 0.002
#  done
#
#fi
printf "\n"
nowtime=`date`
if [[ $nowtime =~ CST ]] ; then
  echo -e "local time  $nowtime"
else
  echo -e "local time  \033[5;41;33m $nowtime \033[0m"
fi
check6
printf "\n"
}
