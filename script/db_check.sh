#!/bin/bash

# ===============================================
# author: fenhuadeng
# desc: check db
# date: 2020-02-20
# ===============================================

kit_path=./script/before_check
source ./script/dbchecks/consulcheck.sh
source ./script/dbchecks/escheck.sh
source ./script/dbchecks/kafkacheck.sh
source ./script/dbchecks/miniocheck.sh
source ./script/dbchecks/mysqlcheck.sh
source ./script/dbchecks/redischeck.sh

case $1 in
  mysql)
    if [[ ! -z $5 ]];then
      mysql_check $2 $3 $4 $5
    else
      mysql_check ${MYSQL_HOST} ${MYSQL_USER} ${MYSQL_PASS} ${MYSQL_PORT}
    fi
    ;;
  redis)
    if [[ ! -z $4 ]]; then
      redis_single_check $2 $3 $4
    elif [[ ! -z $3 ]]; then
      redis_single_check $2 $3
    elif [[ ! -z ${REDIS_TYPE} ]] && [[ "${REDIS_TYPE}" == "cluster" ]];then
      redis_cluster_check ${REDIS_HOST} ${REDIS_PASS}
    else
      redis_single_check ${REDIS_HOST} ${REDIS_PORT} ${REDIS_PASS}
    fi
    ;;
  rediscluster)
    if [[ ! -z $3 ]];then
      redis_cluster_check $2 $3
    elif [[ ! -z $2 ]]; then
      redis_cluster_check $2
    elif [[ ! -z ${REDIS_TYPE} ]] && [[ "${REDIS_TYPE}" == "cluster" ]];then
      redis_cluster_check ${REDIS_HOST} ${REDIS_PASS}
    else
      echo "\033[31mredis_cluster usage: bash db_check.sh rediscluster 'hosts' [login_password]\033[0m"
    fi
    ;;
  es)
    if [[ ! -z $4 ]];then
      es_check $2 $3 $4
    elif [[ ! -z $3 ]]; then
      es_check $2 $3 unuse:unuse
    else
      es_check ${ES_HOST} ${ES_PORT} ${ES_USER}:${ES_PASS}
    fi
    ;;
  consul)
    if [[ ! -z $3 ]];then
      consul_check $2 $3
    else
      consul_check ${CONSUL_HOST} ${CONSUL_PORT}
    fi
    ;;
  kafka)
    if [[ ! -z $3 ]];then
      kafka_check $2 $3
    else
      kafka_check ${KAFKA_HOST} ${KAFKA_PORT}
    fi
    ;;
  minio)
    if [[ ! -z $5 ]];then
      minio_check $2 $3 $4 $5
    else
      minio_check ${MINIO_HOST} ${MINIO_PORT} ${MINIO_ACCESS_KEY} ${MINIO_SECRET_KEY}
    fi
    ;;
  all)
    mysql_check ${MYSQL_HOST} ${MYSQL_USER} ${MYSQL_PASS} ${MYSQL_PORT}
    if [[ ! -z ${REDIS_TYPE} ]] && [[ "${REDIS_TYPE}" == "cluster" ]];then
      redis_cluster_check ${REDIS_HOST} ${REDIS_PASS}
    else
      redis_single_check ${REDIS_HOST} ${REDIS_PORT} ${REDIS_PASS}
    fi
    es_check ${ES_HOST} ${ES_PORT} ${ES_USER}:${ES_PASS}
    consul_check ${CONSUL_HOST} ${CONSUL_PORT}
    kafka_check ${KAFKA_HOST} ${KAFKA_PORT}
    minio_check ${MINIO_HOST} ${MINIO_PORT} ${MINIO_ACCESS_KEY} ${MINIO_SECRET_KEY}
    ;;
esac

#   -h|--help)
#    echo -e "\033[0;36mUsage: bash db_check.sh [OPTION] [PARAMETER]\033[0m
#The parameter is not empty:
#OPTION
#mysql                                                     check mysql
#  mysql login_ip login_user login_password login_port     ex: mysql 172.17.0.1 root [password] 3306
#redis                                                     check redis
#  redis login_ip login_port [login_password]              ex: redis 172.17.0.1 6379 [password]
#rediscluster                                              check redis cluster
#  rediscluster 'hosts' [login_password]                   rediscluster '172.16.102.6:7001,...' [password]
#es                                                        check es
#  es login_ip login_port login_auth                       ex: es 172.17.0.1 9200 unuse:unuse
#consul                                                    check consul
#  consul login_ip login_port                              ex: consul 172.17.0.1 8500
#kafka                                                     check kafka
#  kafka login_ip login_port                               ex: kafka 172.17.0.1 9092
#minio                                                     check minio
#  minio login_ip login_port ACCESS_KEY SECRET_KEY         ex:  minio 172.17.0.1 9000 [access_key] [secret_key]
#all                                                       check all db status"
