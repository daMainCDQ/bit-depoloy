#!/bin/bash
banner_format () {
  # 由 banner_list 函数传递，负责处理打印格式化的选择列表
  length1=${#1}
  let length1-=6 || true
  length2=${#2}
  let length2-=42 || true
  printf "[ -------- ${1}%${length1#-}s ${2}%${length2#-}s --------] \n"
}

banner_list () {
  # 获取产品列表，并生成数字对应关系
   list_install=(`ls emotibot | sort -g`)
   list_sum=`echo ${#list_install[@]}`
   for i in `seq 0 $list_sum`
   do
     # 最后一个不参与输出提示，因为
     # 元组个数从一开始，位置从0开始
     if [[ ! $i -eq ${#list_install[@]} ]]
     then
       num=$i
       numvar=${list_install[$i]}
       # 如果存在选项脚本，则需要展示不同部署
       if [[ -f emotibot/${numvar}/utils.sh ]]
         then
           option=(`emotibot/${numvar}/utils.sh option`)
           for o in ${option[@]}
           do
             explant=`emotibot/${numvar}/utils.sh $o`
             banner_format $o "$explant"
           done
         let num+=1 || true
         continue
       fi
       let num+=1 || true
       numvar=`echo $numvar | awk -F "-" '{print $2}'`
       banner_format $num $numvar
     fi
   done
}

