#!/bin/bash
##
##  guoheyang 
##
xy (){
  # 获取当前光标
  # 防止未换行的光标占位
  echo
  exec < /dev/tty
  oldstty=$(stty -g)
  stty raw -echo min 0
  # on my system, the following line can be replaced by the line below it
  echo -en "\033[6n" > /dev/tty
  # tput u7 > /dev/tty    # when TERM=xterm (and relatives)
  IFS=';' read -r -d R -a pos
  stty $oldstty
  # change from one-based to zero based so they work with: tput cup $row $col
  row=$((${pos[0]:2} - 1))    # strip off the esc-[
  col=$((${pos[1]} - 1))
  #  返回 x y 轴坐标
  echo $row $col
}
upcheck_log_format () {
  # 由 updatecheck 函数传递，负责处理打印格式化的更新列表
  local length1=${#1}
  let length1-=$3 || true
  local length2=${#2}
  let length2-=40 || true
  printf " ${1}%${length1#-}s: ${2}%${length2#-}s \n"
}

updatecheck (){
  # 抓取更新信息
  swarm_update_list=()
  swarm_update_list_col=0
  swarm_list=`docker service ls | awk 'NR>1{print $2}'`
  # 初始化一个变量用来记录最长的名字
  long=0
  echo -e "\033[33m          Update list check ......\033[0m" 
  for service in $swarm_list
  do
    service_upstats=`docker service inspect $service --pretty`
    if [[ ! "$service_upstats" =~ "completed" ]] && [[ "$service_upstats" =~ " Message:" ]]
      then
        if [[ $long -lt ${#service} ]]
        then
          # 抓取哪个名字最长
          long=${#service}
        fi
        swarm_update_list[$swarm_update_list_col]=$service
        let swarm_update_list_col++ || true
    fi
  done
  # 判断是否有更新中的服务，有的话，则开始统计
  if [[ ! ${#swarm_update_list[@]} -eq 0 ]]
    then
    a=(`xy`)
    lines=`tput lines`
    # 如果屏幕剩余空间，足够打满信息，则采用光标跳转的规则
    if [[ $((${a[0]} + ${#swarm_update_list[@]} + 2)) -le $lines ]]
      then
        eval x=$((${a[0]} +1))
    else
      # 如果屏幕剩余空间无法容纳所有信息，则推算光标跳转
      eval diffline=$(($lines - $((${a[0]}))))
      lack=$((${a[0]} + ${#swarm_update_list[@]} +2 - $lines))
      eval x=$((${a[0]} - $lack +1))
    fi
    echo "updataing service ...... ${#swarm_update_list[@]}" 
    # 输出格式化时，最长名称与消息内容的间距
    longprintf=$(($long + 4))
    # 不同的更新条数，采用不同的刷新时间，条数越少，刷新越慢
    # 个人洁癖，意义不大，顶多让cpu歇会
    if [[ ${#swarm_update_list[@]} -lt 5 ]] ; then
      sleeptime=0.5
    elif [[ ${#swarm_update_list[@]} -lt 10 ]] ; then
      sleeptime=0.35
    elif [[ ${#swarm_update_list[@]} -lt 15 ]] ; then
      sleeptime=0.2
    else
      sleeptime=0.1
    fi
    # 开始循环查询
    while true
    do
    # 退出条件预设值
    completed=0
    for i in ${swarm_update_list[@]}
    do
      stats=`docker service inspect $i --pretty | grep " Message:"`
      stats=${stats#*:}
      upcheck_log_format "$i" "$stats" $longprintf
      sleep $sleeptime
      if [[ ! "$stats" =~ "update in progress" ]]
        then
          let completed++ || true
      fi
    done
    if [[ $completed -eq ${#swarm_update_list[@]} ]]
      then
        break
    fi
    # 光标跳转到变量 x y 的坐标轴
    tput cup $x ${a[1]}
    done
  upserver=upserver
  echo -e "\033[33m          Update list image ......\033[0m"
  for ups in ${swarm_update_list[@]}
    do
      upserver="${upserver}|$ups"
    done
      docker service ls  | grep -E "$upserver" | awk '{print $5}'
  fi
}
