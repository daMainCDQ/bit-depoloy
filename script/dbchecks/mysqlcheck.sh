#!/bin/bash

mysql_check() {
  host=$1
  user=$2
  password=$3
  port=$4
  ping=`which ping`
  echo -e "\n\033[0;36m --- Check mysql --- \033[0m"
  if ping -c 1 $host &>/dev/null
  then
    mgr_file=/tmp/mgr_result
    init_result=/tmp/init_result
    max_num=/tmp/maxnum
    loc_num=/tmp/locnum
    read_on=/tmp/readon
    databases=/tmp/databases
    ($kit_path/mysql -h${host} -u${user} -p${password} -P ${port} -e "show databases;" &>$databases )
    if [[ $? -eq 0 ]];then
      echo -e "\n\033[0;36mshow databases:\033[0m" && (cat $databases |grep -v -E "Warning|Database"|tr "\n" "\t")
    else
      (echo -e "\033[31m Can't connect to MySQL server \033[0m" && exit 1)
    fi
    $kit_path/mysql -h${host} -u${user} -p${password} -P ${port} -e "show variables like 'max_connections%';" &>$max_num
    $kit_path/mysql -h${host} -u${user} -p${password} -P ${port} -e "show status like 'Threads_connected';" &>$loc_num
    $kit_path/mysql -h${host} -u${user} -p${password} -P ${port} -e "show global variables like 'read_only';" &>$read_on
    maxnum=(`cat $max_num`)
    locnum=(`cat $loc_num`)
    readon=(`cat $read_on`)
    echo -e "\n\033[0;36mshow mysql config: \033[0m"
    echo "the max connections: ${maxnum[-1]}"
    echo "the thread connections: ${locnum[-1]}"
    echo "the mysql read_only lock status: ${readon[-1]}"
    $kit_path/mysql -h${host} -u${user} -p${password} -P ${port} -e "SELECT MEMBER_HOST, MEMBER_STATE FROM performance_schema.replication_group_members;" &>$mgr_file
    if [ $? -eq 0 ] && [[ ! -z $(grep -v Warning $mgr_file) ]]; then
      echo -e "\n\033[36mshow mysqlmgr status: \033[0m"
      cat $mgr_file
      $kit_path/mysql -h${host} -u${user} -p${password} -P ${port} -e "SELECT MEMBER_HOST, MEMBER_STATE FROM performance_schema.replication_group_members;" &>/tmp/mgr_result
      mgr_result=(`grep -v Warning /tmp/mgr_result`)
      # shellcheck disable=SC2068
      if [ `echo ${mgr_result[@]}|grep -o ONLINE|wc -l` -le 3 ]; then
          echo -e "\033[31m mysqlmgr is unhealthy \033[0m"
      fi
    fi
  else
    echo -e "\033[31m Unable to ping $host \033[0m"
    exit 1
  fi
}
