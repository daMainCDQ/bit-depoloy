#!/bin/bash

consul_check() {
  host=$1
  port=$2
  echo -e "\n\033[0;36m --- Check consul --- \033[0m\n"
  if ping -c 1 $host &>/dev/null
  then
    lead_file=/tmp/lead_result
    peer_file=/tmp/peer_result
    curl http://${host}:${port}/v1/status/leader &>$lead_file
    grep "Connection refused" $lead_file &>/dev/null && (echo -e "\033[31m Unable connect to consul ${host}:${port} \033[0m" && exit 1)
    if [ -z `cat $lead_file` ];
    then
      echo -e "\033[31m No leader \033[0m" && exit 1
    fi
    curl -s http://${host}:${port}/v1/status/peers &>$peer_file && echo -e "check consul status ... \033[32mok\033[0m"
    echo "consul peers: `cat $peer_file`"
  else
    echo -e "\033[31m Unable to ping $host \033[0m"
    exit 1
  fi
}

