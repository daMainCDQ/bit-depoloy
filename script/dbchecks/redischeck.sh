#!/bin/bash

redis_single_check() {
  host=$1
  port=$2
  password=$3
  echo -e "\n\033[0;36m --- Check redis ---\033[0m\n"
  if ping -c 1 $host &>/dev/null
  then
    ping_file=/tmp/ping_result
    set_file=/tmp/set_result
    get_file=/tmp/get_result
    if [ ! -z "$password" ]; then
        $kit_path/redis-cli -h $host -p $port -a $password ping &>$ping_file
        (cat $ping_file|grep "Connection refused" &>/dev/null) && (echo -e "redis connect ... \033[31m failed \033[0m" && exit 1)
        if [[ `cat ${ping_file}` == "PONG" ]];then echo -e "redis connect ... \033[32m success \033[0m";fi
        $kit_path/redis-cli -h $host -p $port -a $password set name fenhuadeng &>$set_file
        $kit_path/redis-cli -h $host -p $port -a $password get name &>$get_file
        if [[ `cat ${set_file}` == "OK" ]] && [[ `cat ${get_file}` == "fenhuadeng" ]]; then
          echo -e "redis write data ... \033[32m success \033[0m"
        else
          echo -e "redis write data ... \033[31m failed \033[0m"
        fi
    else
        $kit_path/redis-cli -h $host -p $port ping &>$ping_file
        (cat $ping_file|grep "Connection refused" &>/dev/null) && (echo -e "redis connect ... \033[31m failed \033[0m" && exit 1)
        if [[ `cat ${ping_file}` == "PONG" ]];then echo -e "redis connect ... \033[32m success \033[0m";fi
        $kit_path/redis-cli -h $host -p $port set name fenhuadeng &>$set_file
        $kit_path/redis-cli -h $host -p $port get name &>$get_file
        if [[ `cat ${set_file}` == "OK" ]] && [[ `cat ${get_file}` == "fenhuadeng" ]]; then
          echo -e "redis write data ... \033[32m success \033[0m"
        else
          echo -e "redis write data ... \033[31m failed \033[0m"
        fi
    fi
  else
    echo -e "\033[31m Unable to ping $host \033[0m"
    exit 1
  fi
}

redis_cluster_check() {
  # "172.16.102.6:7001,172.16.102.7:7001,172.16.102.8:7001"
  hosts=$1
  password=$2
  echo -e "\n\033[0;36m --- Redis check --- \033[0m\n"
  redis_group=(`echo "$hosts"|sed -n 's/,/ /gp'`)
  cluster_stat=ok
  # shellcheck disable=SC2068
  for aredis in ${redis_group[@]}
  do
    ip_port=(`echo "$aredis" |sed -n 's/:/ /gp'`)
    host=${ip_port[0]}
    port=${ip_port[-1]}
    if ping -c 1 $host &>/dev/null
    then
      ping_file=/tmp/ping_result
      set_file=/tmp/set_result
      info_file=/tmp/info_result
      get_file=/tmp/get_result
      ld=`date`
      if [ ! -z "$password" ]; then
        $kit_path/redis-cli -h $host -p $port -a $password -c ping &>${ping_file}
        (cat $ping_file|grep "Connection refused" &>/dev/null) && (echo -e "node ${host}:${port} connect ... \033[31m failed \033[0m")
        if [[ `cat ${ping_file}` == "PONG" ]];then
          echo -e "node ${host}:${port} connect ... \033[32m success \033[0m";
          $kit_path/redis-cli -h $host -p $port -a $password -c set test "${ld}" &>$set_file
          $kit_path/redis-cli -h $host -p $port -a $password -c get test &>$get_file
          if [[ `cat $set_file` == 'OK' ]] && [[ `cat $get_file` == "${ld}" ]]; then
            echo -e "node ${host}:${port} write data ... \033[32m success \033[0m";
            $kit_path/redis-cli -h $host -p $port -a $password -c cluster info &>$info_file
            grep "cluster_state:ok" $info_file &>/dev/null
            if [ $? -ne 0 ]; then
                echo -e "check cluster status ... \033[31mno\033[0m"
                echo -e "\033[0;36m --- show redis cluster nodes --- \033[0m"
                $kit_path/redis-cli -h $host -p $port -a $password -c cluster nodes
                cluster_stat=no
                exit 1
            fi
          else
            echo -e "node ${host}:${port} write data ... \033[31m failed \033[0m"
          fi
        fi
      else
        $kit_path/redis-cli -h $host -p $port -c ping &>${ping_file}
        (cat $ping_file|grep "Connection refused" &>/dev/null) && (echo -e "node ${host}:${port} connect ... \033[31m failed \033[0m")
        if [[ `cat ${ping_file}` == "PONG" ]];then
          echo -e "node ${host}:${port} connect ... \033[32m success \033[0m";
          $kit_path/redis-cli -h $host -p $port -c set test "${ld}" &>$set_file
          $kit_path/redis-cli -h $host -p $port -c get test &>$get_file
          if [[ `cat $set_file` == 'OK' ]] && [[ `cat $get_file` == "${ld}" ]]; then
            echo -e "node ${host}:${port} write data ... \033[32m success \033[0m";
            $kit_path/redis-cli -h $host -p $port -c cluster info &>$info_file
            grep "cluster_state:ok" $info_file &>/dev/null
            if [ $? -ne 0 ]; then
                echo -e "check cluster status ... \033[31mno\033[0m"
                echo -e "\033[0;36m --- show redis cluster nodes --- \033[0m"
                $kit_path/redis-cli -h $host -p $port -c cluster nodes
                cluster_stat=no
                exit 1
            fi
          else
            echo -e "node ${host}:${port} write data ... \033[31m failed \033[0m"
          fi
        fi
      fi
    else
      echo -e "\033[31m Unable to ping $host \033[0m"
    fi
  done
  if [[ "$cluster_stat" == "ok" ]];then
    echo -e "check cluster status ... \033[32mok\033[0m"
  fi
}

