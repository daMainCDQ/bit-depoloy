#!/bin/bash

kafka_check() {
  host=$1
  port=$2
  echo -e "\n\033[0;36m --- Check kafka --- \033[0m\n"
  if ping -c 1 $host &>/dev/null
  then
    # put一个kafka数据
    kafka_topic=fenhuad
    kafka_value=`date +"%Y-%m-%d %H:%M:%S"`
    #chmod +x $kit_path/kafka-producer $kit_path/kafka-consumer
    producer_respose=`$kit_path/kafka-producer $host $port ${kafka_topic} "${kafka_value}"`
    if [[ "${producer_respose}" == "OK" ]]; then
      echo -e "generate kafka topic and put data ... \033[32msuccess\033[0m"
    else
      echo -e "generate kafka topic and put data ... \033[31mfailed\033[0m"
    fi
#    sleep 1
    consumer_response=`$kit_path/kafka-consumer $host $port ${kafka_topic}`
    if [[ "${consumer_response}" == "${kafka_value}" ]];then
      echo -e "consumer topic ... \033[32msuccess\033[0m"
    else
      echo -e "consumer topic ... \033[31mfailed\033[0m"
    fi
  else
    echo -e "\033[31m Unable to ping $host \033[0m"
  fi
}
