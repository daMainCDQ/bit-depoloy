#!/bin/bash

minio_check() {
  host=$1
  port=$2
  access_key=$3
  secret_key=$4
  echo -e "\n\033[0;36m --- Check minio --- \033[0m\n"
  if ping -c 1 $host &>/dev/null
  then
    minio_file=/tmp/minio_test
    bucket_file=/tmp/bucket_result
    $kit_path/miniomc config host add minio http://${host}:${port} ${access_key} ${secret_key} S3v4 &>$minio_file
    grep "successfully" $minio_file &>/dev/null && echo -e "add minio config in mc ... \033[32msuccess\033[0m" ||
    (echo -e "add minio config in mc ... \033[31mfailed, please check the minio port or secret_key\033[0m" && exit 1)
    $kit_path/miniomc ls minio &>$bucket_file
    if [ $? -eq 0 ];then
      grep "migration" $bucket_file &>/dev/null && echo -e "check minio migration ... \033[32mok\033[0m" ||
      echo -e "check minio migration ... \033[31mno\033[0m"
      echo "show bucket's number: $(cat $bucket_file |wc -l)"
    else
      echo -e "Get minio bucket ... \033[31mfailed\033[0m"
    fi
  else
    echo -e "\033[31m Unable to ping $host \033[0m"
    exit 1
  fi
}
