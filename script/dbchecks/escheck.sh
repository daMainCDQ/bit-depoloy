#!/bin/bash

es_check() {
  host=$1
  port=$2
  auth=$3
  type=$4
  echo -e "\n\033[0;36m --- Check elasticsearch --- \033[0m"
  if ping -c 1 $host &>/dev/null
  then
    stat_file=/tmp/stat_result
    migrate_file=/tmp/migrate_result
    cluster_file=/tmp/cluster_result
    curl -s http://${auth}@${host}:${port}/_cat/health?h=status &> $stat_file
    grep "Connection refused" $stat_file &>/dev/null && (echo -e "\033[31m Unable connect to es ${host}:${port} \033[0m" && exit 1)
    grep "failed to authenticate" $stat_file &>/dev/null && (echo -e "\033[31m Auth failed \033[0m" && exit 1)
    result=`cat ${stat_file}`
    if [ "$result" == "yellow" ]; then
      echo -e "es status check ... \033[33myellow\033[0m"
    elif [ "$result" == "green" ]; then
      echo -e "es status check ... \033[32mgreen\033[0m"
    else
      echo -e "es status check ... \033[31mred\033[0m"
    fi
    if [ ! -z $type ] && [ "$type" == "cluster" ]; then
      curl -s http://${auth}@${host}:${port}/_cluster/health?pretty=true &>$cluster_file
      echo -e "es cluster info:\n$(cat $cluster_file)"
    fi
    curl -s http://${auth}@${host}:${port}/_cat/indices?v|grep migration &> $migrate_file
    (grep "migration" $migrate_file &>/dev/null) &&
    echo -e "check es migration result ... \033[32msuccess\033[0m" ||
    (echo -e "\033[31m es no migration \033[0m")
    curl http://${auth}@${host}:${port}/_snapshot/emotibot/_all?pretty &>/dev/null &&
    echo -e "es backup ... \033[32mstarted\033[0m" || echo -e "es backup ... \033[33mclosing\033[0m"
  else
    echo -e "\033[31m Unable to ping $host \033[0m"
    exit 1
  fi
}
