#!/bin/bash
gen_yaml () {
  # 接受参数 [ 目录 1.yaml 2.yaml 3.yaml ...... ] 并在目录下生成与目录同名的隐藏文件，用于启动该项目
  # 如果项目目录下存在utils.sh脚本，则由他自行管理吧，我们只管用
  local dir=$1
  shift 1
  if [[ -f $dir/.${dir}.yaml ]]
  then
    rm -f $dir/.${dir}.yaml
  fi
  local yamlfile=()
  local yamlnum=0
  for y in $@
  do
    yamlfile[$yamlnum]="-f ${dir}/$y"
    let yamlnum++ || true
  done
  #echo ${yamlfile[@]}
  #echo docker-compose ${yamlfile[@]} config > $dir/.${dir}.yaml
  docker-compose ${yamlfile[@]} config > $dir/.${dir}.yaml
}

function refresh_env() {
    set -o allexport
    source dev.env
    netdev=`ip route | grep default | awk '{if(match($0,"'"dev ([^; ]*)"'",a))print a[1]}' || true`
    DOMAIN_URL=$(ip addr show dev ${netdev} | grep -v "/32"| awk -F "/" '{print $1}' | grep -o -E "[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+" || true)
    echo DOMAIN_URL=$DOMAIN_URL > /tmp/.dev.env
    source /tmp/.dev.env
    rm -f /tmp/.dev.env
    # cluster env
    if [[ ! ${DB_HOST_1} == ${DB_HOST_2} ]]
      then
        found=$(ip a | grep "$DB_HOST_1" )
        if [[ $? -eq 0 ]]
          then
            echo "
            DB_HOST_ID=1
            DB_HOST_SELF=$DB_HOST_1
            DB_MYSQL_FIRST=yes
            DB_CONSUL_CMD=-bootstrap-expect=1
            DB_MYSQL_EXERA_HOSTS=mysqlmgr1
            HA_PRORITY=150
            HA_KEEPALIVE_TYPE=MASTER
            REDIS_CLUSTER_HOSTS=''
            REDIS_INIT=no
            ZOO_SERVERS='server.1=0.0.0.0:2888:3888 server.2=${DB_HOST_2}:2888:3888 server.3=${DB_HOST_3}:2888:3888'" > /tmp/.db.dev.env
            source /tmp/.db.dev.env
            rm -f  /tmp/.db.dev.env
        fi
        found=$(ip a | grep "$DB_HOST_2")
        if [[ $? -eq 0 ]]
          then
            echo "
            DB_HOST_ID=2
            DB_HOST_SELF=$DB_HOST_2
            DB_MYSQL_FIRST=no
            DB_CONSUL_CMD='-join ${DB_HOST_1}'
            DB_MYSQL_EXERA_HOSTS=mysqlmgr2
            HA_PRORITY=140
            HA_KEEPALIVE_TYPE=SLAVE 
            REDIS_CLUSTER_HOSTS=''
            REDIS_INIT=no
            ZOO_SERVERS='server.1=${DB_HOST_1}:2888:3888 server.2=0.0.0.0:2888:3888 server.3=${DB_HOST_3}:2888:3888'" > /tmp/.db.dev.env
            source /tmp/.db.dev.env
            rm -f  /tmp/.db.dev.env
        fi
        found=$(ip a | grep $DB_HOST_3 )
        if [[ $? -eq 0 ]]
          then
            redis_master_port=`grep -A 3 "redis:" infra/infra-distributed.yaml |awk -F= '/REDIS_CLUSTER_PORT/{print $2}'`
            redis_slave_port=`grep -A 5 "redis_slave:" infra/infra-distributed.yaml |awk -F= '/REDIS_CLUSTER_PORT/{print $2}'`
            echo "
            DB_HOST_ID=3
            DB_HOST_SELF=$DB_HOST_3
            DB_MYSQL_FIRST=no
            DB_CONSUL_CMD='-join $DB_HOST_1'
            DB_MYSQL_EXERA_HOSTS=mysqlmgr3
            HA_PRORITY=130 
            HA_KEEPALIVE_TYPE=SLAVE 
            REDIS_CLUSTER_HOSTS='${DB_HOST_1}:${redis_master_port},${DB_HOST_1}:${redis_slave_port},${DB_HOST_2}:${redis_master_port},${DB_HOST_2}:${redis_slave_port},${DB_HOST_3}:${redis_master_port},${DB_HOST_3}:${redis_slave_port}'
            REDIS_INIT=yes
            ZOO_SERVERS='server.1=${DB_HOST_1}:2888:3888 server.2=${DB_HOST_2}:2888:3888 server.3=0.0.0.0:2888:3888'" > /tmp/.db.dev.env
            source /tmp/.db.dev.env
            rm -f  /tmp/.db.dev.env
        fi
    fi
    export MINIO_DATA_SEQ_1="$(( (${DB_HOST_ID} - 1) * 4 + 1 ))"
    export MINIO_DATA_SEQ_2="$(( (${DB_HOST_ID} - 1) * 4 + 2 ))"
    export MINIO_DATA_SEQ_3="$(( (${DB_HOST_ID} - 1) * 4 + 3 ))"
    export MINIO_DATA_SEQ_4="$(( (${DB_HOST_ID} - 1) * 4 + 4 ))"

    source emotibot/1-bf/dev.env
    set +o allexport

    export HOSTNAME=${HOSTNAME}

    if [[ ${#MONGO_USER} > 0 ]] && [[ ${#MONGO_PASS} > 0 ]] && [[ ${#MONGO_AUTH_DB} > 0 ]]; then
        export MONGO_URI=mongodb://${MONGO_USER}:${MONGO_PASS}@${MONGO_HOST}:${MONGO_PORT}/${MONGO_AUTH_DB}
    else
        export MONGO_URI=mongodb://${MONGO_HOST}:${MONGO_PORT}
    fi

    if [[ ${#DOMAIN_URL} == 0 ]]
      then
      echo "[WARNING] Failed to get 'DOMAIN_URL', please set this env in dev.env"
    fi
}

