#!/bin/bash
# restart stop rm ps
upkeep (){
  upkeepcmd=$1
  shift 1
  upkeeplist=$@
   if [[ "$upkeeplist" == "0" ]]
     then
       local prolist=(`ls emotibot | sort -g`)
       local upkeeplist=""
       for i in ${prolist[@]}
         do
           i=${i%-*}
           upkeeplist+="${i},"
         done
   fi
  upkeeplist=${upkeeplist//[,\\\/\- +_]/ }
  upkeepcmd=${upkeepcmd//status/ps}
  upkeepcmd=${upkeepcmd//stats/ps}
  upkeepcmd=${upkeepcmd//stat/ps}
  # 如果是DB操作
  if [[ "$upkeeplist" == infra ]]
  then
    if $ONLY_DEPLOY_BASE
      then
        docker-compose -f infra/infra.yaml $upkeepcmd
        exit 0
    else
        docker-compose -f infra/infra.yaml -f infra/infra-distributed.yaml $upkeepcmd
        exit 0
    fi
  fi
  for i in ${upkeeplist}
    do
      upkeeppath=`find emotibot/  -type d -name ${i%.*}-*`
      upkeepfile="${upkeeppath}/.module.yaml"
      upkeepswarm=${upkeeppath#*/}
      if $ONLY_DEPLOY_BASE
        then
          echo -e "\033[33m          project $upkeepfile  \033[0m"
          docker-compose -f $upkeepfile $upkeepcmd
      else
        echo -e "\033[33m          project $upkeepfile  \033[0m"
          if [[ "stop,rm" =~ $upkeepcmd ]]
            then
              docker stack rm $upkeepswarm 
          elif [[ "ps" =~ $upkeepcmd ]]
            then
              docker service ls 
          elif [[ "restart" =~ $upkeepcmd ]]
            then
              for rs in $@ ; do docker service update --force --update-parallelism 1 --update-delay 10s --update-monitor 30s $rs ; done
              exit 0
          fi
      fi
  done
  exit 0
}
upkeep_manager (){
if [[ ! ${#@} -eq 0 ]] && [[ "status,stats,stat,ps,rm,restart,stop" =~ "$1" ]] && [[ -n $2 ]] ; then
  upkeep $@
elif [[ ! ${#@} -eq 0 ]] && [[ "status,stats,stat,ps,rm,restart,stop" =~ "$1" ]] && [[ ! -n $2 ]] ; then
  echo -e "\033[33m                                $1 servers\033[0m"
  banner_list
  select_number upkeep $1
elif [[ ! ${#@} -eq 0 ]] && [[ "saveimages" =~ "$1" ]] && [[ -n $2 ]] ; then
  upkeep_image $@
elif [[ ! ${#@} -eq 0 ]] && [[ "loadimages" =~ "$1" ]] ; then
  upkeep_image loadimage $2
elif [[ ! ${#@} -eq 0 ]] && [[ "saveimages" =~ "$1" ]] && [[ ! -n $2 ]] ; then
  echo -e "\033[33m                                save images\033[0m"
  banner_list
  select_number saveimage
elif [[ ! ${#@} -eq 0 ]] && [[ "savecommit" =~ "$1" ]] ; then
  savecommit $@
elif [[ ! ${#@} -eq 0 ]] && [[ "expose" =~ "$1" ]]  ; then
  if [[ -n `echo $2 | grep '^[[:digit:]]*$'` ]] 2>/dev/null  ; then
    swarm_expose_port $2 oneport
  else
    swarm_expose_port $2
  fi
elif [ "$1" == "--help" -o "$1" == "-h" ]  ; then
  help
elif [ "$1" == log -o "$1" == logs ]  ; then
  echo -e "\033[33m                                check log \033[0m"
  echo "[ -------- infra  infra DB log                               --------]"
  banner_list
  select_number checklog
elif [[ "$1" == onlyup ]]  ; then
  onlyup $@
elif [[ "$1" == license ]] ; then
  docker-compose -f infra/infra.yaml up -d license-hardware
  exit 0
elif [[ "$1" == zabbix-server ]] ; then
  if [[ "$2" == up ]] ; then
    docker-compose -f infra/monitor/module.yaml up -d
    exit 0
  elif [[ "$2" == rm ]] ; then
    docker-compose -f infra/monitor/module.yaml down
    exit 0
  fi
elif [ "$1" == "health" ]; then
  case $2 in
    db)  ./script/db_check.sh all ;;
    mysql) ./script/db_check.sh mysql $MYSQL_HOST $MYSQL_USER $MYSQL_PASS $MYSQL_PORT ;;
    redis)
      if [ $REDIS_TYPE == 'cluster' ]; then
        ./script/db_check.sh rediscluster $REDIS_HOST $REDIS_PASS
      else
        ./script/db_check.sh redis $REDIS_HOST $REDIS_PORT $REDIS_PASS
      fi
      ;;
    es) ./script/db_check.sh es $ES_HOST $ES_PORT ${ES_USER}:${ES_PASS} ;;
    consul) ./script/db_check.sh consul $CONSUL_HOST $CONSUL_PORT ;;
    kafka) ./script/db_check.sh kafka $KAFKA_HOST $KAFKA_PORT ;;
    minio) ./script/db_check.sh minio $MINIO_HOST $MINIO_PORT $MINIO_ACCESS_KEY $MINIO_SECRET_KEY ;;
    *) echo -e "\033[31m Usage: ./deploy.sh health [db/mysql/redis/es/consul/kafka/minio]\033[0m" ;;
  esac
  exit 0
fi
}
