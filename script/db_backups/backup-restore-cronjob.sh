#!/bin/bash
WORK_PATH=$(dirname "$0")
# ==================================================================
# 远端rsync ip地址配置 #请运维人员修改
# ==================================================================
rsync_IP=1.1.1.1

# ==================================================================
# 由于数据导入和导出需要分别在 目标服务器 和 源服务器 上操作
# 以下变量backup_restore_mode 有两个可选值： export/import ,作为判断执行不同脚本的开关，#请运维人员根据场景修改
# ==================================================================
backup_restore_mode=export

########以下命令执行 数据导出 并rsync到 远端服务器########
if [ $backup_restore_mode == export ];then
  ${WORK_PATH}/mysql.sh  export
  ${WORK_PATH}/minio.sh  export
  ${WORK_PATH}/es.sh     export
  ${WORK_PATH}/consul.sh export

  ${WORK_PATH}/mysql.sh  rsync ${rsync_IP}
  ${WORK_PATH}/minio.sh  rsync ${rsync_IP}
  ${WORK_PATH}/es.sh     rsync ${rsync_IP}
  ${WORK_PATH}/consul.sh rsync ${rsync_IP}
fi

######## 以下命令执行 数据导入     ########
######## 注意需要到目标服务器上操作 ########

if [ $backup_restore_mode == import ];then
  ${WORK_PATH}/mysql.sh  import
  ${WORK_PATH}/minio.sh  import
  ${WORK_PATH}/es.sh     import
  ${WORK_PATH}/consul.sh import
fi
