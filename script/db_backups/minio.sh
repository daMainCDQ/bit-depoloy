#!/bin/bash
source_host="127.0.0.1"
ssh_user=deployer
time_stamp=$(date +%Y-%m-%d-%H-%M-%S)
date_stamp=$(date +%Y-%m-%d)
time_stamp_last=$(date -d "-3 days" +"%Y%m%d")
CMD_PATH="../before_check"

log_file="/tmp/data_sync.log"
DATE_N=`date "+%Y-%m-%d %H:%M:%S"`

function log_output() {
    echo -en " \033[33m [`date "+%Y-%m-%d %H:%M:%S"`]:${@} \033[0m \n" | tee -a ${log_file}
}

#初始化备份目录
function init_bak_minio() {

  if [[ -d $2 ]]; then
    bak_dir_all=$2
    log_output data export/import path      $bak_dir_all
  else
    read -t 180 -p $'请输入数据备份导出绝对路径，注意该目录空间一定要足够大，如/data : \n' bak_dir_all
    log_output data export/import path      $bak_dir_all
  fi
  if [ ! -d $bak_dir_all/${date_stamp}/minio ]
    then
      bak_dir="$bak_dir_all/${date_stamp}/minio"
      mkdir -p ${bak_dir}
      if [[ ! $? -eq -0 ]]
        then
          log_output " mkdir -p ${bak_dir} error ......  "
          exit 1
      fi
  else
    bak_dir="$bak_dir_all/${date_stamp}/minio"
  fi

}

function minio_export() {

    #minio 配置连接本地
    echo "${CMD_PATH}/miniomc config host add localhost http://${source_host}:9000 24WTCKX23M0MRI6BA72Y 6mxoQMf1Lha2q1H3fp3fjNHEZRJJ7LvbC5k3+CQX S3v4"
    minio_info=`${CMD_PATH}/miniomc config host add localhost http://${source_host}:9000 24WTCKX23M0MRI6BA72Y 6mxoQMf1Lha2q1H3fp3fjNHEZRJJ7LvbC5k3+CQX S3v4`

    if [[ ! $minio_info =~ "successfully" ]] ; then

    log_output "minio 配置本地连接http://${source_host}:9000失败，请检查！"

    fi

    log_output "start to mirror minio bucket ..."

    # 获取minio上所有的桶
    minio_buckets=(`${CMD_PATH}/miniomc ls localhost |awk '{print $NF}'|awk -F\/ '{print $1}'`)

    for abucket in ${minio_buckets[@]}
    do
      #对应服务端bucket，创建本地目录
      mkdir -p ${bak_dir}/$abucket
      # 将桶中的数据镜像（下载）到本地备份目录中
      ${CMD_PATH}/miniomc mirror localhost/$abucket ${bak_dir}/$abucket/
    done

    log_output "mirror minio bucket finished ~"

}

function minio_rsync() {

    IP=$1
    if [[ ! -n $IP ]] ; then
        log_output "请输入rsync远端ip地址"
        exit 1
    fi
    log_output "请在该服务器上配置deployer 用户免密登录${ssh_user}@${IP} ，否则以下要手动输入deployer用户密码 "
    ssh ${ssh_user}@${IP} mkdir -p ${bak_dir}
    if [[ ! $? -eq -0 ]]
      then
        log_output " 服务器 ${IP} mkdir -p ${bak_dir} error ......  "
        exit 1
    fi
    log_output "开始 rsync 本机 ${bak_dir}目录下的 minio bucket 到 ${ssh_user}@${IP}:${bak_dir} 目录 ..."
    echo "rsync -rvz --delete ${bak_dir}/ ${ssh_user}@${IP}:${bak_dir}"
    eval rsync -rvz --delete ${bak_dir}/ ${ssh_user}@${IP}:${bak_dir}
    if [ $? -eq 0 ];then
      log_output "完成 rsync 本机 ${bak_dir}目录下的 minio bucket 到 ${ssh_user}@${IP}:${bak_dir} 目录 "
    else
      log_output "rsync 本机 ${bak_dir}目录下的 minio bucket 到 ${ssh_user}@${IP}:${bak_dir} 目录失败！请检查"
      exit 1
    fi

}

function minio_import() {

    #minio 配置连接本地
    echo "${CMD_PATH}/miniomc config host add localhost http://${source_host}:9000 24WTCKX23M0MRI6BA72Y 6mxoQMf1Lha2q1H3fp3fjNHEZRJJ7LvbC5k3+CQX S3v4"
    minio_info=`${CMD_PATH}/miniomc config host add localhost http://${source_host}:9000 24WTCKX23M0MRI6BA72Y 6mxoQMf1Lha2q1H3fp3fjNHEZRJJ7LvbC5k3+CQX S3v4`

    if [[ ! $minio_info =~ "successfully" ]] ; then

    log_output "minio 配置本地连接http://${source_host}:9000失败，请检查！"

    fi

    log_output "start to import minio bucket ..."

    # 获取备份目录${bak_dir} 上所有的子目录
    minio_buckets=(`ls ${bak_dir}`)

    for abucket in ${minio_buckets[@]}
    do

      # 将本地备份目录子目录中的目录对应导入 本机minio服务端的 bucket中
      ${CMD_PATH}/miniomc mirror ${bak_dir}/$abucket localhost/$abucket
    done

    log_output "import minio bucket finished ~"

}

init_bak_minio
if [[ $1 == "export" ]]; then
  minio_export
fi
if [[ $1 == "rsync" ]]; then
  IP=$2
  minio_rsync $IP
fi
if [[ $1 == "import" ]]; then
  minio_import
fi