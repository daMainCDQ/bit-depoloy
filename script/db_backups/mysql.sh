#!/bin/bash
source_host="127.0.0.1"
ssh_user=deployer
time_stamp=$(date +%Y-%m-%d-%H-%M-%S)
date_stamp=$(date +%Y-%m-%d)
time_stamp_last=$(date -d "-3 days" +"%Y%m%d")
CMD_PATH="../before_check"

# 一般在从节点上执行mysqldump操作，因为mysqldump会锁表
cont_id=$(docker ps -a | grep mysql | awk '{print $1}')
cont_cmd="docker exec -i ${cont_id}"
mysql_auth="-uroot -ppassword"

log_file="/tmp/data_sync.log"
DATE_N=`date "+%Y-%m-%d %H:%M:%S"`

function log_output() {
    echo -en " \033[33m [`date "+%Y-%m-%d %H:%M:%S"`]:${@} \033[0m \n" | tee -a ${log_file}
}

#初始化备份目录
function init_bak_mysql() {

  if [[ -d $2 ]]; then
    bak_dir_all=$2
    log_output data export/import path      $bak_dir_all
  else
    read -t 180 -p $'请输入数据备份导出绝对路径，注意该目录空间一定要足够大，如/data : \n' bak_dir_all
    log_output data export/import path      $bak_dir_all
  fi
  if [ ! -d $bak_dir_all/${date_stamp}/mysqldump ]
    then
      bak_dir="$bak_dir_all/${date_stamp}/mysqldump"
      mkdir -p ${bak_dir}
      if [[ ! $? -eq -0 ]]
        then
          log_output " mkdir -p ${bak_dir} error ......  "
          exit 1
      fi
  else
    bak_dir="$bak_dir_all/${date_stamp}/mysqldump"
  fi

}

function mysql_export() {

  log_output "mysql   export start..."

  echo "${cont_cmd} mysqldump -h${source_host} ${mysql_auth} -P3306 --triggers --routines --events --set-gtid-purged=OFF  --all-databases > ${bak_dir}/all-db-bfop-${time_stamp}_one_shot.sql"

  ${cont_cmd} mysqldump -h${source_host} ${mysql_auth} -P3306 --triggers --routines --events --set-gtid-purged=OFF  --all-databases > ${bak_dir}/all-db-bfop-${time_stamp}_one_shot.sql

  if [ $? -eq 0 ];then
      log_output "mysql   export complete"
  else
      log_output "mysql  export failure "
      exit 1
  fi

}

function mysql_rsync() {

    IP=$1
    if [[ ! -n $IP ]] ; then
        log_output "请输入rsync远端ip地址"
        exit 1
    fi
    log_output "请在该服务器上配置deployer 用户免密登录${ssh_user}@${IP} ，否则以下要手动输入deployer用户密码 "
    ssh ${ssh_user}@${IP} mkdir -p ${bak_dir}
    if [[ ! $? -eq -0 ]]
      then
        log_output " 服务器 ${IP} mkdir -p ${bak_dir} error ......  "
        exit 1
    fi
    log_output "开始 rsync 本机 ${bak_dir}目录下的 mysqldump 数据 到 ${ssh_user}@${IP}:${bak_dir} 目录 ..."
    echo "rsync -rvz --delete ${bak_dir}/ ${ssh_user}@${IP}:${bak_dir}"
    eval rsync -rvz --delete ${bak_dir}/ ${ssh_user}@${IP}:${bak_dir}
    if [ $? -eq 0 ];then
      log_output "完成 rsync 本机 ${bak_dir}目录下的 mysqldump 数据 到 ${ssh_user}@${IP}:${bak_dir} 目录 "
    else
      log_output "rsync 本机 ${bak_dir}目录下的 mysqldump 数据 到 ${ssh_user}@${IP}:${bak_dir} 目录失败！请检查"
      exit 1
    fi

}

function mysql_import() {

    MYSQL_DUMP=`ls ${bak_dir} | egrep -o "all-db-bfop-[0-9-]{1,20}_one_shot.sql" | tail -1`
    echo "${CMD_PATH}/mysql -h${source_host} ${mysql_auth} -P3306 < ${bak_dir}/${MYSQL_DUMP}"
    ${CMD_PATH}/mysql -h${source_host} ${mysql_auth} -P3306 < ${bak_dir}/${MYSQL_DUMP}

    if [ $? -eq 0 ];then
        log_output "mysql import complete"
    else
        log_output "mysql import failure please check "
        exit 1
    fi
}

init_bak_mysql
if [[ $1 == "export" ]]; then
  mysql_export
fi
if [[ $1 == "rsync" ]]; then
  IP=$2
  mysql_rsync $IP
fi
if [[ $1 == "import" ]]; then
  mysql_import
fi