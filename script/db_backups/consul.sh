#!/bin/bash
source_host="127.0.0.1"
ssh_user=deployer
time_stamp=$(date +%Y-%m-%d-%H-%M-%S)
date_stamp=$(date +%Y-%m-%d)
time_stamp_last=$(date -d "-3 days" +"%Y%m%d")
CMD_PATH="../before_check"


cont_id=$(docker ps -a | grep consul | awk '{print $1}')
cont_cmd="docker exec -i ${cont_id}"

log_file="/tmp/data_sync.log"
DATE_N=`date "+%Y-%m-%d %H:%M:%S"`

function log_output() {
    echo -en " \033[33m [`date "+%Y-%m-%d %H:%M:%S"`]:${@} \033[0m \n" | tee -a ${log_file}
}

#初始化备份目录
function init_bak_consul() {

  if [[ -d $2 ]]; then
    bak_dir_all=$2
    log_output data export/import path      $bak_dir_all
  else
    read -t 180 -p $'请输入数据备份导出绝对路径，注意该目录空间一定要足够大，如/data : \n' bak_dir_all
    log_output data export/import path      $bak_dir_all
  fi
  if [ ! -d $bak_dir_all/${date_stamp}/consul ]
    then
      bak_dir="$bak_dir_all/${date_stamp}/consul"
      mkdir -p ${bak_dir}
      if [[ ! $? -eq -0 ]]
        then
          log_output " mkdir -p ${bak_dir} error ......  "
          exit 1
      fi
  else
    bak_dir="$bak_dir_all/${date_stamp}/consul"
  fi

}

function consul_export() {

    #consul 检查是否连接正常
    consul_info=`curl -s http://${source_host}:8500/v1/status/leader`

    if [[ ! -n $consul_info ]] ; then
      echo "curl -s http://${source_host}:8500/v1/status/leader"
      log_output "consul 连接异常，请检查！"
      exit 1
    fi

    log_output "consul   export start..."
    echo "curl -s http://${source_host}:8500/v1/snapshot?dc=dc1 -o ${bak_dir}/consul_snapshot_${time_stamp}_one_shot.tgz"
    consul_snapshot=`curl -s http://${source_host}:8500/v1/snapshot?dc=dc1 -o ${bak_dir}/consul_snapshot_${time_stamp}_one_shot.tgz`

    log_output "本地 consul snapshot 快照完成"

}
function consul_rsync() {

    IP=$1
    if [[ ! -n $IP ]] ; then
        log_output "请输入rsync远端ip地址"
        exit 1
    fi
    log_output "请在该服务器上配置deployer 用户免密登录${ssh_user}@${IP} ，否则以下要手动输入deployer用户密码 "
    ssh ${ssh_user}@${IP} mkdir -p ${bak_dir}
    if [[ ! $? -eq -0 ]]
      then
        log_output " 服务器 ${IP} mkdir -p ${bak_dir} error ......  "
        exit 1
    fi
    log_output "开始 rsync 本机 ${bak_dir}目录下的 consul 数据 到 ${ssh_user}@${IP}:${bak_dir} 目录 ..."
    echo "rsync -rvz --delete ${bak_dir}/ ${ssh_user}@${IP}:${bak_dir}"
    eval rsync -rvz --delete ${bak_dir}/ ${ssh_user}@${IP}:${bak_dir}
    if [ $? -eq 0 ];then
      log_output "完成 rsync 本机 ${bak_dir}目录下的 consul 数据 到 ${ssh_user}@${IP}:${bak_dir} 目录 "
    else
      log_output "rsync 本机 ${bak_dir}目录下的 consul 数据 到 ${ssh_user}@${IP}:${bak_dir} 目录失败！请检查"
      exit 1
    fi

}

function consul_import() {

    #consul 检查是否连接正常
    consul_info=`curl -s http://${source_host}:8500/v1/status/leader`

    if [[ ! -n $consul_info ]] ; then
      echo "curl -s http://${source_host}:8500/v1/status/leader"
      log_output "consul 连接异常，请检查！"
      exit 1
    fi

    CONSUL_SNAPSHOT=`ls ${bak_dir} | egrep -o "consul_snapshot_[0-9-]{1,20}_one_shot.tgz" | tail -1`
    echo "curl -s --request PUT --data-binary @${bak_dir}/${CONSUL_SNAPSHOT} http://${source_host}:8500/v1/snapshot"
    consul_snapshot=`curl -s --request PUT --data-binary @${bak_dir}/${CONSUL_SNAPSHOT} http://${source_host}:8500/v1/snapshot`

    log_output "本地 consul snapshot 快照导入完成"
}

init_bak_consul
if [[ $1 == "export" ]]; then
  consul_export
fi
if [[ $1 == "rsync" ]]; then
  IP=$2
  consul_rsync $IP
fi
if [[ $1 == "import" ]]; then
  consul_import
fi