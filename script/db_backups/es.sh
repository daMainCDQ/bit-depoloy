#!/bin/bash
source_host="127.0.0.1"
ssh_user=deployer
time_stamp=$(date +%Y-%m-%d-%H-%M-%S)
date_stamp=$(date +%Y-%m-%d)
time_stamp_last=$(date -d "-3 days" +"%Y%m%d")
CMD_PATH="../before_check"

log_file="/tmp/data_sync.log"
DATE_N=`date "+%Y-%m-%d %H:%M:%S"`

function log_output() {
    echo -en " \033[33m [`date "+%Y-%m-%d %H:%M:%S"`]:${@} \033[0m \n" | tee -a ${log_file}
}

#初始化备份目录
function init_bak_es() {

  log_output "开始初始化es 备份目录"
  if [[ -d $2 ]]; then
    bak_dir=$2
    log_output data export/import path      $bak_dir
  else
    read -t 180 -p $'请输入数据备份导出绝对路径，注意该目录空间一定要足够大，\n 默认在/home/deployer/infrastructure/volumes/esforbackups \n 使用默认值请按回车 \n' bak_dir
    bak_dir=${bak_dir:-/home/deployer/infrastructure/volumes/esforbackups}
    log_output es data export/import path      $bak_dir
  fi

  #声明es容器内备份路径/usr/share/elasticsearch/backups
  #这个路径已经挂载出来了，默认在/home/deployer/infrastructure/volumes/esforbackups
  echo "curl -s -X PUT \
  http://elastic:zGGKZUG4D34gdwkyPCtXCd4KU56b2XtA@${source_host}:9200/_snapshot/emotibot \
  -H 'content-type: application/json' \
  -d '{
  "type": "fs",
  "settings": {
  "location": "/usr/share/elasticsearch/backups"
  }
  }'"

  claim_info=`curl -s -X PUT \
  http://elastic:zGGKZUG4D34gdwkyPCtXCd4KU56b2XtA@${source_host}:9200/_snapshot/emotibot \
  -H 'content-type: application/json' \
  -d '{
  "type": "fs",
  "settings": {
  "location": "/usr/share/elasticsearch/backups"
  }
  }'`

  if [[ $claim_info =~ "access_denied_exception" ]] ; then
    log_output " es集群备份目录 ${bak_dir} 没有配置共享存储 或者 es没有访问${bak_dir}的权限，请检查 "
    exit 1
  else
    log_output "初始化es 备份目录完成"
  fi

}

function es_export() {

  log_output "开始进行本地 es站点 快照备份"

  echo "curl -s -X POST \
  "http://elastic:zGGKZUG4D34gdwkyPCtXCd4KU56b2XtA@${source_host}:9200/_snapshot/emotibot/snapshot_${time_stamp}_one_shot?wait_for_completion=true" \
  -H 'content-type: application/json' \
  -d '{
  "include_global_state": true
  }'"

  snapshot_info=`curl -s -X POST \
  "http://elastic:zGGKZUG4D34gdwkyPCtXCd4KU56b2XtA@${source_host}:9200/_snapshot/emotibot/snapshot_${time_stamp}_one_shot?wait_for_completion=true" \
  -H 'content-type: application/json' \
  -d '{
  "include_global_state": true
  }'`

  if [[ $snapshot_info =~ "\"failed\":0" ]] ; then

    log_output "本地 es站点 快照 emotibot/snapshot_${time_stamp}_one_shot 备份完成"

  else

    log_output "本地 es站点 快照 emotibot/snapshot_${time_stamp}_one_shot 备份部分失败，请检查"
    exit 1

  fi


}
function es_rsync() {

    IP=$1
    if [[ ! -n $IP ]] ; then
        log_output "请输入rsync远端ip地址"
        exit 1
    fi
    log_output "请在该服务器上配置deployer 用户免密登录${ssh_user}@${IP} ，否则以下要手动输入deployer用户密码 "

    log_output "开始 rsync 本机 ${bak_dir}目录下的 es 快照数据 到 ${ssh_user}@${IP}:${bak_dir} 目录 ..."
    echo "rsync -rvz --delete ${bak_dir}/ ${ssh_user}@${IP}:${bak_dir}"
    eval rsync -rvz --delete ${bak_dir}/ ${ssh_user}@${IP}:${bak_dir}
    if [ $? -eq 0 ];then
      log_output "完成 rsync 本机 ${bak_dir}目录下的 es 快照数据 到 ${ssh_user}@${IP}:${bak_dir} 目录 "
    else
      log_output "rsync 本机 ${bak_dir}目录下的 es 快照数据 到 ${ssh_user}@${IP}:${bak_dir} 目录失败！请检查"
      exit 1
    fi

}
function es_import() {


  echo "curl -s -XGET http://elastic:zGGKZUG4D34gdwkyPCtXCd4KU56b2XtA@${source_host}:9200/_snapshot/emotibot/snapshot_${date_stamp}*_one_shot | egrep -o 'snapshot_[0-9-]{1,20}_one_shot' | tail -1"
  es_snapshot=`curl -s -XGET http://elastic:zGGKZUG4D34gdwkyPCtXCd4KU56b2XtA@${source_host}:9200/_snapshot/emotibot/snapshot_${date_stamp}*_one_shot | egrep -o 'snapshot_[0-9-]{1,20}_one_shot' | tail -1`
  if [[ $es_snapshot =~ "snapshot_" ]] ; then

    log_output "获取到了本地 es站点 备份目录的今天最新的快照${es_snapshot} "

  else

    log_output "获取本地 es站点 备份目录的今天最新的快照失败，请检查${bak_dir}目录是否有数据"
    exit 1

  fi

  log_output "es导入恢复数据前，开始关闭本地 es站点 全部index"
  echo "curl -s -X POST http://elastic:zGGKZUG4D34gdwkyPCtXCd4KU56b2XtA@${source_host}:9200/_all/_close"
  es_info=`curl -s -X POST http://elastic:zGGKZUG4D34gdwkyPCtXCd4KU56b2XtA@${source_host}:9200/_all/_close`
  if [[ $es_info =~ "\"acknowledged\":true" ]] ; then

    log_output "本地 es站点 index 全部关闭完成"

  else

    log_output "本地 es站点 index 关闭失败，请检查"
    exit 1

  fi

  log_output "开始恢复本地 es站点 快照 emotibot/${es_snapshot} "
  echo "curl -X POST \
  "http://elastic:zGGKZUG4D34gdwkyPCtXCd4KU56b2XtA@${source_host}:9200/_snapshot/emotibot/${es_snapshot}/_restore?wait_for_completion=true" \
  -H 'content-type: application/json' \
  -d '{
  "include_global_state": true
  }'"
  snapshot_info=`curl -X POST \
  "http://elastic:zGGKZUG4D34gdwkyPCtXCd4KU56b2XtA@${source_host}:9200/_snapshot/emotibot/${es_snapshot}/_restore?wait_for_completion=true" \
  -H 'content-type: application/json' \
  -d '{
  "include_global_state": true
  }'`

  if [[ $snapshot_info =~ "\"failed\":0" ]] ; then

    log_output "本地 es站点 快照 emotibot/${es_snapshot} 恢复完成"

  else

    log_output "本地 es站点 快照 emotibot/${es_snapshot} 恢复部分失败，请检查"
    exit 1

  fi
  log_output "开始打开本地 es站点 全部index"
  echo "curl -s -X POST http://elastic:zGGKZUG4D34gdwkyPCtXCd4KU56b2XtA@${source_host}:9200/_all/_open"
  es_info=`curl -s -X POST http://elastic:zGGKZUG4D34gdwkyPCtXCd4KU56b2XtA@${source_host}:9200/_all/_open`
  if [[ $es_info =~ "\"acknowledged\":true" ]] ; then

    log_output "本地 es站点 index 全部打开完成"

  else

    log_output "本地 es站点 index 打开失败，请检查"
    exit 1

  fi
}

init_bak_es
if [[ $1 == "export" ]]; then
  es_export
fi
if [[ $1 == "rsync" ]]; then
  IP=$2
  es_rsync $IP
fi
if [[ $1 == "import" ]]; then
  es_import
fi
