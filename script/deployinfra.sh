#!/bin/bash

# 生成image list
db_runcheck_list () {
  #grep -e image: $@ | grep -v '^#' |awk -F 'image:' '{print $2}'| uniq
  grep "container_name" $@| awk -F ":" '{print $NF}'
}
db_health_exit (){
  echo -e "\033[31m $@ \033[0m "
  exit 1
}
vip_deploy (){
  if [ ${VIP_DEPLOY} == true ]; then
    docker-compose -f infra/haproxy/keepalived.yaml config > infra/.keepalived.yaml
    docker-compose -f infra/.keepalived.yaml up -d
  fi
}
db_health_check () {
  if [[ `ip a` =~  $DB_HOST_1 ]] || $ONLY_DEPLOY_BASE
  then
     sleeptime=4
  elif [[ `ip a` =~  $DB_HOST_2 ]]
    then
    sleeptime=10
  else
    sleeptime=16
  fi  
  for i in  `seq 1 $sleeptime | sed -n '1h;1!G;1!h;$p'`
    do
      printf "wait DB health_check : [%-2ss]  %s\r"  $i
      sleep 1
  done
  source dev.env
  # 检查DB 是否正常
  # mysql master slave
  if ! $ONLY_DEPLOY_BASE
  then
    if [[ ! `ip a` =~  $DB_HOST_1 ]]
      then
        myhost=`docker ps | grep mysqlmgr | awk '{print $NF}'`
        if [[ -z $myhost ]]
        then
           db_health_exit "mysql mgr not exist ...... "
        fi

        Slave_IO_stat=`docker exec -i  $myhost mysql -u$MYSQL_USER -p$MYSQL_PASS -e 'show slave status\G' 2>&1 | grep -e "Slave_IO_Running: Yes" | wc -l`
        Slave_SQL_stat=`docker exec -i $myhost mysql -u$MYSQL_USER -p$MYSQL_PASS -e 'show slave status\G' 2>&1 | grep -e "Slave_SQL_Running: Yes" | wc -l`

        if [[ $Slave_IO_stat -eq 1 ]] && [[ $Slave_SQL_stat -eq 1 ]]
        then
           echo 0 > /dev/null 2>&1 &
        else
           db_health_exit "mysql slave  unhealth ...... "
        fi
    fi
  else
    docker ps |grep mysqlmgr | grep -q unhealthy && db_health_exit "mysql unhealth ......"
  fi
  # es
  curl -u $ES_USER:$ES_PASS -s -m 2 "$DB_HOST_1:$ES_PORT/_cluster/health\?pretty" | grep -q red &&  db_health_exit "ES cluster red"
  # consul
  myhost=`docker ps | grep consul | awk '{print $NF}'`
  if [[ -z $myhost ]]
  then
     db_health_exit "consul not exist ...... "
  fi
  docker exec -i $myhost consul members | grep -q alive || db_health_exit "consul unhealth ......"
  #curl http://127.0.0.1:8500/v1/status/leader
  # redis single
  rediscli=./script/before_check/redis-cli
  if [[ $REDIS_HOST == "${DEFAULT_IP}" ]]; then
    (docker ps |grep " redis" &>>/dev/null) || (echo -e "\033[33m redis not installed ! \033[0m" )
  fi
  if [[ $REDIS_TYPE == "single" ]]; then
    if [[ ! -z $REDIS_PASS ]]; then
      redischeck=`$rediscli -h $REDIS_HOST -p $REDIS_PORT -a $REDIS_PASS set name fenhuad 2>&1`
    else
      redischeck=`$rediscli -h $REDIS_HOST -p $REDIS_PORT set name fenhuad 2>&1`
    fi
  elif [[ $REDIS_TYPE == "cluster" ]]; then
    if [[ ! -z $REDIS_PASS ]]; then
      redischeck=`$rediscli -c -h $REDIS_HOST -p $REDIS_PORT -a $REDIS_PASS set name fenhuad 2>&1`
    else
      redischeck=`$rediscli -c -h $REDIS_HOST -p $REDIS_PORT set name fenhuad 2>&1`
    fi
  else
    db_health_check "unknow REDIS_TYPE"
  fi
  if [[ ! $redischeck =~ "OK" ]] ; then
    db_health_exit " redis unhealthy ......"
  fi

  # zookeeper
  if ! $ONLY_DEPLOY_BASE
  then
    if [[ `ip a` =~  $DB_HOST_3 ]]
    then
      myhost=`docker ps | grep zookeeper | awk '{print $NF}'`
      if [[ -z $myhost ]]
      then
         db_health_exit "zookeeper not exist ...... "
      fi
      zk_check=`docker exec -i $myhost /opt/zookeeper/bin/zkServer.sh status 2>&1`
      if [[ ! $zk_check =~ "follower" ]] && [[ ! $zk_check =~ "leader" ]] ; then
        db_health_exit " zookeeper cluster unhealthy ......"
      fi
    fi
  else
    myhost=`docker ps | grep zookeeper | awk '{print $NF}'`
    if [[ -z $myhost ]]
    then
       db_health_exit "zookeeper not exist ...... "
    fi
    zk_check=`docker exec -i $myhost /opt/zookeeper/bin/zkServer.sh status 2>&1`
    if [[ ! $zk_check =~ "standalone" ]] ; then
      db_health_exit " zookeeper single  unhealthy ......"
    fi
  fi
  # kafka
  if ! $ONLY_DEPLOY_BASE
  then
    if [[ `ip a` =~  $DB_HOST_3 ]]
    then
      myhost=`docker ps | grep kafka | awk '{print $NF}'`
      if [[ -z $myhost ]]
      then
         db_health_exit "kafka not exist ...... "
      fi
      kafka_check1=`docker exec -i $myhost kafka-topics.sh --create --zookeeper zookeeper:2181 --replication-factor 3 --partitions 3 --topic bingjunx 2>&1`
      kafka_check2=`docker exec -i $myhost kafka-topics.sh --list --zookeeper zookeeper:2181 2>&1`
      if [[ ! $kafka_check1 =~ "Created" ]] && [[ ! $kafka_check1 =~ "already exists" ]] ; then
        db_health_exit " kafka cluster unhealthy ......"
      fi
      if [[ ! $kafka_check2 =~ "bingjunx" ]] ; then
        db_health_exit " kafka cluster unhealthy ......"
      fi
    fi
  else
    myhost=`docker ps | grep kafka | awk '{print $NF}'`
    if [[ -z $myhost ]]
    then
       db_health_exit "kafka not exist ...... "
    fi
    kafka_check1=`docker exec -i $myhost kafka-topics.sh --create --zookeeper zookeeper:2181 --replication-factor 1 --partitions 1 --topic bingjunx 2>&1`
    kafka_check2=`docker exec -i $myhost kafka-topics.sh --list --zookeeper zookeeper:2181 2>&1`
    if [[ ! $kafka_check1 =~ "Created" ]] && [[ ! $kafka_check1 =~ "already exists" ]] ; then
      db_health_exit " kafka single unhealthy ......"
    fi
    if [[ ! $kafka_check2 =~ "bingjunx" ]] ; then
      db_health_exit " kafka single unhealthy ......"
    fi
  fi
  # minio
  myhost=`docker ps | grep minio | awk '{print $NF}'`
  if [[ -z $myhost ]]
  then
     db_health_exit "minio not exist ...... "
  fi
  docker exec -i $myhost curl -I -s localhost:9000/minio/health/ready | grep -q "200 OK" || db_health_exit " minio unhealthy ......"
}
db_runcheck () {
  source dev.env
  # 客户提供某个数据库的场景,则该数据库在本地不再启动
  skipdb=""
  # 客户提供mysql 
  if [[ $MYSQL_HOST != "${DEFAULT_IP}" ]] 
    then
      skipdb+="--scale mysql=0 "
  fi
  if [[ $ES_HOST != "${DEFAULT_IP}" ]]
    then
      skipdb+="--scale elasticsearch=0 "
  fi
  if [[ $REDIS_HOST != "${DEFAULT_IP}" ]]
    then
      skipdb+="--scale redis=0 "
  fi
  if [[ $KAFKA_HOST != "${DEFAULT_IP}" ]]; then
      skipdb+="--scale kafka=0 "
  fi
  if [[ $MINIO_HOST != "${DEFAULT_IP}" ]]; then
      skipdb+="--scale minio=0 "
  fi
  if [[ $CONSUL_HOST != "${DEFAULT_IP}" ]]; then
      skipdb+="--scale consul=0 "
  fi
  # 当DB分离部署时
  if [[ $DB_SEPARATE == true ]]
  then
    # 本机IP不属于DB_HOSTS中则部署module
    if [[ ! `ip a` =~ " ${DB_HOST_1}/" && ! `ip a` =~ " ${DB_HOST_2}/" && ! `ip a` =~ " ${DB_HOST_3}/" ]]
    then
      # 部署haproxy
      skipinfras=`echo $skipdb|sed -n 's@--scale @@gp'`
      allinfras=(mysql redis elasticsearch kafka minio consul zookeeper)
      for ainfra in ${allinfras[@]}
      do
        if [[ ! "$skipinfras" =~ $ainfra ]]
        then
          if [[ $ainfra == mysql ]];then
            export PROXY_MYSQL_PORT=${MYSQL_PORT}
          elif [[ $ainfra == redis ]]; then
            export PROXY_REDIS_PORT=${REDIS_PORT}
          elif [[ $ainfra == elasticsearch ]]; then
            export PROXY_ES_PORT=${ES_PORT}
          elif [[ $ainfra == kafka ]]; then
            export PROXY_KAFKA_PORT=${KAFKA_PORT}
          elif [[ $ainfra == minio ]]; then
            export PROXY_MINIO_PORT=${MINIO_PORT}
          elif [[ $ainfra == consul ]]; then
            export PROXY_CONSUL_PORT=${CONSUL_PORT}
          elif [[ $ainfra == zookeeper ]]; then
            export PROXY_ZOOKEEPER_PORT=${ZOOKEEPER_PORT}
          fi
        else
          if [[ $ainfra == mysql ]];then
            export PROXY_MYSQL_PORT=1${MYSQL_PORT}
          elif [[ $ainfra == redis ]]; then
            export PROXY_REDIS_PORT=1${REDIS_PORT}
          elif [[ $ainfra == elasticsearch ]]; then
            export PROXY_ES_PORT=1${ES_PORT}
          elif [[ $ainfra == kafka ]]; then
            export PROXY_KAFKA_PORT=1${KAFKA_PORT}
          elif [[ $ainfra == minio ]]; then
            export PROXY_MINIO_PORT=1${MINIO_PORT}
          elif [[ $ainfra == consul ]]; then
            export PROXY_CONSUL_PORT=1${CONSUL_PORT}
          elif [[ $ainfra == zookeeper ]]; then
            export PROXY_ZOOKEEPER_PORT=1${ZOOKEEPER_PORT}
          fi
        fi
        if [[ ${EFK_DEPLOY} == "true" ]];then
          export PROXY_ES_EFK_PORT=${ES_EFK_PORT}
        else
          export PROXY_ES_EFK_PORT=1${ES_EFK_PORT}
        fi
      done
#      docker-compose -f infra/haproxy/infra-haproxy.yaml config > infra/.infra-haproxy.yaml
#      docker-compose -f infra/.infra-haproxy.yaml up -d
      docker stack deploy -c infra/haproxy/infra-haproxy.yaml db_proxy
      return `echo $?`
    fi
  fi
  tmpv=
  skiplist=
  for i in $skipdb ; do tmpv=`echo $i | sed 's/--scale//g'`; skiplist+="${tmpv%=*} " ; done
  skiplist=${skiplist:=guoheyang}
  skiplist=${skiplist//  /|}
  skiplist=${skiplist// /}
  infraDB=true
  # 检查db是否已经安装,或者是否有更新
  totaldclist=`docker ps | awk '{print $NF}'`
  for i in $(db_runcheck_list $@)
  do
    echo $totaldclist | grep -Eo " ${i}" -q
    if [[ ! $? -eq 0 ]] && [[ ! $i =~  ${skiplist:=guoheyang} ]] 
      then
        i=${i##*/}
        echo -e "\033[33m          infrastructure ${i%:*} not installed  \033[0m"
        infraDB=false
        break
    fi
  done
  # 检查是否yaml文件有更新
  if [[ `md5sum infra/.infra.yaml` != `cat infra/.infracheck.yaml` ]] 2>/dev/null
    then
      echo -e "\033[33m          infrastructure update  \033[0m"
      infraDB=false
      md5sum infra/.infra.yaml > infra/.infracheck.yaml
  fi
  # 循环检查每个DB，如果没有装，并且是单机版 , 则，安装单机版db
  if ! $infraDB && $ONLY_DEPLOY_BASE
    then
      echo docker-compose -f infra/.infra.yaml up -d $skipdb
      docker-compose -f infra/.infra.yaml up -d $skipdb
      db_health_check
  # 如果没有安装db，并且是多机版
  elif ! $infraDB && ! $ONLY_DEPLOY_BASE
    then
      echo echo docker-compose -f infra/.infra.yaml up -d
      docker-compose -f infra/.infra.yaml up -d $skipdb
      db_health_check
      # 第一台启动后，等待第二第三台启动，接受手动enter
      if [[ "$DOMAIN_URL" == "$DB_HOST_1" ]]
        then
        echo -e -n  "\033[33mPlase Up $DB_HOST_2 $DB_HOST_3 "
        read -t 600 -p $' DB ,if done , input enter \033[0m\n'
      # 如果是第二第三台，启动后直接退出
      elif [[ "$DOMAIN_URL" == "$DB_HOST_2" ]]
        then
          exit 0
      elif [[ "$DOMAIN_URL" == "$DB_HOST_3" ]]
        then
          exit 0
      fi
      if [[ ! $? -eq 0 ]]
      then
        echo "input timeout"
        exit 1
      fi
  fi
}
# 检查部署单机版还是多机版
run_db () {
  set -o allexport
  source dev.env
  #第一个参数和第二个参数包含"mini"就触发降配启动infra
  if [[ "mini" == "$1" ]] || [[ "mini" == "$2" ]] ; then
    export ES_MEM=3g
    export ZABBIX=false
    export EFK_DEPLOY=false
    echo "true" > .mini.infra.current
  #查看mini方式部署标志位是否为true，如果为true说明当前环境已经降配启动了infra
  elif [[ -f .mini.infra.current ]] && [[ `cat .mini.infra.current` == "true" ]] ; then
    read -t 180 -p $'脚本发现当前以mini/降配内存 方式部署了infra，默认继续使用该方式部署infra,直接敲回车即可，否则请敲 n \n' flag
    if [[ ${flag} == "y" ]] || [[ ${flag} == "Y" ]] || [[ -z ${flag} ]] ; then
      echo -e "  \033[33m  你已选择继续使用 mini/降配内存 方式部署infra \033[0m "
      export ES_MEM=3g
      export ZABBIX=false
      export EFK_DEPLOY=false
    elif [[ ${flag} == "n" ]] || [[ ${flag} == "N" ]] || [[ ${flag} == "no" ]] ; then
      echo -e "  \033[33m  你已选择切换 非(mini/降配内存) 方式部署 方式部署infra \033[0m "
      echo "false" > .mini.infra.current
    fi
  fi
  if [[ ${ZABBIX_SERVER} == "DEFAULT" ]]; then
    export ZABBIX_SERVER=$DB_HOST_1
    if $ONLY_DEPLOY_BASE && [[ $ZABBIX == "deploy" ]] ; then
      #docker-compose -f infra/monitor/agent.yaml -f infra/monitor/agentport.yaml config > infra/monitor/.agent.yaml 2>/dev/null
      docker-compose -f infra/monitor/module.yaml up -d > /dev/null 2>&1 &
      #docker-compose -f infra/monitor/.agent.yaml up -d > /dev/null 2>&1 &
    elif ! $ONLY_DEPLOY_BASE && [[ $ZABBIX == "deploy" ]] && [[ `ip a` =~ "$DB_HOST_1" ]] && [[ "$K8S_DEPLOY" == false ]] ; then
      #docker-compose -f infra/monitor/agent.yaml -f infra/monitor/agentswarm.yaml config > infra/monitor/.agent.yaml 2>/dev/null
      docker-compose -f infra/monitor/module.yaml up -d > /dev/null 2>&1 &
      #docker stack deploy -c infra/monitor/.agent.yaml zbx_agent > /dev/null 2>&1 &
    elif ! $ONLY_DEPLOY_BASE && [[ $ZABBIX == "deploy" ]] && [[ `ip a` =~ "$DB_HOST_1" ]] && [[ "$K8S_DEPLOY" == true ]] ; then
      #docker-compose -f infra/monitor/agent.yaml -f infra/monitor/agentswarm.yaml config > infra/monitor/.agent.yaml 2>/dev/null
      #k8s_monitor_up_before
      docker-compose -f infra/monitor/module.yaml up -d > /dev/null 2>&1 &
      #kompose -f infra/monitor/.agent.yaml convert -o infra/monitor/.agent-k8s.yaml
      #k8s_monitor_convert_post
      #sed -i '/status:/i\        hostNetwork: true' infra/monitor/.agent-k8s.yaml
      #sed -i '/status:/i\        dnsPolicy: ClusterFirstWithHostNet' infra/monitor/.agent-k8s.yaml
      #kubectl convert -f infra/monitor/.agent-k8s.yaml > infra/monitor/.agent-k8s-convert.yaml 2>/dev/null
      #kubectl apply -f infra/monitor/.agent-k8s-convert.yaml -n $NAMESPACE > /dev/null 2>&1 &
    fi
  else
    if $ONLY_DEPLOY_BASE && [[ $ZABBIX == "deploy" ]] ; then
      true
      #docker-compose -f infra/monitor/agent.yaml -f infra/monitor/agentport.yaml config > infra/monitor/.agent.yaml 2>/dev/null
      #docker-compose -f infra/monitor/.agent.yaml up -d > /dev/null 2>&1 &
    #elif ! $ONLY_DEPLOY_BASE && [[ $ZABBIX == "deploy" ]] && [[ `ip a` =~ "$DB_HOST_1" ]] && [[ "$K8S_DEPLOY" == false ]] ; then
      #docker-compose -f infra/monitor/agent.yaml -f infra/monitor/agentswarm.yaml config > infra/monitor/.agent.yaml 2>/dev/null
      #docker stack deploy -c infra/monitor/.agent.yaml zbx_agent > /dev/null 2>&1 &
    elif ! $ONLY_DEPLOY_BASE && [[ $ZABBIX == "deploy" ]] && [[ `ip a` =~ "$DB_HOST_1" ]] && [[ "$K8S_DEPLOY" == true ]] ; then
      true
      #docker-compose -f infra/monitor/agent.yaml -f infra/monitor/agentswarm.yaml config > infra/monitor/.agent.yaml 2>/dev/null
      #k8s_monitor_up_before
      #kompose -f infra/monitor/.agent.yaml convert -o infra/monitor/.agent-k8s.yaml
      #k8s_monitor_convert_post
      #sed -i '/status:/i\        hostNetwork: true' infra/monitor/.agent-k8s.yaml
      #sed -i '/status:/i\        dnsPolicy: ClusterFirstWithHostNet' infra/monitor/.agent-k8s.yaml
      #kubectl convert -f infra/monitor/.agent-k8s.yaml > infra/monitor/.agent-k8s-convert.yaml 2>/dev/null
      #kubectl apply -f infra/monitor/.agent-k8s-convert.yaml -n $NAMESPACE > /dev/null 2>&1 &
    fi
  fi
  set +o allexport
    # 单机
    if $ONLY_DEPLOY_BASE
    then 
      echo -e "\033[33m                                部署单机版\033[0m"
        export ONLY_DEPLOY_BASE=true
        if $EFK_DEPLOY && [[ "$K8S_DEPLOY" == false ]]
        then
          gen_yaml infra infra.yaml port.yaml efk/es-infra.yaml
          docker-compose -f infra/efk/fluentd.yaml -f infra/efk/kibana.yaml -f infra/efk/zipkin.yaml -f infra/efk/efk-expose-port.yaml config > infra/efk/.efk.yaml 2>/dev/null
          docker-compose -f infra/efk/cronjobs.yaml config > infra/efk/.cronjobs.yaml 2>/dev/null
          docker-compose -f infra/efk/.efk.yaml -f infra/efk/.cronjobs.yaml config > infra/efk/.efk-cronjob.yaml 2>/dev/null
          docker-compose -f infra/efk/.efk-cronjob.yaml up -d > /dev/null 2>&1 &
        elif $EFK_DEPLOY && [[ "$K8S_DEPLOY" == true ]]
        then
          #echo -e "\nkubectl apply -f infra/k8s-yamls/efk/"
          k8s_efk_up_before
          kubectl apply -f infra/k8s-yamls/efk/ > /dev/null 2>&1 &
          gen_yaml infra infra.yaml port.yaml
        else
          gen_yaml infra infra.yaml port.yaml
        fi
    # 多机
    else
      echo -e "\033[32m                                部署多机版\033[0m"
        export  ONLY_DEPLOY_BASE=false
        if $EFK_DEPLOY && [[ "$K8S_DEPLOY" == false ]]
        then
          gen_yaml infra infra.yaml infra-distributed.yaml efk/es-infra-distributed.yaml
          docker-compose -f infra/efk/cronjobs.yaml -f infra/efk/cronjobs-swarm.yaml config > infra/efk/.cronjobs.yaml 2>/dev/null
          docker-compose -f infra/efk/fluentd.yaml -f infra/efk/kibana.yaml -f infra/efk/zipkin.yaml -f infra/efk/efk-swarm.yaml config > infra/efk/.efk.yaml 2>/dev/null
          docker stack deploy -c infra/efk/.efk.yaml efk > /dev/null 2>&1 &
          docker stack deploy -c infra/efk/.cronjobs.yaml efk > /dev/null 2>&1 &
        elif $EFK_DEPLOY && [[ "$K8S_DEPLOY" == true ]]
        then
          #echo -e "\nkubectl apply -f infra/k8s-yamls/efk/"
          k8s_efk_up_before
          kubectl apply -f infra/k8s-yamls/efk/  > /dev/null 2>&1 &
          gen_yaml infra infra.yaml infra-distributed.yaml
        else
          gen_yaml infra infra.yaml infra-distributed.yaml
        fi
    fi
    # 部署时间服务rdate
    if ! $ONLY_DEPLOY_BASE
      then
      if [[ `ip a` =~ "$DB_HOST_1" ]]
        then
        docker-compose -f infra/rdate/rdate.yaml up -d > /dev/null 2>&1 &
        else
        time_remote=`script/before_check/rdate -p $DB_HOST_1 2>&1 | awk -F ']' '{print $2}'`
        time_stamp_local=`date +%s`
        if [[ $time_remote == "" ]]
          then
            echo -e "\033[33m    没有获取到 $DB_HOST_1 的时间，请知悉  \033[0m"
          else
            time_stamp_remote=`date -d "$time_remote" +%s`
            time_diff=$((time_stamp_remote-time_stamp_local))
            time_diff=${time_diff#-}
            if [[ $time_diff -ge 2 ]]
              then
                echo -e "\033[33m    本服务器时间与$DB_HOST_1 差值大于2秒，请检查  \033[0m"
                exit 1
            fi
        fi
      fi
    fi
    if [[ "$1" == skip ]] && [[ "$@" =~ infra ]]
      then
        echo -e "\033[32m                                skip infra\033[0m"
    elif [[ "$K8S_DEPLOY_DB" == false ]]
      then
      db_runcheck infra/.infra.yaml
    fi
    # k8s 部署 infra
    if $K8S_DEPLOY_DB
    then
      echo -e "\033[32m                您已选择k8s方式部署 infra DB，此种方式不能上生产，请知悉\033[0m"
      #echo -e "\033[32m                            启动 k8s infra DB\033[0m"
      k8s_infra_convert_post
      sleep 1
      k8s_infra_up
    fi
}

