#!/bin/bash

k8s_module_up (){

    WORK_PATH=$(dirname "$0")
    KOMPOSE=/usr/local/bin/kompose
    KUBECTL=/usr/local/bin/kubectl
    source $WORK_PATH/dev.env

    if [[ "${NFS_PROTOCOL}" == "nfs" ]];then
        K8S_YAML_VOLUME_DIR=emotibot/$1/k8s-yamls/volumes/nfs/
    elif [[ "${NFS_PROTOCOL}" == "glusterfs" ]];then
        K8S_YAML_VOLUME_DIR=emotibot/$1/k8s-yamls/volumes/gfs/
    fi

    for i in `ls $K8S_YAML_VOLUME_DIR | grep template.yaml`
    do
      echo kubectl apply -f $K8S_YAML_VOLUME_DIR/$i -n $NAMESPACE
      kubectl apply -f $K8S_YAML_VOLUME_DIR/$i -n $NAMESPACE
    done

    for i in `ls emotibot/$1/k8s-yamls/modules/ | grep .yaml`
    do
      echo kubectl apply -f emotibot/$1/k8s-yamls/modules/$i -n $NAMESPACE
      kubectl apply -f emotibot/$1/k8s-yamls/modules/$i -n $NAMESPACE
    done

    for i in `ls emotibot/$1/k8s-yamls/nodeports/ | grep .yaml`
    do
      echo kubectl apply -f emotibot/$1/k8s-yamls/nodeports/$i -n $NAMESPACE
      kubectl apply -f emotibot/$1/k8s-yamls/nodeports/$i -n $NAMESPACE
    done

}
k8s_module_remove (){

    WORK_PATH=$(dirname "$0")
    KOMPOSE=/usr/local/bin/kompose
    KUBECTL=/usr/local/bin/kubectl
    source $WORK_PATH/dev.env

    if [[ "${NFS_PROTOCOL}" == "nfs" ]];then
        K8S_YAML_VOLUME_DIR=emotibot/$1/k8s-yamls/volumes/nfs/
    elif [[ "${NFS_PROTOCOL}" == "glusterfs" ]];then
        K8S_YAML_VOLUME_DIR=emotibot/$1/k8s-yamls/volumes/gfs/
    fi

    for i in `ls emotibot/$1/k8s-yamls/modules/ | grep .yaml`
    do
      echo kubectl delete -f emotibot/$1/k8s-yamls/modules/$i -n $NAMESPACE
      kubectl delete -f emotibot/$1/k8s-yamls/modules/$i -n $NAMESPACE
    done

    for i in `ls $K8S_YAML_VOLUME_DIR | grep template.yaml`
    do
      echo kubectl delete -f $K8S_YAML_VOLUME_DIR/$i -n $NAMESPACE
      kubectl delete -f $K8S_YAML_VOLUME_DIR/$i -n $NAMESPACE
    done

    for i in `ls emotibot/$1/k8s-yamls/nodeports/ | grep .yaml`
    do
      echo kubectl delete -f emotibot/$1/k8s-yamls/nodeports/$i -n $NAMESPACE
      kubectl delete -f emotibot/$1/k8s-yamls/nodeports/$i -n $NAMESPACE
    done



}
k8s_module_update (){

    WORK_PATH=$(dirname "$0")
    KOMPOSE=/usr/local/bin/kompose
    KUBECTL=/usr/local/bin/kubectl
    source $WORK_PATH/dev.env


    for i in `ls emotibot/$1/k8s-yamls/modules/ | grep .yaml`
    do
      echo kubectl apply -f emotibot/$1/k8s-yamls/modules/$i -n $NAMESPACE
      kubectl apply -f emotibot/$1/k8s-yamls/modules/$i -n $NAMESPACE
    done

}
k8s_infra_up (){

    WORK_PATH=$(dirname "$0")
    KOMPOSE=/usr/local/bin/kompose
    KUBECTL=/usr/local/bin/kubectl
    source $WORK_PATH/dev.env
    K8S_INFRA_STATEFULSET_DIR=infra/k8s-yamls/infras/statefulsets/

    if [[ "${NFS_PROTOCOL}" == "nfs" ]];then
       K8S_INFRA_VOLUME_DIR=infra/k8s-yamls/infras/volumes/nfs/
    elif [[ "${NFS_PROTOCOL}" == "glusterfs" ]];then
       K8S_INFRA_VOLUME_DIR=infra/k8s-yamls/infras/volumes/gfs/
    fi

    for i in `ls $K8S_INFRA_VOLUME_DIR | grep template.yaml`
    do
      #echo -e "\nkubectl apply -f $K8S_INFRA_VOLUME_DIR/$i -n $NAMESPACE"
      kubectl apply -f $K8S_INFRA_VOLUME_DIR/$i -n $NAMESPACE > /dev/null 2>&1 &
    done

    for i in `ls $K8S_INFRA_STATEFULSET_DIR | grep .yaml`
    do
      #echo -e "\nkubectl apply -f $K8S_INFRA_STATEFULSET_DIR/$i -n $NAMESPACE"
      kubectl apply -f $K8S_INFRA_STATEFULSET_DIR/$i -n $NAMESPACE > /dev/null 2>&1 &
    done

}
k8s_infra_remove (){

    WORK_PATH=$(dirname "$0")
    KOMPOSE=/usr/local/bin/kompose
    KUBECTL=/usr/local/bin/kubectl
    source $WORK_PATH/dev.env
    K8S_INFRA_STATEFULSET_DIR=infra/k8s-yamls/infras/statefulsets/

    if [[ "${NFS_PROTOCOL}" == "nfs" ]];then
       K8S_INFRA_VOLUME_DIR=infra/k8s-yamls/infras/volumes/nfs/
    elif [[ "${NFS_PROTOCOL}" == "glusterfs" ]];then
       K8S_INFRA_VOLUME_DIR=infra/k8s-yamls/infras/volumes/gfs/
    fi

    for i in `ls $K8S_INFRA_STATEFULSET_DIR | grep .yaml`
    do
      echo kubectl delete -f $K8S_INFRA_STATEFULSET_DIR/$i -n $NAMESPACE
      kubectl delete -f $K8S_INFRA_STATEFULSET_DIR/$i -n $NAMESPACE
    done

    sleep 10

    for i in `ls $K8S_INFRA_VOLUME_DIR | grep template.yaml`
    do
      echo kubectl delete -f $K8S_INFRA_VOLUME_DIR/$i -n $NAMESPACE
      kubectl delete -f $K8S_INFRA_VOLUME_DIR/$i -n $NAMESPACE
    done


}
k8s_infra_restart (){

    WORK_PATH=$(dirname "$0")
    KOMPOSE=/usr/local/bin/kompose
    KUBECTL=/usr/local/bin/kubectl
    source $WORK_PATH/dev.env
    K8S_INFRA_STATEFULSET_DIR=infra/k8s-yamls/infras/statefulsets/

    if [[ "${NFS_PROTOCOL}" == "nfs" ]];then
       K8S_INFRA_VOLUME_DIR=infra/k8s-yamls/infras/volumes/nfs/
    elif [[ "${NFS_PROTOCOL}" == "glusterfs" ]];then
       K8S_INFRA_VOLUME_DIR=infra/k8s-yamls/infras/volumes/gfs/
    fi


    for i in `ls $K8S_INFRA_STATEFULSET_DIR | grep .yaml`
    do
      echo kubectl delete -f $K8S_INFRA_STATEFULSET_DIR/$i -n $NAMESPACE
      kubectl delete -f $K8S_INFRA_STATEFULSET_DIR/$i -n $NAMESPACE
      sleep 10
      echo kubectl apply -f $K8S_INFRA_STATEFULSET_DIR/$i -n $NAMESPACE
      kubectl apply -f $K8S_INFRA_STATEFULSET_DIR/$i -n $NAMESPACE
    done

}
k8s_init_db_up (){

    WORK_PATH=$(dirname "$0")
    KOMPOSE=/usr/local/bin/kompose
    KUBECTL=/usr/local/bin/kubectl
    INIT_DB_K8S=infra/k8s-yamls/init-db/
    source $WORK_PATH/dev.env

    cat infra/.init-db.yaml > infra/.init-db-k8s.yaml
    ### replace init-db docker image repo ###
    if [[ ! -z "$REGISTRY_REPO" ]]; then
         for i in `grep -vE "^[ ]+#"  infra/.init-db-k8s.yaml | grep -vE "^#" | grep " image: " | awk '{len=split($0,a," ");print a[len]}'`
         do
           sed -i "s#${i}#${REGISTRY_REPO}/`echo ${i} | awk '{len=split($0,a,"/");print a[len]}'`#g" infra/.init-db-k8s.yaml
         done
    fi

    ### delete volume field in compose file ###
    sed -i '/volumes:/d' infra/.init-db-k8s.yaml
    sed -i '/- \/.*:\/.*:rw/d' infra/.init-db-k8s.yaml

    kompose -f infra/.init-db-k8s.yaml convert -o $INIT_DB_K8S > /dev/null 2>&1

    sed -i "s/extensions\/v1beta1/batch\/v1/g" $INIT_DB_K8S/*.yaml
    sed -i "s/Deployment/Job/g" $INIT_DB_K8S/*.yaml
    sed -i "s/restartPolicy: Always/restartPolicy: Never/g" $INIT_DB_K8S/*.yaml
    sed -i '/replicas: 1/d' $INIT_DB_K8S/*.yaml
    sed -i '/strategy: {}/d' $INIT_DB_K8S/*.yaml

    echo -e "\nkubectl apply -f infra/k8s-yamls/init-db/${1}-deployment.yaml -n $NAMESPACE"
    kubectl apply -f infra/k8s-yamls/init-db/${1}-deployment.yaml -n $NAMESPACE

}
k8s_init_db_remove (){

    WORK_PATH=$(dirname "$0")
    KOMPOSE=/usr/local/bin/kompose
    KUBECTL=/usr/local/bin/kubectl
    INIT_DB_K8S=infra/k8s-yamls/init-db/
    source $WORK_PATH/dev.env
    source $WORK_PATH/dev-k8s.env

    cat infra/.init-db.yaml > infra/.init-db-k8s.yaml

    ### delete volume field in compose file ###
    sed -i '/volumes:/d' infra/.init-db-k8s.yaml
    sed -i '/- \/.*:\/.*:rw/d' infra/.init-db-k8s.yaml

    kompose -f infra/.init-db-k8s.yaml convert -o $INIT_DB_K8S > /dev/null 2>&1

    sed -i "s/extensions\/v1beta1/batch\/v1/g" $INIT_DB_K8S/*.yaml
    sed -i "s/Deployment/Job/g" $INIT_DB_K8S/*.yaml
    sed -i "s/restartPolicy: Always/restartPolicy: Never/g" $INIT_DB_K8S/*.yaml
    sed -i '/replicas: 1/d' $INIT_DB_K8S/*.yaml
    sed -i '/strategy: {}/d' $INIT_DB_K8S/*.yaml

    echo -e "\nkubectl delete -f infra/k8s-yamls/init-db/${1}-deployment.yaml -n $NAMESPACE"
    kubectl delete -f infra/k8s-yamls/init-db/${1}-deployment.yaml -n $NAMESPACE

}
run_k8s (){
    source dev.env

    if $K8S_DEPLOY
    then
      kubectl create namespace $NAMESPACE > /dev/null 2>&1
      bfop_namespace=`kubectl get namespace | grep $NAMESPACE | awk '{print $1}'`
      if [[ $bfop_namespace != $NAMESPACE ]]
      then
        echo -e "  \033[33m  k8s $NAMESPACE namespace 无法创建，请申请有权限的人员通过如下命令创建 \033[0m "
        echo -e "  \033[33m  kubectl create namespace $NAMESPACE \033[0m "
        exit 1
      fi
    fi

    if $K8S_DEPLOY && [[ "$1" == k8s-module ]] && [[ "$2" == up ]]
    then
      k8s_module_up $3
      sleep 10
      exit 0
    fi
    if $K8S_DEPLOY && [[ "$1" == k8s-module ]] && [[ "$2" == rm ]]
    then
      k8s_module_remove $3
      sleep 10
      exit 0
    fi
    if $K8S_DEPLOY && [[ "$1" == k8s-module ]] && [[ "$2" == update ]]
    then
      k8s_module_update $3
      sleep 10
      exit 0
    fi
    if $K8S_DEPLOY_DB && [[ "$1" == k8s-infra ]] && [[ "$2" == up ]]
    then
      k8s_infra_up
      sleep 10
      exit 0
    fi
    if $K8S_DEPLOY_DB && [[ "$1" == k8s-infra ]] && [[ "$2" == restart ]]
    then
      k8s_infra_restart
      sleep 10
      exit 0
    fi
    if $K8S_DEPLOY_DB && [[ "$1" == k8s-infra ]] && [[ "$2" == rm ]]
    then
      k8s_infra_remove
      sleep 10
      exit 0
    fi
    if $K8S_DEPLOY && [[ "$1" == k8s-init-db ]] && [[ "$2" == up ]]
    then
      k8s_init_db_up $3
      sleep 10
      exit 0
    fi
    if $K8S_DEPLOY && [[ "$1" == k8s-init-db ]] && [[ "$2" == rm ]]
    then
      k8s_init_db_remove $3
      sleep 10
      exit 0
    fi
}