#!/bin/bash
init_portfile (){
  # 创建出来一个空的模板文件
  if [[ ! -f infra/.swarm-expose-port.yaml ]]
  then
  echo "version: '3.4'
services:

  swarm-port:
    image: harbor.emotibot.com/emotibot-k8s/haproxy-keepalived:v4 
    deploy:
      replicas: 1
    ports:
    environment:
      KEEPDOWN: 'yes'
      haproxy_item1: |-

networks:
  default:
    external:
      name : emotibot" > infra/.swarm-expose-port.yaml
  fi
}
port_clash_check (){
  newcome=$1
  # bind端口检测是否冲突，如果冲突，自行选择一个新的可用的
  bindinfo=`grep bind infra/.swarm-expose-port.yaml`
  if [[ $bindinfo =~ $newcome ]]
    then
      let newcome++ || true
      port_clash_check $newcome
  else
    export newcome=$newcome
  fi
}
project_expose_port (){
  export overport=false
  echo $@
  local num=1
  for p in $@
  do
    if [[ $num -eq ${#@} ]]
      then
        export overport=true
    fi
    project=`find emotibot/ -type d -name ${p%.*}-*`
    swarm_expose_port ${project#*/}
    let num++ || true
  done
  exit 0
}
swarm_expose_port (){
  # 开放swarm端口，非常规操作
  # init haproxy yaml
  init_portfile
  # 获取需要开放的端口的内部端口，以及swarm中的服务名称
  if [[ -n `echo $1 | grep '^[[:digit:]]*$'` ]] 2>/dev/null
    then
     port_project=(`ls emotibot`)
     # 遍历各产品目录，获取dev.env 内部变量，替换port.yaml 内部变量key为value
     for (( i=0;i<${#port_project[@]};i++))
     do
       if [[ -f emotibot/${port_project[i]}/dev.env ]] && [[ -f emotibot/${port_project[i]}/port.yaml ]] ; then
         source emotibot/${port_project[i]}/dev.env
         cp emotibot/${port_project[i]}/port.yaml emotibot/${port_project[i]}/.port.yaml
         #获取port.yaml 中端口号 变量
         grep -E -o "\\$\{[a-Z_]*\}" emotibot/${port_project[i]}/port.yaml > emotibot/${port_project[i]}/.env.list
         #遍历端口号变量为具体的端口号
         env_list=(`cat emotibot/${port_project[i]}/.env.list`)
         for (( j=0;j<${#env_list[@]};j++))
         do
          sed -i "s/${env_list[j]}/`eval echo ${env_list[j]}`/g" emotibot/${port_project[i]}/.port.yaml
         done
       fi
     done
     fileinfo=`grep -rn $1 --include .port.yaml emotibot`
     if [[ ! $? -eq 0 ]]
     then
       echo -e "\033[33m No service usage Port $1 ... ...\033[0m"
       exit 1
     fi
     portyamlfile=`echo $fileinfo | awk -F ":" '{print $1}'`
     portyamlline=`echo $fileinfo | awk -F ":" '{print $2}'`
     portyamlport=`echo $fileinfo | awk '{print $NF}'`

     for i in `seq 0 10`
     do
       checkname=`sed -n ${portyamlline}p $portyamlfile | grep -E '^[[:space:]]{2}[a-Z]'`
       if [[ -n $checkname ]]
       then
         checkname=${checkname%:*}
         break
       else
         let portyamlline-- || true
       fi
     done
    # 处理端口信息，端口包含外部端口，内部端口，haproxy监听端口，并保证不冲突，以及一一对应
      insideport=${portyamlport#*:}
      outsideport=${portyamlport%:*}
      insideport=${insideport//\/tcp/}
      insideport=${insideport//\/udp/}
      insideport=${insideport//\//}
     # 过滤掉端口范围的开放方式
     if [[ $insideport =~ "-" ]]
       then
         insideport=${insideport%-*}
         outsideport=${outsideport%-*}
     fi
     # 找到行信息，插入新配置，并检测配置是否冲突,并生成新端口映射关系
     port_clash_check $insideport

    # 如果该配置不存在
    writeconfig=false
    if [[ ! -n `grep "$outsideport:" infra/.swarm-expose-port.yaml` ]] && [[ ! -n `docker service ls |grep ":$outsideport"` ]]
    then
      sed -i "/ports:/a\\      - '${outsideport}:${newcome}'" infra/.swarm-expose-port.yaml
      writeconfig=true
    fi
    echo "        listen ${checkname}${newcome}
            bind *:${newcome}
            mode http
            maxconn 65535
            log   global
            timeout client 1d
            timeout server 1d
            hash-type consistent
            server ${checkname}${insideport} ${checkname}:${insideport} maxconn 10000 check" > /tmp/.swarm-port.tmp
   if [[ ! -n `grep "${checkname}:${newcome}" infra/.swarm-expose-port.yaml` ]] && $writeconfig
   then
     sedline=`grep -n haproxy_item1 infra/.swarm-expose-port.yaml`
     sed -i "${sedline%%:*} r /tmp/.swarm-port.tmp" infra/.swarm-expose-port.yaml
   fi
   if [[ "$2" == oneport ]]
     then
       echo "wait docker stack deploy -c infra/.swarm-expose-port.yaml port"
       echo -e "\033[33mWARNING :  \033[31m该方式仅限debug，禁止对接任何程序\033[0m"
       echo -e "\033[31m请在以下提示后输入：我已经确认不会用于生产。作为你对以上使用方式的承诺\033[0m"
       read -t 180 -p $'请输入: \n' promise
       if [[ $promise =~ "我已经确认不会用于生产" ]] ; then
         echo -e "\033[32m恭喜你承诺成功\033[0m"
         echo -e "\033[31m为了保证你现在智商正常，请计算1+1 的结果，请在以下提示后输入结果\033[0m"
         read -t 10 -p $'请输入计算结果: \n' result
         if [[ $result == 2 ]] ; then
           sleep 1
           docker stack deploy --resolve-image never -c infra/.swarm-expose-port.yaml port
         else
           echo -e "\033[31m你现在处于弱智状态，请你回家休息一下\033[0m"
           exit 1
         fi
       else
         echo -e "\033[31m承诺失败，请重新执行脚本，重新承诺\033[0m"
         exit 1
       fi
       exit 0
   fi
 elif [[ `ls emotibot` =~ "$1" ]] && [[ -n $1 ]]
   then
     project_port_path=emotibot/$1/port.yaml
     project_port_list=`grep -E "[0-9]+:[0-9]+" $project_port_path  | grep -v "#" | awk '{print $2}'`
     for pp in $project_port_list
     do
       pp=${pp%:*}
       if [[ $pp =~ "-" ]]
         then
           for i in `seq ${pp%-*} ${pp#*-}`
           do
             echo "$pp Format not supported ..........        .............."
             swarm_expose_port ${i}
           done
       else
             swarm_expose_port $pp
       fi
     done
   if $overport
     then
       #echo "wait docker stack deploy -c infra/.swarm-expose-port.yaml port"
       docker stack deploy --resolve-image never -c infra/.swarm-expose-port.yaml port
       exit 0
   fi
 else
   echo -e "\033[33m                                swarm expose port\033[0m"
   banner_list
   select_number expose
 fi
}
