#!/bin/bash

k8s_convert_load_balance (){

    WORK_PATH=$(dirname "$0")
    KOMPOSE=/usr/local/bin/kompose
    KUBECTL=/usr/local/bin/kubectl
    source $WORK_PATH/dev.env
    MODULE_YAML=$1/.module-k8s.yaml
    source $1/dev.env

# ==================================================================
# 1-bf
# ==================================================================
    ### change proxy  position env for k8s/nginx ###
    sed -i "s/\${BFB_HOST}/\${BFB_HOST}.\${NAMESPACE}.\${K8S_DOMAIN}/g" $MODULE_YAML
    sed -i "s/\${XEON_KG_HOST}/\${XEON_KG_HOST}.\${NAMESPACE}.\${K8S_DOMAIN}/g" $MODULE_YAML
    sed -i "s/\${XEON_KG_DAL_HOST}/\${XEON_KG_DAL_HOST}.\${NAMESPACE}.\${K8S_DOMAIN}/g" $MODULE_YAML
    sed -i "s/\${ADMIN_SERVICE_HOST}/\${ADMIN_SERVICE_HOST}.\${NAMESPACE}.\${K8S_DOMAIN}/g" $MODULE_YAML
    sed -i "s/\${COMMON_UI_HOST}/\${COMMON_UI_HOST}.\${NAMESPACE}.\${K8S_DOMAIN}/g" $MODULE_YAML
    sed -i "s/\${SSM_UI_HOST}/\${SSM_UI_HOST}.\${NAMESPACE}.\${K8S_DOMAIN}/g" $MODULE_YAML
    sed -i "s/\${CHAT_UI_HOST}/\${CHAT_UI_HOST}.\${NAMESPACE}.\${K8S_DOMAIN}/g" $MODULE_YAML
    sed -i "s/\${SKILL_UI_HOST}/\${SKILL_UI_HOST}.\${NAMESPACE}.\${K8S_DOMAIN}/g" $MODULE_YAML
    sed -i "s/\${TDE_UI_HOST}/\${TDE_UI_HOST}.\${NAMESPACE}.\${K8S_DOMAIN}/g" $MODULE_YAML
    sed -i "s/\${QACORE_UI_HOST}/\${QACORE_UI_HOST}.\${NAMESPACE}.\${K8S_DOMAIN}/g" $MODULE_YAML
    sed -i "s/\${COMMON_VIDEOS_HOST}/\${COMMON_VIDEOS_HOST}.\${NAMESPACE}.\${K8S_DOMAIN}/g" $MODULE_YAML
    sed -i "s/\${SSMDAC_HOST}/\${SSMDAC_HOST}.\${NAMESPACE}.\${K8S_DOMAIN}/g" $MODULE_YAML
    sed -i "s/\${SFT_HOST}/\${SFT_HOST}.\${NAMESPACE}.\${K8S_DOMAIN}/g" $MODULE_YAML
    sed -i "s/\${CCS_UI_HOST}/\${CCS_UI_HOST}.\${NAMESPACE}.\${K8S_DOMAIN}/g" $MODULE_YAML
    sed -i "s/\${CCS_DAL_HOST}/\${CCS_DAL_HOST}.\${NAMESPACE}.\${K8S_DOMAIN}/g" $MODULE_YAML
    sed -i "s/\${CCS_CONTROLLER_HOST}/\${CCS_CONTROLLER_HOST}.\${NAMESPACE}.\${K8S_DOMAIN}/g" $MODULE_YAML
    sed -i "s/\${NER_FACTORY_DAL_HOST}/\${NER_FACTORY_DAL_HOST}.\${NAMESPACE}.\${K8S_DOMAIN}/g" $MODULE_YAML
    sed -i "s/\${PRERECORD_HOST}/\${PRERECORD_HOST}.\${NAMESPACE}.\${K8S_DOMAIN}/g" $MODULE_YAML
    sed -i "s/\${LICENSE_CONTROLLER_HOST}/\${LICENSE_CONTROLLER_HOST}.\${NAMESPACE}.\${K8S_DOMAIN}/g" $MODULE_YAML
    sed -i "s/\${ADMIN_API_HOST}/\${ADMIN_API_HOST}.\${NAMESPACE}.\${K8S_DOMAIN}/g" $MODULE_YAML
    sed -i "s/\${ADMIN_HOST}/\${ADMIN_HOST}.\${NAMESPACE}.\${K8S_DOMAIN}/g" $MODULE_YAML
    sed -i "s/\${ADMIN_AUTH_HOST}/\${ADMIN_AUTH_HOST}.\${NAMESPACE}.\${K8S_DOMAIN}/g" $MODULE_YAML
    sed -i "s/\${ADAPTER_SERVER_HOST}/\${ADAPTER_SERVER_HOST}.\${NAMESPACE}.\${K8S_DOMAIN}/g" $MODULE_YAML
    sed -i "s/\${IE_HOST}/\${IE_HOST}.\${NAMESPACE}.\${K8S_DOMAIN}/g" $MODULE_YAML
    #sed -i "s/\${CONSUL_HOST}/\${CONSUL_HOST}.\${NAMESPACE}.\${K8S_DOMAIN}/g" $MODULE_YAML
    sed -i "s/\${CAPSULE_CONTROLLER_HOST}/\${CAPSULE_CONTROLLER_HOST}.\${NAMESPACE}.\${K8S_DOMAIN}/g" $MODULE_YAML
    sed -i "s/\${COMMON_HELP_DOC_HOST}/\${COMMON_HELP_DOC_HOST}.\${NAMESPACE}.\${K8S_DOMAIN}/g" $MODULE_YAML
    #sed -i "s/\${IM_ADMIN_HOST}/\${IM_ADMIN_HOST}.\${NAMESPACE}.\${K8S_DOMAIN}/g" $MODULE_YAML
    #sed -i "s/\${MSG_KF_HOST}/\${MSG_KF_HOST}.\${NAMESPACE}.\${K8S_DOMAIN}/g" $MODULE_YAML
    #sed -i "s/\${WEB_AGENT_HOST}/\${WEB_AGENT_HOST}.\${NAMESPACE}.\${K8S_DOMAIN}/g" $MODULE_YAML
    sed -i "s/\${EC_HOST}/\${EC_HOST}.\${NAMESPACE}.\${K8S_DOMAIN}/g" $MODULE_YAML

    sed -i "s/\${DEFAULT_IP}:8000/\${DEFAULT_IP}:31280/g" $MODULE_YAML
    sed -i "s/\${DEFAULT_IP}:8502/\${DEFAULT_IP}:31680/g" $MODULE_YAML
    sed -i "s/\${DEFAULT_IP}:8503/\${DEFAULT_IP}:31780/g" $MODULE_YAML
    sed -i "s/\${DEFAULT_IP}:8504/\${DEFAULT_IP}:31404/g" $MODULE_YAML
    sed -i "s/\${DEFAULT_IP}:8505/\${DEFAULT_IP}:31305/g" $MODULE_YAML
    sed -i "s/\${DEFAULT_IP}:8506/\${DEFAULT_IP}:31580/g" $MODULE_YAML
    sed -i "s/\${DEFAULT_IP}:8507/\${DEFAULT_IP}:31807/g" $MODULE_YAML
    sed -i "s/\${DEFAULT_IP}:8508/\${DEFAULT_IP}:31980/g" $MODULE_YAML
    #sed -i "s/\${DEFAULT_IP}:8220/\${DEFAULT_IP}:8220/g" $MODULE_YAML
    #sed -i "s/\${DEFAULT_IP}:16255/\${DEFAULT_IP}:16255/g" $MODULE_YAML
    sed -i "s/\${DEFAULT_IP}:3003/\${DEFAULT_IP}:31303/g" $MODULE_YAML
    sed -i "s/\${DEFAULT_IP}:8085/\${DEFAULT_IP}:31285/g" $MODULE_YAML
    #sed -i "s/\${DEFAULT_IP}:8510/\${DEFAULT_IP}:8510/g" $MODULE_YAML

}
k8s_convert_before (){

    WORK_PATH=$(dirname "$0")
    KOMPOSE=/usr/local/bin/kompose
    KUBECTL=/usr/local/bin/kubectl
    source $WORK_PATH/dev.env
    MODULE_YAML=$1/.module.yaml
    source $1/dev.env
    #for i in `ls $WORK_PATH/emotibot`;do source $WORK_PATH/emotibot/$i/dev.env;done

    #for i in `ls $WORK_PATH/emotibot`
# do
#    if [[ -f $WORK_PATH/emotibot/$i/.module.yaml ]];then
#       MODULE_YAML=$WORK_PATH/emotibot/$i/.module.yaml
#    fi

# ==================================================================
# 1-bf
# ==================================================================

    ### change  admin-ui kg-dal position env for k8s/nginx ###
    sed -i "s/ADMIN_KNOWLEDGE_GRAPH_URL: ${XEON_KG_DAL_HOST}:18811/ADMIN_KNOWLEDGE_GRAPH_URL: ${XEON_KG_DAL_HOST}.${NAMESPACE}.${K8S_DOMAIN}:18811/g" $MODULE_YAML

    ### change  admin-ui kg position env for k8s/nginx ###
    sed -i "s/ADMIN_KNOWLEDGE_KBQA_URL: ${XEON_KG_HOST}:13508/ADMIN_KNOWLEDGE_KBQA_URL: ${XEON_KG_HOST}.${NAMESPACE}.${K8S_DOMAIN}:13508/g" $MODULE_YAML

    ### change ccs-admin-ui  position env for k8s/nginx ###
    sed -i "s/ADMIN_CCS_DAL_URL: ${CCS_DAL_HOST}:16256/ADMIN_CCS_DAL_URL: ${CCS_DAL_HOST}.${NAMESPACE}.${K8S_DOMAIN}:16256/g" $MODULE_YAML
    sed -i "s/ADMIN_CCS_DIVERTER_URL: ${CCS_DIVERTER_HOST}:16255/ADMIN_CCS_DIVERTER_URL: ${CCS_DIVERTER_HOST}.${NAMESPACE}.${K8S_DOMAIN}:16255/g" $MODULE_YAML
    sed -i "s/ADMIN_CCS_CONTROLLER_URL: ${CCS_CONTROLLER_HOST}:10000/ADMIN_CCS_CONTROLLER_URL: ${CCS_CONTROLLER_HOST}.${NAMESPACE}.${K8S_DOMAIN}:10000/g" $MODULE_YAML
    sed -i "s/ADMIN_SSM_DAC_URL: ${SSMDAC_HOST}:8686/ADMIN_SSM_DAC_URL: ${SSMDAC_HOST}.${NAMESPACE}.${K8S_DOMAIN}:8686/g" $MODULE_YAML
    sed -i "s/ADMIN_UTILS_URL: ${ADMIN_SERVICE_HOST}:15307/ADMIN_UTILS_URL: ${ADMIN_SERVICE_HOST}.${NAMESPACE}.${K8S_DOMAIN}:15307/g" $MODULE_YAML
    sed -i "s/ADMIN_CCS_UI_URL: ${CCS_UI_HOST}:8110/ADMIN_CCS_UI_URL: ${CCS_UI_HOST}.${NAMESPACE}.${K8S_DOMAIN}:8110/g" $MODULE_YAML
    sed -i "s/ADMIN_SSM_UI_URL: ${SSM_UI_HOST}:3001/ADMIN_SSM_UI_URL: ${SSM_UI_HOST}.${NAMESPACE}.${K8S_DOMAIN}:3001/g" $MODULE_YAML
    sed -i "s/ADMIN_TASK_PARSER_URL: ${TASK_PARSER_HOST}:8089/ADMIN_TASK_PARSER_URL: ${TASK_PARSER_HOST}.${NAMESPACE}.${K8S_DOMAIN}:8089/g" $MODULE_YAML

    ### delete volume field in compose file ###
    sed -i '/\/etc\/localtime/d' $MODULE_YAML
    sed -i '/volumes:/d' $MODULE_YAML
    sed -i '/- \/.*:\/.*:rw/d' $MODULE_YAML
    sed -i '/\/dev\/mem/d' $MODULE_YAML
    sed -i '/\/var\/license\/key/d' $MODULE_YAML

    ### delete healthcheck field in compose file ###
    sed -i '/healthcheck:/d' $MODULE_YAML
    sed -i '/interval:/d' $MODULE_YAML
    sed -i '/timeout:/d' $MODULE_YAML
    sed -i '/retries:/d' $MODULE_YAML

    ### replace module docker image repo ###
    if [[ ! -z "$REGISTRY_REPO" ]]; then
         for i in `grep -vE "^[ ]+#"  $MODULE_YAML | grep -vE "^#" | grep " image: " | awk '{len=split($0,a," ");print a[len]}'`
         do
           sed -i "s#${i}#${REGISTRY_REPO}/`echo ${i} | awk '{len=split($0,a,"/");print a[len]}'`#g" $MODULE_YAML
         done
    fi

#done

}


k8s_convert_post (){

     WORK_PATH=$(dirname "$0")
     K8S_YAML_MODULE_DIR=$1/k8s-yamls/modules/origins/
     source $WORK_PATH/dev.env

     if [[ "${NFS_PROTOCOL}" == "nfs" ]];then
        K8S_YAML_VOLUME_DIR=$1/k8s-yamls/volumes/nfs/
     elif [[ "${NFS_PROTOCOL}" == "glusterfs" ]];then
        K8S_YAML_VOLUME_DIR=$1/k8s-yamls/volumes/gfs/
     fi

    ### set replicas ###
    sed -i "s/replicas: 1/replicas: ${REPLICAS}/g" $(ls $K8S_YAML_MODULE_DIR/*deployment.yaml)

    ### replace pv volume env ###
    for i in `ls $K8S_YAML_VOLUME_DIR | grep -v template.yaml`
    do
      cat $K8S_YAML_VOLUME_DIR/$i > $K8S_YAML_VOLUME_DIR/$i.yaml
    done
    sed -i "s!\${NFS_SERVER}!$(echo ${NFS_SERVER})!g" $(ls $K8S_YAML_VOLUME_DIR/*.yaml)
    sed -i "s!\${NFS_PATH}!$(echo ${NFS_PATH})!g" $(ls $K8S_YAML_VOLUME_DIR/*.yaml)
    sed -i "s!\${NAMESPACE}!$(echo ${NAMESPACE})!g" $(ls $K8S_YAML_VOLUME_DIR/*.yaml)
    sed -i "s!\${VOLUME_PATH}!$(echo ${VOLUME_PATH})!g" $(ls $K8S_YAML_VOLUME_DIR/*.yaml)


#     for i in `ls $WORK_PATH/emotibot`
# do

#     K8S_YAML_MODULE_DIR=$i/k8s-yamls/modules/origins/

# ==================================================================
# 1-bf
# ==================================================================
if [[ "$1" == "emotibot/1-bf" ]];then
    ### add time volumeMount field to  all  yaml except simple-ft-service|skill-backend|skill-controller|bfop-license.yaml|csa-assistant-transmission-application-deployment.yaml|csa-assistant-text-upload-deployment.yaml|csa-assistant-voice-upload-deployment.yaml in post convert ###
    sed -i '/resources: {}/i\        volumeMounts:\n        - mountPath: /etc/localtime\n          name: hosttime' $(ls $K8S_YAML_MODULE_DIR/*deployment.yaml | grep -vE "simple-ft-service-deployment.yaml|skill-backend-deployment.yaml|skill-controller-deployment.yaml|bfop-license-deployment.yaml")
    sed -i '/restartPolicy:/i\      volumes:\n       - name: hosttime\n         hostPath:\n           path: /etc/localtime' $(ls $K8S_YAML_MODULE_DIR/*deployment.yaml | grep -vE "simple-ft-service-deployment.yaml|skill-backend-deployment.yaml|skill-controller-deployment.yaml|bfop-license-deployment.yaml")

    ### add host volumeMount field to  bfop-license in post convert ###
    if [[ -f "$K8S_YAML_MODULE_DIR/bfop-license-deployment.yaml" ]]; then
       sed -i '/resources: {}/i\        volumeMounts:\n        - mountPath: /dev/mem\n          name: sys\n        - mountPath: /usr/src/app/key\n          name: key\n        - mountPath: /etc/localtime\n          name: hosttime'  $K8S_YAML_MODULE_DIR/bfop-license-deployment.yaml
       sed -i '/status: {}/i\      volumes:\n       - name: sys\n         hostPath:\n           path: /dev/mem\n       - name: key\n         hostPath:\n           path: /var/license/key\n       - name: hosttime\n         hostPath:\n           path: /etc/localtime'  $K8S_YAML_MODULE_DIR/bfop-license-deployment.yaml
    fi

     ### add host volumeMount field to  simple-ft-service in post convert ###
    if [[ -f "$K8S_YAML_MODULE_DIR/simple-ft-service-deployment.yaml" ]]; then
       sed -i '/resources: {}/i\        volumeMounts:\n        - mountPath: /etc/localtime\n          name: hosttime'  $K8S_YAML_MODULE_DIR/simple-ft-service-deployment.yaml
       sed -i '/status: {}/i\      volumes:\n       - name: hosttime\n         hostPath:\n           path: /etc/localtime'  $K8S_YAML_MODULE_DIR/simple-ft-service-deployment.yaml

       sed -i '/resources: {}/i\        - mountPath: /usr/bin/app/model\n          name: model' $K8S_YAML_MODULE_DIR/simple-ft-service-deployment.yaml
       sed -i '/status: {}/i\       - name: model\n         persistentVolumeClaim:\n           claimName: simple-ft-service-claim-model' $K8S_YAML_MODULE_DIR/simple-ft-service-deployment.yaml
    fi

      ### add host volumeMount field to  skill-backend in post convert ###
    #if [[ -f "$K8S_YAML_MODULE_DIR/skill-backend-deployment.yaml" ]]; then
    #   sed -i '/resources: {}/i\        volumeMounts:\n        - mountPath: /etc/localtime\n          name: hosttime'  $K8S_YAML_MODULE_DIR/skill-backend-deployment.yaml
    #   sed -i '/status: {}/i\      volumes:\n       - name: hosttime\n         hostPath:\n           path: /etc/localtime'  $K8S_YAML_MODULE_DIR/skill-backend-deployment.yaml
    #   sed -i '/resources: {}/i\        - mountPath: /home/deployer/skill_springboot/image\n          name: image' $K8S_YAML_MODULE_DIR/skill-backend-deployment.yaml
    #   sed -i '/status: {}/i\       - name: image\n         persistentVolumeClaim:\n           claimName: skill-backend-claim-image' $K8S_YAML_MODULE_DIR/skill-backend-deployment.yaml
    #fi

      ### add host volumeMount field to  skill-controller in post convert ###
    #if [[ -f "$K8S_YAML_MODULE_DIR/skill-controller-deployment.yaml" ]]; then
    #   sed -i '/resources: {}/i\        volumeMounts:\n        - mountPath: /etc/localtime\n          name: hosttime'  $K8S_YAML_MODULE_DIR/skill-controller-deployment.yaml
    #   sed -i '/status: {}/i\      volumes:\n       - name: hosttime\n         hostPath:\n           path: /etc/localtime'  $K8S_YAML_MODULE_DIR/skill-controller-deployment.yaml
    #
    #   sed -i '/resources: {}/i\        - mountPath: /var/log/EmotibotController2.0\n          name: debug' $K8S_YAML_MODULE_DIR/skill-controller-deployment.yaml
    #   sed -i '/status: {}/i\       - name: debug\n         persistentVolumeClaim:\n           claimName: skill-controller-claim-debug' $K8S_YAML_MODULE_DIR/skill-controller-deployment.yaml
    #
    #   sed -i '/resources: {}/i\        - mountPath: /var/log/statistic\n          name: statistic' $K8S_YAML_MODULE_DIR/skill-controller-deployment.yaml
    #   sed -i '/status: {}/i\       - name: statistic\n         persistentVolumeClaim:\n           claimName: skill-controller-claim-statistic' $K8S_YAML_MODULE_DIR/skill-controller-deployment.yaml
    #
    #   sed -i '/resources: {}/i\        - mountPath: /var/log/OppoControllerLog\n          name: log' $K8S_YAML_MODULE_DIR/skill-controller-deployment.yaml
    #   sed -i '/status: {}/i\       - name: log\n         persistentVolumeClaim:\n           claimName: skill-controller-claim-log' $K8S_YAML_MODULE_DIR/skill-controller-deployment.yaml
    #
    #fi
       ### other error converting YAML to JSON ###
     if [[ -f "$K8S_YAML_MODULE_DIR/emotibot-controller-deployment.yaml" ]]; then
       sed -i '/hostname:/d' $K8S_YAML_MODULE_DIR/emotibot-controller-deployment.yaml
     fi
     if [[ -f "$K8S_YAML_MODULE_DIR/xchat-deployment.yaml" ]]; then
       sed -i '/hostname:/d' $K8S_YAML_MODULE_DIR/xchat-deployment.yaml
     fi

       ### replace emotibit-controller service port EC_EXPOSE_PORT to EC_PORT ###
     if [[ -f "$K8S_YAML_MODULE_DIR/emotibot-controller-service.yaml" ]]; then
       sed -i "s/${EC_EXPOSE_PORT}/${EC_PORT}/g" $K8S_YAML_MODULE_DIR/emotibot-controller-service.yaml
     fi
fi
# ==================================================================
# 2-ccbot
# ==================================================================
if [[ "$1" == "emotibot/2-ccbot" ]];then
    ### add time volumeMount field to  all  yaml except  cc-ext-service|cc-license-controller|freeswitch-emotibot|log-service|records-manager|unimrcp-emotibot  in post convert ###
    sed -i '/resources: {}/i\        volumeMounts:\n        - mountPath: /etc/localtime\n          name: hosttime' $(ls $K8S_YAML_MODULE_DIR/*deployment.yaml | grep -vE "cc-ext-service-deployment.yaml|cc-license-controller-deployment.yaml|freeswitch-emotibot-deployment.yaml|log-service-deployment.yaml|records-manager-deployment.yaml|unimrcp-emotibot-deployment.yaml")
    sed -i '/restartPolicy:/i\      volumes:\n       - name: hosttime\n         hostPath:\n           path: /etc/localtime' $(ls $K8S_YAML_MODULE_DIR/*deployment.yaml | grep -vE "cc-ext-service-deployment.yaml|cc-license-controller-deployment.yaml|freeswitch-emotibot-deployment.yaml|log-service-deployment.yaml|records-manager-deployment.yaml|unimrcp-emotibot-deployment.yaml")

    ### add host volumeMount field to  cc-ext-service in post convert ###
    if [[ -f "$K8S_YAML_MODULE_DIR/cc-ext-service-deployment.yaml" ]]; then
       sed -i '/resources: {}/i\        volumeMounts:\n        - mountPath: /etc/localtime\n          name: hosttime'  $K8S_YAML_MODULE_DIR/cc-ext-service-deployment.yaml
       sed -i '/status: {}/i\      volumes:\n       - name: hosttime\n         hostPath:\n           path: /etc/localtime'  $K8S_YAML_MODULE_DIR/cc-ext-service-deployment.yaml

       sed -i '/resources: {}/i\        - mountPath: /home/deployer/voice/freeswitch/recordings\n          name: recordings' $K8S_YAML_MODULE_DIR/cc-ext-service-deployment.yaml
       sed -i '/status: {}/i\       - name: recordings\n         persistentVolumeClaim:\n           claimName: cc-ext-service-claim-recordings' $K8S_YAML_MODULE_DIR/cc-ext-service-deployment.yaml

       sed -i '/resources: {}/i\        - mountPath: /home/deployer/voice/mrcp/pre_record\n          name: pre-record' $K8S_YAML_MODULE_DIR/cc-ext-service-deployment.yaml
       sed -i '/status: {}/i\       - name: pre-record\n         persistentVolumeClaim:\n           claimName: cc-ext-service-claim-pre-record' $K8S_YAML_MODULE_DIR/cc-ext-service-deployment.yaml
    fi

    ### add host volumeMount field to  cc-license-controller in post convert ###
    if [[ -f "$K8S_YAML_MODULE_DIR/cc-license-controller-deployment.yaml" ]]; then
       sed -i '/resources: {}/i\        volumeMounts:\n        - mountPath: /etc/localtime\n          name: hosttime'  $K8S_YAML_MODULE_DIR/cc-license-controller-deployment.yaml
       sed -i '/status: {}/i\      volumes:\n       - name: hosttime\n         hostPath:\n           path: /etc/localtime'  $K8S_YAML_MODULE_DIR/cc-license-controller-deployment.yaml

       sed -i '/resources: {}/i\        - mountPath: /usr/src/app/license\n          name: license' $K8S_YAML_MODULE_DIR/cc-license-controller-deployment.yaml
       sed -i '/status: {}/i\       - name: license\n         persistentVolumeClaim:\n           claimName: cc-license-controller-claim-license' $K8S_YAML_MODULE_DIR/cc-license-controller-deployment.yaml
    fi

    ### add host volumeMount field to  freeswitch-emotibot in post convert ###
    if [[ -f "$K8S_YAML_MODULE_DIR/freeswitch-emotibot-deployment.yaml" ]]; then
       sed -i '/resources: {}/i\        volumeMounts:\n        - mountPath: /etc/localtime\n          name: hosttime'  $K8S_YAML_MODULE_DIR/freeswitch-emotibot-deployment.yaml
       sed -i '/status: {}/i\      volumes:\n       - name: hosttime\n         hostPath:\n           path: /etc/localtime'  $K8S_YAML_MODULE_DIR/freeswitch-emotibot-deployment.yaml

       sed -i '/resources: {}/i\        - mountPath: /usr/local/freeswitch/log\n          name: log' $K8S_YAML_MODULE_DIR/freeswitch-emotibot-deployment.yaml
       sed -i '/status: {}/i\       - name: log\n         persistentVolumeClaim:\n           claimName: freeswitch-emotibot-claim-log' $K8S_YAML_MODULE_DIR/freeswitch-emotibot-deployment.yaml

       sed -i '/resources: {}/i\        - mountPath: /usr/local/freeswitch/audio_custom\n          name: pre-record' $K8S_YAML_MODULE_DIR/freeswitch-emotibot-deployment.yaml
       sed -i '/status: {}/i\       - name: pre-record\n         persistentVolumeClaim:\n           claimName: freeswitch-emotibot-claim-pre-record' $K8S_YAML_MODULE_DIR/freeswitch-emotibot-deployment.yaml

       sed -i '/resources: {}/i\        - mountPath: /usr/local/freeswitch/recordings\n          name: recordings' $K8S_YAML_MODULE_DIR/freeswitch-emotibot-deployment.yaml
       sed -i '/status: {}/i\       - name: recordings\n         persistentVolumeClaim:\n           claimName: freeswitch-emotibot-claim-recordings' $K8S_YAML_MODULE_DIR/freeswitch-emotibot-deployment.yaml
    fi

    ### add host volumeMount field to  log-service in post convert ###
    if [[ -f "$K8S_YAML_MODULE_DIR/log-service-deployment.yaml" ]]; then
       sed -i '/resources: {}/i\        volumeMounts:\n        - mountPath: /etc/localtime\n          name: hosttime'  $K8S_YAML_MODULE_DIR/log-service-deployment.yaml
       sed -i '/status: {}/i\      volumes:\n       - name: hosttime\n         hostPath:\n           path: /etc/localtime'  $K8S_YAML_MODULE_DIR/log-service-deployment.yaml

       sed -i '/resources: {}/i\        - mountPath: /logo\n          name: logo' $K8S_YAML_MODULE_DIR/log-service-deployment.yaml
       sed -i '/status: {}/i\       - name: logo\n         persistentVolumeClaim:\n           claimName: log-service-claim-logo' $K8S_YAML_MODULE_DIR/log-service-deployment.yaml
    fi

    ### add host volumeMount field to  records-manager in post convert ###
    if [[ -f "$K8S_YAML_MODULE_DIR/records-manager-deployment.yaml" ]]; then
       sed -i '/resources: {}/i\        volumeMounts:\n        - mountPath: /etc/localtime\n          name: hosttime'  $K8S_YAML_MODULE_DIR/records-manager-deployment.yaml
       sed -i '/status: {}/i\      volumes:\n       - name: hosttime\n         hostPath:\n           path: /etc/localtime'  $K8S_YAML_MODULE_DIR/records-manager-deployment.yaml

       sed -i '/resources: {}/i\        - mountPath: /voice/mrcp/session\n          name: session' $K8S_YAML_MODULE_DIR/records-manager-deployment.yaml
       sed -i '/status: {}/i\       - name: session\n         persistentVolumeClaim:\n           claimName: records-manager-claim-session' $K8S_YAML_MODULE_DIR/records-manager-deployment.yaml

       sed -i '/resources: {}/i\        - mountPath: /voice/mrcp/pre_record\n          name: pre-record' $K8S_YAML_MODULE_DIR/records-manager-deployment.yaml
       sed -i '/status: {}/i\       - name: pre-record\n         persistentVolumeClaim:\n           claimName: records-manager-claim-pre-record' $K8S_YAML_MODULE_DIR/records-manager-deployment.yaml

       sed -i '/resources: {}/i\        - mountPath: /voice/freeswitch/recordings\n          name: recordings' $K8S_YAML_MODULE_DIR/records-manager-deployment.yaml
       sed -i '/status: {}/i\       - name: recordings\n         persistentVolumeClaim:\n           claimName: records-manager-claim-recordings' $K8S_YAML_MODULE_DIR/records-manager-deployment.yaml
    fi

    ### add host volumeMount field to  unimrcp-emotibot in post convert ###
    if [[ -f "$K8S_YAML_MODULE_DIR/unimrcp-emotibot-deployment.yaml" ]]; then
       sed -i '/resources: {}/i\        volumeMounts:\n        - mountPath: /etc/localtime\n          name: hosttime'  $K8S_YAML_MODULE_DIR/unimrcp-emotibot-deployment.yaml
       sed -i '/status: {}/i\      volumes:\n       - name: hosttime\n         hostPath:\n           path: /etc/localtime'  $K8S_YAML_MODULE_DIR/unimrcp-emotibot-deployment.yaml

       sed -i '/resources: {}/i\        - mountPath: /opt/unimrcp/log\n          name: log' $K8S_YAML_MODULE_DIR/unimrcp-emotibot-deployment.yaml
       sed -i '/status: {}/i\       - name: log\n         persistentVolumeClaim:\n           claimName: unimrcp-emotibot-claim-log' $K8S_YAML_MODULE_DIR/unimrcp-emotibot-deployment.yaml

       sed -i '/resources: {}/i\        - mountPath: /opt/unimrcp/var\n          name: pre-record' $K8S_YAML_MODULE_DIR/unimrcp-emotibot-deployment.yaml
       sed -i '/status: {}/i\       - name: pre-record\n         persistentVolumeClaim:\n           claimName: unimrcp-emotibot-claim-pre-record' $K8S_YAML_MODULE_DIR/unimrcp-emotibot-deployment.yaml

       sed -i '/resources: {}/i\        - mountPath: /opt/unimrcp/recordings\n          name: pre-record-2' $K8S_YAML_MODULE_DIR/unimrcp-emotibot-deployment.yaml
       sed -i '/status: {}/i\       - name: pre-record-2\n         persistentVolumeClaim:\n           claimName: unimrcp-emotibot-claim-pre-record-2' $K8S_YAML_MODULE_DIR/unimrcp-emotibot-deployment.yaml

       sed -i '/resources: {}/i\        - mountPath: /opt/unimrcp/var/rdr\n          name: session' $K8S_YAML_MODULE_DIR/unimrcp-emotibot-deployment.yaml
       sed -i '/status: {}/i\       - name: session\n         persistentVolumeClaim:\n           claimName: unimrcp-emotibot-claim-session' $K8S_YAML_MODULE_DIR/unimrcp-emotibot-deployment.yaml
    fi

    #############################################
    ###  add hostNetwork & dnsPolicy field    ###
    #############################################
    ### add hostNetwork & dnsPolicy field to  cc-license-controller in post convert ###
    if [[ -f "$K8S_YAML_MODULE_DIR/cc-license-controller-deployment.yaml" ]]; then
       sed -i '/status: {}/i\      hostNetwork: true' $K8S_YAML_MODULE_DIR/cc-license-controller-deployment.yaml
       sed -i '/status: {}/i\      dnsPolicy: ClusterFirstWithHostNet' $K8S_YAML_MODULE_DIR/cc-license-controller-deployment.yaml
    fi
    ### add hostNetwork & dnsPolicy field to  freeswitch-emotibot in post convert ###
    if [[ -f "$K8S_YAML_MODULE_DIR/freeswitch-emotibot-deployment.yaml" ]]; then
       sed -i '/status: {}/i\      hostNetwork: true' $K8S_YAML_MODULE_DIR/freeswitch-emotibot-deployment.yaml
       sed -i '/status: {}/i\      dnsPolicy: ClusterFirstWithHostNet' $K8S_YAML_MODULE_DIR/freeswitch-emotibot-deployment.yaml
    fi
    ### add hostNetwork & dnsPolicy field to  unimrcp-emotibot in post convert ###
    if [[ -f "$K8S_YAML_MODULE_DIR/unimrcp-emotibot-deployment.yaml" ]]; then
       sed -i '/status: {}/i\      hostNetwork: true' $K8S_YAML_MODULE_DIR/unimrcp-emotibot-deployment.yaml
       sed -i '/status: {}/i\      dnsPolicy: ClusterFirstWithHostNet' $K8S_YAML_MODULE_DIR/unimrcp-emotibot-deployment.yaml
    fi

    #############################################
    ###  convert yaml Deployment to DaemonSet ###
    #############################################
    ###  convert cc-license-controller yaml Deployment to DaemonSet ###
    if [[ -f "$K8S_YAML_MODULE_DIR/cc-license-controller-deployment.yaml" ]]; then
       sed -i "s/Deployment/DaemonSet/g" $K8S_YAML_MODULE_DIR/cc-license-controller-deployment.yaml
       sed -i '/replicas: 1/d' $K8S_YAML_MODULE_DIR/cc-license-controller-deployment.yaml
    fi
    ###  convert freeswitch-emotibot yaml Deployment to DaemonSet ###
    if [[ -f "$K8S_YAML_MODULE_DIR/freeswitch-emotibot-deployment.yaml" ]]; then
       sed -i "s/Deployment/DaemonSet/g" $K8S_YAML_MODULE_DIR/freeswitch-emotibot-deployment.yaml
       sed -i '/replicas: 1/d' $K8S_YAML_MODULE_DIR/freeswitch-emotibot-deployment.yaml
    fi
    ###  convert unimrcp-emotibot yaml Deployment to DaemonSet ###
    if [[ -f "$K8S_YAML_MODULE_DIR/unimrcp-emotibot-deployment.yaml" ]]; then
       sed -i "s/Deployment/DaemonSet/g" $K8S_YAML_MODULE_DIR/unimrcp-emotibot-deployment.yaml
       sed -i '/replicas: 1/d' $K8S_YAML_MODULE_DIR/unimrcp-emotibot-deployment.yaml
    fi
fi
# ==================================================================
# 4-csa
# ==================================================================
if [[ "$1" == "emotibot/4-csa" ]];then
    ### add time volumeMount field to  all  yaml except csa-assistant-voice-transmission-deployment.yaml|csa-assistant-text-transmission-deployment.yaml|csa-assistant-transmission-application-deployment.yaml|csa-assistant-text-upload-deployment.yaml|csa-assistant-voice-upload-deployment.yaml in post convert ###
    sed -i '/resources: {}/i\        volumeMounts:\n        - mountPath: /etc/localtime\n          name: hosttime' $(ls $K8S_YAML_MODULE_DIR/*deployment.yaml | grep -vE "csa-assistant-voice-transmission-deployment.yaml|csa-assistant-text-transmission-deployment.yaml|csa-assistant-transmission-application-deployment.yaml|csa-assistant-text-upload-deployment.yaml|csa-assistant-voice-upload-deployment.yaml")
    sed -i '/restartPolicy:/i\      volumes:\n       - name: hosttime\n         hostPath:\n           path: /etc/localtime' $(ls $K8S_YAML_MODULE_DIR/*deployment.yaml | grep -vE "csa-assistant-voice-transmission-deployment.yaml|csa-assistant-text-transmission-deployment.yaml|csa-assistant-transmission-application-deployment.yaml|csa-assistant-text-upload-deployment.yaml|csa-assistant-voice-upload-deployment.yaml")

     ### add host volumeMount field to  csa-assistant-voice-transmission in post convert ###
    if [[ -f "$K8S_YAML_MODULE_DIR/csa-assistant-voice-transmission-deployment.yaml" ]]; then
       sed -i '/resources: {}/i\        volumeMounts:\n        - mountPath: /etc/localtime\n          name: hosttime'  $K8S_YAML_MODULE_DIR/csa-assistant-voice-transmission-deployment.yaml
       sed -i '/status: {}/i\      volumes:\n       - name: hosttime\n         hostPath:\n           path: /etc/localtime'  $K8S_YAML_MODULE_DIR/csa-assistant-voice-transmission-deployment.yaml

       sed -i '/resources: {}/i\        - mountPath: /usr/src/assistant-voice-transmission/logs\n          name: logs' $K8S_YAML_MODULE_DIR/csa-assistant-voice-transmission-deployment.yaml
       sed -i '/status: {}/i\       - name: logs\n         persistentVolumeClaim:\n           claimName: csa-assistant-voice-transmission-claim-logs' $K8S_YAML_MODULE_DIR/csa-assistant-voice-transmission-deployment.yaml
    fi

    ### add host volumeMount field to  csa-assistant-text-transmission in post convert ###
    if [[ -f "$K8S_YAML_MODULE_DIR/csa-assistant-text-transmission-deployment.yaml" ]]; then
       sed -i '/resources: {}/i\        volumeMounts:\n        - mountPath: /etc/localtime\n          name: hosttime'  $K8S_YAML_MODULE_DIR/csa-assistant-text-transmission-deployment.yaml
       sed -i '/status: {}/i\      volumes:\n       - name: hosttime\n         hostPath:\n           path: /etc/localtime'  $K8S_YAML_MODULE_DIR/csa-assistant-text-transmission-deployment.yaml

       sed -i '/resources: {}/i\        - mountPath: /usr/src/assistant-text-transmission/logs\n          name: logs' $K8S_YAML_MODULE_DIR/csa-assistant-text-transmission-deployment.yaml
       sed -i '/status: {}/i\       - name: logs\n         persistentVolumeClaim:\n           claimName: csa-assistant-text-transmission-claim-logs' $K8S_YAML_MODULE_DIR/csa-assistant-text-transmission-deployment.yaml
    fi

    ### add host volumeMount field to  csa-assistant-transmission-application in post convert ###
    if [[ -f "$K8S_YAML_MODULE_DIR/csa-assistant-transmission-application-deployment.yaml" ]]; then
       sed -i '/resources: {}/i\        volumeMounts:\n        - mountPath: /etc/localtime\n          name: hosttime'  $K8S_YAML_MODULE_DIR/csa-assistant-transmission-application-deployment.yaml
       sed -i '/status: {}/i\      volumes:\n       - name: hosttime\n         hostPath:\n           path: /etc/localtime'  $K8S_YAML_MODULE_DIR/csa-assistant-transmission-application-deployment.yaml

       sed -i '/resources: {}/i\        - mountPath: /usr/src/assistant-transmission-application/logs\n          name: logs' $K8S_YAML_MODULE_DIR/csa-assistant-transmission-application-deployment.yaml
       sed -i '/status: {}/i\       - name: logs\n         persistentVolumeClaim:\n           claimName: csa-assistant-transmission-application-claim-logs' $K8S_YAML_MODULE_DIR/csa-assistant-transmission-application-deployment.yaml
    fi

    ### add host volumeMount field to  csa-assistant-text-upload in post convert ###
    if [[ -f "$K8S_YAML_MODULE_DIR/csa-assistant-text-upload-deployment.yaml" ]]; then
       sed -i '/resources: {}/i\        volumeMounts:\n        - mountPath: /etc/localtime\n          name: hosttime'  $K8S_YAML_MODULE_DIR/csa-assistant-text-upload-deployment.yaml
       sed -i '/status: {}/i\      volumes:\n       - name: hosttime\n         hostPath:\n           path: /etc/localtime'  $K8S_YAML_MODULE_DIR/csa-assistant-text-upload-deployment.yaml

       sed -i '/resources: {}/i\        - mountPath: /usr/src/assistant-text-upload/text_file\n          name: text' $K8S_YAML_MODULE_DIR/csa-assistant-text-upload-deployment.yaml
       sed -i '/status: {}/i\       - name: text\n         persistentVolumeClaim:\n           claimName: csa-assistant-text-upload-claim-text' $K8S_YAML_MODULE_DIR/csa-assistant-text-upload-deployment.yaml
    fi

    ### add host volumeMount field to  csa-assistant-voice-upload in post convert ###
    if [[ -f "$K8S_YAML_MODULE_DIR/csa-assistant-voice-upload-deployment.yaml" ]]; then
       sed -i '/resources: {}/i\        volumeMounts:\n        - mountPath: /etc/localtime\n          name: hosttime'  $K8S_YAML_MODULE_DIR/csa-assistant-voice-upload-deployment.yaml
       sed -i '/status: {}/i\      volumes:\n       - name: hosttime\n         hostPath:\n           path: /etc/localtime'  $K8S_YAML_MODULE_DIR/csa-assistant-voice-upload-deployment.yaml

       sed -i '/resources: {}/i\        - mountPath: /home/deployer/assistant/upload/file/voice\n          name: voice' $K8S_YAML_MODULE_DIR/csa-assistant-voice-upload-deployment.yaml
       sed -i '/status: {}/i\       - name: voice\n         persistentVolumeClaim:\n           claimName: csa-assistant-voice-upload-claim-voice' $K8S_YAML_MODULE_DIR/csa-assistant-voice-upload-deployment.yaml
    fi
fi
#done

}

k8s_infra_convert_post (){

    WORK_PATH=$(dirname "$0")
    source $WORK_PATH/dev.env
    K8S_YAML_INFRA_DIR=$WORK_PATH/infra/k8s-yamls/infras/statefulsets/

    if [[ "${NFS_PROTOCOL}" == "nfs" ]];then
       K8S_INFRA_VOLUME_DIR=$WORK_PATH/infra/k8s-yamls/infras/volumes/nfs/
    elif [[ "${NFS_PROTOCOL}" == "glusterfs" ]];then
       K8S_INFRA_VOLUME_DIR=$WORK_PATH/infra/k8s-yamls/infras/volumes/gfs/
    fi

    ### replace infra pv volume env ###
    for i in `ls $K8S_INFRA_VOLUME_DIR | grep -v template.yaml`
    do
      cat $K8S_INFRA_VOLUME_DIR/$i > $K8S_INFRA_VOLUME_DIR/$i.yaml
    done
    sed -i "s!\${NFS_SERVER}!$(echo ${NFS_SERVER})!g" $(ls $K8S_INFRA_VOLUME_DIR/*.yaml)
    sed -i "s!\${NFS_PATH}!$(echo ${NFS_PATH})!g" $(ls $K8S_INFRA_VOLUME_DIR/*.yaml)
    sed -i "s!\${NAMESPACE}!$(echo ${NAMESPACE})!g" $(ls $K8S_INFRA_VOLUME_DIR/*.yaml)

    ### replace infra docker image repo ###
    if [[ ! -z "$REGISTRY_REPO" ]]; then
       for i in `ls $K8S_YAML_INFRA_DIR | grep 'statefulset'`
       do
         INFRA_IMAGE=`grep -vE "^[ ]+#"  $K8S_YAML_INFRA_DIR/$i | grep -vE "^#" | grep " image: " | awk '{len=split($0,a," ");print a[len]}'`
         sed -i "s#${INFRA_IMAGE}#${REGISTRY_REPO}/`echo ${INFRA_IMAGE} | awk '{len=split($0,a,"/");print a[len]}'`#g" $K8S_YAML_INFRA_DIR/$i
       done
    fi

}

k8s_efk_up_before (){

    WORK_PATH=$(dirname "$0")
    source $WORK_PATH/dev.env
    K8S_EFK_DIR=$WORK_PATH/infra/k8s-yamls/efk/

    if [[ "${Docker_Root_Dir}" != "/var/lib/docker" ]];then
       sed -i "s#/var/lib/docker#${Docker_Root_Dir}#g" $K8S_EFK_DIR/*
    fi

    ### replace efk docker image repo ###
    if [[ ! -z "$REGISTRY_REPO" ]]; then
       for i in `ls $K8S_EFK_DIR | grep 'yaml'`
       do
         EFK_IMAGE=`grep -vE "^[ ]+#"  $K8S_EFK_DIR/$i | grep -vE "^#" | grep " image: " | awk '{len=split($0,a," ");print a[len]}'`
         for j in $EFK_IMAGE
         do
          sed -i "s#${j}#${REGISTRY_REPO}/`echo ${j} | awk '{len=split($0,a,"/");print a[len]}'`#g" $K8S_EFK_DIR/$i
         done
       done
    fi

}

k8s_monitor_up_before (){

    WORK_PATH=$(dirname "$0")
    K8S_MONITOR_DIR=$WORK_PATH/infra/monitor/

    ### replace monitor docker image repo ###
    if [[ ! -z "$REGISTRY_REPO" ]]; then
       for i in `ls -a $K8S_MONITOR_DIR | grep 'yaml'`
       do
         MONITOR_IMAGE=`grep -vE "^[ ]+#"  $K8S_MONITOR_DIR/$i | grep -vE "^#" | grep " image: " | awk '{len=split($0,a," ");print a[len]}'`
         for j in $MONITOR_IMAGE
         do
          sed -i "s#${j}#${REGISTRY_REPO}/`echo ${j} | awk '{len=split($0,a,"/");print a[len]}'`#g" $K8S_MONITOR_DIR/$i
         done
       done
    fi

    ### delete volume field in agent compose file ###
    sed -i '/- \/usr\/bin\/docker/d' $K8S_MONITOR_DIR/.agent.yaml || true
    sed -i '/- \/lib64\/libltdl.so.7/d' $K8S_MONITOR_DIR/.agent.yaml || true
    sed -i '/- \/var\/run\/docker.sock/d' $K8S_MONITOR_DIR/.agent.yaml || true
    sed -i '/volumes:/d' $K8S_MONITOR_DIR/.agent.yaml || true

}

k8s_monitor_convert_post (){

    WORK_PATH=$(dirname "$0")
    K8S_MONITOR_DIR=$WORK_PATH/infra/monitor/
    ### add host volumeMount field to  agent in post convert ###
    if [[ -f "$K8S_MONITOR_DIR/.agent-k8s.yaml" ]]; then

       sed -i '/resources: {}/i\          volumeMounts:\n          - mountPath: /var/run/docker.sock\n            name: dockersock'  $K8S_MONITOR_DIR/.agent-k8s.yaml
       sed -i '/status:/i\        volumes:\n        - name: dockersock\n          hostPath:\n            path: /var/run/docker.sock'  $K8S_MONITOR_DIR/.agent-k8s.yaml

    fi

}
