#!/bin/bash
  # in-container check
  echo -en "\033[31mrun check env   :    \033[0m"
  if [[ -f /.dockerenv ]]
    then
      echo -e "\033[35mcontainer\033[0m"
  elif [[ `dmesg|grep -i dmi` =~ Virtual ]]
    then
      echo -e "\033[32mVirtual Host\033[0m"
  else
      echo -e "\033[32m hardware\033[0m"
  fi
  # run user
  echo -en "\033[31mrun check user  :    \033[0m"
  id
  # 物理cpu
  echo -en "\033[31mcpu physical    :    \033[0m"
  cat /proc/cpuinfo| grep "physical id"| sort| uniq| wc -l
  # 逻辑cpu
  echo -en "\033[31mprocessor       :    \033[0m"
  cat /proc/cpuinfo| grep "processor"| wc -l
  # cpu 型号
  echo -en "\033[31mcpuinfo         :    \033[0m"
  cat /proc/cpuinfo | grep name |uniq | awk '{print $0}'
  # cpu 指令集
  echo -en "\033[31mcpuflags        :    \033[0m"
  flags=`cat /proc/cpuinfo  | grep flags | uniq | grep avx2 | awk -F ":" '{print $2}'`
  if [[ $flags =~ avx2 ]] ; then echo $flags | grep --color=auto avx2; else echo -e $flags "\033[35mNo found avx2\033[0m" ; fi
  # 磁盘大小
  echo -e "\033[31mdisk info       :    \033[0mDisk > 10G "
  df -B 1g | awk '{if ($2>10 && NR>1){print "                     "$2"G",$NF}}'
  # 磁盘io
  # 是否有共享磁盘
  echo -e "\033[31mshare disk      :    \033[0m"
  df -h | grep ":"
  # 内存信息
  echo -e "\033[31mfree info       :    \033[0mtotal        used        free      shared  buff/cache   available"
  free -h | awk 'NR>1{print "                     "$0}'
  # 系统版本
  echo -en "\033[31mOS              :    \033[0m"
  if [ -f /etc/redhat-release ] ; then cat /etc/redhat-release; fi
  which lsb_release > /dev/null 2>&1 && if `which apt-get > /dev/null 2>&1`; then  lsb_release -ir | awk 'NR>1{print "                     "$0}'; fi
  # 时间
  nowtime=`date`
  if [[ $nowtime =~ CST ]] ; then
    echo -e "\033[31mtime            :    \033[0m$nowtime"
  else
    echo -e "\033[31mtime            :    \033[0m\033[35m$nowtime\033[0m"
  fi
  # kernel
  echo -en "\033[31mkernel          :    \033[0m"
  uname -a
  # 主机名
  echo -en "\033[31mhostname        :    \033[0m"
  hostname
  # getenforce
  echo -en "\033[31mselinux         :    \033[0m"
  getenforce
  # sysctl 关键参数
  echo -e "\033[31msysctl          :    \033[0m"
  sysctl -a 2>&1 | grep vm.max_map_count | awk '{print "                     "$0}'
  sysctl -a 2>&1 | grep net.ipv4.ip_forward | awk '{print "                     "$0}'
  sysctl -a 2>&1 | grep fs.file-max | awk '{print "                     "$0}'
  sysctl -a 2>&1 | grep net.ipv4.tcp_max_tw_buckets | awk '{print "                     "$0}'
  sysctl -a 2>&1 | grep net.netfilter.nf_conntrack_max | awk '{print "                     "$0}'
  # openfile
  echo -en "\033[31mulimit   -n     :    \033[0m"
  ulimit -n
  # 真实ip检查
  netdev=`ip route | grep default | awk '{if(match($0,"'"dev ([^; ]*)"'",a))print a[1]}'`
  echo -e "\033[31mip check        :    \033[0mTotal ip " `ip addr show dev ${netdev} | grep -v "/32"| awk -F "/" '{print $1}' | grep -o -E "[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+" | wc -l`
  ip addr show dev ${netdev} | grep -v "/32"| awk -F "/" '{print $1}' | grep -o -E "[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+" | awk '{print "                     "$0}'
  # ping baidu.com
  echo -en "\033[31mout network     :    \033[0m"
  curl -s www.baidu.com -o /tmp/.tmp.check || pingresult=OFF
  echo ${pingresult:-ON}
  # docker
  echo -en "\033[31mdocker          :    \033[0m"
  which dockerd > /dev/null 2>&1 || dcresult="Not installed"
  echo ${dcresult:-Installed}
  # docker version 
  echo -en "\033[31mdocker version  :    \033[0m"
  if [[ $dcresult != "Not installed" ]] ; then  dockerd -v ; else echo Null; fi
  # 安装了docker并且不是root用户，则检查一下docker权限
  if [[ ! `id -u` -eq 0 ]] ;then echo -en "\033[31mdockerPermission:    \033[0m" ; docker info > /tmp/.tmpcheck 2>&1 ; if [[ `cat /tmp/.tmpcheck` =~ "permission denied" ]] ; then echo "permission denied" ; elif [[ `cat /tmp/.tmpcheck` =~ "Is the docker daemon running?" ]] ; then echo "docker Down"; elif [[ `cat /tmp/.tmpcheck` =~ "Server Version" ]] ; then echo "Ok" ; else echo "Null" ; fi ; fi
  # docker-compose 
  echo -en "\033[31mdocker compose  :    \033[0m"
  which docker-compose > /dev/null 2>&1 || dccompose="Not installed"
  echo ${dccompose:-Installed}
  # docker-compose version
  echo -en "\033[31mcompose version :    \033[0m"
  docker-compose -v  > /dev/null 2>&1 || dccomposever="Null"
  if [[ $dccomposever != "Null" ]] ; then  docker-compose -v ; a=`which docker-compose` ; if [[ `id -u` -eq 0 ]] ; then chmod a+x $a; fi ; else echo Null; fi 
  # k8s check
  echo -en "\033[31mkubectl         :    \033[0m"
  which kubectl > /dev/null 2>&1 || kubecheck="Not installed"
  echo ${kubecheck:-Installed}
  # kubectl version
  echo -e "\033[31mkubectl version :    \033[0m"
  kubectl version > /dev/null 2>&1 || kubectlver="Null"
  if [[ $kubectlver != "Null" ]] ; then kubectl version | awk '{print "                     "$0}' ;  else echo "Connection Error" | awk '{print "                     "$0}' ; fi
  # kubectl node list
  echo -e "\033[31mk8s node list   :    \033[0m"
  kubectl get node 2>&1 | awk '{print "                     "$0}' || echo "Connection Error" | awk '{print "                     "$0}'
  # git
  echo -en "\033[31mgit             :    \033[0m"
  which git > /dev/null 2>&1 || gitck="Not installed"
  echo ${gitck:-Installed}
  # git version
  echo -en "\033[31mgit version     :    \033[0m"
  git --version  > /dev/null 2>&1 || gitv="Null"
  if [[ $gitv != "Null" ]] ; then git --version ; else echo Null; fi
  # which tools
  er="\033[35m" ok="\033[32m" default="\033[0m"
  echo -e "\033[31mcmd tools       :  " $er "Not install    " $ok "Installed    " 
  echo -n "                    "
  applist="ipconfig ip wget curl tar lsof unzip zip iostat vi vim"
  for i in $applist ; do which $i > /dev/null 2>&1 ; if [[ $? -eq 0 ]];then echo -en $ok "$i    " ; else echo -en $er "$i    " ;fi ; done; echo -e $default
  # add user
  if [[ `id -u` -eq 0 ]] ; then
    useradd deployer  > /dev/null 2>&1
    groupadd docker > /dev/null 2>&1
    echo "Emotibot1:deployer" | chpasswd  > /dev/null 2>&1
    gpasswd -a deployer wheel  > /dev/null 2>&1
    gpasswd -a deployer docker  > /dev/null 2>&1
    # Permission
    per1=false
    su - deployer -c "docker info > /tmp/.tmpcheck 2>&1"
    #if [[ $dcresult == "Not installed" ]]; then echo "Not install"; elif $per1 ; then  echo "deployer docker Permission denied" ; else echo "docker Permission Pass" ; fi ; else echo "Not root " ;fi
    if true ;then echo -en "\033[31mdockerPermission:    \033[0m" ; docker info > /tmp/.tmpcheck 2>&1 ; if [[ `cat /tmp/.tmpcheck` =~ "permission denied" ]] ; then echo "permission denied" ; elif [[ `cat /tmp/.tmpcheck` =~ "Is the docker daemon running?" ]] ; then echo "docker Down"; elif [[ `cat /tmp/.tmpcheck` =~ "Server Version" ]] ; then echo "Ok" ; fi ; fi; fi

  #                      如果没有外部，或者没有多机可不填
  # 多机检查
  DB_HOST_1=  DB_HOST_2=  DB_HOST_3=
  # 使用外部mysql
  MYSQL_HOST=  MYSQL_PORT= MYSQL_USER=  MYSQL_PASSWORD= MYSQL_DBNAME=
  # 使用外部es
  ES_HOST=  ES_PORT=  ES_USER=  ES_PASSWORD=
  # 使用外部redis
  REDIS_HOST=  REDIS_PORT=  REDIS_PASSWORD=
  #                                                           example
  #DB_HOST_1=11.0.2.166  DB_HOST_2=11.0.2.194  DB_HOST_3=11.0.2.222
  # 使用外部mysql
  #MYSQL_HOST=11.0.2.166  MYSQL_PORT=3306 MYSQL_USER=root  MYSQL_PASSWORD=password MYSQL_DBNAME=emotibot
  # 使用外部es
  #ES_HOST=172.16.101.120  ES_PORT=9200  ES_USER=elastic  ES_PASSWORD=zGGKZUG4D34gdwkyPCtXCd4KU56b2XtA
  # 使用外部redis
  #REDIS_HOST=172.16.101.120  REDIS_PORT=6379  REDIS_PASSWORD=password
  # db host ping
  if [[ -n $DB_HOST_1 ]]; then echo -en "\033[34mDB_HOST_1 ping  :    \033[0m" ; if `ping $DB_HOST_1 -c 1 > /dev/null 2>&1 || false`; then  echo "True" ; else echo "False" ;fi; fi
  if [[ -n $DB_HOST_2 ]]; then echo -en "\033[34mDB_HOST_2 ping  :    \033[0m" ; if `ping $DB_HOST_1 -c 1 > /dev/null 2>&1 || false`; then  echo "True" ; else echo "False" ;fi; fi
  if [[ -n $DB_HOST_3 ]]; then echo -en "\033[34mDB_HOST_3 ping  :    \033[0m" ; if `ping $DB_HOST_1 -c 1 > /dev/null 2>&1 || false`; then  echo "True" ; else echo "False" ;fi; fi
  # mysql 连接测试
  getmysql="Not connection"
  if [[ -n $MYSQL_HOST && -n $MYSQL_PORT ]]  ; then echo -en "\033[34mget mysql       :    \033[0m" ; ./mysql -u$MYSQL_USER -h$MYSQL_HOST -P$MYSQL_PORT -p$MYSQL_PASSWORD -e "select 1" > /dev/null 2>&1 && getmysql="Connection";echo $getmysql; fi
  # 创建表，创建库测试
  createtable="True";if [[ -n $MYSQL_DBNAME ]] ; then echo -en "\033[34mcreate table    :    \033[0m" ; ./mysql -u$MYSQL_USER -h$MYSQL_HOST -P$MYSQL_PORT -p$MYSQL_PASSWORD $MYSQL_DBNAME -e "create table zheshiyigelinshiceshibiao (id int)" > /dev/null 2>&1 || createtable="False" ; if [[ $createtable == "True" ]]; then ./mysql -u$MYSQL_USER -h$MYSQL_HOST -P$MYSQL_PORT -p$MYSQL_PASSWORD $MYSQL_DBNAME -e "drop table zheshiyigelinshiceshibiao" > /dev/null 2>&1 ;fi; echo $createtable ;fi
  # 测试redis set get
  setredis="True"; if [[ -n $REDIS_HOST && -n $REDIS_PORT && -n $REDIS_PASSWORD ]] ; then  echo -e "\033[34mREDIS info      :    \033[0m" ; ./redis-cli -h $REDIS_HOST -p $REDIS_PORT -a $REDIS_PASSWORD -r 1 -i 1 info  | grep -E "redis_version|role|cluster_enabled" | awk '{print "                     "$0}' ; fi
  # es 测试
  if [[ -n $ES_HOST && -n $ES_PORT ]] ; then echo -en "\033[34mES check        :    \033[0m" ; curl -s -u $ES_USER:$ES_PASSWORD $ES_HOST:$ES_PORT | tr -d " ," | grep number || echo "False"; fi
