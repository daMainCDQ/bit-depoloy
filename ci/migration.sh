#!/bin/bash

WORK_PATH=$(dirname "$0")

#source ${WORK_PATH}/../script/deploy-utils.sh
#
#WORK_PATH=$(dirname "$0")

ENV_FILE=${WORK_PATH}/../dev.env
set -o allexport
source ${ENV_FILE}
set +o allexport

export HOSTNAME=${HOSTNAME}

if [[ ${#MONGO_USER} > 0 ]] && [[ ${#MONGO_PASS} > 0 ]] && [[ ${#MONGO_AUTH_DB} > 0 ]]; then
    export MONGO_URI=mongodb://${MONGO_USER}:${MONGO_PASS}@${MONGO_HOST}:${MONGO_PORT}/${MONGO_AUTH_DB}
else
    export MONGO_URI=mongodb://${MONGO_HOST}:${MONGO_PORT}
fi

if [[ ${#DOMAIN_URL} == 0 ]]; then
    netdev=`ip route | grep default | awk '{print $NF}' || true`
    DOMAIN_URL=$(ip addr show dev ${netdev} | awk -F "/" '{print $1}' | grep -o -E "[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+" || true)

    if [[ $? -ne 0 ]] || [[ ${#DOMAIN_URL} == 0 ]]; then
        echo "[WARNING] Failed to get 'DOMAIN_URL', please set this env in dev.env"
    fi

    echo "Get DOMAIN_URL=$DOMAIN_URL"
    export DOMAIN_URL=${DOMAIN_URL}
fi

#migration ${WORK_PATH}/tool.yaml $@

TOOL_YAML=${WORK_PATH}/../infra/init-db.yaml
MODULE=$@
TMP_INIT_DB_YAML=.init-db.yaml
echo ${MODULE}
docker-compose -f ${TOOL_YAML} config > ${TMP_INIT_DB_YAML}

sed -i "s/INIT_MYSQL_INIT: \"true\"/INIT_MYSQL_INIT: \"false\"/g" ${TMP_INIT_DB_YAML}
sed -i "s/INIT_ES_INIT: \"true\"/INIT_ES_INIT: \"false\"/g" ${TMP_INIT_DB_YAML}
sed -i "s/INIT_ES_LOG_INIT: \"true\"/INIT_ES_LOG_INIT: \"false\"/g" ${TMP_INIT_DB_YAML}
sed -i "s/INIT_MINIO_INIT: \"true\"/INIT_MINIO_INIT: \"false\"/g" ${TMP_INIT_DB_YAML}
sed -i "s/INIT_MODULE_INIT: \"true\"/INIT_MODULE_INIT: \"false\"/g" ${TMP_INIT_DB_YAML}

case "$MODULE" in
    mysql) sed -i "s/INIT_MYSQL_INIT: \"false\"/INIT_MYSQL_INIT: \"true\"/g" ${TMP_INIT_DB_YAML}
    ;;
    es) sed -i "s/INIT_ES_INIT: \"false\"/INIT_ES_INIT: \"true\"/g" ${TMP_INIT_DB_YAML}
    ;;
    minio) sed -i "s/INIT_MINIO_INIT: \"false\"/INIT_MINIO_INIT: \"true\"/g" ${TMP_INIT_DB_YAML}
    ;;
    db) sed -i "s/INIT_MYSQL_INIT: \"false\"/INIT_MYSQL_INIT: \"true\"/g" ${TMP_INIT_DB_YAML}
    sed -i "s/INIT_ES_INIT: \"false\"/INIT_ES_INIT: \"true\"/g" ${TMP_INIT_DB_YAML}
    sed -i "s/INIT_MINIO_INIT: \"false\"/INIT_MINIO_INIT: \"true\"/g" ${TMP_INIT_DB_YAML}
    ;;
    module) sed -i "s/INIT_MODULE_INIT: \"false\"/INIT_MODULE_INIT: \"true\"/g" ${TMP_INIT_DB_YAML}
    ;;
esac

cat ${ENV_FILE}
cat ${TMP_INIT_DB_YAML}

cmd="docker-compose -f ${TMP_INIT_DB_YAML} run --rm 1-bf"

echo ${cmd}
eval ${cmd}
