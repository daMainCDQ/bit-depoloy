#!/bin/bash

GIT_DATE=$(git log HEAD -n1 --pretty='format:%cd' --date=format:'%Y%m%d-%H%M')
GIT_TAG=$(git rev-parse --short=7 HEAD)
TAG="$GIT_TAG-$GIT_DATE"
TS=`date +%s`
DIFF=`git log -r ${GIT_PREVIOUS_SUCCESSFUL_COMMIT}..${GIT_COMMIT} --pretty=format:"### %h(%an) - %s"| sed -e "s/\"/'/g"`

#RED
COLOR="#E06064"

TASK_NAME="$2"
if [[ -z "$2" ]];
then
  # use default task name
  TASK_NAME="emotibot_deploy"
fi

TITLE="Build ${TASK_NAME} $1"
case $1 in
  "success")
    #GREEN
    COLOR="#36a64f"
    ;;
  "failed")
    #RED
    COLOR="#E06064"
    ;;
  # detect no input for compatible
  "")
    #RED
    COLOR="#E06064"
    TITLE="$TITLE failed"
    ;;
  *)
    COLOR="#FFFF00"
    ;;
esac

curl -k -X POST \
  http://192.168.3.84:5521/slack \
  -H 'Authorization: Bearer xoxp-34036332497-62774160423-298721146950-3073151d21b721a34177bae210b8bb74' \
  -H 'Content-Type: application/json' \
  -d "{
  \"username\": \"出錯追殺者\",
  \"channel\": \"cicd\",
  \"icon_url\": \"http://icons.iconarchive.com/icons/papirus-team/papirus-apps/512/builder-icon.png\",
  \"attachments\": [
    {
      \"color\": \"${COLOR}\",
      \"author_name\": \"Jenkins\",
      \"title\": \"${TITLE}\",
      \"title_link\": \"${BUILD_URL}\",
      \"footer\": \"jenkins CI\",
      \"fields\": [
        {
          \"title\": \"COMMIT\",
          \"value\": \"${TAG}\",
          \"short\": true
        },
        {
          \"title\": \"Branch\",
          \"value\": \"${BRANCH_NAME}\",
          \"short\": true
        },
        {
          \"title\": \"Changes\",
          \"value\": \"${DIFF}\",
          \"short\": false
        }
      ],
      \"footer_icon\": \"https://slack-files2.s3-us-west-2.amazonaws.com/avatars/2018-01-10/297462671574_a17ef962a0e918d04820_72.png\",
      \"ts\": ${TS}
    }
  ]
}"
