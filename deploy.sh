#!/bin/bash
path=$(dirname "$0")
cd $path
for i in `ls script/*.sh` ; do source $i ; done
#获取ENV
refresh_env
# 运行准备工作
deploy_befor
# upkeep script check
upkeep_manager $@
# Quiet deployment
#deploy_quiet
# 运行环境检查
run_check
# run k8s
run_k8s $@
# 启动DB
run_db $@
# 开启VIP
vip_deploy
# 打印服务列表 script/banner.sh
banner_list
# 选择服务列表,并根据服务列表创建可执行的yaml文档，执行启动
# 不通过交互模式进行安装部署
select_number $@
# run project module
run_services $@
