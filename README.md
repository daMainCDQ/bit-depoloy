# 部署脚本说明

@(结构描述请查看)[wiki说明](http://wiki.emotibot.com/pages/viewpage.action?pageId=22644288)

- **直接部署**  ：脚本无需指明多机还是单机，可以直接运行，主要依赖dev.env文件
- **快捷部署**  ：执行脚本也可以直接指明部署应用的序号，将在执行安装列表选择时，自动输入，并进行后续操作
- **跳过infrainit**  ：默认或者正常流程中，infra必装，init必须正确完成，但是如果极端环境不需要这两个操作，可以选择跳过
- **外部  DB** ：如果使用外部DB，在dev.env中指向地址就可以，脚本则不再部署及管理这个客户提供的服务，如mysql  elasticsearch
- **管理服务** ：脚本后缀可以直接新增 restart stop rm ps status等做服务管理
- **镜像打包** ：打包需要的image，方便携带，输入目录，自动加载tag.gz文件保存的镜像,也可以反过来直接load
- **日志查看** ：可能需要过多参数及输入，暂时不支持，已完成，只支持交互模式
- **swarm端口** ： 可以直接开放swarm中的端口，而无需修改任何配置
- **--help -h** : 打印帮助信息

-------------------

*  [初始化机器](#初始化机器)
*  [直接部署](#直接部署)
  *  [示例](#示例)
*  [快捷部署](#快捷部署)
*  [跳过infra init](#跳过infrainit)
*  [使用外部DB](#使用外部DB)
*  [管理服务](#管理服务)
*  [镜像打包](#镜像打包)
*  [镜像加载](#镜像加载)
*  [完善与修改](#完善与修改)
*  [开放swarm中端口](#swarm端口)
*  [日志查看](#日志查看)


### 初始化机器

> 使用 root执行脚本，或者sudo执行，则会进行初始化

sudo ./deploy.sh

### 直接部署

> **直接运行deploy.sh 脚本即可**


### 示例
``` python
./deploy.sh
run_check: [##################################################] [100%]  # 环境检查
                                部署单机版                                # 单机多机识别 
[ -------- 1      bf                                         --------] 
[ -------- 2.1    ccbot using fluented as log driver         --------] 
[ -------- 2.2    ccbot using json-file as log driver        --------]  # 同一服务的不同方式
[ -------- 3      aquarius                                   --------] 
[ -------- 4      csa                                        --------]  # 部署列表
[ -------- 5      nlu                                        --------] 
[ -------- 6      qabot                                      --------] 
[ -------- 7      sa                                         --------] 
[ -------- 8      vca                                        --------] 
请输入操作序号，如 1,2.1,4,7                                               # 也支持分隔符 / \ - 等

# 注，init DB，操作不在DB部署时执行，脚本会根据你启动的对应模块，去做相应的init
# 你无需关心有init这一部分操作，由脚本识别并确认是否需要init

```
### 快捷部署

> 直接运行deploy.sh 数字列表  
>  deploy.sh 0 代表安装所有
>  deploy.sh 1,2.2 代表安装1-bf ，2-ccbot的json-file模式  )

``` python
./deploy.sh 1,2.1
请输入操作序号，如 1,2.1,4,7 

1,2.1                                                                 # 自动快捷输入

   检测到 init有变更，或者有错误，正在重新init         init 1-bf            # 需要init则自动执行
   Creating 1-bf ... done
......
deploy emotibot/1-bf
docker-compose -f emotibot/1-bf/.module.yaml up -d                    # 启动module，并打印过程

```
### 跳过infrainit
>  如不想启动infra，或者不想init，或者两者同时都不想执行

``` python
./deploy.sh skip infra                            # 不部署DB
./deploy.sh skip init                             # 不进行init
./deploy.sh skip inra init                        # 不部署DB，也不进行init
```


### 使用外部DB
>  当某个数据库，举例如 mysql  由客户提供时，脚本将不再部署mysql，该场景也包含了DB分离部署的场景
>  dev.env 中 mysql默认地址  ${DB_DEFAULT_IP}，如果被修改，则认为需要DB 模块分离，或者客户提供DB
>  其他 ES  redis 同理

``` python
cat dev.env | grep MYSQL
MYSQL_HOST=22.22.22.22

# 自动部署DB时，将跳过mysql的部署，其他依旧

```

### 管理服务
> 辅助运维及命令简化
> 
``` python
./deploy.sh ps 2       # 查看2-ccbot服务的运行情况，也可以使用 status stats 
          project emotibot/2-ccbot/.module.yaml  
       Name                      Command                  State               Ports         
--------------------------------------------------------------------------------------------
api-gateway           /usr/src/app/entrypoint.sh       Up             0.0.0.0:8085->8085/tcp
call-center-ui        ./entrypoint.sh                  Up (healthy)   0.0.0.0:8000->80/tcp  
call-scheduler        /usr/src/app/entrypoint.sh       Up                                   
callcenter-manager    /usr/src/app/entrypoint.sh       Up             0.0.0.0:9999->9999/tcp
cc-ext-service        /bin/sh -c exec java -Djav ...   Up             0.0.0.0:8881->8881/tcp
config-manager        /usr/src/app/entrypoint.sh       Up             0.0.0.0:7979->7979/tcp

./deploy.sh 1,2,3,4,5      # 全部可以同时查看多个 0代表所有
# 其他参数  
./deploy.sh stop 1           # 停止1-bf
./deploy.sh rm 1             # 删除 1-bf
./deploy.sh restart 1,2,3        # 重启 1-bf,2-ccbot,3-xxx
./deploy.sh start 1          # 启动
./deploy.sh ps infra         # infra  操作db的关键字
```

> 裁剪/降配部署 命令
``` python
./deploy.sh mini      #单独降配模块 内存/cpu使用量 的部署命令
./deploy.sh cut       #单独裁剪模块部署命令，cut参数后支持接如下参数以供指定裁剪模块使用(参数之间以空格分开)
                 ccbot qic emotion faq-lite faq-ft faq auth skill ner taskengine intent kg mlp
./deploy.sh mini cut  #同时降配/裁剪 部署命令
```

> k8s 运维命令
``` python
./deploy.sh k8s-infra rm      #删除infra部分，并且删除外挂卷，可能会导致数据清空，慎用
./deploy.sh k8s-infra up      #创建infra部分，并且创建外挂卷
./deploy.sh k8s-infra restart #仅重启infra部分docker容器，数据保持不动

./deploy.sh k8s-module rm 1-bf     #删除相应产品的module部分，并且删除外挂卷，可能会导致容器持久化数据清空
./deploy.sh k8s-module up 1-bf     #创建相应产品的module部分，并且创建外挂卷
./deploy.sh k8s-module update 1-bf #更新相应产品的module部分（容器已经启动过，使用该命令更新docker image）

./deploy.sh k8s-init-db rm 1-bf    #删除相应产品的init-db
./deploy.sh k8s-init-db up 1-bf    #启动相应产品的init-db

```

### 镜像打包
> 可以直接打包你需要的镜像为tag.gz，方便部署携带
> 可以直接识别目录，加载tar.gz镜像进行导入
``` python
./deploy.sh saveimage 1,2,3       # 将项目 1，2，3使用的镜像进行打包
请输入image path :                # 输入要保存的位置
/tmp/
# 保存的路径及命名格式
/tmp/images/1-20191105-admin-service_749c412-20191022-1453.tar.gz
路径  + images + 项目编号 +日期 + 镜像名称+ 镜像tag + 后缀tag.gz

也可以使用交互方式
./deploy.sh saveimage             # 不输入编号，则进入交互save模式
[ -------- 1      bf                                         --------] 
[ -------- 2.1    ccbot using fluented as log driver         --------] 
[ -------- 2.2    ccbot using json-file as log driver        --------] 
[ -------- 3      aquarius                                   --------] 
[ -------- 4      csa                                        --------] 
[ -------- 5      nlu                                        --------] 
[ -------- 6      qabot                                      --------] 
[ -------- 7      sa                                         --------] 
[ -------- 8      vca                                        --------] 
请输入操作序号，如 1,2.1,4,7      # 你要打包镜像的项目编号
1
input number is 1
请输入image path :                # 同上，你要保存的路径，之后过程与非交互一致

./deploy.sh savecommit oldtag newtag path        # 根据两个tag或者commit的差异进行image打包
./deploy.sh savecommit                           # 同时，也支持交互模式
[deployer@mysql-cluster01 bf2020]$ ./deploy.sh savecommit
old TAG : a
new TAG : b
save path : /tmp/

```
## 镜像加载
``` python
./deploy.sh loadimage /tmp        # 直接指定目录，加载目录下所有tar.gz
./deploy.sh loadimage             # 不输入参数，交互模式输入路径
请输入image path:  
/tmp
load image ... ...
```

## swarm端口
``` python
./deploy.sh expose 14101          # swarm expose 端口14101，服务，内部端口，外部端口，端口冲突，等会自动解决
./deploy.sh expose 1-bf           # 不指定端口，可以在服务列表中，选择全部开放某个服务
./deploy.sh expose                # 也可以交互模式开放某个服务所有端口
                                swarm expose port
[ -------- 1      bf                                         --------] 
[ -------- 2.1    ccbot using fluented as log driver         --------] 
[ -------- 2.2    ccbot using json-file as log driver        --------] 
[ -------- 3      aquarius                                   --------] 
[ -------- 4      csa                                        --------] 
[ -------- 5      nlu                                        --------] 
[ -------- 6      qabot                                      --------] 
[ -------- 7      sa                                         --------] 
[ -------- 8      vca                                        --------] 
请输入操作序号，如 1,2.1,4,7

```
## 日志查看
``` python
./deploy.h log                    # 使用交互模式，选择你要查看的日志，根据提示做选择就可以
```

## 完善与修改
> 脚本还需要后续完善，逐步增加更多功能，方便部署管理，修改后请提交至git
> 待完善功能
> - [x] 日志查看 支持单机，swarm（识别错误与正常程序日志） ，infra查看
> - [x] 自动机器初始化,系统参数配置，seliux，docker config，等如果使用sudo或者root执行才生效
> - [x] 增加DB部署后的检测，失败则给出提示后退出，
- > - [x] mysql
  > - [x] es
  > - [x] consul
  > - [x] mongo
  > - [x] redis
  > - [x] kafka
  > - [x] zookeeper
  > - [ ] minio
> - [x] 单独restart  up 某个容器
> - [ ] 支持DB多机，应用单机docker-compose，并支持负载均衡
> - [ ] db快速备份、恢复
> - [ ] 如果跳过某个DB，还会继续识别未安装，但是安装会继续跳过，永远无法完成
> - [ ] 超过3台本地部署haproxy代理到DB集群
> - [ ] 变量包含固定ip，并且不是${DEFAULT_IP}的，进行报警
> - [x] 部署前 的 check.sh增加时间检查
> - [x] deploy.sh 增加时间检测
> - [x] 增加onlyinit功能
> - [x] 镜像自动打包
> - [x] 镜像自动加载
> - [x] 打包没有infra目录，等待添加
> - [x] swarm 部署
> - [x] swarm 部署 单独开放某个端口出来
> - [x] swarm 部署 全部开放所有端口出来
> - [x] 根据两个git commit 打包镜像
> - [x] save image 的时候，如果中断，会全部从新打包，等待增加增量识别，打好的直接跳过
> - [ ] 在k8s环境中部署
> - [ ] 自动安装依赖，如docker docker-compose
> - [x] 配套zabbix监控，使用非10050端口，避开可能的与客户监控冲突端口
- > - [x] 基础信息采集，内存，硬盘，swap，iowait，cpu，流量等其他，需要人工精剪默认模板
  > - [ ] 应用系统信息采集，获取各个关键模块，或者所有模块的web get 的healthcheck
  > - [ ] 数据库信息采集，如mysql ，es ，redis，mongo，等集群状态监视
  > - [ ] swarm 或者 k8s 状态监控，包含容器数量与定义的副本数量是否一致，其他没想好
> - [ ] 自动部署vip地址
> - [ ] swarm部署时onlyup不生效

帮助：郭鹤阳  <heyangguo@emotibot.com>

---------
感谢阅读这份帮助文档，并协助完成更多部署管理支持。
