#!/bin/bash

if [[ "${INIT_DB_DEBUG}" == 'true' ]]; then
  set -x
fi

# Try to import emotion corpora files to emotion engine
if [[ "$1" == 'eeimporter' ]]; then
  ee_host="${EE_HOST:-172.17.0.1}"
  ee_port="${EE_PORT:-8888}"
  timeout="${TIMEOUT:-30}"
  elapsed_secs='0'
  emotion_corpora_dir="${2:-/emotion-files}"
  if [[ ! -d "${emotion_corpora_dir}" ]] || \
    [[ "$(ls "${emotion_corpora_dir}")" == '' ]]; then
    exit 0
  fi
  echo -n "Waiting ${timeout} seconds for the emotion engine to start"
  until eeimporter -addr "${ee_host}:${ee_port}" &>/dev/null \
    || [[ "${elapsed_secs}" -gt "${timeout}" ]]; do
    sleep 1
    echo -n '.'
    ((elapsed_secs++))
  done
  echo ''
  if  [[ "${elapsed_secs}" -le "${timeout}" ]]; then
    echo "The emotion engine has started"
    eeimporter -addr "${ee_host}:${ee_port}" -f "${emotion_corpora_dir}"
    exit "$?"
  else
    echo "Timeout: the emotion engine has not started, skip importing emotions" 1>&2
    exit 1
  fi
fi

if [ "${INIT_MYSQL_INIT}" == "true" ] || [ "${INIT_MYSQL_INIT}" == "TRUE" ] ; then
    echo "# =================================================================="
    echo "# sql"
    echo "# =================================================================="
    cd /usr/bin/app/sql && ./up.sh development
    
    if [[ $? -ne 0 ]]; then
        echo "# Fail to migrate sql"
        exit -1
    fi

    
fi


if [ "${INIT_ES_INIT}" == "true" ] || [ "${INIT_ES_INIT}" == "TRUE" ] ; then
    echo "# =================================================================="
    echo "# elasticsearch"
    echo "# =================================================================="
    cd /usr/bin/app/elasticsearch && ./entrypoint.sh
    
    if [[ $? -ne 0 ]]; then
        echo "# Fail to migrate elasticsearch"
        exit -1
    fi
fi


if [ "${INIT_MINIO_INIT}" == "true" ] || [ "${INIT_MINIO_INIT}" == "TRUE" ] ; then
    echo "# =================================================================="
    echo "# minio"
    echo "# =================================================================="
    cd /usr/bin/app/minio && ./entrypoint.sh
    
    if [[ $? -ne 0 ]]; then
        echo "# Fail to migrate minio"
        exit -1
    fi
fi

if [ "${INIT_MODULE_INIT}" == "true" ] || [ "${INIT_MODULE_INIT}" == "TRUE" ] ; then
    echo "# =================================================================="
    echo "# module"
    echo "# =================================================================="
    cd /usr/bin/app/module && ./entrypoint.sh
    
    if [[ $? -ne 0 ]]; then
        echo "# Fail to migrate modules"
        exit -1
    fi
fi



