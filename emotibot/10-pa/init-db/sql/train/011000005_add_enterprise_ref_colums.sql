-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `train`.`organ_course_ref` 
ADD COLUMN `organization_id` bigint(21) UNSIGNED NOT NULL DEFAULT 0 COMMENT '直属组织' ;

ALTER TABLE `organ_course_ref`
ADD COLUMN enterprise_uuid CHAR(32);

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `train`.`organ_course_ref`  DROP COLUMN `organization_id`;
ALTER TABLE `train`.`organ_course_ref`  DROP COLUMN `enterprise_uuid`;