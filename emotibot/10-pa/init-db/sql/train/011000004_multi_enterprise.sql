-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `train`.`course` 
ADD COLUMN `enterprise` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户所属企业';

ALTER TABLE `train`.`organization` 
ADD COLUMN `enterprise` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属企业';

ALTER TABLE `train`.`organization` 
ADD COLUMN `creater_uuid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `train`.`course`  DROP COLUMN `enterprise`;
ALTER TABLE `train`.`organization`  DROP COLUMN `enterprise`;