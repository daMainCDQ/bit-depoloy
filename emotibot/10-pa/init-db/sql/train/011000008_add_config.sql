-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE IF NOT EXISTS `score_weight_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '规则名称',
  `creater` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `info` json DEFAULT NULL COMMENT 'json格式的各规则占比',
  `num` int(11) DEFAULT NULL COMMENT '排序字段',
  `enterprise` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '企业id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='分数权重配置';

CREATE TABLE IF NOT EXISTS `speech_service_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(20) DEFAULT NULL,
  `creater` varchar(32) DEFAULT NULL COMMENT '创建人UUID',
  `create_time` datetime DEFAULT NULL,
  `info` json DEFAULT NULL COMMENT '明细',
  `type` int(1) DEFAULT NULL,
  `enterprise` char(32) DEFAULT NULL COMMENT '用户所属企业UUID',
  `supplier` varchar(32) DEFAULT NULL COMMENT '语音服务厂商',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='语音服务配置';

ALTER TABLE `train`.`course` 
ADD COLUMN `asr_id` int(11) NULL COMMENT 'asr服务id',
ADD COLUMN `tts_id` int(11) NULL COMMENT 'tts服务的id',
ADD COLUMN `score_weight_config_id` int(11) NULL COMMENT '分数权重的id' ;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE IF EXISTS `score_weight_config`;
DROP TABLE IF EXISTS `speech_service_config`;
ALTER TABLE `train`.`course`  DROP COLUMN `asr_id`, DROP COLUMN `asrtts_id_id`, DROP COLUMN `score_weight_config_id`,;