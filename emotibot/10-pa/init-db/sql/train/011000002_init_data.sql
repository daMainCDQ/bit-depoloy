-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
INSERT INTO `train`.`users` (`uuid`, `display_name`, `user_name`, `email`, `phone`, `enterprise`, `type`, `password`, `status`, `created_time`)
VALUES
  ('4b21158a395311e88a710242ac110003','CSBOT','csbotadmin','csbotadmin@emotibot.com','','bb3e3925f0ad11e7bd860242ac120003',1,'ac04367d3155bb651df2e4220bdb8303',1,'2018-06-12 17:23:58');

INSERT INTO `train`.`organization`( `ORGAN_NAME`, `ORGAN_CODE`, `PARENT_ORGAN_CODE`, `CREATE_TIME`, `IS_DELETE`) VALUES ( '总公司', '00', 'root', '2019-11-15 17:46:29', 0);
INSERT INTO `train`.`organization`( `ORGAN_NAME`, `ORGAN_CODE`, `PARENT_ORGAN_CODE`, `CREATE_TIME`, `IS_DELETE`) VALUES ( '总部', '0000', '00', '2019-11-15 17:46:29', 0);

INSERT INTO `train`.`user_privileges`( `human`, `machine`, `role`) VALUES ( '4b21158a395311e88a710242ac110003', 'csbot', 'tableDirector');
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

