-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `train`.`users` 
ADD COLUMN `organization` bigint(21) UNSIGNED NOT NULL DEFAULT 0 COMMENT '组织id' ;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `train`.`users`  DROP COLUMN `organization`;