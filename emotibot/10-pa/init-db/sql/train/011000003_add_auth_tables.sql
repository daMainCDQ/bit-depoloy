-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE IF NOT EXISTS `enterprises` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `uuid` char(32) NOT NULL DEFAULT '' COMMENT '企业id',
  `parent_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '上级组织序号',
  `level` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '组织层级',
  `name` varchar(64) NOT NULL DEFAULT '' COMMENT '企业名称',
  `secret` char(72) NOT NULL DEFAULT '' COMMENT '企业所属 secret key',
  `description` varchar(200) NOT NULL DEFAULT '' COMMENT '企业描述',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '企业状态 0: disable, 1: active',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立日期',
  PRIMARY KEY (`id`),
  KEY `idx_uuid` (`uuid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='企业列表';

CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `uuid` char(32) NOT NULL DEFAULT '' COMMENT '角色id',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '权限角色名称',
  `enterprise` char(32) NOT NULL DEFAULT '' COMMENT '所属企业',
  `description` varchar(200) NOT NULL DEFAULT '' COMMENT '权限角色描述',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立日期',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uuid` (`uuid`) USING BTREE,
  KEY `enterprise of role` (`enterprise`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='权限角色列表';



-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE IF EXISTS `enterprises`;
DROP TABLE IF EXISTS `roles`;

