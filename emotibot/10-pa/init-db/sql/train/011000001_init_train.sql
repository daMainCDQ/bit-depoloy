-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE DATABASE IF NOT EXISTS `train` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

USE `train`;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for course
-- ----------------------------
CREATE TABLE IF NOT EXISTS `course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '课程名字，质检系统随路字段service_type关联字段',
  `level` varchar(255) DEFAULT NULL COMMENT '等级',
  `channel` varchar(255) DEFAULT NULL COMMENT '渠道',
  `app_id` varchar(255) DEFAULT NULL COMMENT '机器人id',
  `type` int(255) DEFAULT NULL COMMENT '类型（1=问答，2=情景对话，3=跟读）',
  `is_exam` int(255) DEFAULT NULL COMMENT '加入考试（1=练习课程，2=练习、考试课程）',
  `app_url` varchar(255) DEFAULT NULL COMMENT '访问机器人url',
  `creater_uuid` varchar(255) DEFAULT NULL,
  `creater_name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `practice_time_limit` int(11) DEFAULT '30',
  `require_limit` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for exam_result
-- ----------------------------
CREATE TABLE IF NOT EXISTS `exam_result` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_uuid` varchar(32) DEFAULT NULL,
  `start_time` bigint(20) DEFAULT NULL,
  `end_time` bigint(20) DEFAULT NULL,
  `score` int(3) DEFAULT '0',
  `course_id` int(255) DEFAULT NULL,
  `date` varchar(10) DEFAULT NULL,
  `organ_id` int(11) DEFAULT NULL,
  `user_display_name` varchar(255) DEFAULT NULL,
  `call_uuid` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for module_group
-- ----------------------------
CREATE TABLE IF NOT EXISTS `module_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `code` char(32) NOT NULL DEFAULT '' COMMENT '权限群组代号',
  `zh_cn` varchar(36) NOT NULL DEFAULT '' COMMENT '权限群组名字简中',
  `zh_tw` varchar(36) NOT NULL DEFAULT '' COMMENT '权限群组名字繁中',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立日期',
  `product` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `app exist` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='机器人列表';

-- ----------------------------
-- Table structure for modules
-- ----------------------------
CREATE TABLE IF NOT EXISTS `modules` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `code` char(32) NOT NULL DEFAULT '' COMMENT '程式码或frontend中的代码',
  `name` varchar(36) NOT NULL DEFAULT '' COMMENT '模组名称 但目前frontend不使用此栏位',
  `enterprise` char(32) DEFAULT '' COMMENT '0: 表示系统预设模组, 不为0: 该企业有自订该模组的开关或开放的权限',
  `cmd_list` char(255) NOT NULL COMMENT '该模组所有的操作列表 ex: view, create, edit, export, import, delete, 该栏位会影响前端权限群组显示',
  `description` varchar(200) NOT NULL DEFAULT '' COMMENT '模组描述',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '系统预设该模组开关状态',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立日期',
  `group` char(32) DEFAULT NULL COMMENT '该模组所属群组',
  `product` bigint(20) DEFAULT NULL COMMENT '该模组所属产品',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `enterprise of modules` (`enterprise`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='模组与权限定义';

-- ----------------------------
-- Table structure for organ_course_ref
-- ----------------------------
CREATE TABLE IF NOT EXISTS `organ_course_ref` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organ_code` varchar(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for organization
-- ----------------------------
CREATE TABLE IF NOT EXISTS `organization` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ORGAN_NAME` varchar(128) NOT NULL COMMENT '机构名',
  `ORGAN_CODE` varchar(20) NOT NULL COMMENT '机构代码',
  `PARENT_ORGAN_CODE` varchar(20) NOT NULL COMMENT '父级机构代码',
  `CREATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `IS_DELETE` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=未删除,1=已删除',
  PRIMARY KEY (`ID`),
  KEY `index_organ_code` (`ORGAN_CODE`) USING BTREE,
  KEY `index_parent_organ_code` (`PARENT_ORGAN_CODE`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='组织机构';

-- ----------------------------
-- Table structure for practice_result
-- ----------------------------
CREATE TABLE IF NOT EXISTS `practice_result` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_uuid` varchar(32) DEFAULT NULL,
  `start_time` bigint(20) DEFAULT NULL,
  `end_time` bigint(20) DEFAULT NULL,
  `score` int(3) DEFAULT '0',
  `date` varchar(10) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `user_display_name` varchar(255) DEFAULT NULL,
  `language_ability` int(20) DEFAULT '0' COMMENT '话术熟练度',
  `affinity_ability` int(20) DEFAULT '0' COMMENT '亲和力',
  `logic_ability` int(20) DEFAULT '0' COMMENT '逻辑能力',
  `skill_ability` int(20) DEFAULT '0' COMMENT '技巧能力',
  `reaction_ability` int(20) DEFAULT '0' COMMENT '反应能力',
  `call_uuid` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for privileges
-- ----------------------------
CREATE TABLE IF NOT EXISTS `privileges` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `role` bigint(20) NOT NULL COMMENT '角色id',
  `module` bigint(20) NOT NULL COMMENT '模组id',
  `cmd_list` char(255) NOT NULL DEFAULT '' COMMENT '拥有模组的哪些权限',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立日期',
  PRIMARY KEY (`id`),
  KEY `id of role` (`role`),
  KEY `id of module` (`module`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='角色拥有的模组';

-- ----------------------------
-- Table structure for product
-- ----------------------------
CREATE TABLE IF NOT EXISTS `product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `secret` char(255) NOT NULL DEFAULT '' COMMENT 'oauth 认证使用 secret',
  `redirect_uri` char(255) NOT NULL DEFAULT '' COMMENT 'oauth 认证转址位置',
  `zh_cn` char(255) NOT NULL DEFAULT '' COMMENT '模组名称(简)',
  `zh_tw` char(255) NOT NULL DEFAULT '' COMMENT '模组名称(繁)',
  `code` varchar(64) NOT NULL DEFAULT '' COMMENT '产品 code',
  `create_time` bigint(20) NOT NULL COMMENT '创建时间',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '产品启用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='Auth 模组产品列表';

-- ----------------------------
-- Table structure for user_detail
-- ----------------------------
CREATE TABLE IF NOT EXISTS `user_detail` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `USER_CODE` varchar(32) NOT NULL COMMENT '用户编号',
  `CTI_CODE` varchar(32) DEFAULT NULL COMMENT '通话和用户关联',
  `SOFT_PHONE` varchar(32) DEFAULT NULL COMMENT '坐席软电话号',
  `ORGAN_CODE` varchar(32) DEFAULT NULL COMMENT '坐席归属机构编号',
  `ORGAN_NAME` varchar(128) DEFAULT NULL COMMENT '坐席归属机构',
  `DIC_KEY` varchar(32) DEFAULT NULL COMMENT '序列编号',
  `DIC_NAME` varchar(64) DEFAULT NULL COMMENT '序列名称',
  `USER_NAME` varchar(64) DEFAULT NULL COMMENT '用户名称',
  `USER_UUID` varchar(32) DEFAULT NULL,
  `CREATE_TIME` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  KEY `index_user_code` (`USER_CODE`) USING BTREE,
  KEY `index_organ_code` (`ORGAN_CODE`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='用户详情';

-- ----------------------------
-- Table structure for user_privileges
-- ----------------------------
CREATE TABLE IF NOT EXISTS `user_privileges` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `human` char(32) NOT NULL DEFAULT '' COMMENT 'Human id',
  `machine` char(32) NOT NULL DEFAULT '' COMMENT 'Machine id',
  `role` char(32) NOT NULL DEFAULT '' COMMENT 'Human id 对定到machine id的角色权限',
  PRIMARY KEY (`id`),
  KEY `human exist` (`human`),
  KEY `machine exist` (`machine`),
  KEY `role exist` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='用户权限';

-- ----------------------------
-- Table structure for users
-- ----------------------------
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `uuid` char(32) NOT NULL DEFAULT '' COMMENT '帐号id',
  `display_name` varchar(64) NOT NULL DEFAULT '' COMMENT '显示名称',
  `user_name` char(32) NOT NULL DEFAULT '' COMMENT '用户帐号',
  `email` char(255) NOT NULL DEFAULT '' COMMENT '用户email',
  `phone` char(20) NOT NULL DEFAULT '' COMMENT '用户电话',
  `enterprise` char(32) DEFAULT NULL COMMENT '用户所属企业, 若为空则type必须为0',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '2' COMMENT '0: 系统管理员, 1: 企业管理员, 2: 一般使用者',
  `password` char(32) NOT NULL DEFAULT '' COMMENT 'Password in MD5 after aes 128',
  `product` char(64) NOT NULL DEFAULT '' COMMENT '建立日期',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '保留栏位',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '状态',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  KEY `enterprise of user` (`enterprise`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='帐号列表';

SET FOREIGN_KEY_CHECKS = 1;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP DATABASE `train`;
