#!/bin/bash

COLOR_REST='\033[0m'
COLOR_RED='\033[0;31m'
COLOR_BOLD='\033[0;32m'
# build a image based on folder
function build_docker() {
    echo -e "${COLOR_BOLD}"
    printf '=%.0s' {1..15}
    printf "Building image from $1"
    printf '=%.0s' {1..15}
    printf '\n'
    echo -e "${COLOR_REST}"
    cd "$1" || return 1
    if [ ! -f "./build.env" ]; then
        echo -e "${COLOR_RED}need build.env for building image${COLOR_REST}" >&2
        return 1
    fi
    # shellcheck source=/dev/null
    source ./build.env
    buildArg=""
    # Caputre the build-arg from var start with ARG_*
    for e in $(set -o posix ; set | less);
    do
        if [[ $e =~ ^ARG_* ]]; then
            buildArg="$buildArg --build-arg $e"
        fi
    done
    valid_config "$1/docker-compose.yml"
    #Build the image
    docker-compose -f "$1/docker-compose.yml" build $buildArg || return 1
    echo $IMAGE_NAME

    return 0
}


# Validate the Compose file to ensure no environment is missing
function valid_config() {
    if [ -z "$1" ]; then
        echo "Please specify the path of compose file."
        return 64
    fi
    set +e
    text=$(docker-compose -f $1 config 2>&1)
    exitCode=$?
    set -e
    if [[ $exitCode -ne 0 || $(echo "$text" | grep -c 'Defaulting to a blank string') -ne 0 ]]; then
        echo -e "${COLOR_RED}something wrong in your dockercompose file:\n${COLOR_REST} $text" >&2
        return 1
    fi
}
