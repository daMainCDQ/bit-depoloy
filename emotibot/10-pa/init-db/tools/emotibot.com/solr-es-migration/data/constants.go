package data

const ESQACoreTemplate = "emotibot_qa_core_template"
const ESQACoreTemplateFile = "./elasticsearch/templates/qa_core_template.json"
const ESQACoreIndex = "emotibot-qa-core"
const ESQACoreType = "doc"

const COLLECTION_3RD_CORE = "3rd_core"
const COLLECTION_MERGE_6_25 = "merge_6_25"
