#!/bin/bash

#----------单机---------
#只安装质检
function deploye_only(){
    BASE_YAML_PATH=$1
    OUTPUT_PATH=$2
    # 配置logging start
    more $BASE_YAML_PATH/module_log_driver.yaml > $BASE_YAML_PATH/.module_log_driver.yaml
    # 配置logging end
    SOURCE_MODULE_YAML=$BASE_YAML_PATH/module.yaml
    export MODULE_YAML=$OUTPUT_PATH/.module.yaml

    PORT_YAML=$BASE_YAML_PATH/port.yaml

    set -e

    docker-compose \
      -f $SOURCE_MODULE_YAML \
      -f $PORT_YAML \
      $QABOT_PROFILE_OPTS \
      config > $MODULE_YAML

     if [ $? -ne 0 ]; then
    echo "failed"
    else
    echo "succeed"
    fi
    set +e
}
#安装质检+分析
function deploye_with_analysis(){
    BASE_YAML_PATH=$1
    OUTPUT_PATH=$2
    # 配置logging start
    more $BASE_YAML_PATH/module_log_driver.yaml > $BASE_YAML_PATH/.module_log_driver.yaml
    # 配置logging end
    SOURCE_MODULE_YAML=$BASE_YAML_PATH/module.yaml
    export MODULE_YAML=$OUTPUT_PATH/.module.yaml

    
    ANALYSIS_YAML=$BASE_YAML_PATH/analysis-and-report/module-analysis.yaml

    PORT_YAML=$BASE_YAML_PATH/port.yaml
    PORT_ANALYSIS_YAML=$BASE_YAML_PATH/analysis-and-report/module-analysis-port.yaml

    set -e

    docker-compose \
      -f $SOURCE_MODULE_YAML \
      -f $ANALYSIS_YAML \
      -f $PORT_YAML \
      -f $PORT_ANALYSIS_YAML \
      $QABOT_PROFILE_OPTS \
      config > $MODULE_YAML

     if [ $? -ne 0 ]; then
    echo "failed"
    else
    echo "succeed"
    fi
    set +e


}
#安装质检+报表
function deploye_with_report(){
    BASE_YAML_PATH=$1
    OUTPUT_PATH=$2

    # 配置logging start
    more $BASE_YAML_PATH/module_log_driver.yaml > $BASE_YAML_PATH/.module_log_driver.yaml
    sed -n '2,1000p' $BASE_YAML_PATH/analysis-and-report/module_log_driver_report.yaml >> $BASE_YAML_PATH/.module_log_driver.yaml
    # 配置logging end

    SOURCE_MODULE_YAML=$BASE_YAML_PATH/module.yaml
    export MODULE_YAML=$OUTPUT_PATH/.module.yaml

    
    REPORT_YAML=$BASE_YAML_PATH/analysis-and-report/module-report.yaml

    PORT_YAML=$BASE_YAML_PATH/port.yaml
    PORT_REPORT_YAML=$BASE_YAML_PATH/analysis-and-report/module-report-port.yaml

    set -e

    docker-compose \
      -f $SOURCE_MODULE_YAML \
      -f $REPORT_YAML \
      -f $PORT_YAML \
      -f $PORT_REPORT_YAML \
      $QABOT_PROFILE_OPTS \
      config > $MODULE_YAML

     if [ $? -ne 0 ]; then
    echo "failed"
    else
    echo "succeed"
    fi
    set +e


}
#安装质检+分析+报表
function deploye_with_analysis_and_report(){
    BASE_YAML_PATH=$1
    OUTPUT_PATH=$2

    # 配置logging start
    more $BASE_YAML_PATH/module_log_driver.yaml > $BASE_YAML_PATH/.module_log_driver.yaml
    sed -n '2,1000p' $BASE_YAML_PATH/analysis-and-report/module_log_driver_report.yaml >> $BASE_YAML_PATH/.module_log_driver.yaml
    # 配置logging end

    SOURCE_MODULE_YAML=$BASE_YAML_PATH/module.yaml
    export MODULE_YAML=$OUTPUT_PATH/.module.yaml

    
    ANALYSIS_YAML=$BASE_YAML_PATH/analysis-and-report/module-analysis.yaml
    REPORT_YAML=$BASE_YAML_PATH/analysis-and-report/module-report.yaml

    PORT_YAML=$BASE_YAML_PATH/port.yaml
    PORT_ANALYSIS_YAML=$BASE_YAML_PATH/analysis-and-report/module-analysis-port.yaml
    PORT_REPORT_YAML=$BASE_YAML_PATH/analysis-and-report/module-report-port.yaml

    set -e

    docker-compose \
      -f $SOURCE_MODULE_YAML \
      -f $ANALYSIS_YAML \
      -f $REPORT_YAML \
      -f $PORT_YAML \
      -f $PORT_ANALYSIS_YAML \
      -f $PORT_REPORT_YAML \
      $QABOT_PROFILE_OPTS \
      config > $MODULE_YAML

     if [ $? -ne 0 ]; then
    echo "failed"
    else
    echo "succeed"
    fi
    set +e


}
#----------多机---------
#只安装质检
function deploye_only_swarm(){
    BASE_YAML_PATH=$1
    OUTPUT_PATH=$2

    # 输出日志挂载 start
    more $BASE_YAML_PATH/module_log_driver.yaml > $BASE_YAML_PATH/.module_log_driver.yaml
    # 输出日志挂载 end

    SOURCE_MODULE_YAML=$BASE_YAML_PATH/module.yaml
    export MODULE_YAML=$OUTPUT_PATH/.module.yaml

    PORT_YAML=$BASE_YAML_PATH/port.yaml

    SWARM_YAML=$BASE_YAML_PATH/swarm.yaml

    set -e

    docker-compose \
      -f $SOURCE_MODULE_YAML \
      -f $SWARM_YAML \
      $QABOT_PROFILE_OPTS \
      config > $MODULE_YAML

     if [ $? -ne 0 ]; then
    echo "failed"
    else
    echo "succeed"
    fi
    set +e
}
#安装质检+分析
function deploye_with_analysis_swarm(){
    BASE_YAML_PATH=$1
    OUTPUT_PATH=$2

    # 输出日志挂载 start
    more $BASE_YAML_PATH/module_log_driver.yaml > $BASE_YAML_PATH/.module_log_driver.yaml
    # 输出日志挂载 end

    SOURCE_MODULE_YAML=$BASE_YAML_PATH/module.yaml
    export MODULE_YAML=$OUTPUT_PATH/.module.yaml

    
    ANALYSIS_YAML=$BASE_YAML_PATH/analysis-and-report/module-analysis.yaml

    PORT_YAML=$BASE_YAML_PATH/port.yaml
    PORT_ANALYSIS_YAML=$BASE_YAML_PATH/analysis-and-report/module-analysis-port.yaml

    SWARM_YAML=$BASE_YAML_PATH/swarm.yaml
    SWARM_ANALYSIS_YAML=$BASE_YAML_PATH/analysis-and-report/module-analysis-swarm.yaml

    set -e

    docker-compose \
      -f $SOURCE_MODULE_YAML \
      -f $ANALYSIS_YAML \
      -f $SWARM_YAML \
      -f $SWARM_ANALYSIS_YAML \
      $QABOT_PROFILE_OPTS \
      config > $MODULE_YAML

     if [ $? -ne 0 ]; then
    echo "failed"
    else
    echo "succeed"
    fi
    set +e


}
#安装质检+报表
function deploye_with_report_swarm(){
    BASE_YAML_PATH=$1
    OUTPUT_PATH=$2

    # 配置logging start
    more $BASE_YAML_PATH/module_log_driver.yaml> $BASE_YAML_PATH/.module_log_driver.yaml
    sed -n '2,1000p' $BASE_YAML_PATH/analysis-and-report/module_log_driver_report.yaml >> $BASE_YAML_PATH/.module_log_driver.yaml
    # 配置logging end

    SOURCE_MODULE_YAML=$BASE_YAML_PATH/module.yaml
    export MODULE_YAML=$OUTPUT_PATH/.module.yaml

    
    REPORT_YAML=$BASE_YAML_PATH/analysis-and-report/module-report.yaml

    PORT_YAML=$BASE_YAML_PATH/port.yaml
    PORT_REPORT_YAML=$BASE_YAML_PATH/analysis-and-report/module-report-port.yaml

    SWARM_YAML=$BASE_YAML_PATH/swarm.yaml
    SWARM_REPORT_YAML=$BASE_YAML_PATH/analysis-and-report/module-report-swarm.yaml

    set -e

    docker-compose \
      -f $SOURCE_MODULE_YAML \
      -f $REPORT_YAML \
      -f $SWARM_YAML \
      -f $SWARM_REPORT_YAML \
      $QABOT_PROFILE_OPTS \
      config > $MODULE_YAML

     if [ $? -ne 0 ]; then
    echo "failed"
    else
    echo "succeed"
    fi
    set +e


}
#安装质检+分析+报表
function deploye_with_analysis_and_report_swarm(){
    BASE_YAML_PATH=$1
    OUTPUT_PATH=$2

    # 配置logging start
    more $BASE_YAML_PATH/module_log_driver.yaml> $BASE_YAML_PATH/.module_log_driver.yaml
    sed -n '2,1000p' $BASE_YAML_PATH/analysis-and-report/module_log_driver_report.yaml >> $BASE_YAML_PATH/.module_log_driver.yaml
    # 配置logging end

    SOURCE_MODULE_YAML=$BASE_YAML_PATH/module.yaml
    export MODULE_YAML=$OUTPUT_PATH/.module.yaml

    
    ANALYSIS_YAML=$BASE_YAML_PATH/analysis-and-report/module-analysis.yaml
    REPORT_YAML=$BASE_YAML_PATH/analysis-and-report/module-report.yaml

    PORT_YAML=$BASE_YAML_PATH/port.yaml
    PORT_ANALYSIS_YAML=$BASE_YAML_PATH/analysis-and-report/module-analysis-port.yaml
    PORT_REPORT_YAML=$BASE_YAML_PATH/analysis-and-report/module-report-port.yaml

    SWARM_YAML=$BASE_YAML_PATH/swarm.yaml
    SWARM_ANALYSIS_YAML=$BASE_YAML_PATH/analysis-and-report/module-analysis-swarm.yaml
    SWARM_REPORT_YAML=$BASE_YAML_PATH/analysis-and-report/module-report-swarm.yaml

    set -e

    docker-compose \
      -f $SOURCE_MODULE_YAML \
      -f $ANALYSIS_YAML \
      -f $REPORT_YAML \
      -f $SWARM_YAML  \
      -f $SWARM_ANALYSIS_YAML \
      -f $SWARM_REPORT_YAML \
      $QABOT_PROFILE_OPTS \
      config > $MODULE_YAML

     if [ $? -ne 0 ]; then
    echo "failed"
    else
    echo "succeed"
    fi
    set +e


}

function exec_calsim(){
    docker run -d --privileged --name cal-sum --restart=always -p 22106:8886 docker-reg.emotibot.com.cn:55688/calsim:v1
}
