#!/bin/bash

WORKDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

function main(){
  bash "${WORKDIR}/eeimport/eeimporter.sh"
}

main "$@"
