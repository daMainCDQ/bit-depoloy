#!/bin/bash

WORK_PATH=$(dirname "$0")

source ${WORK_PATH}/analysis-and-report/deploy-utils.sh


# If the profile is in directory 'emotibot/6-qabot/profiles',
# it can enable inheritance mode.

# e.g. 'export QABOT_PROFILE=asr-ali/asr-172.16.100.58.yaml' means
# '-f /path/to/emotibot/6-qabot/profiles/asr-ali.yaml \
# -f /path/to/emotibot/6-qabot/profiles/asr-ali/asr-172.16.100.58.yaml'
# for docker-compose.
function get_profile_opts(){
  local profile_name="$1"
  local profile_base_path="$(cd "$(dirname "${BASH_SOURCE[0]}")/profiles" && pwd)"
  local profile_path="${profile_name}"
  local profile_opts

  if [[ -z "${profile_name}" ]]; then
    echo -e "error: no deployment profile specified" 1>&2
    return 1
  fi

  if [[ ! -f "${profile_path}" ]]; then
    profile_path="${profile_base_path}/${profile_path}"
    if [[ ! -f "${profile_path}" ]]; then
      echo "error: profile '${profile_path}' does not exist" 1>&2
      return 1
    fi
  fi
  # Get full path of profile (Avoid to use realpath cmd)
  profile_path="$(cd "$(dirname "${profile_path}")"; pwd)/$(basename "${profile_path}")"

  profile_opts="-f ${profile_path}"

  # If the profile is in the default profile_base_path, enable inheritance feature
  if [[ "${profile_path}" == "${profile_base_path}"* ]]; then
    while true; do
      profile_path="$(cd "$(dirname "${profile_path}")" && pwd)"
      if [[ -f "${profile_path}.yaml" ]]; then
        profile_opts="-f ${profile_path}.yaml ${profile_opts}"
      elif [[ "${profile_path}" -ef "${profile_base_path}" ]]; then
        # Profile discovery completed
        break
      else
        echo "error: missing parent profile: '${profile_path}.yaml'" 1>&2
        return 1
      fi
    done
  fi

  echo "${profile_opts}"
}

if [[ -n "${QABOT_PROFILE}" ]]; then
  export QABOT_PROFILE_OPTS
  QABOT_PROFILE_OPTS="$(get_profile_opts "${QABOT_PROFILE}")" || exit 1
fi

if [ $# -eq 1 ]; then
    mode=$1
    if  [[ $mode == "option" ]]; then
        echo "6.1 6.2 6.3 6.4 "
    elif [[ $mode == "6.1" ]]; then
        echo "qabot with analysis and report"
    elif [[ $mode == "6.2" ]]; then
        echo "qabot with analysis "
    elif [[ $mode == "6.3" ]]; then
        echo "qabot with report"
    elif [[ $mode == "6.4" ]]; then
        echo "qabot only"
    fi

elif [[ $# -eq 2 && $2 == "genyaml" ]]; then
    mode=$1
    if   [[ $mode == "6.1" ]] ;then
         deploye_with_analysis_and_report $WORK_PATH $WORK_PATH
    elif [[ $mode == "6.2" ]] ;then
         deploye_with_analysis $WORK_PATH $WORK_PATH
    elif [[ $mode == "6.3" ]] ;then
         deploye_with_report $WORK_PATH $WORK_PATH
    elif [[ $mode == "6.4" ]] ;then
         deploye_only $WORK_PATH $WORK_PATH
    fi
elif [[ $# -eq 3 && $2 == "genyaml" && $3 == "swarm" ]] ; then
    mode=$1
    if   [[ $mode == "6.1" ]] ;then
         deploye_with_analysis_and_report_swarm $WORK_PATH $WORK_PATH
    elif [[ $mode == "6.2" ]] ;then
         deploye_with_analysis_swarm $WORK_PATH $WORK_PATH
    elif [[ $mode == "6.3" ]] ;then
         deploye_with_report_swarm $WORK_PATH $WORK_PATH
    elif [[ $mode == "6.4" ]] ;then
         deploye_only_swarm $WORK_PATH $WORK_PATH
    fi
fi






