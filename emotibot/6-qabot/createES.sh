
source ../../dev.env

curl -u ${ES_USER}:${ES_PASS} -H "Content-Type: application/json" -XPUT ${ES_HOST}:${ES_PORT}/analysis -d '
{
	"mappings": {
		"content": {
			"properties": {
				"_class": {
					"type": "text",
					"fields": {
						"keyword": {
							"type": "keyword",
							"ignore_above": 256
						}
					}
				},
				"summary_type":{
					"type": "keyword"
				},
				"serial_number":{
					"type": "keyword"
				},
				"agentID": {
					"type": "keyword"
				},
				"agentId": {
					"type": "text",
					"fields": {
						"keyword": {
							"type": "keyword",
							"ignore_above": 256
						}
					}
				},
				"business_type": {
					"type": "keyword"
				},
				"callId": {
					"type": "keyword"
				},
				"callStatus": {
					"properties": {
						"callId": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						},
						"customerEmotion": {
							"type": "keyword"
						},
						"seatEmotion": {
							"type": "keyword"
						},
						"speechRate": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						},
						"stealWord": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						}
					}
				},
				"call_area": {
					"type": "keyword"
				},
				"call_group_uuid": {
					"type": "text",
					"fields": {
						"keyword": {
							"type": "keyword",
							"ignore_above": 256
						}
					}
				},
				"call_id": {
					"type": "text",
					"fields": {
						"keyword": {
							"type": "keyword",
							"ignore_above": 256
						}
					}
				},
				"call_number": {
					"type": "keyword"
				},
				"call_uuid": {
					"type": "text",
					"fields": {
						"keyword": {
							"type": "keyword",
							"ignore_above": 256
						}
					}
				},
				"channel": {
					"type": "keyword"
				},
				"chatMessages": {
					"type": "nested",
					"properties": {
						"_id": {
							"properties": {
								"$oid": {
									"type": "text",
									"fields": {
										"keyword": {
											"type": "keyword",
											"ignore_above": 256
										}
									}
								}
							}
						},
						"agentID": {
							"type": "keyword"
						},
						"agentId": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						},
						"callID": {
							"type": "keyword"
						},
						"callId": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						},
						"customerId": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						},
						"index": {
							"type": "long"
						},
						"role": {
							"type": "long"
						},
						"text": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						},
						"text_ik": {
							"type": "text",
							"analyzer": "ik_max_word",
							"search_analyzer": "ik_smart"
						},
						"time": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						},
						"vad": {
							"type": "long"
						}
					}
				},
				"check_score": {
					"type": "float"
				},
				"customerId": {
					"type": "text",
					"fields": {
						"keyword": {
							"type": "keyword",
							"ignore_above": 256
						}
					}
				},
				"customerName": {
					"type": "keyword"
				},
				"customerPhone": {
					"type": "keyword"
				},
				"customer_id": {
					"type": "keyword"
				},
				"dateTime": {
					"properties": {
						"$date": {
							"type": "long"
						}
					}
				},
				"dialogue_duration": {
					"type": "long"
				},
				"dialogue_time": {
					"type": "keyword"
				},
				"endTime": {
					"properties": {
						"$date": {
							"type": "long"
						}
					}
				},
				"extension": {
					"type": "keyword"
				},
				"flowQualityInspection": {
					"properties": {
						"callId": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						},
						"currentFlowList": {
							"type": "keyword"
						},
						"flowId": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						},
						"navigations": {
							"type": "nested",
							"properties": {
								"comment": {
									"type": "text",
									"fields": {
										"keyword": {
											"type": "keyword",
											"ignore_above": 256
										}
									}
								},
								"intent_name": {
									"type": "keyword"
								},
								"name": {
									"type": "keyword"
								},
								"nodes": {
									"type": "nested",
									"properties": {
										"hit": {
											"type": "boolean"
										},
										"optional": {
											"type": "boolean"
										},
										"role": {
											"type": "text",
											"fields": {
												"keyword": {
													"type": "keyword",
													"ignore_above": 256
												}
											}
										},
										"sg_id": {
											"type": "text",
											"fields": {
												"keyword": {
													"type": "keyword",
													"ignore_above": 256
												}
											}
										},
										"sg_name": {
											"type": "keyword"
										}
									}
								},
								"role": {
									"type": "text",
									"fields": {
										"keyword": {
											"type": "keyword",
											"ignore_above": 256
										}
									}
								},
								"type": {
									"type": "keyword"
								}
							}
						},
						"sensitive": {
							"type": "keyword"
						}
					}
				},
				"hotwords": {
					"type": "nested",
					"properties": {
						"hotword": {
							"type": "keyword"
						},
						"source": {
							"type": "long"
						}
					}
				},
				"insurance_name": {
					"type": "keyword"
				},
				"insurance_type": {
					"type": "keyword"
				},
				"knowledges": {
					"type": "nested",
					"properties": {
						"_id": {
							"properties": {
								"$oid": {
									"type": "text",
									"fields": {
										"keyword": {
											"type": "keyword",
											"ignore_above": 256
										}
									}
								}
							}
						},
						"agentId": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						},
						"callId": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						},
						"code": {
							"type": "keyword"
						},
						"content": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						},
						"customerId": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						},
						"details": {
							"type": "nested",
							"properties": {
								"entity": {
									"type": "text",
									"fields": {
										"keyword": {
											"type": "keyword",
											"ignore_above": 256
										}
									}
								},
								"property": {
									"type": "keyword"
								},
								"value": {
									"type": "text",
									"fields": {
										"keyword": {
											"type": "keyword",
											"ignore_above": 256
										}
									}
								}
							}
						},
						"evaluate": {
							"type": "keyword"
						},
						"evaluateContent": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						},
						"index": {
							"type": "long"
						},
						"sentence": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						},
						"title": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						},
						"type": {
							"type": "keyword"
						}
					}
				},
				"satisfaction_code": {
					"type": "keyword"
				},
				"satisfaction_name": {
					"type": "keyword"
				},
				"service_type": {
					"type": "keyword"
				},
				"silence_duration": {
					"type": "long"
				},
				"staff": {
					"type": "keyword"
				},
				"staff_group_type": {
					"type": "keyword"
				},
				"staff_level": {
					"type": "keyword"
				},
				"staff_online_time": {
					"type": "keyword"
				},
				"totalTime": {
					"type": "long"
				},
				"type": {
					"type": "long"
				},
				"viewUserPortraits": {
					"type": "nested",
					"properties": {
						"callId": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						},
						"customerId": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						},
						"isClick": {
							"type": "boolean"
						},
						"labelName": {
							"type": "keyword"
						},
						"status": {
							"type": "long"
						}
					}
				},
				"warnInfos": {
					"type": "nested",
					"properties": {
						"_id": {
							"properties": {
								"$oid": {
									"type": "text",
									"fields": {
										"keyword": {
											"type": "keyword",
											"ignore_above": 256
										}
									}
								}
							}
						},
						"agentId": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						},
						"callId": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						},
						"code": {
							"type": "long"
						},
						"content": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						},
						"cutomerId": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						},
						"date": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						},
						"evaluate": {
							"type": "keyword"
						},
						"index": {
							"type": "long"
						},
						"sentence": {
							"type": "text",
							"fields": {
								"keyword": {
									"type": "keyword",
									"ignore_above": 256
								}
							}
						},
						"type": {
							"type": "keyword"
						}
					}
				}
			}
		}
	}
}'