#!/bin/bash

WORKDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
set -o allexport
source $WORKDIR/../../../dev.env
source $WORKDIR/../dev.env
set +o allexport
function import_corpora(){
  export EE_OPTS="$(echo "$*" | sed -e "s|-\bf\b||g" -e "s|${input_corpora_dir}||g")"
  export CORPORA_DIR="${WORKDIR}/example-emotion-corpora"
  export EE_IMAGE="$(cat ${WORKDIR}/../../../infra/init-db.yaml \
    | sed -n 's/^ *image: *\(.*init-db:qabot.*\)/\1/p' | head -n 1)"
  run_yaml="${WORKDIR}/ee.yaml"
  docker-compose -f $run_yaml run --rm 6-qabot
}

function main(){
  import_corpora "$@"
  return "$?"
}

main "$@"
