#!/bin/bash

if [[ "${INIT_DB_DEBUG}" == 'true' ]]; then
  set -x
fi

# Try to import emotion corpora files to emotion engine
if [[ "$1" == 'eeimporter' ]]; then
  elapsed_secs='0'
  emotion_corpora_dir="${2:-/emotion-files}"
  if [[ ! -d "${emotion_corpora_dir}" ]] || \
    [[ "$(ls "${emotion_corpora_dir}")" == '' ]]; then
    exit 0
  fi
  if echo "${EEIMPORTER_OPTS}" | grep -q -w -- '-h\|--help'; then
    # With --help
    EEIMPORTER_OPTS="--help"
  else
    # Without --help
    if ! echo "${EEIMPORTER_OPTS}" | grep -q -w -- '-addr'; then
      # Emotion engine IP address from ${EE_HOST}
      echo -n "Wait max ${TIMEOUT} seconds for the emotion engine to start"
      if [[ "${INIT_DB_DEBUG}" != 'true' ]]; then
        no_output_cmd='&>/dev/null'
      fi
      if [[ $EE_VERSION == "1" ]]; then
          test_ee_cmd="curl -f -XPOST '${EE_HOST}:${EE_PORT}/list_models' \
              -d '{\"app_id\": \"${EE_APPID}\"}' ${no_output_cmd}"
      elif [[ $EE_VERSION == "2" ]]; then
          test_ee_cmd="curl -f -XPOST '${MLP_HOST}:${MLP_PORT}/mlpapi/offline/mlp'"
      fi
      until bash -c "${test_ee_cmd}" \
        || [[ "${elapsed_secs}" -gt "${TIMEOUT}" ]]; do
        sleep 1
        echo -n '.'
        ((elapsed_secs++))
      done
      echo ''
    fi
    EEIMPORTER_OPTS="${EEIMPORTER_OPTS} -f ${emotion_corpora_dir}"
  fi
  if [[ "${elapsed_secs}" -gt "${TIMEOUT}" ]]; then
    echo "Timeout: the emotion engine has not started, skip importing emotions" 1>&2
    exit 1
  fi

  if [[ $EE_VERSION == "1" ]]; then
    eeimporter ${EEIMPORTER_OPTS} -addr ${EE_HOST}:${EE_PORT} -d ${EE_ID}
  elif [[ $EE_VERSION == "2" ]]; then
    eeimporter2 ${EEIMPORTER_OPTS} -addr ${MLP_HOST}:${MLP_PORT} -appId qabot-default -problemId ${EE_ID}
  fi
  exit "$?"
fi

if [ "${INIT_MYSQL_INIT}" == "true" ] || [ "${INIT_MYSQL_INIT}" == "TRUE" ] ; then
    echo "# =================================================================="
    echo "# sql"
    echo "# =================================================================="
    cd /usr/bin/app/sql && ./up.sh development

    if [[ $? -ne 0 ]]; then
        echo "# Fail to migrate sql"
        exit -1
    fi


fi


if [ "${INIT_ES_INIT}" == "true" ] || [ "${INIT_ES_INIT}" == "TRUE" ] ; then
    echo "# =================================================================="
    echo "# elasticsearch"
    echo "# =================================================================="
    cd /usr/bin/app/elasticsearch && ./entrypoint.sh

    if [[ $? -ne 0 ]]; then
        echo "# Fail to migrate elasticsearch"
        exit -1
    fi
fi


if [ "${INIT_MINIO_INIT}" == "true" ] || [ "${INIT_MINIO_INIT}" == "TRUE" ] ; then
    echo "# =================================================================="
    echo "# minio"
    echo "# =================================================================="
    cd /usr/bin/app/minio && ./entrypoint.sh

    if [[ $? -ne 0 ]]; then
        echo "# Fail to migrate minio"
        exit -1
    fi
fi

if [ "${INIT_MODULE_INIT}" == "true" ] || [ "${INIT_MODULE_INIT}" == "TRUE" ] ; then
    echo "# =================================================================="
    echo "# module"
    echo "# =================================================================="
    cd /usr/bin/app/module && ./entrypoint.sh

    if [[ $? -ne 0 ]]; then
        echo "# Fail to migrate modules"
        exit -1
    fi
fi



