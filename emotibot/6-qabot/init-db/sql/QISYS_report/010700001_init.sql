-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for QRTZ_BLOB_TRIGGERS
-- ----------------------------
CREATE TABLE `QRTZ_BLOB_TRIGGERS` (
                                      `SCHED_NAME` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                      `TRIGGER_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                      `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                      `BLOB_DATA` blob,
                                      PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`) USING BTREE,
                                      KEY `SCHED_NAME` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for QRTZ_CALENDARS
-- ----------------------------
CREATE TABLE `QRTZ_CALENDARS` (
                                  `SCHED_NAME` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                  `CALENDAR_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                  `CALENDAR` blob NOT NULL,
                                  PRIMARY KEY (`SCHED_NAME`,`CALENDAR_NAME`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for QRTZ_CRON_TRIGGERS
-- ----------------------------
CREATE TABLE `QRTZ_CRON_TRIGGERS` (
                                      `SCHED_NAME` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                      `TRIGGER_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                      `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                      `CRON_EXPRESSION` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                      `TIME_ZONE_ID` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
                                      PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of QRTZ_CRON_TRIGGERS
-- ----------------------------
BEGIN;
INSERT INTO `QRTZ_CRON_TRIGGERS` VALUES ('quartzScheduler', 'DEFAULT_JOB_NAME', 'DEFAULT_JOB_GROUP', '*/5 * * * * ?', 'Asia/Shanghai');
COMMIT;

-- ----------------------------
-- Table structure for QRTZ_FIRED_TRIGGERS
-- ----------------------------
CREATE TABLE `QRTZ_FIRED_TRIGGERS` (
                                       `SCHED_NAME` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                       `ENTRY_ID` varchar(95) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                       `TRIGGER_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                       `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                       `INSTANCE_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                       `FIRED_TIME` bigint(13) NOT NULL,
                                       `SCHED_TIME` bigint(13) NOT NULL,
                                       `PRIORITY` int(11) NOT NULL,
                                       `STATE` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                       `JOB_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
                                       `JOB_GROUP` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
                                       `IS_NONCONCURRENT` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
                                       `REQUESTS_RECOVERY` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
                                       PRIMARY KEY (`SCHED_NAME`,`ENTRY_ID`) USING BTREE,
                                       KEY `IDX_QRTZ_FT_TRIG_INST_NAME` (`SCHED_NAME`,`INSTANCE_NAME`) USING BTREE,
                                       KEY `IDX_QRTZ_FT_INST_JOB_REQ_RCVRY` (`SCHED_NAME`,`INSTANCE_NAME`,`REQUESTS_RECOVERY`) USING BTREE,
                                       KEY `IDX_QRTZ_FT_J_G` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`) USING BTREE,
                                       KEY `IDX_QRTZ_FT_JG` (`SCHED_NAME`,`JOB_GROUP`) USING BTREE,
                                       KEY `IDX_QRTZ_FT_T_G` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`) USING BTREE,
                                       KEY `IDX_QRTZ_FT_TG` (`SCHED_NAME`,`TRIGGER_GROUP`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of QRTZ_FIRED_TRIGGERS
-- ----------------------------
BEGIN;
INSERT INTO `QRTZ_FIRED_TRIGGERS` VALUES ('quartzScheduler', 'NON_CLUSTERED1583317594287', 'DEFAULT_JOB_NAME', 'DEFAULT_JOB_GROUP', 'NON_CLUSTERED', 1583327530047, 1583327535000, 5, 'ACQUIRED', NULL, NULL, '0', '0');
COMMIT;

-- ----------------------------
-- Table structure for QRTZ_JOB_DETAILS
-- ----------------------------
CREATE TABLE `QRTZ_JOB_DETAILS` (
                                    `SCHED_NAME` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                    `JOB_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                    `JOB_GROUP` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                    `DESCRIPTION` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
                                    `JOB_CLASS_NAME` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                    `IS_DURABLE` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                    `IS_NONCONCURRENT` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                    `IS_UPDATE_DATA` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                    `REQUESTS_RECOVERY` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                    `JOB_DATA` blob,
                                    PRIMARY KEY (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`) USING BTREE,
                                    KEY `IDX_QRTZ_J_REQ_RECOVERY` (`SCHED_NAME`,`REQUESTS_RECOVERY`) USING BTREE,
                                    KEY `IDX_QRTZ_J_GRP` (`SCHED_NAME`,`JOB_GROUP`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of QRTZ_JOB_DETAILS
-- ----------------------------
BEGIN;
INSERT INTO `QRTZ_JOB_DETAILS` VALUES ('quartzScheduler', 'DEFAULT_JOB_NAME', 'DEFAULT_JOB_GROUP', '5 seconds to scan the report task.', 'com.emotibot.qic.report.service.JobService', '0', '0', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787000737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F40000000000010770800000010000000007800);
COMMIT;

-- ----------------------------
-- Table structure for QRTZ_LOCKS
-- ----------------------------
CREATE TABLE `QRTZ_LOCKS` (
                              `SCHED_NAME` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                              `LOCK_NAME` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                              PRIMARY KEY (`SCHED_NAME`,`LOCK_NAME`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of QRTZ_LOCKS
-- ----------------------------
BEGIN;
INSERT INTO `QRTZ_LOCKS` VALUES ('quartzScheduler', 'TRIGGER_ACCESS');
COMMIT;

-- ----------------------------
-- Table structure for QRTZ_PAUSED_TRIGGER_GRPS
-- ----------------------------
CREATE TABLE `QRTZ_PAUSED_TRIGGER_GRPS` (
                                            `SCHED_NAME` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                            `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                            PRIMARY KEY (`SCHED_NAME`,`TRIGGER_GROUP`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for QRTZ_SCHEDULER_STATE
-- ----------------------------
CREATE TABLE `QRTZ_SCHEDULER_STATE` (
                                        `SCHED_NAME` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                        `INSTANCE_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                        `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
                                        `CHECKIN_INTERVAL` bigint(13) NOT NULL,
                                        PRIMARY KEY (`SCHED_NAME`,`INSTANCE_NAME`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for QRTZ_SIMPLE_TRIGGERS
-- ----------------------------
CREATE TABLE `QRTZ_SIMPLE_TRIGGERS` (
                                        `SCHED_NAME` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                        `TRIGGER_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                        `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                        `REPEAT_COUNT` bigint(7) NOT NULL,
                                        `REPEAT_INTERVAL` bigint(12) NOT NULL,
                                        `TIMES_TRIGGERED` bigint(10) NOT NULL,
                                        PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for QRTZ_SIMPROP_TRIGGERS
-- ----------------------------
CREATE TABLE `QRTZ_SIMPROP_TRIGGERS` (
                                         `SCHED_NAME` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                         `TRIGGER_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                         `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                         `STR_PROP_1` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
                                         `STR_PROP_2` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
                                         `STR_PROP_3` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
                                         `INT_PROP_1` int(11) DEFAULT NULL,
                                         `INT_PROP_2` int(11) DEFAULT NULL,
                                         `LONG_PROP_1` bigint(20) DEFAULT NULL,
                                         `LONG_PROP_2` bigint(20) DEFAULT NULL,
                                         `DEC_PROP_1` decimal(13,4) DEFAULT NULL,
                                         `DEC_PROP_2` decimal(13,4) DEFAULT NULL,
                                         `BOOL_PROP_1` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
                                         `BOOL_PROP_2` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
                                         PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for QRTZ_TRIGGERS
-- ----------------------------
CREATE TABLE `QRTZ_TRIGGERS` (
                                 `SCHED_NAME` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                 `TRIGGER_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                 `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                 `JOB_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                 `JOB_GROUP` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                 `DESCRIPTION` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
                                 `NEXT_FIRE_TIME` bigint(13) DEFAULT NULL,
                                 `PREV_FIRE_TIME` bigint(13) DEFAULT NULL,
                                 `PRIORITY` int(11) DEFAULT NULL,
                                 `TRIGGER_STATE` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                 `TRIGGER_TYPE` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                 `START_TIME` bigint(13) NOT NULL,
                                 `END_TIME` bigint(13) DEFAULT NULL,
                                 `CALENDAR_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
                                 `MISFIRE_INSTR` smallint(2) DEFAULT NULL,
                                 `JOB_DATA` blob,
                                 PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`) USING BTREE,
                                 KEY `IDX_QRTZ_T_J` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`) USING BTREE,
                                 KEY `IDX_QRTZ_T_JG` (`SCHED_NAME`,`JOB_GROUP`) USING BTREE,
                                 KEY `IDX_QRTZ_T_C` (`SCHED_NAME`,`CALENDAR_NAME`) USING BTREE,
                                 KEY `IDX_QRTZ_T_G` (`SCHED_NAME`,`TRIGGER_GROUP`) USING BTREE,
                                 KEY `IDX_QRTZ_T_STATE` (`SCHED_NAME`,`TRIGGER_STATE`) USING BTREE,
                                 KEY `IDX_QRTZ_T_N_STATE` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`) USING BTREE,
                                 KEY `IDX_QRTZ_T_N_G_STATE` (`SCHED_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`) USING BTREE,
                                 KEY `IDX_QRTZ_T_NEXT_FIRE_TIME` (`SCHED_NAME`,`NEXT_FIRE_TIME`) USING BTREE,
                                 KEY `IDX_QRTZ_T_NFT_ST` (`SCHED_NAME`,`TRIGGER_STATE`,`NEXT_FIRE_TIME`) USING BTREE,
                                 KEY `IDX_QRTZ_T_NFT_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`) USING BTREE,
                                 KEY `IDX_QRTZ_T_NFT_ST_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_STATE`) USING BTREE,
                                 KEY `IDX_QRTZ_T_NFT_ST_MISFIRE_GRP` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_GROUP`,`TRIGGER_STATE`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of QRTZ_TRIGGERS
-- ----------------------------
BEGIN;
INSERT INTO `QRTZ_TRIGGERS` VALUES ('quartzScheduler', 'DEFAULT_JOB_NAME', 'DEFAULT_JOB_GROUP', 'DEFAULT_JOB_NAME', 'DEFAULT_JOB_GROUP', 'the default trigger', 1583327535000, 1583327530000, 5, 'ACQUIRED', 'CRON', 1580723225000, 0, NULL, 0, '');
COMMIT;

-- ----------------------------
-- Table structure for report_data
-- ----------------------------
CREATE TABLE `report_data` (
                               `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
                               `task_id` bigint(20) unsigned DEFAULT NULL COMMENT 'task creater if we have',
                               `create_time` bigint(20) DEFAULT '0' COMMENT 'the timestamp of creating time',
                               `order_num` bigint(20) DEFAULT '0' COMMENT 'order_num',
                               `field` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT 'field',
                               PRIMARY KEY (`id`) USING BTREE,
                               KEY `idx_task_id` (`task_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7871 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of report_data
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for report_meta
-- ----------------------------
CREATE TABLE `report_meta` (
                               `id` int(16) NOT NULL COMMENT '0:The report list. The report type and name sep by /\n1:The create conditions\n2:The fixed header it has order\n',
                               `type` int(255) DEFAULT NULL COMMENT 'type',
                               `uuid` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'uuid',
                               `field` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'The json field\n',
                               `order_num` int(16) DEFAULT NULL COMMENT 'order_num',
                               `lang` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'en cn tw\n',
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of report_meta
-- ----------------------------
BEGIN;
INSERT INTO `report_meta` VALUES (1, 0, 'qDetailCallOutAudio_machine', '{\"name\":\"机器质检明细报表\",\"code\":\"reportDetailList/qDetailCallOutAudio_machine\"}', 1, 'zh_cn');
INSERT INTO `report_meta` VALUES (2, 0, 'qDetailCallOutAudio_machine', '{\"name\":\"Machine check info report\",\"code\":\"reportDetailList/qDetailCallOutAudio_machine\"}', 2, 'zh_en');
INSERT INTO `report_meta` VALUES (3, 0, 'qDetailCallOutAudio_machine', '{\"name\":\"機器質檢明細報表\",\"code\":\"reportDetailList/qDetailCallOutAudio_machine\"}', 3, 'zh_tw');
INSERT INTO `report_meta` VALUES (4, 0, 'qDetailCallOutAudio_machineHuman', '{\"name\":\"人工抽检明细报表\",\"code\":\"reportDetailList/qDetailCallOutAudio_machineHuman\"}', 4, 'zh_cn');
INSERT INTO `report_meta` VALUES (5, 0, 'qDetailCallOutAudio_machineHuman', '{\"name\":\"Human check info report\",\"code\":\"reportDetailList/qDetailCallOutAudio_machineHuman\"}', 5, 'zh_en');
INSERT INTO `report_meta` VALUES (6, 0, 'qDetailCallOutAudio_machineHuman', '{\"name\":\"人工抽檢明細報表\",\"code\":\"reportDetailList/qDetailCallOutAudio_machineHuman\"}', 6, 'zh_tw');
INSERT INTO `report_meta` VALUES (7, 0, 'qDetailCallOutAudio_audioRepository', '{\"name\":\"教材库明细报表\",\"code\":\"reportDetailList/qDetailCallOutAudio_audioRepository\"}', 7, 'zh_cn');
INSERT INTO `report_meta` VALUES (8, 0, 'qDetailCallOutAudio_audioRepository', '{\"name\":\"Audio repository info report\",\"code\":\"reportDetailList/qDetailCallOutAudio_audioRepository\"}', 8, 'zh_en');
INSERT INTO `report_meta` VALUES (9, 0, 'qDetailCallOutAudio_audioRepository', '{\"name\":\"教材庫明細報表\",\"code\":\"reportDetailList/qDetailCallOutAudio_audioRepository\"}', 9, 'zh_tw');
INSERT INTO `report_meta` VALUES (10, 0, 'qDetailCallOutAudio_issue', '{\"name\":\"问题件明细报表\",\"code\":\"reportDetailList/qDetailCallOutAudio_issue}', 10, 'zh_cn');
INSERT INTO `report_meta` VALUES (11, 0, 'qDetailCallOutAudio_issue', '{\"name\":\"Issue info report\",\"code\":\"reportDetailList/qDetailCallOutAudio_issue\"}', 11, 'zh_en');
INSERT INTO `report_meta` VALUES (12, 0, 'qDetailCallOutAudio_issue', '{\"name\":\"問題件明細報表\",\"code\":\"reportDetailList/qDetailCallOutAudio_issue\"}', 12, 'zh_tw');
INSERT INTO `report_meta` VALUES (13, 0, 'qDetailCallOutAudio_appeal', '{\"name\":\"申诉件明细报表cn\",\"code\":\"reportDetailList/qDetailCallOutAudio_appeal\"}', 13, 'zh_cn');
INSERT INTO `report_meta` VALUES (14, 0, 'qDetailCallOutAudio_appeal', '{\"name\":\"Appeal info report\",\"code\":\"reportDetailList/qDetailCallOutAudio_appeal\"}', 14, 'zh_en');
INSERT INTO `report_meta` VALUES (15, 0, 'qDetailCallOutAudio_appeal', '{\"name\":\"申訴件明細報表\",\"code\":\"reportDetailList/qDetailCallOutAudio_appeal\"}', 15, 'zh_tw');
INSERT INTO `report_meta` VALUES (16, 1, 'qDetailCallOutAudio_machine', '[{\"key\":\"audioTime\",\"type\":\"s_e_date\",\"startKey\":\"audio_start_time\",\"endKey\":\"audio_end_time\",\"keyValue\":{\"title\":\"录音起止时间\",\"values\":{\"start\":{\"key\":\"audio_start_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"},\"end\":{\"key\":\"audio_end_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"}}}},{\"key\":\"scoreRange\",\"type\":\"range\",\"startKey\":\"min_score\",\"endKey\":\"max_score\",\"keyValue\":{\"title\":\"质检最高分\",\"values\":{\"start\":{\"key\":\"min_score\"},\"end\":{\"key\":\"max_score\"}}}},{\"key\":\"scenario_uuid\",\"type\":\"select\",\"keyValue\":{\"title\":\"场景\",\"selected\":[],\"values\":[]}}]', 16, 'zh_cn');
INSERT INTO `report_meta` VALUES (17, 1, 'qDetailCallOutAudio_machine', '[{\"key\":\"audioTime\",\"type\":\"s_e_date\",\"startKey\":\"audio_start_time\",\"endKey\":\"audio_end_time\",\"keyValue\":{\"title\":\"Audio time\",\"values\":{\"start\":{\"key\":\"audio_start_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"},\"end\":{\"key\":\"audio_end_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"}}}},{\"key\":\"scoreRange\",\"type\":\"range\",\"startKey\":\"min_score\",\"endKey\":\"max_score\",\"keyValue\":{\"title\":\"Checked score\",\"values\":{\"start\":{\"key\":\"min_score\"},\"end\":{\"key\":\"max_score\"}}}},{\"key\":\"scenario_uuid\",\"type\":\"select\",\"keyValue\":{\"title\":\"Scenario\",\"selected\":[],\"values\":[]}}]', 17, 'zh_en');
INSERT INTO `report_meta` VALUES (18, 1, 'qDetailCallOutAudio_machine', '[{\"key\":\"audioTime\",\"type\":\"s_e_date\",\"startKey\":\"audio_start_time\",\"endKey\":\"audio_end_time\",\"keyValue\":{\"title\":\"錄音起止時間\",\"values\":{\"start\":{\"key\":\"audio_start_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"},\"end\":{\"key\":\"audio_end_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"}}}},{\"key\":\"scoreRange\",\"type\":\"range\",\"startKey\":\"min_score\",\"endKey\":\"max_score\",\"keyValue\":{\"title\":\"質檢最高分\",\"values\":{\"start\":{\"key\":\"min_score\"},\"end\":{\"key\":\"max_score\"}}}},{\"key\":\"scenario_uuid\",\"type\":\"select\",\"keyValue\":{\"title\":\"場景\",\"selected\":[],\"values\":[]}}]', 18, 'zh_tw');
INSERT INTO `report_meta` VALUES (19, 1, 'qDetailCallOutAudio_machineHuman', '[{\"key\":\"audioTime\",\"type\":\"s_e_date\",\"startKey\":\"audio_start_time\",\"endKey\":\"audio_end_time\",\"keyValue\":{\"title\":\"录音起止时间\",\"values\":{\"start\":{\"key\":\"audio_start_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"},\"end\":{\"key\":\"audio_end_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"}}}},{\"key\":\"checkTime\",\"type\":\"s_e_date\",\"startKey\":\"check_start_time\",\"endKey\":\"check_end_time\",\"keyValue\":{\"title\":\"抽检完成时间\",\"values\":{\"start\":{\"key\":\"check_start_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"},\"end\":{\"key\":\"check_end_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"}}}},{\"key\":\"scoreRange\",\"type\":\"range\",\"startKey\":\"check_min_score\",\"endKey\":\"check_max_score\",\"keyValue\":{\"title\":\"质检最高分\",\"values\":{\"start\":{\"key\":\"check_min_score\"},\"end\":{\"key\":\"check_max_score\"}}}},{\"key\":\"scenario_uuid\",\"type\":\"select\",\"keyValue\":{\"title\":\"场景\",\"selected\":[],\"values\":[]}}]', 19, 'zh_cn');
INSERT INTO `report_meta` VALUES (20, 1, 'qDetailCallOutAudio_machineHuman', '[{\"key\":\"audioTime\",\"type\":\"s_e_date\",\"startKey\":\"audio_start_time\",\"endKey\":\"audio_end_time\",\"keyValue\":{\"title\":\"Audio time\",\"values\":{\"start\":{\"key\":\"audio_start_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"},\"end\":{\"key\":\"audio_end_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"}}}},{\"key\":\"checkTime\",\"type\":\"s_e_date\",\"startKey\":\"check_start_time\",\"endKey\":\"check_end_time\",\"keyValue\":{\"title\":\"Checked time\",\"values\":{\"start\":{\"key\":\"check_start_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"},\"end\":{\"key\":\"check_end_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"}}}},{\"key\":\"scoreRange\",\"type\":\"range\",\"startKey\":\"check_min_score\",\"endKey\":\"check_max_score\",\"keyValue\":{\"title\":\"Check score\",\"values\":{\"start\":{\"key\":\"check_min_score\"},\"end\":{\"key\":\"check_max_score\"}}}},{\"key\":\"scenario_uuid\",\"type\":\"select\",\"keyValue\":{\"title\":\"Scenario\",\"selected\":[],\"values\":[]}}]', 20, 'zh_en');
INSERT INTO `report_meta` VALUES (21, 1, 'qDetailCallOutAudio_machineHuman', '[{\"key\":\"audioTime\",\"type\":\"s_e_date\",\"startKey\":\"audio_start_time\",\"endKey\":\"audio_end_time\",\"keyValue\":{\"title\":\"錄音起止時間\",\"values\":{\"start\":{\"key\":\"audio_start_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"},\"end\":{\"key\":\"audio_end_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"}}}},{\"key\":\"checkTime\",\"type\":\"s_e_date\",\"startKey\":\"check_start_time\",\"endKey\":\"check_end_time\",\"keyValue\":{\"title\":\"抽檢完成時間\",\"values\":{\"start\":{\"key\":\"check_start_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"},\"end\":{\"key\":\"check_end_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"}}}},{\"key\":\"scoreRange\",\"type\":\"range\",\"startKey\":\"check_min_score\",\"endKey\":\"check_max_score\",\"keyValue\":{\"title\":\"質檢最高分\",\"values\":{\"start\":{\"key\":\"check_min_score\"},\"end\":{\"key\":\"check_max_score\"}}}},{\"key\":\"scenario_uuid\",\"type\":\"select\",\"keyValue\":{\"title\":\"場景\",\"selected\":[],\"values\":[]}}]', 21, 'zh_tw');
INSERT INTO `report_meta` VALUES (22, 1, 'qDetailCallOutAudio_audioRepository', '[{\"key\":\"audioTime\",\"type\":\"s_e_date\",\"startKey\":\"audio_start_time\",\"endKey\":\"audio_end_time\",\"keyValue\":{\"title\":\"录音起止时间\",\"values\":{\"start\":{\"key\":\"audio_start_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"},\"end\":{\"key\":\"audio_end_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"}}}},{\"key\":\"initTime\",\"type\":\"s_e_date\",\"startKey\":\"init_start_time\",\"endKey\":\"init_end_time\",\"keyValue\":{\"title\":\"发起教材时间\",\"values\":{\"start\":{\"key\":\"init_start_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"},\"end\":{\"key\":\"init_end_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"}}}}]', 22, 'zh_cn');
INSERT INTO `report_meta` VALUES (23, 1, 'qDetailCallOutAudio_audioRepository', '[{\"key\":\"audioTime\",\"type\":\"s_e_date\",\"startKey\":\"audio_start_time\",\"endKey\":\"audio_end_time\",\"keyValue\":{\"title\":\"Audio time\",\"values\":{\"start\":{\"key\":\"audio_start_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"},\"end\":{\"key\":\"audio_end_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"}}}},{\"key\":\"initTime\",\"type\":\"s_e_date\",\"startKey\":\"init_start_time\",\"endKey\":\"init_end_time\",\"keyValue\":{\"title\":\"Check time\",\"values\":{\"start\":{\"key\":\"init_start_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"},\"end\":{\"key\":\"init_end_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"}}}}]', 23, 'zh_en');
INSERT INTO `report_meta` VALUES (24, 1, 'qDetailCallOutAudio_audioRepository', '[{\"key\":\"audioTime\",\"type\":\"s_e_date\",\"startKey\":\"audio_start_time\",\"endKey\":\"audio_end_time\",\"keyValue\":{\"title\":\"錄音起止時間\",\"values\":{\"start\":{\"key\":\"audio_start_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"},\"end\":{\"key\":\"audio_end_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"}}}},{\"key\":\"initTime\",\"type\":\"s_e_date\",\"startKey\":\"init_start_time\",\"endKey\":\"init_end_time\",\"keyValue\":{\"title\":\"發起教材時間\",\"values\":{\"start\":{\"key\":\"init_start_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"},\"end\":{\"key\":\"init_end_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"}}}}]', 24, 'zh_tw');
INSERT INTO `report_meta` VALUES (25, 1, 'qDetailCallOutAudio_issue', '[{\"key\":\"audioTime\",\"type\":\"s_e_date\",\"startKey\":\"audio_start_time\",\"endKey\":\"audio_end_time\",\"keyValue\":{\"title\":\"录音起止时间\",\"values\":{\"start\":{\"key\":\"audio_start_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"},\"end\":{\"key\":\"audio_end_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"}}}},{\"key\":\"checkTime\",\"type\":\"s_e_date\",\"startKey\":\"check_start_time\",\"endKey\":\"check_end_time\",\"keyValue\":{\"title\":\"发起问题件时间\",\"values\":{\"start\":{\"key\":\"check_start_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"},\"end\":{\"key\":\"check_end_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"}}}},{\"key\":\"issue_uuid\",\"type\":\"select\",\"keyValue\":{\"title\":\"问题件类型\",\"selected\":[],\"values\":[]}}]', 25, 'zh_cn');
INSERT INTO `report_meta` VALUES (26, 1, 'qDetailCallOutAudio_issue', '[{\"key\":\"audioTime\",\"type\":\"s_e_date\",\"startKey\":\"audio_start_time\",\"endKey\":\"audio_end_time\",\"keyValue\":{\"title\":\"Audio time\",\"values\":{\"start\":{\"key\":\"audio_start_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"},\"end\":{\"key\":\"audio_end_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"}}}},{\"key\":\"checkTime\",\"type\":\"s_e_date\",\"startKey\":\"check_start_time\",\"endKey\":\"check_end_time\",\"keyValue\":{\"title\":\"Issue time\",\"values\":{\"start\":{\"key\":\"check_start_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"},\"end\":{\"key\":\"check_end_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"}}}},{\"key\":\"issue_uuid\",\"type\":\"select\",\"keyValue\":{\"title\":\"Issue type\",\"selected\":[],\"values\":[]}}]', 26, 'zh_en');
INSERT INTO `report_meta` VALUES (27, 1, 'qDetailCallOutAudio_issue', '[{\"key\":\"audioTime\",\"type\":\"s_e_date\",\"startKey\":\"audio_start_time\",\"endKey\":\"audio_end_time\",\"keyValue\":{\"title\":\"錄音起止時間\",\"values\":{\"start\":{\"key\":\"audio_start_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"},\"end\":{\"key\":\"audio_end_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"}}}},{\"key\":\"checkTime\",\"type\":\"s_e_date\",\"startKey\":\"check_start_time\",\"endKey\":\"check_end_time\",\"keyValue\":{\"title\":\"發起問題件時間\",\"values\":{\"start\":{\"key\":\"check_start_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"},\"end\":{\"key\":\"check_end_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"}}}},{\"key\":\"issue_uuid\",\"type\":\"select\",\"keyValue\":{\"title\":\"問題件類型\",\"selected\":[],\"values\":[]}}]', 27, 'zh_tw');
INSERT INTO `report_meta` VALUES (28, 1, 'qDetailCallOutAudio_appeal', '[{\"key\":\"audioTime\",\"type\":\"s_e_date\",\"startKey\":\"audio_start_time\",\"endKey\":\"audio_end_time\",\"keyValue\":{\"title\":\"录音起止时间\",\"values\":{\"start\":{\"key\":\"audio_start_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"},\"end\":{\"key\":\"audio_end_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"}}}},{\"key\":\"checkTime\",\"type\":\"s_e_date\",\"startKey\":\"check_start_time\",\"endKey\":\"check_end_time\",\"keyValue\":{\"title\":\"发起申诉时间\",\"values\":{\"start\":{\"key\":\"check_start_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"},\"end\":{\"key\":\"check_end_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"}}}},{\"key\":\"scenario_uuid\",\"type\":\"select\",\"keyValue\":{\"title\":\"场景\",\"selected\":[],\"values\":[]}}]', 28, 'zh_cn');
INSERT INTO `report_meta` VALUES (29, 1, 'qDetailCallOutAudio_appeal', '[{\"key\":\"audioTime\",\"type\":\"s_e_date\",\"startKey\":\"audio_start_time\",\"endKey\":\"audio_end_time\",\"keyValue\":{\"title\":\"Audio time\",\"values\":{\"start\":{\"key\":\"audio_start_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"},\"end\":{\"key\":\"audio_end_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"}}}},{\"key\":\"checkTime\",\"type\":\"s_e_date\",\"startKey\":\"check_start_time\",\"endKey\":\"check_end_time\",\"keyValue\":{\"title\":\"Appeal time\",\"values\":{\"start\":{\"key\":\"check_start_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"},\"end\":{\"key\":\"check_end_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"}}}},{\"key\":\"scenario_uuid\",\"type\":\"select\",\"keyValue\":{\"title\":\"Scenario\",\"selected\":[],\"values\":[]}}]', 29, 'zh_en');
INSERT INTO `report_meta` VALUES (30, 1, 'qDetailCallOutAudio_appeal', '[{\"key\":\"audioTime\",\"type\":\"s_e_date\",\"startKey\":\"audio_start_time\",\"endKey\":\"audio_end_time\",\"keyValue\":{\"title\":\"錄音起止時間\",\"values\":{\"start\":{\"key\":\"audio_start_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"},\"end\":{\"key\":\"audio_end_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"}}}},{\"key\":\"checkTime\",\"type\":\"s_e_date\",\"startKey\":\"check_start_time\",\"endKey\":\"check_end_time\",\"keyValue\":{\"title\":\"發起申訴時間\",\"values\":{\"start\":{\"key\":\"check_start_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"},\"end\":{\"key\":\"check_end_time\",\"type\":\"date\",\"valueFormat\":\"yyyy/MM/dd HH:mm:ss\"}}}},{\"key\":\"scenario_uuid\",\"type\":\"select\",\"keyValue\":{\"title\":\"場景\",\"selected\":[],\"values\":[]}}]', 30, 'zh_tw');
INSERT INTO `report_meta` VALUES (31, 2, 'qDetailCallOutAudio_machine', '[{\"key\":\"BASIC_VALUE\",\"text\":\"通话信息\",\"sub\":[{\"key\":\"call_uuid\",\"text\":\"档名\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"staff_id\",\"text\":\"坐席ID\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"staff_name\",\"text\":\"坐席姓名\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"department\",\"text\":\"所属部门\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"call_time\",\"text\":\"通话开始时间\",\"type\":\"DATETIME\",\"rule\":\"LAST\"},{\"key\":\"duration\",\"text\":\"通话时间\",\"type\":\"DURATION\",\"rule\":\"SUM\"},{\"key\":\"customer_id\",\"text\":\"客户ID\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"customer_name\",\"text\":\"客户姓名\",\"type\":\"STRING\",\"rule\":\"LAST\"},{\"key\":\"customer_phone\",\"text\":\"客户电话\",\"type\":\"STRING\",\"rule\":\"LAST\"},{\"key\":\"call_id\",\"text\":\"call_id\",\"type\":\"STRING\",\"rule\":\"COMBINE\"}]},{\"key\":\"USER_VALUE\",\"text\":\"随录字段\",\"sub\":[]},{\"key\":\"MACHINE_VALUE\",\"text\":\"机器质检\",\"sub\":[{\"key\":\"score\",\"text\":\"质检分数\",\"type\":\"STRING\"},{\"key\":\"checkTime\",\"text\":\"时间\",\"type\":\"DATETIME\"},{\"key\":\"SCENARIO_VALUE\",\"text\":\"时间\",\"type\":\"DATETIME\",\"sub\":[]}]}]', 31, 'zh_cn');
INSERT INTO `report_meta` VALUES (32, 2, 'qDetailCallOutAudio_machine', '[{\"key\":\"BASIC_VALUE\",\"text\":\"call info\",\"sub\":[{\"key\":\"call_uuid\",\"text\":\"file name\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"staff_id\",\"text\":\"staff id\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"staff_name\",\"text\":\"staff name\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"department\",\"text\":\"department\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"call_time\",\"text\":\"call time\",\"type\":\"DATETIME\",\"rule\":\"LAST\"},{\"key\":\"duration\",\"text\":\"duration\",\"type\":\"DURATION\",\"rule\":\"SUM\"},{\"key\":\"customer_id\",\"text\":\"customer id\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"customer_name\",\"text\":\"customer name\",\"type\":\"STRING\",\"rule\":\"LAST\"},{\"key\":\"customer_phone\",\"text\":\"customer phone\",\"type\":\"STRING\",\"rule\":\"LAST\"},{\"key\":\"call_id\",\"text\":\"call id\",\"type\":\"STRING\",\"rule\":\"COMBINE\"}]},{\"key\":\"USER_VALUE\",\"text\":\"user value\",\"sub\":[]},{\"key\":\"MACHINE_VALUE\",\"text\":\"machine check\",\"sub\":[{\"key\":\"score\",\"text\":\"score\",\"type\":\"STRING\"},{\"key\":\"checkTime\",\"text\":\"check time\",\"type\":\"DATETIME\"},{\"key\":\"SCENARIO_VALUE\",\"text\":\"scenario value\",\"type\":\"DATETIME\",\"sub\":[]}]}]', 32, 'zh_en');
INSERT INTO `report_meta` VALUES (33, 2, 'qDetailCallOutAudio_machine', '[{\"key\":\"BASIC_VALUE\",\"text\":\"通話信息\",\"sub\":[{\"key\":\"call_uuid\",\"text\":\"檔名\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"staff_id\",\"text\":\"坐席ID\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"staff_name\",\"text\":\"坐席姓名\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"department\",\"text\":\"所屬部門\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"call_time\",\"text\":\"通話開始時間\",\"type\":\"DATETIME\",\"rule\":\"LAST\"},{\"key\":\"duration\",\"text\":\"通話時間\",\"type\":\"DURATION\",\"rule\":\"SUM\"},{\"key\":\"customer_id\",\"text\":\"客戶ID\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"customer_name\",\"text\":\"客戶姓名\",\"type\":\"STRING\",\"rule\":\"LAST\"},{\"key\":\"customer_phone\",\"text\":\"客戶電話\",\"type\":\"STRING\",\"rule\":\"LAST\"},{\"key\":\"call_id\",\"text\":\"call_id\",\"type\":\"STRING\",\"rule\":\"COMBINE\"}]},{\"key\":\"USER_VALUE\",\"text\":\"隨錄字段\",\"sub\":[]},{\"key\":\"MACHINE_VALUE\",\"text\":\"機器質檢\",\"sub\":[{\"key\":\"score\",\"text\":\"質檢分數\",\"type\":\"STRING\"},{\"key\":\"checkTime\",\"text\":\"時間\",\"type\":\"DATETIME\"},{\"key\":\"SCENARIO_VALUE\",\"text\":\"時間\",\"type\":\"DATETIME\",\"sub\":[]}]}]', 33, 'zh_tw');
INSERT INTO `report_meta` VALUES (34, 2, 'qDetailCallOutAudio_machineHuman', '[{\"key\":\"BASIC_VALUE\",\"text\":\"通话信息\",\"sub\":[{\"key\":\"call_uuid\",\"text\":\"档名\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"staff_id\",\"text\":\"坐席ID\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"staff_name\",\"text\":\"坐席姓名\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"department\",\"text\":\"所属部门\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"call_time\",\"text\":\"通话开始时间\",\"type\":\"DATETIME\",\"rule\":\"LAST\"},{\"key\":\"duration\",\"text\":\"通话时间\",\"type\":\"DURATION\",\"rule\":\"SUM\"},{\"key\":\"customer_id\",\"text\":\"客户ID\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"customer_name\",\"text\":\"客户姓名\",\"type\":\"STRING\",\"rule\":\"LAST\"},{\"key\":\"customer_phone\",\"text\":\"客户电话\",\"type\":\"STRING\",\"rule\":\"LAST\"},{\"key\":\"call_id\",\"text\":\"call_id\",\"type\":\"STRING\",\"rule\":\"COMBINE\"}]},{\"key\":\"USER_VALUE\",\"text\":\"随录字段\",\"sub\":[]},{\"key\":\"MACHINE_VALUE\",\"text\":\"机器质检\",\"sub\":[{\"key\":\"score\",\"text\":\"质检分数\",\"type\":\"STRING\"},{\"key\":\"checkTime\",\"text\":\"时间\",\"type\":\"DATETIME\"},{\"key\":\"SCENARIO_VALUE\",\"text\":\"时间\",\"type\":\"DATETIME\",\"sub\":[]}]},{\"key\":\"MANUALLY_VALUE\",\"text\":\"人工抽检\",\"sub\":[{\"key\":\"inspectStatus\",\"text\":\"抽检状态\",\"type\":\"STRING\"},{\"key\":\"inspectTime\",\"text\":\"抽检日期\",\"type\":\"DATETIME\"},{\"key\":\"inspector\",\"text\":\"抽检人员\",\"type\":\"STRING\"},{\"key\":\"inspectScore\",\"text\":\"得分\",\"type\":\"String\"},{\"key\":\"MANUALLY_SCENARIO_VALUE\",\"text\":\"场景信息\",\"sub\":[]}]}]', 34, 'zh_cn');
INSERT INTO `report_meta` VALUES (35, 2, 'qDetailCallOutAudio_machineHuman', '[{\"key\":\"BASIC_VALUE\",\"text\":\"call info\",\"sub\":[{\"key\":\"call_uuid\",\"text\":\"call uuid\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"staff_id\",\"text\":\"staff id\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"staff_name\",\"text\":\"staff name\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"department\",\"text\":\"department\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"call_time\",\"text\":\"call time\",\"type\":\"DATETIME\",\"rule\":\"LAST\"},{\"key\":\"duration\",\"text\":\"duration\",\"type\":\"DURATION\",\"rule\":\"SUM\"},{\"key\":\"customer_id\",\"text\":\"customer id\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"customer_name\",\"text\":\"customer name\",\"type\":\"STRING\",\"rule\":\"LAST\"},{\"key\":\"customer_phone\",\"text\":\"customer phone\",\"type\":\"STRING\",\"rule\":\"LAST\"},{\"key\":\"call_id\",\"text\":\"call id\",\"type\":\"STRING\",\"rule\":\"COMBINE\"}]},{\"key\":\"USER_VALUE\",\"text\":\"user value\",\"sub\":[]},{\"key\":\"MACHINE_VALUE\",\"text\":\"machine check\",\"sub\":[{\"key\":\"score\",\"text\":\"score\",\"type\":\"STRING\"},{\"key\":\"checkTime\",\"text\":\"check time\",\"type\":\"DATETIME\"},{\"key\":\"SCENARIO_VALUE\",\"text\":\"时间\",\"type\":\"DATETIME\",\"sub\":[]}]},{\"key\":\"MANUALLY_VALUE\",\"text\":\"manually value\",\"sub\":[{\"key\":\"inspectStatus\",\"text\":\"inspect status\",\"type\":\"STRING\"},{\"key\":\"inspectTime\",\"text\":\"inspect time\",\"type\":\"DATETIME\"},{\"key\":\"inspector\",\"text\":\"inspector\",\"type\":\"STRING\"},{\"key\":\"inspectScore\",\"text\":\"inspect score\",\"type\":\"String\"},{\"key\":\"MANUALLY_SCENARIO_VALUE\",\"text\":\"MANUALLY_SCENARIO_VALUE\",\"sub\":[]}]}]', 35, 'zh_en');
INSERT INTO `report_meta` VALUES (36, 2, 'qDetailCallOutAudio_machineHuman', '[{\"key\":\"BASIC_VALUE\",\"text\":\"通話信息\",\"sub\":[{\"key\":\"call_uuid\",\"text\":\"檔名\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"staff_id\",\"text\":\"坐席ID\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"staff_name\",\"text\":\"坐席姓名\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"department\",\"text\":\"所屬部門\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"call_time\",\"text\":\"通話開始時間\",\"type\":\"DATETIME\",\"rule\":\"LAST\"},{\"key\":\"duration\",\"text\":\"通話時間\",\"type\":\"DURATION\",\"rule\":\"SUM\"},{\"key\":\"customer_id\",\"text\":\"客戶ID\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"customer_name\",\"text\":\"客戶姓名\",\"type\":\"STRING\",\"rule\":\"LAST\"},{\"key\":\"customer_phone\",\"text\":\"客戶電話\",\"type\":\"STRING\",\"rule\":\"LAST\"},{\"key\":\"call_id\",\"text\":\"call_id\",\"type\":\"STRING\",\"rule\":\"COMBINE\"}]},{\"key\":\"USER_VALUE\",\"text\":\"隨錄字段\",\"sub\":[]},{\"key\":\"MACHINE_VALUE\",\"text\":\"機器質檢\",\"sub\":[{\"key\":\"score\",\"text\":\"質檢分數\",\"type\":\"STRING\"},{\"key\":\"checkTime\",\"text\":\"時間\",\"type\":\"DATETIME\"},{\"key\":\"SCENARIO_VALUE\",\"text\":\"時間\",\"type\":\"DATETIME\",\"sub\":[]}]},{\"key\":\"MANUALLY_VALUE\",\"text\":\"人工抽檢\",\"sub\":[{\"key\":\"inspectStatus\",\"text\":\"抽檢狀態\",\"type\":\"STRING\"},{\"key\":\"inspectTime\",\"text\":\"抽檢日期\",\"type\":\"DATETIME\"},{\"key\":\"inspector\",\"text\":\"抽檢人員\",\"type\":\"STRING\"},{\"key\":\"inspectScore\",\"text\":\"得分\",\"type\":\"String\"},{\"key\":\"MANUALLY_SCENARIO_VALUE\",\"text\":\"場景信息\",\"sub\":[]}]}]', 36, 'zh_tw');
INSERT INTO `report_meta` VALUES (37, 2, 'qDetailCallOutAudio_audioRepository', '[{\"key\":\"BASIC_VALUE\",\"text\":\"通话信息\",\"sub\":[{\"key\":\"call_uuid\",\"text\":\"档名\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"staff_id\",\"text\":\"坐席ID\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"staff_name\",\"text\":\"坐席姓名\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"department\",\"text\":\"所属部门\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"call_time\",\"text\":\"通话开始时间\",\"type\":\"DATETIME\",\"rule\":\"LAST\"},{\"key\":\"duration\",\"text\":\"通话时间\",\"type\":\"DURATION\",\"rule\":\"SUM\"},{\"key\":\"customer_id\",\"text\":\"客户ID\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"customer_name\",\"text\":\"客户姓名\",\"type\":\"STRING\",\"rule\":\"LAST\"},{\"key\":\"customer_phone\",\"text\":\"客户电话\",\"type\":\"STRING\",\"rule\":\"LAST\"},{\"key\":\"call_id\",\"text\":\"call_id\",\"type\":\"STRING\",\"rule\":\"COMBINE\"}]},{\"key\":\"USER_VALUE\",\"text\":\"随录字段\",\"sub\":[]},{\"key\":\"TEACHING_VALUE\",\"text\":\"录音入库\",\"sub\":[{\"key\":\"teachingTag\",\"text\":\"录音标记\",\"type\":\"STRING\"},{\"key\":\"initiator\",\"text\":\"标记质检员\",\"type\":\"DATETIME\"},{\"key\":\"createTime\",\"text\":\"标记日期\",\"type\":\"DATETIME\"},{\"key\":\"state\",\"text\":\"审核状态\",\"type\":\"STRING\"},{\"key\":\"operator\",\"text\":\"审核人员\",\"type\":\"DATETIME\"},{\"key\":\"updateTime\",\"text\":\"审核日期\",\"type\":\"DATETIME\"}]}]', 37, 'zh_cn');
INSERT INTO `report_meta` VALUES (38, 2, 'qDetailCallOutAudio_audioRepository', '[{\"key\":\"BASIC_VALUE\",\"text\":\"call info\",\"sub\":[{\"key\":\"call_uuid\",\"text\":\"call uuid\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"staff_id\",\"text\":\"staff id\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"staff_name\",\"text\":\"staff name\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"department\",\"text\":\"department\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"call_time\",\"text\":\"call time\",\"type\":\"DATETIME\",\"rule\":\"LAST\"},{\"key\":\"duration\",\"text\":\"duration\",\"type\":\"DURATION\",\"rule\":\"SUM\"},{\"key\":\"customer_id\",\"text\":\"customer id\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"customer_name\",\"text\":\"客户姓名\",\"type\":\"STRING\",\"rule\":\"LAST\"},{\"key\":\"customer_phone\",\"text\":\"客户电话\",\"type\":\"STRING\",\"rule\":\"LAST\"},{\"key\":\"call_id\",\"text\":\"call id\",\"type\":\"STRING\",\"rule\":\"COMBINE\"}]},{\"key\":\"USER_VALUE\",\"text\":\"随录字段\",\"sub\":[]},{\"key\":\"TEACHING_VALUE\",\"text\":\"teaching value\",\"sub\":[{\"key\":\"teachingTag\",\"text\":\"teaching tag\",\"type\":\"STRING\"},{\"key\":\"initiator\",\"text\":\"initiator\",\"type\":\"DATETIME\"},{\"key\":\"createTime\",\"text\":\"create Time\",\"type\":\"DATETIME\"},{\"key\":\"state\",\"text\":\"state\",\"type\":\"STRING\"},{\"key\":\"operator\",\"text\":\"operator\",\"type\":\"DATETIME\"},{\"key\":\"updateTime\",\"text\":\"update time\",\"type\":\"DATETIME\"}]}]', 38, 'zh_en');
INSERT INTO `report_meta` VALUES (39, 2, 'qDetailCallOutAudio_audioRepository', '[{\"key\":\"BASIC_VALUE\",\"text\":\"通話信息\",\"sub\":[{\"key\":\"call_uuid\",\"text\":\"檔名\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"staff_id\",\"text\":\"坐席ID\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"staff_name\",\"text\":\"坐席姓名\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"department\",\"text\":\"所屬部門\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"call_time\",\"text\":\"通話開始時間\",\"type\":\"DATETIME\",\"rule\":\"LAST\"},{\"key\":\"duration\",\"text\":\"通話時間\",\"type\":\"DURATION\",\"rule\":\"SUM\"},{\"key\":\"customer_id\",\"text\":\"客戶ID\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"customer_name\",\"text\":\"客戶姓名\",\"type\":\"STRING\",\"rule\":\"LAST\"},{\"key\":\"customer_phone\",\"text\":\"客戶電話\",\"type\":\"STRING\",\"rule\":\"LAST\"},{\"key\":\"call_id\",\"text\":\"call_id\",\"type\":\"STRING\",\"rule\":\"COMBINE\"}]},{\"key\":\"USER_VALUE\",\"text\":\"隨錄字段\",\"sub\":[]},{\"key\":\"TEACHING_VALUE\",\"text\":\"錄音入庫\",\"sub\":[{\"key\":\"teachingTag\",\"text\":\"錄音標記\",\"type\":\"STRING\"},{\"key\":\"initiator\",\"text\":\"標記質檢員\",\"type\":\"DATETIME\"},{\"key\":\"createTime\",\"text\":\"標記日期\",\"type\":\"DATETIME\"},{\"key\":\"state\",\"text\":\"審核狀態\",\"type\":\"STRING\"},{\"key\":\"operator\",\"text\":\"審核人員\",\"type\":\"DATETIME\"},{\"key\":\"updateTime\",\"text\":\"審核日期\",\"type\":\"DATETIME\"}]}]', 39, 'zh_tw');
INSERT INTO `report_meta` VALUES (40, 2, 'qDetailCallOutAudio_issue', '[{\"key\":\"BASIC_VALUE\",\"text\":\"通话信息\",\"sub\":[{\"key\":\"call_uuid\",\"text\":\"档名\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"staff_id\",\"text\":\"坐席ID\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"staff_name\",\"text\":\"坐席姓名\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"department\",\"text\":\"所属部门\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"call_time\",\"text\":\"通话开始时间\",\"type\":\"DATETIME\",\"rule\":\"LAST\"},{\"key\":\"duration\",\"text\":\"通话时间\",\"type\":\"DURATION\",\"rule\":\"SUM\"},{\"key\":\"customer_id\",\"text\":\"客户ID\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"customer_name\",\"text\":\"客户姓名\",\"type\":\"STRING\",\"rule\":\"LAST\"},{\"key\":\"customer_phone\",\"text\":\"客户电话\",\"type\":\"STRING\",\"rule\":\"LAST\"},{\"key\":\"call_id\",\"text\":\"call_id\",\"type\":\"STRING\",\"rule\":\"COMBINE\"}]},{\"key\":\"USER_VALUE\",\"text\":\"随录字段\",\"sub\":[]},{\"key\":\"ISSUE_VALUE\",\"text\":\"问题件\",\"sub\":[{\"key\":\"initTime\",\"text\":\"发起时间\",\"type\":\"STRING\"},{\"key\":\"issueTypeName\",\"text\":\"问题件类型\",\"type\":\"STRING\"},{\"key\":\"selectedRuleInfo\",\"text\":\"选定规则\",\"type\":\"STRING\"},{\"key\":\"initRemark\",\"text\":\"发起问题件描述\",\"type\":\"STRING\"},{\"key\":\"initOperator\",\"text\":\"发起人员\",\"type\":\"STRING\"},{\"key\":\"issueStatus\",\"text\":\"处理状态\",\"type\":\"STRING\"},{\"key\":\"handleTime\",\"text\":\"处理时间\",\"type\":\"STRING\"},{\"key\":\"handleOperator\",\"text\":\"处理人员\",\"type\":\"STRING\"},{\"key\":\"handleResult\",\"text\":\"处理结果\",\"type\":\"STRING\"},{\"key\":\"handleRemark\",\"text\":\"处理结果细则\",\"type\":\"STRING\"},{\"key\":\"finishResult\",\"text\":\"复检结果\",\"type\":\"STRING\"},{\"key\":\"finishRemark\",\"text\":\"结案细则\",\"type\":\"STRING\"},{\"key\":\"finishTime\",\"text\":\"结案时间\",\"type\":\"STRING\"},{\"key\":\"finishOperator\",\"text\":\"结案人员\",\"type\":\"STRING\"}]}]', 40, 'zh_cn');
INSERT INTO `report_meta` VALUES (41, 2, 'qDetailCallOutAudio_issue', '[{\"key\":\"BASIC_VALUE\",\"text\":\"call info\",\"sub\":[{\"key\":\"call_uuid\",\"text\":\"call uuid\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"staff_id\",\"text\":\"staff id\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"staff_name\",\"text\":\"staff name\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"department\",\"text\":\"department\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"call_time\",\"text\":\"call time\",\"type\":\"DATETIME\",\"rule\":\"LAST\"},{\"key\":\"duration\",\"text\":\"duration\",\"type\":\"DURATION\",\"rule\":\"SUM\"},{\"key\":\"customer_id\",\"text\":\"customer id\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"customer_name\",\"text\":\"customer name\",\"type\":\"STRING\",\"rule\":\"LAST\"},{\"key\":\"customer_phone\",\"text\":\"customer phone\",\"type\":\"STRING\",\"rule\":\"LAST\"},{\"key\":\"call_id\",\"text\":\"call id\",\"type\":\"STRING\",\"rule\":\"COMBINE\"}]},{\"key\":\"USER_VALUE\",\"text\":\"user value\",\"sub\":[]},{\"key\":\"ISSUE_VALUE\",\"text\":\"Issue value\",\"sub\":[{\"key\":\"initTime\",\"text\":\"init time\",\"type\":\"STRING\"},{\"key\":\"issueTypeName\",\"text\":\"issue type name\",\"type\":\"STRING\"},{\"key\":\"selectedRuleInfo\",\"text\":\"rule info\",\"type\":\"STRING\"},{\"key\":\"initRemark\",\"text\":\"init remark\",\"type\":\"STRING\"},{\"key\":\"initOperator\",\"text\":\"init operator\",\"type\":\"STRING\"},{\"key\":\"issueStatus\",\"text\":\"issue status\",\"type\":\"STRING\"},{\"key\":\"handleTime\",\"text\":\"handle time\",\"type\":\"STRING\"},{\"key\":\"handleOperator\",\"text\":\"handle operator\",\"type\":\"STRING\"},{\"key\":\"handleResult\",\"text\":\"handle result\",\"type\":\"STRING\"},{\"key\":\"handleRemark\",\"text\":\"handle remark\",\"type\":\"STRING\"},{\"key\":\"finishResult\",\"text\":\"finish result\",\"type\":\"STRING\"},{\"key\":\"finishRemark\",\"text\":\"finish remark\",\"type\":\"STRING\"},{\"key\":\"finishTime\",\"text\":\"finish time\",\"type\":\"STRING\"},{\"key\":\"finishOperator\",\"text\":\"finish operator\",\"type\":\"STRING\"}]}]', 41, 'zh_en');
INSERT INTO `report_meta` VALUES (42, 2, 'qDetailCallOutAudio_issue', '[{\"key\":\"BASIC_VALUE\",\"text\":\"通話信息\",\"sub\":[{\"key\":\"call_uuid\",\"text\":\"檔名\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"staff_id\",\"text\":\"坐席ID\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"staff_name\",\"text\":\"坐席姓名\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"department\",\"text\":\"所屬部門\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"call_time\",\"text\":\"通話開始時間\",\"type\":\"DATETIME\",\"rule\":\"LAST\"},{\"key\":\"duration\",\"text\":\"通話時間\",\"type\":\"DURATION\",\"rule\":\"SUM\"},{\"key\":\"customer_id\",\"text\":\"客戶ID\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"customer_name\",\"text\":\"客戶姓名\",\"type\":\"STRING\",\"rule\":\"LAST\"},{\"key\":\"customer_phone\",\"text\":\"客戶電話\",\"type\":\"STRING\",\"rule\":\"LAST\"},{\"key\":\"call_id\",\"text\":\"call_id\",\"type\":\"STRING\",\"rule\":\"COMBINE\"}]},{\"key\":\"USER_VALUE\",\"text\":\"隨錄字段\",\"sub\":[]},{\"key\":\"ISSUE_VALUE\",\"text\":\"問題件\",\"sub\":[{\"key\":\"initTime\",\"text\":\"發起時間\",\"type\":\"STRING\"},{\"key\":\"issueTypeName\",\"text\":\"問題件類型\",\"type\":\"STRING\"},{\"key\":\"selectedRuleInfo\",\"text\":\"選定規則\",\"type\":\"STRING\"},{\"key\":\"initRemark\",\"text\":\"發起問題件描述\",\"type\":\"STRING\"},{\"key\":\"initOperator\",\"text\":\"發起人員\",\"type\":\"STRING\"},{\"key\":\"issueStatus\",\"text\":\"處理狀態\",\"type\":\"STRING\"},{\"key\":\"handleTime\",\"text\":\"處理時間\",\"type\":\"STRING\"},{\"key\":\"handleOperator\",\"text\":\"處理人員\",\"type\":\"STRING\"},{\"key\":\"handleResult\",\"text\":\"處理結果\",\"type\":\"STRING\"},{\"key\":\"handleRemark\",\"text\":\"處理結果細則\",\"type\":\"STRING\"},{\"key\":\"finishResult\",\"text\":\"複檢結果\",\"type\":\"STRING\"},{\"key\":\"finishRemark\",\"text\":\"結案細則\",\"type\":\"STRING\"},{\"key\":\"finishTime\",\"text\":\"結案時間\",\"type\":\"STRING\"},{\"key\":\"finishOperator\",\"text\":\"結案人員\",\"type\":\"STRING\"}]}]', 42, 'zh_tw');
INSERT INTO `report_meta` VALUES (43, 2, 'qDetailCallOutAudio_appeal', '[{\"key\":\"BASIC_VALUE\",\"text\":\"通话信息\",\"sub\":[{\"key\":\"call_uuid\",\"text\":\"档名\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"staff_id\",\"text\":\"坐席ID\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"staff_name\",\"text\":\"坐席姓名\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"department\",\"text\":\"所属部门\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"call_time\",\"text\":\"通话开始时间\",\"type\":\"DATETIME\",\"rule\":\"LAST\"},{\"key\":\"duration\",\"text\":\"通话时间\",\"type\":\"DURATION\",\"rule\":\"SUM\"},{\"key\":\"customer_id\",\"text\":\"客户ID\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"customer_name\",\"text\":\"客户姓名\",\"type\":\"STRING\",\"rule\":\"LAST\"},{\"key\":\"customer_phone\",\"text\":\"客户电话\",\"type\":\"STRING\",\"rule\":\"LAST\"},{\"key\":\"call_id\",\"text\":\"call_id\",\"type\":\"STRING\",\"rule\":\"COMBINE\"}]},{\"key\":\"USER_VALUE\",\"text\":\"随录字段\",\"sub\":[]},{\"key\":\"APPEAL_VALUE\",\"text\":\"申诉件\",\"sub\":[{\"key\":\"initOperator\",\"text\":\"申诉人\",\"type\":\"STRING\"},{\"key\":\"initTime\",\"text\":\"申诉时间\",\"type\":\"STRING\"},{\"key\":\"initRemark\",\"text\":\"申诉原因\",\"type\":\"STRING\"},{\"key\":\"appealStatus\",\"text\":\"申诉状态\",\"type\":\"STRING\"},{\"key\":\"handleRemark\",\"text\":\"申诉处理备注\",\"type\":\"STRING\"},{\"key\":\"handleResult\",\"text\":\"申诉处理结果\",\"type\":\"STRING\"},{\"key\":\"finishTime\",\"text\":\"结案时间\",\"type\":\"STRING\"},{\"key\":\"appealScore\",\"text\":\"得分\",\"type\":\"STRING\"}]}]', 43, 'zh_cn');
INSERT INTO `report_meta` VALUES (44, 2, 'qDetailCallOutAudio_appeal', '[{\"key\":\"BASIC_VALUE\",\"text\":\"call info\",\"sub\":[{\"key\":\"call_uuid\",\"text\":\"call uuid\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"staff_id\",\"text\":\"staff id\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"staff_name\",\"text\":\"staff name\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"department\",\"text\":\"department\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"call_time\",\"text\":\"call time\",\"type\":\"DATETIME\",\"rule\":\"LAST\"},{\"key\":\"duration\",\"text\":\"duration\",\"type\":\"DURATION\",\"rule\":\"SUM\"},{\"key\":\"customer_id\",\"text\":\"customer id\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"customer_name\",\"text\":\"customer name\",\"type\":\"STRING\",\"rule\":\"LAST\"},{\"key\":\"customer_phone\",\"text\":\"customer phone\",\"type\":\"STRING\",\"rule\":\"LAST\"},{\"key\":\"call_id\",\"text\":\"call id\",\"type\":\"STRING\",\"rule\":\"COMBINE\"}]},{\"key\":\"USER_VALUE\",\"text\":\"user value\",\"sub\":[]},{\"key\":\"APPEAL_VALUE\",\"text\":\"appeal value\",\"sub\":[{\"key\":\"initOperator\",\"text\":\"init operator\",\"type\":\"STRING\"},{\"key\":\"initTime\",\"text\":\"init time\",\"type\":\"STRING\"},{\"key\":\"initRemark\",\"text\":\"init remark\",\"type\":\"STRING\"},{\"key\":\"appealStatus\",\"text\":\"appeal status\",\"type\":\"STRING\"},{\"key\":\"handleRemark\",\"text\":\"handle remark\",\"type\":\"STRING\"},{\"key\":\"handleResult\",\"text\":\"handle result\",\"type\":\"STRING\"},{\"key\":\"finishTime\",\"text\":\"finish time\",\"type\":\"STRING\"},{\"key\":\"appealScore\",\"text\":\"appeal score\",\"type\":\"STRING\"}]}]', 44, 'zh_en');
INSERT INTO `report_meta` VALUES (45, 2, 'qDetailCallOutAudio_appeal', '[{\"key\":\"BASIC_VALUE\",\"text\":\"通話信息\",\"sub\":[{\"key\":\"call_uuid\",\"text\":\"檔名\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"staff_id\",\"text\":\"坐席ID\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"staff_name\",\"text\":\"坐席姓名\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"department\",\"text\":\"所屬部門\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"call_time\",\"text\":\"通話開始時間\",\"type\":\"DATETIME\",\"rule\":\"LAST\"},{\"key\":\"duration\",\"text\":\"通話時間\",\"type\":\"DURATION\",\"rule\":\"SUM\"},{\"key\":\"customer_id\",\"text\":\"客戶ID\",\"type\":\"STRING\",\"rule\":\"COMBINE\"},{\"key\":\"customer_name\",\"text\":\"客戶姓名\",\"type\":\"STRING\",\"rule\":\"LAST\"},{\"key\":\"customer_phone\",\"text\":\"客戶電話\",\"type\":\"STRING\",\"rule\":\"LAST\"},{\"key\":\"call_id\",\"text\":\"call_id\",\"type\":\"STRING\",\"rule\":\"COMBINE\"}]},{\"key\":\"USER_VALUE\",\"text\":\"隨錄字段\",\"sub\":[]},{\"key\":\"APPEAL_VALUE\",\"text\":\"申訴件\",\"sub\":[{\"key\":\"initOperator\",\"text\":\"申訴人\",\"type\":\"STRING\"},{\"key\":\"initTime\",\"text\":\"申訴時間\",\"type\":\"STRING\"},{\"key\":\"initRemark\",\"text\":\"申訴原因\",\"type\":\"STRING\"},{\"key\":\"appealStatus\",\"text\":\"申訴狀態\",\"type\":\"STRING\"},{\"key\":\"handleRemark\",\"text\":\"申訴處理備注\",\"type\":\"STRING\"},{\"key\":\"handleResult\",\"text\":\"申訴處理結果\",\"type\":\"STRING\"},{\"key\":\"finishTime\",\"text\":\"結案時間\",\"type\":\"STRING\"},{\"key\":\"appealScore\",\"text\":\"得分\",\"type\":\"STRING\"}]}]', 45, 'zh_tw');
COMMIT;

-- ----------------------------
-- Table structure for report_task
-- ----------------------------
CREATE TABLE `report_task` (
                               `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
                               `report_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'report_type',
                               `report_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'report_name',
                               `create_time` bigint(20) DEFAULT '0' COMMENT 'the timestamp of creating time' COMMENT 'create_time',
                               `status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT ':new|runing|finish',
                               `filter_condition` varchar(2048) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'filter_condition',
                               `update_time` bigint(20) NOT NULL DEFAULT '0' COMMENT 'update_time',
                               `enterprise` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '0' COMMENT 'enterprise',
                               PRIMARY KEY (`id`) USING BTREE,
                               KEY `idx_status` (`status`) USING BTREE,
                               KEY `idx_create_time` (`create_time`) USING BTREE,
                               KEY `idx_report_type` (`report_type`) USING BTREE,
                               KEY `idx_report_name` (`report_name`) USING BTREE,
                               KEY `idx_sct` (`status`,`create_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=172 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of report_task
-- ----------------------------
BEGIN;
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

DROP TABLE IF EXISTS `QRTZ_BLOB_TRIGGERS`;
DROP TABLE IF EXISTS `QRTZ_CALENDARS`;
DROP TABLE IF EXISTS `QRTZ_CRON_TRIGGERS`;
DROP TABLE IF EXISTS `QRTZ_FIRED_TRIGGERS`;
DROP TABLE IF EXISTS `QRTZ_JOB_DETAILS`;
DROP TABLE IF EXISTS `QRTZ_LOCKS`;
DROP TABLE IF EXISTS `QRTZ_PAUSED_TRIGGER_GRPS`;
DROP TABLE IF EXISTS `QRTZ_SCHEDULER_STATE`;
DROP TABLE IF EXISTS `QRTZ_SIMPLE_TRIGGERS`;
DROP TABLE IF EXISTS `QRTZ_SIMPROP_TRIGGERS`;
DROP TABLE IF EXISTS `QRTZ_TRIGGERS`;
DROP TABLE IF EXISTS `report_data`;
DROP TABLE IF EXISTS `report_meta`;
DROP TABLE IF EXISTS `report_task`;