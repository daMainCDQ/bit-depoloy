-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
-- -----------------------------------------------------
-- Table `Relation_RuleGroup_Silence`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Relation_RuleGroup_Silence` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `rg_uuid` VARCHAR(32) NOT NULL,
  `sil_uuid` VARCHAR(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `rg_index` (`rg_uuid` ASC),
  INDEX `sil_index` (`sil_uuid` ASC))
ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE IF EXISTS `Relation_RuleGroup_Silence`;