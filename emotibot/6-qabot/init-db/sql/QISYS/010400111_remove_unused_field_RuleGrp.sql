-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `RuleGroup`
  DROP COLUMN `limit_speed`,
  DROP COLUMN `limit_silence`;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `RuleGroup`
  ADD COLUMN `limit_speed` INT NOT NULL DEFAULT 0,
  ADD COLUMN `limit_silence` FLOAT NOT NULL DEFAULT 0;