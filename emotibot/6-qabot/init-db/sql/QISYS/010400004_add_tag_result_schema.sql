-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- -----------------------------------------------------
-- Table `SegmentPredict`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SegmentPredict` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `segment_id` BIGINT UNSIGNED NOT NULL,
  `tag_id` BIGINT UNSIGNED NOT NULL,
  `score` INT NOT NULL,
  `match` VARCHAR(256) NOT NULL DEFAULT '',
  `match_text` VARCHAR(256) NOT NULL DEFAULT '',
  `create_time` BIGINT NOT NULL DEFAULT 0,
  `update_time` BIGINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `idx_tag` (`tag_id` ASC),
  INDEX `idx_segment` (`segment_id` ASC))
ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



-- -----------------------------------------------------
-- Table `Recommendations`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Recommendations` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `link_id` BIGINT UNSIGNED NOT NULL COMMENT 'the id in SentenceGroup table that this recommendation sentences belongs to.',
  `sentence` VARCHAR(128) NOT NULL DEFAULT '' COMMENT 'the recommendation words',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `recommendations_id_UNIQUE` (`id` ASC),
  INDEX `index_link_id` (`link_id` ASC))
ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
COMMENT = 'records the recommendation wording';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE IF EXISTS `SegmentPredict`;
DROP TABLE IF EXISTS `Recommendations`;