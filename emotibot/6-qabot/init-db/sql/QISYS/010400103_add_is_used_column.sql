-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `HistoryBoundary`
  DROP COLUMN `is_delete`,
  DROP INDEX `idx_query`,
  ADD COLUMN `is_used` TINYINT NOT NULL DEFAULT 1,
  ADD INDEX `idx_query` (`enterprise` ASC, `name`(8) ASC);

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `HistoryBoundary`
  DROP COLUMN `is_used`,
  DROP INDEX `idx_query`,
  ADD COLUMN `is_delete` TINYINT NOT NULL DEFAULT 0,
  ADD INDEX `idx_query` (`enterprise` ASC, `is_delete` ASC, `name`(8) ASC);