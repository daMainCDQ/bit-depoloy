-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- -----------------------------------------------------
-- Table `Navigation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Navigation` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` VARCHAR(32) NOT NULL,
  `name` VARCHAR(128) NOT NULL,
  `ignore_intent` TINYINT NOT NULL DEFAULT 0,
  `intent_name` VARCHAR(128) NOT NULL DEFAULT '',
  `intent_link_id` BIGINT NOT NULL DEFAULT 0,
  `enterprise` VARCHAR(32) NOT NULL,
  `create_time` BIGINT NOT NULL DEFAULT 0,
  `update_time` BIGINT NOT NULL DEFAULT 0,
  `is_delete` INT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `idx_link_id` (`intent_link_id` ASC),
  INDEX `idx_enterprise` (`enterprise` ASC),
  UNIQUE INDEX `uuid_UNIQUE` (`uuid` ASC),
  INDEX `idx_uuid` (`uuid` ASC),
  INDEX `idx_is_delete` (`is_delete` ASC),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT = 'record the call-in intent';


-- -----------------------------------------------------
-- Table `Relation_Nav_SenGrp`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Relation_Nav_SenGrp` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nav_id` BIGINT UNSIGNED NOT NULL,
  `sg_id` BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `idx_nav_id` (`nav_id` ASC),
  INDEX `idx_sg_id` (`sg_id` ASC))
ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT = 'records the node in the navigation flow';

ALTER TABLE `SentenceGroup`
ADD COLUMN `optional` TINYINT NOT NULL DEFAULT 0,
ADD COLUMN `type` TINYINT NOT NULL DEFAULT 0,
ADD INDEX `idx_type` (`type` ASC),
ADD INDEX `idx_optional` (`optional` ASC);


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP Table IF EXISTS `Navigation`;
DROP Table IF EXISTS `Relation_Nav_SenGrp`;

ALTER TABLE `SentenceGroup`
DROP COLUMN `optional`,
DROP COLUMN `type`;