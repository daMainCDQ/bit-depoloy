-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `QISYS`.`qa_sampletag`
ADD COLUMN `enterprise` varchar(36) NULL COMMENT '企业UUID' AFTER `update_time`;

ALTER TABLE `QISYS`.`qa_problem_item_type`
ADD COLUMN `enterprise` varchar(36) NULL COMMENT '企业UUID' AFTER `is_deleted`;

update qa_sampletag qs set qs.enterprise = 'bb3e3925f0ad11e7bd860242ac120003' where qs.enterprise is null;
update qa_problem_item_type set enterprise='bb3e3925f0ad11e7bd860242ac120003' where enterprise is null;


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back