-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
INSERT INTO `manually_cu_predict_result` SELECT * FROM CUPredictResult WHERE type in (0, 1, 10, 11, 12, 13, 60, 70, 71);

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
TRUNCATE manually_cu_predict_result;
