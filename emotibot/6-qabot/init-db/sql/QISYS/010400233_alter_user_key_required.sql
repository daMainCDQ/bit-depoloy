-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `UserKey`
  ADD COLUMN `required` TINYINT DEFAULT 0 NOT NULL COMMENT 'indicaton whether every new call must contains this key';
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `UserKey`
  DROP COLUMN `required`;