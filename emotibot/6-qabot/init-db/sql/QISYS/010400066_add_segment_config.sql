-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

CREATE TABLE `SegmentConfig` (
    `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT, 
    `enterprise` VARCHAR(32) NOT NULL,
    `merge_milliseconds` INT UNSIGNED NOT NULL default 0 COMMENT 'milliseconds for how long ',
    `enabled`  TINYINT NOT NULL DEFAULT false COMMENT 'determine is enabled or not',
    `create_time` BIGINT NOT NULL DEFAULT 0,
    `update_time` BIGINT NOT NULL DEFAULT 0,
    `is_delete` TINYINT NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT = 'segment configuration';

-- +migrate Down
DROP TABLE `SegmentConfig`;