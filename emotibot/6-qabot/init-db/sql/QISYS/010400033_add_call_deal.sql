-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `call` ADD `deal` TINYINT(4) NOT NULL default 0;
UPDATE `call` SET `deal` = 1 WHERE task_id in (select `task_id` FROM `task` WHERE deal > 0 );

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `call` DROP COLUMN `deal`;