-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `QISYS`.`qa_inspectorplan_parameter`
MODIFY COLUMN `qip_parameter` json NULL COMMENT '抽检 范围参数JOSN字符串' AFTER `qip_plan_code`;

ALTER TABLE `QISYS`.`qa_regular_quality_task_parameter`
MODIFY COLUMN `qiqp_parameter` json NULL COMMENT '抽检 范围参数JOSN字符串' AFTER `qiqp_regular_quality_task_id`;



-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `QISYS`.`qa_inspectorplan_parameter`
MODIFY COLUMN `qip_parameter` varchar(5000) NULL COMMENT '抽检 范围参数JOSN字符串' AFTER `qip_plan_code`;

ALTER TABLE `QISYS`.`qa_regular_quality_task_parameter`
MODIFY COLUMN `qiqp_parameter` varchar(5000) NULL COMMENT '抽检 范围参数JOSN字符串' AFTER `qiqp_regular_quality_task_id`;