-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
-- -----------------------------------------------------
-- Table `UserValue`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `UserValue` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `userkey_id` BIGINT UNSIGNED NOT NULL COMMENT 'the userkey id value belong to',
  `link_id` BIGINT UNSIGNED NOT NULL COMMENT 'parent table id, different type point to different table',
  `type` TINYINT NOT NULL DEFAULT 0 COMMENT '1 for group, 2 for call, 3 for sensitive word',
  `value` VARCHAR(256) NOT NULL COMMENT 'the matching value of group condition or the value of user input',
  `is_delete` TINYINT NOT NULL DEFAULT 0 COMMENT 'soft delete flag',
  `create_time` BIGINT NOT NULL COMMENT 'UTC timestamp of created time',
  `update_time` BIGINT NOT NULL COMMENT 'UTC timestamp of updated time',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `idx_user_key` (`userkey_id` ASC),
  INDEX `idx_link_id` (`link_id` ASC),
  INDEX `idx_type` (`type` ASC))
ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT = 'user input value for custom defined key';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP Table IF EXISTS `UserValue`;
