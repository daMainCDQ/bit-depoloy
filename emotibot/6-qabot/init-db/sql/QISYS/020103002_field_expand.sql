-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `QISYS`.`qa_issue_detail` 
MODIFY COLUMN `description` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '质检结果备注' AFTER `call_type`;

ALTER TABLE `QISYS`.`qa_appeal_detail` 
MODIFY COLUMN `description` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '质检结果备注' AFTER `call_type`;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

