-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `CUPredictResult` MODIFY `revise` INT;
ALTER TABLE `CUPredictResultGroup` MODIFY `revise` INT;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
UPDATE `CUPredictResult` set `revise`=-1 WHERE `type` IN(0,1);
UPDATE `CUPredictResultGroup` set `revise`=-1 WHERE `type` IN(0,1);
ALTER TABLE `CUPredictResult`  MODIFY `revise` TINYINT;
ALTER TABLE `CUPredictResultGroup` MODIFY `revise` TINYINT;
