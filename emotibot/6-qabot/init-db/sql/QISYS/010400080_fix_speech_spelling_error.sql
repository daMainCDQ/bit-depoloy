-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `call`
CHANGE `left_speach_ratio` `left_speech_ratio` int DEFAULT NULL COMMENT 'speech time ratio of left channel',
CHANGE `right_speach_ratio` `right_speech_ratio` int DEFAULT NULL COMMENT 'speech time ratio of left channel',
DROP INDEX `idx_left_speach_ratio`,
DROP INDEX `idx_right_speach_ratio`,
ADD INDEX `idx_left_speech_ratio` (`left_speech_ratio` ASC),
ADD INDEX `idx_right_speech_ratio` (`right_speech_ratio` ASC);

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `call`
CHANGE `left_speech_ratio` `left_speach_ratio` int DEFAULT NULL COMMENT 'speach time ratio of left channel',
CHANGE `right_speech_ratio` `right_speach_ratio` int DEFAULT NULL COMMENT 'speach time ratio of left channel',
DROP INDEX `idx_left_speech_ratio`,
DROP INDEX `idx_right_speech_ratio`,
ADD INDEX `idx_left_speach_ratio` (`left_speach_ratio` ASC),
ADD INDEX `idx_right_speach_ratio` (`right_speach_ratio` ASC);
