-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `InterposalRule`
ADD COLUMN `uuid` VARCHAR(32) NOT NULL,
ADD INDEX `idx_uuid` (`uuid` ASC);

ALTER TABLE `SpeedRule`
ADD COLUMN `uuid` VARCHAR(32) NOT NULL,
ADD INDEX `idx_uuid` (`uuid` ASC);


ALTER TABLE `SilenceRule`
ADD COLUMN `uuid` VARCHAR(32) NOT NULL,
ADD INDEX `idx_uuid` (`uuid` ASC);

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `InterposalRule`
DROP COLUMN `uuid`;

ALTER TABLE `SpeedRule`
DROP COLUMN `uuid`;

ALTER TABLE `SilenceRule`
DROP COLUMN `uuid`;