-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `SpeedRule`
ADD COLUMN `granularity` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '0 for the call, 1 for each segment';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `SpeedRule`
DROP COLUMN `granularity`;