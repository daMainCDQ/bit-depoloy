-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `Sentence`
  ADD INDEX `idx_duplicate_name` (`is_delete` ASC, `enterprise` ASC, `name` ASC);

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `Sentence`
  DROP INDEX `idx_duplicate_name`;