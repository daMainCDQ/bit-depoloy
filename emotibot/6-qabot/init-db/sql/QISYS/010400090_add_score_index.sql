-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `CUPredictResult`
    ADD INDEX `idx_lev_score` (`score` ASC, `type` ASC);

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

ALTER TABLE `CUPredictResult`
  DROP INDEX `idx_lev_score`;