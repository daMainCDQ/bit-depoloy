-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `SensitiveWord`
  ADD INDEX `idx_query` (`is_delete` ASC, `enterprise` ASC, `category_id` ASC);

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

ALTER TABLE `SensitiveWord`
  DROP INDEX `idx_query`;
