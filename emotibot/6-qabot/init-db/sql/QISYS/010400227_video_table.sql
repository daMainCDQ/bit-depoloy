-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

CREATE TABLE `identify_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `enterprise` varchar(128) NOT NULL COMMENT '企業id',
  `user_type` tinyint(4) NOT NULL COMMENT '用戶類型, 0:坐席, 1:客戶',
  `name` varchar(64) NOT NULL COMMENT '用戶名',
  `user_id` varchar(64) NOT NULL COMMENT '用戶id',
  `update_time` bigint(20) NOT NULL COMMENT '更新時間',
  `create_time` bigint(20) NOT NULL COMMENT '創建時間',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`,`user_id`),
  INDEX `ide_user_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='身份辨識之使用者表';

CREATE TABLE `identify_user_photo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `enterprise` varchar(128) NOT NULL COMMENT '企業id',
  `file_name` varchar(100) NOT NULL COMMENT '名稱',
  `user_id` bigint(20) NOT NULL COMMENT '用戶id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='身分辨識照片';

CREATE TABLE `video_frame_result` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `call_id` bigint(20) NOT NULL COMMENT '通話id',
  `video_time` bigint(20) NOT NULL COMMENT '影片時間',
  `user_id` bigint(20) NOT NULL COMMENT '對應userid',
  `emotion` varchar(20) NOT NULL COMMENT '情緒',
  PRIMARY KEY (`id`),
  INDEX `idx_call_id` (`call_id`),
  INDEX `idx_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='記錄video辨識結果';

CREATE TABLE `video_frame_result_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `call_id` bigint(20) NOT NULL COMMENT '通話id',
  `user_type` tinyint(4) NOT NULL COMMENT '類型0:坐席,1:客戶',
  `name` varchar(64) NOT NULL COMMENT '用戶名',
  `user_id` varchar(64) NOT NULL COMMENT '用戶id',
  `photo_id` bigint(20) NOT NULL COMMENT '照片id',
  `photo` varchar(100) NOT NULL COMMENT '照片',
  PRIMARY KEY (`id`),
  INDEX `idx_call_id` (`call_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='video辨識結果之user記錄';

INSERT INTO `video_frame_result_user` (`id`, `call_id`, `user_type`, `name`, `user_id`, `photo_id`, `photo`) VALUES(-1, 0, -2, '', '', 0, '');
INSERT INTO `video_frame_result_user` (`id`, `call_id`, `user_type`, `name`, `user_id`, `photo_id`, `photo`) VALUES(1, 0, -1, '', '', 0, '');


CREATE TABLE `video_identify_result` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `call_id` bigint(20) NOT NULL COMMENT '通話id',
  `parent_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '對應規則root id',
  `user_type` tinyint(4) NOT NULL COMMENT '用戶類別, 0:坐席, 1:客戶',
  `result_code` tinyint(4) NOT NULL COMMENT '0:正確, 1,錯誤, -1,-2: 無法辨識',
  PRIMARY KEY (`id`),
  INDEX `idx_call_id` (`call_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='video身分辨識結果';

CREATE TABLE `qic_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `enterprise` varchar(128) NOT NULL COMMENT '企業id',
  `module` varchar(50) NOT NULL COMMENT '模組名',
  `code` varchar(50) NOT NULL COMMENT '代碼',
  `value` varchar(1000) NOT NULL COMMENT '值',
  `update_time` bigint(20) NOT NULL COMMENT '更新時間',
  `status` tinyint(4) NOT NULL COMMENT '狀態: 0正常',
  PRIMARY KEY (`id`),
  INDEX `idx_module` (`module`, `code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='質檢系統設定值';

INSERT INTO `qic_config` (`id`, `enterprise`, `module`, `code`, `value`, `update_time`, `status`) VALUES
(1, 'default', 'identify_user', 'staff', 'on', 0, 0),
(2, 'default', 'identify_user', 'customer', 'on', 0, 0),
(3, 'default', 'identify_user', 'controller', 'on', 0, 0);
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
