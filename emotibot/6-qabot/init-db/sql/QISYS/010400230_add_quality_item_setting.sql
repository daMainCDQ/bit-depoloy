-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

delete from  QISYS.qa_base_dic_info WHERE dic_type IN ( 'times', 'effect_time' ) AND is_deleted = '0' AND enterprise IS NOT NULL;

INSERT INTO QISYS.qa_base_dic_info ( `dic_description`, `dic_type`, `type_name`, `dic_status`, `dic_key`, `dic_name`, `enterprise` ) (
        SELECT
        `dic_description`,
        `dic_type`,
        `type_name`,
        `dic_status`,
        `dic_key`,
        `dic_name`,
       e.uuid
        FROM
        QISYS.qa_base_dic_info,QISYS_auth.enterprises e
        WHERE
        dic_type IN ( 'times', 'effect_time' )

        AND is_deleted = '0'
        AND enterprise IS NULL
        );

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back