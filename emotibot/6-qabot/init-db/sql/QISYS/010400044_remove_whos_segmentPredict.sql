-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE SegmentPredict DROP COLUMN whos;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE SegmentPredict ADD COLUMN whos;