-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `call`
  ADD COLUMN `history_boundary_id` BIGINT UNSIGNED NOT NULL DEFAULT 0,
  ADD INDEX `idx_statistics_query` (`history_boundary_id` ASC, `call_time` ASC, `enterprise` ASC, `department` ASC, `staff_id` ASC, `staff_name` ASC) COMMENT 'index for statistics/staff/calls';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `call`
  DROP COLUMN `history_boundary_id`,
  DROP INDEX `idx_statistics_query`;