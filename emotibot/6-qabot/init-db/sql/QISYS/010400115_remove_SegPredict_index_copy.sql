-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `SegmentPredict`
  DROP INDEX `id_UNIQUE`;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `SegmentPredict`
  ADD UNIQUE INDEX `id_UNIQUE` (`id` ASC);