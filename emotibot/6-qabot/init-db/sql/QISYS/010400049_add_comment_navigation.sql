-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `Navigation` ADD COLUMN  `intent_comment` VARCHAR(256) NOT NULL DEFAULT '' COMMENT 'comment of intent, some third-party info can be put in here';


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `Navigation` DROP `intent_comment`;
