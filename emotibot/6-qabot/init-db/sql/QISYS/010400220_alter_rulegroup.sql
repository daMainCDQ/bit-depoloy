-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `RuleGroup`
  ADD COLUMN `ignore_sensitive` TINYINT(4) NOT NULL DEFAULT 0 AFTER `uuid`;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `RuleGroup`
  DROP COLUMN `ignore_sensitive`;