-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
DELETE FROM `Category` WHERE `is_delete`=1;
ALTER TABLE Category DROP COLUMN is_delete,
DROP COLUMN deletable;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE Category ADD COLUMN is_delete,
ADD COLUMN deletable;
