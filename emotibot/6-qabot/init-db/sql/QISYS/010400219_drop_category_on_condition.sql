-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `Condition`
    DROP COLUMN `category`;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `Condition`
    ADD COLUMN `category` VARCHAR(32) NULL DEFAULT NULL COMMENT 'the business category' after `customer_phone`;