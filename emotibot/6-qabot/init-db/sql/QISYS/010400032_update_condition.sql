-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
UPDATE `RuleGroupCondition` SET `upload_time` = 0 WHERE `upload_time` is NULL;
UPDATE `RuleGroupCondition` SET `file_name` = '' WHERE `file_name` is NULL;
UPDATE `RuleGroupCondition` SET `deal` = -1 WHERE `deal` is NULL;
UPDATE `RuleGroupCondition` SET `series` = '' WHERE `series` is NULL;
UPDATE `RuleGroupCondition` SET `staff_id` = '' WHERE `staff_id` is NULL;
UPDATE `RuleGroupCondition` SET `staff_name` = '' WHERE `staff_name` is NULL;
UPDATE `RuleGroupCondition` SET `extension` = '' WHERE `extension` is NULL;
UPDATE `RuleGroupCondition` SET `department` = '' WHERE `department` is NULL;
UPDATE `RuleGroupCondition` SET `customer_id` = '' WHERE `customer_id` is NULL;
UPDATE `RuleGroupCondition` SET `customer_name` = '' WHERE `customer_name` is NULL;
UPDATE `RuleGroupCondition` SET `customer_phone` = '' WHERE `customer_phone` is NULL;
UPDATE `RuleGroupCondition` SET `call_end` = 0 WHERE `call_end` is NULL;
UPDATE `RuleGroupCondition` SET `call_start` = '' WHERE `call_start` is NULL;

ALTER TABLE `RuleGroupCondition` 
    CHANGE `upload_time` `upload_time_start` BIGINT(20)  NOT NULL  DEFAULT 0  COMMENT 'the upload time of the file',
    CHANGE `file_name` `file_name` VARCHAR(256)  CHARACTER SET utf8mb4  COLLATE utf8mb4_unicode_ci  NOT NULL  DEFAULT ''  COMMENT 'file name of the file that user upload',
    CHANGE `deal` `deal` TINYINT(4)  NOT NULL  DEFAULT '-1'  COMMENT 'is it deal. 0 for fail, 1 for deal',
    CHANGE `series` `series` VARCHAR(128)  CHARACTER SET utf8mb4  COLLATE utf8mb4_unicode_ci  NOT NULL  DEFAULT ''  COMMENT 'series number of contract',
    CHANGE `staff_id` `staff_id` VARCHAR(64)  CHARACTER SET utf8mb4  COLLATE utf8mb4_unicode_ci  NOT NULL  DEFAULT ''  COMMENT 'staff id',
    CHANGE `staff_name` `staff_name` VARCHAR(64)  CHARACTER SET utf8mb4  COLLATE utf8mb4_unicode_ci  NOT NULL  DEFAULT ''  COMMENT 'staff name',
    CHANGE `extension` `extension` VARCHAR(32)  CHARACTER SET utf8mb4  COLLATE utf8mb4_unicode_ci  NOT NULL  DEFAULT ''  COMMENT 'extension number of the staff',
    CHANGE `department` `department` VARCHAR(64)  CHARACTER SET utf8mb4  COLLATE utf8mb4_unicode_ci  NOT NULL  DEFAULT ''  COMMENT 'the department of the staff belongs to',
    CHANGE `customer_id` `customer_id` VARCHAR(64)  CHARACTER SET utf8mb4  COLLATE utf8mb4_unicode_ci  NOT NULL  DEFAULT ''  COMMENT 'the id of the client',
    CHANGE `customer_name` `customer_name` VARCHAR(32)  CHARACTER SET utf8mb4  COLLATE utf8mb4_unicode_ci  NOT NULL  DEFAULT ''  COMMENT 'the name of the client',
    CHANGE `customer_phone` `customer_phone` VARCHAR(32)  CHARACTER SET utf8mb4  COLLATE utf8mb4_unicode_ci  NOT NULL  DEFAULT ''  COMMENT 'the phone number of client',
    CHANGE `call_end` `call_end` BIGINT(20)  NOT NULL  DEFAULT 0 COMMENT 'the time range, end time for calling time of file',
    CHANGE `call_start` `call_start` BIGINT(20)  NOT NULL  DEFAULT 0  COMMENT 'the time range, start time for calling time of file',
    DROP COLUMN `left_channel`,
    DROP COLUMN `right_channel`,
    ADD `left_channel_role`  TINYINT(4)  NOT NULL  DEFAULT 2  COMMENT 'the matching code for left channel role',
    ADD `right_channel_role` TINYINT(4)  NOT NULL  DEFAULT 2  COMMENT 'the matching code for right channel role',
    ADD `upload_time_end` BIGINT(20)  NOT NULL  DEFAULT 0 AFTER `upload_time_start`;
-- left_channel_role & right_channel_role is not used on the current system. so the current values dont need to migrate to new column.


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back


ALTER TABLE `RuleGroupCondition` 
    CHANGE `upload_time_start` `upload_time` BIGINT(20)   NULL  COMMENT 'the upload time of the file',
    CHANGE `file_name` `file_name` VARCHAR(256)  CHARACTER SET utf8mb4  COLLATE utf8mb4_unicode_ci   NULL  DEFAULT ''  COMMENT 'file name of the file that user upload',
    CHANGE `deal` `deal` TINYINT(4) NULL DEFAULT NULL  COMMENT 'is it deal. 0 for fail, 1 for deal',
    CHANGE `series` `series` VARCHAR(128)  CHARACTER SET utf8mb4  COLLATE utf8mb4_unicode_ci   NULL  DEFAULT ''  COMMENT 'series number of contract',
    CHANGE `staff_id` `staff_id` VARCHAR(64)  CHARACTER SET utf8mb4  COLLATE utf8mb4_unicode_ci   NULL  DEFAULT ''  COMMENT 'staff id',
    CHANGE `staff_name` `staff_name` VARCHAR(64)  CHARACTER SET utf8mb4  COLLATE utf8mb4_unicode_ci   NULL  DEFAULT ''  COMMENT 'staff name',
    CHANGE `extension` `extension` VARCHAR(32)  CHARACTER SET utf8mb4  COLLATE utf8mb4_unicode_ci   NULL  DEFAULT ''  COMMENT 'extension number of the staff',
    CHANGE `department` `department` VARCHAR(64)  CHARACTER SET utf8mb4  COLLATE utf8mb4_unicode_ci   NULL  DEFAULT ''  COMMENT 'the department of the staff belongs to',
    CHANGE `customer_id` `customer_id` VARCHAR(64)  CHARACTER SET utf8mb4  COLLATE utf8mb4_unicode_ci   NULL  DEFAULT ''  COMMENT 'the id of the client',
    CHANGE `customer_name` `customer_name` VARCHAR(32)  CHARACTER SET utf8mb4  COLLATE utf8mb4_unicode_ci   NULL  DEFAULT ''  COMMENT 'the name of the client',
    CHANGE `customer_phone` `customer_phone` VARCHAR(32)  CHARACTER SET utf8mb4  COLLATE utf8mb4_unicode_ci   NULL  DEFAULT ''  COMMENT 'the phone number of client',
    CHANGE `call_end` `call_end` BIGINT(20)   NULL  COMMENT 'the time range, end time for calling time of file',
    CHANGE `call_start` `call_start` BIGINT(20)   NULL  DEFAULT '0'  COMMENT 'the time range, start time for calling time of file',
    ADD `left_channel` VARCHAR(16)   NULL  COMMENT 'who’s voice is in the left channel of audio file',
    ADD `right_channel` VARCHAR(16)   NULL  COMMENT 'who’s voice is in the right channel of audio file',
    DROP COLUMN `left_channel_role`,
    DROP COLUMN `right_channel_role`,
    DROP COLUMN `upload_time_end`;

    UPDATE `RuleGroupCondition` SET `left_channel` = 0;
    UPDATE `RuleGroupCondition` SET `right_channel` = 1;