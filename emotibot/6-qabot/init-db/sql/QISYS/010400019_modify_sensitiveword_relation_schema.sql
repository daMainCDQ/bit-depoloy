-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- because there relation between sensitive word and science
-- might be staff(0) or customer(1)
ALTER TABLE `Relation_SensitiveWord_Sentence`
ADD COLUMN `type` TINYINT UNSIGNED NOT NULL DEFAULT 0 COMMENT '0 for staff, 1 for customer';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `Relation_SensitiveWord_Sentence`
DROP COLUMN `type`;