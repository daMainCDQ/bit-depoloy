-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `CallGroup`
  ADD COLUMN `history_boundary_id` BIGINT UNSIGNED NOT NULL DEFAULT 0;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `CallGroup`
  DROP COLUMN `history_boundary_id`;