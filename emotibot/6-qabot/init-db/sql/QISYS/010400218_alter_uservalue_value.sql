-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `UserValue`
  Modify COLUMN `Value` VARBINARY(1024);

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `UserValue`
  Modify COLUMN `Value` VARCHAR(256);