-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `RuleGroupCondition`
CHANGE `rg_id` `link_id` BIGINT UNSIGNED NOT NULL COMMENT 'parent level table id, different level point to different table',
ADD COLUMN `level` TINYINT NOT NULL DEFAULT 1 COMMENT '1 for RuleGroup, 5 for conversation Rule, 6 for SilenceRule, 7 for SpeedRule, 8 for InterposalRule' AFTER `link_id`,
ADD INDEX `idx_link_id` (`link_id` ASC),
ADD INDEX `idx_level` (`type` ASC),
DROP INDEX `idx_rg_id`;


-- +migrate Down
ALTER TABLE `RuleGroupCondition`
CHANGE `link_id` `rg_id` BIGINT UNSIGNED NOT NULL COMMENT 'id in  RuleGroup table',
ADD INDEX `idx_rg_id` (`rg_id` ASC),
DROP COLUMN `level`,
DROP INDEX `idx_link_id`,
DROP INDEX `idx_level`;
