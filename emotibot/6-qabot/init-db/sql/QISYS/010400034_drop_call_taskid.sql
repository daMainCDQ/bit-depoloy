-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `call` 
    DROP COLUMN `task_id`,
    DROP INDEX `idx_task_id`;
-- WARNING: even though, the task id can be added back, the relation will never back again.

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

ALTER TABLE `call`
    ADD COLUMN `task_id` bigint(11) unsigned NOT NULL COMMENT 'id in the task table' AFTER `call_id`,
    ADD INDEX `idx_task_id` (`task_id` ASC);
-- since we lost task id info, we can only add random task back to avoid total panic.
UPDATE `call` SET `task_id` = 1;