-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied


ALTER TABLE `ConversationFlow`
ADD COLUMN `min` TINYINT NOT NULL DEFAULT 0  COMMENT 'must repeat at least time';

ALTER TABLE `Sentence`
ADD COLUMN `category_id` BIGINT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'the category this sentence belongs to';

CREATE TABLE IF NOT EXISTS `Category` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(128) NOT NULL,
  `enterprise` VARCHAR(32) NOT NULL,
  `is_delete` TINYINT NOT NULL DEFAULT 0,
  `deletable` TINYINT NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `unique_name` (`name` ASC, `enterprise` ASC),
  INDEX `idx_enterprise` (`enterprise` ASC),
  INDEX `idx_is_delete` (`is_delete` ASC))
ENGINE = InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `ConversationFlow`
DROP COLUMN `min`;

ALTER TABLE `Sentence`
DROP COLUMN `category_id`;

DROP TABLE IF EXISTS `Category`;