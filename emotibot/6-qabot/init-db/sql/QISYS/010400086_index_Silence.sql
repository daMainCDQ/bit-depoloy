-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `SilenceRule`
  ADD INDEX `idx_query` (`is_delete` ASC, `enterprise` ASC),
  ADD INDEX `idx_create_time` (`create_time` ASC);

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

ALTER TABLE `SilenceRule`
  DROP INDEX `idx_query`,
  DROP INDEX `idx_create_time`;
