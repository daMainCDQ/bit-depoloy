-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `Condition`
  ADD COLUMN `connected_call_type` TINYINT NOT NULL DEFAULT 2 COMMENT '0 is with condition, 1 is no condition, 2 is not selected';
  
UPDATE `Condition` SET `connected_call_type` = 0 WHERE `type`= 2 AND `level` = 1;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `Condition`
  DROP COLUMN `connected_call_type`;