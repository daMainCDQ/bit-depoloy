-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE  `Relation_GroupRule_CallGroup` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` TINYINT NOT NULL COMMENT '1 for RuleGroup, 5 for conversation Rule, 6 for SilenceRule, 7 for SpeedRule, 8 for InterposalRule',
  `link_id` BIGINT UNSIGNED NOT NULL COMMENT 'point to the id in the table based on the type attribute',
  `cgc_id` BIGINT UNSIGNED NOT NULL COMMENT 'is the id in the CallGroupCondition table',
  PRIMARY KEY (`id`),
  INDEX `idx_query_cg` (`type` ASC, `cgc_id` ASC))
ENGINE = InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `Relation_GroupRule_CallGroup`;