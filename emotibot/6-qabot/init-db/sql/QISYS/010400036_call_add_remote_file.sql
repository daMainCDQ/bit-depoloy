-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `call` ADD COLUMN `remote_file` VARCHAR(1024) COMMENT 'url of remote wav file';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `call` DROP `remote_file`;