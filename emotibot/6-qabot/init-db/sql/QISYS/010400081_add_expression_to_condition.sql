-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `Condition`
ADD COLUMN `expression` text COLLATE utf8mb4_unicode_ci COMMENT 'condition logic expression of UserKey' AFTER `right_channel_role`;

UPDATE `Condition`
SET `expression` = ""
WHERE `expression` IS NULL;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

ALTER TABLE `Condition`
DROP COLUMN `expression`;
