-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
DROP TABLE IF EXISTS StatisticValue;

CREATE TABLE `StatisticValue` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `key1` BIGINT UNSIGNED NOT NULL,
  `key2` BIGINT UNSIGNED NOT NULL DEFAULT 0,
  `key3` BIGINT UNSIGNED NOT NULL DEFAULT 0,
  `key4` BIGINT NOT NULL DEFAULT 0,
  `key5` BIGINT NOT NULL DEFAULT 0,
  `value` INT NOT NULL,
  `create_time` BIGINT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_group_query` (`create_time` ASC, `key1` ASC, `key2` ASC, `key3` ASC, `key4` ASC, `key5` ASC, `value` ASC))
ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `StatisticValue`;