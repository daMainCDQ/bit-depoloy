-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `CallGroupCondition`
ADD COLUMN `uuid` VARCHAR(32) NOT NULL,
ADD INDEX `idx_uuid` (`uuid` ASC);

ALTER TABLE `CallGroupConditionKey`
DROP COLUMN `staff_id`,
DROP COLUMN `staff_name`,
DROP COLUMN `extension`,
DROP COLUMN `department`,
DROP COLUMN `customer_id`,
DROP COLUMN `customer_name`,
DROP COLUMN `customer_phone`,
ADD COLUMN `is_delete` TINYINT NOT NULL DEFAULT 0,
ADD COLUMN `create_time` BIGINT NOT NULL DEFAULT 0,
ADD COLUMN `update_time` BIGINT NOT NULL DEFAULT 0,
ADD INDEX `idx_is_deleted` (`is_delete` ASC),
ADD INDEX `idx_create_time` (`create_time` ASC),
ADD INDEX `idx_update_time` (`update_time` ASC);


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `CallGroupCondition`
DROP COLUMN `uuid`;

ALTER TABLE `CallGroupConditionKey`
DROP COLUMN `is_delete`,
DROP COLUMN `create_time`,
DROP COLUMN `update_time`,
ADD COLUMN `staff_id` varchar(64) COLLATE 'utf8mb4_unicode_ci',
ADD COLUMN `staff_name` varchar(64) COLLATE 'utf8mb4_unicode_ci',
ADD COLUMN `extension` varchar(32) COLLATE 'utf8mb4_unicode_ci',
ADD COLUMN `department` varchar(64) COLLATE 'utf8mb4_unicode_ci',
ADD COLUMN `customer_id` varchar(64) COLLATE 'utf8mb4_unicode_ci',
ADD COLUMN `customer_name` varchar(32) COLLATE 'utf8mb4_unicode_ci',
ADD COLUMN `customer_phone` varchar(32) COLLATE 'utf8mb4_unicode_ci';
