-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `TFResult`
    CHANGE COLUMN `id` `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
-- Do nothing at rollback,
-- there are no way we want to rollback to non-auto-incre's id.