-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
-- author: huantianlv

ALTER TABLE `ReportData`
  ADD INDEX `idx_task_id`(`task_id`);

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

ALTER TABLE `ReportData`
  DROP INDEX `idx_task_id`;
