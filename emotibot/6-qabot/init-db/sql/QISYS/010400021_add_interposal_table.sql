-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
-- -----------------------------------------------------
-- Table `InterposalRule`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `InterposalRule` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(64) NOT NULL DEFAULT '',
  `enterprise` VARCHAR(32) NOT NULL,
  `score` INT NOT NULL,
  `overlapped_sec` INT NOT NULL,
  `overlapped_time` INT NOT NULL,
  `is_delete` TINYINT NOT NULL DEFAULT 0,
  `create_time` BIGINT NOT NULL,
  `update_time` BIGINT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `idx_enterprise` (`enterprise` ASC))
ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE IF EXISTS `InterposalRule`;