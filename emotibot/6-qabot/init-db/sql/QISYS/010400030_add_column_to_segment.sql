-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `Segment`
ADD COLUMN `status` INT NOT NULL DEFAULT 0,
ADD COLUMN `update_time` BIGINT NOT NULL DEFAULT 0,
ADD INDEX `idx_status` (`status` ASC);

UPDATE `Segment` SET `status` = 500 WHERE `asr_text` = '';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `Segment`
DROP COLUMN `status`;