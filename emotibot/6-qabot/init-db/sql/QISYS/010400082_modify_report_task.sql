-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `ReportTask`
  DROP COLUMN `start_time2`,
  DROP COLUMN `end_time2`,
  DROP COLUMN `strArr`,
  DROP COLUMN `strArr2`,
  DROP COLUMN `server_type`,
  DROP COLUMN `visit_type`,
  DROP COLUMN `creater_id`,
  DROP COLUMN `start_time`,
  DROP COLUMN `end_time`,
  MODIFY COLUMN `filter_condition` varchar(2048),
  ADD COLUMN `update_time` bigint NOT NULL DEFAULT 0,
  ADD INDEX `idx_status`(`status`),
  ADD INDEX `idx_create_time`(`create_time`),
  ADD INDEX `idx_report_type`(`report_type`),
  ADD INDEX `idx_report_name`(`report_name`);

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

ALTER TABLE `ReportTask`
  ADD COLUMN `start_time2` bigint(20) unsigned DEFAULT NULL,
  ADD COLUMN `end_time2` bigint(20) unsigned DEFAULT NULL,
  ADD COLUMN `strArr` text,
  ADD COLUMN `strArr2` text,
  ADD COLUMN `server_type` varchar(50),
  ADD COLUMN `visit_type` varchar(50),
  ADD COLUMN `creater_id` bigint(20) unsigned DEFAULT NULL,
  ADD COLUMN `start_time` bigint(20) unsigned DEFAULT NULL,
  ADD COLUMN `end_time` bigint(20) unsigned DEFAULT NULL,
  MODIFY COLUMN `filter_condition` varchar(1024),
  DROP COLUMN `update_time`,
  DROP INDEX `idx_status`,
  DROP INDEX `idx_create_time`,
  DROP INDEX `idx_report_type`,
  DROP INDEX `idx_report_name`;
