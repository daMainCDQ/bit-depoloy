-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `RuleGroupCondition` RENAME `Condition`;

-- +migrate Down
ALTER TABLE `Condition` RENAME `RuleGroupCondition`;