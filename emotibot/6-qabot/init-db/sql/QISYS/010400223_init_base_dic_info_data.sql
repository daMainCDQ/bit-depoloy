-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

INSERT INTO `qa_base_dic_info` VALUES ('1', '不限制', 'task_time', '发话时间', '1', '0', '不限制', '1', '0', '1', '0', '0');
INSERT INTO `qa_base_dic_info` VALUES ('2', '1天', 'task_time', '发话时间', '1', '1', '1天', '1', '0', '1', '0', '0');
INSERT INTO `qa_base_dic_info` VALUES ('3', '7天', 'task_time', '发话时间', '1', '2', '7天', '1', '0', '1', '0', '0');
INSERT INTO `qa_base_dic_info` VALUES ('4', '自订', 'task_time', '发话时间', '1', '3', '自订', '1', '0', '1', '0', '0');
INSERT INTO `qa_base_dic_info` VALUES ('5', '发起问题件', '1', '问题件质检状态', '1', '10', '发起问题件', '1', '0', '1', '0', '0');
INSERT INTO `qa_base_dic_info` VALUES ('6', '审核中', '1', '问题件质检状态', '1', '11', '审核中', '1', '0', '1', '0', '0');
INSERT INTO `qa_base_dic_info` VALUES ('7', '完结', '1', '问题件质检状态', '1', '12', '完结', '1', '0', '1', '0', '0');
INSERT INTO `qa_base_dic_info` VALUES ('8', '审核完成', '1', '问题件质检状态', '1', '13', '审核完成', '1', '0', '1', '0', '0');
INSERT INTO `qa_base_dic_info` VALUES ('9', '处理中', '1', '问题件质检状态', '1', '14', '处理中', '1', '0', '1', '0', '0');
INSERT INTO `qa_base_dic_info` VALUES ('10', '处理完成', '1', '问题件质检状态', '1', '15', '处理完成', '1', '0', '1', '0', '0');
INSERT INTO `qa_base_dic_info` VALUES ('11', '处理失败', '1', '问题件质检状态', '1', '16', '处理失败', '1', '0', '1', '0', '0');
INSERT INTO `qa_base_dic_info` VALUES ('12', '核实中', '1', '问题件质检状态', '1', '17', '核实中', '1', '0', '1', '0', '0');
INSERT INTO `qa_base_dic_info` VALUES ('13', '核实完结', '1', '问题件质检状态', '1', '18', '核实完结', '1', '0', '1', '0', '0');
INSERT INTO `qa_base_dic_info` VALUES ('14', '核实完成', '1', '问题件质检状态', '1', '19', '核实完成', '1', '0', '1', '0', '0');
INSERT INTO `qa_base_dic_info` VALUES ('15', '问题件发起次数', 'times', '次数', '1', '1', '33', '1', '0', '1', '0', '0');
INSERT INTO `qa_base_dic_info` VALUES ('16', '问题件时效', 'effect_time', '时效', '1', '1', '44', '1', '0', '1', '0', '0');
INSERT INTO `qa_base_dic_info` VALUES ('17', '申诉件发起次数', 'times', '次数', '1', '2', '55', '1', '0', '1', '0', '0');
INSERT INTO `qa_base_dic_info` VALUES ('19', '发起申诉件', '2', '申诉件质检状态', '1', '20', '发起申诉件', '1', '0', '1', '0', '0');
INSERT INTO `qa_base_dic_info` VALUES ('20', '审核中', '2', '申诉件质检状态', '1', '21', '审核中', '1', '0', '1', '0', '0');
INSERT INTO `qa_base_dic_info` VALUES ('21', '完结', '2', '申诉件质检状态', '1', '22', '完结', '1', '0', '1', '0', '0');
INSERT INTO `qa_base_dic_info` VALUES ('22', '审核完成', '2', '申诉件质检状态', '1', '23', '审核完成', '1', '0', '1', '0', '0');
INSERT INTO `qa_base_dic_info` VALUES ('23', '处理中', '2', '申诉件质检状态', '1', '24', '处理中', '1', '0', '1', '0', '0');
INSERT INTO `qa_base_dic_info` VALUES ('24', '处理完成', '2', '申诉件质检状态', '1', '25', '处理完成', '1', '0', '1', '0', '0');
INSERT INTO `qa_base_dic_info` VALUES ('25', '处理失败', '2', '申诉件质检状态', '1', '26', '处理失败', '1', '0', '1', '0', '0');
INSERT INTO `qa_base_dic_info` VALUES ('26', '核实中', '2', '申诉件质检状态', '1', '27', '核实中', '1', '0', '1', '0', '0');
INSERT INTO `qa_base_dic_info` VALUES ('27', '核实完结', '2', '申诉件质检状态', '1', '28', '核实完结', '1', '0', '1', '0', '0');
INSERT INTO `qa_base_dic_info` VALUES ('28', '核实完成', '2', '申诉件质检状态', '1', '29', '核实完成', '1', '0', '1', '0', '0');
INSERT INTO `qa_base_dic_info` VALUES ('29', '质检件类型', 'task_type', '质检件类型', '1', '1', '问题件', '1', '0', '1', '0', '0');
INSERT INTO `qa_base_dic_info` VALUES ('30', '质检件类型', 'task_type', '质检件类型', '1', '2', '申诉件', '1', '0', '1', '0', '0');
INSERT INTO `qa_base_dic_info` VALUES ('31', '申诉件时效', 'effect_time', '时效', '1', '2', '66', '1', '0', '1', '0', '0');

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

DELETE FROM `qa_base_dic_info` WHERE `id` <= 31;