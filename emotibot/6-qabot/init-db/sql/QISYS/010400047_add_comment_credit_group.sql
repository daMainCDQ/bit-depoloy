-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `CUPredictResultGroup` ADD COLUMN `description` VARCHAR(128) NOT NULL DEFAULT '' COMMENT 'the comment to describe the revision';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `CUPredictResultGroup` DROP `description`;