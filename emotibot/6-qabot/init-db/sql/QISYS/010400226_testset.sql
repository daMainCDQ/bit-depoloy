-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE IF NOT EXISTS `TestFile` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` VARCHAR(32) NOT NULL,
  `category_id` BIGINT UNSIGNED NOT NULL DEFAULT 0,
  `enterprise` VARCHAR(32) NOT NULL,
  `name` VARCHAR(128) NOT NULL,
  `status` TINYINT NOT NULL,
  `fileURI` VARCHAR(256) NOT NULL,
  `left_channel_role` TINYINT NOT NULL,
  `right_channel_role` TINYINT NOT NULL,
  `left_speed` FLOAT NOT NULL DEFAULT 0,
  `right_speed` FLOAT NOT NULL DEFAULT 0,
  `type` TINYINT NOT NULL,
  `error_msg` VARCHAR(256) NOT NULL,
  `create_time` BIGINT NOT NULL,
  `update_time` BIGINT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_category` (`enterprise` ASC, `category_id` ASC),
  INDEX `idx_uuid` (`uuid` ASC, `enterprise` ASC)
) ENGINE = InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='file(audio or text) uploaded by user';

CREATE TABLE IF NOT EXISTS `TestFileData` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `tfile_id` BIGINT UNSIGNED NOT NULL,
  `data` MEDIUMTEXT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_tfile_id` (`tfile_id` ASC)
) ENGINE = InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='file raw json data';

CREATE TABLE IF NOT EXISTS `TestTask_TestFile` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `ttask_uuid` VARCHAR(32) NOT NULL,
  `tfile_uuid` VARCHAR(32) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_ttask_uuid` (`ttask_uuid` ASC),
  INDEX `idx_tfile_uuid` (`tfile_uuid` ASC)
) ENGINE = InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='tasks to files relation';

CREATE TABLE IF NOT EXISTS `TestAnswer` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` VARCHAR(32) NOT NULL,
  `tfile_uuid` VARCHAR(32) NOT NULL,
  `rule_uuid` VARCHAR(32) NOT NULL,
  `ttask_uuid` VARCHAR(32) NOT NULL,
  `rule_type` TINYINT NOT NULL,
  `expect_result` TINYINT NOT NULL,
  `create_time` BIGINT NOT NULL,
  `update_time` BIGINT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uuid_UNIQUE` (`uuid` ASC)
) ENGINE = InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='answer set by user for given combination(task, file, rule)';

CREATE TABLE IF NOT EXISTS `TestTask` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(128) NOT NULL,
  `uuid` VARCHAR(32) NOT NULL,
  `enterprise` VARCHAR(32) NOT NULL,
  `status` TINYINT NOT NULL,
  `create_time` BIGINT NOT NULL,
  `update_time` BIGINT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uuid_UNIQUE` (`uuid` ASC)
) ENGINE = InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='a basic unit for running testset, grouping rules and files';

CREATE TABLE IF NOT EXISTS `TestReport` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` VARCHAR(32) NOT NULL,
  `task_uuid` VARCHAR(32) NOT NULL,
  `status` TINYINT NOT NULL,
  `create_time` BIGINT NOT NULL,
  `update_time` BIGINT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_task_uuid` (`task_uuid` ASC),
  INDEX `uuid` (`uuid` ASC)
) ENGINE = InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='history stat for one testset run';


CREATE TABLE IF NOT EXISTS `TestReportData` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `treport_id` BIGINT NULL,
  `data` MEDIUMTEXT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='raw json data for report';

CREATE TABLE IF NOT EXISTS `TFResult` (
  `id` INT NOT NULL,
  `uuid` VARCHAR(32) NOT NULL,
  `tfile_uuid` VARCHAR(32) NOT NULL,
  `treport_uuid` VARCHAR(32) NOT NULL,
  `status` TINYINT NOT NULL,
  `create_time` BIGINT NOT NULL,
  `update_time` BIGINT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uuid_UNIQUE` (`uuid` ASC),
  INDEX `idx_tfile_uuid` (`tfile_uuid` ASC),
  INDEX `idx_treport_uuid` (`treport_uuid` ASC)
) ENGINE = InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='computed result for given file and report';

CREATE TABLE IF NOT EXISTS `TFResultData` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `tf_result_id` BIGINT NOT NULL,
  `data` MEDIUMTEXT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_tf_result_id` (`tf_result_id` ASC)
) ENGINE = InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='raw json data for result';

CREATE TABLE IF NOT EXISTS `TestTask_Rule` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `ttask_uuid` VARCHAR(32) NOT NULL,
  `rule_uuid` VARCHAR(32) NOT NULL,
  `rule_type` TINYINT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_task_uuid` (`ttask_uuid` ASC)
) ENGINE = InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='tasks to rules relation';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back