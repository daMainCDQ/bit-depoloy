-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

/*!40101 SET NAMES utf8mb4 */;

CREATE DATABASE IF NOT EXISTS `QISYS` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `QISYS`;

START TRANSACTION;

--
-- Table structure for table `CUPredict`
--
CREATE TABLE IF NOT EXISTS `CUPredict` (
  `cu_id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `call_id` bigint(11) NOT NULL,
  `app_id` bigint(11) NOT NULL,
  `predict` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`cu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `Conversation`
--

CREATE TABLE IF NOT EXISTS `call` (
  `call_id` bigint(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'call id',
  `status` tinyint NOT NULL DEFAULT 0 comment 'is the call status',
  `call_uuid` varchar(32) NOT NULL COMMENT 'uuid for the call',
  `file_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'file name',
  `file_path` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'file path',
  `description` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'description the file',
  `duration` int NOT NULL DEFAULT 0 COMMENT 'duration of the call',
  `upload_time` bigint NOT NULL DEFAULT 0 COMMENT 'when the file is uploaded',
  `call_time` bigint NOT NULL DEFAULT 0 COMMENT 'the happened time to this call',
  `staff_id` varchar(64) NOT NULL DEFAULT '' COMMENT 'id of the staff who serve the call',
  `staff_name` varchar(64) NOT NULL DEFAULT '' COMMENT 'name of the staff who serve the call',
  `extension` varchar(32) NOT NULL DEFAULT '' COMMENT 'staff extension',
  `department` varchar(64) NOT NULL DEFAULT '' COMMENT 'staff deparmtment',
  `customer_id` varchar(64) NOT NULL DEFAULT '' COMMENT 'id of the customer in the call',
  `customer_name` varchar(32) NOT NULL DEFAULT '' COMMENT 'name of the customer in the call',
  `customer_phone` varchar(32) NOT NULL DEFAULT '' COMMENT 'phone number of the customer in the call',
  `enterprise` varchar(32) NOT NULL COMMENT 'enterprise who owns the file',
  `uploader` varchar(32) NOT NULL COMMENT 'who upload the file',
  `left_silence_time` float DEFAULT NULL COMMENT 'silence time of the left channel',
  `right_silence_time` float DEFAULT NULL COMMENT 'silence time of the right channel',
  `left_speed` float DEFAULT NULL COMMENT 'speak speed of the left channel',
  `right_speed` float DEFAULT NULL COMMENT 'speak speed of the right channel',
  `type` tinyint NOT NULL DEFAULT 0 COMMENT 'the type of this task is created, 0 for default which is from upload audio file, 1 for task from flow',
  `left_channel` tinyint NOT NULL DEFAULT 0 COMMENT '0 for default, 1 for host, 2 for guest',
  `right_channel` tinyint NOT NULL DEFAULT 0 COMMENT '0 for default, 1 for host, 2 for guest',
  `apply_group_list` varchar(512) NULL DEFAULT NULL COMMENT 'the list, id of group that this call would to be apply to.',
  PRIMARY KEY (`call_id`),
  INDEX `idx_status` (`status` ASC),
  INDEX `idx_call_time` (`call_time` ASC),
  INDEX `idx_upload_time` (`upload_time` ASC),
  INDEX `idx_staff_id` (`staff_id` ASC),
  INDEX `idx_staff_name` (`staff_name` ASC),
  INDEX `idx_extension` (`extension` ASC),
  INDEX `idx_department` (`department` ASC),
  INDEX `idx_customer_id` (`customer_id` ASC),
  INDEX `idx_customer_name` (`customer_name` ASC),
  INDEX `idx_customer_phone` (`customer_phone` ASC),
  INDEX `idx_enterprise` (`enterprise` ASC),
  INDEX `idx_user` (`uploader` ASC),
  INDEX `idx_duration` (`duration` ASC),
  INDEX `index_type` (`type` ASC),
  UNIQUE INDEX `call_uuid_UNIQUE` (`call_uuid` ASC)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `Conversation`
--

CREATE TABLE IF NOT EXISTS `task` (
  `task_id` bigint(11) unsigned NOT NULL AUTO_INCREMENT comment 'task id',
  `status` tinyint unsigned NOT NULL DEFAULT '0' comment 'task status',
  `description` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL comment 'description of the task',
  `deal` tinyint NOT NULL DEFAULT 0 comment 'is the call deal',
  `series` varchar(128) NOT NULL DEFAULT '' comment 'series for the task',
  `create_time` bigint NOT NULL DEFAULT 0 comment 'create time of the task',
  `update_time` bigint NOT NULL DEFAULT 0 comment 'last update of the task',
  `creator` varchar(32) NOT NULL comment 'who create the task',
  `updator` varchar(32) NOT NULL comment 'who is the last one update the task',
  PRIMARY KEY (`task_id`),
  INDEX `idx_deal` (`deal` ASC),
  INDEX `idx_serial` (`series` ASC),
  INDEX `idx_create_time` (`create_time` ASC),
  INDEX `idx_update_time` (`update_time` ASC),
  INDEX `idx_creator` (`creator` ASC),
  INDEX `idx_status` (`status` ASC)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `VoiceASR`
--

CREATE TABLE IF NOT EXISTS `VoiceASR` (
  `voice_id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `sentences` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `host_silence_time` float DEFAULT '0',
  `host_silence_rate` float DEFAULT '0',
  `guest_silence_time` float DEFAULT '0',
  `guest_silence_rate` float DEFAULT '0',
  PRIMARY KEY (`voice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- -----------------------------------------------------
-- Table `Segment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Segment` (
  `segment_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'the unique id for each segment',
  `call_id` BIGINT UNSIGNED NOT NULL COMMENT 'the call_id in Conversation table',
  `start_time` FLOAT UNSIGNED NOT NULL COMMENT 'the start time of segment in the audio file.',
  `end_time` FLOAT UNSIGNED NOT NULL COMMENT 'the end time of segment in the audio file',
  `channel` TINYINT UNSIGNED NOT NULL COMMENT 'the channel this segment in',
  `create_time` BIGINT NOT NULL COMMENT 'unix timestamp in second',
  PRIMARY KEY (`segment_id`),
  UNIQUE INDEX `segment_id_UNIQUE` (`segment_id` ASC),
  INDEX `idx_call_id` (`call_id` ASC),
  INDEX `idx_start_time` (`start_time` ASC),
  INDEX `idx_channel` (`channel` ASC))
ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='records each segment in the file';

-- -----------------------------------------------------
-- Table `SegEmotionScore`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `SegEmotionScore` (
  `seg_emotion_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'the unique id for each record in this table',
  `segment_id` BIGINT UNSIGNED NOT NULL COMMENT 'the segment_id in Segment table',
  `type` TINYINT NOT NULL COMMENT 'emotion type',
  `score` FLOAT NOT NULL COMMENT 'the score of each emotion',
  PRIMARY KEY (`seg_emotion_id`),
  UNIQUE INDEX `emotion_seg_id_UNIQUE` (`seg_emotion_id` ASC),
  INDEX `idx_seggment_id` (`segment_id` ASC),
  INDEX `idx_type` (`type` ASC),
  INDEX `idx_score` (`score` ASC))
ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='records each score for each emotion in the segment';


-- -----------------------------------------------------
-- Table `EmotionScore`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `EmotionScore` (
  `emotion_score_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'the unique id',
  `call_id` BIGINT UNSIGNED NOT NULL COMMENT 'the call_id in Conversation table',
  `type` TINYINT NOT NULL COMMENT 'emotion type',
  `channel` TINYINT NOT NULL,
  `score` FLOAT NOT NULL,
  PRIMARY KEY (`emotion_score_id`),
  INDEX `idx_call_id` (`call_id` ASC),
  INDEX `idx_type` (`type` ASC),
  INDEX `idx_channel` (`channel` ASC),
  INDEX `idx_score` (`score` ASC))
ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='records the summary score of each emotion for each file';

