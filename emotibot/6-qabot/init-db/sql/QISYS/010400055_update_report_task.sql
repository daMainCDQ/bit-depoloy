-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `ReportTask` ADD COLUMN `filter_condition` varchar(1024) DEFAULT '';
-- +migrate Down
ALTER TABLE `ReportTask` DROP COLUMN `filter_condition`;