-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

INSERT INTO `Condition` (`link_id`, `level`, `type`) (SELECT `id`, 5, 1 FROM `Rule`
WHERE `is_delete` = 0);

INSERT INTO `Condition` (`link_id`, `level`, `type`) (SELECT `id`, 6, 1 FROM `SilenceRule`
WHERE `is_delete` = 0);

INSERT INTO `Condition` (`link_id`, `level`, `type`) (SELECT `id`, 7, 1 FROM `SpeedRule`
WHERE `is_delete` = 0);

INSERT INTO `Condition` (`link_id`, `level`, `type`) (SELECT `id`, 8, 1 FROM `InterposalRule`
WHERE `is_delete` = 0);

-- +migrate Down

DELETE FROM `Condition` WHERE `level` = 5;
DELETE FROM `Condition` WHERE `level` = 6;
DELETE FROM `Condition` WHERE `level` = 7;
DELETE FROM `Condition` WHERE `level` = 8;
