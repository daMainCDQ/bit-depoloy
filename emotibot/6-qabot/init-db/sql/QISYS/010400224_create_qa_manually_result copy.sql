-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

CREATE TABLE `qa_manually_result` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `org_id` bigint(32) NOT NULL COMMENT '实体ID，当org_type为0：抽检，1:复核，2:问题件，3:申诉件',
  `org_type` tinyint(4) NOT NULL COMMENT '0：抽检，1:复核，2:问题件，3:申诉件',
  `state` tinyint(4) DEFAULT NULL COMMENT '状态：1.进行中 2.完结',
  `is_deleted` tinyint(4) DEFAULT NULL COMMENT '1-删除，0-未删除',
  `call_type` tinyint(5) NOT NULL COMMENT '通话类型 0-通话，1-关联通话',
  `call_id` bigint(20) NOT NULL COMMENT 'call_type 为0 时 callId 为1时callgroupId',
  `create_time` bigint(20) DEFAULT NULL,
  `call_group_id` bigint(20) DEFAULT NULL COMMENT 'callgroupId',
  PRIMARY KEY (`id`),
  KEY `idx_query` (`org_type`,`org_id`,`call_type`,`call_id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COMMENT='人工质检评分表：评分过程中存在，完成时更新回机器质检评分表，数据删除掉';


CREATE TABLE `qa_manually_result_modifyresult` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `manuallyresult_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'qa_manually_result 表id',
  `type` tinyint(4) DEFAULT '0' COMMENT '0 for default, 1 for RuleGroup. 10 for rule in positive, 11 for rule in negative, 20 for ConversationFlow, 30 for sentence group, 40 for sentence',
  `parent_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'For tag, parent_id points to the logic that contains it. For logic, parent_id points to the rule that contains it. For rule, parent_id points to the group that contains it. For group, parent_id is 0.\nparent_id points to some record in this table which means that it points to some predict_id',
  `org_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'the id for group/rule/logic/tag based on the type field',
  `valid` tinyint(4) DEFAULT '0' COMMENT '0 is default, 1 for meet the rule/logic/tag specification. 2 for not meet',
  `revise` int(11) DEFAULT NULL,
  `score` int(11) DEFAULT '0',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'the timestamp of creating time',
  `description` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'the comment to describe the revision',
  `revise_id` bigint(20) DEFAULT NULL COMMENT '评分表ID',
  `revise_type` tinyint(4) DEFAULT NULL COMMENT '0-CUPredictResult 1-CUPredictResultGroup',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `idx_quality_id` (`manuallyresult_id`)
) ENGINE=InnoDB AUTO_INCREMENT=37650 DEFAULT CHARSET=utf8mb4 COMMENT='人工质检评分详情表';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `QISYS`.`qa_manually_result`;
DROP TABLE `QISYS`.`qa_manually_result_modifyresult`;