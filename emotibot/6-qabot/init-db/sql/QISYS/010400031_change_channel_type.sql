-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE Segment MODIFY `channel` TINYINT NOT NULL;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE Segment MODIFY `channel` TINYINT UNSIGNED NOT NULL;

