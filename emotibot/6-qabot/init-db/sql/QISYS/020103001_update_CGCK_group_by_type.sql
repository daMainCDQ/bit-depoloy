-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `CallGroupConditionKey` CHANGE `group_by` `group_by` BIGINT NULL DEFAULT '0';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `CallGroupConditionKey` CHANGE `group_by` `group_by` VARCHAR(64) NULL DEFAULT NULL;
