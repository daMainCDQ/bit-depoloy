-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
RENAME TABLE `task` to `deprecated_task`;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
RENAME TABLE `deprecated_task` to `task`;