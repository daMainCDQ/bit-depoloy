-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
-- Global userkey has no meaning for currrent system, since callGroup already support custom key. 
-- Just disable it.
UPDATE `UserKey` SET `is_delete` = 1 WHERE `enterprise` = 'global';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
UPDATE `UserKey` SET `is_delete` = 0 WHERE `enterprise` = 'global';