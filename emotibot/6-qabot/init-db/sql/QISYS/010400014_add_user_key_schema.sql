-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- -----------------------------------------------------
-- Table `UserKey`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `UserKey` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'db id, no bussiness usage',
  `name` VARCHAR(256) NOT NULL COMMENT 'the displaying name for the UserKey',
  `enterprise` VARCHAR(32) NOT NULL COMMENT 'Enterprise ID',
  `inputname` VARCHAR(256) NOT NULL COMMENT 'the name of the UserKey',
  `type` TINYINT NOT NULL DEFAULT 0 COMMENT 'Call value schema, 1 for array of string',
  `is_delete` TINYINT NOT NULL DEFAULT 0 COMMENT 'soft delete flag',
  `create_time` BIGINT NOT NULL COMMENT 'UTC timestamp of created time',
  `update_time` BIGINT NOT NULL COMMENT 'UTC timestamp of updated time',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `idx_enterprise` (`enterprise` ASC),
  INDEX `idx_uuid` (`inputname` ASC))
ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT = 'user definiation input column';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP Table IF EXISTS `UserKey`;
