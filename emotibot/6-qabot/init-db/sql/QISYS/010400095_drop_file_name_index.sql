-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `call`
  DROP INDEX `idx_file_name`;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `call`
  ADD INDEX `idx_file_name`(`file_name` ASC);