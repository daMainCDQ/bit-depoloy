-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `QISYS`.`qa_inspectorplan`
ADD COLUMN `enterprise` varchar(36) NULL COMMENT '企业UUID' AFTER `plan_fail_describe`;

ALTER TABLE `QISYS`.`qa_qualitytaskinfo`
ADD COLUMN `enterprise` varchar(36) NULL COMMENT '企业UUID' AFTER `staff_id`;

ALTER TABLE `QISYS`.`qa_per_quality_rule`
ADD COLUMN `enterprise` varchar(36) NULL COMMENT '企业UUID' AFTER `is_deleted`;

ALTER TABLE `QISYS`.`qa_sample`
ADD COLUMN `enterprise` varchar(36) NULL COMMENT '企业ID' AFTER `submitter`;

ALTER TABLE `QISYS`.`qa_regular_quality_task`
ADD COLUMN `enterprise` varchar(36) NULL COMMENT '企业UUID' AFTER `qiqt_cron_execution`;

ALTER TABLE `QISYS`.`qa_base_dic_info`
ADD COLUMN `enterprise` varchar(36) NULL COMMENT '企业UUID' AFTER `is_deleted`;








update qa_inspectorplan p set p.enterprise = 'bb3e3925f0ad11e7bd860242ac120003' where p.enterprise is null;
update qa_qualitytaskinfo i set i.enterprise = 'bb3e3925f0ad11e7bd860242ac120003' where i.enterprise is null;
update  qa_per_quality_rule r set r.enterprise = 'bb3e3925f0ad11e7bd860242ac120003' where r.enterprise is null;
update  qa_sample qs set qs.enterprise='bb3e3925f0ad11e7bd860242ac120003' where qs.enterprise is null;
update qa_regular_quality_task q set q.enterprise = 'bb3e3925f0ad11e7bd860242ac120003' where q.enterprise is null;


ALTER TABLE `QISYS`.`qa_qualitytaskinfo`
MODIFY COLUMN `inspector` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '质检人员' AFTER `task_description`,
MODIFY COLUMN `reviewing_officer` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '复核人员' AFTER `inspector`;


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back