-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied


-- -----------------------------------------------------
-- Table `Relation_Call_RuleGroup`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Relation_Call_RuleGroup` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `call_id` BIGINT UNSIGNED NOT NULL,
  `rg_id` BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `idx_call_id` (`call_id` ASC),
  INDEX `idx_rg_id` (`rg_id` ASC))
ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `call` DROP COLUMN apply_group_list;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE IF EXISTS `Relation_Call_RuleGroup`;
ALTER TABLE `call` ADD COLUMN apply_group_list varchar(512) NULL DEFAULT NULL;