-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE IF NOT EXISTS `TrainedModel` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `create_time` BIGINT NOT NULL,
  `update_time` BIGINT NOT NULL,
  `enterprise` VARCHAR(32) NOT NULL,
  `status` TINYINT NOT NULL DEFAULT 0 COMMENT '0 is for training, 1 is for ready, 2 is for training error, 3 is for currently using, 4 is for deletetion',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `idx_enterprise` (`enterprise` ASC),
  INDEX `idx_status` (`status` ASC))
ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT = 'records the cu model that is used by enterprise';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE IF EXISTS `TrainedModel`;