-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `SegmentPredict`
ADD COLUMN  `whos` TINYINT NOT NULL DEFAULT 0,
ADD INDEX `idx_whos` (`whos` ASC);

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `SegmentPredict`
DROP COLUMN `whos`;