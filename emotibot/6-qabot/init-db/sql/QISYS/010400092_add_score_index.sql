-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `CUPredictResult`
   ADD INDEX `idx_call_score` (`call_id` ASC, `type` ASC,`score` ASC) ;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

ALTER TABLE `CUPredictResult`
  DROP INDEX `idx_call_score`;