-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE `GroupCallSync` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cg_condition_id` BIGINT UNSIGNED NOT NULL,
  `create_time` BIGINT NOT NULL,
  `call_id` BIGINT UNSIGNED NOT NULL,
  `is_trigger` TINYINT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `query_index` (`cg_condition_id` ASC))
ENGINE = InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `GroupCallSync`;


