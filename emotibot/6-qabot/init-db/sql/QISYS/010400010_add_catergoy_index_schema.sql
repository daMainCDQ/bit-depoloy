-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `Sentence`
ADD INDEX `idx_category` (`category_id` ASC);


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `Sentence`
DROP INDEX `idx_category`;

