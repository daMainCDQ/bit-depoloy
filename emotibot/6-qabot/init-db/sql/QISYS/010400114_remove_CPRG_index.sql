-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `CUPredictResultGroup`
  DROP INDEX `id_UNIQUE`,
  DROP INDEX `idx_valid`,
  DROP INDEX `idx_revise`;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `CUPredictResultGroup`
  ADD UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  ADD INDEX `idx_valid` (`valid` ASC),
  ADD INDEX `idx_revise` (`revise` ASC);