-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `call`
  ADD COLUMN `checksum` char(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'MD5 checksum of call upload or remote file';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

ALTER TABLE `call`
  DROP COLUMN `checksum`;