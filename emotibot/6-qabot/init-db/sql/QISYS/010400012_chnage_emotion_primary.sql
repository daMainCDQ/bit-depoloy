-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `EmotionScore`
CHANGE `emotion_score_id` `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT;



-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `EmotionScore`
CHANGE `id` `emotion_score_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT;