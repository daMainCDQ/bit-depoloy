-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `CallGroup`
  ADD COLUMN `is_revised` TINYINT NOT NULL DEFAULT 0 COMMENT '(0: false, 1: true) If callgroup already been edited by manual inspection' AFTER `is_delete`;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `CallGroup`
  DROP COLUMN `is_revised`;