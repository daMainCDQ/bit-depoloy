-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied


ALTER TABLE `call`
ADD COLUMN `task_id` bigint(11) unsigned NOT NULL COMMENT 'id in the task table' AFTER `call_id`,
ADD INDEX `idx_task_id` (`task_id` ASC);

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `call`
DROP COLUMN `task_id`;