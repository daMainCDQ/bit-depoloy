-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `CUPredictResult`
  DROP INDEX `predict_id_UNIQUE`,
  DROP INDEX `idx_valid`,
  DROP INDEX `idx_revise`;
  

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `CUPredictResult`
  ADD UNIQUE INDEX `predict_id_UNIQUE` (`predict_id` ASC),
  ADD INDEX `idx_valid` (`valid` ASC),
  ADD INDEX `idx_revise` (`revise` ASC);