-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE Category DROP INDEX unique_name,
ADD UNIQUE INDEX `unique_name` (`name` ASC, `enterprise` ASC, `type` ASC);

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE Category DROP INDEX unique_name,
ADD UNIQUE INDEX `unique_name` (`name` ASC, `enterprise` ASC);
