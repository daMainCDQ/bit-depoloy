-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- -----------------------------------------------------
-- Table `SegmentPredict`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `InspectorTask` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `is_delete` TINYINT NOT NULL DEFAULT 0,
  `name` VARCHAR(128) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL DEFAULT '',
  `enterprise` VARCHAR(32) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL,
  `description` VARCHAR(1024) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT '',
  `create_time` BIGINT NULL DEFAULT 0,
  `update_time` BIGINT NULL DEFAULT 0,
  `status` TINYINT NULL DEFAULT 0,
  `type` TINYINT NOT NULL DEFAULT 0 COMMENT '0 for default, 1 for flow usage',
  `uuid` VARCHAR(32) NOT NULL,
  `call_start` BIGINT NULL DEFAULT 0,
  `call_end` BIGINT NULL DEFAULT 0,
  `inspector_percentage` TINYINT UNSIGNED NOT NULL,
  `inspector_byperson` SMALLINT NULL DEFAULT 0,
  `publish_time` BIGINT NULL DEFAULT 0,
  `review_percentage` TINYINT UNSIGNED NULL DEFAULT 0,
  `review_byperson` SMALLINT NULL DEFAULT 0,
  `creator` VARCHAR(64) NOT NULL,
  `form_id` BIGINT UNSIGNED NOT NULL,
  `exclude_inspected` TINYINT UNSIGNED NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `idx_is_deleted` (`is_delete` ASC),
  INDEX `idx_enterprise` (`enterprise` ASC),
  INDEX `idx_uuid` (`uuid` ASC),
  INDEX `idx_create_time` (`create_time` ASC),
  INDEX `idx_update_time` (`update_time` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `Relation_InspectorTask_Outline` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `task_id` BIGINT UNSIGNED NOT NULL,
  `outline_id` BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_task_id` (`task_id` ASC),
  INDEX `idx_outline_id` (`outline_id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `Relation_InspectorTask_Staff` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `task_id` BIGINT UNSIGNED NOT NULL,
  `staff_id` VARCHAR(64) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_task_id` (`task_id` ASC),
  INDEX `idx_staff_id` (`staff_id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;


CREATE TABLE IF NOT EXISTS `Relation_InspectorTask_Call_Staff` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `task_id` BIGINT UNSIGNED NOT NULL,
  `staff_id` BIGINT UNSIGNED NOT NULL,
  `call_id` BIGINT UNSIGNED NOT NULL,
  `status` TINYINT UNSIGNED NULL DEFAULT 0,
  `type` TINYINT UNSIGNED NULL DEFAULT 0, 
  PRIMARY KEY (`id`),
  INDEX `idx_task_id` (`task_id` ASC),
  INDEX `idx_call_id` (`call_id` ASC),
  INDEX `idx_staff_id` (`staff_id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `Outline` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(128) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL DEFAULT '',
  `enterprise` VARCHAR(32) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL,
  `uuid` VARCHAR(32) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_uuid` (`uuid` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `ScoreForm` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(128) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL DEFAULT '',
  `enterprise` VARCHAR(32) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL,
   PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE IF EXISTS `InspectorTask`;