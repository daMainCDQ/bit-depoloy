-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `SentenceGroup`
  ADD COLUMN `is_same_role_dst` TINYINT NOT NULL DEFAULT 0 COMMENT 'whether the constraint of distance only compute the distance between the same role, 0 computes distance between all roles.' AFTER `range` ;


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `SentenceGroup`
  DROP COLUMN `is_same_role_dst`;