-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `QISYS`.`qa_qulityresult`
ADD COLUMN `opt_type_state` tinyint(4) NULL COMMENT '1.发起时快照，2.完结时快照' AFTER `opt_type`;

ALTER TABLE `QISYS`.`qa_qulityresult_modifyresult`
MODIFY COLUMN `description` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'the comment to describe the revision' AFTER `create_time`;

