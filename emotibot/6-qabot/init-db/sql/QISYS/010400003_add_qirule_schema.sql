-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- -----------------------------------------------------
-- Table `CUPredictResult`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `CUPredictResult` (
  `predict_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `call_id` BIGINT UNSIGNED NOT NULL DEFAULT 0,
  `type` TINYINT NULL DEFAULT 0 COMMENT '0 for default, 1 for group. 10 for rule in positive, 11 for rule in negative, 20 for logic, 30 for keyword, 31 for intent',
  `parent_id` BIGINT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'For tag, parent_id points to the logic that contains it. For logic, parent_id points to the rule that contains it. For rule, parent_id points to the group that contains it. For group, parent_id is 0.\nparent_id points to some record in this table which means that it points to some predict_id',
  `org_id` BIGINT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'the id for group/rule/logic/tag based on the type field',
  `valid` TINYINT NULL DEFAULT 0 COMMENT '0 is default, 1 for meet the rule/logic/tag specification. 2 for not meet',
  `revise` TINYINT NULL DEFAULT 0 COMMENT '0 for default, human check the result and revise it. 1 for match, 2 for not match',
  `score` INT NULL DEFAULT 0,
  `create_time` BIGINT NULL DEFAULT 0 COMMENT 'the timestamp of creating time',
  `update_time` BIGINT NULL DEFAULT 0 COMMENT 'the timestamp of updating time',
  PRIMARY KEY (`predict_id`),
  UNIQUE INDEX `predict_id_UNIQUE` (`predict_id` ASC),
  INDEX `idx_type` (`type` ASC),
  INDEX `idx_parent_id` (`parent_id` ASC),
  INDEX `idx_org_id` (`org_id` ASC),
  INDEX `idx_call_id` (`call_id` ASC),
  INDEX `idx_valid` (`valid` ASC),
  INDEX `idx_revise` (`revise` ASC),
  INDEX `idx_create_time` (`create_time` ASC))
ENGINE = InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- -----------------------------------------------------
-- Table `CUPredictMatch`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `CUPredictMatch` (
  `predict_score_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `predict_id` BIGINT UNSIGNED NOT NULL,
  `segment_id` BIGINT UNSIGNED NULL DEFAULT 0,
  `match_word` VARCHAR(128) NULL DEFAULT '',
  `create_time` BIGINT NULL DEFAULT 0,
  `update_time` BIGINT NULL DEFAULT 0,
  `is_delete` TINYINT NULL DEFAULT 0,
  PRIMARY KEY (`predict_score_id`),
  INDEX `idx_segment_id` (`segment_id` ASC),
  INDEX `idx_predict_id` (`predict_id` ASC),
  INDEX `idx_is_delete` (`is_delete` ASC),
  INDEX `idx_create_time` (`create_time` ASC),
  INDEX `idx_update_time` (`update_time` ASC))
ENGINE = InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE IF NOT EXISTS `RuleGroup` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `is_delete` TINYINT NOT NULL DEFAULT 0,
  `name` VARCHAR(128) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL DEFAULT '',
  `enterprise` VARCHAR(32) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL,
  `description` VARCHAR(1024) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT '',
  `create_time` BIGINT NULL DEFAULT 0,
  `update_time` BIGINT NULL DEFAULT 0,
  `is_enable` TINYINT NULL DEFAULT 1,
  `limit_speed` INT NULL DEFAULT 0,
  `limit_silence` FLOAT NULL DEFAULT 0,
  `type` TINYINT NOT NULL DEFAULT 0 COMMENT '0 for default, 1 for flow usage',
  `uuid` VARCHAR(32) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_is_deleted` (`is_delete` ASC),
  INDEX `idx_enterprise` (`enterprise` ASC),
  INDEX `idx_is_enable` (`is_enable` ASC),
  INDEX `idx_uuid` (`uuid` ASC),
  INDEX `idx_create_time` (`create_time` ASC),
  INDEX `idx_update_time` (`update_time` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `RuleGroupCondition` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'the unique id of condition id',
  `rg_id` BIGINT UNSIGNED NOT NULL COMMENT 'id in  RuleGroup table',
  `type` TINYINT NULL DEFAULT 1 COMMENT '1 for use to all file, 0 for used to the file that matches to below conditions ',
  `file_name` VARCHAR(256) NULL DEFAULT NULL COMMENT 'file name of the file that user upload',
  `deal` TINYINT NULL DEFAULT NULL COMMENT 'is it deal. 0 for fail, 1 for deal',
  `series` VARCHAR(128) NULL DEFAULT NULL COMMENT 'series number of contract',
  `upload_time` BIGINT NULL DEFAULT NULL COMMENT 'the upload time of the file',
  `staff_id` VARCHAR(64) NULL DEFAULT NULL COMMENT 'staff id',
  `staff_name` VARCHAR(64) NULL DEFAULT NULL COMMENT 'staff name',
  `extension` VARCHAR(32) NULL DEFAULT NULL COMMENT 'extension number of the staff',
  `department` VARCHAR(64) NULL DEFAULT NULL COMMENT 'the department of the staff belongs to',
  `customer_id` VARCHAR(64) NULL DEFAULT NULL COMMENT 'the id of the client',
  `customer_name` VARCHAR(32) NULL DEFAULT NULL COMMENT 'the name of the client',
  `customer_phone` VARCHAR(32) NULL DEFAULT NULL COMMENT 'the phone number of client',
  `category` VARCHAR(32) NULL DEFAULT NULL COMMENT 'the business category',
  `call_start` BIGINT NULL DEFAULT NULL COMMENT 'the time range, start time for calling time of file',
  `call_end` BIGINT NULL DEFAULT NULL COMMENT 'the time range, end time for calling time of file',
  `left_channel` VARCHAR(16) NULL DEFAULT NULL COMMENT 'who’s voice is in the left channel of audio file',
  `right_channel` VARCHAR(16) NULL DEFAULT NULL COMMENT 'who’s voice is in the right channel of audio file',
  PRIMARY KEY (`id`),
  INDEX `idx_rg_id` (`rg_id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
COMMENT = 'the condition used to match meta data of the file ';

CREATE TABLE IF NOT EXISTS `Rule` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `is_delete` TINYINT NOT NULL DEFAULT 0,
  `name` VARCHAR(128) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL DEFAULT '',
  `method` TINYINT NULL DEFAULT 0,
  `score` INT NULL DEFAULT 0,
  `description` VARCHAR(1024) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT '',
  `enterprise` VARCHAR(32) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL,
  `min` INT NOT NULL DEFAULT 0,
  `max` INT NOT NULL DEFAULT 0,
  `severity` TINYINT NOT NULL DEFAULT 0 COMMENT '0 is for normal rule, 1 is for severe rule',
  `uuid` VARCHAR(32) NOT NULL,
  `create_time` BIGINT NOT NULL DEFAULT 0,
  `update_time` BIGINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `idx_enterprise` (`enterprise` ASC),
  INDEX `idx_is_delete` (`is_delete` ASC),
  INDEX `idx_method` (`method` ASC),
  INDEX `idx_severity` (`severity` ASC),
  INDEX `idx_uuid` (`uuid` ASC),
  INDEX `idx_create_time` (`create_time` ASC),
  INDEX `idx_update_time` (`update_time` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `ConversationFlow` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `is_delete` TINYINT NOT NULL DEFAULT 0,
  `name` VARCHAR(128) NOT NULL DEFAULT '',
  `enterprise` VARCHAR(32) NOT NULL,
  `expression` TEXT NULL,
  `uuid` VARCHAR(32) NOT NULL,
  `create_time` BIGINT NOT NULL DEFAULT 0,
  `update_time` BIGINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `idx_enterprise` (`enterprise` ASC),
  INDEX `idx_is_delete` (`is_delete` ASC),
  INDEX `idx_uuid` (`uuid` ASC),
  INDEX `idx_create_time` (`create_time` ASC),
  INDEX `idx_update_time` (`update_time` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `SentenceGroup` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `is_delete` TINYINT NOT NULL DEFAULT 0,
  `name` VARCHAR(128) NOT NULL DEFAULT '',
  `enterprise` VARCHAR(32) NOT NULL,
  `role` TINYINT NOT NULL DEFAULT 0 COMMENT '0 is default, means to apply to all. 1 is apply to staff. 2 is apply to customer.',
  `position` TINYINT NOT NULL DEFAULT 0 COMMENT '0 is default. 1 is from top. 2 is from bottom.',
  `range` INT NOT NULL DEFAULT 0 COMMENT '0 is default. range is the constraint in n sentences. when the field, position, is set.',
  `uuid` VARCHAR(32) NOT NULL,
  `create_time` BIGINT NOT NULL DEFAULT 0,
  `update_time` BIGINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `index_enterprise` (`enterprise` ASC),
  INDEX `idx_is_delete` (`is_delete` ASC),
  INDEX `idx_uuid` (`uuid` ASC),
  INDEX `idx_create_time` (`create_time` ASC),
  INDEX `idx_update_time` (`update_time` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `Sentence` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `is_delete` TINYINT NOT NULL DEFAULT 0,
  `name` VARCHAR(128) NOT NULL DEFAULT '',
  `enterprise` VARCHAR(32) NOT NULL,
  `uuid` VARCHAR(32) NOT NULL,
  `create_time` BIGINT NOT NULL DEFAULT 0,
  `update_time` BIGINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `index_enterprise` (`enterprise` ASC),
  INDEX `index_is_delete` (`is_delete` ASC),
  INDEX `idx_uuid` (`uuid` ASC),
  INDEX `idx_create_time` (`create_time` ASC),
  INDEX `idx_update_time` (`update_time` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `Tag` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `is_delete` TINYINT NOT NULL DEFAULT 0,
  `name` VARCHAR(128) NOT NULL DEFAULT '',
  `type` TINYINT NULL DEFAULT 0 COMMENT '0 is default, 1 for keyword, 2 for intent. 3 for user response',
  `pos_sentences` LONGTEXT NULL,
  `neg_sentences` MEDIUMTEXT NULL,
  `create_time` BIGINT NOT NULL DEFAULT 0,
  `update_time` BIGINT NOT NULL DEFAULT 0,
  `enterprise` VARCHAR(32) NOT NULL,
  `uuid` VARCHAR(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `tag_id_UNIQUE` (`id` ASC),
  INDEX `idx_id_delete` (`is_delete` ASC),
  INDEX `idx_type` (`type` ASC),
  INDEX `idx_enterprise` (`enterprise` ASC),
  INDEX `idx_create_time` (`create_time` ASC),
  INDEX `idx_update_time` (`update_time` ASC),
  INDEX `idx_uuid` (`uuid` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;


CREATE TABLE IF NOT EXISTS `Relation_RuleGroup_Rule` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `rg_id` BIGINT UNSIGNED NOT NULL COMMENT 'rg_id is id in RuleGroup table',
  `rule_id` BIGINT UNSIGNED NOT NULL COMMENT 'rule_id  is rule_id in Rule table',
  INDEX `idx_rg_id` (`rg_id` ASC),
  INDEX `idx_rule_id` (`rule_id` ASC),
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
COMMENT = 'records which group has which rule';


CREATE TABLE IF NOT EXISTS `Relation_Rule_ConversationFlow` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `rule_id` BIGINT UNSIGNED NOT NULL,
  `cf_id` BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `index_rule_id` (`rule_id` ASC),
  INDEX `index_cf_od` (`cf_id` ASC),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;


CREATE TABLE IF NOT EXISTS `Relation_ConversationFlow_SentenceGroup` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cf_id` BIGINT UNSIGNED NOT NULL,
  `sg_id` BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `index_cf_id` (`cf_id` ASC),
  INDEX `index_sg_id` (`sg_id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;


CREATE TABLE IF NOT EXISTS `Relation_SentenceGroup_Sentence` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `sg_id` BIGINT UNSIGNED NOT NULL,
  `s_id` BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `idx_sg_id` (`sg_id` ASC),
  INDEX `idx_s_id` (`s_id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;


CREATE TABLE IF NOT EXISTS `Relation_Sentence_Tag` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `s_id` BIGINT UNSIGNED NOT NULL,
  `tag_id` BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `index_s_id` (`s_id` ASC),
  INDEX `index_tag_id` (`tag_id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE IF EXISTS `CUPredictResult`;
DROP TABLE IF EXISTS `CUPredictMatch`;
DROP TABLE IF EXISTS `RuleGroup`;
DROP TABLE IF EXISTS `Rule`;
DROP TABLE IF EXISTS `ConversationFlow`;
DROP TABLE IF EXISTS `SentenceGroup`;
DROP TABLE IF EXISTS `Sentence`;
DROP TABLE IF EXISTS `Tag`;
DROP TABLE IF EXISTS `Relation_Group_Rule`;
DROP TABLE IF EXISTS `Relation_Rule_ConversationFlow`;
DROP TABLE IF EXISTS `Relation_ConversationFlow_SentenceGroup`;
DROP TABLE IF EXISTS `Relation_SentenceGroup_Sentence`;
DROP TABLE IF EXISTS `Relation_Sentence_Tag`;
DROP TABLE IF EXISTS `RuleGroupCondition`;
