-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `call`
ADD COLUMN `left_speach_ratio` int DEFAULT NULL COMMENT 'speach time ratio of left channel' AFTER `right_silence_time`,
ADD COLUMN `right_speach_ratio` int DEFAULT NULL COMMENT 'speach time ratio of right channel' AFTER `left_speach_ratio`,
ADD INDEX `idx_left_speach_ratio` (`left_speach_ratio` ASC),
ADD INDEX `idx_right_speach_ratio` (`right_speach_ratio` ASC);

UPDATE `call`
SET `left_speach_ratio` = (`duration` - (`left_silence_time` * 1000)) / `duration` * 10000
WHERE `duration` IS NOT NULL AND `duration` > 0 AND `left_silence_time` IS NOT NULL;

UPDATE `call`
SET `right_speach_ratio` = (`duration` - (`right_silence_time` * 1000)) / `duration` * 10000
WHERE `duration` IS NOT NULL AND `duration` > 0 AND `right_silence_time` IS NOT NULL;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `call`
DROP COLUMN `left_speach_ratio`,
DROP COLUMN `right_speach_ratio`,
DROP INDEX `idx_left_speach_ratio`,
DROP INDEX `idx_right_speach_ratio`;