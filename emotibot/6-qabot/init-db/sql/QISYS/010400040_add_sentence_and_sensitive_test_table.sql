-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

CREATE TABLE `sentence_test` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `is_delete` tinyint(4) NOT NULL DEFAULT '0',
  `name` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'test sentence content',
  `enterprise` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uuid` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_time` bigint(20) NOT NULL DEFAULT '0',
  `update_time` bigint(20) NOT NULL DEFAULT '0',
  `category_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'the category this sentence belongs to',
  `tested_id` bigint(20) DEFAULT NULL COMMENT 'associate with sentence id',
  `hit_tags` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT '[]',
  `fail_tags` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT '[]',
  `match_text` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT '[]',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `index_enterprise` (`enterprise`),
  KEY `index_is_delete` (`is_delete`),
  KEY `idx_uuid` (`uuid`),
  KEY `idx_create_time` (`create_time`),
  KEY `idx_update_time` (`update_time`),
  KEY `idx_category` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `sentence_test_result` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `is_delete` tinyint(4) NOT NULL DEFAULT '0',
  `name` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `enterprise` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uuid` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_time` bigint(20) NOT NULL DEFAULT '0',
  `update_time` bigint(20) NOT NULL DEFAULT '0',
  `category_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'the category this sentence belongs to',
  `hit` int(11) DEFAULT NULL COMMENT 'num of correct case',
  `total` int(11) DEFAULT NULL COMMENT 'num of all test case',
  `accuracy` float DEFAULT NULL,
  `org_id` bigint(20) DEFAULT '0' COMMENT 'sentence id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `index_enterprise` (`enterprise`),
  KEY `index_is_delete` (`is_delete`),
  KEY `idx_uuid` (`uuid`),
  KEY `idx_create_time` (`create_time`),
  KEY `idx_update_time` (`update_time`),
  KEY `idx_category` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `sensitive_word_test` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `is_delete` tinyint(4) NOT NULL DEFAULT '0',
  `name` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'sensitive word content',
  `enterprise` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uuid` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_time` bigint(20) NOT NULL DEFAULT '0',
  `update_time` bigint(20) NOT NULL DEFAULT '0',
  `category_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'the category this sentence belongs to',
  `sw_id` bigint(20) DEFAULT NULL COMMENT 'sensitive word id',
  `hit_tags` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT '[]' COMMENT 'an array of segment id',
  `call_id` bigint(20) DEFAULT NULL COMMENT 'call id',
  `machine_valid` tinyint(4) DEFAULT '0' COMMENT 'result checked by machine',
  `human_valid` tinyint(4) DEFAULT '0' COMMENT 'result checked by human',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `index_enterprise` (`enterprise`),
  KEY `index_is_delete` (`is_delete`),
  KEY `idx_uuid` (`uuid`),
  KEY `idx_create_time` (`create_time`),
  KEY `idx_update_time` (`update_time`),
  KEY `idx_category` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `sensitive_word_test_result` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `is_delete` tinyint(4) NOT NULL DEFAULT '0',
  `name` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'audio file name',
  `enterprise` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uuid` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_time` bigint(20) NOT NULL DEFAULT '0',
  `update_time` bigint(20) NOT NULL DEFAULT '0',
  `category_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'the category this sentence belongs to',
  `hit` int(11) DEFAULT NULL COMMENT 'an array of sensitive word id',
  `total` int(11) DEFAULT NULL,
  `accuracy` float DEFAULT NULL,
  `call_id` bigint(20) DEFAULT '0' COMMENT 'call id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `index_enterprise` (`enterprise`),
  KEY `index_is_delete` (`is_delete`),
  KEY `idx_uuid` (`uuid`),
  KEY `idx_create_time` (`create_time`),
  KEY `idx_update_time` (`update_time`),
  KEY `idx_category` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE IF EXISTS `sentence_test`;
DROP TABLE IF EXISTS `sentence_test_result`;
DROP TABLE IF EXISTS `sensitive_word_test`;
DROP TABLE IF EXISTS `sensitive_word_test_result`;
