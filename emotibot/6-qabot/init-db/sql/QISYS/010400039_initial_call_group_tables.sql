-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `UserValue`
CHANGE COLUMN `type`
`type` TINYINT NOT NULL DEFAULT 0 COMMENT '1 for group, 2 for call, 3 for sensitive word, 4 for CallGroupCondition';

CREATE TABLE IF NOT EXISTS `CUPredictResultGroup` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `call_group_id` BIGINT UNSIGNED NOT NULL DEFAULT 0,
  `type` TINYINT NULL DEFAULT 0 COMMENT '0 for default, 1 for RuleGroup. 10 for rule in positive, 11 for rule in negative, 20 for ConversationFlow, 30 for sentence group, 40 for sentence',
  `parent_id` BIGINT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'For tag, parent_id points to the logic that contains it. For logic, parent_id points to the rule that contains it. For rule, parent_id points to the group that contains it. For group, parent_id is 0.\nparent_id points to some record in this table which means that it points to some predict_id',
  `org_id` BIGINT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'the id for group/rule/logic/tag based on the type field',
  `valid` TINYINT NULL DEFAULT 0 COMMENT '0 is default, 1 for meet the rule/logic/tag specification. 2 for not meet',
  `revise` TINYINT NULL DEFAULT 0 COMMENT '0 for default, human check the result and revise it. 1 for match, 2 for not match',
  `score` INT NULL DEFAULT 0,
  `create_time` BIGINT NULL DEFAULT 0 COMMENT 'the timestamp of creating time',
  `update_time` BIGINT NULL DEFAULT 0 COMMENT 'the timestamp of updating time',
  `call_id` BIGINT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `idx_type` (`type` ASC),
  INDEX `idx_parent_id` (`parent_id` ASC),
  INDEX `idx_org_id` (`org_id` ASC),
  INDEX `idx_call_group_id` (`call_group_id` ASC),
  INDEX `idx_valid` (`valid` ASC),
  INDEX `idx_revise` (`revise` ASC),
  INDEX `idx_create_time` (`create_time` ASC),
  INDEX `idx_call_id` (`call_id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `CallGroup` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` VARCHAR(32) NOT NULL,
  `is_delete` TINYINT NOT NULL DEFAULT 0,
  `cg_cond_id` BIGINT UNSIGNED NOT NULL,
  `enterprise` VARCHAR(32) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL,
  `last_call_id` BIGINT UNSIGNED NOT NULL,
  `last_call_time` BIGINT NOT NULL DEFAULT 0,
  `create_time` BIGINT NOT NULL DEFAULT 0,
  `update_time` BIGINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `idx_uuid` (`uuid` ASC),
  INDEX `idx_is_deleted` (`is_delete` ASC),
  INDEX `idx_cg_cond_id` (`cg_cond_id` ASC),
  INDEX `idx_enterprise` (`enterprise` ASC),
  INDEX `idx_last_call_id` (`last_call_id` ASC),
  INDEX `idx_last_call_time` (`last_call_time` ASC),
  INDEX `idx_create_time` (`create_time` ASC),
  INDEX `idx_update_time` (`update_time` ASC),
  UNIQUE INDEX `uuid_UNIQUE` (`uuid` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `Relation_CallGroup_Call` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `call_group_id` BIGINT UNSIGNED NOT NULL,
  `call_id` BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_call_group_id` (`call_group_id` ASC),
  INDEX `idx_call_id` (`call_id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `CallGroupCondition` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `is_delete` TINYINT NOT NULL DEFAULT 0,
  `name` VARCHAR(128) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL DEFAULT '',
  `description` VARCHAR(1024) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' DEFAULT '',
  `enterprise` VARCHAR(32) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL,
  `is_enable` TINYINT NOT NULL DEFAULT 1,
  `day_range` int(11),
  `duration_min` int(11),
  `duration_max` int(11),
  `create_time` BIGINT NOT NULL DEFAULT 0,
  `update_time` BIGINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `idx_is_deleted` (`is_delete` ASC),
  INDEX `idx_enterprise` (`enterprise` ASC),
  INDEX `idx_is_enable` (`is_enable` ASC),
  INDEX `idx_day_range` (`day_range` ASC),
  INDEX `idx_duration_min` (`duration_min` ASC),
  INDEX `idx_duration_max` (`duration_max` ASC),
  INDEX `idx_create_time` (`create_time` ASC),
  INDEX `idx_update_time` (`update_time` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `CallGroupConditionKey` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cg_condition_id` BIGINT UNSIGNED NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 for group by key, 2 filter by key value',
  `group_by` varchar(64) COLLATE 'utf8mb4_unicode_ci',
  `staff_id` varchar(64) COLLATE 'utf8mb4_unicode_ci',
  `staff_name` varchar(64) COLLATE 'utf8mb4_unicode_ci',
  `extension` varchar(32) COLLATE 'utf8mb4_unicode_ci',
  `department` varchar(64) COLLATE 'utf8mb4_unicode_ci',
  `customer_id` varchar(64) COLLATE 'utf8mb4_unicode_ci',
  `customer_name` varchar(32) COLLATE 'utf8mb4_unicode_ci',
  `customer_phone` varchar(32) COLLATE 'utf8mb4_unicode_ci',
  PRIMARY KEY (`id`),
  INDEX `idx_cg_condition_id` (`cg_condition_id` ASC),
  INDEX `idx_type` (`type` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;


INSERT INTO `CallGroupCondition` (`name`, `description`, `enterprise`, `is_enable`, `is_delete`, `day_range`, `duration_min`, `duration_max`, `create_time`, `update_time`)
VALUES
	('预设关联条件', '预设关联条件', 'global', 1, 0, 30, NULL, NULL,  UNIX_TIMESTAMP(),  UNIX_TIMESTAMP());
SET @CGCondID = (SELECT c.id FROM `CallGroupCondition` c WHERE c.enterprise = 'global');

INSERT INTO `UserKey` (`name`, `enterprise`, `inputname`, `type`, `is_delete`, `create_time`, `update_time`)
VALUES
	('回访结果类型', 'global', 'returnResult', 1, 0,  UNIX_TIMESTAMP(),  UNIX_TIMESTAMP()),
	('服务类型', 'global', 'serviceType', 1, 0,  UNIX_TIMESTAMP(),  UNIX_TIMESTAMP()),
	('回访业务', 'global', 'returnType', 1, 0,  UNIX_TIMESTAMP(),  UNIX_TIMESTAMP()),
	('保单号', 'global', 'serialNumber', 1, 0,  UNIX_TIMESTAMP(),  UNIX_TIMESTAMP());
SET @ReturnResultID = (SELECT u.id FROM `UserKey` u WHERE u.inputname = 'returnResult' AND u.enterprise = 'global');
SET @ServiceTypeID = (SELECT u.id FROM `UserKey` u WHERE u.inputname = 'serviceType' AND u.enterprise = 'global');
SET @ReturnTypeID = (SELECT u.id FROM `UserKey` u WHERE u.inputname = 'returnType' AND u.enterprise = 'global');
SET @SerialNumberID = (SELECT u.id FROM `UserKey` u WHERE u.inputname = 'serialNumber' AND u.enterprise = 'global');

INSERT INTO `CallGroupConditionKey` (`cg_condition_id`, `type`, `group_by`, `staff_id`, `staff_name`, `extension`, `department`, `customer_id`, `customer_name`, `customer_phone`)
VALUES
	(@CGCondID, 2, @ReturnResultID, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(@CGCondID, 2, @ServiceTypeID, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(@CGCondID, 2, @ReturnTypeID, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
  (@CGCondID, 1, @SerialNumberID, NULL, NULL, NULL, NULL, NULL, NULL, NULL);


INSERT INTO `UserValue` (`userkey_id`, `link_id`, `type`, `value`, `is_delete`, `create_time`, `update_time`)
VALUES
	(@ReturnResultID, @CGCondID, 4, '成功', 0,  UNIX_TIMESTAMP(),  UNIX_TIMESTAMP()),
	(@ServiceTypeID, @CGCondID, 4, '呼出数据', 0,  UNIX_TIMESTAMP(),  UNIX_TIMESTAMP()),
	(@ReturnTypeID, @CGCondID, 4, '新契约', 0,  UNIX_TIMESTAMP(),  UNIX_TIMESTAMP());

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
SET @CGCondID = (SELECT c.id FROM `CallGroupCondition` c WHERE c.enterprise = 'global');

DELETE FROM `UserValue` WHERE link_id = @CGCondID AND uv.type = 4;

DELETE FROM `UserKey` WHERE enterprise = 'global';


DROP Table IF EXISTS `CUPredictResultGroup`;
DROP Table IF EXISTS `CallGroup`;
DROP Table IF EXISTS `Relation_CallGroup_Call`;
DROP Table IF EXISTS `CallGroupCondition`;
DROP Table IF EXISTS `CallGroupConditionKey`;

ALTER TABLE `UserValue`
CHANGE COLUMN `type`
`type` TINYINT NOT NULL DEFAULT 0 COMMENT '1 for group, 2 for call, 3 for sensitive word';