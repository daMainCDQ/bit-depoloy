-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
-- Remove NULLABLE in type, to remove the need for application to handle exception
ALTER TABLE `RuleGroupCondition` CHANGE `type` `type` TINYINT(4)  NOT NULL  DEFAULT '1'  COMMENT '1 for use to all file, 0 for used to the file that matches to below conditions ';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `RuleGroupCondition` CHANGE `type` `type` TINYINT(4)  DEFAULT '1'  COMMENT '1 for use to all file, 0 for used to the file that matches to below conditions ';