-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE `ReportTask` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `creater_id` bigint(20) unsigned DEFAULT NULL COMMENT 'task creater if we have',
  `report_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '',
  `report_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '',
  `start_time` bigint(20) unsigned DEFAULT NULL COMMENT 'the start time of report request',
  `end_time` bigint(20) unsigned DEFAULT NULL COMMENT 'the end time of report request',
  `start_time2` bigint(20) unsigned DEFAULT NULL COMMENT 'the start time of report request',
  `end_time2` bigint(20) unsigned DEFAULT NULL COMMENT 'the start time of report request',
  `strArr` text COLLATE utf8mb4_unicode_ci,
  `strArr2` text COLLATE utf8mb4_unicode_ci,
  `server_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '',
  `visit_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '',
  `create_time` bigint(20) DEFAULT '0' COMMENT 'the timestamp of creating time',
  `status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT ':new|runing|finish',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `ReportTask`;