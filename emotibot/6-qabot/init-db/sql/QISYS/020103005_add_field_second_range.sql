-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `SentenceGroup`
  ADD COLUMN `second_range` INT NOT NULL DEFAULT 0 COMMENT '0 is not setting, positive is starts in N seconds, negative is ends in N seconds';


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `SentenceGroup`
  DROP COLUMN `second_range`;