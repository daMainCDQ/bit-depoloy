-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE `CallError` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `call_id` int(11) NOT NULL COMMENT '发生错误的通话ID',
  `message` text NOT NULL COMMENT '错误讯息',
  `create_time` bigint(20) NOT NULL COMMENT '错误发生的时间',
  PRIMARY KEY (`id`),
  KEY `idx_call_id` (`call_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `CallError`;