-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `Navigation`
ADD COLUMN `node_order` TEXT NOT NULL;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `Navigation`
DROP COLUMN `node_order`;