-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `TestFile`
  ADD COLUMN `duration` INT DEFAULT 0 NOT NULL COMMENT 'the duration of the test file in millisecond';
  
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `TestFile`
  DROP COLUMN `duration`;