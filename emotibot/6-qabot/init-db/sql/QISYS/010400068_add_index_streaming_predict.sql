-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `CUPredict`
ADD INDEX `idx_call_id` (`call_id` ASC);

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `CUPredict`
DROP INDEX `idx_call_id`;