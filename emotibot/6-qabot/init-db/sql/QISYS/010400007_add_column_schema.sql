-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied


ALTER TABLE `call`
ADD COLUMN `demo_file_path` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'file path to the content for the demonstration' AFTER `file_path`;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `call`
DROP COLUMN `demo_file_path`;