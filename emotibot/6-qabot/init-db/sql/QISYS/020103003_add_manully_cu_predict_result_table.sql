-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE IF NOT EXISTS `manually_cu_predict_result` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'id' AUTO_INCREMENT,
  `call_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `type` tinyint(4) DEFAULT '0' COMMENT '0 for default, 1 for group. 10 for rule in positive, 11 for rule in negative, 20 for logic, 30 for keyword, 31 for intent',
  `parent_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'For tag, parent_id points to the logic that contains it. For logic, parent_id points to the rule that contains it. For rule, parent_id points to the group that contains it. For group, parent_id is 0.\nparent_id points to some record in this table which means that it points to some predict_id',
  `org_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'the id for group/rule/logic/tag based on the type field',
  `valid` tinyint(4) DEFAULT '0' COMMENT '0 is default, 1 for meet the rule/logic/tag specification. 2 for not meet',
  `revise` int(11) DEFAULT NULL,
  `score` int(11) DEFAULT '0',
  `create_time` bigint(20) DEFAULT '0' COMMENT 'the timestamp of creating time',
  `update_time` bigint(20) DEFAULT '0' COMMENT 'the timestamp of updating time',
  `description` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'the comment to describe the revision',
  PRIMARY KEY (`id`,`call_id`),
  KEY `idx_type` (`type`),
  KEY `idx_parent_id` (`parent_id`),
  KEY `idx_org_id` (`org_id`),
  KEY `idx_call_id` (`call_id`),
  KEY `idx_create_time` (`create_time`),
  KEY `idx_lev_score` (`score`,`type`),
  KEY `idx_call_score` (`call_id`,`type`,`score`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `manually_cu_predict_result`;
