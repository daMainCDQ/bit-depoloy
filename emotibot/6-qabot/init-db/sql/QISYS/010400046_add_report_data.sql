-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE IF NOT EXISTS `ReportData` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `task_id` bigint(20) unsigned DEFAULT NULL COMMENT 'task creater if we have',
  `create_time` bigint(20) DEFAULT '0' COMMENT 'the timestamp of creating time',
  `order_num` bigint(20) DEFAULT '0',
  `field` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `ReportData`;