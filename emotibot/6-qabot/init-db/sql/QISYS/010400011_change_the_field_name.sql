-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `CUPredictMatch`
CHANGE `predict_score_id` `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `CUPredictResult`
CHANGE `predict_id` `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `CUPredict`
CHANGE `cu_id` `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `Segment`
CHANGE `segment_id` `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'the unique id for each segment';



-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `CUPredictMatch`
CHANGE `id` `predict_score_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `CUPredictResult`
CHANGE `id` `predict_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `CUPredict`
CHANGE `id` `cu_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `Segment`
CHANGE `id` `segment_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'the unique id for each segment';