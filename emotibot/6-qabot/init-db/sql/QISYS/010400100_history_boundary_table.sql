-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE `HistoryBoundary` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `upper_bound` INT NOT NULL,
  `lower_bound` INT NOT NULL,
  `base` INT NOT NULL,
  `enterprise` VARCHAR(32) NOT NULL,
  `create_time` BIGINT NOT NULL,
  `update_time` BIGINT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_query` (`enterprise` ASC, `create_time` ASC))
ENGINE = InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `HistoryBoundary`;