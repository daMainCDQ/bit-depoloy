-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE  `StatisticKey` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `key` VARCHAR(256) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_key` (`key`(64) ASC),
  UNIQUE INDEX `key_UNIQUE` (`key` ASC))
ENGINE = InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `StatisticKey`;