-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `Segment`
ADD COLUMN `asr_text` VARCHAR(256) NULL DEFAULT '' COMMENT 'the asr text';


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `Segment`
DROP COLUMN `asr_text`;

