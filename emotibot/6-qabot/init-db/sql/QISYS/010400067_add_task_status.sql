-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE `migration_task` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `is_delete` tinyint(4) NOT NULL DEFAULT '0',
  `name` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `type` tinyint(4) DEFAULT '0' COMMENT '0 for full import rule, 1 for increment import rule, 2 for full import navigation, 3 for increment import navigation, 4 for increment import sentence, 5 for increment import tag',
  `create_time` bigint(20) NOT NULL DEFAULT '0',
  `update_time` bigint(20) NOT NULL DEFAULT '0',
  `enterprise` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uuid` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'unfinish, finish, error',
  PRIMARY KEY (`id`),
  UNIQUE KEY `task_id_unique` (`id`),
  KEY `idx_id_delete` (`is_delete`),
  KEY `idx_type` (`type`),
  KEY `idx_enterprise` (`enterprise`),
  KEY `idx_create_time` (`create_time`),
  KEY `idx_update_time` (`update_time`),
  KEY `idx_uuid` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- +migrate Down
DROP TABLE `migration_task`;