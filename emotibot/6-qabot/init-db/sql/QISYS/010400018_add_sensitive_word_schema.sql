-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- -----------------------------------------------------
-- Table `SensitiveWord`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SensitiveWord` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` VARCHAR(32) NOT NULL,
  `name` VARCHAR(128) NOT NULL,
  `enterprise` VARCHAR(32) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL,
  `score` INT NULL DEFAULT 0,
  `category_id` BIGINT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'the category which this sensitive word belongs to',
  `is_delete` TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `idx_enterprise` (`enterprise` ASC),
  INDEX `idx_uuid` (`uuid` ASC),
  INDEX `idx_is_delete` (`is_delete` ASC),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT = 'sensitive words';


-- -----------------------------------------------------
-- Table `Relation_SensitiveWord_Sentence`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Relation_SensitiveWord_Sentence` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `sw_id` BIGINT UNSIGNED NOT NULL,
  `s_id` BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `idx_sw_id` (`sw_id` ASC),
  INDEX `idx_s_id` (`s_id` ASC))
ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT = 'relation between SensitiveWord and Sentence';


ALTER TABLE `Category`
ADD COLUMN `type` TINYINT UNSIGNED NOT NULL DEFAULT 0 COMMENT '0 for sentence, 1 for sensitive word';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP Table IF EXISTS `SensitiveWord`;
DROP Table IF EXISTS `Relation_SensitiveWord_Sentence`;

ALTER TABLE `Category`
DROP COLUMN `type`;
