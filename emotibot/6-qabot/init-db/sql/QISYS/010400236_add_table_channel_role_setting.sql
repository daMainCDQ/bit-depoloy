-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE `channel_role_setting` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'unique id of this record in integer format',
  `enterprise` VARCHAR(32) NOT NULL COMMENT 'the enterprise id',
  `left_channel` TINYINT NOT NULL COMMENT 'the role for left channel. 0 is staff, 1 is customer',
  `right_channel` TINYINT NOT NULL COMMENT 'the role for right channel. 0 is staff, 1 is customer',
  `create_time` BIGINT NOT NULL COMMENT 'the created time of this record',
  `update_time` BIGINT NOT NULL COMMENT 'the updated time of this record',
  PRIMARY KEY (`id`),
  INDEX `idx_query` (`enterprise` ASC))
ENGINE = InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT 'default setting of the role for left and right channel for each enterprise';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `channel_role_setting`;