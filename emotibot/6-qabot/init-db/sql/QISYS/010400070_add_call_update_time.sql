-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `call`
  ADD `update_time` bigint(20) NOT NULL DEFAULT '0' COMMENT 'update time of the call';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `call`
  DROP `update_time`;