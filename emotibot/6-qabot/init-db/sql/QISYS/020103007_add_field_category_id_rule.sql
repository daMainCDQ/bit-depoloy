-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `Rule`
  ADD COLUMN `category_id` BIGINT NOT NULL DEFAULT 0 COMMENT '0 is no category, others refers to rule_category table';


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `Rule`
  DROP COLUMN `category_id`;