-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE `connectedCallCondition` (
  `id` bigint(20) unsigned NOT NULL COMMENT 'id' AUTO_INCREMENT,
  `level` tinyint(4) NOT NULL COMMENT 'binding resource type',
  `link_id` bigint(20) unsigned NOT NULL COMMENT 'foreign id',
  `expression` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'condition logic expression of UserKey',
  `default_condition` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'fixed column setting',
  PRIMARY KEY (`id`),
  KEY `idx_query` (`link_id`,`level`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT 'condition for call group';
-- Reset all setting for connected_call_type
UPDATE `Condition` SET `connected_call_type` = 2;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `connectedCallCondition`;