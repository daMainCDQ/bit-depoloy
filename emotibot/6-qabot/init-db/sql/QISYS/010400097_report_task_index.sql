-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
-- author: huantianlv

ALTER TABLE `ReportTask`
  ADD INDEX `idx_sct`(`status`, `create_time`);

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

ALTER TABLE `ReportTask`
  DROP INDEX `idx_sct`;
