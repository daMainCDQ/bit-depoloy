-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `Rule` ADD COLUMN  `completeness` TINYINT NOT NULL DEFAULT -1 COMMENT '-1 for unchecked, 0 for false,1 for true',
ADD INDEX `idx_completeness` (`completeness` ASC);


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `Rule` DROP `completeness`;
