-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `SentenceGroup`
  ADD COLUMN `description`VARCHAR(512) NOT NULL DEFAULT '' COMMENT 'the description of the sentence group';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `SentenceGroup`
  DROP COLUMN `description`;
