-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `UserValue`
  ADD INDEX `idx_specific_query` (`is_delete` ASC, `type` ASC, `link_id` ASC, `userkey_id` ASC, `value` ASC);
 

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

ALTER TABLE `UserValue`
  DROP INDEX `idx_specific_query`;
