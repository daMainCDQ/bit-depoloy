-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE `rule_category` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'auto incremental id',
  `uuid` VARCHAR(32) NOT NULL COMMENT 'unique global id',
  `name` VARCHAR(64) NOT NULL COMMENT 'displaying name',
  `enterprise` VARCHAR(32) NOT NULL COMMENT 'enterprise id',
  `is_delete` TINYINT NOT NULL DEFAULT 0 COMMENT 'whether this is deleted or not',
  `create_time` BIGINT NOT NULL COMMENT 'created timestamp',
  `update_time` BIGINT NOT NULL COMMENT 'updated timestamp',
  PRIMARY KEY (`id`),
  INDEX `idx_uuid_query` (`uuid` ASC),
  INDEX `idx_name` (`enterprise`(24) ASC, `name`(8) ASC)
) ENGINE = InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT 'the user defined category name for rule';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `rule_category`;
