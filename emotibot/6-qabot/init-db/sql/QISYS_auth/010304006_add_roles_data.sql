-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied 
INSERT INTO `QISYS_auth`.`roles`(`id`, `uuid`, `name`, `enterprise`, `description`, `created_time`) VALUES (1, 'ed8c7632d5e9449b85d49d0db4da0637', '系统管理员', '', '', '2019-11-14 15:08:03');
INSERT INTO `QISYS_auth`.`roles`(`id`, `uuid`, `name`, `enterprise`, `description`, `created_time`) VALUES (2, 'qualityDirector', '质检主管', 'bb3e3925f0ad11e7bd860242ac120003', '', '2019-11-14 15:08:11');
INSERT INTO `QISYS_auth`.`roles`(`id`, `uuid`, `name`, `enterprise`, `description`, `created_time`) VALUES (3, 'qualityGroupLeader', '质检组长', 'bb3e3925f0ad11e7bd860242ac120003', '', '2019-11-14 02:14:00');
INSERT INTO `QISYS_auth`.`roles`(`id`, `uuid`, `name`, `enterprise`, `description`, `created_time`) VALUES (4, 'qualityInspector', '质检人员', 'bb3e3925f0ad11e7bd860242ac120003', '', '2019-11-14 02:14:00');
INSERT INTO `QISYS_auth`.`roles`(`id`, `uuid`, `name`, `enterprise`, `description`, `created_time`) VALUES (5, 'tableDirector', '坐席主管', 'bb3e3925f0ad11e7bd860242ac120003', '', '2019-11-14 02:14:00');
INSERT INTO `QISYS_auth`.`roles`(`id`, `uuid`, `name`, `enterprise`, `description`, `created_time`) VALUES (6, 'tableGroupLeader', '坐席组长', 'bb3e3925f0ad11e7bd860242ac120003', '', '2019-11-14 02:14:00');
INSERT INTO `QISYS_auth`.`roles`(`id`, `uuid`, `name`, `enterprise`, `description`, `created_time`) VALUES (7, 'tableStaff', '坐席人员', 'bb3e3925f0ad11e7bd860242ac120003', '', '2019-11-14 02:14:00');