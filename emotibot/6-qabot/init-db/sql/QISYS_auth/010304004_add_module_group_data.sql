-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
INSERT INTO `QISYS_auth`.`module_group`(`id`, `code`, `zh_cn`, `zh_tw`, `created_time`, `product`) VALUES (10, 'assistant_process_navigation', '助手流程导航', '', '2019-05-23 12:50:29', 3);
INSERT INTO `QISYS_auth`.`module_group`(`id`, `code`, `zh_cn`, `zh_tw`, `created_time`, `product`) VALUES (11, 'report_management', '报表管理', '', '2019-05-23 12:50:29', 3);
INSERT INTO `QISYS_auth`.`module_group`(`id`, `code`, `zh_cn`, `zh_tw`, `created_time`, `product`) VALUES (12, 'quality_list', '质检列表', '', '2019-05-23 12:50:29', 3);
INSERT INTO `QISYS_auth`.`module_group`(`id`, `code`, `zh_cn`, `zh_tw`, `created_time`, `product`) VALUES (13, 'artificial_quality', '人工质检', '', '2019-05-23 12:50:29', 3);
INSERT INTO `QISYS_auth`.`module_group`(`id`, `code`, `zh_cn`, `zh_tw`, `created_time`, `product`) VALUES (14, 'library_materials', '教材库', '', '2019-05-23 12:50:30', 3);
INSERT INTO `QISYS_auth`.`module_group`(`id`, `code`, `zh_cn`, `zh_tw`, `created_time`, `product`) VALUES (15, 'system', '系统', '', '2019-05-23 12:50:30', 3);
INSERT INTO `QISYS_auth`.`module_group`(`id`, `code`, `zh_cn`, `zh_tw`, `created_time`, `product`) VALUES (16, 'quality_management_rules', '质检规则管理', '', '2019-05-23 12:50:30', 3);


