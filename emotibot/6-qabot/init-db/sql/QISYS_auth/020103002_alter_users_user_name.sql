-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `QISYS_auth`.`users` 
MODIFY COLUMN `user_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户帐号' AFTER `display_name`;