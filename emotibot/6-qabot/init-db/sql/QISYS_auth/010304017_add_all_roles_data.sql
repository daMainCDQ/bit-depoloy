-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied 
INSERT INTO `QISYS_auth`.`user_privileges`(`human`, `machine`, `role`) VALUES ('4b21158a395311e88a710242ac110003', 'csbot', 'qualityDirector');
INSERT INTO `QISYS_auth`.`user_privileges`(`human`, `machine`, `role`) VALUES ('4b21158a395311e88a710242ac110003', 'csbot', 'qualityGroupLeader');
INSERT INTO `QISYS_auth`.`user_privileges`(`human`, `machine`, `role`) VALUES ('4b21158a395311e88a710242ac110003', 'csbot', 'qualityInspector');
INSERT INTO `QISYS_auth`.`user_privileges`(`human`, `machine`, `role`) VALUES ('4b21158a395311e88a710242ac110003', 'csbot', 'tableDirector');
INSERT INTO `QISYS_auth`.`user_privileges`(`human`, `machine`, `role`) VALUES ('4b21158a395311e88a710242ac110003', 'csbot', 'tableGroupLeader');
INSERT INTO `QISYS_auth`.`user_privileges`(`human`, `machine`, `role`) VALUES ('4b21158a395311e88a710242ac110003', 'csbot', 'tableStaff');
