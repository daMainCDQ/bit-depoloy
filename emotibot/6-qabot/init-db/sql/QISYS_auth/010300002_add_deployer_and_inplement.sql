-- +migrate Up
SET AUTOCOMMIT=0;
INSERT IGNORE INTO `users` (`uuid`, `display_name`, `user_name`, `email`, `phone`, `enterprise`, `type`, `password`, `status`, `product`)
VALUES
	('4b21158a395311e88a710242ac110002', 'SYSTEM', 'deployer', 'admin@emotibot.com', '', NULL, 0, '7e2ba10110f719dd65a0403305770b08', 1, '');

INSERT INTO `modules` (`id`,`code`, `name`, `enterprise`, `cmd_list`, `description`, `status`)
VALUES
	('28','integration', '', NULL, 'view,edit', '', 1);

SET AUTOCOMMIT=1;
-- +migrate Down
DELETE FROM `modules` WHERE `code` = 'integration' AND `enterprise` IS NULL;
DELETE FROM `users` WHERE `user_name` = 'deployer';
