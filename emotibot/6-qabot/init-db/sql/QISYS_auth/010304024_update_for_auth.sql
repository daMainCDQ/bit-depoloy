-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `QISYS_auth`.`organization`
ADD COLUMN `ENTERPRISE` varchar(36) NOT NULL COMMENT '企业ID' AFTER `IS_DELETE`;

ALTER TABLE `QISYS_auth`.`user_detail`
ADD COLUMN `ENTERPRISE` varchar(36) NULL COMMENT '所属企业' AFTER `CREATE_TIME`;

ALTER TABLE `QISYS_auth`.`enterprises`
ADD COLUMN `status` tinyint(4) NULL DEFAULT 1 COMMENT '企业状态 0: disable, 1: active' AFTER `created_time`;

ALTER TABLE `QISYS_auth`.`organization`
MODIFY COLUMN `ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键' FIRST,
ADD COLUMN `PARENT_ID` bigint(20) NULL COMMENT '父节点ID' AFTER `ENTERPRISE`;

ALTER TABLE `QISYS_auth`.`organization`
MODIFY COLUMN `IS_DELETE` tinyint(4) NOT NULL DEFAULT 0 COMMENT '状态 1启动 0禁用' AFTER `CREATE_TIME`;

ALTER TABLE `QISYS_auth`.`organization`
MODIFY COLUMN `ORGAN_CODE` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '机构代码' AFTER `ORGAN_NAME`,
MODIFY COLUMN `PARENT_ORGAN_CODE` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '父级机构代码' AFTER `ORGAN_CODE`;

ALTER TABLE `QISYS_auth`.`user_privileges`
ADD COLUMN `organization_id` bigint(20) NULL COMMENT '组织ID' AFTER `role`,
ADD COLUMN `enterprise` varchar(36) NULL COMMENT '企业UUID' AFTER `organization_id`;

ALTER TABLE `QISYS_auth`.`users`
MODIFY COLUMN `type` tinyint(4) UNSIGNED NOT NULL DEFAULT 2 COMMENT '1管理员2一般用户' AFTER `enterprise`,
MODIFY COLUMN `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '状态 1启动 0禁用' AFTER `product`;

ALTER TABLE `QISYS_auth`.`user_privileges`
MODIFY COLUMN `machine` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT 'Machine id' AFTER `human`;

ALTER TABLE `QISYS_auth`.`privileges`
ADD COLUMN `enterprise` varchar(36) NULL COMMENT '企业UUID' AFTER `created_time`;


