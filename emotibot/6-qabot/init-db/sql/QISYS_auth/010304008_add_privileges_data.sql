-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied 
INSERT INTO `QISYS_auth`.`privileges`(`id`, `role`, `module`, `cmd_list`, `created_time`) VALUES (1, 1, 42, 'view,setting', '2019-11-19 10:38:37');
INSERT INTO `QISYS_auth`.`privileges`(`id`, `role`, `module`, `cmd_list`, `created_time`) VALUES (2, 1, 43, 'view_band,export', '2019-11-19 10:38:36');
INSERT INTO `QISYS_auth`.`privileges`(`id`, `role`, `module`, `cmd_list`, `created_time`) VALUES (3, 1, 44, 'view_report,export,create_report', '2019-11-19 10:38:36');
INSERT INTO `QISYS_auth`.`privileges`(`id`, `role`, `module`, `cmd_list`, `created_time`) VALUES (4, 1, 45, 'view,upload,export', '2019-11-19 10:38:37');
INSERT INTO `QISYS_auth`.`privileges`(`id`, `role`, `module`, `cmd_list`, `created_time`) VALUES (5, 1, 47, 'view,export,edit_result', '2019-11-19 10:38:37');
INSERT INTO `QISYS_auth`.`privileges`(`id`, `role`, `module`, `cmd_list`, `created_time`) VALUES (6, 1, 50, 'view,create,assign_check,assign_review,edit', '2019-11-19 10:38:36');
INSERT INTO `QISYS_auth`.`privileges`(`id`, `role`, `module`, `cmd_list`, `created_time`) VALUES (7, 1, 51, 'view,reassign', '2019-11-19 10:38:36');
INSERT INTO `QISYS_auth`.`privileges`(`id`, `role`, `module`, `cmd_list`, `created_time`) VALUES (8, 1, 52, 'spotcheck,textsign,textrevision,modification,create_issues', '2019-11-19 10:38:36');
INSERT INTO `QISYS_auth`.`privileges`(`id`, `role`, `module`, `cmd_list`, `created_time`) VALUES (9, 1, 53, 'view_info,designate,batchdesignate,assign_issues,assign_appeal', '2019-11-19 10:38:36');
INSERT INTO `QISYS_auth`.`privileges`(`id`, `role`, `module`, `cmd_list`, `created_time`) VALUES (10, 1, 54, 'audit,handel_appeal,assign_appeal,modification_result,verify,confirm,handel_issues,handel_issuesaudit,reassign', '2019-11-19 10:38:36');
INSERT INTO `QISYS_auth`.`privileges`(`id`, `role`, `module`, `cmd_list`, `created_time`) VALUES (11, 1, 55, 'create_appeal', '2019-11-19 10:38:36');
INSERT INTO `QISYS_auth`.`privileges`(`id`, `role`, `module`, `cmd_list`, `created_time`) VALUES (12, 1, 57, 'view,edit,edit_text,delete_text,export_text', '2019-11-19 10:38:37');
INSERT INTO `QISYS_auth`.`privileges`(`id`, `role`, `module`, `cmd_list`, `created_time`) VALUES (13, 1, 58, 'view,audit,setting,sample_announce', '2019-11-19 10:38:37');
INSERT INTO `QISYS_auth`.`privileges`(`id`, `role`, `module`, `cmd_list`, `created_time`) VALUES (14, 1, 59, 'view,setting', '2019-11-19 10:38:36');
INSERT INTO `QISYS_auth`.`privileges`(`id`, `role`, `module`, `cmd_list`, `created_time`) VALUES (15, 1, 60, 'view,setting', '2019-11-19 10:38:37');
INSERT INTO `QISYS_auth`.`privileges`(`id`, `role`, `module`, `cmd_list`, `created_time`) VALUES (16, 1, 61, 'view,setting', '2019-11-19 10:38:37');
INSERT INTO `QISYS_auth`.`privileges`(`id`, `role`, `module`, `cmd_list`, `created_time`) VALUES (17, 1, 62, 'view,add,edit,import,export,delete', '2019-11-19 10:38:37');
INSERT INTO `QISYS_auth`.`privileges`(`id`, `role`, `module`, `cmd_list`, `created_time`) VALUES (18, 1, 63, 'view,setting', '2019-11-19 10:38:37');
INSERT INTO `QISYS_auth`.`privileges`(`id`, `role`, `module`, `cmd_list`, `created_time`) VALUES (19, 1, 64, 'view,setting', '2019-11-19 10:38:37');
INSERT INTO `QISYS_auth`.`privileges`(`id`, `role`, `module`, `cmd_list`, `created_time`) VALUES (20, 1, 65, 'view,setting', '2019-11-19 10:38:37');