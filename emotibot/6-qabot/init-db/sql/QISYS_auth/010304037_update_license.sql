-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `QISYS_auth`.`license_enterprise`
MODIFY COLUMN `used_total_length` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '已用总时长（s）' AFTER `used_total_amount`;

ALTER TABLE `QISYS_auth`.`license_enterprise`
DROP COLUMN `enterprise_name`;


