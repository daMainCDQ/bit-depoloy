-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE `QISYS_auth`.`users_bak` LIKE `QISYS_auth`.`users`;
INSERT INTO `QISYS_auth`.`users_bak` SELECT * FROM `QISYS_auth`.`users`;
CREATE TABLE `QISYS_auth`.`enterprises_bak` LIKE `QISYS_auth`.`enterprises`;
INSERT INTO `QISYS_auth`.`enterprises_bak` SELECT * FROM `QISYS_auth`.`enterprises`;
CREATE TABLE `QISYS_auth`.`organization_bak` LIKE `QISYS_auth`.`organization`;
INSERT INTO `QISYS_auth`.`organization_bak` SELECT * FROM `QISYS_auth`.`organization`;




delete from QISYS_auth.users where user_name <> 'deployer';
delete from QISYS_auth.enterprises;
INSERT into QISYS_auth.enterprises (id,uuid,`name`,description,created_time,`status`)  (select id,uuid,`name`,description,created_time,`status` from auth.enterprises where `level` = '0' );
delete from QISYS_auth.organization;
insert into QISYS_auth.organization (id,ORGAN_NAME,ORGAN_CODE,PARENT_ORGAN_CODE,PARENT_ID,create_time,IS_DELETE,enterprise) (select id,`name`, LPAD(id, 4, 0),'enterprise','0',created_time,`status`,uuid from auth.enterprises where  `level` = '0' );