-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied 
update `QISYS_auth`.`modules` set cmd_list = 'view,export,edit_result,edit_asr_text' where `code` = 'quality_results';
update `QISYS_auth`.`privileges` set cmd_list = 'view,export,edit_result,edit_asr_text' where `module` = 47 and role in (1,2);