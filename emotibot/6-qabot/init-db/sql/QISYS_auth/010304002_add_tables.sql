-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE IF NOT EXISTS `product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `secret` char(255) NOT NULL DEFAULT '' COMMENT 'oauth 认证使用 secret',
  `redirect_uri` char(255) NOT NULL DEFAULT '' COMMENT 'oauth 认证转址位置',
  `zh_cn` char(255) NOT NULL DEFAULT '' COMMENT '模组名称(简)',
  `zh_tw` char(255) NOT NULL DEFAULT '' COMMENT '模组名称(繁)',
  `code` varchar(64) NOT NULL DEFAULT '' COMMENT '产品 code',
  `create_time` bigint(20) NOT NULL COMMENT '创建时间',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '产品启用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COMMENT='Auth 模组产品列表';

CREATE TABLE IF NOT EXISTS `module_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `code` char(32) NOT NULL DEFAULT '' COMMENT '权限群组代号',
  `zh_cn` varchar(36) NOT NULL DEFAULT '' COMMENT '权限群组名字简中',
  `zh_tw` varchar(36) NOT NULL DEFAULT '' COMMENT '权限群组名字繁中',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立日期',
  `product` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `app exist` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8mb4 COMMENT='机器人列表';



ALTER TABLE `QISYS_auth`.`modules`
MODIFY COLUMN `cmd_list` char(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '该模组所有的操作列表 ex: view, create, edit, export, import, delete, 该栏位会影响前端权限群组显示' AFTER `enterprise`;

ALTER TABLE `QISYS_auth`.`privileges`
MODIFY COLUMN `cmd_list` char(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '拥有模组的哪些权限' AFTER `module`;

ALTER TABLE `QISYS_auth`.`modules`
ADD COLUMN `product` bigint(20) NULL DEFAULT NULL COMMENT '该模组所属产品' AFTER `created_time`,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`id`) USING BTREE;

ALTER TABLE `QISYS_auth`.`modules`
ADD COLUMN `group` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '该模组所属群组' AFTER `created_time`,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`id`) USING BTREE;


CREATE TABLE IF NOT EXISTS `seat_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `DIC_KEY` varchar(32) NOT NULL DEFAULT '' COMMENT '小组代号',
  `DIC_NAME` varchar(64) NOT NULL DEFAULT '' COMMENT '小组名称',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='坐席小组';


CREATE TABLE IF NOT EXISTS `user_detail` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `USER_CODE` varchar(32) NOT NULL COMMENT '用户编号',
  `CTI_CODE` varchar(32) DEFAULT NULL COMMENT '通话和用户关联',
  `SOFT_PHONE` varchar(32) DEFAULT NULL COMMENT '坐席软电话号',
  `ORGAN_CODE` varchar(32) DEFAULT NULL COMMENT '坐席归属机构编号',
  `ORGAN_NAME` varchar(128) DEFAULT NULL COMMENT '坐席归属机构',
  `DIC_KEY` varchar(32) DEFAULT NULL COMMENT '序列编号',
  `DIC_NAME` varchar(64) DEFAULT NULL COMMENT '序列名称',
  `USER_NAME` varchar(64) DEFAULT NULL COMMENT '用户名称',
  `USER_UUID` varchar(32) DEFAULT NULL,
  `CREATE_TIME` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  KEY `index_user_code` (`USER_CODE`) USING BTREE,
  KEY `index_organ_code` (`ORGAN_CODE`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT '用户详情';


CREATE TABLE IF NOT EXISTS `organization` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ORGAN_NAME` varchar(128) NOT NULL COMMENT '机构名',
  `ORGAN_CODE` varchar(20) NOT NULL COMMENT '机构代码',
  `PARENT_ORGAN_CODE` varchar(20) NOT NULL COMMENT '父级机构代码',
  `CREATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `IS_DELETE` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=未删除,1=已删除',
  PRIMARY KEY (`ID`),
  KEY `index_organ_code` (`ORGAN_CODE`) USING BTREE,
  KEY `index_parent_organ_code` (`PARENT_ORGAN_CODE`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT '组织机构';

CREATE TABLE IF NOT EXISTS `qa_roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `target_code` varchar(32) NOT NULL,
  `target_name` varchar(32) NOT NULL,
  `source_uuid` char(32) NOT NULL DEFAULT '',
  `source_name` varchar(50) NOT NULL DEFAULT '',
  `source_enterprise` char(32) DEFAULT '',
  `source_description` varchar(200) DEFAULT '',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `operator` varchar(64) DEFAULT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_qa_roles_id` (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT '质检系统角色';

