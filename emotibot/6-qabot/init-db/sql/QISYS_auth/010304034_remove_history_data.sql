-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
delete from QISYS_auth.user_privileges where human <> '4b21158a395311e88a710242ac110002';
delete from QISYS_auth.user_detail ;

delete from `privileges`  where enterprise <> 'default';

insert into QISYS_auth.`privileges` (role,module,cmd_list,enterprise)   (
select  p.role,
          p.module,
          p.cmd_list,
         e.uuid enterprise from QISYS_auth.`privileges` p ,enterprises e   order by  e.uuid ,p.role,
        p.module
    );
