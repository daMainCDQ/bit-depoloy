-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied 

delete from modules where `code`='assistant_process_navigation';
delete from module_group where `code`='assistant_process_navigation';
INSERT INTO `QISYS_auth`.`module_group`(`id`, `code`, `zh_cn`, `zh_tw`, `created_time`, `product`) VALUES (10, 'assistant_process_navigation', '助手流程导航', '', '2019-05-23 12:50:29', 3);
INSERT INTO `QISYS_auth`.`modules`(`id`, `code`, `name`, `enterprise`, `cmd_list`, `description`, `status`, `created_time`, `group`, `product`) VALUES (42, 'assistant_process_navigation', '助手流程导航', 'bb3e3925f0ad11e7bd860242ac120003', 'view,setting', '', 0, '2019-05-23 12:50:29', '10', 3);
