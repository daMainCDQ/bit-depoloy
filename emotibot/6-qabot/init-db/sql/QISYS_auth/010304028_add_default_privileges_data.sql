-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (1, 42, 'view,setting', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (1, 43, 'view,export', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (1, 44, 'view_report,export,create_report', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (1, 45, 'view,upload,export', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (1, 47, 'view,export,edit_asr_text', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (1, 50, 'view,create,assign_check,assign_review,edit', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (1, 51, 'view,reassign', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (1, 52, 'spotcheck,textsign,textrevision,modification,create_issues', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (1, 53, 'view_info,designate,batchdesignate,assign_issues,assign_appeal', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (1, 54, 'audit,handel_appeal,assign_appeal,modification_result,verify,confirm,handel_issues,handel_issuesaudit,reassign', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (1, 55, 'create_appeal', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (1, 57, 'view,delete_text,export_text', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (1, 58, 'view,audit,setting,sample_announce', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (1, 59, 'view,setting', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (1, 60, 'view,setting', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (1, 61, 'view,setting', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (1, 62, 'view,add,edit,import,export,delete', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (1, 63, 'view,setting', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (1, 64, 'view,setting', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (1, 65, 'view,setting', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (1, 68, 'view,export', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (1, 70, 'view', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (1, 73, 'view', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (1, 75, 'create,delete,edit', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (1, 76, 'create,delete,view,edit', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (1, 77, 'create,delete', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (1, 90, 'view', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (2, 42, 'view,setting', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (2, 43, 'view,export', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (2, 44, 'view_report,export,create_report', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (2, 45, 'view,upload', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (2, 47, 'view,export,edit_asr_text', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (2, 50, 'view,create,assign_check,assign_review,edit,etail_view', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (2, 53, 'view_info,designate,batchdesignate,etail_assign_appeal,etail_modification_result,etail_handel_issuesaudit', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (2, 57, 'view,delete_text,export_text', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (2, 58, 'view,audit,setting,sample_announce', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (2, 59, 'view,setting', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (2, 60, 'view,setting', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (2, 61, 'view,setting', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (2, 62, 'view,add,edit,import,export,delete', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (2, 63, 'view,setting', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (2, 64, 'view,setting', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (2, 65, 'view,setting', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (2, 68, 'view,export', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (2, 70, 'view', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (2, 73, 'view', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (2, 75, 'create,delete,edit', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (2, 76, 'create,delete,view,edit', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (2, 77, 'create,delete', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (2, 90, 'view', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (3, 43, 'view,export', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (3, 44, 'view_report,export,create_report', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (3, 47, 'view,export,edit_asr_text', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (3, 50, 'view,create,assign_check,assign_review,edit', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (3, 51, 'view', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (3, 52, 'spotcheck', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (3, 53, 'view_info,designate,batchdesignate,assign_issues,assign_appeal', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (3, 54, 'handel_appeal,assign_appeal,handel_issuesaudit', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (3, 57, 'view,delete_text,export_text', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (3, 58, 'view,audit', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (3, 60, 'view,setting', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (3, 61, 'view,setting', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (3, 62, 'view,add,edit,import,export,delete', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (3, 63, 'view,setting', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (3, 64, 'view,setting', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (3, 65, 'view,setting', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (3, 68, 'view,export', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (3, 70, 'view', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (3, 73, 'view', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (3, 75, 'create,delete,edit', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (3, 76, 'create,delete,view,edit', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (3, 77, 'create,delete', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (3, 90, 'view', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (4, 43, 'view,export', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (4, 44, 'view_report,export,create_report', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (4, 47, 'view,export,edit_asr_text', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (4, 50, 'view,create', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (4, 51, 'view', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (4, 52, 'spotcheck,textsign,textrevision,modification,create_issues', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (4, 53, 'view_info,designate,assign_issues,assign_appeal', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (4, 54, 'handel_appeal,modification_result,verify,handel_issuesaudit', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (4, 57, 'view,delete_text,export_text', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (4, 60, 'view,setting', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (4, 61, 'view,setting', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (4, 62, 'view,add,edit,import,export,delete', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (4, 63, 'view,setting', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (4, 64, 'view,setting', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (4, 65, 'view,setting', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (4, 68, 'view,export', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (4, 70, 'view', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (4, 73, 'view', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (4, 75, 'create,delete,edit', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (4, 76, 'create,delete,view,edit', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (4, 77, 'create,delete', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (4, 90, 'view', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (5, 42, 'view,setting', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (5, 43, 'view,export', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (5, 44, 'view_report,export,create_report', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (5, 45, 'view,upload', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (5, 47, 'view,export,edit_asr_text', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (5, 50, 'view', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (5, 53, 'view_info,assign_appeal', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (5, 57, 'view,delete_text,export_text', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (5, 68, 'view,export', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (5, 70, 'view', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (5, 73, 'view', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (5, 75, 'create,delete,edit', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (5, 76, 'create,delete,view,edit', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (5, 77, 'create,delete', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (5, 90, 'view', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (6, 43, 'view,export', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (6, 44, 'view_report,export,create_report', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (6, 45, 'view,upload,export', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (6, 47, 'view,export,edit_asr_text', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (6, 50, 'view,create,assign_check,assign_review,edit', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (6, 51, 'view', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (6, 52, 'spotcheck,textsign,textrevision,modification,create_issues', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (6, 53, 'view_info,designate,batchdesignate,assign_issues,assign_appeal', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (6, 54, 'audit,handel_appeal,assign_appeal,modification_result,verify,confirm,handel_issues,handel_issuesaudit', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (6, 55, 'create_appeal', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (6, 57, 'view,delete_text,export_text', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (6, 68, 'view,export', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (6, 70, 'view', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (6, 73, 'view', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (6, 75, 'create,delete,edit', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (6, 76, 'create,delete,view,edit', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (6, 77, 'create,delete', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (6, 90, 'view', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (7, 43, 'view,export', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (7, 44, 'view_report,export,create_report', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (7, 45, 'view', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (7, 47, 'view,export,edit_asr_text', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (7, 50, 'view', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (7, 53, 'view_info,assign_appeal,etail_assign_appeal,etail_confirm,etail_handel_issues', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (7, 55, 'create_appeal', 'default');
INSERT INTO `privileges`(`role`, `module`, `cmd_list`, `enterprise`) VALUES (7, 57, 'view,delete_text,export_text', 'default');
