-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- Fit BF user history data

-- 1.Delete the 'deployer' user ,
delete from QISYS_auth.users where uuid = '4b21158a395311e88a710242ac110002';
delete from QISYS_auth.user_detail where USER_UUID = '4b21158a395311e88a710242ac110002';
delete from QISYS_auth.user_privileges where human = '4b21158a395311e88a710242ac110002';

-- 2.Add organization relationship
INSERT into QISYS_auth.user_detail (USER_CODE,ORGAN_CODE,ORGAN_NAME,DIC_KEY,DIC_NAME,USER_NAME,USER_UUID,ENTERPRISE) (
		SELECT
			u.uuid user_code,
			o.ORGAN_CODE,
			o.ORGAN_NAME,
			case when u.type in (0,1) then '2' else '1' end DIC_KEY,
			case when u.type in (0,1) then '质检组' else '坐席组' end DIC_NAME,
			u.user_name,
			u.uuid user_uuid,
			u.enterprise 
		FROM
			auth.users u
			LEFT JOIN QISYS_auth.users iu ON u.uuid = iu.uuid,
			QISYS_auth.organization o 
		WHERE
			u.organization = o.ID 
			AND iu.uuid IS NULL 
);

-- 3.Add user role
INSERT into QISYS_auth.user_privileges (human,role,organization_id,enterprise)(
	SELECT
			u.uuid human,
			case when u.type in (0,1) then 'ed8c7632d5e9449b85d49d0db4da0637' else '7' end role,
			u.organization organization_id,
			u.enterprise enterprise
		FROM
			auth.users u
			LEFT JOIN QISYS_auth.users iu on u.uuid = iu.uuid
		WHERE
		 iu.uuid IS NULL 
);

-- 4.Add user
insert into  QISYS_auth.users(id,uuid,display_name,user_name,email,phone,enterprise,type,`status`) (
	SELECT
		u.id,
		u.uuid,
		u.display_name,
		u.user_name,
		u.email,
		u.phone,
		u.enterprise,
		u.type,
		u.`status`
	FROM
		auth.users u
		LEFT JOIN QISYS_auth.users iu ON u.uuid = iu.uuid 
		where  iu.uuid IS NULL

);

