-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied 
update `QISYS_auth`.`modules` set cmd_list = 'audit,handel_appeal,assign_appeal,modification_result,verify,confirm,handel_issues,handel_issuesaudit,reassign' where `code` = 'quality_handlingdetail';
update `QISYS_auth`.`modules` set cmd_list = 'view,reassign' where `code` = 'my_task_qcdetail';