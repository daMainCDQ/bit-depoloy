-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied 
update modules set cmd_list ='view' where `code` = 'regclass_analysis';
update modules set cmd_list ='view' where `code` ='cross_analysis';
update modules set cmd_list ='view,delete_text,export_text' where `code`='textbook_example';
update modules set cmd_list ='view,export,edit_asr_text' where `code`='quality_results';

update modules set `code` = 'seat_evaluation' where `code`='statistical_kanban';
update modules set cmd_list ='view,export' where `code`='seat_evaluation';

update `privileges` set cmd_list = (select cmd_list from modules where `code` = 'regclass_analysis') where module =(select id from modules where `code` = 'regclass_analysis');
update `privileges` set cmd_list = (select cmd_list from modules where `code` ='cross_analysis')  where module =(select id from modules where `code` = 'cross_analysis');
update `privileges` set cmd_list = (select cmd_list from modules where `code`='textbook_example')  where module =(select id from modules where `code` = 'textbook_example');
update `privileges` set cmd_list = (select cmd_list from modules where `code`='quality_results')  where module =(select id from modules where `code` = 'quality_results');
update `privileges` set cmd_list = (select cmd_list from modules where `code`='seat_evaluation')  where module =(select id from modules where `code` = 'seat_evaluation');

