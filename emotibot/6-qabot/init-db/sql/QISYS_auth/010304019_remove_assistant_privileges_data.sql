-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied 
delete from `privileges` where module = (select id from modules where `code`='assistant_process_navigation');
delete from modules where `code`='assistant_process_navigation';
delete from module_group where `code`='assistant_process_navigation';
