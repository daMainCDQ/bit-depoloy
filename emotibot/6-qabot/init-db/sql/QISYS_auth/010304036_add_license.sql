-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

CREATE TABLE IF NOT EXISTS `license_enterprise` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `enterprise_id` char(32) NOT NULL DEFAULT '' COMMENT '企业UUID',
  `enterprise_name` varchar(64) NOT NULL DEFAULT '' COMMENT '企业名称',
  `start_time` varchar(64) NOT NULL DEFAULT '0' COMMENT '开始时间',
  `expire_time` varchar(64) NOT NULL DEFAULT '0' COMMENT '过期时间,加密',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'enterprise license status',
  `total_amount` varchar(128) DEFAULT '0' COMMENT '文件总数量',
  `amount_per_day` varchar(128) DEFAULT NULL COMMENT '每天最大文件数量',
  `total_length` varchar(128) DEFAULT NULL COMMENT '总时长（h）',
  `length_per_day` varchar(128) DEFAULT NULL COMMENT '每天最大时长（h）',
  `used_total_amount` varchar(128) DEFAULT NULL COMMENT '已用文件总数量',
  `used_total_length` varchar(128) DEFAULT NULL COMMENT '已用总时长（h）',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8mb4 COMMENT='企业授权表';

CREATE TABLE IF NOT EXISTS `license_enterprise_record` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `enterprise_id` char(32) NOT NULL DEFAULT '' COMMENT '企业UUID',
  `amount` bigint(20) DEFAULT NULL COMMENT '文件数量',
  `length` bigint(20) DEFAULT NULL COMMENT '时长（秒）',
  `time` bigint(20) NOT NULL DEFAULT '0' COMMENT '时间（只保存到天的数据）',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `query_index` (`enterprise_id`,`time`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8mb4 COMMENT='企业授权使用记录';

CREATE TABLE IF NOT EXISTS `license_usage_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `usage_amount` varchar(128) DEFAULT NULL COMMENT '已用总文件数量 加密',
  `usage_length` varchar(128) DEFAULT NULL COMMENT '已用总时长 加密',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8mb4 COMMENT='许可证已用信息';




