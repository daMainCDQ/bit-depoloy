-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `QISYS_auth`.`enterprises` 
MODIFY COLUMN `uuid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '企业id' AFTER `id`;
ALTER TABLE `QISYS_auth`.`human` 
MODIFY COLUMN `uuid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '帐号id或帐号群组id' AFTER `id`;
ALTER TABLE `QISYS_auth`.`license_enterprise` 
MODIFY COLUMN `enterprise_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '企业UUID' AFTER `id`;
ALTER TABLE `QISYS_auth`.`license_enterprise_record` 
MODIFY COLUMN `enterprise_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '企业UUID' AFTER `id`;
ALTER TABLE `QISYS_auth`.`module_group` 
MODIFY COLUMN `code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '权限群组代号' AFTER `id`;
ALTER TABLE `QISYS_auth`.`modules` 
MODIFY COLUMN `code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '程式码或frontend中的代码' AFTER `id`,
MODIFY COLUMN `enterprise` varchar(32) NULL COMMENT '0: 表示系统预设模组, 不为0: 该企业有自订该模组的开关或开放的权限' AFTER `name`,
MODIFY COLUMN `cmd_list` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '该模组所有的操作列表 ex: view, create, edit, export, import, delete, 该栏位会影响前端权限群组显示' AFTER `enterprise`,
MODIFY COLUMN `group` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '该模组所属群组' AFTER `created_time`;
ALTER TABLE `QISYS_auth`.`privileges` 
MODIFY COLUMN `cmd_list` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '拥有模组的哪些权限' AFTER `module`;
ALTER TABLE `QISYS_auth`.`roles` 
MODIFY COLUMN `uuid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '角色id' AFTER `id`,
MODIFY COLUMN `enterprise` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '所属企业' AFTER `name`;
ALTER TABLE `QISYS_auth`.`user_info` 
MODIFY COLUMN `user_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户id' AFTER `id`;
ALTER TABLE `QISYS_auth`.`user_privileges` 
MODIFY COLUMN `human` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'Human id' AFTER `id`,
MODIFY COLUMN `machine` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT 'Machine id' AFTER `human`,
MODIFY COLUMN `role` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'Human id 对定到machine id的角色权限' AFTER `machine`;
ALTER TABLE `QISYS_auth`.`users` 
MODIFY COLUMN `uuid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '帐号id' AFTER `id`,
MODIFY COLUMN `user_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户帐号' AFTER `display_name`,
MODIFY COLUMN `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户email' AFTER `user_name`,
MODIFY COLUMN `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户电话' AFTER `email`,
MODIFY COLUMN `enterprise` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户所属企业, 若为空则type必须为0' AFTER `phone`,
MODIFY COLUMN `password` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'Password in MD5 after aes 128' AFTER `type`,
MODIFY COLUMN `product` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '建立日期' AFTER `password`;
