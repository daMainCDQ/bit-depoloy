-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
INSERT INTO `QISYS_auth`.`organization`(`ID`, `ORGAN_NAME`, `ORGAN_CODE`, `PARENT_ORGAN_CODE`, `IS_DELETE`,`ENTERPRISE`,`PARENT_ID`)  VALUES (1, 'emotibot', '0001', 'enterprise', 1,'bb3e3925f0ad11e7bd860242ac120003',0);

update  user_detail ud set ud.ORGAN_CODE='0001',ud.USER_UUID='4b21158a395311e88a710242ac110003',ud.ENTERPRISE='bb3e3925f0ad11e7bd860242ac120003' where ud.USER_CODE = '4b21158a395311e88a710242ac110003';

update `privileges` p set p.enterprise = 'bb3e3925f0ad11e7bd860242ac120003'  where p.id < 125;

update  user_privileges up set up.organization_id = 1 ,up.enterprise = 'bb3e3925f0ad11e7bd860242ac120003' where up.human = '4b21158a395311e88a710242ac110003';