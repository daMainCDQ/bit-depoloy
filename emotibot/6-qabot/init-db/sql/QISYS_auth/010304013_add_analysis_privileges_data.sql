-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied 
INSERT INTO `QISYS_auth`.`module_group`(`id`, `code`, `zh_cn`, `zh_tw`, `created_time`, `product`) VALUES (18, 'hot_spot_analysis', '热点分析', '', '2019-05-23 12:50:30', 3);
INSERT INTO `QISYS_auth`.`module_group`(`id`, `code`, `zh_cn`, `zh_tw`, `created_time`, `product`) VALUES (20, 'set_up', '设置', '', '2019-05-23 12:50:30', 3);

INSERT INTO `QISYS_auth`.`modules`(`id`, `code`, `name`, `enterprise`, `cmd_list`, `description`, `status`, `created_time`, `group`, `product`) VALUES (68, 'analysis_of_hot_words', '热词分析', 'bb3e3925f0ad11e7bd860242ac120003', 'view,export', '', 0, '2019-05-23 12:50:30', '18', 3);
INSERT INTO `QISYS_auth`.`modules`(`id`, `code`, `name`, `enterprise`, `cmd_list`, `description`, `status`, `created_time`, `group`, `product`) VALUES (70, 'correlation_analysis', '关联性分析', 'bb3e3925f0ad11e7bd860242ac120003', 'view', '', 0, '2019-05-23 12:50:30', '18', 3);
INSERT INTO `QISYS_auth`.`modules`(`id`, `code`, `name`, `enterprise`, `cmd_list`, `description`, `status`, `created_time`, `group`, `product`) VALUES (73, 'cross_analysis', '交叉分析', 'bb3e3925f0ad11e7bd860242ac120003', 'view,export', '', 0, '2019-05-23 12:50:30', '18', 3);
INSERT INTO `QISYS_auth`.`modules`(`id`, `code`, `name`, `enterprise`, `cmd_list`, `description`, `status`, `created_time`, `group`, `product`) VALUES (75, 'custom_filter_settings', '自定义过滤词表设置', 'bb3e3925f0ad11e7bd860242ac120003', 'create,delete,edit', '', 0, '2019-05-23 12:50:30', '20', 3);
INSERT INTO `QISYS_auth`.`modules`(`id`, `code`, `name`, `enterprise`, `cmd_list`, `description`, `status`, `created_time`, `group`, `product`) VALUES (76, 'general_filter_settings', '通用过滤词表设置', 'bb3e3925f0ad11e7bd860242ac120003', 'create,delete,view,edit', '', 0, '2019-05-23 12:50:30', '20', 3);
INSERT INTO `QISYS_auth`.`modules`(`id`, `code`, `name`, `enterprise`, `cmd_list`, `description`, `status`, `created_time`, `group`, `product`) VALUES (77, 'custom_settings_hot_words', '自定义热词设置', 'bb3e3925f0ad11e7bd860242ac120003', 'create,delete', '', 0, '2019-05-23 12:50:30', '20', 3);
INSERT INTO `QISYS_auth`.`modules`(`id`, `code`, `name`, `enterprise`, `cmd_list`, `description`, `status`, `created_time`, `group`, `product`) VALUES (90, 'regclass_analysis', '聚类分析', 'bb3e3925f0ad11e7bd860242ac120003', '', '', 0, '2019-11-13 15:22:30', '18', 3);

INSERT INTO `QISYS_auth`.`privileges`(`id`, `role`, `module`, `cmd_list`, `created_time`) VALUES (21, 1, 68, 'view,export', '2019-11-21 10:52:03');
INSERT INTO `QISYS_auth`.`privileges`(`id`, `role`, `module`, `cmd_list`, `created_time`) VALUES (22, 1, 70, 'view', '2019-11-21 10:52:03');
INSERT INTO `QISYS_auth`.`privileges`(`id`, `role`, `module`, `cmd_list`, `created_time`) VALUES (23, 1, 73, 'view,export', '2019-11-21 10:52:03');
INSERT INTO `QISYS_auth`.`privileges`(`id`, `role`, `module`, `cmd_list`, `created_time`) VALUES (24, 1, 75, 'create,delete,edit', '2019-11-21 10:52:03');
INSERT INTO `QISYS_auth`.`privileges`(`id`, `role`, `module`, `cmd_list`, `created_time`) VALUES (25, 1, 76, 'create,delete,view,edit', '2019-11-21 10:52:03');
INSERT INTO `QISYS_auth`.`privileges`(`id`, `role`, `module`, `cmd_list`, `created_time`) VALUES (26, 1, 77, 'create,delete', '2019-11-21 10:52:03');
INSERT INTO `QISYS_auth`.`privileges`(`id`, `role`, `module`, `cmd_list`, `created_time`) VALUES (27, 1, 90, '', '2019-11-21 10:52:03');