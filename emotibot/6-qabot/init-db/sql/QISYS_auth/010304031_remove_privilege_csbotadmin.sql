-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
delete FROM user_privileges  WHERE human = '4b21158a395311e88a710242ac110003' AND role IN ('qualityGroupLeader','qualityDirector','qualityInspector','tableGroupLeader','tableDirector','tableStaff');