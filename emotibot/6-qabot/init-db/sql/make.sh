#!/bin/bash
# create the yaml on demand

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
export env=$1

if [[ ${#INIT_MYSQL_HOST} == 0 ]]; then
	export INIT_MYSQL_HOST=172.17.0.1
fi
if [[ ${#INIT_MYSQL_PORT} == 0 ]]; then
	export INIT_MYSQL_PORT=3306
fi

if [[ ${#INIT_MYSQL_USER} == 0 ]]; then
	export INIT_MYSQL_USER=root
fi
if [[ ${#INIT_MYSQL_PASSWORD} == 0 ]]; then
	export INIT_MYSQL_PASSWORD=password
fi

# since docker always run in clean state & we will replace the whole file, it is unnecessary to do the remove cleanup.
# rm -f $DIR/*.yml

grep -v '^#' "$DIR/DB_LIST" | while IFS= read -r DB_NAME
do
    export DB_NAME=$DB_NAME
    echo "creating $DB_NAME config"
    envsubst < "$DIR/db_yaml.template" > "$DIR/$DB_NAME.yml"
done