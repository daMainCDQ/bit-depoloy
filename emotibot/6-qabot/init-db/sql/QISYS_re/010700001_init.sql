-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

/*!40101 SET NAMES utf8mb4 */;

CREATE TABLE `tab_re_load_map` (
  `_id` varchar(24) NOT NULL DEFAULT '' COMMENT 'id',
  `model_id` varchar(24) NOT NULL DEFAULT '' COMMENT '模型id',
  `app_id` varchar(100) NOT NULL DEFAULT '' COMMENT '機器人id',
  `queue_id` varchar(24) NOT NULL DEFAULT '' COMMENT '隊列id',
  `model_version` varchar(30) DEFAULT NULL COMMENT '模型版本',
  `model_type` varchar(50) DEFAULT NULL COMMENT '模型類型',
  PRIMARY KEY (`_id`),
  KEY `app_id` (`app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='模型對應機器人表';

CREATE TABLE `tab_re_queue` (
  `_id` varchar(24) NOT NULL DEFAULT '' COMMENT 'id',
  `app_id` varchar(100) DEFAULT NULL COMMENT '機器人id',
  `trainset_id` varchar(24) DEFAULT NULL COMMENT '訓練id',
  `status` varchar(20) DEFAULT NULL COMMENT '狀態',
  `model_id` varchar(24) DEFAULT NULL COMMENT '模型id',
  `create_time` datetime DEFAULT NULL COMMENT '創建時間',
  `config` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '設定值',
  `auto_reload` tinyint(1) DEFAULT NULL COMMENT '是否自動讀取',
  `model_version` varchar(30) DEFAULT NULL COMMENT '模型版本',
  `model_type` varchar(50) DEFAULT NULL COMMENT '模型類型',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='模型對應任務表';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

DROP TABLE IF EXISTS `tab_re_load_map`;
DROP TABLE IF EXISTS `tab_re_queue`;
