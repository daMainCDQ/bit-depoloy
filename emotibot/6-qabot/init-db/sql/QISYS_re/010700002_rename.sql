-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

/*!40101 SET NAMES utf8mb4 */;

RENAME TABLE tab_re_load_map TO ie_load_map;
RENAME TABLE tab_re_queue TO ie_queue;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

RENAME TABLE ie_load_map TO tab_re_load_map;
RENAME TABLE ie_queue TO tab_re_queue;
