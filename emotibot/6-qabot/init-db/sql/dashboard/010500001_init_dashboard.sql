-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE DATABASE IF NOT EXISTS `dashboard` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

USE `dashboard`;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gorp_migrations
-- ----------------------------
CREATE TABLE IF NOT EXISTS `gorp_migrations` (
  `id` varchar(255) NOT NULL,
  `applied_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for hot_word_list
-- ----------------------------
CREATE TABLE IF NOT EXISTS `hot_word_list` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` bigint(20) NOT NULL,
  `modified_date` bigint(20) NOT NULL,
  `word` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for stop_word
-- ----------------------------
CREATE TABLE IF NOT EXISTS `stop_word` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` bigint(20) NOT NULL,
  `modified_date` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=275 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for stop_word_content
-- ----------------------------
CREATE TABLE IF NOT EXISTS `stop_word_content` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `text` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=18202 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for stop_word_rel
-- ----------------------------
CREATE TABLE IF NOT EXISTS `stop_word_rel` (
  `s_id` bigint(20) NOT NULL,
  `w_id` bigint(20) NOT NULL,
  PRIMARY KEY (`s_id`,`w_id`) USING BTREE,
  UNIQUE KEY `UK_lso832m22o3s3k3jpt9cv551b` (`w_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

SET FOREIGN_KEY_CHECKS = 1;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP DATABASE `dashboard`;
