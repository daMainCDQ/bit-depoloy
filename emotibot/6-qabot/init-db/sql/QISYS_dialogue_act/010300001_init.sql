-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

/*!40101 SET NAMES utf8mb4 */;

CREATE DATABASE IF NOT EXISTS `QISYS_dialogue_act` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

USE `QISYS_dialogue_act`;

CREATE TABLE `ie_load_map` (
  `_id` varchar(24) NOT NULL DEFAULT '',
  `model_id` varchar(24) NOT NULL DEFAULT '',
  `app_id` varchar(100) NOT NULL DEFAULT '',
  `queue_id` varchar(24) NOT NULL DEFAULT '',
  `model_version` varchar(30) DEFAULT NULL,
  `model_type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  KEY `app_id` (`app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `ie_queue` (
  `_id` varchar(24) NOT NULL DEFAULT '',
  `app_id` varchar(100) DEFAULT NULL,
  `trainset_id` varchar(24) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `model_id` varchar(24) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `config` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `auto_reload` tinyint(1) DEFAULT NULL,
  `model_version` varchar(30) DEFAULT NULL,
  `model_type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

DROP TABLE IF EXISTS `ie_load_map`;
DROP TABLE IF EXISTS `ie_queue`;
