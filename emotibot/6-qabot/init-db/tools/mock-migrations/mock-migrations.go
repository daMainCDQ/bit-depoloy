//Package main
//Usage of this binary:
//	check the migration folders, and mock migration files below the semantic naming rule.
//
//	./mock-migration {version}
//		{version}: semantic version of your current install.
package main

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"

	"gopkg.in/yaml.v2"

	_ "github.com/go-sql-driver/mysql"

	"github.com/coreos/go-semver/semver"
)

func main() {
	if level, ok := os.LookupEnv("MODULE_LEVEL"); ok {
		if level == "0" {
			logger = log.New(os.Stdout, "[DEBUG]", log.Ltime|log.Lshortfile)
		}
	}
	if len(os.Args) < 2 {
		fmt.Println("should have at least one argument")
		os.Exit(1)
	}
	ver := os.Args[1]
	curVer, err := semver.NewVersion(ver)
	logger.Println("current version: ", curVer)
	if err != nil {
		logger.Printf("%v\n", err)
		os.Exit(1)
	}
	cwd := getwd()
	logger.Println("current location: " + cwd)
	data, err := ioutil.ReadFile(cwd + "/DB_LIST")
	if err != nil {
		fmt.Println("reading from DB_LIST failed, ", err)
		os.Exit(1)
	}
	tables := strings.Split(string(data), "\n")
	var tblRecords = make(map[migrateConfig][]string, 4)
	for _, t := range tables {
		t = strings.TrimSpace(t)
		if len(t) == 0 || strings.HasPrefix(t, "#") {
			continue
		}
		fileName := cwd + "/" + t + ".yml"
		data, err := ioutil.ReadFile(fileName)
		if err != nil {
			logger.Println("open config ", fileName, " failed, err: ", err)
			continue
		}
		var config migrateConfig
		err = yaml.Unmarshal(data, &config)
		if err != nil {
			logger.Println("unmarshal config ", fileName, " failed, err: ", err)
			continue
		}
		filesDir := filepath.Join(cwd, config.Development.Dir)
		records, err := CompareDir(curVer, filesDir)
		if err != nil {
			logger.Println("compare failed in ", filesDir, ", err: ", err)
			continue
		}
		// Filter out empty dir
		if len(records) == 0 {
			continue
		}
		tblRecords[config] = records
	}

	if len(tblRecords) == 0 {
		fmt.Println("no mock config has been found")
		os.Exit(0)
	}

	logger.Println("records found:")
	for c, records := range tblRecords {
		logger.Println("\tSource: " + c.Development.Datasource)
		logger.Printf(strings.Join(records, "\t\t\n"))
		logger.Printf("\n")
	}
	var successRound = 0
	for config, records := range tblRecords {
		logger.Println("Connect to ", config.Development.Dialect, " of ", config.Development.Datasource)
		db, err := sql.Open(config.Development.Dialect, config.Development.Datasource)
		if err != nil {
			logger.Printf("open sql db failed, %v\n", err)
			continue
		}
		//use context to limit the connection time.
		//context will not work with some driver, check here(https://github.com/golang/go/issues/27476)
		ctx, cancelCtx := context.WithTimeout(context.Background(), time.Second*3)
		if err = db.PingContext(ctx); err != nil {
			logger.Println("connect to db failed", err)
			cancelCtx()
			continue
		}
		cancelCtx()
		err = MockDB(db, records)
		if err != nil {
			logger.Println("mock db failed, ", err)
			continue
		}
		successRound++
	}
	if successRound != len(tblRecords) {
		fmt.Println("mock table total: ", len(tblRecords), "success: ", successRound)
		os.Exit(1)
	}

}

var getwd = func() string {
	wd, _ := os.Getwd()
	return wd
}

var logger = log.New(ioutil.Discard, "", log.Ltime|log.Lshortfile)

type migrateConfig struct {
	Development struct {
		Dialect    string `yaml:"dialect"`
		Datasource string `yaml:"datasource"`
		Dir        string `yaml:"dir"`
	} `yaml:"development"`
}

func newSemverWithoutDot(version string) (*semver.Version, error) {
	if len(version) != 6 {
		return nil, errors.New("version string should be exact 6 digit")
	}

	formatVer := version[0:2] + "." + version[2:4] + "." + version[4:6]
	return semver.NewVersion(formatVer)
}

// CompareDir will compare every sql file in path with curVer.
// any file version lower than curVer will be include in return string.
// if any sql file does not match version syntax or io read failed, error will be return.
func CompareDir(curVer *semver.Version, path string) (records []string, err error) {
	sqlFiles, err := ioutil.ReadDir(path)
	if err != nil {
		return nil, fmt.Errorf("find migration failed, %v", err)
	}
	for _, sf := range sqlFiles {
		if ext := filepath.Ext(sf.Name()); len(sf.Name()) < 10 && ext != ".sql" {
			continue
		}
		logger.Println(sf.Name())
		fileVer, err := newSemverWithoutDot(sf.Name()[:6])
		if err != nil {
			return nil, fmt.Errorf("create file version failed, %v", err)
		}
		logger.Println("file version: " + fileVer.String())
		if curVer.LessThan(*fileVer) {
			continue
		}
		records = append(records, sf.Name())
	}
	return records, nil
}

var createTblTemplate = fmt.Sprintf(`CREATE TABLE IF NOT EXISTS %s (
  %s varchar(255) NOT NULL,
  %s datetime DEFAULT NULL,
  PRIMARY KEY (%s)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;`, gorpTable.TblName, gorpTable.ColID, gorpTable.COlAppliedAt, gorpTable.ColID)

var insertRowTemplate = fmt.Sprintf(`INSERT IGNORE INTO %s (%s, %s) VALUE(?, ?)`, gorpTable.TblName, gorpTable.ColID, gorpTable.COlAppliedAt)

var gorpTable = struct {
	TblName      string
	ColID        string
	COlAppliedAt string
}{
	TblName:      "gorp_migrations",
	ColID:        "id",
	COlAppliedAt: "applied_at",
}

// MockDB create the gorp table for the db with records in it's row.
// return error if db exec have problem.
func MockDB(db *sql.DB, records []string) error {
	if len(records) <= 0 {
		return nil
	}
	_, err := db.Exec(createTblTemplate)
	if err != nil {
		return fmt.Errorf("sql create table failed, %v", err)
	}
	tx, err := db.Begin()
	if err != nil {
		return fmt.Errorf("sql acquire transaction failed, %v", err)
	}
	stmt, err := tx.Prepare(insertRowTemplate)
	if err != nil {
		return fmt.Errorf("prepare insert failed, %v", err)
	}
	defer stmt.Close()
	for _, r := range records {
		_, err := stmt.Exec(r, time.Now())
		if err != nil {
			tx.Rollback()
			return fmt.Errorf("stmt exec failed, %v", err)
		}
	}

	tx.Commit()
	return nil
}
