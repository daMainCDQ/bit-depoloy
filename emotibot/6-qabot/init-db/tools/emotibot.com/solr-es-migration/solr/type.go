package solr

import (
	"encoding/json"
	"strconv"

	"emotibot.com/solr-es-migration/logger"
)

type LukeResponse struct {
	Fields DatabaseFields `json:"fields"`
}

type DatabaseFields struct {
	Database struct {
		Distinct int64         `json:"distinct"`
		TopTerms []interface{} `json:"topTerms"`
	} `json:"database"`
}

type CursorResponse struct {
	Header struct {
		Status int `json:"status"`
		QTime  int `json:"QTime"`
	} `json:"header"`
	Response struct {
		NumFound int          `json:"numFound"`
		Start    int          `json:"start"`
		Docs     []*QACoreDoc `json:"docs"`
	} `json:"response"`
	NextCursorMark string `json:"nextCursorMark"`
}

type QACoreDoc struct {
	ID               string             `json:"id"`
	RelatedSentences []*RelatedSentence `json:"related_sentences"`
	Sentence         string             `json:"sentence"`
	SentenceOrig     string             `json:"sentence_original"`
	SentenceType     string             `json:"sentence_type"`
	SentenceCU       *SentenceCU        `json:"sentence_cu"`
	SentenceKeywords string             `json:"sentence_keywords"`
	SentencePos      string             `json:"sentence_pos"`
	Source           string             `json:"source"`
	AutofillEnabled  *bool              `json:"autofill_enabled"`
}

func (doc *QACoreDoc) UnmarshalJSON(d []byte) error {
	type Alias QACoreDoc
	aux := &struct {
		SentenceCU string `json:"sentence_cu"`
		*Alias
	}{
		Alias: (*Alias)(doc),
	}

	err := json.Unmarshal(d, &aux)
	if err != nil {
		logger.Warn.Printf("Unmarshal JSON, field: 'sentence_cu' failed, error: %s", err.Error())
		return nil
	}

	if aux.SentenceCU != "" {
		sentenceCU := SentenceCU{}

		err = json.Unmarshal([]byte(aux.SentenceCU), &sentenceCU)
		if err != nil {
			return err
		}

		doc.SentenceCU = &sentenceCU
	}

	return nil
}

type RelatedSentence struct {
	Answer  string `json:"answer"`
	CU      *CU    `json:"cu"`
	Creator string `json:"creator"`
}

func (s *RelatedSentence) UnmarshalJSON(d []byte) error {
	type Alias RelatedSentence
	aux := &struct {
		CU string `json:"cu"`
		*Alias
	}{
		Alias: (*Alias)(s),
	}

	// Unquote JSON string first.
	u, err := strconv.Unquote(string(d))
	if err != nil {
		return err
	}

	err = json.Unmarshal([]byte(u), &aux)
	if err != nil {
		return err
	}

	if aux.CU != "" {
		cu := CU{}

		err = json.Unmarshal([]byte(aux.CU), &cu)
		if err != nil {
			return err
		}

		s.CU = &cu
	}

	return nil
}

type CU struct {
	Emotion   Emotion   `json:"emotion"`
	Topic     Topic     `json:"topic"`
	SpeechAct SpeechAct `json:"speech_act"`
}

type Emotion struct {
	Res []EmotionRes `json:"res"`
}

type EmotionRes struct {
	Item  string  `json:"item"`
	Score float64 `json:"score"`
}

type Topic struct {
	Res []TopicRes `json:"res"`
}

type TopicRes struct {
	Item  string  `json:"item"`
	Score float64 `json:"score,string"`
}

type SpeechAct struct {
	Res []SpeechActRes `json:"res"`
}

type SpeechActRes struct {
	Item  string  `json:"item"`
	Score float64 `json:"score"`
}

type SentenceCU struct {
	CU
}
