module emotibot.com/emotigo/module/eeimporter

go 1.13

replace (
	emotibot.com/emotigo/pkg/api => ./pkg/api
	emotibot.com/emotigo/pkg/api/emotion-engine => ./pkg/api/emotion-engine
	emotibot.com/emotigo/pkg/logger => ./pkg/logger
)

require emotibot.com/emotigo/pkg/api/emotion-engine v0.0.0-00010101000000-000000000000
