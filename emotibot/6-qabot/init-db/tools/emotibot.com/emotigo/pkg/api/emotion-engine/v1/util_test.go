package emotionengine

import (
	"testing"
)

func TestYAMLToEmotion(t *testing.T) {
	yamlFilePath := "../../../../testdata/happiness.yaml"

	_, err := YAMLToEmotion(yamlFilePath)
	if err != nil {
		t.Error(err)
	}
}

func TestEmotionToYAML(t *testing.T) {
	yamlFilePath := "../../../../testdata/scare.yaml"

	testCase := Emotion{
		Name: "害怕",
		PositiveSentence: []string{
			"没有安全感",
			"我越来越害怕",
			"你干嘛东西这么吓人",
			"我真的担心他会不要我了",
			"我已经吓死了",
			"我担心我姐姐",
			"我怕天谴",
			"有点恐怖",
		},
		NegativeSentence: []string{
			"听起来你刚刚是有点过分了",
			"他不嫌丢人我还嫌丢人呢",
			"哥哥你唱歌真好听",
		},
	}

	err := EmotionToYAML(testCase, yamlFilePath)
	if err != nil {
		t.Error(err)
	}
}
