//Package mlp provide a api client to interact with mlp module
package mlp

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"time"

	"emotibot.com/emotigo/pkg/logger"

	"emotibot.com/emotigo/pkg/api"
)

//MlpClient is the api client to communicate with
type MlpClient struct {
	// Transport specify how the HTTP Request are handled.
	// Which should be compatible with the net/http package.
	// If nil, http.DefaultClient is used.
	Transport api.HTTPClient
	// ServerURL is the host and port of the emotion-engine module.
	// which shouold be "http://{host}:{port}"
	ServerURL string
}

// Train will tell emotion-engine to create a model based on the model's AppID
// It will return modelID if successful. On error, modelID will be empty
// returned error can be http, emotion-engine, or other error.
// It is seperated by message prefix HTTP, EE, or OTHER.
func (c *MlpClient) Train(apiModel Model) (modelID string, err error) {
	var transporter api.HTTPClient

	if c.Transport == nil {
		transporter = http.DefaultClient
	} else {
		transporter = c.Transport
	}

	data, err := json.Marshal(apiModel)
	if err != nil {
		return "", fmt.Errorf("OTHER: json marshal failed, %v", err)
	}

	req, err := http.NewRequest(http.MethodPost, c.ServerURL+"/mlpapi/offline/mlp/v1/api/train", bytes.NewBuffer(data))
	if err != nil {
		return "", fmt.Errorf("OTHER: New Request failed, %v", err)
	}
	req.Header.Add("content-type", "application/json")
	resp, err := transporter.Do(req)
	if err != nil {
		return "", fmt.Errorf("HTTP: %v", err)
	}
	defer resp.Body.Close()

	data, err = ioutil.ReadAll(resp.Body)
	var respBody trainResponse
	err = json.Unmarshal(data, &respBody)
	if err != nil {
		logger.Error.Println("raw output: ", string(data))
		return "", fmt.Errorf("response format invalid, %v", err)
	}
	if respBody.Status != "OK" {
		return "", fmt.Errorf("got unsuccessful status '%s', error message: %s", respBody.Status, respBody.ErrMsg)
	}
	return respBody.ModelID, nil
}

// Predict will sending request to emotion-engine to get the PredictResult from it.
// It will return a slice of Predicts in response of the request.Sentence.
// returned error can be http, emotion-engine, or other error.
// It is seperated by message prefix HTTP, EE, or OTHER.
func (c *MlpClient) Predict(request PredictRequest) (predictions []Predict, err error) {
	var transporter api.HTTPClient

	if c.Transport == nil {
		transporter = http.DefaultClient
	} else {
		transporter = c.Transport
	}
	if len(request.ProblemIDs) == 0 {
		return nil, fmt.Errorf("request.ProblemIDs should not be empty")
	}
	data, err := json.Marshal(request)
	req, err := http.NewRequest(http.MethodPost, c.ServerURL+"/mlpapi/online/mlp/v1/api/predict", bytes.NewBuffer(data))
	req.Header.Add("content-type", "application/json")
	resp, err := transporter.Do(req)
	if err != nil {
		return nil, fmt.Errorf("Transport Do request failed, %v", err)
	}
	defer resp.Body.Close()
	data, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("io read failed, %v", err)
	}
	var respBody predictResponse
	err = json.Unmarshal(data, &respBody)
	if err != nil {
		logger.Error.Println("raw output: ", string(data))
		return nil, fmt.Errorf("predict response is not valid, %v", err)
	}

	if respBody.Status != "OK" {
		if respBody.Error == "No model loaded." {
			return nil, ErrModelNotLoad
		}
		return nil, fmt.Errorf("got unsuccessful status '%s', error message: %s", respBody.Status, respBody.Error)
	}

	var result = []Predict{}
	for _, p := range respBody.Predictions {
		result = append(result, Predict{
			Label: p.Label,
			Score: int(p.Score),
		})
	}
	return result, nil
}

// HealthCheck check the given problemID is in valid state.
// If err return ErrModelNotLoad, caller should train data first.
// others err can be various issues(ex: network, IO etc...)
func (c *MlpClient) HealthCheck(problemID string) (err error) {
	loop := 3
	for i := 1; i <= loop; i++ {
		if err != nil {
			time.Sleep(time.Duration(1) * time.Second)
		}
		_, err = c.Predict(PredictRequest{
			ProblemIDs: []string{problemID},
			Sentence:   "APPID 測試",
		})
		if err == ErrModelNotLoad {
			return ErrModelNotLoad
		}
		if err == nil {
			break
		}
		if i == loop {
			return fmt.Errorf("request to ee failed, last try error: %v", err)
		}
	}
	return nil
}

// Wait the trained model ready
func (c *MlpClient) Wait(appID, modelID string, timeout time.Duration) error {
	var transporter api.HTTPClient

	if c.Transport == nil {
		transporter = http.DefaultClient
	} else {
		transporter = c.Transport
	}
	postData := []byte("{\"app_id\": \"" + appID + "\", \"model_id\": \"" + modelID + "\"}")

	startTime := time.Now()
	for {
		req, err := http.NewRequest(http.MethodPost, c.ServerURL+"/mlpapi/offline/mlp/v1/api/status", bytes.NewBuffer(postData))
		if err != nil {
			return fmt.Errorf("OTHER: New Request failed, %v", err)
		}
		req.Header.Add("content-type", "application/json")
		resp, err := transporter.Do(req)
		if err != nil {
			return fmt.Errorf("HTTP: Transport Do request failed, %v", err)
		}
		defer resp.Body.Close()
		data, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return fmt.Errorf("HTTP: io read failed, %v", err)
		}

		res := make(map[string]interface{})
		err = json.Unmarshal(data, &res)
		if err != nil {
			return fmt.Errorf("Wait: unmarshal response failed, %v", err)
		}
		logger.Info.Printf("%s", string(data))

		modelStatus, ok := res["train_status"].(string)
		if !ok {
			return fmt.Errorf("Wait: could not get the model status, %v", err)
		}
		if modelStatus == "completed" {
			break
		}

		if time.Since(startTime) > timeout {
			return fmt.Errorf("timeout to wait for model trained: %v", timeout)
		}

		// Wait for next round
		time.Sleep(time.Second * 1)
	}

	return nil
}

// Test the trained model with the training data
func (c *MlpClient) Test(model Model, sampleRate float32, detail bool) (*TotalTestResults, error) {
	totalCorporaSampleAmount := 0
	totalCorporaHitAmount := 0
	totalTestResults := NewTotalTestResults()
	if sampleRate < 0.0 || sampleRate >= 1.0 {
		return nil, fmt.Errorf("Test: sampleRate must be greater than 0 and less than 1")
	}
	for _, class := range model.Data.Classes {
		label := class.ClassName
		sampleAmount := 1
		if sampleRate > 0.0 {
			sampleAmount = int(float32(len(class.Corpus.Positive)) * sampleRate)
		}
		sampleIndexes := rand.Perm(sampleAmount)
		sampleHitAmount := 0
		totalTestResults.TestResults[label] = &TestResult{
			AccuracyRate: 0.0,
			TestedData: make([]struct {
				TestedResults []Predict `json:"tested_results"`
				Data          string    `json:"data"`
			}, 0, 100),
		}

		totalCorporaSampleAmount += sampleAmount
		for i := 0; i < sampleAmount; i++ {
			p := PredictRequest{
				ProblemIDs: []string{model.ProblemID},
				Sentence:   class.Corpus.Positive[sampleIndexes[i]],
			}
			results, err := c.Predict(p)
			if err != nil {
				return nil, fmt.Errorf("HTTP: predict failed, %v", err)
			}
			logger.Info.Printf("Test %s", class.Corpus.Positive[sampleIndexes[i]])
			if detail {
				totalTestResults.TestResults[label].TestedData =
					append(totalTestResults.TestResults[label].TestedData, struct {
						TestedResults []Predict `json:"tested_results"`
						Data          string    `json:"data"`
					}{
						TestedResults: results,
						Data:          p.Sentence,
					})
			}
			for _, result := range results {
				if label == result.Label {
					sampleHitAmount++
					break
				}
			}
		}
		totalCorporaHitAmount += sampleHitAmount
		totalTestResults.TestResults[label].AccuracyRate =
			rate(sampleHitAmount) / rate(sampleAmount)
	}

	totalTestResults.TotalAccuracyRate =
		rate(totalCorporaHitAmount) / rate(totalCorporaSampleAmount)

	return totalTestResults, nil
}

//PredictRequest is the input of Client.Predict api.
type PredictRequest struct {
	ProblemIDs []string `json:"problem_ids"`
	Sentence   string   `json:"sentence"`
}

//Predict is the result of Client.Predict().
type Predict struct {
	// Label is the category tag for the input sentence, which should be the same with Emotion Name.
	Label string
	// Score is the confident score of the Label, from 0 to 100.
	// Threshold should be determine by the user.
	Score int
}
type predictResponse struct {
	Status      string       `json:"status"`
	Predictions []rawPredict `json:"predictions"`
	Error       string       `json:"error"`
}

const (
	errPredictNotLoad = "No model loaded."
	errPredictLoading = "Model is loading."
)

var (
	// ErrModelNotLoad is the predefined error from emotion-engine.
	// Which mean model has not issued to load yet. or mode is not found.
	ErrModelNotLoad = errors.New("predifined model not loaded message has been returned")
	// ErrModelLoading is the predfined error from emotion-engine.
	// Which mean model hasn't finish loading yet. please try again later.
	ErrModelLoading = errors.New("")
)

type rawPredict struct {
	Label     string        `json:"label"`
	Score     float32       `json:"score"`
	OtherInfo []interface{} `json:"other_info"`
}

type Model struct {
	AppID       string    `json:"app_id"`
	ProblemID   string    `json:"problem_id"`
	ProblemName string    `json:"problem_name"`
	Data        ModelData `json:"data"`
	Algorithm   string    `json:"alg"`
}

type ModelData struct {
	Classes []ModelDataClass `json:"classes"`
}

type ModelDataClass struct {
	ClassName string `json:"class_name"`
	Corpus    Corpus `json:"corpus"`
}

type Corpus struct {
	Positive []string `json:"positive" yaml:"positive"`
	Negative []string `json:"negative" yaml:"negative"`
}

type trainResponse struct {
	Status  string `json:"status"`
	ModelID string `json:"model_id"`
	ErrMsg  string `json:"error"`
}

//oldModelStruct represent an emotion of the model. The name should always be unique in model.
type oldModelStruct struct {
	EmotionName string `json:"emotion_name" yaml:"emotion"`
	Sentences   Corpus `json:"sentences" yaml:"corpora"`
}

type rate float32

func (r *rate) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf("%.2f", *r)), nil
}

// TotalTestResults stores all label test results
type TotalTestResults struct {
	// Between 0.0 to 1.0 (0% to 100%)
	TotalAccuracyRate rate                   `json:"total_accuracy_rate"`
	TestResults       map[string]*TestResult `json:"test_results"`
}

// TestResult stores test results for each label
type TestResult struct {
	// Between 0.0 to 1.0 (0% to 100%)
	AccuracyRate rate `json:"accuracy_rate"`
	TestedData   []struct {
		TestedResults []Predict `json:"tested_results"`
		Data          string    `json:"data"`
	} `json:"tested_data,omitempty"`
}

// NewTotalTestResults creates a new TotalTestResults
func NewTotalTestResults() *TotalTestResults {
	t := &TotalTestResults{}
	t.TestResults = make(map[string]*TestResult)

	return t
}

func (t TotalTestResults) String() string {
	str := ""
	for label, data := range t.TestResults {
		str += fmt.Sprintf("Label: %s,\tAccuracy Rate: %.2f%%\n", label, data.AccuracyRate*100)
	}
	return fmt.Sprintf("%sTotal Hit Rate: %.2f%%", str, t.TotalAccuracyRate*100)
}
