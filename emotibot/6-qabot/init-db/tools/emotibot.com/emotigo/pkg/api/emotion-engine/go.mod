module emotionengine

go 1.13

require (
	emotibot.com/emotigo/pkg/api v0.0.0-00010101000000-000000000000
	emotibot.com/emotigo/pkg/logger v0.0.0-00010101000000-000000000000
	gopkg.in/yaml.v2 v2.2.7
)

replace (
	emotibot.com/emotigo/pkg/api => ../
	emotibot.com/emotigo/pkg/logger => ../../logger
)
