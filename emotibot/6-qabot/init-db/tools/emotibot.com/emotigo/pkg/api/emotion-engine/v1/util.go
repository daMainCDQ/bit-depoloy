package emotionengine

import (
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"strings"

	"gopkg.in/yaml.v2"
)

// CSVToEmotion is a convention function helper to read a example csv file to Emotion struct.
// csv format header(optional):
//		sentence, tag
// Column 1: training text
// Column 2: tag, to tell which
func CSVToEmotion(csvFile, emotionName string) (Emotion, error) {
	cf, err := os.Open(csvFile)
	if err != nil {
		return Emotion{}, fmt.Errorf("OS Open file failed, %v", err)
	}
	defer cf.Close()
	reader := csv.NewReader(cf)
	var e = Emotion{
		Name:             emotionName,
		PositiveSentence: make([]string, 0, 0),
		NegativeSentence: make([]string, 0, 0),
	}
	var sentence, tag string
	var i int
	const BOM = "\uFEFF"
	for {
		row, err := reader.Read()
		if err == io.EOF {
			break
		}
		i++
		if err != nil {
			return Emotion{}, fmt.Errorf("csv read error: %v", err)
		}

		if len(row) < 2 {
			return Emotion{}, fmt.Errorf("parsing error: row %d is empty", i)
		}

		sentence = strings.TrimSpace(row[0])
		tag = strings.TrimSpace(row[1])

		//skip header line
		passibleHeaderOfSentenceCases := []string{"", "sentence", BOM + "sentence"}
		if caseInsensitiveSearch(sentence, passibleHeaderOfSentenceCases) {
			continue
		}

		postiveTagCases := []string{"正例", "positive"}
		negativeTagCases := []string{"反例", "negative"}

		if caseInsensitiveSearch(tag, postiveTagCases) {
			e.PositiveSentence = append(e.PositiveSentence, sentence)
		} else if caseInsensitiveSearch(tag, negativeTagCases) {
			e.NegativeSentence = append(e.NegativeSentence, sentence)
		} else {
			return Emotion{}, fmt.Errorf("parsing error: wrong tag: %s in line %d\n", tag, i)
		}
	}
	return e, nil
}

// YAMLToEmotion reads a YAML emotion corpora file to an Emotion struct.
func YAMLToEmotion(yamlFilePath string) (Emotion, error) {
	f, err := os.Open(yamlFilePath)
	if err != nil {
		return Emotion{}, fmt.Errorf("OS Open file failed, %v", err)
	}
	defer f.Close()

	e := &emotion{
		EmotionName: "",
		Sentences: sentence{
			Positive: []string{},
			Negative: []string{},
		},
	}
	err = yaml.NewDecoder(f).Decode(e)
	if err != nil {
		return Emotion{}, fmt.Errorf("error: %v", err)
	}

	E := Emotion{
		Name:             e.EmotionName,
		PositiveSentence: e.Sentences.Positive,
		NegativeSentence: e.Sentences.Negative,
	}

	return E, err
}

// EmotionToYAML writes an Emotion struct to a YAML file
func EmotionToYAML(E Emotion, yamlFilePath string) error {
	e := &emotion{
		EmotionName: E.Name,
		Sentences: sentence{
			Positive: E.PositiveSentence,
			Negative: E.NegativeSentence,
		},
	}

	f, err := os.OpenFile(yamlFilePath, os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return fmt.Errorf("OS Open file failed, %v", err)
	}
	defer f.Close()

	err = yaml.NewEncoder(f).Encode(e)
	if err != nil {
		return fmt.Errorf("error: %v", err)
	}

	return nil
}

func caseInsensitiveSearch(s string, list []string) bool {
	for _, sCase := range list {
		if strings.EqualFold(s, sCase) {
			return true
		}
	}
	return false
}
