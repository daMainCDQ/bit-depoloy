package mlp

import (
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"strings"

	"gopkg.in/yaml.v2"
)

// CSVToClass is a convention function helper to read a example csv file to class struct.
// csv format header(optional):
//		sentence, tag
// Column 1: training text
// Column 2: tag, to tell which
func CSVToClass(csvFile, emotionName string) (ModelDataClass, error) {
	cf, err := os.Open(csvFile)
	if err != nil {
		return ModelDataClass{}, fmt.Errorf("OS Open file failed, %v", err)
	}
	defer cf.Close()
	reader := csv.NewReader(cf)
	PositiveSentence := make([]string, 0, 0)
	NegativeSentence := make([]string, 0, 0)

	var sentence, tag string
	var i int
	const BOM = "\uFEFF"
	for {
		row, err := reader.Read()
		if err == io.EOF {
			break
		}
		i++
		if err != nil {
			return ModelDataClass{}, fmt.Errorf("csv read error: %v", err)
		}

		if len(row) < 2 {
			return ModelDataClass{}, fmt.Errorf("parsing error: row %d is empty", i)
		}

		sentence = strings.TrimSpace(row[0])
		tag = strings.TrimSpace(row[1])

		//skip header line
		passibleHeaderOfSentenceCases := []string{"", "sentence", BOM + "sentence"}
		if caseInsensitiveSearch(sentence, passibleHeaderOfSentenceCases) {
			continue
		}

		postiveTagCases := []string{"正例", "positive"}
		negativeTagCases := []string{"反例", "negative"}

		if caseInsensitiveSearch(tag, postiveTagCases) {
			PositiveSentence = append(PositiveSentence, sentence)
		} else if caseInsensitiveSearch(tag, negativeTagCases) {
			NegativeSentence = append(NegativeSentence, sentence)
		} else {
			return ModelDataClass{}, fmt.Errorf("parsing error: wrong tag: %s in line %d", tag, i)
		}
	}
	return ModelDataClass{
		ClassName: emotionName,
		Corpus: Corpus{
			Positive: PositiveSentence,
			Negative: NegativeSentence,
		},
	}, nil
}

// YAMLToModelData reads a YAML emotion corpora file to an Emotion struct.
func YAMLToModelData(yamlFilePath string) (ModelDataClass, error) {
	f, err := os.Open(yamlFilePath)
	if err != nil {
		return ModelDataClass{}, fmt.Errorf("OS Open file failed, %v", err)
	}
	defer f.Close()

	e := &oldModelStruct{
		EmotionName: "",
		Sentences: Corpus{
			Positive: []string{},
			Negative: []string{},
		},
	}
	err = yaml.NewDecoder(f).Decode(e)
	if err != nil {
		return ModelDataClass{}, fmt.Errorf("error: %v", err)
	}

	E := ModelDataClass{
		ClassName: e.EmotionName,
		Corpus:    e.Sentences,
	}

	return E, err
}

func caseInsensitiveSearch(s string, list []string) bool {
	for _, sCase := range list {
		if strings.EqualFold(s, sCase) {
			return true
		}
	}
	return false
}
