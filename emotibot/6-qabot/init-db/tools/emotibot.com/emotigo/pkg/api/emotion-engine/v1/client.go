//Package emotionengine provide a api client to interact with emotion-engine module
package emotionengine

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"time"

	"emotibot.com/emotigo/pkg/logger"

	"emotibot.com/emotigo/pkg/api"
)

//Client is the api client to communicate with
type Client struct {
	// Transport specify how the HTTP Request are handled.
	// Which should be compatible with the net/http package.
	// If nil, http.DefaultClient is used.
	Transport api.HTTPClient
	// ServerURL is the host and port of the emotion-engine module.
	// which shouold be "http://{host}:{port}"
	ServerURL string
}

// Train will tell emotion-engine to create a model based on the model's AppID
// It will return modelID if successful. On error, modelID will be empty
// returned error can be http, emotion-engine, or other error.
// It is seperated by message prefix HTTP, EE, or OTHER.
func (c *Client) Train(apiModel Model) (modelID string, err error) {
	var transporter api.HTTPClient

	if c.Transport == nil {
		transporter = http.DefaultClient
	} else {
		transporter = c.Transport
	}

	m := model{
		AppID:      apiModel.AppID,
		AutoReload: apiModel.IsAutoReload,
		Data:       make(map[string]interface{}, 0),
	}

	emotions := make([]emotion, 0, len(apiModel.Data))
	for name, e := range apiModel.Data {
		if name != e.Name {
			return "", fmt.Errorf("EE: invalid model data, EmotionName '%s' should be the same with the key of it's Data map '%s'", e.Name, name)
		}
		if e.PositiveSentence == nil || len(e.PositiveSentence) == 0 {
			return "", fmt.Errorf("EE: invalid model data, Emotion %s's PositiveSentence should have at least one element", name)
		}
		var negativeSentence []string
		// Handling edge case. If we send null or negative sentence is not presented in json.
		// emotion-engine(last verified version: 58a9eb1) will return error. but the spec said it's optional.
		if e.NegativeSentence == nil {
			negativeSentence = []string{}
		} else {
			negativeSentence = e.NegativeSentence
		}
		emotions = append(emotions, emotion{
			EmotionName: e.Name,
			Sentences: sentence{
				Positive: e.PositiveSentence,
				Negative: negativeSentence,
			},
		})
	}
	m.Data["emotion"] = emotions

	data, err := json.Marshal(m)
	if err != nil {
		return "", fmt.Errorf("OTHER: json marshal failed, %v", err)
	}

	req, err := http.NewRequest(http.MethodPost, c.ServerURL+"/train", bytes.NewBuffer(data))
	if err != nil {
		return "", fmt.Errorf("OTHER: New Request failed, %v", err)
	}
	resp, err := transporter.Do(req)
	if err != nil {
		return "", fmt.Errorf("HTTP: %v", err)
	}
	defer resp.Body.Close()

	data, err = ioutil.ReadAll(resp.Body)
	var respBody trainResponse
	err = json.Unmarshal(data, &respBody)
	if err != nil {
		logger.Error.Println("raw output: ", string(data))
		return "", fmt.Errorf("EE: response format invalid, %v", err)
	}
	if respBody.Status != "OK" {
		return "", fmt.Errorf("EE: got unsuccessful status '%s', error message: %s", respBody.Status, respBody.ErrMsg)
	}
	return respBody.ModelID, nil
}

// Predict will sending request to emotion-engine to get the PredictResult from it.
// It will return a slice of Predicts in response of the request.Sentence.
// returned error can be http, emotion-engine, or other error.
// It is seperated by message prefix HTTP, EE, or OTHER.
func (c *Client) Predict(request PredictRequest) (predictions []Predict, err error) {
	if request.AppID == "" {
		return nil, fmt.Errorf("EE: predict appID should not be empty")
	}
	data, err := json.Marshal(request)
	req, err := http.NewRequest(http.MethodPost, c.ServerURL+"/predict", bytes.NewBuffer(data))
	resp, err := c.Transport.Do(req)
	if err != nil {
		return nil, fmt.Errorf("HTTP: Transport Do request failed, %v", err)
	}
	defer resp.Body.Close()
	data, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("HTTP: io read failed, %v", err)
	}
	var respBody predictResponse
	err = json.Unmarshal(data, &respBody)
	if err != nil {
		logger.Error.Println("raw output: ", string(data))
		return nil, fmt.Errorf("EE: predict response is not valid, %v", err)
	}

	if respBody.Status != "OK" {
		return nil, fmt.Errorf("EE: got unsuccessful status '%s', error message: %s", respBody.Status, respBody.Error)
	}

	var result = []Predict{}
	for _, p := range respBody.Predictions {
		result = append(result, Predict{
			Label: p.Label,
			Score: p.Score,
		})
	}
	return result, nil
}

// Wait the trained model ready
func (c *Client) Wait(appID, modelID string, timeout time.Duration) error {
	postData := []byte("{\"app_id\": \"" + appID + "\", \"model_id\": \"" + modelID + "\"}")

	startTime := time.Now()
	for {
		req, err := http.NewRequest(http.MethodPost, c.ServerURL+"/status", bytes.NewBuffer(postData))
		if err != nil {
			return fmt.Errorf("OTHER: New Request failed, %v", err)
		}

		resp, err := c.Transport.Do(req)
		if err != nil {
			return fmt.Errorf("HTTP: Transport Do request failed, %v", err)
		}
		defer resp.Body.Close()
		data, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return fmt.Errorf("HTTP: io read failed, %v", err)
		}

		res := make(map[string]interface{})
		err = json.Unmarshal(data, &res)
		if err != nil {
			return fmt.Errorf("Wait: unmarshal response failed, %v", err)
		}

		modelStatus, ok := res["status"].(string)
		if !ok {
			return fmt.Errorf("Wait: could not get the model status, %v", err)
		}

		if modelStatus == "ready" {
			break
		}

		if time.Since(startTime) > timeout {
			return fmt.Errorf("timeout to wait for model trained: %v", timeout)
		}

		// Wait for next round
		time.Sleep(time.Second * 1)
	}

	return nil
}

// LoadModel loads model for this APP ID
func (c *Client) LoadModel(appID, modelID string) error {
	postData := []byte("{\"app_id\": \"" + appID + "\", \"model_id\": \"" + modelID + "\"}")

	req, err := http.NewRequest(http.MethodPost, c.ServerURL+"/load_model", bytes.NewBuffer(postData))
	if err != nil {
		return fmt.Errorf("OTHER: New Request failed, %v", err)
	}

	resp, err := c.Transport.Do(req)
	if err != nil {
		return fmt.Errorf("HTTP: Transport Do request failed, %v", err)
	}
	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("HTTP: io read failed, %v", err)
	}

	res := make(map[string]interface{})
	err = json.Unmarshal(data, &res)
	if err != nil {
		return fmt.Errorf("Wait: unmarshal response failed, %v", err)
	}

	status, ok := res["status"].(string)
	if !ok {
		return fmt.Errorf("Wait: could not get the model status, %v", err)
	}

	if status != "OK" {
		return fmt.Errorf("error: load model failed, status: %s", status)
	}

	return nil
}

// UnloadModel loads model for this APP ID
func (c *Client) UnloadModel(appID, modelID string) error {
	postData := []byte("{\"app_id\": \"" + appID + "\", \"model_id\": \"" + modelID + "\"}")

	req, err := http.NewRequest(http.MethodPost, c.ServerURL+"/unload_model", bytes.NewBuffer(postData))
	if err != nil {
		return fmt.Errorf("OTHER: New Request failed, %v", err)
	}

	resp, err := c.Transport.Do(req)
	if err != nil {
		return fmt.Errorf("HTTP: Transport Do request failed, %v", err)
	}
	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("HTTP: io read failed, %v", err)
	}

	res := make(map[string]interface{})
	err = json.Unmarshal(data, &res)
	if err != nil {
		return fmt.Errorf("Wait: unmarshal response failed, %v", err)
	}

	status, ok := res["status"].(string)
	if !ok {
		return fmt.Errorf("Wait: could not get the model status, %v", err)
	}

	if status != "ok" {
		return fmt.Errorf("error: unload model failed")
	}

	return nil
}

type rate float32

func (r *rate) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf("%.2f", *r)), nil
}

// TotalTestResults stores all label test results
type TotalTestResults struct {
	// Between 0.0 to 1.0 (0% to 100%)
	TotalAccuracyRate rate                   `json:"total_accuracy_rate"`
	TestResults       map[string]*TestResult `json:"test_results"`
}

// TestResult stores test results for each label
type TestResult struct {
	// Between 0.0 to 1.0 (0% to 100%)
	AccuracyRate rate `json:"accuracy_rate"`
	TestedData   []struct {
		TestedResults []Predict `json:"tested_results"`
		Data          string    `json:"data"`
	} `json:"tested_data,omitempty"`
}

// NewTotalTestResults creates a new TotalTestResults
func NewTotalTestResults() *TotalTestResults {
	t := &TotalTestResults{}
	t.TestResults = make(map[string]*TestResult)

	return t
}

func (t TotalTestResults) String() string {
	str := ""
	for label, data := range t.TestResults {
		str += fmt.Sprintf("Label: %s,\tAccuracy Rate: %.2f%%\n", label, data.AccuracyRate*100)
	}
	return fmt.Sprintf("%sTotal Hit Rate: %.2f%%", str, t.TotalAccuracyRate*100)
}

// PrintDetails prints all test detail of TotalTestResults
func (t TotalTestResults) PrintDetails() {
	for label, testResults := range t.TestResults {
		for _, d := range testResults.TestedData {
			fmt.Printf("Label: %s, test result: %v, data: %s\n",
				label, d.TestedResults, d.Data)
		}
	}
	fmt.Println(t)
}

// Test the trained model with the training data
func (c *Client) Test(apiModel Model, sampleRate float32, detail bool) (*TotalTestResults, error) {
	totalCorporaSampleAmount := 0
	totalCorporaHitAmount := 0
	appID := apiModel.AppID
	totalTestResults := NewTotalTestResults()
	if sampleRate <= 0.0 || sampleRate >= 1.0 {
		return nil, fmt.Errorf("Test: sampleRate must be greater than 0 and less than 1")
	}
	for label, e := range apiModel.Data {
		sampleAmount := int(float32(len(e.PositiveSentence)) * sampleRate)
		sampleIndexes := rand.Perm(sampleAmount)
		sampleHitAmount := 0
		totalTestResults.TestResults[label] = &TestResult{
			AccuracyRate: 0.0,
			TestedData: make([]struct {
				TestedResults []Predict `json:"tested_results"`
				Data          string    `json:"data"`
			}, 0, 100),
		}
		totalCorporaSampleAmount += sampleAmount
		for i := 0; i < sampleAmount; i++ {
			p := PredictRequest{
				AppID:    appID,
				Sentence: e.PositiveSentence[sampleIndexes[i]],
			}
			results, err := c.Predict(p)
			if err != nil {
				return nil, fmt.Errorf("HTTP: predict failed, %v", err)
			}
			if detail {
				totalTestResults.TestResults[label].TestedData =
					append(totalTestResults.TestResults[label].TestedData, struct {
						TestedResults []Predict `json:"tested_results"`
						Data          string    `json:"data"`
					}{
						TestedResults: results,
						Data:          p.Sentence,
					})
			}
			for _, result := range results {
				if e.Name == result.Label {
					sampleHitAmount++
					break
				}
			}
		}
		totalCorporaHitAmount += sampleHitAmount
		totalTestResults.TestResults[label].AccuracyRate =
			rate(sampleHitAmount) / rate(sampleAmount)
	}

	totalTestResults.TotalAccuracyRate =
		rate(totalCorporaHitAmount) / rate(totalCorporaSampleAmount)

	return totalTestResults, nil
}

// Model contain config and data of a emotion model, which can be used to predict emotion.
// Model also is the input value for train api.
// Be remind, it is not an 1 to 1 mapping of original
type Model struct {
	// AppID will be the unique id of the model.
	// Any two equal AppID model will be override in sequence manner.
	AppID string
	//IsAutoReload is an indicator if the model need to loaded on trained.
	IsAutoReload bool
	//Data is used for training, and the key should be the same of Emotion's Name.
	//All Emotions should not have duplicated name.
	Data map[string]Emotion
}

//Emotion represent an emotion of the model. The name should always be unique in model.
type Emotion struct {
	//name is the key in the emotion model
	Name string
	//PositiveSentence is a required argument. it should never be nil.
	PositiveSentence []string
	//NegativeSentence is an optional argument. it can be nil or empty.
	NegativeSentence []string
}

//PredictRequest is the input of Client.Predict api.
type PredictRequest struct {
	AppID    string `json:"app_id"`
	Sentence string `json:"sentence"`
}

//Predict is the result of Client.Predict().
type Predict struct {
	// Label is the category tag for the input sentence, which should be the same with Emotion Name.
	Label string
	// Score is the confident score of the Label, from 0 to 100.
	// Threshold should be determine by the user.
	Score int
}
type predictResponse struct {
	Status      string       `json:"status"`
	Predictions []rawPredict `json:"predictions"`
	Error       string       `json:"error"`
}

const (
	errPredictNotLoad = "No model loaded."
	errPredictLoading = "Model is loading."
)

var (
	// ErrModelNotLoad is the predefined error from emotion-engine.
	// Which mean model has not issued to load yet. or mode is not found.
	ErrModelNotLoad = errors.New("predifined model not loaded message has been returned")
	// ErrModelLoading is the predfined error from emotion-engine.
	// Which mean model hasn't finish loading yet. please try again later.
	ErrModelLoading = errors.New("")
)

type rawPredict struct {
	Label     string        `json:"label"`
	Score     int           `json:"score"`
	OtherInfo []interface{} `json:"other_info"`
}

type model struct {
	AppID      string                 `json:"app_id"`
	AutoReload bool                   `json:"auto_reload"`
	Data       map[string]interface{} `json:"data"`
}

type emotion struct {
	EmotionName string   `json:"emotion_name" yaml:"emotion"`
	Sentences   sentence `json:"sentences" yaml:"corpora"`
}

type sentence struct {
	Positive []string `json:"positive" yaml:"positive"`
	Negative []string `json:"negative" yaml:"negative"`
}

type trainResponse struct {
	Status  string `json:"status"`
	ModelID string `json:"model_id"`
	ErrMsg  string `json:"error"`
}
