package main

import (
	"encoding/json"
	"flag"
	"log"
	"math/rand"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"time"

	emotionengine "emotibot.com/emotigo/pkg/api/emotion-engine/v1"
	mlp "emotibot.com/emotigo/pkg/api/mlp/v1"
)

var (
	folder          string
	clientAddress   string
	appID           string
	problemID       string
	modelID         string
	sampleRate      float64
	convert         bool
	test            bool
	testOnly        bool
	showTestDetails bool
	algorithm       string
	reTrain         bool
)

func main() {
	rand.Seed(time.Now().UnixNano())
	flag.StringVar(&folder, "f", "./", "folder to scan for YAML emotion corpora files (CSV for conversion only).")
	flag.StringVar(&clientAddress, "addr", "localhost:8888", "engine address(default: localhost:8888")
	flag.StringVar(&appID, "appId", "demo", "appID of the training subject")
	flag.StringVar(&problemID, "problemId", "demo", "problemID of the training subject")
	flag.StringVar(&algorithm, "algorithm", "emotion", "algorithm of engine")
	flag.BoolVar(&test, "test", false, "Test corpora after training")
	flag.BoolVar(&testOnly, "test-only", false, "Test only, no training")
	flag.Float64Var(&sampleRate, "test-sample-rate", 0.3, "Sample rate of testing data for test")
	flag.BoolVar(&showTestDetails, "show-test-details", false, "Show detail result of test")
	flag.BoolVar(&convert, "c", false, "convert lagecy CSV emotion corpora files to YAML files and exit")
	flag.BoolVar(&reTrain, "r", false, "re train")
	flag.Parse()
	folder = filepath.Clean(folder)
	var location http.FileSystem = http.Dir(folder)
	file, err := location.Open("/")
	if err != nil {
		log.Fatal("folder path "+folder+" error: ", err)
	}
	files, err := file.Readdir(0)
	if err != nil {
		log.Fatal("ReadDir error: ", err)
	}
	if len(files) == 0 {
		log.Fatal("dir is empty")
	}
	var model = mlp.Model{
		AppID:       appID,
		ProblemID:   problemID,
		ProblemName: "emotion",
		Data: mlp.ModelData{
			Classes: make([]mlp.ModelDataClass, 0),
		},
		Algorithm: algorithm,
	}
	for _, f := range files {
		filename := f.Name()
		absFilePath := folder + "/" + filename
		if convert {
			if path.Ext(filename) != ".csv" {
				continue
			}
			name := filename[:len(filename)-4]
			emotion, err := emotionengine.CSVToEmotion(absFilePath, name)
			if err != nil {
				log.Println("csv file: ", f.Name(), "can not import: ", err)
				os.Exit(1)
			}
			yamlFilePath := folder + "/" + name + ".yaml"
			emotionengine.EmotionToYAML(emotion, yamlFilePath)
			if err != nil {
				log.Println("emotion: ", f.Name(), "can not convert to YAML file: ", err)
				os.Exit(1)
			}
			log.Println("yaml file: successfully converted to", yamlFilePath)
		} else {
			if path.Ext(filename) != ".yaml" {
				continue
			}
			class, err := mlp.YAMLToModelData(absFilePath)
			if err != nil {
				log.Println("yaml file: ", f.Name(), "can not import: ", err)
				os.Exit(1)
			}
			model.Data.Classes = append(model.Data.Classes, class)
		}

	}
	if len(model.Data.Classes) == 0 {
		log.Println("no such emotion corpora files")
		os.Exit(1)
	}
	if convert {
		log.Println("successfully converted all csv files to yaml files.")
		os.Exit(0)
	}

	eClient := mlp.MlpClient{
		Transport: &http.Client{
			Timeout: 10 * time.Second,
		},
		ServerURL: "http://" + clientAddress,
	}

	newModelID := ""
	if !testOnly {
		// Train the model with corpora
		if !reTrain {
			// run test
			result, err := eClient.Test(model, float32(0.0), false)
			if err != nil {
				log.Fatal(err)
				return
			}
			log.Printf(result.String())
			if result.TotalAccuracyRate > 0.0 {
				log.Printf("skip train")
				return
			}
		}
		newModelID, err = eClient.Train(model)
		if err != nil {
			log.Fatal("train failed, ", err)
		}

		log.Println("trained send, model_id: ", newModelID, ", app_id: ", model.AppID)
		log.Println("check /status api for ready")
		log.Println("trained emotions:")
		for _, class := range model.Data.Classes {
			log.Println(class.ClassName)
		}
		modelID = newModelID
	}

	if test || testOnly {
		if modelID != "" {
			timeout := time.Second * 120
			log.Printf("Wait for model: %s to be ready, max wait time is %v...", modelID,
				timeout)
			err = eClient.Wait(appID, modelID, timeout)
			if err != nil {
				log.Fatal(err)
			}
		}
		log.Println("Start to test the trained model with the training data...")
		timeout := time.Second * 60
		startTime := time.Now()
		totalTestResults, err := eClient.Test(model, float32(sampleRate), showTestDetails)
		if err != nil {
			log.Fatal("test failed, ", err)
		}
		if time.Since(startTime) > timeout {
			log.Fatalf("timeout for model loading: %v", timeout)
		}
		time.Sleep(time.Second)

		totalTestResultsJSON, err := json.Marshal(totalTestResults)
		if err != nil {
			log.Fatal("marshal test result to json failed, ", err)
		}
		log.Println(string(totalTestResultsJSON))
	}
}

