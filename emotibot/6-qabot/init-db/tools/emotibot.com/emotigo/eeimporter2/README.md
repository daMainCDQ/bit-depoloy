# emotion-engine importer

It is a simple util tool to import csv file for the demo.
The app_id will always be 'demo', re-import will override the the older one. the emotion name will be the file name(ex: anger.csv -> emotion name: anger)

csv file must be UTF-8 encoded.

csv format header(optional): sentence, tag Content columns: Column 1: training text. Column 2: tag. Allowed tags are '正例', 'positive' for positive case, and '反例', 'negative' for negative case for this emotion.

e.g. happy.csv

```
sentence,tag
我喜欢,positive
真费劲,negative
太好了,正例
办事太差,反例
```

The app_id will always be 'demo'

csv format header(optional):
    sentence, tag
Content columns:
    Column 1: training text 
    Column 2: tag, to tell which

## Flags:

```
-h help
-addr string
    emotion-engine address(default: localhost:8888
-f string
    folder to scan for csv files. (default: ./)
    all .csv files will be imported
```

## How to use

```
# import the folder ./example to 172.16.100.32
./eeimporter -addr 172.16.100.32:8888 -f ./example
```