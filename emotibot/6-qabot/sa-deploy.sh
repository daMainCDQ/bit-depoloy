#!/bin/bash
set -e

# Standalone QABOT deployemnt for temporary use

WORKDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
ROOTDIR="$(cd "${WORKDIR}"/../../ && pwd)"
COLOR_RED=$'\e[1;31m'
COLOR_GREEN=$'\e[1;32m'
COLOR_END=$'\e[0m'
BF_LIST="license-controller admin-api common-resources-ui admin-ui load-balance permission-managment admin-auth common-help-docs emotion-engine emotion-engine-trainer emotion-sentence-embedding ml-platform-online ml-platform-offline model-scheduler"
INFRA_YAML_FILEPATH="${ROOTDIR}/infra/infra.yaml"
BF_ORIG_MODULE_YAML_FILEPATH="${ROOTDIR}/emotibot/1-bf/module.yaml"
BF_ORIG_PORT_YAML_FILEPATH="${ROOTDIR}/emotibot/1-bf/port.yaml"
QABOT_ORIG_MODULE_YAML_FILEPATH="${ROOTDIR}/emotibot/6-qabot/module.yaml"
QABOT_ORIG_PORT_YAML_FILEPATH="${ROOTDIR}/emotibot/6-qabot/port.yaml"
QABOT_MONITOR_YAML_FILEPATH="${ROOTDIR}/emotibot/6-qabot/monitor.yaml"
QABOT_OPTION_FILEPATH="${ROOTDIR}/emotibot/6-qabot/analysis-and-report"
QABOT_ANALYSIS_MODULE_YAML_OPT="-f ${QABOT_OPTION_FILEPATH}/module-analysis.yaml -f ${QABOT_OPTION_FILEPATH}/module-analysis-port.yaml"
QABOT_REPORT_MODULE_YAML_OPT="-f ${QABOT_OPTION_FILEPATH}/module-report.yaml -f ${QABOT_OPTION_FILEPATH}/module-report-port.yaml"
trap handler EXIT

function handler() {
  local rc="$?"
  if [[ "${rc}" != '0' ]]; then
    echo "error occurs" 1>&2
    if [[ -n "${ERR_MSG}" ]]; then
      echo "${ERR_MSG}" 1>&2
    fi
    # Force remove container 'es' if it exists
  fi

  # Remove the temp file
  if [[ -f "${TMP_FILE}" ]]; then
    rm -f "${TMP_FILE}"
  fi

  exit "${rc}"
}

function format_banner () {
  local length1="${#1}"
  let length1-=6
  local length2="${#2}"
  let length2-=42
  printf "[ -------- ${1}%${length1#-}s ${2}%${length2#-}s --------] \n"
}

function show_banner_options(){
  local explant options option
  format_banner " " 'Options'
  if [[ -x "${WORKDIR}/utils.sh" ]]; then
      options=( $(bash "${WORKDIR}/utils.sh" 'option') )
      for option in ${options[@]}
      do
        local explant="$(bash "${WORKDIR}/utils.sh" "${option}")"
        format_banner "${option}" "${explant}"
      done
  fi
}

function list_profiles(){
  echo "List existing profiles in '${WORKDIR}/profiles':"
  echo '-----------------------'
  bash -c "cd '${WORKDIR}/profiles' && find . -name '*.env' | sed -n 's/^.\/\(.*\)$/  \1/p' | sort"
  echo '-----------------------'
}

function source_profile_opts(){
  local profile_name="$1"
  local profile_base_path="${WORKDIR}/profiles"
  local profile_path="${profile_base_path}/${profile_name}"

  if [[ ! -f "${profile_path}" ]]; then
    profile_path="${profile_path}.env"
    if [[ ! -f "${profile_path}" ]]; then
      echo "error: profile '${profile_path}' does not exist" 1>&2
      return 1
    fi
  fi
  echo -e "${COLOR_GREEN}Deployment profile specified: '${profile_path}'${COLOR_END}" 1>&2

  cat ${profile_path} > ${WORKDIR}/.tmp.env
  while true; do
    profile_path="$(dirname "${profile_path}")"
    if [[ -f "${profile_path}.env" ]]; then
      # append ${profile_path}.env to head of .tmp.env 
      cat ${profile_path}.env > ${WORKDIR}/.tmp2.env
      cat ${WORKDIR}/.tmp.env >> ${WORKDIR}/.tmp2.env
      mv ${WORKDIR}/.tmp2.env ${WORKDIR}/.tmp.env
    elif [[ "${profile_path}" -ef "${profile_base_path}" ]]; then
      # Profile discovery completed
      break
    else
      echo "error: missing parent profile: '${profile_path}.env'" 1>&2
      return 1
    fi
  done
  set -o allexport
  source ${WORKDIR}/.tmp.env
  set +o allexport
}

function loadenv(){
  set -o allexport
  source "${ROOTDIR}"/dev.env
  source "${ROOTDIR}"/emotibot/1-bf/dev.env
  source "${ROOTDIR}"/emotibot/6-qabot/dev.env
  source "${ROOTDIR}"/emotibot/6-qabot/sa.env
  export METRIC_TYPE=STATSD
  set +o allexport
}

function force_rm_container_by_name(){
  local container_name container_id
  container_name="${1?error: force_rm_container_by_name() needs container_name}"

  container_id="$(docker ps -a --filter "name=\b${container_name}\b" -q)"
  if [[ -z "${container_id}" ]]; then
    return 0
  fi

  echo -n "Force remove the container: ${container_name} ... "
  if docker kill "${container_id}" &>/dev/null \
    || true \
    && docker rm -fv "${container_id}" 1>/dev/null; then
    echo -e "${COLOR_GREEN}done${COLOR_END}"
  else
    echo -e "${COLOR_RED}failed${COLOR_END}"
    return 1
  fi
}

function deploy(){
  local initdb_yaml_filepath \
    module_yaml_filepath deploy_option profile_name
  profile_name="$1"
  deploy_option="$2"
  # Skip the immature system check phase
  if [[ ! -f '/tmp/.run_check.spot' ]]; then
    touch '/tmp/.run_check.spot'
  fi

  echo '1'
  if [[ -z "${deploy_option}" ]]; then
    show_banner_options
    if ! read -t 90 -p $'Please select the deploy option, e.g. 6.4\n' \
      deploy_option; then
        echo "error: input timeout" 1>&2
        return 1
    fi
  fi

  deploy_opts=""
  if [[ "${deploy_option}" == '6.0' ]] \
    || ! echo "${deploy_option}" | grep -q '\b6\.[0-9]\+\b'; then
    echo "error: wrong option: ${deploy_option}" 1>&2
    return 1
  elif [[ "${deploy_option}" == '6.1' ]]; then
    deploy_opts="${QABOT_ANALYSIS_MODULE_YAML_OPT} ${QABOT_REPORT_MODULE_YAML_OPT}"
  elif [[ "${deploy_option}" == '6.2' ]]; then
    deploy_opts="${QABOT_ANALYSIS_MODULE_YAML_OPT}"
  elif [[ "${deploy_option}" == '6.3' ]]; then
    deploy_opts="${QABOT_REPORT_MODULE_YAML_OPT}"
  fi

  cd ${ROOTDIR}
  for i in `ls ${ROOTDIR}/script/*.sh` ; do source $i ; done
  echo "ZABBIX=no_deploy" >> "${ROOTDIR}"/dev.env
  echo "EFK_DEPLOY=false" >> "${ROOTDIR}"/dev.env
  set +e
  refresh_env
  deploy_befor
  upkeep_manager $@
  run_check
  run_db $@
  set -e
  head -n -2 "${ROOTDIR}"/dev.env > tmp-xxx.env && mv tmp-xxx.env "${ROOTDIR}"/dev.env
  echo "run db done"

  initdb_yaml_filepath="${ROOTDIR}"/infra/.init-db.yaml
  cp "${ROOTDIR}"/infra/init-db.yaml $initdb_yaml_filepath
  if [[ -f "${initdb_yaml_filepath}" ]]; then
    echo 'Init common DB schema...'
    sed -i 's/INIT_ES_INIT: "true"/INIT_ES_INIT: "false"/g' "${initdb_yaml_filepath}"
    sed -i 's/INIT_MINIO_INIT: "true"/INIT_MINIO_INIT: "false"/g' "${initdb_yaml_filepath}"
    sed -i 's/INIT_ES_INIT=true/INIT_ES_INIT=false/g' "${initdb_yaml_filepath}"
    sed -i 's/INIT_MINIO_INIT=true/INIT_MINIO_INIT=false/g' "${initdb_yaml_filepath}"
    docker-compose -f "${initdb_yaml_filepath}" run --rm 1-bf
    docker-compose -f "${initdb_yaml_filepath}" run --rm 6-qabot
  fi
  
  loadenv
  source_profile_opts "${profile_name}"
  echo 'Start container in 1-bf...'
  docker-compose \
    -f "${BF_ORIG_MODULE_YAML_FILEPATH}" \
    -f "${BF_ORIG_PORT_YAML_FILEPATH}" \
    up -d ${BF_LIST}
  echo 'Start container in 6-qabot...'
  docker-compose \
    -f "${QABOT_ORIG_MODULE_YAML_FILEPATH}" \
    -f "${QABOT_ORIG_PORT_YAML_FILEPATH}" \
    ${deploy_opts} \
    up -d
  echo 'Start container in 6-qabot monotor...'
  docker-compose \
    -f "${QABOT_MONITOR_YAML_FILEPATH}" \
    up -d

  bash "${ROOTDIR}"/emotibot/6-qabot/postjobs.sh
  echo 'Deploy QABOT succeed!'

}

function stop(){
  loadenv
  echo 'stop infra'
  docker-compose \
    -f "${INFRA_YAML_FILEPATH}" \
    rm -f -s
  echo 'stop container in 1-bf...'
  docker-compose \
    -f "${BF_ORIG_MODULE_YAML_FILEPATH}" \
    rm -f -s
  echo 'stop container in 6-qabot...'
  docker-compose \
    -f "${QABOT_ORIG_MODULE_YAML_FILEPATH}" \
    -f "${QABOT_ORIG_PORT_YAML_FILEPATH}" \
    ${QABOT_ANALYSIS_MODULE_YAML_OPT} \
    ${QABOT_REPORT_MODULE_YAML_OPT} \
    rm -f -s
  echo 'stop container in 6-qabot monitor'
  docker-compose \
    -f "${QABOT_MONITOR_YAML_FILEPATH}" \
    rm -f -a
}

function show_help() {
  local USAGE="Usage: ${0##*/} [command] [options...]

  Stop services but keep data
  e.g. ${0##*/} stop

  Set deployment profile
  e.g. ${0##*/} --profile tp/qabot-dev-1

Command:
     stop                               Stop services but keep data

Options:
     --help                             Show this message
     --profile PROFILE_NAME             Specify the deployment profile
                                        (e.g. --profile asr-ali/asr-172.16.100.58 or --profile asr-voice-team)
     --list-profiles                    List existing profiles in '${WORKDIR}/profiles'
     --option                           Set the deployment option (e.g. '6.2' for qabot with analysis)
"

  echo "${USAGE}"
}

function handler_missing_arugment(){
  echo "error: misssing arugment of option $1" 1>&2
}

function get_opts() {
  while :; do
    case "$1" in
      -h|-\?|--help)
        show_help
        exit 0
        ;;
      -p|--profile)
        if [[ -n "$2" ]]; then
          PROFILE_NAME="$2"
          shift
        else
          handler_missing_arugment '--profile'
          return 1
        fi
        ;;
      --list-profiles)
        LIST_PROFILES='true'
        ;;
      -o|--option)
        if [[ -n "$2" ]]; then
          DEPLOY_OPTION="$2"
          shift
        else
          handler_missing_arugment '--option'
          return 1
        fi
        ;;
      --)
        shift
        break
        ;;
      *)
        break
    esac
    shift
  done

  LIST_PROFILES="${LIST_PROFILES:-false}"
}

function main(){
  local cmd="$1"
  get_opts "$@"
  loadenv

  if ${LIST_PROFILES}; then
    list_profiles
    exit "$?"
  fi

  if [[ "${cmd}" == 'stop' ]]; then
    stop
  else
    if [[ -z "${PROFILE_NAME}" ]]; then
      echo -e "${COLOR_RED}error: no deployment profile specified, '--profile PROFILE_NAME' is requried${COLOR_END}" 1>&2
      list_profiles 1>&2
      return 1
    fi
    deploy "${PROFILE_NAME}" "${DEPLOY_OPTION}"
  fi
}
main "$@"
