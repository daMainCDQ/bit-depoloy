#!/bin/sh

if [[ "${INIT_DB_DEBUG}" == 'true' ]]; then
  set -x
fi

if [ "${INIT_MYSQL_INIT}" == "true" ] || [ "${INIT_MYSQL_INIT}" == "TRUE" ] ; then
    echo "# =================================================================="
    echo "# sql"
    echo "# =================================================================="
    cd /usr/bin/app/sql && ./up.sh development
    
    if [[ $? -ne 0 ]]; then
        echo "# Fail to migrate sql"
        exit -1
    fi

    
fi

echo "init migrate modules end"
