# Migration tool

希望能透過具有版本紀錄的 Migration tool 來控管開發環境上的 SQL Schema, 目前尚未支援其他的infra控管  
**任何已經整入develop branch 的 sql 腳本都不應該再被修改，避免已上線的環境衝突**
需要修改的情況應該是再提出一個修改腳本  

**請勿自行執行檔案夾內的.sql檔案，否則會造成資料損壞或遺失!!!**  
**請勿自行執行檔案夾內的.sql檔案，否則會造成資料損壞或遺失!!!**  
**請勿自行執行檔案夾內的.sql檔案，否則會造成資料損壞或遺失!!!**  

## Script & binary 說明

- **sql-migrate**  
需先安裝的binary執行檔，可直接從source-code編譯(`go get -v github.com/rubenv/sql-migrate/...`), docker 內已經pre-install。 
[Github Link](https://github.com/rubenv/sql-migrate/)
- **up.sh**  
啟動migration的腳本, 須先安裝 sql-migrate binary.
- **make.sh**  
動態產生migration yaml的腳本，須先安裝envsubst(包含在GNU gettext套件內， OSX直接安裝Golang的相容版本較快 [Github Link](https://github.com/a8m/envsubst))
- **mock-migrations**  
自定義的mock-migration執行檔, 由golang 編寫。因為有些環境是在還沒有migration的時候建置的，這些環境會無法直接執行 up.sh 。因此該執行檔會根據 input 的版本號檢查各個migration yaml是否有需要 mock
``` bash
    ./mock-migration {version}
        {version}: semantic version of your current install.
        Environment variable "MODULE_LEVEL" will determine the logging level. (0: log, other: no log)
```

## 規則

### 檔案命名
migration tool執行檔案會依照檔名排序來執行，並忽略任何小於已執行過的檔名  
目前的檔名規則為:  `<主版號>00<次版號>00<修訂號>00<序號>000_<說明>plain_text.sql`  

- *主、次、修訂號* 為該 migration 腳本的BFOP版本  
- *序號* 則為該版本應連續執行之腳本順序。**注意撰寫時其他人也有可能寫出相同序號者，請後來Merge者需要調整本身之序號**。  
- *說明*則是簡短說明該腳本執行之行為。  

範例: `010200001_init.sql`

### SQL腳本內容
腳本需包含兩必要部分，up 以及 down, up為升版時所執行腳本，down為還原腳本。
格式如下:

```sql
-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE DATABASE IF NOT EXISTS `Example` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP DATABASE `Example`;

```

**一個腳本只能有一個Atomic statement(DDL 語法或是 TRANSACTION)**，並且必需一定要有 **分號(;)**。這是為了避免腳本運行多個 statement 出錯時，migration-tool 會無法執行 rollback 也無法執行 up 的狀態。並且 **migration 尚不支援DELIMITER語法，目前只能先手動在第一次的時候匯入。**

跑完 migration 之後會在每一個執行的 database 內產生一個gorp_migrations

例外檔案:

1. 每一個db的第一個檔案允許多個操作，減少第一次init所需的檔案數(ROLLBACK 理論只要直接 DROP DATABASE就可以)
2. emotibot_addition_view_sp_trigger.sql 是 emotibot 的 SSM 模組所需要用到的 view, sp, trigger. 但是

## 如何新增一個database

請先確定您是否一定要新增database，是否可以在舊有database上做更新即可？
如果真的要新增一個database的話請依序完成以下步驟:

1. make.sh內的 DB_LIST陣列內 新增db name, 需與資料夾相同名稱
2. 在 sys 資料夾內新增一筆migration, 因為 migration 本身不負責 CREATE DATABASE
3. 新增database migrate 資料夾，並加上 migrate 的 sql 檔案

## 常見 SQL 注意事項

以下列出一些常見的SQL錯誤，會隨時間修訂。請務必在commit sql以前檢視。

1. Table 的Encoding 請勿使用utf8 而是使用utf8mb4
2. 目前migration file 不支援DELIMITER keyword, 所以 stored procedure & trigger 都尚未支援
3. 盡量不要直接使用 Tool 匯出的 SQL syntax 當作migration 腳本, 會有陷阱(ex: up 的時候不應該放入先 DROP 在 CREATE TABLE 的語法)
4. 不需要用use 語法