-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied


-- ----------------------------
-- Table structure for ability_base
-- ----------------------------
DROP TABLE IF EXISTS `ability_base`;
CREATE TABLE `ability_base` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL COMMENT '能力名称',
  `status` int(11) NOT NULL COMMENT '是否开启',
  `category` int(11) unsigned zerofill NOT NULL COMMENT '能力类型',
  `app_id` varchar(64) NOT NULL COMMENT '机器人ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- ----------------------------
-- Table structure for ability_config_json
-- ----------------------------
DROP TABLE IF EXISTS `ability_config_json`;
CREATE TABLE `ability_config_json` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `config` text NOT NULL COMMENT '能力配置json',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;


-- ----------------------------
-- Table structure for ability_extract
-- ----------------------------
DROP TABLE IF EXISTS `ability_extract`;
CREATE TABLE `ability_extract` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) NOT NULL,
  `category` int(11) unsigned zerofill NOT NULL COMMENT '能力类型',
  `count` int(11) unsigned zerofill NOT NULL COMMENT '限制数',
  `intent_name` varchar(64) NOT NULL COMMENT '意图名称',
  `parser_id` varchar(64) NOT NULL COMMENT '解析器ID',
  `classifier_id` varchar(64) NOT NULL COMMENT '分类器ID',
  `tag_code` varchar(64) NOT NULL COMMENT '客户标签类型',
  PRIMARY KEY (`id`),
  KEY `ability_extract_parent_id_index` (`parent_id`) USING BTREE COMMENT '能力ID索引'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for kg_review
-- ----------------------------
DROP TABLE IF EXISTS `kg_review`;
CREATE TABLE `kg_review` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `session_record` mediumtext COMMENT '用户会话',
  `recommend_answer` text COMMENT '推荐答案',
  `target` varchar(500) NOT NULL COMMENT '点评对象',
  `target_id` varchar(50) NOT NULL COMMENT '点评对象id',
  `comment` varchar(5000) DEFAULT NULL COMMENT '点评内容',
  `type` int(4) NOT NULL COMMENT '点评类型',
  `reviewer` varchar(50) DEFAULT NULL COMMENT '点评人',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '点评时间',
  `ct` bigint(14) NOT NULL COMMENT '点评时间时间戳',
  `status` int(4) NOT NULL DEFAULT '0' COMMENT '点评状态',
  `updated_time` datetime DEFAULT NULL COMMENT '维护时间',
  `operator` varchar(50) DEFAULT NULL COMMENT '维护人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='知识点评';


-- ----------------------------
-- Table structure for product_model_dict
-- ----------------------------
DROP TABLE IF EXISTS `product_model_dict`;
CREATE TABLE `product_model_dict` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `model` varchar(60) NOT NULL DEFAULT '' COMMENT '型号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `enterprise_config_json`;
