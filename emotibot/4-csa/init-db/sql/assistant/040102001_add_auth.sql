-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for enterprise
-- ----------------------------
CREATE TABLE IF NOT EXISTS `enterprise` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `uuid` char(32) NOT NULL COMMENT '企业ID',
  `name` varchar(64) NOT NULL COMMENT '企业名称',
  `status` tinyint(3) unsigned zerofill NOT NULL COMMENT '企业状态 0: disable, 1: active',
  `parent_id` int(11) NOT NULL COMMENT '父组织ID',
  `organization_id` int(11) NOT NULL COMMENT '组织ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `enterprise_uuid_index` (`uuid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for enterprise_scene
-- ----------------------------
CREATE TABLE IF NOT EXISTS `enterprise_scene` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `enterprise_id` char(32) NOT NULL COMMENT '企业ID',
  `scene_id` char(32) NOT NULL COMMENT '场景ID',
  `status` tinyint(5) unsigned zerofill NOT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `enterprise_scene_enterprise_id_index` (`enterprise_id`) USING BTREE COMMENT '企业ID索引'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for role
-- ----------------------------
CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL COMMENT '角色id',
  `name` varchar(64) NOT NULL COMMENT '角色名',
  `op_privileges` text NOT NULL COMMENT '操作权限',
  `csa_op_privileges` text NOT NULL COMMENT '助手操作权限',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色表';

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
CREATE TABLE IF NOT EXISTS `user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` char(32) NOT NULL COMMENT '用户ID',
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `enterprise_id` char(32) NOT NULL COMMENT '企业ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户角色关系';

-- ----------------------------
-- Table structure for user_roles
-- ----------------------------
CREATE TABLE IF NOT EXISTS `user_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `roles` text NOT NULL COMMENT '全部角色',
  `enterprise_id` char(32) NOT NULL COMMENT '企业ID',
  `user_name` char(64) NOT NULL DEFAULT '' COMMENT '用户登录名',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_roles_user_id_index` (`user_id`) USING BTREE,
  KEY `user_roles_enterprise_id_index` (`enterprise_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户全部角色';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `enterprise`;
DROP TABLE `enterprise_scene`;
DROP TABLE `role`;
DROP TABLE `user_role`;
DROP TABLE `user_roles`;
