-- +migrate Up

ALTER TABLE `user_roles` ADD `type` tinyint(5)  NOT NULL DEFAULT '2' COMMENT '用户类型';
ALTER TABLE `user_roles` ADD `status` tinyint(5)  NOT NULL DEFAULT '1' COMMENT '用户状态';

-- +migrate Down
