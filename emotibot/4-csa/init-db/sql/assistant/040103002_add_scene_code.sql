-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for enterprise_config_json
-- ----------------------------
ALTER TABLE `enterprise_config_json` ADD `code` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '企业编码';


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
