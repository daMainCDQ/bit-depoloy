-- +migrate Up

ALTER TABLE `role` ADD `enterprise_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '企业ID';

ALTER TABLE `role` ADD UNIQUE INDEX `role_enterprise_id_role_id_index` (`enterprise_id`,`role_id`) USING BTREE;

INSERT INTO role(`role_id`,`name`,`op_privileges`,`csa_op_privileges`,`enterprise_id`) SELECT * from (SELECT `role_id`,`name`,`op_privileges`,`csa_op_privileges` from `role` where `enterprise_id`='') a, (SELECT `uuid` from auth.enterprises where `level`=0) b where not exists(select 1 from `role` c where c.`enterprise_id`=b.`uuid` and c.`role_id`=a.`role_id` and c.`enterprise_id` != '');

CREATE TABLE IF NOT EXISTS `phone_enterprise` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `enterprise_id` char(32) NOT NULL COMMENT '企业ID',
  `phone_number` varchar(32) NOT NULL COMMENT '电话号',
  `status` tinyint(5) unsigned zerofill NOT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `enterprise_phone_enterprise_id_index` (`enterprise_id`) USING BTREE COMMENT '企业ID索引'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='电话企业对应表';

-- +migrate Down
