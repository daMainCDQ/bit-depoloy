-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE DATABASE IF NOT EXISTS `assistant` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

USE `assistant`;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for text_message
-- ----------------------------
CREATE TABLE IF NOT EXISTS `text_message` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `call_id` varchar(64) NOT NULL COMMENT '通话ID',
  `sentence_index` int(10) unsigned zerofill NOT NULL COMMENT '消息序号',
  `direction` smallint(5) unsigned zerofill NOT NULL COMMENT '消息角色',
  `message_time` varchar(20) NOT NULL DEFAULT '' COMMENT '消息时间',
  `text` varchar(4000) NOT NULL DEFAULT '' COMMENT '消息内容',
  PRIMARY KEY (`id`),
  KEY `text_message_call_id_index` (`call_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for text_message_history
-- ----------------------------
CREATE TABLE IF NOT EXISTS `text_message_history` (
  `id` bigint(20) NOT NULL,
  `call_id` varchar(64) NOT NULL COMMENT '通话ID',
  `sentence_index` int(10) unsigned zerofill NOT NULL COMMENT '消息序号',
  `direction` smallint(5) unsigned zerofill NOT NULL COMMENT '消息角色',
  `message_time` varchar(20) NOT NULL DEFAULT '' COMMENT '消息时间',
  `text` varchar(4000) NOT NULL DEFAULT '' COMMENT '消息内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for voice_message
-- ----------------------------
CREATE TABLE IF NOT EXISTS `voice_message` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `call_id` varchar(64) NOT NULL COMMENT '通话ID',
  `sentence_index` int(11) NOT NULL COMMENT '消息序号',
  `sentence_begin_time` int(11) unsigned zerofill NOT NULL COMMENT '转译消息开始时间',
  `trans_sentence_time` int(11) unsigned zerofill NOT NULL COMMENT '转译时间',
  `direction` smallint(6) NOT NULL COMMENT '消息角色',
  `text` varchar(4000) NOT NULL COMMENT '转译文本',
  PRIMARY KEY (`id`),
  KEY `voice_message_call_id_index` (`call_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for voice_message_history
-- ----------------------------
CREATE TABLE IF NOT EXISTS `voice_message_history` (
  `id` bigint(20) NOT NULL,
  `call_id` varchar(64) NOT NULL COMMENT '通话ID',
  `sentence_index` int(11) NOT NULL COMMENT '消息序号',
  `sentence_begin_time` int(11) unsigned zerofill NOT NULL COMMENT '转译消息开始时间',
  `trans_sentence_time` int(11) unsigned zerofill NOT NULL COMMENT '转译时间',
  `direction` smallint(6) NOT NULL COMMENT '消息角色',
  `text` varchar(4000) NOT NULL COMMENT '转译文本',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for call_info
-- ----------------------------
CREATE TABLE IF NOT EXISTS `call_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `call_id` varchar(64) NOT NULL COMMENT '通话ID',
  `agent_id` varchar(32) NOT NULL DEFAULT '' COMMENT '坐席ID',
  `agent_name` varchar(32) NOT NULL DEFAULT '' COMMENT '坐席姓名',
  `customer_id` varchar(32) NOT NULL DEFAULT '' COMMENT '客户ID',
  `customer_name` varchar(32) NOT NULL DEFAULT '' COMMENT '客户姓名',
  `flow_id` varchar(32) NOT NULL DEFAULT '' COMMENT '流程导航ID',
  `agent_phone` varchar(32) NOT NULL DEFAULT '' COMMENT '坐席电话号码',
  `customer_phone` varchar(32) NOT NULL DEFAULT '' COMMENT '用户电话号码',
  `message_status` smallint(5) unsigned zerofill NOT NULL COMMENT '实时转译消息归档状态',
  `history_status` smallint(5) unsigned zerofill NOT NULL COMMENT '历史归档状态',
  `enterprise_id` varchar(32) NOT NULL COMMENT '企业ID',
  `date_time` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '通话开始时间',
  `type` smallint(6) NOT NULL DEFAULT '2' COMMENT '通话类型，1 文本，2语音',
  `end_time` datetime DEFAULT NULL COMMENT '通话结束时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `call_info_call_id_index` (`call_id`) USING BTREE,
  KEY `call_info_date_time_index` (`date_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for enterprise_phone
-- ----------------------------
CREATE TABLE IF NOT EXISTS `enterprise_phone` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `phone_number` varchar(32) NOT NULL COMMENT '分机号',
  `status` int(11) NOT NULL COMMENT '通话状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for enterprise_config
-- ----------------------------
CREATE TABLE IF NOT EXISTS `enterprise_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bfop_id` varchar(32) NOT NULL DEFAULT '' COMMENT 'BFOPID',
  `bfop_ip` varchar(32) NOT NULL DEFAULT '' COMMENT 'BFOPIP',
  `account` varchar(32) NOT NULL DEFAULT '' COMMENT '账号',
  `password` varchar(32) NOT NULL DEFAULT '' COMMENT '密码',
  `password_md5` varchar(32) NOT NULL DEFAULT '' COMMENT '密码MD5',
  `knowledge_faq_appid` varchar(32) NOT NULL DEFAULT '' COMMENT '知识推荐ID',
  `knowledge_faq_name` varchar(32) NOT NULL DEFAULT '' COMMENT '知识推荐名称',
  `taboo_faq_appid` varchar(32) NOT NULL DEFAULT '' COMMENT '禁语ID',
  `taboo_faq_name` varchar(32) NOT NULL DEFAULT '' COMMENT '禁语名称',
  `graph_ip` varchar(32) NOT NULL DEFAULT '' COMMENT '图谱IP',
  `graph_appid` varchar(32) NOT NULL DEFAULT '' COMMENT '图谱ID',
  `te_appid` varchar(32) NOT NULL DEFAULT '' COMMENT '多轮ID',
  `te_name` varchar(32) NOT NULL DEFAULT '' COMMENT '多轮名称',
  `user_intent_appid` varchar(32) NOT NULL DEFAULT '' COMMENT '画像意图ID',
  `user_intent_name` varchar(32) NOT NULL DEFAULT '' COMMENT '画像意图名称',
  `user_faq_appid` varchar(32) NOT NULL DEFAULT '' COMMENT '画像FAQID',
  `user_faq_name` varchar(32) NOT NULL DEFAULT '' COMMENT '画像FAQ名称',
  `quality_ip` varchar(32) NOT NULL DEFAULT '' COMMENT '质检IP',
  `quality_id` varchar(32) NOT NULL DEFAULT '' COMMENT '质检名称',
  `enterprise_name` varchar(32) NOT NULL DEFAULT '' COMMENT '企业名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for warn_setting
-- ----------------------------
CREATE TABLE IF NOT EXISTS `warn_setting` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL DEFAULT '' COMMENT '类型',
  `speech_rate` int(11) unsigned zerofill NOT NULL COMMENT '语速设置',
  `speech_rate_tip` varchar(32) NOT NULL DEFAULT '' COMMENT '语速警示',
  `grab_tip` varchar(32) NOT NULL DEFAULT '' COMMENT '抢话警示',
  `customer_emotion_tip` varchar(32) NOT NULL DEFAULT '' COMMENT '客户情绪警示',
  `agent_emotion_tip` varchar(32) NOT NULL DEFAULT '' COMMENT '坐席情绪警示',
  `sensitive_tip` varchar(32) NOT NULL DEFAULT '' COMMENT '敏感词警示',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

SET FOREIGN_KEY_CHECKS = 1;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP DATABASE `assistant`;
