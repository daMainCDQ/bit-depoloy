-- +migrate Up

CREATE TABLE IF NOT EXISTS `seat_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `dic_key` varchar(32) NOT NULL DEFAULT '' COMMENT '小组代号',
  `dic_name` varchar(64) NOT NULL DEFAULT '' COMMENT '小组名称',
  `enterprise_id` char(32) NOT NULL COMMENT '企业ID',
  PRIMARY KEY (`id`),
  KEY `seat_group_enterprise_id_index` (`enterprise_id`) USING BTREE COMMENT '企业ID索引'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='坐席小组';

CREATE TABLE IF NOT EXISTS `user_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_code` varchar(32) NOT NULL COMMENT '用户编号',
  `cti_code` varchar(32) DEFAULT NULL COMMENT '通话和用户关联',
  `soft_phone` varchar(32) DEFAULT NULL COMMENT '坐席软电话号',
  `organ_code` varchar(32) DEFAULT NULL COMMENT '坐席归属机构编号',
  `organ_name` varchar(128) DEFAULT NULL COMMENT '坐席归属机构',
  `dic_key` varchar(32) DEFAULT NULL COMMENT '序列编号',
  `dic_name` varchar(64) DEFAULT NULL COMMENT '序列名称',
  `user_name` varchar(64) DEFAULT NULL COMMENT '用户名称',
  `user_status` varchar(2) DEFAULT NULL COMMENT '是否离职 Y-离职 N-未离职',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户详情';

-- +migrate Down
DROP TABLE `seat_group`;
DROP TABLE `user_detail`;
