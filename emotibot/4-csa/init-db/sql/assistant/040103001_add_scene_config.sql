-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for enterprise_config_json
-- ----------------------------
CREATE TABLE IF NOT EXISTS `enterprise_config_json` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `config` text NOT NULL COMMENT '场景配置json',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `enterprise_config_json`;
