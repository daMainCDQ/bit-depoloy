-- +migrate Up
ALTER TABLE `enterprise_config` ADD `vocabulary_id` VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'asr热词id';
ALTER TABLE `enterprise_config` ADD `customization_id` VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'asr模型id';
-- +migrate Down

