-- +migrate Up
INSERT INTO enterprise(`uuid`,`name`,`status`,`parent_id`,`organization_id`) (SELECT b.`uuid`,b.`name`,b.`status`,b.`parent_id`,b.`id` from auth.enterprises b where b.level=0 and not exists(select 1 from enterprise a where a.uuid=b.uuid));

alter table enterprise_scene modify column scene_id int NOT NULL DEFAULT 0 COMMENT '场景ID';

-- +migrate Down

