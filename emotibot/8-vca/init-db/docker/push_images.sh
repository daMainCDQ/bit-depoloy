#!/bin/bash

COLOR_REST='\033[0m'
COLOR_RED='\033[0;31m'

if [ -z $1 ]; then 
    echo -e "${COLOR_RED}input is empty, please enter image full name(ex: harbor.emotibot.com/project/test:123)${COLOR_REST}" >&2
    exit 1
fi

if [ -z "$(git status --porcelain)" ]; then 
    # Working directory clean
    docker push "$1" && docker rmi "$1";
else 
    # Uncommitted changes
    echo -e "${COLOR_RED}detect uncommitted changes, please commit before you build${COLOR_REST}"
    echo "$(git status --porcelain)"
    exit 1
fi