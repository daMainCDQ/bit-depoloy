-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
/*!40101 SET NAMES utf8mb4 */;
START TRANSACTION;

alter table t_tenants_configuration MODIFY COLUMN config_value text;

COMMIT;


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
