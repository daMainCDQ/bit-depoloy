-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
/*!40101 SET NAMES utf8mb4 */;
START TRANSACTION;

insert ignore into `im`.`t_channel`(`id`,`name`,`code`,`status`,`act`,`ctime`,`version`,`order`) values
('1','全渠道','ALL','1','add','2017-06-13 14:40:59',null,'1'),
('2','公众号','PUBLIC','1','add','2017-06-13 14:40:26',null,'2'),
('5','WEB','WEB','1','add','2017-06-13 14:40:59',null,'5'),
('6','第三方','OTHER','1','add','2017-06-13 14:40:59',null,'56');

insert ignore into `im`.`t_kf_menu`(`id`,`pid`,`name`,`menuKey`,`des`,`type`,`navicon`,`url`,`orderNum`,`supermange`,`mange`,`customer`,`orderkf`,`chat`,`manageMode`,`kfMode`,`level`,`status`,`ctime`) values
('1',null,'主页','main_frame',null,'url','iconav_1','/index','1','0','0','0','0','0','on',null,'0','0','2017-04-21 00:00:00'),
('10','14','角色设置','customer_list_role_config',null,'tab',null,'/kfOnlineService/roleconfigindex','3','1','1','0','0','0','on','on','1','1','2017-05-03 14:08:02'),
('104','63','app','insert_app',null,'tab',null,'/inmgr/load/app','5','1','1','0','0','0','on','on','1','1','2017-09-07 11:05:29'),
('105','13','基础报表','base_summary','基础报表','tab',null,'/report/base','1','1','1','0','0','0','on',null,'1','1','2017-09-14 15:00:00'),
('12',null,'留言','leave_msg',null,'url','iconav_11',null,'19','0','0','0','0','0',null,'on','0','0','2017-05-04 10:49:14'),
('125','14','会话通知','session_notice',null,'tab',null,'/setting/notice','3','1','0','0','0','0',null,null,'0','1','2017-10-20 13:38:24'),
('14',null,'设置','setting',null,'url','iconav_10','/setting/index','21','1','1','1','1','1','on','on','0','1','2017-05-04 10:50:06'),
('140','3','客户列表','union_customer_list',null,'tab',null,'/custom/cust','0','1','1','1','0','1','on','on','1','1','2017-12-11 17:47:03'),
('155','14','机器人设置','robot_setting',null,'tab',null,'/setting/robotsetting','2','1','1','0','0','0','on','on','1','1','2017-05-05 13:37:04'),
('2',null,'座席管理','customer_manager',null,'url','iconav_9','/kfOnlineService/index','17','1','1','0','0','0','on',null,'0','1','2017-04-21 00:00:00'),
('21',null,'业务技能','business_knowledge',null,'url',null,'/group/index','2','0','0','0','0','0',null,null,'0','0','2017-04-21 00:00:00'),
('22',null,'业务类型','business_type',null,'url',null,'/serviceType/index','5','0','0','0','0','0',null,null,'0','0','2017-04-21 00:00:00'),
('23',null,'座席等级','seat_level',null,'url',null,'/customerLevel','6','0','0','0','0','0',null,null,'0','0','2017-04-21 00:00:00'),
('24',null,'座席人员','seat_person',null,'url',null,'/kfOnlineService/index','8','0','0','0','0','0',null,null,'0','0','2017-04-21 00:00:00'),
('3',null,'客户管理','customer_list',null,'url','iconav_6','/custom/index','10','1','1','1','0','1','on','on','0','1','2017-04-21 00:00:00'),
('4',null,'历史会话','history_conversation',null,'url','iconav_5','/kfLineHis/main','11','1','1','1','0','1','on','on','0','1','2017-04-21 00:00:00'),
('5',null,'工单中心','order_center','管理端的工单中心与客服端的保持一致','url','iconav_4','/workorder/index','12','1','1','1','1','0','on','on','0','1','2017-04-21 00:00:00'),
('52','5','我受理的工单','order_center_myorder',null,'tab',null,'/workorder/myorder.html','1','1','1','1','1','0','on','on','1','1','2017-04-21 00:00:00'),
('53','5','已提交工单','order_center_create_dorder',null,'tab',null,'/workorder/createdorder.html','2','1','1','1','1','0','on','on','1','1','2017-05-03 14:36:28'),
('54','5','历史工单','order_center_history_dorder',null,'tab',null,'/workorder/historyorder.html','3','1','1','1','1','0','on','on','1','1','2017-05-03 14:37:19'),
('55','5','组内工单','order_center_group_order',null,'tab',null,'/workorder/grouporder.html','4','1','1','1','1','0','on','on','1','1','2017-05-03 14:37:53'),
('6',null,'质量检测','quality_check',null,'url','iconav_4',null,'18','0','0','0','0','0','on','on','0','1','2017-05-04 10:50:14'),
('60','14','基础设置','base_setting',null,'tab',null,'/setting/basesetting','1','1','1','1','1','1','on','on','1','1','2017-05-05 13:37:04'),
('61','81','快捷回复','quick_reply',null,'tab',null,'/setting/kfquickreply','4','1','1','0','0','0','on','on','1','1','2017-05-05 13:38:53'),
('62','5','工单分类','work_order',null,'tab',null,'/setting/ordertype','5','1','1','1','1','0','on','on','1','1','2017-05-05 13:40:18'),
('63',null,'接入管理','insert_manager',null,'url','iconav_7','/inmgr/index','3','1','1','0','0','0','on',null,'0','1','2017-05-16 16:59:24'),
('65','63','公众号','insert_public',null,'tab',null,'/inmgr/load/wm','2','1','1','0','0','0','on','on','1','1','2017-05-17 16:32:07'),
('67','63','WEB','insert_web',null,'tab',null,'/inmgr/load/web','4','1','1','0','0','0','on','on','1','1','2017-05-17 16:32:07'),
('7',null,'当前会话','current_conversation',null,'url','iconav_2','/onlineService/index','4','0','0','1','0','0',null,'on','0','1','2017-04-21 00:00:00'),
('74','3','WEB','customer_web_guest',null,'tab',null,'/custom/web/guest','7','1','1','1','0','1','on','on','1','1','2017-06-02 11:03:45'),
('8','2','坐席列表','customer_list_all',null,'tab',null,'/kfOnlineService/kfOnlineServiceIndex','1','1','1','0','0','0','on','on','1','1','2017-05-03 14:05:25'),
('86','4','历史会话','history_catalog',null,'tab',null,'/kfLineHis/index','1','1','1','1','0','0','on','on','1','1','2017-07-06 09:39:55'),
('9','2','坐席分组','customer_list_group',null,'tab',null,'/kfGroup/index','2','1','1','0','0','0','on','on','1','1','2017-05-03 14:06:45'),
('99','14','路由策略配置','tenants_config',null,'tab',null,'/setting/tenantsConfigIndex','4','1','1','0','0','0','on','on','1','1','2017-07-26 13:40:18'),
('1001','14','工作时间设置','workSchedule',null,'tab',null,'/setting/scheduleIndex',5,1,1,0,0,0,'on','on',1,1,now()),
('30',NULL,'监控','monitor',NULL,'url','iconav_1','/report/index',1,1,1,0,0,0,'on',NULL,0,1,'2019-05-07 00:00:00'),
('3001','30','实时监控','realtime_monitor',NULL,'tab',NULL,'/report/realtime',1,1,1,0,0,0,'on',NULL,1,1,'2019-05-07 00:00:00'),
('3002','30','数据报表','data_report',NULL,'url',NULL,'/report/data',2,1,1,0,0,0,'on',NULL,1,1,'2019-05-07 00:00:00'),
('11','14','坐席辅助','robotAssist',NULL,'tab',NULL,'/kfOnlineService/assist',4,1,1,0,0,0,'on','on',1,0,'2017-05-03 14:06:45'),
('zj_00',NULL,'质量检测','quality_check',NULL,'url','iconav_4','/quality/index',18,1,1,0,0,0,'on','on',0,1,now()),
('zj_00_01','zj_00','质量检查','quality_inspection',NULL,'tab',NULL,'/quality/insp',1,1,1,0,0,0,'on','on',1,1,now()),
('125', '14', '场景话术设置','session_notice',NULL, 'tab', NULL,'/setting/notice',3,1,0,0,0,0,'on','on',1,1,now());

COMMIT;


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

TRUNCATE TABLE `im`.`t_channel`;
TRUNCATE TABLE `im`.`t_kf_menu`;
