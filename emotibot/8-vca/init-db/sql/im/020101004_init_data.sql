-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
/*!40101 SET NAMES utf8mb4 */;
START TRANSACTION;

TRUNCATE TABLE im.t_admin_account;
TRUNCATE TABLE im.t_account_tenants_role_ref;
TRUNCATE TABLE im.t_admin_tenants;
TRUNCATE TABLE im.t_kf_role;
TRUNCATE TABLE im.t_admin_account_group_ref;


COMMIT;


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
