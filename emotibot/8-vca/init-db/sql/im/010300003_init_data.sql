-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
/*!40101 SET NAMES utf8mb4 */;
START TRANSACTION;

insert ignore into `im`.`t_kf_menu`(`id`,`pid`,`name`,`menuKey`,`des`,`type`,`navicon`,`url`,`orderNum`,`supermange`,`mange`,`customer`,`orderkf`,`chat`,`manageMode`,`kfMode`,`level`,`status`,`ctime`) values
('curr_conver_01','7','当前会话','sub_curr_conversation',NULL,'tab',NULL,'/onlineService/index',1,0,0,1,0,0,'on','on',1,1,'2019-08-13 15:56:22');

COMMIT;


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
