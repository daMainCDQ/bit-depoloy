-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
/*!40101 SET NAMES utf8mb4 */;
START TRANSACTION;

CREATE TABLE IF NOT EXISTS `im`.`t_kf_line_his_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `json_info` json comment 'json数据',
  `line_id` varchar(50),
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  INDEX  (`line_id`),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `im`.`t_kf_cust` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `json_info` json comment 'json数据',
  `user_id` varchar(50),
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  INDEX  (`user_id`),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `im`.`t_account_tenants_role_ref` (
  `id` varchar(50) NOT NULL DEFAULT '',
  `account_id` varchar(50) NOT NULL,
  `role_id` varchar(50) NOT NULL,
  `tenants_id` varchar(50) NOT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `deletestatus` int(11) DEFAULT '0' COMMENT '0:正常，1:已删除',
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `coper` varchar(50) DEFAULT NULL,
  `utime` datetime DEFAULT NULL,
  `uoper` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

alter table `im`.t_kf_role add column data_privileges text comment '数据权限';
alter table `im`.t_admin_tenants add column `parent_id` varchar(50);
alter table `im`.t_admin_tenants modify column `name` varchar(200);
alter table `im`.t_kf_group add column `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;


COMMIT;


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
