-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
/*!40101 SET NAMES utf8mb4 */;
START TRANSACTION;

alter table `im`.t_kf_role drop primary key;
alter table `im`.t_kf_role add column roleId int(11) not null PRIMARY KEY auto_increment comment '给BFOP的roleId';
alter table `im`.t_admin_account add column bf_user_id varchar(50);
alter table `im`.t_admin_account add column bf_uuid varchar(50);

DROP TABLE IF EXISTS `im`.`t_kf_menu`;
CREATE TABLE `im`.`t_kf_menu` (
  `id` varchar(50) NOT NULL,
  `pid` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `menuKey` varchar(50) DEFAULT NULL,
  `des` varchar(100) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL COMMENT 'tab:标签页 url:链接',
  `navicon` varchar(50) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `orderNum` int(11) DEFAULT NULL,
  `supermange` int(11) DEFAULT '0',
  `mange` int(11) DEFAULT '0',
  `customer` int(11) DEFAULT '0',
  `orderkf` int(11) DEFAULT '0',
  `chat` int(11) DEFAULT '0',
  `manageMode` varchar(10) DEFAULT NULL COMMENT '管理模式菜单开关 on:off',
  `kfMode` varchar(10) DEFAULT NULL COMMENT '客服模式菜单开关 on:off',
  `level` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `menu_key` varchar(255) DEFAULT NULL,
  `order_num` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='INSERT INTO `t_kf_menu` select max(cast(id as UNSIGNED INTEGER))+1,NULL,''测试测试测试'',''main_frame'',NULL,''url'',''iconav_1'',''/index'',1,''on'',NULL,0,1,now() from `t_kf_menu`;';


INSERT INTO `im`.`t_kf_menu` VALUES ('0',NULL,'服务监控','statistic',NULL,NULL,'icon-fuwujiankong1','null',0,1,1,0,0,0,NULL,NULL,0,1,'2020-04-15 18:57:58',NULL,NULL),('1','0','实时监控','real_time',NULL,NULL,'','real-time',0,1,1,0,0,0,NULL,NULL,1,1,'2020-04-15 18:57:58',NULL,NULL),('100',NULL,'坐席管理','seat',NULL,NULL,'icon-zuoxiguanli1','null',10,1,1,0,0,0,NULL,NULL,0,1,'2020-04-15 18:57:58',NULL,NULL),('101','100','坐席列表','seat_list',NULL,NULL,'','seat-list',0,1,1,0,0,0,NULL,NULL,1,1,'2020-04-15 18:57:58',NULL,NULL),('102','100','坐席分组','seat_group',NULL,NULL,'','seat-group',5,1,1,0,0,0,NULL,NULL,1,1,'2020-04-15 18:57:58',NULL,NULL),('150',NULL,'访客会话','dialog',NULL,NULL,'icon-multi-round-task','null',15,1,1,1,0,0,NULL,NULL,0,1,'2020-04-15 18:57:58',NULL,NULL),('151','150','访客会话','dialog_list',NULL,NULL,'','dialog',0,1,1,1,0,0,NULL,NULL,1,1,'2020-04-15 18:57:58',NULL,NULL),('2','0','数据报表','report',NULL,NULL,'','report',5,1,1,0,0,0,NULL,NULL,1,1,'2020-04-15 18:57:58',NULL,NULL),('200',NULL,'历史会话','history',NULL,NULL,'icon-lishihuihua','null',20,1,1,1,0,0,NULL,NULL,0,1,'2020-04-15 18:57:58',NULL,NULL),('201','200','历史会话','history_list',NULL,NULL,'','history',0,1,1,1,0,0,NULL,NULL,1,1,'2020-04-15 18:57:58',NULL,NULL),('250',NULL,'质量检测','quality',NULL,NULL,'icon-zhiliangjiance','null',25,1,1,0,0,0,NULL,NULL,0,1,'2020-04-15 18:57:58',NULL,NULL),('251','250','质量检查','quality_list',NULL,NULL,'','quality',0,1,1,0,0,0,NULL,NULL,1,1,'2020-04-15 18:57:58',NULL,NULL),('300',NULL,'系统设置','setting',NULL,NULL,'icon-robot-setting','null',30,1,1,0,0,0,NULL,NULL,0,1,'2020-04-15 18:57:58',NULL,NULL),('301','300','基础设置','base_setting',NULL,NULL,'','base-setting',0,1,1,0,0,0,NULL,NULL,1,1,'2020-04-15 18:57:58',NULL,NULL),('302','300','场景话术设置','word_setting',NULL,NULL,'','word-setting',5,1,1,0,0,0,NULL,NULL,1,1,'2020-04-15 18:57:58',NULL,NULL),('303','300','工作时间设置','time_setting',NULL,NULL,'','time-setting',10,1,1,0,0,0,NULL,NULL,1,1,'2020-04-15 18:57:58',NULL,NULL),('50',NULL,'接入管理','insert',NULL,NULL,'icon-jieruguanli','null',5,1,1,0,0,0,NULL,NULL,0,1,'2020-04-15 18:57:58',NULL,NULL),('51','50','接入列表','insert-list',NULL,NULL,'','insert',0,1,1,0,0,0,NULL,NULL,1,1,'2020-04-15 18:57:58',NULL,NULL);


COMMIT;


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
