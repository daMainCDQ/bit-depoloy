-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

CREATE TABLE `im`.`t_account_role_ref` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` varchar(50) NOT NULL,
  `role_id` int(11) NOT NULL,
  `deletestatus` int(11) DEFAULT '0' COMMENT '0:正常，1:已删除',
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `coper` varchar(50) DEFAULT NULL,
  `utime` datetime DEFAULT NULL,
  `uoper` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_admin_account` (
  `id` varchar(50) NOT NULL,
  `tenantsCode` varchar(100) DEFAULT NULL,
  `account` varchar(200) DEFAULT NULL,
  `pwd` varchar(200) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `nick_name` varchar(200) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `headimgurl` varchar(150) DEFAULT NULL,
  `kf_role_id` varchar(50) DEFAULT NULL,
  `max_count` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `creater_kf_online_service_id` varchar(50) DEFAULT NULL,
  `tenantsName` varchar(100) DEFAULT NULL,
  `seatCode` varchar(50) DEFAULT NULL COMMENT '座席工号 4位数字',
  sip_client_id varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_admin_account_chat_group_ref` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` varchar(50) DEFAULT NULL,
  `kf_chat_group_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_admin_account_group_ref` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `account_id` varchar(50) DEFAULT NULL,
  `kf_group_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1757 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_admin_reception_tag_ref` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` varchar(50) DEFAULT NULL,
  `tag_id` varchar(50) DEFAULT NULL,
  `tenantsCode` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_admin_tenants` (
  `id` varchar(50) NOT NULL,
  `tenantsCode` varchar(100) DEFAULT NULL,
  `tenantsName` varchar(100) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `nick_name` varchar(200) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_advisory_type` (
  `id` varchar(50) NOT NULL,
  `tenantsCode` varchar(100) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `pid` varchar(50) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_app_bind_account` (
  `id` varchar(50) NOT NULL,
  `tenantsCode` varchar(100) DEFAULT NULL,
  `appName` varchar(100) DEFAULT NULL,
  `appId` varchar(100) DEFAULT NULL,
  `robotSwitch` int(11) DEFAULT NULL COMMENT '1:开,0:关',
  `agentSwitch` int(11) DEFAULT NULL COMMENT '1:开,0:关',
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deletestatus` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_app_user` (
  `id` varchar(50) NOT NULL,
  `tenantsCode` varchar(100) DEFAULT NULL,
  `appId` varchar(50) DEFAULT NULL,
  `userId` varchar(50) DEFAULT NULL,
  `userName` varchar(200) DEFAULT NULL,
  `remark` varchar(200) DEFAULT NULL,
  `deletestatus` int(11) DEFAULT '0' COMMENT '0:正常，1:已删除',
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `coper` varchar(50) DEFAULT NULL,
  `utime` datetime DEFAULT NULL,
  `uoper` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_business_command` (
  `business_code` varchar(50) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`business_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_channel` (
  `id` varchar(50) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `code` varchar(20) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `act` varchar(20) DEFAULT NULL COMMENT 'add:增加 del:删除 update:变更',
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `version` varchar(20) DEFAULT NULL,
  `order` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_contact_msg_info` (
  `id` varchar(50) NOT NULL,
  `wxMsgType` int(11) DEFAULT NULL COMMENT '51:系统消息，1：文字消息，43：视频消息，3：图片消息，34：语音消息',
  `type` int(11) DEFAULT NULL COMMENT '1:发送，2：接收',
  `refwxaccountid` varchar(50) DEFAULT NULL,
  `refwxfriendid` varchar(50) DEFAULT NULL COMMENT '关联的消息的用户或者群的id',
  `content` varchar(10000) DEFAULT NULL,
  `sendstatus` int(11) DEFAULT NULL COMMENT '0：准备发送，1：发送中，2：发送成功，-1：发送失败',
  `deletestatus` int(11) DEFAULT '0' COMMENT '0:正常，1:已删除',
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `coper` varchar(50) DEFAULT NULL,
  `utime` datetime DEFAULT NULL,
  `uoper` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_customer` (
  `id` varchar(50) NOT NULL,
  `joinId` varchar(50) DEFAULT NULL COMMENT 'null：主客户记录， else：被合并的客户记录',
  `tenantsCode` varchar(100) DEFAULT NULL,
  `UserName` varchar(200) DEFAULT NULL,
  `NickName` varchar(3000) DEFAULT NULL,
  `HeadImgUrl` varchar(500) DEFAULT NULL,
  `Sex` int(11) DEFAULT NULL COMMENT '0：未定义，1：男，2：女',
  `Statues` int(11) DEFAULT NULL,
  `Province` varchar(20) DEFAULT NULL,
  `City` varchar(20) DEFAULT NULL,
  `Alias` varchar(200) DEFAULT NULL,
  `wxFriendId` varchar(50) DEFAULT NULL,
  `wxGroupMemberId` varchar(50) DEFAULT NULL,
  `publicCustId` varchar(50) DEFAULT NULL,
  `webCustId` varchar(50) DEFAULT NULL,
  `appUserId` varchar(50) DEFAULT NULL,
  `phoneUserId` varchar(50) DEFAULT NULL COMMENT '电话渠道客户ID',
  `deletestatus` int(11) DEFAULT '0' COMMENT '0:正常，1:已删除',
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `coper` varchar(50) DEFAULT NULL,
  `utime` datetime DEFAULT NULL,
  `uoper` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_customer_service_account` (
  `id` varchar(50) NOT NULL,
  `platformAccountId` varchar(50) DEFAULT NULL,
  `loginName` varchar(100) DEFAULT NULL,
  `loginPwd` varchar(100) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `jobNO` varchar(100) DEFAULT NULL,
  `limitCount` int(11) DEFAULT NULL,
  `serviceGroupId` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '0:下线; 1:在线; 2:接待中',
  `deletestatus` int(11) DEFAULT '0' COMMENT '0:正常，1:已删除',
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `coper` varchar(50) DEFAULT NULL,
  `utime` datetime DEFAULT NULL,
  `uoper` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_customer_service_group` (
  `id` varchar(50) NOT NULL,
  `platformAccountId` varchar(50) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `deletestatus` int(11) DEFAULT '0' COMMENT '0:正常，1:已删除',
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `coper` varchar(50) DEFAULT NULL,
  `utime` datetime DEFAULT NULL,
  `uoper` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_customer_term_ref` (
  `id` varchar(50) NOT NULL,
  `tenantsCode` varchar(100) DEFAULT NULL,
  `customerId` varchar(50) NOT NULL,
  `sourceType` varchar(20) NOT NULL DEFAULT '',
  `sourceId` varchar(200) DEFAULT NULL,
  `deletestatus` int(11) DEFAULT '0' COMMENT '0:正常，1:已删除',
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `customerId` (`customerId`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_group_msg_info` (
  `id` varchar(50) NOT NULL,
  `wxMsgType` int(11) DEFAULT NULL COMMENT '51:系统消息，1：文字消息，43：视频消息，3：图片消息，34：语音消息',
  `type` int(11) DEFAULT NULL COMMENT '1：发送，2：接收',
  `refwxaccountid` varchar(50) DEFAULT NULL,
  `refgroupid` varchar(50) DEFAULT NULL COMMENT '关联的消息的用户或者群的id',
  `refmemberid` varchar(50) DEFAULT NULL,
  `content` varchar(10000) DEFAULT NULL,
  `sendstatus` int(11) DEFAULT NULL,
  `deletestatus` int(11) DEFAULT '0' COMMENT '0:正常，1:已删除',
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `coper` varchar(50) DEFAULT NULL,
  `utime` datetime DEFAULT NULL,
  `uoper` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_kf_chat_group` (
  `id` varchar(50) NOT NULL,
  `tenantsCode` varchar(100) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_kf_group` (
  `id` varchar(50) NOT NULL,
  `tenantsCode` varchar(100) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_kf_group_channel_chat_ref` (
  `id` varchar(50) NOT NULL,
  `kf_group_id` varchar(50) DEFAULT NULL,
  `kf_channel_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_kf_group_channel_ref` (
  `id` varchar(50) NOT NULL,
  `group_id` varchar(50) DEFAULT NULL,
  `channel_id` varchar(50) DEFAULT NULL,
  `channel_type` varchar(50) DEFAULT NULL,
  `tenantsCode` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_kf_group_tag_ref` (
  `id` varchar(50) NOT NULL,
  `kf_group_id` varchar(50) DEFAULT NULL,
  `tag_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_kf_line_his` (
  `ID` varchar(50) NOT NULL,
  `tenantsCode` varchar(100) DEFAULT NULL,
  `advisory_type_id` varchar(50) DEFAULT NULL,
  `account_id` varchar(50) DEFAULT NULL,
  `customer_id` varchar(100) DEFAULT NULL,
  `transFromId` varchar(50) DEFAULT NULL,
  `start_time` varchar(20) DEFAULT NULL,
  `end_time` varchar(20) DEFAULT NULL,
  `reception_result` varchar(10) DEFAULT NULL,
  `satisfaction` varchar(100) DEFAULT NULL,
  `status` int(11) DEFAULT '1' COMMENT '1： 解决，0 未解决',
  `channel` varchar(20) DEFAULT NULL,
  `lineType` varchar(20) DEFAULT NULL,
  `remark` varchar(200) DEFAULT NULL,
  `voiceFile` varchar(200) DEFAULT NULL COMMENT '语音会话的文件地址',
  `kf_start_time` varchar(20) DEFAULT NULL,
  `extendInof` text,
  `uin` varchar(30) DEFAULT NULL,
  `isFreeChat` int(11) DEFAULT NULL,
  `extend_sessionId` varchar(100) DEFAULT NULL,
  `extend_name` varchar(200) DEFAULT NULL,
  `extend_headImg` varchar(300) DEFAULT NULL,
  `extend_refType` varchar(10) DEFAULT NULL,
  `extend_sourceTerminal` varchar(100) DEFAULT NULL,
  `isToTop` int(11) DEFAULT NULL,
  `toTopTime` varchar(20) DEFAULT NULL,
  `line_count` int(11) DEFAULT '0' COMMENT '会话轮次',
  `res_time` varchar(20) DEFAULT '' COMMENT '座席首次响应时间',
  `session_id` varchar(50) DEFAULT '' COMMENT '',
  `score` int(11) COMMENT '质检评分',
  `insp_remark` varchar(200) DEFAULT '' COMMENT '质检备注',
  `staff_line_count` varchar(200) DEFAULT '' COMMENT '坐席会话数',
  `close_type` varchar(200) DEFAULT '' COMMENT '会话关闭类型',
  `last_res` varchar(200) DEFAULT '' COMMENT '最后接待方',
  `queue_time` varchar(200) DEFAULT '' COMMENT '',
  `avg_res_time` int(11)  COMMENT '座席平均响应时间,单位 毫秒',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_kf_line_summary` (
  `ID` varchar(50) NOT NULL,
  `start_time` timestamp NULL DEFAULT NULL COMMENT '统计会话开始时间',
  `tenantsCode` varchar(100) DEFAULT NULL,
  `total` bigint(20) DEFAULT '0' COMMENT '内总会话数',
  `robot` bigint(20) DEFAULT '0' COMMENT '内机器人会话数',
  `kf` bigint(20) DEFAULT '0' COMMENT '人工会话数',
  `robot_solved` bigint(20) DEFAULT '0' COMMENT '机器人解决问题数',
  `kf_solved` bigint(20) DEFAULT '0' COMMENT '人工解决问题数',
  `unreco_ques` bigint(20) DEFAULT '0' COMMENT '未识别问题数量',
  `robot_talks` bigint(20) DEFAULT NULL COMMENT '机器人回复数',
  `total_talks` bigint(20) DEFAULT '0' COMMENT '总会话轮次',
  `total_time_long` bigint(20) DEFAULT '0' COMMENT '累计会话时长(秒)',
  `ctime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `utime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_kf_line_top_ques_summary` (
  `ID` varchar(50) NOT NULL,
  `time_field` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '统计开始时间',
  `tenantsCode` varchar(100) DEFAULT NULL,
  `content` mediumblob,
  `intent` varchar(200) DEFAULT NULL COMMENT '客户问题识别的意图',
  `intent_code` varchar(50) DEFAULT NULL COMMENT '意图编码',
  `total` int(11) DEFAULT '0' COMMENT '统计客户问题数量',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_kf_menu` (
  `id` varchar(50) NOT NULL,
  `pid` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `menuKey` varchar(50) DEFAULT NULL,
  `des` varchar(100) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL COMMENT 'tab:标签页 url:链接',
  `navicon` varchar(10) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `orderNum` int(11) DEFAULT NULL,
  `supermange` int(11) DEFAULT '0',
  `mange` int(11) DEFAULT '0',
  `customer` int(11) DEFAULT '0',
  `orderkf` int(11) DEFAULT '0',
  `chat` int(11) DEFAULT '0',
  `manageMode` varchar(10) DEFAULT NULL COMMENT '管理模式菜单开关 on:off',
  `kfMode` varchar(10) DEFAULT NULL COMMENT '客服模式菜单开关 on:off',
  `level` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='INSERT INTO `t_kf_menu` select max(cast(id as UNSIGNED INTEGER))+1,NULL,''测试测试测试'',''main_frame'',NULL,''url'',''iconav_1'',''/index'',1,''on'',NULL,0,1,now() from `t_kf_menu`;';

CREATE TABLE `im`.`t_kf_quick_reply` (
  `ID` varchar(50) NOT NULL,
  `tenantsCode` varchar(100) DEFAULT NULL,
  `keyword` varchar(100) DEFAULT NULL,
  `answer` varchar(500) DEFAULT NULL,
  `account_id` varchar(50) DEFAULT NULL,
  `reply_type_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_kf_role` (
  `id` varchar(50) NOT NULL,
  `tenantsCode` varchar(100) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL COMMENT 'superadmin:超级管理员 manager:管理员 order:工单客服 customer:普通客服',
  `des` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_kf_role_menu_ref` (
  `id` varchar(50) NOT NULL DEFAULT '0',
  `menuId` varchar(50) DEFAULT NULL,
  `roleId` varchar(50) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL COMMENT 'on : off',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_kf_satisfaction_content` (
  `id` varchar(50) NOT NULL,
  `tenantsCode` varchar(100) DEFAULT NULL,
  `satisfaction_model_id` varchar(50) DEFAULT NULL,
  `num` int(11) DEFAULT NULL,
  `content` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_kf_satisfaction_invitation` (
  `id` varchar(50) NOT NULL,
  `tenantsCode` varchar(100) DEFAULT NULL,
  `content` varchar(200) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1:启用 0:停用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_kf_satisfaction_model` (
  `id` varchar(50) NOT NULL,
  `tenantsCode` varchar(100) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1:开启 一个租户下只能有一个模式',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_kf_seat_code` (
  `id` varchar(100) NOT NULL DEFAULT '',
  `tenantsCode` varchar(100) DEFAULT NULL,
  `seatCode` varchar(100) DEFAULT NULL,
  `ctime` timestamp NULL DEFAULT NULL,
  `kfGroupId` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_kf_service_type` (
  `id` varchar(50) NOT NULL,
  `tenantsCode` varchar(100) DEFAULT NULL,
  `kf_group_id` varchar(50) DEFAULT NULL,
  `service_id` varchar(50) DEFAULT NULL,
  `service_name` varchar(100) DEFAULT NULL,
  `service_des` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_kf_tag` (
  `id` varchar(50) NOT NULL,
  `tenantsCode` varchar(100) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `orderNum` int(11) DEFAULT NULL,
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_material` (
  `id` varchar(50) NOT NULL,
  `refPlatformAccountId` varchar(50) DEFAULT NULL,
  `materialType` int(11) DEFAULT NULL COMMENT '1.文本，2.图片，3.语言，4.视频',
  `title` varchar(200) DEFAULT NULL,
  `content` longtext,
  `deletestatus` int(11) DEFAULT '0' COMMENT '0:正常，1:已删除',
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `coper` varchar(50) DEFAULT NULL,
  `utime` datetime DEFAULT NULL,
  `uoper` varchar(50) DEFAULT NULL,
  `tenantsCode` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_notice_config` (
  `id` varchar(50) NOT NULL,
  `tenantsCode` varchar(100) NOT NULL,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `value` varchar(500) DEFAULT NULL,
  `default_value` varchar(500) DEFAULT NULL,
  `enable_edit` smallint(6) DEFAULT '1' COMMENT '是否可编辑 0 开启 、1 关闭',
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT '0开启 ，1关闭',
  `note` varchar(500) DEFAULT NULL,
  `params` varchar(500) DEFAULT NULL COMMENT '["content","name"]',
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `utime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_optional_param_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tenantsCode` varchar(100) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL COMMENT 'time:时间类型参数，trie-tree:树类型参数',
  `attr` varchar(50) DEFAULT NULL COMMENT 'trie-tree类型使用，标记使用哪个trie-tree',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_qrcode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tenantsCode` varchar(50) DEFAULT NULL,
  `accountId` varchar(50) DEFAULT NULL,
  `uuid` varchar(50) DEFAULT NULL,
  `qrCodePath` varchar(200) DEFAULT NULL,
  `uin` varchar(32) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1.初始状态，2.已扫描，3.已登录，4.成功获取微信账号信息（绑定成功），5.同步好友及常用群组完成,-1.绑定失败（超时）,-2.重复绑定',
  `deletestatus` int(11) DEFAULT '0' COMMENT '0:正常，1:已删除',
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `coper` varchar(50) DEFAULT NULL,
  `utime` datetime DEFAULT NULL,
  `uoper` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2409 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_quick_reply_type` (
  `id` varchar(50) NOT NULL,
  `tenantsCode` varchar(100) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL COMMENT 'public:公众库  ; personal:个人库',
  `account_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_role_menu_ref` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `deletestatus` int(11) DEFAULT '0' COMMENT '0:正常，1:已删除',
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `coper` varchar(50) DEFAULT NULL,
  `utime` datetime DEFAULT NULL,
  `uoper` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_sms_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mobile` varchar(20) DEFAULT NULL,
  `code` varchar(20) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '0，登记，1，发送成功',
  `deletestatus` int(11) DEFAULT '0' COMMENT '0:正常，1:已删除',
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `coper` varchar(50) DEFAULT NULL,
  `utime` datetime DEFAULT NULL,
  `uoper` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_split_detail` (
  `id` varchar(50) NOT NULL,
  `import_id` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `split_path` varchar(200) DEFAULT NULL,
  `accountId` varchar(50) DEFAULT NULL,
  `confirm_tme` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `ctime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_synonym_all` (
  `id` varchar(50) NOT NULL,
  `vocable` varchar(50) DEFAULT NULL,
  `pos` varchar(20) DEFAULT NULL COMMENT 'part of speech',
  `synonym` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `act` varchar(20) DEFAULT NULL COMMENT 'add:增加 del:删除 update:变更',
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `utime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `actorName` varchar(20) DEFAULT NULL,
  `actorId` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_synonym_all_tenants` (
  `id` varchar(50) NOT NULL,
  `tenantsCode` varchar(100) DEFAULT NULL,
  `vocable` varchar(50) DEFAULT NULL,
  `pos` varchar(20) DEFAULT NULL COMMENT 'part of speech',
  `synonym` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `act` varchar(20) DEFAULT NULL COMMENT 'add:增加 del:删除 update:变更',
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `utime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `actorName` varchar(20) DEFAULT NULL,
  `actorId` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_sys_role_menu_ref` (
  `id` varchar(50) NOT NULL,
  `roleId` varchar(50) DEFAULT NULL,
  `t_s_id` varchar(50) DEFAULT NULL,
  `status` varchar(50) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_system_account` (
  `id` varchar(50) NOT NULL,
  `account` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `nickname` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `AK_Key_account` (`account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_tenants_configuration` (
  `id` varchar(50) NOT NULL,
  `tenantsCode` varchar(100) NOT NULL,
  `config_key` varchar(100) DEFAULT NULL COMMENT 'enable:开关类型，value:值类型，字符串',
  `config_value` text DEFAULT NULL,
  `msg` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_tenants_current_version` (
  `id` varchar(50) NOT NULL,
  `tenantsCode` varchar(100) DEFAULT NULL,
  `version` varchar(100) DEFAULT NULL,
  `utime` varchar(20) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_txt_upload` (
  `id` varchar(50) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `initial_path` varchar(200) DEFAULT NULL,
  `accountId` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `ctime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleteStatus` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_unmatch_question_record` (
  `id` varchar(50) NOT NULL,
  `tenantsCode` varchar(100) NOT NULL,
  `question` varchar(2000) DEFAULT NULL,
  `channelType` varchar(20) DEFAULT NULL,
  `terminalName` varchar(100) DEFAULT NULL,
  `sourceType` varchar(20) DEFAULT NULL,
  `userName` varchar(50) DEFAULT NULL,
  `lineId` varchar(50) DEFAULT NULL,
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) DEFAULT '1' COMMENT '1:正常 0:删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_upload_file` (
  `id` varchar(50) NOT NULL,
  `hashCode` varchar(64) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `type` int(11) DEFAULT NULL COMMENT '1.jpg，2.amr，3.mp4',
  `deletestatus` int(11) DEFAULT '0' COMMENT '0:正常，1:已删除',
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `coper` varchar(50) DEFAULT NULL,
  `utime` datetime DEFAULT NULL,
  `uoper` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `hashCode` (`hashCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_version_sync_record` (
  `id` varchar(50) NOT NULL,
  `tenantsCode` varchar(100) DEFAULT NULL,
  `version` varchar(100) DEFAULT NULL,
  `sync_type` varchar(10) DEFAULT NULL,
  `actorName` varchar(20) DEFAULT NULL,
  `actorId` varchar(50) DEFAULT NULL,
  `sync_status` varchar(20) DEFAULT '1',
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_web_reception_domain` (
  `id` varchar(50) NOT NULL,
  `tenantsCode` varchar(100) DEFAULT NULL,
  `receptionDomain` varchar(1024) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `style` varchar(5000) DEFAULT NULL COMMENT 'json格式数据',
  `ctime` varchar(19) DEFAULT NULL,
  `deletestatus` int(11) DEFAULT '0',
  `robotSwitch` int(11) DEFAULT NULL,
  `agentSwitch` int(11) DEFAULT NULL,
  `robot_id` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_wm_api_authorizer_token` (
  `Id` varchar(50) NOT NULL DEFAULT '',
  `appid` varchar(50) DEFAULT NULL,
  `authorizer_access_token` text,
  `expires_in` int(11) DEFAULT NULL,
  `authorizer_refresh_token` text,
  `errcode` int(11) DEFAULT NULL,
  `errmsg` varchar(250) DEFAULT NULL,
  `ctime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `utime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_wm_api_component_token` (
  `Id` varchar(50) NOT NULL DEFAULT '',
  `component_access_token` text,
  `expires_in` int(11) DEFAULT NULL,
  `errcode` int(11) DEFAULT NULL,
  `errmsg` varchar(250) DEFAULT NULL,
  `ctime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `utime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_wm_authorizaer_option` (
  `Id` varchar(50) NOT NULL DEFAULT '',
  `authorizer_appid` varchar(50) DEFAULT NULL,
  `option_name` varchar(50) DEFAULT NULL,
  `option_value` varchar(50) DEFAULT NULL,
  `errcode` int(11) DEFAULT NULL,
  `errmsg` varchar(250) DEFAULT NULL,
  `ctime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `utime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_wm_authorization_info` (
  `Id` varchar(50) NOT NULL DEFAULT '',
  `appid` varchar(50) DEFAULT NULL,
  `tenantsCode` varchar(50) DEFAULT NULL,
  `authorizer_appid` varchar(50) DEFAULT NULL,
  `pre_auth_id` varchar(50) DEFAULT NULL,
  `target_app_id` varchar(100) DEFAULT NULL,
  `component_access_token` text,
  `auth_code` varchar(150) DEFAULT NULL,
  `cat_expires_in` varchar(150) DEFAULT NULL,
  `authorizer_access_token` text,
  `expires_in` int(11) DEFAULT NULL,
  `authorizer_refresh_token` text,
  `func_info` text,
  `deletestatus` int(11) DEFAULT '0',
  `errcode` int(11) DEFAULT NULL,
  `errmsg` varchar(250) DEFAULT NULL,
  `ctime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `utime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_wm_authorizer_info` (
  `Id` varchar(50) NOT NULL DEFAULT '',
  `appid` varchar(50) DEFAULT NULL,
  `tenantsCode` varchar(50) DEFAULT NULL,
  `authorizer_appid` varchar(50) DEFAULT NULL,
  `target_app_id` varchar(100) DEFAULT NULL,
  `nick_name` varchar(50) DEFAULT NULL,
  `head_img` varchar(150) DEFAULT NULL,
  `service_type_info` varchar(50) DEFAULT NULL,
  `verify_type_info` varchar(50) DEFAULT NULL,
  `user_name` varchar(50) DEFAULT NULL,
  `principal_name` varchar(100) DEFAULT NULL,
  `signature` varchar(100) DEFAULT NULL,
  `business_info` varchar(150) DEFAULT NULL,
  `idc` int(11) DEFAULT NULL,
  `alias` varchar(50) DEFAULT NULL,
  `qrcode_url` varchar(150) DEFAULT NULL,
  `robotSwitch` int(11) DEFAULT '0',
  `agentSwitch` int(11) DEFAULT '0',
  `deletestatus` int(11) DEFAULT '0',
  `errcode` int(11) DEFAULT NULL,
  `errmsg` varchar(250) DEFAULT NULL,
  `ctime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `utime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `next_openid` varchar(100) DEFAULT NULL COMMENT 'null/next_openid',
  `loadMemberStatus` int(11) DEFAULT '0' COMMENT '0 未完成 其他 已完成',
  `routeSwitch` varchar(20),
  `robotUrl` varchar(300),
  `robotAppid` varchar(150),
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_wm_msg_info` (
  `id` varchar(50) NOT NULL,
  `wxMsgType` int(11) DEFAULT NULL COMMENT '51:系统消息，1：文字消息，43：视频消息，3：图片消息，34：语音消息',
  `type` int(11) DEFAULT NULL COMMENT '1:发送，2：接收',
  `app_id` varchar(50) DEFAULT NULL,
  `target_app_id` varchar(100) DEFAULT NULL,
  `msg_from` varchar(50) DEFAULT NULL,
  `msg_to` varchar(50) DEFAULT NULL,
  `content` text,
  `sendstatus` int(11) DEFAULT NULL COMMENT '0：准备发送，1：发送中，2：发送成功，-1：发送失败',
  `deletestatus` int(11) DEFAULT '0' COMMENT '0:正常，1:已删除',
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `coper` varchar(50) DEFAULT NULL,
  `utime` datetime DEFAULT NULL,
  `uoper` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_wm_openid_info` (
  `Id` varchar(50) NOT NULL DEFAULT '',
  `appid` varchar(50) DEFAULT NULL,
  `authorizer_appid` varchar(50) DEFAULT NULL,
  `subscribe` int(11) DEFAULT NULL,
  `openid` varchar(100) DEFAULT NULL,
  `nickname` varchar(100) DEFAULT NULL,
  `sex` int(11) DEFAULT NULL,
  `language` varchar(20) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `province` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `headimgurl` text,
  `subscribe_time` int(11) DEFAULT NULL,
  `unionid` varchar(50) DEFAULT NULL,
  `remark` text,
  `groupid` int(11) DEFAULT NULL,
  `tagidList` varchar(100) DEFAULT NULL,
  `infoFlag` int(11) DEFAULT '0' COMMENT '0 新纪录没有更新信息， 其他已经更新',
  `errcode` int(11) DEFAULT NULL,
  `errmsg` varchar(250) DEFAULT NULL,
  `ctime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `utime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_wm_pre_auth_code` (
  `Id` varchar(50) NOT NULL DEFAULT '',
  `target_app_id` varchar(100) DEFAULT NULL,
  `pre_auth_code` varchar(200) DEFAULT NULL,
  `expires_in` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `errcode` int(11) DEFAULT NULL,
  `errmsg` varchar(250) DEFAULT NULL,
  `ctime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `utime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tenantsCode` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_wm_sys_cfg` (
  `Id` varchar(50) NOT NULL DEFAULT '',
  `cfgKey` varchar(50) DEFAULT NULL,
  `cfgValue` text,
  `extraValue` varchar(150) DEFAULT NULL,
  `expires_in` int(11) DEFAULT NULL,
  `authorizer_refresh_token` varchar(200) DEFAULT NULL,
  `errcode` int(11) DEFAULT NULL,
  `errmsg` varchar(50) DEFAULT NULL,
  `ctime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `utime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_word_statistic` (
  `intent` varchar(100) NOT NULL,
  `word` varchar(100) NOT NULL,
  `times` int(11) NOT NULL,
  PRIMARY KEY (`intent`,`word`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_word_statistic_new` (
  `tenantsCode` varchar(50) NOT NULL,
  `intent` varchar(100) NOT NULL,
  `word` varchar(100) NOT NULL,
  `times` int(11) NOT NULL,
  PRIMARY KEY (`tenantsCode`,`intent`,`word`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_work_order` (
  `id` varchar(50) NOT NULL,
  `order_no` varchar(100) NOT NULL,
  `tenantsCode` varchar(100) DEFAULT NULL,
  `account_id` varchar(50) DEFAULT NULL COMMENT '创建工单的客服id',
  `title` varchar(200) DEFAULT NULL,
  `content` text,
  `priority` int(11) DEFAULT NULL,
  `accept_type` varchar(20) DEFAULT NULL,
  `kf_group_id` varchar(50) DEFAULT NULL,
  `accept_account_id` varchar(50) DEFAULT NULL,
  `work_type_id` varchar(50) DEFAULT NULL,
  `chat_id` varchar(50) DEFAULT NULL COMMENT '会话id',
  `name` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(10) DEFAULT NULL COMMENT 'init:未受理 accept:受理中 close:完结',
  `cur_remark_id` varchar(50) DEFAULT NULL,
  `utime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_work_order_attachment` (
  `id` varchar(50) NOT NULL,
  `tenantsCode` varchar(100) DEFAULT NULL,
  `work_order_id` varchar(50) DEFAULT NULL,
  `filename` varchar(100) DEFAULT NULL,
  `filetype` varchar(20) DEFAULT NULL,
  `att_url` varchar(500) DEFAULT NULL,
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_work_order_remark` (
  `id` varchar(50) NOT NULL,
  `tenantsCode` varchar(100) DEFAULT NULL,
  `account_id` varchar(50) DEFAULT NULL,
  `work_order_id` varchar(50) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL COMMENT 'init: 新流转单 accepted:流转组工单被接单 reply:回复 forward:转交工单',
  `accept_type` varchar(20) DEFAULT NULL,
  `kf_group_id` varchar(50) DEFAULT NULL,
  `accept_account_id` varchar(50) DEFAULT NULL,
  `content` text,
  `recored_num` int(11) DEFAULT NULL,
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `utime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_work_order_remark_attachment` (
  `id` varchar(50) NOT NULL,
  `tenantsCode` varchar(100) DEFAULT NULL,
  `work_order_remark_id` varchar(50) DEFAULT NULL,
  `filename` varchar(100) DEFAULT NULL,
  `filetype` varchar(20) DEFAULT NULL,
  `att_url` varchar(500) DEFAULT NULL,
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_work_order_type` (
  `id` varchar(50) NOT NULL,
  `tenantsCode` varchar(100) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `order_type_id` varchar(50) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_kf_work_schedule_head` (
  `id` varchar(50) NOT NULL ,
  `tenantsCode` varchar(100) NULL,
  `keyType` int(11) NULL,
  `status` int(11) DEFAULT '1' COMMENT '0:关闭，1:开启',
  `ctime` timestamp DEFAULT CURRENT_TIMESTAMP,
  `utime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_kf_work_schedule` (
  `id` varchar(50) NOT NULL ,
  `tenantsCode` varchar(100) NULL,
  `keyType` int(11) NULL,
  `startTime` varchar(20) NULL,
  `stopTime` varchar(20) NULL,
  `orderNum` int(11) NULL,
  `status` int(11) DEFAULT '1' COMMENT '0:删除，1:正常',
  `ctime` timestamp DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_kf_work_schedule_special` (
  `id` varchar(50) NOT NULL ,
  `tenantsCode` varchar(100) NULL,
  `startDate` varchar(20) NULL,
  `stopDate` varchar(20) NULL,
  `startTime` varchar(20) NULL,
  `stopTime` varchar(20) NULL,
  `orderNum` int(11) NULL,
  `status` int(11) DEFAULT '1' COMMENT '0:删除，1:正常',
  `ctime` timestamp DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_kf_max_reception` (
  `id` varchar(50) NOT NULL ,
  `tenantsCode` varchar(100) NULL,
  `maxCount` int(11) NULL,
  `status` int(11) DEFAULT '1' COMMENT '0:关闭，1:开启',
  `ctime` timestamp DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `im`.`t_im_report_data` (
  `id` varchar(50) NOT NULL,
  `tenantsCode` varchar(50) DEFAULT NULL,
  `text` text,
  `type` varchar(50) DEFAULT NULL,
  `deletestatus` int(11) DEFAULT '0' COMMENT '0:正常，1:已删除',
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `coper` varchar(50) DEFAULT NULL,
  `utime` datetime DEFAULT NULL,
  `uoper` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `im`.`t_account_role_ref`;
DROP TABLE `im`.`t_admin_account`;
DROP TABLE `im`.`t_admin_account_chat_group_ref`;
DROP TABLE `im`.`t_admin_account_group_ref`;
DROP TABLE `im`.`t_admin_reception_tag_ref`;
DROP TABLE `im`.`t_admin_tenants`;
DROP TABLE `im`.`t_advisory_type`;
DROP TABLE `im`.`t_app_bind_account`;
DROP TABLE `im`.`t_app_user`;
DROP TABLE `im`.`t_business_command`;
DROP TABLE `im`.`t_channel`;
DROP TABLE `im`.`t_contact_msg_info`;
DROP TABLE `im`.`t_customer`;
DROP TABLE `im`.`t_customer_service_account`;
DROP TABLE `im`.`t_customer_service_group`;
DROP TABLE `im`.`t_customer_term_ref`;
DROP TABLE `im`.`t_group_msg_info`;
DROP TABLE `im`.`t_kf_chat_group`;
DROP TABLE `im`.`t_kf_group`;
DROP TABLE `im`.`t_kf_group_channel_chat_ref`;
DROP TABLE `im`.`t_kf_group_channel_ref`;
DROP TABLE `im`.`t_kf_group_tag_ref`;
DROP TABLE `im`.`t_kf_line_his`;
DROP TABLE `im`.`t_kf_line_summary`;
DROP TABLE `im`.`t_kf_line_top_ques_summary`;
DROP TABLE `im`.`t_kf_menu`;
DROP TABLE `im`.`t_kf_quick_reply`;
DROP TABLE `im`.`t_kf_role`;
DROP TABLE `im`.`t_kf_role_menu_ref`;
DROP TABLE `im`.`t_kf_satisfaction_content`;
DROP TABLE `im`.`t_kf_satisfaction_invitation`;
DROP TABLE `im`.`t_kf_satisfaction_model`;
DROP TABLE `im`.`t_kf_seat_code`;
DROP TABLE `im`.`t_kf_service_type`;
DROP TABLE `im`.`t_kf_tag`;
DROP TABLE `im`.`t_material`;
DROP TABLE `im`.`t_notice_config`;
DROP TABLE `im`.`t_optional_param_config`;
DROP TABLE `im`.`t_qrcode`;
DROP TABLE `im`.`t_quick_reply_type`;
DROP TABLE `im`.`t_role_menu_ref`;
DROP TABLE `im`.`t_sms_code`;
DROP TABLE `im`.`t_split_detail`;
DROP TABLE `im`.`t_synonym_all`;
DROP TABLE `im`.`t_synonym_all_tenants`;
DROP TABLE `im`.`t_sys_role_menu_ref`;
DROP TABLE `im`.`t_system_account`;
DROP TABLE `im`.`t_tenants_configuration`;
DROP TABLE `im`.`t_tenants_current_version`;
DROP TABLE `im`.`t_txt_upload`;
DROP TABLE `im`.`t_unmatch_question_record`;
DROP TABLE `im`.`t_upload_file`;
DROP TABLE `im`.`t_version_sync_record`;
DROP TABLE `im`.`t_web_reception_domain`;
DROP TABLE `im`.`t_wm_api_authorizer_token`;
DROP TABLE `im`.`t_wm_api_component_token`;
DROP TABLE `im`.`t_wm_authorizaer_option`;
DROP TABLE `im`.`t_wm_authorization_info`;
DROP TABLE `im`.`t_wm_authorizer_info`;
DROP TABLE `im`.`t_wm_msg_info`;
DROP TABLE `im`.`t_wm_openid_info`;
DROP TABLE `im`.`t_wm_pre_auth_code`;
DROP TABLE `im`.`t_wm_sys_cfg`;
DROP TABLE `im`.`t_word_statistic`;
DROP TABLE `im`.`t_word_statistic_new`;
DROP TABLE `im`.`t_work_order`;
DROP TABLE `im`.`t_work_order_attachment`;
DROP TABLE `im`.`t_work_order_remark`;
DROP TABLE `im`.`t_work_order_remark_attachment`;
DROP TABLE `im`.`t_work_order_type`;
DROP TABLE `im`.`t_wx_label`;
DROP TABLE `im`.`t_kf_work_schedule_head`;
DROP TABLE `im`.`t_kf_work_schedule`;
DROP TABLE `im`.`t_kf_work_schedule_special`;
DROP TABLE `im`.`t_kf_max_reception`;
DROP TABLE `im`.`t_im_report_data`;