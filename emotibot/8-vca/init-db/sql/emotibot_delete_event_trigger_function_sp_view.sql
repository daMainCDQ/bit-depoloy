#DELETE FROM mysql.event WHERE db = 'emotibot';
USE emotibot;
DROP FUNCTION IF EXISTS FUNC_GET_APPID_IN_REQUEST_INFO;

DROP FUNCTION IF EXISTS FUNC_GET_CURR_HOUR;
DROP FUNCTION IF EXISTS FUNC_GET_LAST_HOUR;
DROP FUNCTION IF EXISTS FUNC_GET_VALIDATOR_MESSAGE;
DROP FUNCTION IF EXISTS func_app_id_has_child;
DROP FUNCTION IF EXISTS func_get_appid;
DROP FUNCTION IF EXISTS func_get_appname_by_appid;
DROP FUNCTION IF EXISTS func_get_config;
DROP FUNCTION IF EXISTS func_get_config_version;
DROP FUNCTION IF EXISTS func_get_parent_id;
DROP FUNCTION IF EXISTS func_get_username_by_userid;
DROP FUNCTION IF EXISTS func_has_new_config_version;
DROP FUNCTION IF EXISTS func_has_same_nickname;


DROP TRIGGER IF EXISTS ent_config_AFTER_INSERT;
DROP TRIGGER IF EXISTS ent_config_AFTER_UPDATE;
DROP TRIGGER IF EXISTS ent_config_AFTER_DELETE;
DROP TRIGGER IF EXISTS ent_config_appid_customization_AFTER_INSERT;
DROP TRIGGER IF EXISTS ent_config_appid_customization_AFTER_UPDATE;
DROP TRIGGER IF EXISTS ent_config_appid_customization_AFTER_DELETE;
DROP TRIGGER IF EXISTS ent_config_user_customization_AFTER_INSERT;
DROP TRIGGER IF EXISTS ent_config_user_customization_AFTER_UPDATE;
DROP TRIGGER IF EXISTS ent_config_user_customization_AFTER_DELETE;
DROP TRIGGER IF EXISTS ent_ml_test_case_BEFORE_DELETE;


DROP PROCEDURE IF EXISTS sp_app_id_addnode;
DROP PROCEDURE IF EXISTS sp_app_id_get_parents;
DROP PROCEDURE IF EXISTS sp_chat_log_archive;
DROP PROCEDURE IF EXISTS sp_chat_log_collection_last_day;
DROP PROCEDURE IF EXISTS sp_chat_log_collection_last_hour;
DROP PROCEDURE IF EXISTS sp_chat_log_collection_recent_all;
DROP PROCEDURE IF EXISTS sp_chat_log_collection_sq_hot_recent_all;
DROP PROCEDURE IF EXISTS sp_chat_log_save_label;
DROP PROCEDURE IF EXISTS sp_chat_optimization_save_label;
DROP PROCEDURE IF EXISTS sp_data_archive;
DROP PROCEDURE IF EXISTS sp_data_archive;
DROP PROCEDURE IF EXISTS sp_find_auth;

DROP PROCEDURE IF EXISTS sp_get_proccessor;
DROP PROCEDURE IF EXISTS sp_get_robot_stage_info;
DROP PROCEDURE IF EXISTS sp_manual_trigger;
DROP PROCEDURE IF EXISTS sp_request_log_archive_last_day;
DROP PROCEDURE IF EXISTS sp_save_chat_history;
DROP PROCEDURE IF EXISTS sp_save_chat_history_withoutput;


DROP PROCEDURE IF EXISTS sp_save_tag_withtime;
DROP PROCEDURE IF EXISTS sp_save_upload_corpus_history;
DROP PROCEDURE IF EXISTS sp_set_chatlog_ignore_status;
DROP PROCEDURE IF EXISTS sp_set_chat_log_ignore_status;
DROP PROCEDURE IF EXISTS sp_set_proccessor;
DROP PROCEDURE IF EXISTS sp_set_stage;
DROP PROCEDURE IF EXISTS sp_set_stage_status;
DROP PROCEDURE IF EXISTS sp_update_stage;



DROP TABLE IF EXISTS emotibot.view_activate_app;
DROP TABLE IF EXISTS emotibot.view_activate_appid;
DROP TABLE IF EXISTS emotibot.view_appid_tree;
DROP TABLE IF EXISTS emotibot.view_chat_log_collect_last_hour;
DROP TABLE IF EXISTS emotibot.view_chat_log_collect_last_hour_by_non_solutions;
DROP TABLE IF EXISTS emotibot.view_chat_log_collect_last_hour_by_nonsq;
DROP TABLE IF EXISTS emotibot.view_chat_log_collect_last_hour_by_solutions;
DROP TABLE IF EXISTS emotibot.view_chat_log_collect_last_hour_by_sq;
DROP TABLE IF EXISTS emotibot.view_chat_log_collect_recent_all;
DROP TABLE IF EXISTS emotibot.view_chat_log_collect_recent_all_by_nonsq;
DROP TABLE IF EXISTS emotibot.view_chat_log_collect_recent_all_by_sq;
DROP TABLE IF EXISTS emotibot.view_chat_log_history;
DROP TABLE IF EXISTS emotibot.view_chat_log_maunal_clustering_group;
DROP TABLE IF EXISTS emotibot.view_chat_log_maunal_clustering_group_content;
DROP TABLE IF EXISTS emotibot.view_chat_log_sq_hot_recent_all;
DROP TABLE IF EXISTS emotibot.view_chat_log_statistic_chat_count_per_hour;
DROP TABLE IF EXISTS emotibot.view_chat_log_statistic_sq_hot_2past_day;
DROP TABLE IF EXISTS emotibot.view_chat_log_statistic_sq_hot_per_day;
DROP TABLE IF EXISTS emotibot.view_chat_log_statistic_sq_hot_yesterday;
DROP TABLE IF EXISTS emotibot.view_chat_log_statistic_sq_trend;
DROP TABLE IF EXISTS emotibot.view_chat_optimization_report;
DROP TABLE IF EXISTS emotibot.view_chat_optimization_report_tag_group;
DROP TABLE IF EXISTS emotibot.view_request_exception;
DROP TABLE IF EXISTS emotibot.view_request_for_operater_log_level1;
DROP TABLE IF EXISTS emotibot.view_request_for_operater_log_level2;
DROP TABLE IF EXISTS emotibot.view_request_for_operater_log_level3;
DROP TABLE IF EXISTS emotibot.view_request_info;
DROP TABLE IF EXISTS emotibot.view_robot_solutions;
