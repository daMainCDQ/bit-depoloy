#!/bin/bash

if [ "${INIT_MYSQL_INIT}" == "true" ] || [ "${INIT_MYSQL_INIT}" == "TRUE" ] ; then
    echo "# =================================================================="
    echo "# sql"
    echo "# =================================================================="
    cd /usr/bin/app/sql && ./up.sh development

    if [[ $? -ne 0 ]]; then
        echo "# Fail to migrate sql"
        exit -1
    fi

    cd /usr/bin/app/sql && ./emotibot_view_sp.sh
fi



