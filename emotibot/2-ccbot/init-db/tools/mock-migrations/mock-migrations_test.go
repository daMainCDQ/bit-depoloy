package main

import (
	"os"
	"path/filepath"
	"testing"

	"gopkg.in/DATA-DOG/go-sqlmock.v1"
)

func TestMain(t *testing.T) {
	getwd = func() string {
		pwd, _ := os.Getwd()
		return filepath.Join(pwd, "../../")
	}
	os.Args = []string{"", "1.1.0"}
	os.Setenv("MODULE_LEVEL", "0")
	main()

}

func TestMockDB(t *testing.T) {
	db, writer, _ := sqlmock.New()
	exampleRows := []string{"010200001_init_schema.sql", "010200002_init_data.sql"}
	writer.ExpectExec("CREATE TABLE").WillReturnResult(sqlmock.NewResult(0, 1))
	writer.ExpectBegin()
	stmt := writer.ExpectPrepare("INSERT IGNORE")
	for i := range exampleRows {
		stmt.ExpectExec().WillReturnResult(sqlmock.NewResult(int64(i), 1))
	}
	writer.ExpectCommit()

	err := MockDB(db, exampleRows)
	if err != nil {
		t.Fatal(err)
	}
	if err = writer.ExpectationsWereMet(); err != nil {
		t.Fatal(err)
	}

}
