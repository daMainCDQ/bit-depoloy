package data

import (
	"errors"
)

var (
	ErrMissingEnv             = errors.New("Env missing")
	ErrESNotAcknowledged      = errors.New("Elasticsearch: Not acknowledged")
)
