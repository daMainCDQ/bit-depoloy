package elasticsearch

import (
	"context"
	"fmt"
	"io/ioutil"
	"os"

	"emotibot.com/solr-es-migration/data"
	"emotibot.com/solr-es-migration/logger"

	"github.com/olivere/elastic"
)

var esClient *elastic.Client
var esCtx context.Context

func Setup() error {
	logger.Info.Println("Initializing Elasticsearch...")

	err := initESClient()
	if err != nil {
		return err
	}

	return createQACoreIndextemplateIfNeed()
}

func initESClient() error {
	esHost := os.Getenv("INIT_ES_HOST")
	esPort := os.Getenv("INIT_ES_PORT")
	esUser := os.Getenv("INIT_ES_USER")
	esPass := os.Getenv("INIT_ES_PASS")


	if esHost == "" {
		logger.Error.Println("ES_HOST env not specified")
		return data.ErrMissingEnv
	}

	if esPort == "" {
		logger.Error.Println("ES_PORT env not specified")
		return data.ErrMissingEnv
	}

	logger.Info.Printf("ES_HOST: %s, ES_PORT: %s", esHost, esPort)
	esURL := fmt.Sprintf("http://%s:%s@%s:%s", esUser, esPass, esHost, esPort)

	// Turn-off sniffing.
	client, err := elastic.NewClient(elastic.SetURL(esURL), elastic.SetSniff(false))
	if err != nil {
		logger.Error.Printf("Fail to connect Elasticsearch: %s", err.Error())
		return err
	}

	esClient = client
	esCtx = context.Background()

	return nil
}

func GetClient() (context.Context, *elastic.Client, error) {
	if esClient == nil || esCtx == nil {
		err := initESClient()
		if err != nil {
			return nil, nil, err
		}
	}
	return esCtx, esClient, nil
}

func createQACoreIndextemplateIfNeed() error {
	ctx, client, err := GetClient()
	if err != nil {
		return err
	}

	// Check existence of records index template.
	exists, err := client.IndexTemplateExists(data.ESQACoreTemplate).Do(ctx)
	if err != nil {
		return err
	}

	logger.Info.Printf("%s not found, create the template", data.ESQACoreTemplate)

	if !exists {
		// Create records index template.
		template, err := ioutil.ReadFile(data.ESQACoreTemplateFile)
		if err != nil {
			return err
		}

		service, err := client.IndexPutTemplate(data.ESQACoreTemplate).BodyString(string(template)).Do(ctx)
		if err != nil {
			return err
		}

		if !service.Acknowledged {
			err = data.ErrESNotAcknowledged
			return err
		}
	}

	return nil
}

func BulkInsertQACoreData(docs []*QACoreDoc) error {
	if len(docs) == 0 {
		return nil
	}

	logger.Info.Printf("Bulk inserting %d documents...", len(docs))
	service := elastic.NewBulkService(esClient)

	for _, doc := range docs {
		indexRequest := elastic.NewBulkIndexRequest()
		indexRequest.Index(data.ESQACoreIndex).Type(data.ESQACoreType).Id(doc.DocID).Doc(doc)
		service.Add(indexRequest)
	}

	_, err := service.Do(esCtx)
	if err != nil {
		return err
	}

	return nil
}
