-- -----------------------------------------------------
-- View `emotibot`.`view_upload_corpus_history`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `emotibot`.`view_upload_corpus_history`;
USE `emotibot`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `emotibot`.`view_upload_corpus_history` AS select `emotibot`.`tbl_upload_corpus_history`.`id` AS `id`,`emotibot`.`tbl_upload_corpus_history`.`user_id` AS `user_id`,`emotibot`.`tbl_upload_corpus_history`.`app_id` AS `app_id`,`emotibot`.`tbl_upload_corpus_history`.`is_part` AS `is_part`,`emotibot`.`tbl_upload_corpus_history`.`type` AS `type`,(case `emotibot`.`tbl_upload_corpus_history`.`type` when 'SQ-Answer' then '标准问题' when 'MIXED' then '混合语料' when 'TEST_CASE' then '测试题' when 'LQ-BQ' then '已标注语料' when 'EQ-BQ' then '扩写语料' when 'USER-LOG' then '未标注语料' end) AS `type_text`,(case `emotibot`.`tbl_upload_corpus_history`.`is_part` when 1 then '增量' else '全量' end) AS `is_part_text`,`emotibot`.`tbl_upload_corpus_history`.`comments` AS `comments`,concat('/Files/ssm/',convert(`emotibot`.`tbl_upload_corpus_history`.`app_id` using utf8),'/',`emotibot`.`tbl_upload_corpus_history`.`file_path`) AS `download_path`,concat('/Files/ssm/',convert(`emotibot`.`tbl_upload_corpus_history`.`app_id` using utf8),'/',concat(left(`emotibot`.`tbl_upload_corpus_history`.`file_path`,(char_length(`emotibot`.`tbl_upload_corpus_history`.`file_path`) - locate('_',reverse(`emotibot`.`tbl_upload_corpus_history`.`file_path`)))),'.xlsx')) AS `raw_file_path`,`emotibot`.`tbl_upload_corpus_history`.`valid_rows` AS `valid_rows`,`emotibot`.`tbl_upload_corpus_history`.`date_time` AS `date_time` from `emotibot`.`tbl_upload_corpus_history` order by `emotibot`.`tbl_upload_corpus_history`.`date_time` desc;



-- -----------------------------------------------------
-- procedure sp_delete_lq_semblance_map
-- -----------------------------------------------------

DELIMITER $$
USE `emotibot`$$
DROP PROCEDURE IF EXISTS sp_delete_lq_semblance_map;
CREATE PROCEDURE `sp_delete_lq_semblance_map`(IN `_appid` VARCHAR(100), IN `lq` VARCHAR(4000) CHARSET utf8mb4)
BEGIN

		delete from emotibot.tbl_lq_semblance_map where (appid = _appid) and (lq1 = lq or lq2 = lq);    
                
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_delete_sqlq_semblance_map
-- -----------------------------------------------------

DELIMITER $$
USE `emotibot`$$
DROP PROCEDURE IF EXISTS sp_delete_sqlq_semblance_map;
CREATE PROCEDURE `sp_delete_sqlq_semblance_map`(IN `_appid` VARCHAR(100), IN `sq` VARCHAR(4000) CHARSET utf8mb4)
BEGIN


		delete from emotibot.tbl_lq_semblance_map where sq_map_id in (select GROUP_CONCAT(appid, sq1, sq2)  from emotibot.tbl_sq_semblance_map where appid = _appid and (sq1 = sq or sq2 = sq));

		delete from emotibot.tbl_sq_semblance_map where appid = _appid and (sq1 = sq or sq2 = sq);
                
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_delete_tag
-- -----------------------------------------------------

DELIMITER $$
USE `emotibot`$$
DROP PROCEDURE IF EXISTS sp_delete_tag;
CREATE PROCEDURE `sp_delete_tag`(IN appid VARCHAR(100),  IN tag_id VARCHAR(100))
BEGIN
		
        DECLARE state INT;
        
		IF EXISTS(SELECT * FROM emotibot.tbl_robot_tag WHERE app_id = appid AND id = tag_id) THEN
				
				IF EXISTS(SELECT * FROM emotibot.tbl_robot_tag WHERE app_id = appid AND ( app_id != 'system' AND type != 'system') AND  id = tag_id) THEN
				                 
                
						DELETE  FROM `emotibot`.`tbl_robot_tag`
								WHERE  app_id = appid AND id = tag_id AND type = 'userdefine';
				
						SET state = 0;
                
                ELSE
						SET state = 2;
                
                END IF;
            
		ELSE
			
				SET state = 1;
		END IF;
        
				
		SELECT state;
                
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_dump_user_info_by_access_token
-- -----------------------------------------------------

DELIMITER $$
USE `emotibot`$$
DROP PROCEDURE IF EXISTS sp_dump_user_info_by_access_token;
CREATE PROCEDURE `sp_dump_user_info_by_access_token`(_token varchar(100))
BEGIN
 	
 
	select * from api_user where userid = (
		SELECT  user_id  FROM tbl_user_access_token
        WHERE 
            TIMESTAMPDIFF(SECOND, create_datetime, NOW()) <= expiration
        AND ACCESS_TOKEN = _token);
        
        
 
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_gen_access_token
-- -----------------------------------------------------

DELIMITER $$
USE `emotibot`$$
DROP PROCEDURE IF EXISTS sp_gen_access_token;
CREATE PROCEDURE `sp_gen_access_token`(_user_id varchar(60))
BEGIN
	
    
    
	
    
    
    
    
		
		
		INSERT INTO  `tbl_user_access_token`
		(`USER_ID`,
		`access_token`,
		`expiration`,
		`create_datetime`)
		VALUES
		(_USER_ID,
		  concat(md5(concat(now(), _USER_ID)) , '-' , sha1(rand())) ,
		  (select `value` from ent_config where name = 'access_token_expiration'),
		now());
    
    
    
    select * from tbl_user_access_token where USER_ID = _user_id order by create_datetime desc limit 1;
    
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_get_config
-- -----------------------------------------------------

DELIMITER $$
USE `emotibot`$$
DROP PROCEDURE IF EXISTS sp_get_config;
CREATE PROCEDURE `sp_get_config`(
	_appId  varchar(50),
	_name   varchar(50)    
)
BEGIN

	
    

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_get_recent_result
-- -----------------------------------------------------

DELIMITER $$
USE `emotibot`$$
DROP PROCEDURE IF EXISTS sp_get_recent_result;
CREATE PROCEDURE `sp_get_recent_result`(IN appid varchar(100), IN num int(5))
BEGIN
SET @rn=0;
set @appid='';
select app_id, id, correct_rate, correct_rate_text, date_time
from
(SELECT app_id, id, correct_rate, correct_rate_text, date_time,
IF(@appid = app_id, @rn := @rn + 1, @rn := 1) AS rn,
@appid := app_id AS appid
FROM emotibot.view_ml_test_history
where status = 'done'
and app_id = appid
order by 1, 2 desc) tmp
where rn <= num;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_get_tag_by_id
-- -----------------------------------------------------

DELIMITER $$
USE `emotibot`$$
DROP PROCEDURE IF EXISTS sp_get_tag_by_id;
CREATE PROCEDURE `sp_get_tag_by_id`(IN appid VARCHAR(100),  IN tag_id VARCHAR(100) )
BEGIN
        
        SELECT * FROM emotibot.tbl_robot_tag WHERE (app_id = appid  OR ( app_id = 'system' AND type = 'system')) AND id = tag_id ORDER BY createtime desc;
        
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_get_tag_by_name
-- -----------------------------------------------------

DELIMITER $$
USE `emotibot`$$
DROP PROCEDURE IF EXISTS sp_get_tag_by_name;
CREATE PROCEDURE `sp_get_tag_by_name`(IN appid VARCHAR(100),  IN tag_name VARCHAR(50) charset utf8 )
BEGIN
        
        SELECT * FROM emotibot.tbl_robot_tag WHERE (app_id = appid  OR ( app_id = 'system' AND type = 'system')) AND name = tag_name ORDER BY createtime desc;
        
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_get_tag_list
-- -----------------------------------------------------

DELIMITER $$
USE `emotibot`$$
DROP PROCEDURE IF EXISTS sp_get_tag_list;
CREATE PROCEDURE `sp_get_tag_list`(IN appid VARCHAR(100) )
BEGIN
        
        SELECT * FROM emotibot.tbl_robot_tag WHERE app_id = appid  OR ( app_id = 'system' AND type = 'system') ORDER BY createtime desc;
        
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_save_ml_test_case
-- -----------------------------------------------------

DELIMITER $$
USE `emotibot`$$
DROP PROCEDURE IF EXISTS sp_save_ml_test_case;
CREATE PROCEDURE `sp_save_ml_test_case`(
	arg_id int,
    arg_app_id varchar(45) ,
    arg_question varchar(4000) charset utf8,
    arg_label_sq varchar(4000) charset utf8,
    arg_create_by_batch_mode bit(1)
)
BEGIN


if (IFNULL(arg_id, 0) = 0) then 
	insert  `emotibot`.`ent_ml_test_case`
	(
		`app_id`,
		`question`,
		`label_sq`,
		`create_by_batch_mode`
	)
	VALUES
	(
		arg_app_id ,
		arg_question ,
		arg_label_sq ,
		arg_create_by_batch_mode
	);
else

	DELETE FROM  `tbl_ml_test_result_no_case_text` WHERE ml_test_case_id = arg_id;
 
    
	UPDATE `emotibot`.`ent_ml_test_case`
	SET 
	`question` = arg_question,
	`label_sq` = arg_label_sq,
	`update_datetime` = CURRENT_TIMESTAMP
	WHERE `id` = arg_id;

end if;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_save_system_tag
-- -----------------------------------------------------

DELIMITER $$
USE `emotibot`$$
DROP PROCEDURE IF EXISTS sp_save_system_tag;
CREATE PROCEDURE `sp_save_system_tag`(IN tag_name VARCHAR(50) charset utf8
																,  IN tag_category VARCHAR(50) charset utf8
                                                                ,  IN create_time DATETIME,  IN description VARCHAR(500) charset utf8)
BEGIN

        DECLARE state INT;
        
        IF NOT EXISTS(SELECT * FROM emotibot.tbl_robot_tag WHERE app_id = 'system' AND type = 'system' AND name = tag_name) THEN
			
				INSERT INTO `emotibot`.`tbl_robot_tag`
						(`app_id`
                        , `name`
                        , `type`
                        , `category`
                        , `createtime`
                        , `description`)
                        VALUES
                        ('system'
                        , tag_name
                        , 'system'
                        , tag_category
                        , create_time
                        , description);
                        
                        SET state = 0;
                        
		ELSE 
        
						SET state = 1;
        
        END IF;
        
        SELECT state, emotibot.tbl_robot_tag.* FROM emotibot.tbl_robot_tag WHERE app_id = 'system' AND type = 'system' AND name = tag_name;
        
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_save_tag
-- -----------------------------------------------------

DELIMITER $$
USE `emotibot`$$
DROP PROCEDURE IF EXISTS sp_save_tag;
CREATE PROCEDURE `sp_save_tag`(IN appid VARCHAR(100),  IN tag_name VARCHAR(50) charset utf8
																,  IN tag_type VARCHAR(50) charset utf8,  IN tag_category VARCHAR(50) charset utf8
                                                                ,  IN create_time DATETIME,  IN description VARCHAR(500) charset utf8)
BEGIN

		DECLARE state INT;
        
        IF (tag_type = 'userdefine') THEN
        
				IF NOT EXISTS(SELECT * FROM emotibot.tbl_robot_tag WHERE (app_id = appid  OR ( app_id = 'system' AND type = 'system')) AND name = tag_name) THEN
					
						INSERT INTO `emotibot`.`tbl_robot_tag`
								(`app_id`
								, `name`
								, `type`
								, `category`
								, `createtime`
								, `description`)
								VALUES
								(appid
								, tag_name
								, tag_type
								, tag_category
								, create_time
								, description);
								
								SET state = 0;
								
				ELSE 
				
								SET state = 1;
				
				END IF;
                
				SELECT state, emotibot.tbl_robot_tag.* FROM emotibot.tbl_robot_tag WHERE (app_id = appid  OR ( app_id = 'system' AND type = 'system')) AND name = tag_name;
         
        
        ELSE
        
				SET state = 3;
                
                SELECT state, NULL AS id, NULL AS app_id, NULL AS name, NULL AS type, NULL AS category, NULL AS createtime, NULL AS description;
         
            
        END IF;
        

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_save_upload_ml_test_history
-- -----------------------------------------------------

DELIMITER $$
USE `emotibot`$$
DROP PROCEDURE IF EXISTS sp_save_upload_ml_test_history;
CREATE PROCEDURE `sp_save_upload_ml_test_history`(
arg_user_id varchar(45),
arg_app_id varchar(45),
arg_is_part bit(1),
arg_comments  varchar(4096)  CHARSET utf8  ,
arg_file_path  varchar(4096)  CHARSET utf8 ,
arg_rows int
)
BEGIN

INSERT INTO `emotibot`.`tbl_upload_ml_test_history`
( 
`user_id`,
`app_id`,
`comments`,
`file_path`,
`rows`,
`is_part`)
VALUES
( 
arg_user_id,
arg_app_id,
arg_comments,
arg_file_path,
arg_rows,
arg_is_part
);


END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_save_upload_result_details
-- -----------------------------------------------------

DELIMITER $$
USE `emotibot`$$
DROP PROCEDURE IF EXISTS sp_save_upload_result_details;
CREATE PROCEDURE `sp_save_upload_result_details`(IN `_upload_id` INT, IN `_action` VARCHAR(45), IN `_row_id` INT, IN `_code` VARCHAR(45), IN `_message` VARCHAR(2000) CHARSET utf8mb4)
BEGIN
INSERT INTO `emotibot`.`tbl_upload_result_details`
(`upload_id`,
`action`,
`row_id`,
`code`,
`message`)
VALUES
(_upload_id,
_action,
_row_id,
_code,
_message);
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_search_tag_list
-- -----------------------------------------------------

DELIMITER $$
USE `emotibot`$$
DROP PROCEDURE IF EXISTS sp_search_tag_list;
CREATE PROCEDURE `sp_search_tag_list`(IN appid VARCHAR(100),  IN keyword VARCHAR(100) charset utf8)
BEGIN
        
        SELECT * FROM emotibot.tbl_robot_tag WHERE (app_id = appid  OR ( app_id = 'system' AND type = 'system')) AND name LIKE CONCAT('%' , keyword , '%')  ORDER BY createtime desc;
        
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_search_user_role
-- -----------------------------------------------------

DELIMITER $$
USE `emotibot`$$
DROP PROCEDURE IF EXISTS sp_search_user_role;
CREATE PROCEDURE `sp_search_user_role`(_searchKey varchar(50))
BEGIN

if _searchKey is null then
	select * from ent_role;
else

	SELECT `ent_role`.`ID`,
    `ent_role`.`NAME`,
    `ent_role`.`DESCRIPTION`,
    `ent_role`.`ENABLED`
FROM `emotibot`.`ent_role`
where `id` like concat('%', _searchKey, '%')
OR `NAME` like concat('%', _searchKey, '%')
OR `DESCRIPTION` like concat('%', _searchKey, '%')
OR `ENABLED` like concat('%', _searchKey, '%');
end if;    
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_switch_role_enabled
-- -----------------------------------------------------

DELIMITER $$
USE `emotibot`$$
DROP PROCEDURE IF EXISTS sp_switch_role_enabled;
CREATE PROCEDURE `sp_switch_role_enabled`(_id int)
BEGIN
UPDATE
`emotibot`.`ent_role`
set enabled = not enabled
where `id` = _id;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_update_config_version
-- -----------------------------------------------------

DELIMITER $$
USE `emotibot`$$
DROP PROCEDURE IF EXISTS sp_update_config_version;
CREATE PROCEDURE `sp_update_config_version`(_userid varchar(50), _appid varchar(50))
BEGIN

if _userid is not null then
	update ent_config_version  set `version_value` = now()   where   user_id = _userid;
end if;

if _appid is not null then

	  set _userid = (select UserId from api_userkey where apikey = _appid);
      
      if _userid is not null then
	  replace `emotibot`.`ent_config_version`
		(`user_id`,
		`app_id`,
		`version_value`)
		VALUES
		(_userid,
		 _appid,
		now());
        end if;
end if;

END$$

DELIMITER ;


-- -----------------------------------------------------
-- procedure sp_update_tag
-- -----------------------------------------------------

DELIMITER $$
USE `emotibot`$$
DROP PROCEDURE IF EXISTS sp_update_tag;
CREATE PROCEDURE `sp_update_tag`(IN tag_id VARCHAR(100), IN appid VARCHAR(100),  IN tag_name VARCHAR(50) charset utf8
																,  IN tag_type VARCHAR(50) charset utf8,  IN tag_category VARCHAR(50) charset utf8
                                                                ,  IN create_time DATETIME,  IN description VARCHAR(500)  charset utf8)
BEGIN

		DECLARE state INT;
        
        IF NOT EXISTS(SELECT * FROM emotibot.tbl_robot_tag WHERE (app_id = appid  OR  app_id = 'system' )  AND name = tag_name AND id != tag_id) THEN

				IF EXISTS(SELECT * FROM emotibot.tbl_robot_tag WHERE app_id = appid AND id = tag_id) THEN
						
						IF EXISTS(SELECT * FROM emotibot.tbl_robot_tag WHERE (app_id = appid AND ( app_id != 'system' AND type != 'system')) AND id = tag_id) THEN
							
								UPDATE `emotibot`.`tbl_robot_tag`
								SET 
										`name` = tag_name,
										`type` = tag_type,
										`category` = tag_category,
										`createtime` = create_time,
										`description` = description
										WHERE  app_id = appid AND id = tag_id AND type = 'userdefine';
								
								SET state = 0;
								
						ELSE
								SET state = 2;
						
						END IF;
						
						
				ELSE
				
								SET state = 1;
						
				END IF;
        
        
        ELSE
				SET state = 3;
        
        END IF;
        
        
        SELECT state, emotibot.tbl_robot_tag.* FROM emotibot.tbl_robot_tag WHERE (app_id = appid  OR ( app_id = 'system' AND type = 'system'))  AND id = tag_id;
        
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_user_select_robot
-- -----------------------------------------------------

DELIMITER $$
USE `emotibot`$$
DROP PROCEDURE IF EXISTS sp_user_select_robot;
CREATE PROCEDURE `sp_user_select_robot`(
arg_user_id varchar(45),
arg_app_id varchar(45)
)
BEGIN

replace `emotibot`.`tbl_user_stare_robot`
(`user_id`,
`app_id`,
`stare_date_time`)
VALUES
(arg_user_id,
arg_app_id,
now());

END$$

DELIMITER ;

-- -----------------------------------------------------
-- View `emotibot`.`view_ai_algorithm`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `emotibot`.`view_ai_algorithm`;
USE `emotibot`;
CREATE   VIEW `emotibot`.`view_ai_algorithm` AS select `emotibot`.`ent_ai_algorithm`.`id` AS `id`,`emotibot`.`ent_ai_algorithm`.`name` AS `name`,`emotibot`.`ent_ai_algorithm`.`kind` AS `kind`,`emotibot`.`ent_ai_algorithm_kind`.`name` AS `kind_name`,`emotibot`.`ent_ai_algorithm`.`enabled` AS `enabled`,`emotibot`.`ent_ai_algorithm`.`description` AS `description`,`emotibot`.`ent_ai_algorithm`.`provider` AS `provider`,`emotibot`.`ent_ai_algorithm`.`default_config` AS `default_config`,`emotibot`.`ent_ai_algorithm`.`predict_url` AS `predict_url`,`emotibot`.`ent_ai_algorithm`.`train_url` AS `train_url`,`emotibot`.`ent_ai_algorithm`.`test_url` AS `test_url`,`emotibot`.`ent_ai_algorithm`.`remove_url` AS `remove_url`,`emotibot`.`ent_ai_algorithm`.`create_datetime` AS `create_datetime` from (`emotibot`.`ent_ai_algorithm` join `emotibot`.`ent_ai_algorithm_kind` on((`emotibot`.`ent_ai_algorithm`.`kind` = `emotibot`.`ent_ai_algorithm_kind`.`id`)));


-- -----------------------------------------------------
-- View `emotibot`.`view_ml_test_case_count`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `emotibot`.`view_ml_test_case_count`;
USE `emotibot`;
CREATE   VIEW `emotibot`.`view_ml_test_case_count` AS select `emotibot`.`ent_ml_test_case`.`app_id` AS `app_id`,`emotibot`.`ent_ml_test_case`.`label_sq` AS `label_sq`,`emotibot`.`ent_ml_test_case`.`space` AS `space`,count(`emotibot`.`ent_ml_test_case`.`id`) AS `test_case_count` from `emotibot`.`ent_ml_test_case` group by `emotibot`.`ent_ml_test_case`.`app_id`,`emotibot`.`ent_ml_test_case`.`label_sq`,`emotibot`.`ent_ml_test_case`.`space`;

-- -----------------------------------------------------
-- View `emotibot`.`view_ml_test_correct_count`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `emotibot`.`view_ml_test_correct_count`;
USE `emotibot`;
CREATE   VIEW `emotibot`.`view_ml_test_correct_count` AS select `view_ml_test_correct_rate_details`.`ml_test_history_id` AS `ml_test_history_id`,`view_ml_test_correct_rate_details`.`app_id` AS `app_id`,sum(`view_ml_test_correct_rate_details`.`correct_count`) AS `correct_count`,sum(`view_ml_test_correct_rate_details`.`test_count`) AS `test_count` from `emotibot`.`view_ml_test_correct_rate_details` group by `view_ml_test_correct_rate_details`.`ml_test_history_id`,`view_ml_test_correct_rate_details`.`app_id`;

-- -----------------------------------------------------
-- View `emotibot`.`view_ml_test_correct_rate`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `emotibot`.`view_ml_test_correct_rate`;
USE `emotibot`;
CREATE   VIEW `emotibot`.`view_ml_test_correct_rate` AS select `view_ml_test_correct_count`.`ml_test_history_id` AS `ml_test_history_id`,`view_ml_test_correct_count`.`correct_count` AS `correct_count`,`view_ml_test_correct_count`.`app_id` AS `app_id`,`view_ml_test_correct_count`.`test_count` AS `test_count`,(`view_ml_test_correct_count`.`correct_count` / `view_ml_test_correct_count`.`test_count`) AS `correct_rate`,concat(format(((100 * `view_ml_test_correct_count`.`correct_count`) / `view_ml_test_correct_count`.`test_count`),2),'%') AS `correct_rate_text` from `emotibot`.`view_ml_test_correct_count`;

-- -----------------------------------------------------
-- View `emotibot`.`view_ml_test_correct_rate_details`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `emotibot`.`view_ml_test_correct_rate_details`;
USE `emotibot`;
CREATE   VIEW `emotibot`.`view_ml_test_correct_rate_details` AS select `view_ml_test_case_count`.`app_id` AS `app_id`,`view_ml_test_case_count`.`label_sq` AS `label_sq`,`view_ml_test_case_count`.`test_case_count` AS `test_case_count`,`view_ml_test_case_count`.`space` AS `space`,`view_ml_test_result_count_by_sq`.`ml_test_history_id` AS `ml_test_history_id`,ifnull(`view_ml_test_result_count_by_sq`.`test_count`,0) AS `test_count`,ifnull(`view_ml_test_result_correct_count_by_sq`.`correct_count`,0) AS `correct_count`,(ifnull(`view_ml_test_result_correct_count_by_sq`.`correct_count`,0) / `view_ml_test_result_count_by_sq`.`test_count`) AS `correct_rate`,concat(format(((100 * ifnull(`view_ml_test_result_correct_count_by_sq`.`correct_count`,0)) / `view_ml_test_result_count_by_sq`.`test_count`),2),'%') AS `correct_rate_text` from ((`emotibot`.`view_ml_test_case_count` left join `emotibot`.`view_ml_test_result_count_by_sq` on(((`view_ml_test_case_count`.`app_id` = `view_ml_test_result_count_by_sq`.`app_id`) and (`view_ml_test_case_count`.`space` = `view_ml_test_result_count_by_sq`.`space`) and (`view_ml_test_case_count`.`label_sq` = `view_ml_test_result_count_by_sq`.`label_sq`)))) left join `emotibot`.`view_ml_test_result_correct_count_by_sq` on(((`view_ml_test_result_count_by_sq`.`app_id` = `view_ml_test_result_correct_count_by_sq`.`app_id`) and (`view_ml_test_result_count_by_sq`.`space` = `view_ml_test_result_correct_count_by_sq`.`space`) and (`view_ml_test_result_count_by_sq`.`label_sq` = `view_ml_test_result_correct_count_by_sq`.`label_sq`) and (`view_ml_test_result_count_by_sq`.`ml_test_history_id` = `view_ml_test_result_correct_count_by_sq`.`ml_test_history_id`))));

-- -----------------------------------------------------
-- View `emotibot`.`view_ml_test_history`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `emotibot`.`view_ml_test_history`;
USE `emotibot`;
CREATE   VIEW `emotibot`.`view_ml_test_history` AS select `emotibot`.`tbl_ml_test_history`.`id` AS `id`,`emotibot`.`tbl_ml_test_history`.`app_id` AS `app_id`,`emotibot`.`tbl_ml_test_history`.`date_time` AS `date_time`,ifnull(`view_ml_test_correct_rate`.`correct_count`,0) AS `correct_count`,ifnull(`view_ml_test_correct_rate`.`test_count`,0) AS `test_count`,ifnull(`view_ml_test_correct_rate`.`correct_rate`,0) AS `correct_rate`,ifnull(`view_ml_test_correct_rate`.`correct_rate_text`,0) AS `correct_rate_text`,`emotibot`.`tbl_ml_test_history`.`status` AS `status`,`emotibot`.`tbl_ml_test_history`.`train_result_id` AS `train_result_id` from (`emotibot`.`tbl_ml_test_history` left join `emotibot`.`view_ml_test_correct_rate` on((`emotibot`.`tbl_ml_test_history`.`id` = `view_ml_test_correct_rate`.`ml_test_history_id`)));

-- -----------------------------------------------------
-- View `emotibot`.`view_ml_test_history_with_train_model`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `emotibot`.`view_ml_test_history_with_train_model`;
USE `emotibot`;
CREATE   VIEW `emotibot`.`view_ml_test_history_with_train_model` AS select `emotibot`.`tbl_ml_test_history_archive`.`id` AS `id`,`emotibot`.`tbl_ml_test_history_archive`.`app_id` AS `app_id`,`emotibot`.`tbl_ml_test_history_archive`.`date_time` AS `date_time`,`emotibot`.`tbl_ml_test_history_archive`.`correct_count` AS `correct_count`,`emotibot`.`tbl_ml_test_history_archive`.`test_count` AS `test_count`,`emotibot`.`tbl_ml_test_history_archive`.`correct_rate` AS `correct_rate`,`emotibot`.`tbl_ml_test_history_archive`.`correct_rate_text` AS `correct_rate_text`,`emotibot`.`tbl_ml_test_history_archive`.`status` AS `status`,`emotibot`.`tbl_ml_test_history_archive`.`train_result_id` AS `train_result_id`,`view_ml_train_history`.`algo_kind_name` AS `algo_kind_name`,`view_ml_train_history`.`algo_name` AS `algo_name`,`view_ml_train_history`.`algo_solution_name` AS `algo_solution_name`,`view_ml_train_history`.`algo_id` AS `algo_id`,`view_ml_train_history`.`algo_kind_id` AS `algo_kind_id`,`view_ml_train_history`.`algo_solution_id` AS `algo_solution_id`,`view_ml_train_history`.`model_id` AS `model_id` from (`emotibot`.`tbl_ml_test_history_archive` join `emotibot`.`view_ml_train_history` on((`view_ml_train_history`.`id` = `emotibot`.`tbl_ml_test_history_archive`.`train_result_id`)));

-- -----------------------------------------------------
-- View `emotibot`.`view_ml_test_lastest`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `emotibot`.`view_ml_test_lastest`;
USE `emotibot`;
CREATE   VIEW `emotibot`.`view_ml_test_lastest` AS select `emotibot`.`tbl_ml_test_history`.`id` AS `id`,`emotibot`.`tbl_ml_test_history`.`app_id` AS `app_id`,`emotibot`.`tbl_ml_test_history`.`date_time` AS `date_time`,`emotibot`.`tbl_ml_test_history`.`status` AS `status` from (`emotibot`.`tbl_ml_test_history` join `emotibot`.`view_ml_test_lastest_datetime` `lastest` on(((`lastest`.`app_id` = `emotibot`.`tbl_ml_test_history`.`app_id`) and (`lastest`.`date_time` = `emotibot`.`tbl_ml_test_history`.`date_time`))));

-- -----------------------------------------------------
-- View `emotibot`.`view_ml_test_lastest_correct_rate_details`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `emotibot`.`view_ml_test_lastest_correct_rate_details`;
USE `emotibot`;
CREATE   VIEW `emotibot`.`view_ml_test_lastest_correct_rate_details` AS select `view_ml_test_case_count`.`app_id` AS `app_id`,`view_ml_test_case_count`.`label_sq` AS `label_sq`,`view_ml_test_case_count`.`test_case_count` AS `test_case_count`,NULL AS `ml_test_history_id`,NULL AS `test_count`,NULL AS `correct_count`,NULL AS `correct_rate`,NULL AS `correct_rate_text` from `emotibot`.`view_ml_test_case_count` where (not(`view_ml_test_case_count`.`app_id` in (select `emotibot`.`tbl_ml_test_history`.`app_id` from `emotibot`.`tbl_ml_test_history`))) union select `view_ml_test_correct_rate_details`.`app_id` AS `app_id`,`view_ml_test_correct_rate_details`.`label_sq` AS `label_sq`,ifnull(`view_ml_test_correct_rate_details`.`test_case_count`,0) AS `test_case_count`,`view_ml_test_correct_rate_details`.`ml_test_history_id` AS `ml_test_history_id`,`view_ml_test_correct_rate_details`.`test_count` AS `test_count`,`view_ml_test_correct_rate_details`.`correct_count` AS `correct_count`,`view_ml_test_correct_rate_details`.`correct_rate` AS `correct_rate`,`view_ml_test_correct_rate_details`.`correct_rate_text` AS `correct_rate_text` from (`emotibot`.`view_ml_test_correct_rate_details` join `emotibot`.`view_ml_test_lastest` on((`view_ml_test_lastest`.`id` = `view_ml_test_correct_rate_details`.`ml_test_history_id`))) union select `view_ml_test_correct_rate_details`.`app_id` AS `app_id`,`view_ml_test_correct_rate_details`.`label_sq` AS `label_sq`,ifnull(`view_ml_test_correct_rate_details`.`test_case_count`,0) AS `test_case_count`,`view_ml_test_correct_rate_details`.`ml_test_history_id` AS `ml_test_history_id`,`view_ml_test_correct_rate_details`.`test_count` AS `test_count`,`view_ml_test_correct_rate_details`.`correct_count` AS `correct_count`,`view_ml_test_correct_rate_details`.`correct_rate` AS `correct_rate`,`view_ml_test_correct_rate_details`.`correct_rate_text` AS `correct_rate_text` from `emotibot`.`view_ml_test_correct_rate_details` where isnull(`view_ml_test_correct_rate_details`.`ml_test_history_id`);

-- -----------------------------------------------------
-- View `emotibot`.`view_ml_test_lastest_datetime`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `emotibot`.`view_ml_test_lastest_datetime`;
USE `emotibot`;
CREATE   VIEW `emotibot`.`view_ml_test_lastest_datetime` AS select `max_ori`.`app_id` AS `app_id`,max(`max_ori`.`date_time`) AS `date_time` from `emotibot`.`tbl_ml_test_history` `max_ori` group by `max_ori`.`app_id`;

-- -----------------------------------------------------
-- View `emotibot`.`view_ml_test_lastest_history`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `emotibot`.`view_ml_test_lastest_history`;
USE `emotibot`;
CREATE   VIEW `emotibot`.`view_ml_test_lastest_history` AS select `view_ml_test_history`.`id` AS `id`,`view_ml_test_history`.`app_id` AS `app_id`,`view_ml_test_history`.`date_time` AS `date_time`,`view_ml_test_history`.`correct_count` AS `correct_count`,`view_ml_test_history`.`test_count` AS `test_count`,`view_ml_test_history`.`correct_rate` AS `correct_rate`,`view_ml_test_history`.`correct_rate_text` AS `correct_rate_text`,`view_ml_test_history`.`status` AS `status` from (`emotibot`.`view_ml_test_history` join `emotibot`.`view_ml_test_lastest_datetime` `lastest` on(((`lastest`.`app_id` = `view_ml_test_history`.`app_id`) and (`lastest`.`date_time` = `view_ml_test_history`.`date_time`))));

-- -----------------------------------------------------
-- View `emotibot`.`view_ml_test_lastest_result`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `emotibot`.`view_ml_test_lastest_result`;
USE `emotibot`;
CREATE   VIEW `emotibot`.`view_ml_test_lastest_result` AS select `emotibot`.`ent_ml_test_case`.`id` AS `test_case_id`,`emotibot`.`ent_ml_test_case`.`app_id` AS `app_id`,NULL AS `ml_test_history_id`,`emotibot`.`ent_ml_test_case`.`question` AS `question`,`emotibot`.`ent_ml_test_case`.`label_sq` AS `label_sq`,NULL AS `matched_sq`,NULL AS `right` from `emotibot`.`ent_ml_test_case` where (not(`emotibot`.`ent_ml_test_case`.`id` in (select `emotibot`.`tbl_ml_test_result_no_case_text`.`ml_test_case_id` from `emotibot`.`tbl_ml_test_result_no_case_text`))) union select `view_ml_test_result`.`test_case_id` AS `test_case_id`,`view_ml_test_result`.`app_id` AS `app_id`,`view_ml_test_result`.`ml_test_history_id` AS `ml_test_history_id`,`view_ml_test_result`.`question` AS `question`,`view_ml_test_result`.`label_sq` AS `label_sq`,`view_ml_test_result`.`matched_sq` AS `matched_sq`,`view_ml_test_result`.`right` AS `right` from (`emotibot`.`view_ml_test_result` join `emotibot`.`view_ml_test_lastest_history` on((`view_ml_test_result`.`ml_test_history_id` = `view_ml_test_lastest_history`.`id`)));

-- -----------------------------------------------------
-- View `emotibot`.`view_ml_test_result`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `emotibot`.`view_ml_test_result`;
USE `emotibot`;
CREATE   VIEW `emotibot`.`view_ml_test_result` AS select `emotibot`.`ent_ml_test_case`.`id` AS `test_case_id`,`emotibot`.`ent_ml_test_case`.`app_id` AS `app_id`,`emotibot`.`tbl_ml_test_result_no_case_text`.`ml_test_history_id` AS `ml_test_history_id`,`emotibot`.`ent_ml_test_case`.`question` AS `question`,`emotibot`.`ent_ml_test_case`.`label_sq` AS `label_sq`,(case `emotibot`.`tbl_ml_test_result_no_case_text`.`matched_sq` when -(1) then '不能找到正确应答' else `emotibot`.`tbl_ml_test_result_no_case_text`.`matched_sq` end) AS `matched_sq`,(case `emotibot`.`tbl_ml_test_result_no_case_text`.`matched_sq` when `emotibot`.`ent_ml_test_case`.`label_sq` then 1 else 0 end) AS `right`,`emotibot`.`tbl_ml_test_result_no_case_text`.`score` AS `score`,`emotibot`.`tbl_ml_test_result_no_case_text`.`source` AS `source`,`emotibot`.`ent_ml_test_case`.`space` AS `space` from (`emotibot`.`ent_ml_test_case` join `emotibot`.`tbl_ml_test_result_no_case_text` on((`emotibot`.`tbl_ml_test_result_no_case_text`.`ml_test_case_id` = `emotibot`.`ent_ml_test_case`.`id`)));

-- -----------------------------------------------------
-- View `emotibot`.`view_ml_test_result_correct_count_by_sq`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `emotibot`.`view_ml_test_result_correct_count_by_sq`;
USE `emotibot`;
CREATE   VIEW `emotibot`.`view_ml_test_result_correct_count_by_sq` AS select `view_ml_test_result`.`app_id` AS `app_id`,`view_ml_test_result`.`ml_test_history_id` AS `ml_test_history_id`,`view_ml_test_result`.`label_sq` AS `label_sq`,`view_ml_test_result`.`space` AS `space`,count(`view_ml_test_result`.`test_case_id`) AS `correct_count` from `emotibot`.`view_ml_test_result` where (`view_ml_test_result`.`right` = 1) group by `view_ml_test_result`.`app_id`,`view_ml_test_result`.`ml_test_history_id`,`view_ml_test_result`.`label_sq`,`view_ml_test_result`.`space`;

-- -----------------------------------------------------
-- View `emotibot`.`view_ml_test_result_count`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `emotibot`.`view_ml_test_result_count`;
USE `emotibot`;
CREATE   VIEW `emotibot`.`view_ml_test_result_count` AS select `emotibot`.`tbl_ml_test_result_no_case_text`.`ml_test_history_id` AS `ml_test_history_id`,count(`emotibot`.`tbl_ml_test_result_no_case_text`.`ml_test_case_id`) AS `test_count` from `emotibot`.`tbl_ml_test_result_no_case_text` group by `emotibot`.`tbl_ml_test_result_no_case_text`.`ml_test_history_id`;

-- -----------------------------------------------------
-- View `emotibot`.`view_ml_test_result_count_by_sq`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `emotibot`.`view_ml_test_result_count_by_sq`;
USE `emotibot`;
CREATE   VIEW `emotibot`.`view_ml_test_result_count_by_sq` AS select `view_ml_test_result`.`app_id` AS `app_id`,`view_ml_test_result`.`ml_test_history_id` AS `ml_test_history_id`,`view_ml_test_result`.`label_sq` AS `label_sq`,`view_ml_test_result`.`space` AS `space`,(case when isnull(`view_ml_test_result`.`ml_test_history_id`) then NULL else count(`view_ml_test_result`.`test_case_id`) end) AS `test_count` from `emotibot`.`view_ml_test_result` group by `view_ml_test_result`.`app_id`,`view_ml_test_result`.`ml_test_history_id`,`view_ml_test_result`.`label_sq`,`view_ml_test_result`.`space`;

-- -----------------------------------------------------
-- View `emotibot`.`view_ml_train_history`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `emotibot`.`view_ml_train_history`;
USE `emotibot`;
CREATE   VIEW `emotibot`.`view_ml_train_history` AS select `emotibot`.`tbl_ml_train_history`.`id` AS `id`,`emotibot`.`tbl_ml_train_history`.`app_id` AS `app_id`,`emotibot`.`ent_ai_algorithm_kind`.`id` AS `algo_kind_id`,`emotibot`.`ent_ai_algorithm_kind`.`name` AS `algo_kind_name`,`emotibot`.`ent_ai_algorithm`.`name` AS `algo_name`,`emotibot`.`tbl_ai_solution`.`name` AS `algo_solution_name`,`emotibot`.`tbl_ml_train_history`.`status` AS `status`,`emotibot`.`tbl_ml_train_history`.`model_id` AS `model_id`,`emotibot`.`tbl_ml_train_history`.`result` AS `result`,`emotibot`.`tbl_ml_train_history`.`create_datetime` AS `create_datetime`,`emotibot`.`tbl_ml_train_history`.`update_datetime` AS `update_datetime`,`emotibot`.`tbl_ml_train_history`.`algo_id` AS `algo_id`,`emotibot`.`tbl_ml_train_history`.`algo_solution_id` AS `algo_solution_id`,`emotibot`.`tbl_ml_train_history`.`train_url` AS `train_url`,`emotibot`.`tbl_ml_train_history`.`config` AS `config` from (((`emotibot`.`tbl_ml_train_history` join `emotibot`.`ent_ai_algorithm_kind` on((`emotibot`.`ent_ai_algorithm_kind`.`id` = `emotibot`.`tbl_ml_train_history`.`algo_kind_id`))) join `emotibot`.`ent_ai_algorithm` on((`emotibot`.`ent_ai_algorithm`.`id` = `emotibot`.`tbl_ml_train_history`.`algo_id`))) left join `emotibot`.`tbl_ai_solution` on((`emotibot`.`tbl_ai_solution`.`id` = `emotibot`.`tbl_ml_train_history`.`algo_solution_id`)));

-- -----------------------------------------------------
-- View `emotibot`.`view_right_module`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `emotibot`.`view_right_module`;
USE `emotibot`;
CREATE   VIEW `emotibot`.`view_right_module` AS select `emotibot`.`ent_right`.`NAME` AS `Name`,`emotibot`.`ent_module`.`Name` AS `ModuleName` from (`emotibot`.`ent_right` join `emotibot`.`ent_module`) where (`emotibot`.`ent_module`.`ID` = `emotibot`.`ent_right`.`MODULEID`);

-- -----------------------------------------------------
-- View `emotibot`.`view_upload_test_history`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `emotibot`.`view_upload_test_history`;
USE `emotibot`;
CREATE   VIEW `emotibot`.`view_upload_test_history` AS select `emotibot`.`tbl_upload_ml_test_history`.`id` AS `id`,`emotibot`.`tbl_upload_ml_test_history`.`app_id` AS `app_id`,`emotibot`.`tbl_upload_ml_test_history`.`user_id` AS `user_id`,`emotibot`.`tbl_upload_ml_test_history`.`is_part` AS `is_part`,`emotibot`.`tbl_upload_ml_test_history`.`comments` AS `comments`,(case `emotibot`.`tbl_upload_ml_test_history`.`is_part` when 1 then '增量' else '全量' end) AS `is_part_text`,concat('/Files/ml_test/',convert(`emotibot`.`tbl_upload_ml_test_history`.`app_id` using utf8),'/',`emotibot`.`tbl_upload_ml_test_history`.`file_path`) AS `download_path`,concat('/Files/ml_test/',convert(`emotibot`.`tbl_upload_ml_test_history`.`app_id` using utf8),'/',concat(left(`emotibot`.`tbl_upload_ml_test_history`.`file_path`,(char_length(`emotibot`.`tbl_upload_ml_test_history`.`file_path`) - locate('_',reverse(`emotibot`.`tbl_upload_ml_test_history`.`file_path`)))),'.xlsx')) AS `raw_file_path`,`emotibot`.`tbl_upload_ml_test_history`.`rows` AS `rows`,`emotibot`.`tbl_upload_ml_test_history`.`date_time` AS `date_time` from `emotibot`.`tbl_upload_ml_test_history` order by `emotibot`.`tbl_upload_ml_test_history`.`date_time` desc;

-- -----------------------------------------------------
-- View `emotibot`.`view_upload_userlog_history`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `emotibot`.`view_upload_userlog_history`;
USE `emotibot`;
CREATE   VIEW `emotibot`.`view_upload_userlog_history` AS select `view_upload_corpus_history`.`id` AS `id`,`view_upload_corpus_history`.`user_id` AS `user_id`,`view_upload_corpus_history`.`app_id` AS `app_id`,`view_upload_corpus_history`.`is_part` AS `is_part`,`view_upload_corpus_history`.`type` AS `type`,`view_upload_corpus_history`.`type_text` AS `type_text`,`view_upload_corpus_history`.`is_part_text` AS `is_part_text`,`view_upload_corpus_history`.`comments` AS `comments`,`view_upload_corpus_history`.`download_path` AS `download_path`,`view_upload_corpus_history`.`raw_file_path` AS `raw_file_path`,`view_upload_corpus_history`.`valid_rows` AS `valid_rows`,`view_upload_corpus_history`.`date_time` AS `date_time` from `emotibot`.`view_upload_corpus_history` where ((`view_upload_corpus_history`.`type` = 'USER-LOG') or (`view_upload_corpus_history`.`type` = 'LQ-BQ')) order by `view_upload_corpus_history`.`date_time` desc;

-- -----------------------------------------------------
-- View `emotibot`.`view_user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `emotibot`.`view_user`;
USE `emotibot`;
CREATE   VIEW `emotibot`.`view_user` AS select `emotibot`.`api_user`.`UserId` AS `UserId`,`emotibot`.`api_user`.`Phone` AS `Phone`,`emotibot`.`api_user`.`Email` AS `Email`,`emotibot`.`api_user`.`CreatedTime` AS `CreatedTime`,`emotibot`.`api_user`.`Password` AS `Password`,`emotibot`.`api_user`.`NickName` AS `UserName`,`emotibot`.`api_user`.`Gender` AS `Gender`,`emotibot`.`api_user`.`Type` AS `Type`,`emotibot`.`api_user`.`Status` AS `Status`,`emotibot`.`api_user`.`AccountStatus` AS `account_status`,`emotibot`.`api_user`.`UpdatedTime` AS `UpdatedTime`,`emotibot`.`api_user`.`Owner` AS `Owner`,`emotibot`.`api_user`.`Remark` AS `Remark`,`emotibot`.`api_user`.`AiNickName` AS `AiNickName`,`emotibot`.`api_user`.`Msg` AS `Msg`,`emotibot`.`api_user`.`enterprise_id` AS `enterprise_id`,`emotibot`.`api_user`.`RoleId` AS `roleid`,`emotibot`.`api_enterprise`.`enterprise_name` AS `enterprise_name`,`emotibot`.`api_enterprise`.`account_type` AS `account_type`,`emotibot`.`api_enterprise`.`customer_manager` AS `customer_manager`,`emotibot`.`api_enterprise`.`email` AS `super_user_email`,`emotibot`.`api_enterprise`.`phone` AS `super_user_phone`,`emotibot`.`api_enterprise`.`project_leader` AS `project_leader`,`emotibot`.`api_enterprise`.`account_status` AS `enterprise_account_status`,`emotibot`.`api_enterprise`.`is_remove` AS `is_remove`,`emotibot`.`api_enterprise`.`business_card` AS `business_card`,`emotibot`.`api_enterprise`.`remark` AS `enterprise_remark`,`emotibot`.`api_enterprise`.`enterprise_type` AS `enterprise_type` from (`emotibot`.`api_user` left join `emotibot`.`api_enterprise` on((convert(`emotibot`.`api_user`.`enterprise_id` using utf8mb4) = `emotibot`.`api_enterprise`.`id`)));

-- -----------------------------------------------------
-- View `emotibot`.`view_user_lastest_stared_robot`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `emotibot`.`view_user_lastest_stared_robot`;
USE `emotibot`;
CREATE   VIEW `emotibot`.`view_user_lastest_stared_robot` AS select `emotibot`.`tbl_user_stare_robot`.`user_id` AS `user_id`,`emotibot`.`tbl_user_stare_robot`.`app_id` AS `app_id`,max(`emotibot`.`tbl_user_stare_robot`.`stare_date_time`) AS `lastest_stared_datetime` from `emotibot`.`tbl_user_stare_robot` group by `emotibot`.`tbl_user_stare_robot`.`user_id`,`emotibot`.`tbl_user_stare_robot`.`app_id`;

-- -----------------------------------------------------
-- View `emotibot`.`view_user_role_ex`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `emotibot`.`view_user_role_ex`;
USE `emotibot`;
CREATE   VIEW `emotibot`.`view_user_role_ex` AS select `emotibot`.`ent_role`.`ID` AS `ID`,`emotibot`.`ent_role`.`NAME` AS `NAME`,`emotibot`.`ent_role`.`DESCRIPTION` AS `DESCRIPTION`,`emotibot`.`ent_role`.`ENABLED` AS `enabled`,(case `emotibot`.`ent_role`.`ENABLED` when 1 then '有效' else '无效' end) AS `enabledStr` from `emotibot`.`ent_role`;

-- -----------------------------------------------------
-- View `emotibot`.`view_valid_robot`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `emotibot`.`view_valid_robot`;
USE `emotibot`;
CREATE   VIEW `emotibot`.`view_valid_robot` AS select `emotibot`.`api_userkey`.`NickName` AS `nickname`,`emotibot`.`api_userkey`.`ApiKey` AS `apikey` from `emotibot`.`api_userkey` where (`emotibot`.`api_userkey`.`Status` = 1);
USE `emotibot`;

