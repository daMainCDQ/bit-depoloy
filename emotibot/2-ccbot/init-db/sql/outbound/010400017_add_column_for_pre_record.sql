-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `pre_record` ADD COLUMN `enterprise` VARCHAR(32) NOT NULL DEFAULT '' AFTER `id`;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

ALTER TABLE `pre_record` DROP COLUMN `enterprise`;
