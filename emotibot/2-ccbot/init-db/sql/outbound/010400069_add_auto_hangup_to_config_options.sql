-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
BEGIN;
INSERT INTO `config_options` (`enterprise_id`, `catalog_name`, `config_item`, `item_description`, `value`, `range`) VALUES
('default', 'auto_hangup', 'max_call_duration', '超时通话自动挂机时间，单位秒，0值无效', '0', NULL),
('default', 'auto_hangup', 'max_alert_duration', '超长振铃自动挂机时间，0值无效，单位秒。时间根据真实网络情况调整', '0', NULL);
COMMIT;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DELETE FROM `outbound`.`config_options` WHERE `catalog_name` = 'auto_hangup';
