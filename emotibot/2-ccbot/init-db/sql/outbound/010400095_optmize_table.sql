-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
DROP TABLE `outbound`.`human_agents`;
DROP TABLE `outbound`.`speech_config`;
DROP TABLE `outbound`.`enterprise_info`;
DROP TABLE `outbound`.`user_info`;

ALTER TABLE `outbound`.`call_list` ADD COLUMN `redial_status` tinyint NOT NULL DEFAULT '0' COMMENT '重拨状态，1待重拨，2表示已经重拨，0默认状态。';
ALTER TABLE `outbound`.`call_list` ADD COLUMN `redial_num` tinyint NOT NULL DEFAULT '0' COMMENT '已重拨数量，0表示没有重拨。';
ALTER TABLE `outbound`.`call_list` ADD COLUMN `later_call_status` tinyint NOT NULL DEFAULT '0' COMMENT '追打状态，1待追打，2表示已追打，0默认状态。';
ALTER TABLE `outbound`.`call_list` ADD COLUMN `later_call_num` tinyint NOT NULL DEFAULT '0' COMMENT '已追打数量，0表示没有追打。';

ALTER TABLE `outbound`.`enterprise_line_assign` ADD COLUMN `line_used` smallint NOT NULL DEFAULT '0' COMMENT '线路当前被使用掉的数量' AFTER `line_assigned`;
ALTER TABLE `outbound`.`callout_phone` ADD COLUMN `line_used` smallint NOT NULL DEFAULT '0' COMMENT '线路当前被使用掉的数量' AFTER `capacity`;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back