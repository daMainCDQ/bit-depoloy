-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for tts_scenario_text
-- ----------------------------
DROP TABLE IF EXISTS `tts_scenario_text`;
CREATE TABLE `tts_scenario_text` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `enterprise_id` varchar(50) NOT NULL COMMENT '企业id',
  `app_id` varchar(50) NOT NULL COMMENT '机器人id',
  `scenario_id` varchar(50) NOT NULL COMMENT '场景id',
  `uuid` varchar(50) NOT NULL COMMENT '由md5(enterpriseId+appId+scenarioId)',
  `text` text NOT NULL COMMENT '常量话术',
  `text_md5` varchar(32) NOT NULL COMMENT 'text的md5值，用来建索引',
  `original_filename` varchar(200) DEFAULT NULL COMMENT '初始文件名',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `udx_union` (`uuid`,`text_md5`) USING BTREE,
  KEY `idx_text` (`text_md5`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='tts场景话术表';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `tts_scenario_text`;
