
-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for tts_common_text
-- ----------------------------
CREATE TABLE `tts_common_text` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `text` text NOT NULL COMMENT '常量话术',
  `audio_file_path` varchar(200) NOT NULL COMMENT '音频文件路径',
  `conf_uuid` varchar(50) NOT NULL COMMENT '对应fs_config表的conf_uuid',
  `status` tinyint(4) NOT NULL COMMENT '0:未完成，1:已完成',
  `duration` int(11) NOT NULL COMMENT '音频文件时长',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `udx_text_confuuid` (`text`(50),`conf_uuid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='tts常量话术表';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `tts_common_text`;