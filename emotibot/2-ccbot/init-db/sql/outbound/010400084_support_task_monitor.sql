-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `outbound`.`task` ADD COLUMN `activate_time` bigint(20) NOT NULL DEFAULT '0' COMMENT 'activate time';
ALTER TABLE `outbound`.`call_record` ADD COLUMN `call_state` varchar(32) NULL DEFAULT '' COMMENT '通话的实时状态，包括DOWN,EARLY,ACTIVE,HANGUP,RINGING等状态';
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back