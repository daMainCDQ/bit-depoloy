-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `outbound`.`black_list` CHANGE `phone` `phone` varchar(255) NOT NULL DEFAULT '' COMMENT 'phone number ';
ALTER TABLE `outbound`.`call_list` CHANGE `phone_number` `phone_number` varchar(255) NOT NULL COMMENT 'phone unmber';
ALTER TABLE `outbound`.`call_record` CHANGE `caller_number` `caller_number` varchar(255) NOT NULL COMMENT '主叫号码';
ALTER TABLE `outbound`.`call_record` CHANGE `callee_number` `callee_number` varchar(255) NULL DEFAULT '' COMMENT '被叫号码';
ALTER TABLE `outbound`.`callout_phone` CHANGE `caller_phone_number` `caller_phone_number` varchar(255) NULL COMMENT 'phone number';
ALTER TABLE `outbound`.`filter_record` CHANGE `phone_number` `phone_number` varchar(255) NOT NULL COMMENT '电话号码';
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back