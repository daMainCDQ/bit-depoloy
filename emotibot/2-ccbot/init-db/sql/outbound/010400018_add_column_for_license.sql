-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `license_enterprise` ADD COLUMN `start` bigint(20) NOT NULL DEFAULT '0' COMMENT 'start of validity' AFTER `name`;
ALTER TABLE `license_enterprise` ADD COLUMN `expire` bigint(20) NOT NULL DEFAULT '0' COMMENT 'end of validity' AFTER `start`;

ALTER TABLE `license_user_list` ADD COLUMN `start` bigint(20) NOT NULL DEFAULT '0' COMMENT 'start of validity' AFTER `type`;
ALTER TABLE `license_user_list` ADD COLUMN `expire` bigint(20) NOT NULL DEFAULT '0' COMMENT 'end of validity' AFTER `start`;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

ALTER TABLE `license_enterprise` DROP COLUMN `start`;
ALTER TABLE `license_enterprise` DROP COLUMN `expire`;

ALTER TABLE `license_user_list` DROP COLUMN `start`;
ALTER TABLE `license_user_list` DROP COLUMN `expire`;
