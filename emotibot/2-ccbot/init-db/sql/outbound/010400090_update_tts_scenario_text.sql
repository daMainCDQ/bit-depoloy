-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `outbound`.`tts_scenario_text` DROP INDEX `udx_union`;
ALTER TABLE `outbound`.`tts_scenario_text` DROP COLUMN `enterprise_id`;
ALTER TABLE `outbound`.`tts_scenario_text` DROP COLUMN `app_id`;
ALTER TABLE `outbound`.`tts_scenario_text` DROP COLUMN `scenario_id`;
ALTER TABLE `outbound`.`tts_scenario_text` DROP COLUMN `uuid`;
ALTER TABLE `outbound`.`tts_scenario_text` ADD COLUMN `tts_scenario_id` bigint(20) NULL DEFAULT NULL COMMENT 'tts_scenario表主键' AFTER `id`;
ALTER TABLE `outbound`.`tts_scenario_text` ADD UNIQUE INDEX `udx_id_text`(`tts_scenario_id`, `text_md5`) USING BTREE;
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
