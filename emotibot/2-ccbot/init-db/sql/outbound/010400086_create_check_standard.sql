-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for check_standard
-- ----------------------------
DROP TABLE IF EXISTS `check_standard`;
CREATE TABLE `check_standard` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `task_id` bigint(11) NOT NULL COMMENT '任务id',
  `content` text NOT NULL COMMENT '标准内容',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_task_id` (`task_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='检查标准表';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `check_standard`;
