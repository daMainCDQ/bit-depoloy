-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for enterprise_line_assign
-- ----------------------------
CREATE TABLE `enterprise_line_assign` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `enterprise_id` varchar(32) NOT NULL,
  `line_assigned` int(11) NOT NULL,
  `line_type` tinyint(4) NOT NULL DEFAULT '0',
  `create_time` bigint(20) NOT NULL,
  `last_update_time` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `enterprise_line_assign`;


