-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for tts_session_pre_generate_detail
-- ----------------------------
DROP TABLE IF EXISTS `tts_session_pre_generate_detail`;
CREATE TABLE `tts_session_pre_generate_detail` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pre_generate_id` bigint(11) NOT NULL COMMENT 'tts会话预生成表 主键',
  `scenario_id` varchar(50) NOT NULL COMMENT '场景id',
  `text_with_var_key` text NOT NULL COMMENT '包含global变量的文本话术',
  `text_with_var_key_md5` varchar(32) NOT NULL COMMENT 'text_with_var_key的md5值，用来建索引',
  `text_with_var_value` text NOT NULL COMMENT '替换global变量后的文本话术',
  `audio_file_path` varchar(200) NOT NULL COMMENT '音频文件路径',
  `status` tinyint(4) NOT NULL COMMENT '0:未完成，1:已完成',
  `duration` int(11) NOT NULL COMMENT '音频文件时长',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `udx_union` (`pre_generate_id`,`text_with_var_key_md5`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='tts会话预生成明细表';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `tts_session_pre_generate_detail`;
