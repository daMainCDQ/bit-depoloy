-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
DELETE FROM `config_options` WHERE `enterprise_id`='default';
INSERT INTO `config_options` (`enterprise_id`, `catalog_name`, `config_item`, `item_description`, `value`, `range`) VALUES ('default', 'data_masking', 'switch', '脱敏开关', '[{\"enable\":false,\"beginPos\":4,\"length\":4}]', '');
INSERT INTO `config_options` (`enterprise_id`, `catalog_name`, `config_item`, `item_description`, `value`, `range`) VALUES ('default', 'tts_pregenerate', 'switch', 'TTS预生成开关', 'off', '');
INSERT INTO `config_options` (`enterprise_id`, `catalog_name`, `config_item`, `item_description`, `value`, `range`) VALUES ('default', 'do_not_call', 'dtc_periods', '不呼叫时段', '', '');
INSERT INTO `config_options` (`enterprise_id`, `catalog_name`, `config_item`, `item_description`, `value`, `range`) VALUES ('default', 'recording_download', 'batch_down_num', '录音批量下载数量', '50', '{\"min\":10,\"max\":100}');
INSERT INTO `config_options` (`enterprise_id`, `catalog_name`, `config_item`, `item_description`, `value`, `range`) VALUES ('default', 'recording_download', 'record_format', '录音下载格式', '{\"format_name\":\"标准模式\",\"channel_type\":\"双声道\",\"bit_num\":\"16 bit\",\"bit_rate\":\"128kbps\",\"sample_rate\":\"8khz\"}', '[{\"format_name\":\"高保真模式\",\"channel_type\":\"双声道\",\"bit_num\":\"16 bit\",\"bit_rate\":\"256kbps\",\"sample_rate\":\"8khz\"},{\"format_name\":\"标准模式\",\"channel_type\":\"双声道\",\"bit_num\":\"16 bit\",\"bit_rate\":\"128kbps\",\"sample_rate\":\"8khz\"},{\"format_name\":\"经济模式\",\"channel_type\":\"单声道\",\"bit_num\":\"16 bit\",\"bit_rate\":\"64kbps\",\"sample_rate\":\"8khz\"}]');
INSERT INTO `config_options` (`enterprise_id`, `catalog_name`, `config_item`, `item_description`, `value`, `range`) VALUES ('default', 'auto_redial', 'fail_redial', '呼叫失败自动重拨', '[\"无人接听\",\"拒接/忙线\"]', '[\"无人接听\",\"拒接/忙线\",\"关机/飞行模式\",\"空号\",\"停机\",\"外地号码\",\"不在服务区\"]');
INSERT INTO `config_options` (`enterprise_id`, `catalog_name`, `config_item`, `item_description`, `value`, `range`) VALUES ('default', 'fs_spec', 'max_calls', 'FS所能支持的最大并发呼叫数', '200', '');
INSERT INTO `config_options` (`enterprise_id`, `catalog_name`, `config_item`, `item_description`, `value`, `range`) VALUES ('default', 'add_zero', 'switch', '是否需要为外地号码自动加拨0的', 'on', '');
INSERT INTO `config_options` (`enterprise_id`, `catalog_name`, `config_item`, `item_description`, `value`, `range`) VALUES ('default', 'region', 'region', '国家和地区', 'cn', '[{\"region\":\"cn\",\"desc\":\"中国大陆\"},{\"region\":\"tw\",\"desc\":\"中国台湾\"},{\"region\":\"hkmacao\",\"desc\":\"中国香港/澳门\"}]');
INSERT INTO `config_options` (`enterprise_id`, `catalog_name`, `config_item`, `item_description`, `value`, `range`) VALUES ('default', 'bargein', 'switch', '是否开启打断', 'off', '');
INSERT INTO `config_options` (`enterprise_id`, `catalog_name`, `config_item`, `item_description`, `value`, `range`) VALUES ('default', 'bargein', 'threshold', '用户讲话时间超过阈值，机器人回暂停当前讲话', '1', '');
INSERT INTO `config_options` (`enterprise_id`, `catalog_name`, `config_item`, `item_description`, `value`, `range`) VALUES ('default', 'bargein', 'reply_text', '打断回复话术', '好的，您说', '');
INSERT INTO `config_options` (`enterprise_id`, `catalog_name`, `config_item`, `item_description`, `value`, `range`) VALUES ('default', 'silent_reminder', 'silent_duration', '用户不讲话时间超过静默时长，机器人会主动提醒用户', '5', '');
INSERT INTO `config_options` (`enterprise_id`, `catalog_name`, `config_item`, `item_description`, `value`, `range`) VALUES ('default', 'silent_reminder', 'reminder_text', '提醒话术', '您好，还在吗？', '');

-- +migrate Down
DELETE FROM `config_options` WHERE `enterprise_id`='default'; 

