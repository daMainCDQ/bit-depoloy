-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `outbound`.`scenario_config` ADD COLUMN `custom_info` text NULL COMMENT '扩展字段，jsonstring' AFTER `barge_in`;
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

ALTER TABLE `outbound`.`scenario_config` DROP COLUMN `custom_info`;


