-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for user_roles
-- ----------------------------
CREATE TABLE `user_roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id must be unique',
  `user_id` varchar(50) NOT NULL DEFAULT '' COMMENT 'user uuid',
  `role_uuid` varchar(50) NOT NULL DEFAULT '' COMMENT 'role uuid',
  `enterprise` varchar(32) NOT NULL DEFAULT '' COMMENT 'user enterprise',
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_role_uuid` (`role_uuid`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COMMENT='user roles mapping';

-- ----------------------------
-- Table structure for roles
-- ----------------------------
CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id must be unique',
  `role_uuid` varchar(50) NOT NULL DEFAULT '' COMMENT 'role uuid',
  `role_name` varchar(50) NOT NULL DEFAULT '' COMMENT 'role name',
  `role_type` varchar(32) NOT NULL DEFAULT '' COMMENT 'adminRole and userRole',
  `enterprise` varchar(50) NOT NULL DEFAULT '' COMMENT 'enterprise',
  `description` varchar(512) NOT NULL COMMENT 'description',
  `create_time` bigint(20) NOT NULL DEFAULT '0' COMMENT 'create time',
  `last_update_time` bigint(20) NOT NULL DEFAULT '0' COMMENT 'update time',
  PRIMARY KEY (`id`),
  KEY `idx_role_uuid` (`role_uuid`),
  KEY `idx_role_name` (`role_name`),
  KEY `idx_enterprise` (`enterprise`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COMMENT='roles discribe';

-- ----------------------------
-- Records of roles
-- ----------------------------
BEGIN;
INSERT INTO `roles` VALUES (1, 'b85218cb15594235bd3b9fac98630dcb', '管理员默认角色', 'adminRole', 'default', '管理员权限', '1559734217', '1559734218');
INSERT INTO `roles` VALUES (2, '9b929934412b43129f912106f0a6c665', '用户默认角色', 'userRole', 'default', '普通用户权限', '1559734217', '1559734218');
COMMIT;

-- ----------------------------
-- Table structure for privileges
-- ----------------------------
CREATE TABLE `privileges` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id must be unique',
  `role_uuid` varchar(50) NOT NULL DEFAULT '' COMMENT 'role uuid',
  `module_id` bigint(20) unsigned NOT NULL COMMENT 'modules table id',
  `cmd_list` varchar(100) NOT NULL DEFAULT '' COMMENT 'list of operate type(view,edit,export,import....)',
  PRIMARY KEY (`id`),
  KEY `idx_role_uuid` (`role_uuid`),
  KEY `idx_module_id` (`module_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COMMENT='role privileges mapping';

-- ----------------------------
-- Records of privileges
-- ----------------------------
BEGIN;
INSERT INTO `privileges` (`id`, `role_uuid`, `module_id`, `cmd_list`) VALUES ('1', 'b85218cb15594235bd3b9fac98630dcb', '1', 'view');
INSERT INTO `privileges` (`id`, `role_uuid`, `module_id`, `cmd_list`) VALUES ('2', 'b85218cb15594235bd3b9fac98630dcb', '2', 'view,create,edit,export,delete');
INSERT INTO `privileges` (`id`, `role_uuid`, `module_id`, `cmd_list`) VALUES ('3', 'b85218cb15594235bd3b9fac98630dcb', '3', 'view,create,edit,export,delete');
INSERT INTO `privileges` (`id`, `role_uuid`, `module_id`, `cmd_list`) VALUES ('4', 'b85218cb15594235bd3b9fac98630dcb', '4', 'view,export,play');
INSERT INTO `privileges` (`id`, `role_uuid`, `module_id`, `cmd_list`) VALUES ('5', 'b85218cb15594235bd3b9fac98630dcb', '5', 'view,create,edit,play,delete');
INSERT INTO `privileges` (`id`, `role_uuid`, `module_id`, `cmd_list`) VALUES ('6', 'b85218cb15594235bd3b9fac98630dcb', '6', 'view,create,edit,import,export,delete');
INSERT INTO `privileges` (`id`, `role_uuid`, `module_id`, `cmd_list`) VALUES ('7', 'b85218cb15594235bd3b9fac98630dcb', '7', 'view,export');
INSERT INTO `privileges` (`id`, `role_uuid`, `module_id`, `cmd_list`) VALUES ('8', '9b929934412b43129f912106f0a6c665', '1', 'view');
INSERT INTO `privileges` (`id`, `role_uuid`, `module_id`, `cmd_list`) VALUES ('9', '9b929934412b43129f912106f0a6c665', '2', 'view,create,edit,export,delete');
INSERT INTO `privileges` (`id`, `role_uuid`, `module_id`, `cmd_list`) VALUES ('10', '9b929934412b43129f912106f0a6c665', '3', 'view');
INSERT INTO `privileges` (`id`, `role_uuid`, `module_id`, `cmd_list`) VALUES ('11', '9b929934412b43129f912106f0a6c665', '4', 'view,export,play');
INSERT INTO `privileges` (`id`, `role_uuid`, `module_id`, `cmd_list`) VALUES ('12', '9b929934412b43129f912106f0a6c665', '5', 'view,play');
INSERT INTO `privileges` (`id`, `role_uuid`, `module_id`, `cmd_list`) VALUES ('13', '9b929934412b43129f912106f0a6c665', '6', 'view');
INSERT INTO `privileges` (`id`, `role_uuid`, `module_id`, `cmd_list`) VALUES ('14', '9b929934412b43129f912106f0a6c665', '7', 'view,export');
COMMIT;

-- ----------------------------
-- Table structure for modules
-- ----------------------------
CREATE TABLE `modules` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id must be unique',
  `code` varchar(50) NOT NULL DEFAULT '' COMMENT 'code of module_name',
  `module_name` varchar(30) NOT NULL DEFAULT '' COMMENT 'management backround module name',
  `cmd_list` varchar(100) NOT NULL DEFAULT '' COMMENT 'list of operate type(view,edit,export,import....)',
  `description` varchar(512) NOT NULL COMMENT 'description',
  `binding_data` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'whether the operation binds data(0:not, 1:yes)',
  PRIMARY KEY (`id`),
  KEY `idx_code` (`code`),
  KEY `idx_module_name` (`module_name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COMMENT='modules discribe';

-- ----------------------------
-- Records of modules
-- ----------------------------
BEGIN;
INSERT INTO `modules` (`id`, `code`, `module_name`, `cmd_list`, `description`, `binding_data`) VALUES ('1', 'dashboard', '数据展板', 'view', '数据展板功能', '0');
INSERT INTO `modules` (`id`, `code`, `module_name`, `cmd_list`, `description`, `binding_data`) VALUES ('2', 'task_management', '任务管理', 'view,create,edit,export,delete', '任务管理功能', '0');
INSERT INTO `modules` (`id`, `code`, `module_name`, `cmd_list`, `description`, `binding_data`) VALUES ('3', 'list_filter', '名单过滤', 'view,create,edit,export,delete', '名单过滤功能', '0');
INSERT INTO `modules` (`id`, `code`, `module_name`, `cmd_list`, `description`, `binding_data`) VALUES ('4', 'record_management', '录音管理', 'view,export,play', '录音管理功能', '0');
INSERT INTO `modules` (`id`, `code`, `module_name`, `cmd_list`, `description`, `binding_data`) VALUES ('5', 'pre_record_management', '预录音管理', 'view,create,edit,play,delete', '预录音管理功能', '0');
INSERT INTO `modules` (`id`, `code`, `module_name`, `cmd_list`, `description`, `binding_data`) VALUES ('6', 'blacklist', '黑名单', 'view,create,edit,import,export,delete', '黑名单功能', '0');
INSERT INTO `modules` (`id`, `code`, `module_name`, `cmd_list`, `description`, `binding_data`) VALUES ('7', 'report_center', '报表中心', 'view,export', '报表中心功能', '0');
COMMIT;

-- ----------------------------
-- Table structure for data_types
-- ----------------------------
CREATE TABLE `data_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id must be unique',
  `data_type` varchar(16) NOT NULL DEFAULT '' COMMENT 'ENTERPRISE，BOT，SCENARIO，USER',
  `data_sub_type` varchar(255) DEFAULT '' COMMENT '用户可以查看的数据',
  `description` varchar(512) NOT NULL COMMENT 'description',
  PRIMARY KEY (`id`),
  KEY `idx_data_type` (`data_type`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='data types';

-- ----------------------------
-- Records of data_types
-- ----------------------------
BEGIN;
INSERT INTO `data_types` VALUES (1, 'BOT', '', 'bot');
INSERT INTO `data_types` VALUES (2, 'SCENARIO', '', 'scenario');
INSERT INTO `data_types` VALUES (3, 'ENTERPRISE', '', 'enterprise');
INSERT INTO `data_types` VALUES (4, 'USER', 'ALL,SELF,SPECIFIC_USERS', 'user');
COMMIT;

-- ----------------------------
-- Table structure for data_privileges
-- ----------------------------
CREATE TABLE `data_privileges` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id must be unique',
  `role_uuid` varchar(50) NOT NULL DEFAULT '' COMMENT 'role uuid',
  `data_type_id` bigint(20) unsigned NOT NULL COMMENT 'data_types table id',
  `data_sub_type` varchar(32) DEFAULT '' COMMENT '数据权限的子类型，只有User有如all/self/specific等',
  `data_value` varchar(50) DEFAULT ''  COMMENT 'data value',
  PRIMARY KEY (`id`),
  KEY `idx_role_uuid` (`role_uuid`),
  KEY `idx_data_type_id` (`data_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COMMENT='data privileges';

INSERT INTO `data_privileges` (`id`, `role_uuid`, `data_type_id`, `data_sub_type`, `data_value`) VALUES ('1', 'b85218cb15594235bd3b9fac98630dcb', '4', 'ALL', '');
INSERT INTO `data_privileges` (`id`, `role_uuid`, `data_type_id`, `data_sub_type`, `data_value`) VALUES ('2', '9b929934412b43129f912106f0a6c665', '4', 'SELF', '');

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `user_roles`;
DROP TABLE `roles`;
DROP TABLE `privileges`;
DROP TABLE `modules`;
DROP TABLE `data_types`;
DROP TABLE `data_privileges`;
