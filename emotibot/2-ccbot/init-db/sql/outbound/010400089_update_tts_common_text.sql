-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `outbound`.`tts_common_text` DROP INDEX `udx_uuid_text`;
ALTER TABLE `outbound`.`tts_common_text` DROP COLUMN `uuid`;
ALTER TABLE `outbound`.`tts_common_text` ADD COLUMN `tts_common_id` bigint(11) NOT NULL COMMENT 'tts_common 主键' AFTER `id`;
ALTER TABLE `outbound`.`tts_common_text` ADD COLUMN `expired` tinyint(4) NULL DEFAULT 0 COMMENT '数据是否过期 0: 未过期，1:过期' AFTER `duration`;
ALTER TABLE `outbound`.`tts_common_text` ADD COLUMN `retry_times` tinyint(4) NULL DEFAULT 0 COMMENT '失败重试次数' AFTER `expired`;
ALTER TABLE `outbound`.`tts_common_text` ADD UNIQUE INDEX `udx_id_text`(`tts_common_id`, `text_md5`) USING BTREE;
ALTER TABLE `outbound`.`tts_common_text` ADD INDEX `idx_retry_times`(`retry_times`) USING BTREE;
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
