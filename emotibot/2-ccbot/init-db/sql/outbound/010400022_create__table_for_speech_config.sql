-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for speech_config
-- ----------------------------

CREATE TABLE `speech_config` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'unique id of each row',
  `enterprise_id` varchar(32) NOT NULL DEFAULT '',
  `app_id` varchar(32) NOT NULL DEFAULT '',
  `scenario_id` varchar(64) NOT NULL DEFAULT '',
  `config_type` varchar(32) NOT NULL DEFAULT '',
  `config_value` varchar(512) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `speech_config`;