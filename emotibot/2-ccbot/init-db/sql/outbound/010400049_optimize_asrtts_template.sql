-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
UPDATE `outbound`.`fs_config` SET `xml_content` = '<include>
  <!-- UniMRCP Server MRCPv2 -->
  <profile name="emotibot" version="2">
    <param name="server-ip" value="$${local_ip_v4}"/>
    <param name="server-port" value="8060"/>

    <param name="client-ip" value="$${local_ip_v4}"/>
    <param name="client-port" value="8062"/>
    <param name="sip-transport" value="tcp"/>

    <!--param name="rtp-ext-ip" value="auto"/-->
    <param name="rtp-ip" value="$${local_ip_v4}"/>
    <param name="rtp-port-min" value="12001"/>
    <param name="rtp-port-max" value="13000"/>
    <!--param name="playout-delay" value="50"/-->
    <!--param name="max-playout-delay" value="200"/-->
    <!--param name="ptime" value="20"/-->
    <param name="codecs" value="PCMU PCMA L16/96/8000 telephone-event/101/8000 PCMU/97/16000 PCMA/98/16000 L16/99/16000 telephone-event/102/16000"/>

    <!-- Add any default MRCP params for SPEAK requests here -->
    <synthparams>
          <param name="te-path" value="auto"/>
          <param name="faq-path" value="auto"/>
          <!--  请修改以下大写名称所设定的参数  -->
          <param name="tts-param" value="vendor:VENDOR_NAME|ip:TTS_IP|port:TTS_PORT|voice:TTS_VOICER|speed:VOICE_SPEED|access-token:default|app-key:default|sample-rate:8000|format:pcm|volume:50"/>
    </synthparams>

     <!-- Add any default MRCP params for RECOGNIZE requests here -->
    <recogparams>
      <param name="speech-language" value="en-emotibot"/>
      <param name="session-id" value="auto"/>
      <param name="start-input-timers" value="true"/>
    </recogparams>

  </profile>
</include>
' WHERE `enterprise_id`='template' and `conf_type`='TTS' and `sub_type`='EMOTIBOT_TTS';

UPDATE `outbound`.`fs_config` SET `xml_content` = '<include>
  <!-- UniMRCP Server MRCPv2 -->
  <profile name="alibaba_tts" version="2">
    <param name="server-ip" value="请替换为阿里巴巴TTSMRCP服务器的IP"/>
    <param name="server-port" value="7010"/>

    <param name="client-ip" value="$${local_ip_v4}"/>
    <param name="client-port" value="8097"/>
    <param name="sip-transport" value="tcp"/>

    <!--param name="rtp-ext-ip" value="auto"/-->
    <param name="rtp-ip" value="$${local_ip_v4}"/>
    <param name="rtp-port-min" value="12001"/>
    <param name="rtp-port-max" value="13000"/>
    <!--param name="playout-delay" value="50"/-->
    <!--param name="max-playout-delay" value="200"/-->
    <!--param name="ptime" value="20"/-->
    <param name="codecs" value="PCMU PCMA L16/96/8000 telephone-event/101/8000 PCMU/97/16000 PCMA/98/16000 L16/99/16000 telephone-event/102/16000"/>

    <!-- Add any default MRCP params for SPEAK requests here -->
    <synthparams>
      <voice value="siqi"/>
      <param name="te-path" value="auto"/>
      <param name="faq-path" value="auto"/>
    </synthparams>
  </profile>
</include>
' WHERE `enterprise_id`='template' and `conf_type`='TTS' and `sub_type`='ALIBABA_TTS';

UPDATE `outbound`.`fs_config` SET `xml_content` = '<include>
<profile name="iflytek-tts" version="2">
    <param name="client-ext-ip" value="$${local_ip_v4}"/>
    <param name="client-ip" value="$${local_ip_v4}"/>
    <param name="client-port" value="8091"/>

    <param name="server-ip" value="请替换为科大讯飞TTSMRCP服务器的IP"/>
    <param name="server-port" value="5060"/>
    <!--param name="force-destination" value="1"/-->
    <param name="sip-transport" value="tcp"/>
    <param name="ua-name" value="TVP"/>
    <param name="sdp-origin" value="TVP"/>
    <!--
    <param name="rtp-ext-ip" value="auto"/>
    -->
    <param name="rtp-ip" value="$${local_ip_v4}"/>
    <param name="rtp-port-min" value="12001"/>
    <param name="rtp-port-max" value="13000"/>
<!-- enable/disable rtcp support -->
    <param name="rtcp" value="1"/>
<!-- rtcp bye policies (rtcp must be enabled first)
     0 - disable rtcp bye
     1 - send rtcp bye at the end of session
     2 - send rtcp bye also at the end of each talkspurt (input)
-->
    <param name="rtcp-bye" value="2"/>
    <!-- rtcp transmission interval in msec (set 0 to disable) -->
    <param name="rtcp-tx-interval" value="5000"/>
    <!-- period (timeout) to check for new rtcp messages in msec (set 0 to disable) -->
    <param name="rtcp-rx-resolution" value="1000"/>
    <!--param name="playout-delay" value="50"/-->
    <!--param name="max-playout-delay" value="200"/-->
    <param name="ptime" value="20"/>
    <param name="codecs" value="PCMA PCMU L16/96/8000"/>
    <param name="jsgf-mime-type" value="application/jsgf"/>
    <!-- Add any default MRCP params for SPEAK requests here -->
    <synthparams>
        <voice value="xiaoyan"/>
        <param name="te-path" value="auto"/>
        <param name="faq-path" value="auto"/>
    </synthparams>
  </profile>
</include>
' WHERE `enterprise_id`='template' and `conf_type`='TTS' and `sub_type`='IFLYTEK_TTS';


UPDATE `outbound`.`fs_config` SET `xml_content` = '<include>
  <!-- UniMRCP Server MRCPv2 -->
  <profile name="emotibot" version="2">
    <param name="server-ip" value="$${local_ip_v4}"/>
    <param name="server-port" value="8060"/>

    <param name="client-ip" value="$${local_ip_v4}"/>
    <param name="client-port" value="8062"/>
    <param name="sip-transport" value="tcp"/>

    <!--param name="rtp-ext-ip" value="auto"/-->
    <param name="rtp-ip" value="$${local_ip_v4}"/>
    <param name="rtp-port-min" value="12001"/>
    <param name="rtp-port-max" value="13000"/>
    <!--param name="playout-delay" value="50"/-->
    <!--param name="max-playout-delay" value="200"/-->
    <!--param name="ptime" value="20"/-->
    <param name="codecs" value="PCMU PCMA L16/96/8000 telephone-event/101/8000 PCMU/97/16000 PCMA/98/16000 L16/99/16000 telephone-event/102/16000"/>

    <!-- Add any default MRCP params for RECOGNIZE requests here -->
    <recogparams>
      <param name="speech-language" value="en-emotibot"/>
      <param name="session-id" value="auto"/>
      <param name="start-input-timers" value="true"/>
    </recogparams>
  </profile>
</include>
' WHERE `enterprise_id`='template' and `conf_type`='ASR' and `sub_type`='EMOTIBOT_ASR';

UPDATE `outbound`.`fs_config` SET `xml_content` = '<include>
<profile name="iflytek-asr" version="2">
    <param name="client-ext-ip" value="$${local_ip_v4}"/>
    <param name="client-ip" value="$${local_ip_v4}"/>
    <param name="client-port" value="8090"/>

    <param name="server-ip" value="请替换为科大讯飞ASRMRCP服务器的IP"/>
    <param name="server-port" value="5060"/>
    <!--param name="force-destination" value="1"/-->
    <param name="sip-transport" value="tcp"/>
    <param name="ua-name" value="TVP"/>
    <param name="sdp-origin" value="TVP"/>
    <!--
    <param name="rtp-ext-ip" value="auto"/>
    -->
    <param name="rtp-ip" value="$${local_ip_v4}"/>
    <param name="rtp-port-min" value="12001"/>
    <param name="rtp-port-max" value="13000"/>
<!-- enable/disable rtcp support -->
    <param name="rtcp" value="1"/>
<!-- rtcp bye policies (rtcp must be enabled first)
     0 - disable rtcp bye
     1 - send rtcp bye at the end of session
     2 - send rtcp bye also at the end of each talkspurt (input)
-->
    <param name="rtcp-bye" value="2"/>
    <!-- rtcp transmission interval in msec (set 0 to disable) -->
    <param name="rtcp-tx-interval" value="5000"/>
    <!-- period (timeout) to check for new rtcp messages in msec (set 0 to disable) -->
    <param name="rtcp-rx-resolution" value="1000"/>
    <!--param name="playout-delay" value="50"/-->
    <!--param name="max-playout-delay" value="200"/-->
    <param name="ptime" value="20"/>
    <param name="codecs" value="PCMA PCMU L16/96/8000"/>
    <param name="jsgf-mime-type" value="application/jsgf"/>

    <!-- Add any default MRCP params for RECOGNIZE requests here -->
    <recogparams>
      <grammar value="ahlt_ats"/>
      <param name="N-Best-List-Length" value="1"/>
      <param name="Recognition-Timeout" value="100000"/>
      <param name="Speech-Complete-Timeout" value="2000"/>
      <param name="Confidence-Threshold" value="0.01"/>
      <param name="Start-Input-Timers" value="true"/>
      <param name="Recognition-Mode" value="normal"/>
      <param name="Speech-language" value="zh-CN"/>
    </recogparams>
  </profile>
</include>
' WHERE `enterprise_id`='template' and `conf_type`='ASR' and `sub_type`='IFLYTEK_ASR';

UPDATE `outbound`.`fs_config` SET `xml_content` = '<include>
  <!-- UniMRCP Server MRCPv2 -->
  <profile name="alibaba_asr" version="2">
    <param name="server-ip" value="请替换为阿里巴巴ASRMRCP服务器的IP"/>
    <param name="server-port" value="7010"/>

    <param name="client-ip" value="$${local_ip_v4}"/>
    <param name="client-port" value="8098"/>
    <param name="sip-transport" value="tcp"/>

    <!--param name="rtp-ext-ip" value="auto"/-->
    <param name="rtp-ip" value="$${local_ip_v4}"/>
    <param name="rtp-port-min" value="12001"/>
    <param name="rtp-port-max" value="13000"/>
    <!--param name="playout-delay" value="50"/-->
    <!--param name="max-playout-delay" value="200"/-->
    <!--param name="ptime" value="20"/-->
    <param name="codecs" value="PCMU PCMA L16/96/8000 telephone-event/101/8000 PCMU/97/16000 PCMA/98/16000 L16/99/16000 telephone-event/102/16000"/>

    <!-- Add any default MRCP params for RECOGNIZE requests here -->
    <recogparams>
      <param name="start-input-timers" value="true"/>
      <param name="speech-complete-timeout" value="1300"/>
    </recogparams>
  </profile>
</include>
' WHERE `enterprise_id`='template' and `conf_type`='ASR' and `sub_type`='ALIBABA_ASR';

UPDATE `outbound`.`fs_config` SET `xml_content` = '
<configuration name="unimrcp.conf" description="UniMRCP Client">
  <settings>
    <!-- UniMRCP profile to use for TTS -->
    <!-- 请将emotibot替换为您想使用TTS的配置文件名-->
    <param name="default-tts-profile" value="emotibot"/>
    <!-- UniMRCP profile to use for ASR -->
    <!-- 请将alibaba_asr替换为您想默认使用ASR的配置文件名-->
    <param name="default-asr-profile" value="alibaba_asr"/>
    <!-- UniMRCP logging level to appear in freeswitch.log.  Options are:
         EMERGENCY|ALERT|CRITICAL|ERROR|WARNING|NOTICE|INFO|DEBUG -->
    <param name="log-level" value="DEBUG"/>
    <!-- Enable events for profile creation, open, and close -->
    <param name="enable-profile-events" value="false"/>

    <param name="max-connection-count" value="300"/>
    <param name="offer-new-connection" value="1"/>
    <param name="request-timeout" value="5000"/>
  </settings>

  <profiles>
    <X-PRE-PROCESS cmd="include" data="../mrcp_profiles/*.xml"/>
  </profiles>

</configuration>' WHERE `enterprise_id`='template' and `conf_type`='UNIMRCP';


INSERT INTO `outbound`.`fs_config` (`conf_uuid`, `enterprise_id`, `conf_type`, `conf_name`, `xml_content`, `enable`, `authorized_num`, `gw_add_zero`) VALUES ('a6d20a171ed5437f85226bd4d2cc26bd', 'global_enterprise_id', 'EXTENSION', '1000', '<include>
  <user id="1000">
    <params>
      <param name="password" value="$${default_password}"/>
      <param name="vm-password" value="1000"/>
    </params>
    <variables>
      <variable name="toll_allow" value="domestic,international,local"/>
      <variable name="accountcode" value="1000"/>
      <variable name="user_context" value="default"/>
      <variable name="effective_caller_id_name" value="Extension 1000"/>
      <variable name="effective_caller_id_number" value="1000"/>
      <variable name="outbound_caller_id_name" value="$${outbound_caller_name}"/>
      <variable name="outbound_caller_id_number" value="$${outbound_caller_id}"/>
      <variable name="callgroup" value="techsupport"/>
    </variables>
  </user>
</include>', '0', '0', '0');

INSERT INTO `outbound`.`fs_config` (`conf_uuid`, `enterprise_id`, `conf_type`, `conf_name`, `xml_content`, `enable`, `authorized_num`, `gw_add_zero`) VALUES ('ae5db95a095b4e2299ec11bfd2d25a41', 'global_enterprise_id', 'EXTENSION', '1001', '<include>
  <user id="1001">
    <params>
      <param name="password" value="$${default_password}"/>
      <param name="vm-password" value="1001"/>
    </params>
    <variables>
      <variable name="toll_allow" value="domestic,international,local"/>
      <variable name="accountcode" value="1001"/>
      <variable name="user_context" value="default"/>
      <variable name="effective_caller_id_name" value="Extension 1001"/>
      <variable name="effective_caller_id_number" value="1001"/>
      <variable name="outbound_caller_id_name" value="$${outbound_caller_name}"/>
      <variable name="outbound_caller_id_number" value="$${outbound_caller_id}"/>
      <variable name="callgroup" value="techsupport"/>
    </variables>
  </user>
</include>', '0', '0', '0');

INSERT INTO `outbound`.`fs_config` (`conf_uuid`, `enterprise_id`, `conf_type`, `conf_name`, `xml_content`, `enable`, `authorized_num`, `gw_add_zero`) VALUES ('e38b03cc765f4cfa95ccfe6ee19d7cf9', 'global_enterprise_id', 'EXTENSION', '1002', '<include>
  <user id="1002">
    <params>
      <param name="password" value="$${default_password}"/>
      <param name="vm-password" value="1002"/>
    </params>
    <variables>
      <variable name="toll_allow" value="domestic,international,local"/>
      <variable name="accountcode" value="1002"/>
      <variable name="user_context" value="default"/>
      <variable name="effective_caller_id_name" value="Extension 1002"/>
      <variable name="effective_caller_id_number" value="1002"/>
      <variable name="outbound_caller_id_name" value="$${outbound_caller_name}"/>
      <variable name="outbound_caller_id_number" value="$${outbound_caller_id}"/>
      <variable name="callgroup" value="techsupport"/>
    </variables>
  </user>
</include>', '0', '0', '0');


INSERT INTO `outbound`.`fs_config` (`conf_uuid`, `enterprise_id`, `conf_type`, `conf_name`, `xml_content`, `enable`, `authorized_num`, `gw_add_zero`) VALUES ('4f6462092089452c97e23e382cc9a5f5', 'global_enterprise_id', 'EXTENSION', '1003', '<include>
  <user id="1003">
    <params>
      <param name="password" value="$${default_password}"/>
      <param name="vm-password" value="1003"/>
    </params>
    <variables>
      <variable name="toll_allow" value="domestic,international,local"/>
      <variable name="accountcode" value="1003"/>
      <variable name="user_context" value="default"/>
      <variable name="effective_caller_id_name" value="Extension 1003"/>
      <variable name="effective_caller_id_number" value="1003"/>
      <variable name="outbound_caller_id_name" value="$${outbound_caller_name}"/>
      <variable name="outbound_caller_id_number" value="$${outbound_caller_id}"/>
      <variable name="callgroup" value="techsupport"/>
    </variables>
  </user>
</include>', '0', '0', '0');


INSERT INTO `outbound`.`fs_config` (`conf_uuid`, `enterprise_id`, `conf_type`, `conf_name`, `xml_content`, `enable`, `authorized_num`, `gw_add_zero`) VALUES ('3b1b3ea90d01428b8ccad2a34fbd009f', 'global_enterprise_id', 'EXTENSION', '1004', '<include>
  <user id="1004">
    <params>
      <param name="password" value="$${default_password}"/>
      <param name="vm-password" value="1004"/>
    </params>
    <variables>
      <variable name="toll_allow" value="domestic,international,local"/>
      <variable name="accountcode" value="1004"/>
      <variable name="user_context" value="default"/>
      <variable name="effective_caller_id_name" value="Extension 1004"/>
      <variable name="effective_caller_id_number" value="1004"/>
      <variable name="outbound_caller_id_name" value="$${outbound_caller_name}"/>
      <variable name="outbound_caller_id_number" value="$${outbound_caller_id}"/>
      <variable name="callgroup" value="techsupport"/>
    </variables>
  </user>
</include>', '0', '0', '0');

INSERT INTO `outbound`.`fs_config` (`conf_uuid`, `enterprise_id`, `conf_type`, `conf_name`, `xml_content`, `enable`, `authorized_num`, `gw_add_zero`) VALUES ('851866d0e9334a15acec580c3f50e028', 'global_enterprise_id', 'EXTENSION', '1005', '<include>
  <user id="1005">
    <params>
      <param name="password" value="$${default_password}"/>
      <param name="vm-password" value="1005"/>
    </params>
    <variables>
      <variable name="toll_allow" value="domestic,international,local"/>
      <variable name="accountcode" value="1005"/>
      <variable name="user_context" value="default"/>
      <variable name="effective_caller_id_name" value="Extension 1005"/>
      <variable name="effective_caller_id_number" value="1005"/>
      <variable name="outbound_caller_id_name" value="$${outbound_caller_name}"/>
      <variable name="outbound_caller_id_number" value="$${outbound_caller_id}"/>
      <variable name="callgroup" value="techsupport"/>
    </variables>
  </user>
</include>', '0', '0', '0');

INSERT INTO `outbound`.`fs_config` (`conf_uuid`, `enterprise_id`, `conf_type`, `conf_name`, `xml_content`, `enable`, `authorized_num`, `gw_add_zero`) VALUES ('0090f965dadf488a8220c11d923956d7', 'global_enterprise_id', 'EXTENSION', '1006', '<include>
  <user id="1006">
    <params>
      <param name="password" value="$${default_password}"/>
      <param name="vm-password" value="1006"/>
    </params>
    <variables>
      <variable name="toll_allow" value="domestic,international,local"/>
      <variable name="accountcode" value="1006"/>
      <variable name="user_context" value="default"/>
      <variable name="effective_caller_id_name" value="Extension 1006"/>
      <variable name="effective_caller_id_number" value="1006"/>
      <variable name="outbound_caller_id_name" value="$${outbound_caller_name}"/>
      <variable name="outbound_caller_id_number" value="$${outbound_caller_id}"/>
      <variable name="callgroup" value="techsupport"/>
    </variables>
  </user>
</include>', '0', '0', '0');

INSERT INTO `outbound`.`fs_config` (`conf_uuid`, `enterprise_id`, `conf_type`, `conf_name`, `xml_content`, `enable`, `authorized_num`, `gw_add_zero`) VALUES ('b6ac581518e44fbc85e996b65535bbbe', 'global_enterprise_id', 'EXTENSION', '1007', '<include>
  <user id="1007">
    <params>
      <param name="password" value="$${default_password}"/>
      <param name="vm-password" value="1007"/>
    </params>
    <variables>
      <variable name="toll_allow" value="domestic,international,local"/>
      <variable name="accountcode" value="1007"/>
      <variable name="user_context" value="default"/>
      <variable name="effective_caller_id_name" value="Extension 1007"/>
      <variable name="effective_caller_id_number" value="1007"/>
      <variable name="outbound_caller_id_name" value="$${outbound_caller_name}"/>
      <variable name="outbound_caller_id_number" value="$${outbound_caller_id}"/>
      <variable name="callgroup" value="techsupport"/>
    </variables>
  </user>
</include>', '0', '0', '0');

INSERT INTO `outbound`.`fs_config` (`conf_uuid`, `enterprise_id`, `conf_type`, `conf_name`, `xml_content`, `enable`, `authorized_num`, `gw_add_zero`) VALUES ('f2e570e7b6a5455b895aae5ae112fbfc', 'global_enterprise_id', 'EXTENSION', '1008', '<include>
  <user id="1008">
    <params>
      <param name="password" value="$${default_password}"/>
      <param name="vm-password" value="1008"/>
    </params>
    <variables>
      <variable name="toll_allow" value="domestic,international,local"/>
      <variable name="accountcode" value="1008"/>
      <variable name="user_context" value="default"/>
      <variable name="effective_caller_id_name" value="Extension 1008"/>
      <variable name="effective_caller_id_number" value="1008"/>
      <variable name="outbound_caller_id_name" value="$${outbound_caller_name}"/>
      <variable name="outbound_caller_id_number" value="$${outbound_caller_id}"/>
      <variable name="callgroup" value="techsupport"/>
    </variables>
  </user>
</include>', '0', '0', '0');

INSERT INTO `outbound`.`fs_config` (`conf_uuid`, `enterprise_id`, `conf_type`, `conf_name`, `xml_content`, `enable`, `authorized_num`, `gw_add_zero`) VALUES ('ba1fa1580b574c47a90249466a95619b', 'global_enterprise_id', 'EXTENSION', '1009', '<include>
  <user id="1009">
    <params>
      <param name="password" value="$${default_password}"/>
      <param name="vm-password" value="1009"/>
    </params>
    <variables>
      <variable name="toll_allow" value="domestic,international,local"/>
      <variable name="accountcode" value="1009"/>
      <variable name="user_context" value="default"/>
      <variable name="effective_caller_id_name" value="Extension 1009"/>
      <variable name="effective_caller_id_number" value="1009"/>
      <variable name="outbound_caller_id_name" value="$${outbound_caller_name}"/>
      <variable name="outbound_caller_id_number" value="$${outbound_caller_id}"/>
      <variable name="callgroup" value="techsupport"/>
    </variables>
  </user>
</include>', '0', '0', '0');

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
