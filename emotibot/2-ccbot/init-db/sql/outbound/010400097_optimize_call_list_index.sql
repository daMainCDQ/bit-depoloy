-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `outbound`.`call_list` DROP INDEX `id_UNIQUE`;
ALTER TABLE `outbound`.`call_list` DROP INDEX `try_count_idx`;
ALTER TABLE `outbound`.`call_list` DROP INDEX `status_idx`;
ALTER TABLE `outbound`.`call_list` DROP INDEX `last_update_time_idx`;
ALTER TABLE `outbound`.`call_list` ADD INDEX `tts_call_idx` (`task_id`,`status`,`tts_update_time`) USING BTREE;
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back