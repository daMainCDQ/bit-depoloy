-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `outbound`.`tts_session_global_variable` ADD COLUMN `retry_times` tinyint(4) NULL DEFAULT 0 COMMENT '失败重试次数' AFTER `duration`;
ALTER TABLE `outbound`.`tts_session_global_variable` ADD INDEX `idx_retry_times`(`retry_times`) USING BTREE;
ALTER TABLE `outbound`.`tts_session_global_variable` DROP INDEX `idx_pgi`;
ALTER TABLE `outbound`.`tts_session_global_variable` DROP INDEX `idx_union`;
ALTER TABLE `outbound`.`tts_session_global_variable` MODIFY COLUMN `key_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'global变量的键' AFTER `scenario_id`;
ALTER TABLE `outbound`.`tts_session_global_variable` ADD COLUMN `key_text_md5` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'key_text md5' AFTER `value_text`;
UPDATE `outbound`.`tts_session_global_variable` SET key_text_md5 = MD5(key_text);
ALTER TABLE `outbound`.`tts_session_global_variable` ADD UNIQUE INDEX `udx_pgi_text`(`pre_generate_id`, `key_text_md5`) USING BTREE;
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back