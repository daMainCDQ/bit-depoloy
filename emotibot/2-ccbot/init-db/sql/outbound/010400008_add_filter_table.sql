-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for license_enterprise
-- ----------------------------
CREATE TABLE `filter_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(128) NOT NULL DEFAULT '' COMMENT 'name of the type',
  `condition_type` smallint(1) unsigned zerofill NOT NULL COMMENT '0 for sql 1 for regex',
  `value_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'type of the value',
  `value_enum` varchar(1024) NOT NULL DEFAULT '' COMMENT 'list of the possible value',
  `operation_enum` varchar(1024) NOT NULL COMMENT 'list of the possible operation',
  `condition_pattern` varchar(2048) NOT NULL COMMENT 'list of the possible condition pattern',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of filter_type
-- ----------------------------
BEGIN;
INSERT INTO `filter_type` VALUES (1, '拨打结果', 0, 'string', '接通@@@@未接通', '=', '{\"0\":\"=\",\"1\":\"!=\"}');
INSERT INTO `filter_type` VALUES (2, '拨打时间', 0, 'time', '', 'between', '');
INSERT INTO `filter_type` VALUES (3, '通话时长', 0, 'num', '', 'between', '');
INSERT INTO `filter_type` VALUES (4, '被叫号码', 1, '', '固定电话@@@@移动电话', '', '{\"0\":\"^(\\\\+\\\\d{2}-)?0\\\\d{2,3}-\\\\d{7,8}$\",\"1\":\"^(\\\\+\\\\d{2}-)?(\\\\d{2,3}-)?([1][3,4,5,7,8][0-9]\\\\d{8})$\"}');
COMMIT;


-- ----------------------------
-- Table structure for filter_condition
-- ----------------------------
CREATE TABLE `filter_condition` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `type_id` int(11) NOT NULL COMMENT 'type id',
  `rule_uuid` varchar(32) NOT NULL DEFAULT '' COMMENT 'rule uuid',
  `internal_id` int(11) NOT NULL COMMENT 'internal id',
  `operation_value` varchar(128) NOT NULL COMMENT 'operation value',
  `param1` varchar(2048) NOT NULL DEFAULT '' COMMENT 'param1',
  `param2` varchar(2048) NOT NULL DEFAULT '' COMMENT 'param2',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;


-- ----------------------------
-- Table structure for filter_rule
-- ----------------------------
CREATE TABLE `filter_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `rule_uuid` varchar(32) NOT NULL COMMENT 'rule uuid',
  `rule_name` varchar(255) NOT NULL DEFAULT '' COMMENT 'rule name',
  `description` varchar(512) NOT NULL COMMENT 'description',
  `condition_expression` varchar(1024) NOT NULL COMMENT 'condition expression',
  `creator` varchar(64) NOT NULL COMMENT 'creator',
  `enterprise` varchar(64) NOT NULL COMMENT 'enterprise',
  `status` smallint(1) NOT NULL COMMENT 'status',
  `create_time` int(11) NOT NULL COMMENT 'create time',
  `last_update_time` int(11) NOT NULL COMMENT 'update time',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `filter_type`;
DROP TABLE `filter_condition`;
DROP TABLE `filter_rule`;





