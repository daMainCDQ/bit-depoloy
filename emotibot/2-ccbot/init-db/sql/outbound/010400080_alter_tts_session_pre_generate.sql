-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `outbound`.`tts_session_pre_generate` ADD COLUMN `uuid` varchar(50) NOT NULL COMMENT '唯一id' AFTER `phone`;
ALTER TABLE `outbound`.`tts_session_pre_generate` DROP INDEX `udx_union`;
ALTER TABLE `outbound`.`tts_session_pre_generate` ADD INDEX `udx_union` (`enterprise_id`,`app_id`,`scenario_id`,`phone`,`uuid`);

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `outbound`.`tts_session_pre_generate` DROP INDEX `udx_union`;
ALTER TABLE `outbound`.`tts_session_pre_generate` DROP COLUMN `uuid`;
ALTER TABLE `outbound`.`tts_session_pre_generate` ADD INDEX `udx_union` (`enterprise_id`,`app_id`,`scenario_id`,`phone`);
