-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
UPDATE `outbound`.`fs_config` SET `xml_content` = '<include>
<profile name="iflytek-asr" version="2">
    <param name="client-ext-ip" value="$${local_ip_v4}"/>
    <param name="client-ip" value="$${local_ip_v4}"/>
    <param name="client-port" value="8090"/>

    <param name="server-ip" value="请替换为科大讯飞ASRMRCP服务器的IP"/>
    <param name="server-port" value="5060"/>
    <!--param name="force-destination" value="1"/-->
    <param name="sip-transport" value="tcp"/>
    <param name="ua-name" value="TVP"/>
    <param name="sdp-origin" value="TVP"/>
    <!--
    <param name="rtp-ext-ip" value="auto"/>
    -->
    <param name="rtp-ip" value="$${local_ip_v4}"/>
    <param name="rtp-port-min" value="12001"/>
    <param name="rtp-port-max" value="13000"/>
<!-- enable/disable rtcp support -->
    <param name="rtcp" value="1"/>
<!-- rtcp bye policies (rtcp must be enabled first)
     0 - disable rtcp bye
     1 - send rtcp bye at the end of session
     2 - send rtcp bye also at the end of each talkspurt (input)
-->
    <param name="rtcp-bye" value="2"/>
    <!-- rtcp transmission interval in msec (set 0 to disable) -->
    <param name="rtcp-tx-interval" value="5000"/>
    <!-- period (timeout) to check for new rtcp messages in msec (set 0 to disable) -->
    <param name="rtcp-rx-resolution" value="1000"/>
    <!--param name="playout-delay" value="50"/-->
    <!--param name="max-playout-delay" value="200"/-->
    <param name="ptime" value="20"/>
    <param name="codecs" value="PCMA PCMU L16/96/8000"/>
    <param name="jsgf-mime-type" value="application/jsgf"/>

    <!-- Add any default MRCP params for RECOGNIZE requests here -->
    <recogparams>
      <grammar value="ahlt_ats"/>
      <param name="N-Best-List-Length" value="1"/>
      <param name="Recognition-Timeout" value="100000"/>
      <param name="Speech-Complete-Timeout" value="400"/>
      <param name="Confidence-Threshold" value="0.01"/>
      <param name="Start-Input-Timers" value="true"/>
      <param name="Recognition-Mode" value="normal"/>
      <param name="Speech-language" value="zh-CN"/>
    </recogparams>
  </profile>
</include>
' WHERE `enterprise_id`='template' and `conf_type`='ASR' and `sub_type`='IFLYTEK_ASR';

UPDATE `outbound`.`fs_config` SET `xml_content` = '<include>
  <!-- UniMRCP Server MRCPv2 -->
  <profile name="alibaba_asr" version="2">
    <param name="server-ip" value="请替换为阿里巴巴ASRMRCP服务器的IP"/>
    <param name="server-port" value="7010"/>

    <param name="client-ip" value="$${local_ip_v4}"/>
    <param name="client-port" value="8098"/>
    <param name="sip-transport" value="tcp"/>

    <!--param name="rtp-ext-ip" value="auto"/-->
    <param name="rtp-ip" value="$${local_ip_v4}"/>
    <param name="rtp-port-min" value="12001"/>
    <param name="rtp-port-max" value="13000"/>
    <!--param name="playout-delay" value="50"/-->
    <!--param name="max-playout-delay" value="200"/-->
    <!--param name="ptime" value="20"/-->
    <param name="codecs" value="PCMU PCMA L16/96/8000 telephone-event/101/8000 PCMU/97/16000 PCMA/98/16000 L16/99/16000 telephone-event/102/16000"/>

    <!-- Add any default MRCP params for RECOGNIZE requests here -->
    <recogparams>
      <param name="start-input-timers" value="true"/>
      <param name="speech-complete-timeout" value="400"/>
    </recogparams>
  </profile>
</include>
' WHERE `enterprise_id`='template' and `conf_type`='ASR' and `sub_type`='ALIBABA_ASR';

UPDATE `outbound`.`fs_config` SET `xml_content` = '<include>
  <!-- UniMRCP Server MRCPv2 -->
  <profile name="emotibot" version="2">
    <param name="server-ip" value="请替换为竹间智能TTS MRCP服务器的IP"/>
    <param name="server-port" value="8060"/>

    <param name="client-ip" value="$${local_ip_v4}"/>
    <param name="client-port" value="8062"/>
    <param name="sip-transport" value="tcp"/>

    <!--param name="rtp-ext-ip" value="auto"/-->
    <param name="rtp-ip" value="$${local_ip_v4}"/>
    <param name="rtp-port-min" value="12001"/>
    <param name="rtp-port-max" value="13000"/>
    <!--param name="playout-delay" value="50"/-->
    <!--param name="max-playout-delay" value="200"/-->
    <!--param name="ptime" value="20"/-->
    <param name="codecs" value="PCMU PCMA L16/96/8000 telephone-event/101/8000 PCMU/97/16000 PCMA/98/16000 L16/99/16000 telephone-event/102/16000"/>

    <!-- Add any default MRCP params for SPEAK requests here -->
    <synthparams>
          <param name="te-path" value="auto"/>
          <param name="faq-path" value="auto"/>
          <!-- 请修改以下大写名称所设定的参数，可参考以下阿里和竹间的模板设定-->
          <!-- 阿里语者可选aixia、siqi，语速-500~500，音量0~100 -->
          <!-- param name="tts-param" value="vendor:alibaba|ip:TTS_IP|port:8101|voice:aixia|speed:-200|access-token:default|app-key:default|sample-rate:8000|format:pcm|volume:50"/-->
          <!-- 竹间语速0.5~1.5，音量0.1~2 -->
          <!-- param name="tts-param" value="vendor:emotibot|ip:TTS_IP|port:8100|user-id:speech|speed:1.1|lang:zh-cn|sample-rate:8000"/-->
          <param name="tts-param" value="vendor:VENDOR_NAME|ip:TTS_IP|port:TTS_PORT|voice:TTS_VOICER|speed:VOICE_SPEED|pitch:0|access-token:default|app-key:default|sample-rate:8000|format:pcm|volume:50"/>
    </synthparams>
  </profile>
</include>
' WHERE `enterprise_id`='template' and `conf_type`='TTS' and `sub_type`='EMOTIBOT_TTS';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
