
-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for tts_session_pre_generate
-- ----------------------------
CREATE TABLE `tts_session_pre_generate` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `enterprise_id` varchar(50) NOT NULL COMMENT '企业id',
  `app_id` varchar(50) NOT NULL COMMENT '机器人id',
  `scenario_id` varchar(50) NOT NULL COMMENT '场景id',
  `phone` varchar(20) NOT NULL COMMENT '电话',
  `text_cnt` int(11) NOT NULL COMMENT 'text列表总数',
  `finished_text_cnt` int(11) NOT NULL COMMENT '已完成的text列表数',
  `call_id` varchar(100) NOT NULL COMMENT '请求参数，用于回调',
  `tts_ready_callback_url` varchar(100) NOT NULL COMMENT '预生成完成后的回调url',
  `status` tinyint(4) NOT NULL COMMENT '0:未完成，1:已完成',
  `is_call_finished` tinyint(4) NOT NULL COMMENT '该通电话是否已结束，0：否，1：是',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `udx_union` (`enterprise_id`,`app_id`,`scenario_id`,`phone`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='tts会话预生成表';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `tts_session_pre_generate`;