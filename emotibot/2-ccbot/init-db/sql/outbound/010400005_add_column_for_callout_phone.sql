-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `callout_phone` ADD COLUMN `longdistance_prefix` varchar(10) NOT NULL DEFAULT '' COMMENT 'longdistance_prefix';
ALTER TABLE `callout_phone` ADD COLUMN `outgoing_prefix` varchar(10) NOT NULL DEFAULT '' COMMENT 'outgoing_prefix';
ALTER TABLE `callout_phone` ADD COLUMN `international_prefix` varchar(10) NOT NULL DEFAULT '' COMMENT 'international prefix';
ALTER TABLE `callout_phone` ADD COLUMN `line_name` varchar(64) NOT NULL DEFAULT '' COMMENT 'line_name';
ALTER TABLE `callout_phone` ADD COLUMN `gateway_name` varchar(64) NOT NULL DEFAULT '' COMMENT 'gateway_name';
ALTER TABLE `callout_phone` ADD COLUMN `capacity` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'capacity';
ALTER TABLE `callout_phone` ADD COLUMN `description` varchar(255) NOT NULL DEFAULT '' COMMENT 'describe';


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `callout_phone` DROP COLUMN `longdistance_prefix`;
ALTER TABLE `callout_phone` DROP COLUMN `outgoing_prefix`;
ALTER TABLE `callout_phone` DROP COLUMN `international_prefix`;
ALTER TABLE `callout_phone` DROP COLUMN `line_name`;
ALTER TABLE `callout_phone` DROP COLUMN `gateway_name`;
ALTER TABLE `callout_phone` DROP COLUMN `capacity`;
ALTER TABLE `callout_phone` DROP COLUMN `description`;

