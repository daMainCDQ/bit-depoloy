-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for call_detail_record
-- ----------------------------
CREATE TABLE IF NOT EXISTS `call_detail_record` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uuid` char(32) NOT NULL COMMENT 'session id or freeswitch uuid',
  `call_id` bigint(20) NOT NULL COMMENT 'call id',
  `call_begin_time` bigint(20) NOT NULL COMMENT 'call begin time',
  `ringing_start_time` bigint(20) NOT NULL COMMENT 'ring start time',
  `ringing_end_time` bigint(20) NOT NULL COMMENT 'ring end time',
  `call_end_time` bigint(20) NOT NULL COMMENT 'call end time',
  `status` tinyint(4) NOT NULL COMMENT 'call status',
  `call_direction` varchar(20) NOT NULL COMMENT 'call direction',
  `answer_state` varchar(255) NOT NULL DEFAULT '' COMMENT 'save answer process',
  `caller_number` varchar(255) NOT NULL COMMENT 'caller number, maybe a sip address',
  `callee_number` varchar(255) NOT NULL COMMENT 'callee number',
  `data` text NOT NULL COMMENT 'extra info abount call',
  PRIMARY KEY (`id`),
  KEY `idx_all` (`call_id`,`status`,`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `call_detail_record`;