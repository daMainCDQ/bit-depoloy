-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for black_list_name
-- ----------------------------
DROP TABLE IF EXISTS `black_list_name`;
CREATE TABLE `black_list_name` (
                                 `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'unique id in big int of each row',
                                 `black_name_uuid` varchar(32)  NOT NULL COMMENT '黑名单集合id，唯一标识某个黑名单集合',
                                 `name` varchar(128)  NOT NULL COMMENT '黑名单名字',
                                 `enterprise_id` varchar(32)  NOT NULL COMMENT '名单所属企业id',
                                 `submitter_id` varchar(32)  NOT NULL COMMENT '黑名单提交者id\n黑名单提交者id\n黑名单提交者id\n',
                                 `status` tinyint(16) NOT NULL COMMENT '名单状态：0：待审核，1：审核中，2：审核完成\n',
                                 `description` varchar(256)  DEFAULT NULL COMMENT '黑名单描述信息',
                                 `apply_comments` varchar(255)  DEFAULT NULL COMMENT '申请描述信息',
                                 `audit_comments` varchar(255)  DEFAULT NULL COMMENT '审核描述信息',
                                 `create_time` datetime NOT NULL COMMENT '创建时间',
                                 `update_time` datetime NOT NULL COMMENT '更新时间',
                                 `audit_time` datetime DEFAULT NULL COMMENT '审核时间',
                                 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


-- ----------------------------
-- Table structure for black_list
-- ----------------------------
DROP TABLE IF EXISTS `black_list`;
CREATE TABLE `black_list` (
                            `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'unique id of each row',
                            `black_name_uuid` varchar(32)  NOT NULL COMMENT '黑名单集合id，唯一标识某个黑名单集合',
                            `phone_number` varchar(128)  NOT NULL COMMENT '用户手机号码',
                            `name` varchar(128)  DEFAULT NULL COMMENT '用户姓名',
                            `create_by` tinyint(10) NOT NULL COMMENT '创建者：,0：人工,1：自动\n',
                            `status` tinyint(10) NOT NULL COMMENT '名单状态：,0：待审核,1：审核拒绝,2：审核成功',
                            `description` varchar(256)  DEFAULT NULL COMMENT ' 禁呼描述信息',
                            `submitter_id` varchar(32)  NOT NULL COMMENT '黑名单提交者id',
                            `auditor_id` varchar(32)  DEFAULT NULL COMMENT '黑名单审核者id',
                            `apply_comments` varchar(256)  DEFAULT NULL COMMENT '申请描述信息\n',
                            `audit_comments` varchar(256)  DEFAULT NULL COMMENT '审核描述信息',
                            `create_time` datetime NOT NULL COMMENT '创建时间',
                            `update_time` datetime NOT NULL COMMENT '更新时间',
                            `audit_time` datetime DEFAULT NULL COMMENT '审核时间',
                            `bk` varchar(128)  DEFAULT NULL COMMENT '备注',
                            PRIMARY KEY (`id`),
                            UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ;



-- ----------------------------
-- Table structure for black_list_user
-- ----------------------------
DROP TABLE IF EXISTS `black_list_user`;
CREATE TABLE `black_list_user` (
                                 `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                                 `app_id` varchar(255) NOT NULL,
                                 `black_name_uuid` varchar(255) NOT NULL,
                                 `create_time` datetime NOT NULL,
                                 `enterprise_id` varchar(255) NOT NULL,
                                 `update_time` datetime NOT NULL,
                                 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci  ;

-- ----------------------------
-- Table structure for black_filter_record
-- ----------------------------
DROP TABLE IF EXISTS `black_filter_record`;
CREATE TABLE `black_filter_record` (
                                     `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                     `black_id` varchar(255) NOT NULL,
                                     `black_name_uuid` varchar(255) NOT NULL,
                                     `create_time` datetime NOT NULL,
                                     `task_id` varchar(255) NOT NULL,
                                     PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ;



-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `black_list_name`;
DROP TABLE `black_list`;
DROP TABLE `black_list_user`;
DROP TABLE `black_filter_record`;

