-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for pre_record_solution
-- ----------------------------

CREATE TABLE `pre_record_solution` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `solution_uuid` varchar(50) NOT NULL COMMENT '方案id',
  `solution_type` tinyint(1) NOT NULL COMMENT '方案类型，1：标准问，2：多轮场景',
  `source_type` tinyint(1) NOT NULL COMMENT '预录音来源类型，1：一键生成，2：用户上传',
  `enterprise_id` varchar(50) NOT NULL COMMENT '企业id',
  `app_id` varchar(50) NOT NULL COMMENT '机器人id',
  `scenario_id` varchar(50) DEFAULT '' COMMENT '场景id',
  `create_time` varchar(30) DEFAULT '' COMMENT '方案创建时间',
  `sync_bfop_time` varchar(30) DEFAULT '' COMMENT '方案同步时间，即和bfop同步标准问或多轮场景的时间',
  `update_time` varchar(30) DEFAULT '' COMMENT '方案录音更新时间，点击批量上传的时间，或者点击一键生成的时间',
  `text_total_cnt` int(11) DEFAULT '0' COMMENT '任务文本总数',
  `pre_record_cnt` int(11) DEFAULT '0' COMMENT '预录音文件数量',
  `tts_service_id` varchar(50) DEFAULT '' COMMENT 'TTS服务名称，根据该字段调TTS服务API获取服务相关信息',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `status` tinyint(1) DEFAULT '1' COMMENT '记录预录音生成或上传过程中的错误类型，1:初始化，2:生成中，3:完成',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uidx_enterprise_app_scenario` (`enterprise_id`,`app_id`,`scenario_id`),
  KEY `idx_solution_uuid` (`solution_uuid`) USING BTREE,
  KEY `idx_solution_type` (`solution_type`) USING BTREE,
  KEY `idx_source_type` (`source_type`) USING BTREE,
  KEY `idx_enterprise_id` (`enterprise_id`) USING BTREE,
  KEY `idx_app_id` (`app_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='预录音方案表';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `pre_record_solution`;