-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `call_record` ADD COLUMN `callee_number` VARCHAR(20) DEFAULT '' AFTER `callcenter_record_uuid`;
ALTER TABLE `call_record` ADD COLUMN `app_id` VARCHAR(64) DEFAULT '';
ALTER TABLE `call_record` ADD COLUMN `scenario_id` VARCHAR(64) DEFAULT '';
ALTER TABLE `call_record` ADD COLUMN `fs_id` VARCHAR(64) DEFAULT '';
ALTER TABLE `call_record` ADD COLUMN `enterprise_id` VARCHAR(64) DEFAULT '';
ALTER TABLE `call_record` ADD COLUMN `creator` VARCHAR(64) DEFAULT '';


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

ALTER TABLE `call_record` DROP COLUMN `callee_number`;
ALTER TABLE `call_record` DROP COLUMN `app_id`;
ALTER TABLE `call_record` DROP COLUMN `scenario_id`;
ALTER TABLE `call_record` DROP COLUMN `fs_id`;
ALTER TABLE `call_record` DROP COLUMN `enterprise_id`;
ALTER TABLE `call_record` DROP COLUMN `creator`;