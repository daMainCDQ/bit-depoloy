
-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for user_info
-- ----------------------------
CREATE TABLE `user_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `user_uuid` varchar(64) NOT NULL COMMENT 'user role uuid',
  `user_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1管理员，2是普通用户',
  `user_name` varchar(255) NOT NULL DEFAULT '' COMMENT '用户名称,登陆用',
  `organization_id` bigint(20) DEFAULT '0' COMMENT '组织ID',
  `enterprise_id` varchar(32) NOT NULL DEFAULT '' COMMENT '企业ID',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态 1启动 0禁用',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `user_info`;