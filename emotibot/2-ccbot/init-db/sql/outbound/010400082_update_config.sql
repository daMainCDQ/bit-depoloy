-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
UPDATE `outbound`.`config_options` SET `config_item` = 'emotion_detection', `item_description` = '情绪检测功能，0值禁用，1为启用' WHERE `catalog_name`='audio_detect' and `enterprise_id` = 'default' and `config_item` = 'audio_output';
INSERT INTO `outbound`.`config_options` (`enterprise_id`, `catalog_name`, `config_item`, `item_description`, `value`) VALUES ('default', 'audio_detect', 'gender_detection', '性别检测功能，0值禁用，1为启用', '0');
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
