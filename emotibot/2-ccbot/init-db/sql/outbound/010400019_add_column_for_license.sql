-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `license_enterprise` ADD COLUMN `status` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'enterprise license status' AFTER `ai_count`;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

ALTER TABLE `license_enterprise` DROP COLUMN `status`;
