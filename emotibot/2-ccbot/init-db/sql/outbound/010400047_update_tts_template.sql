-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
UPDATE `outbound`.`fs_config` SET `xml_content` = '<include>
  <!-- UniMRCP Server MRCPv2 -->
  <profile name="alibaba_tts" version="2">
    <param name="server-ip" value="请替换为阿里巴巴TTSMRCP服务器的IP"/>
    <param name="server-port" value="7010"/>

    <param name="client-ip" value="FS_HOST_IP_REPLACE"/>
    <param name="client-port" value="8097"/>
    <param name="sip-transport" value="tcp"/>

    <!--param name="rtp-ext-ip" value="auto"/-->
    <param name="rtp-ip" value="FS_HOST_IP_REPLACE"/>
    <param name="rtp-port-min" value="12001"/>
    <param name="rtp-port-max" value="13000"/>
    <!--param name="playout-delay" value="50"/-->
    <!--param name="max-playout-delay" value="200"/-->
    <!--param name="ptime" value="20"/-->
    <param name="codecs" value="PCMU PCMA L16/96/8000 telephone-event/101/8000 PCMU/97/16000 PCMA/98/16000 L16/99/16000 telephone-event/102/16000"/>

    <!-- Add any default MRCP params for SPEAK requests here -->
    <synthparams>
      <voice value="siqi"/>
      <param name="te-path" value="auto"/>
      <param name="faq-path" value="auto"/>
    </synthparams>
  </profile>
</include>
' WHERE `enterprise_id`='template' and `conf_type`='TTS' and `sub_type`='ALIBABA_TTS';

UPDATE `outbound`.`fs_config` SET `xml_content` = '<include>
<profile name="iflytek-tts" version="2">
    <param name="client-ext-ip" value="FS_HOST_IP_REPLACE"/>
    <param name="client-ip" value="FS_HOST_IP_REPLACE"/>
    <param name="client-port" value="8091"/>

    <param name="server-ip" value="请替换为科大讯飞TTSMRCP服务器的IP"/>
    <param name="server-port" value="5060"/>
    <!--param name="force-destination" value="1"/-->
    <param name="sip-transport" value="tcp"/>
    <param name="ua-name" value="TVP"/>
    <param name="sdp-origin" value="TVP"/>
    <!--
    <param name="rtp-ext-ip" value="auto"/>
    -->
    <param name="rtp-ip" value="FS_HOST_IP_REPLACE"/>
    <param name="rtp-port-min" value="12001"/>
    <param name="rtp-port-max" value="13000"/>
<!-- enable/disable rtcp support -->
    <param name="rtcp" value="1"/>
<!-- rtcp bye policies (rtcp must be enabled first)
     0 - disable rtcp bye
     1 - send rtcp bye at the end of session
     2 - send rtcp bye also at the end of each talkspurt (input)
-->
    <param name="rtcp-bye" value="2"/>
    <!-- rtcp transmission interval in msec (set 0 to disable) -->
    <param name="rtcp-tx-interval" value="5000"/>
    <!-- period (timeout) to check for new rtcp messages in msec (set 0 to disable) -->
    <param name="rtcp-rx-resolution" value="1000"/>
    <!--param name="playout-delay" value="50"/-->
    <!--param name="max-playout-delay" value="200"/-->
    <param name="ptime" value="20"/>
    <param name="codecs" value="PCMA PCMU L16/96/8000"/>
    <param name="jsgf-mime-type" value="application/jsgf"/>
    <!-- Add any default MRCP params for SPEAK requests here -->
    <synthparams>
        <voice value="xiaoyan"/>
        <param name="te-path" value="auto"/>
        <param name="faq-path" value="auto"/>
    </synthparams>
  </profile>
</include>
' WHERE `enterprise_id`='template' and `conf_type`='TTS' and `sub_type`='IFLYTEK_TTS';

UPDATE `outbound`.`fs_config` SET `xml_content` = '<include>
  <!-- UniMRCP Server MRCPv2 -->
  <profile name="emotibot" version="2">
    <param name="server-ip" value="请替换为竹间智能MRCP服务器的IP"/>
    <param name="server-port" value="8060"/>

    <param name="client-ip" value="FS_HOST_IP_REPLACE"/>
    <param name="client-port" value="8062"/>
    <param name="sip-transport" value="tcp"/>

    <!--param name="rtp-ext-ip" value="auto"/-->
    <param name="rtp-ip" value="FS_HOST_IP_REPLACE"/>
    <param name="rtp-port-min" value="12001"/>
    <param name="rtp-port-max" value="13000"/>
    <!--param name="playout-delay" value="50"/-->
    <!--param name="max-playout-delay" value="200"/-->
    <!--param name="ptime" value="20"/-->
    <param name="codecs" value="PCMU PCMA L16/96/8000 telephone-event/101/8000 PCMU/97/16000 PCMA/98/16000 L16/99/16000 telephone-event/102/16000"/>

    <!-- Add any default MRCP params for SPEAK requests here -->
    <synthparams>
          <param name="te-path" value="auto"/>
          <param name="faq-path" value="auto"/>
          <!--  PLEASE REPLACE THE UPPER CASE VARIABLE  -->
          <param name="tts-param" value="vendor:VENDOR_NAME|ip:TTS_IP|port:TTS_PORT|voice:TTS_VOICER|speed:VOICE_SPEED|access-token:default|app-key:default|sample-rate:8000|format:pcm|volume:50"/>
    </synthparams>

  </profile>
</include>
' WHERE `enterprise_id`='template' and `conf_type`='TTS' and `sub_type`='EMOTIBOT_TTS';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
UPDATE `outbound`.`fs_config` SET `xml_content` = '<include>
  <!-- UniMRCP Server MRCPv2 -->
  <profile name="alibaba_tts" version="2">
    <param name="server-ip" value="请替换为阿里巴巴TTSMRCP服务器的IP"/>
    <param name="server-port" value="7010"/>

    <param name="client-ip" value="FS_HOST_IP_REPLACE"/>
    <param name="client-port" value="8097"/>
    <param name="sip-transport" value="tcp"/>

    <!--param name="rtp-ext-ip" value="auto"/-->
    <param name="rtp-ip" value="FS_HOST_IP_REPLACE"/>
    <param name="rtp-port-min" value="12001"/>
    <param name="rtp-port-max" value="13000"/>
    <!--param name="playout-delay" value="50"/-->
    <!--param name="max-playout-delay" value="200"/-->
    <!--param name="ptime" value="20"/-->
    <param name="codecs" value="PCMU PCMA L16/96/8000 telephone-event/101/8000 PCMU/97/16000 PCMA/98/16000 L16/99/16000 telephone-event/102/16000"/>

    <!-- Add any default MRCP params for SPEAK requests here -->
    <synthparams>
      <voice value="siqi"/>
    </synthparams>
  </profile>
</include>
' WHERE `enterprise_id`='template' and `conf_type`='TTS' and `sub_type`='ALIBABA_TTS';

UPDATE `outbound`.`fs_config` SET `xml_content` = '<include>
<profile name="iflytek-tts" version="2">
    <param name="client-ext-ip" value="FS_HOST_IP_REPLACE"/>
    <param name="client-ip" value="FS_HOST_IP_REPLACE"/>
    <param name="client-port" value="8091"/>

    <param name="server-ip" value="请替换为科大讯飞TTSMRCP服务器的IP"/>
    <param name="server-port" value="5060"/>
    <!--param name="force-destination" value="1"/-->
    <param name="sip-transport" value="tcp"/>
    <param name="ua-name" value="TVP"/>
    <param name="sdp-origin" value="TVP"/>
    <!--
    <param name="rtp-ext-ip" value="auto"/>
    -->
    <param name="rtp-ip" value="FS_HOST_IP_REPLACE"/>
    <param name="rtp-port-min" value="12001"/>
    <param name="rtp-port-max" value="13000"/>
<!-- enable/disable rtcp support -->
    <param name="rtcp" value="1"/>
<!-- rtcp bye policies (rtcp must be enabled first)
     0 - disable rtcp bye
     1 - send rtcp bye at the end of session
     2 - send rtcp bye also at the end of each talkspurt (input)
-->
    <param name="rtcp-bye" value="2"/>
    <!-- rtcp transmission interval in msec (set 0 to disable) -->
    <param name="rtcp-tx-interval" value="5000"/>
    <!-- period (timeout) to check for new rtcp messages in msec (set 0 to disable) -->
    <param name="rtcp-rx-resolution" value="1000"/>
    <!--param name="playout-delay" value="50"/-->
    <!--param name="max-playout-delay" value="200"/-->
    <param name="ptime" value="20"/>
    <param name="codecs" value="PCMA PCMU L16/96/8000"/>
    <param name="jsgf-mime-type" value="application/jsgf"/>
    <!-- Add any default MRCP params for SPEAK requests here -->
    <synthparams>
        <voice value="xiaoyan"/>
    </synthparams>
  </profile>
</include>
' WHERE `enterprise_id`='template' and `conf_type`='TTS' and `sub_type`='IFLYTEK_TTS';

UPDATE `outbound`.`fs_config` SET `xml_content` = '<include>
  <!-- UniMRCP Server MRCPv2 -->
  <profile name="emotibot" version="2">
    <param name="server-ip" value="请替换为竹间智能MRCP服务器的IP"/>
    <param name="server-port" value="8060"/>

    <param name="client-ip" value="FS_HOST_IP_REPLACE"/>
    <param name="client-port" value="8062"/>
    <param name="sip-transport" value="tcp"/>

    <!--param name="rtp-ext-ip" value="auto"/-->
    <param name="rtp-ip" value="FS_HOST_IP_REPLACE"/>
    <param name="rtp-port-min" value="12001"/>
    <param name="rtp-port-max" value="13000"/>
    <!--param name="playout-delay" value="50"/-->
    <!--param name="max-playout-delay" value="200"/-->
    <!--param name="ptime" value="20"/-->
    <param name="codecs" value="PCMU PCMA L16/96/8000 telephone-event/101/8000 PCMU/97/16000 PCMA/98/16000 L16/99/16000 telephone-event/102/16000"/>

    <!-- Add any default MRCP params for SPEAK requests here -->
    <synthparams>
    </synthparams>

  </profile>
</include>
' WHERE `enterprise_id`='template' and `conf_type`='TTS' and `sub_type`='EMOTIBOT_TTS';
