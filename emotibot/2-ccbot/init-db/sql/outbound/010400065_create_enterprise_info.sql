
-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for enterprise_info
-- ----------------------------
CREATE TABLE `enterprise_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_id` varchar(32) NOT NULL DEFAULT '' COMMENT '企业ID',
  `enterprise_name` varchar(32) DEFAULT '' COMMENT '企业名称',
  `organization_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '组织ID',
  `organization_name` varchar(32) NOT NULL DEFAULT '' COMMENT '组织名称',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '上级组织ID',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态0禁用，1启用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `enterprise_info`;