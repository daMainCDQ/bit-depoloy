-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for black_list
-- ----------------------------
CREATE TABLE `black_list` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'unique id of each row',
  `black_list_name_id` bigint(20) unsigned NOT NULL COMMENT 'the id in black_list_name table',
  `phone` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'phone number ',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `idx_belonging` (`black_list_name_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='each row records the black list of phone number';

-- ----------------------------
-- Table structure for black_list_name
-- ----------------------------
CREATE TABLE `black_list_name` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'unique id in big int of each row',
  `black_uuid` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'the unique id in string format',
  `name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'name of black list',
  `enterprise` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'the enterprise id',
  `create_time` bigint(20) NOT NULL DEFAULT '0',
  `update_time` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `black_uuid_UNIQUE` (`black_uuid`),
  KEY `idx_enterprose` (`enterprise`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='each row records the black list name for each enterprise';

-- ----------------------------
-- Table structure for call_list
-- ----------------------------
CREATE TABLE `call_list` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id must be unique',
  `task_id` bigint(20) unsigned NOT NULL COMMENT 'The id of the task',
  `first_name` varchar(64) NOT NULL COMMENT 'First name',
  `last_name` varchar(64) NOT NULL COMMENT 'Last name',
  `gender` tinyint(3) unsigned NOT NULL COMMENT '0 for female, 1 for male, 2 for multi, 3 for unkonwn',
  `level` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Level',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0 for unfinished, 1 for finished, 2 for in queuing, 3 for reach max retry, 4 for talking',
  `last_update_time` bigint(20) NOT NULL DEFAULT '0' COMMENT 'Update time',
  `try_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Call time counts',
  `phone_number` varchar(16) NOT NULL COMMENT 'phone unmber',
  `information` text COMMENT 'arguments to pass to nlu',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `task_id_idx` (`task_id`),
  KEY `status_idx` (`status`),
  KEY `try_count_idx` (`try_count`),
  KEY `last_update_time_idx` (`last_update_time`)
) ENGINE=InnoDB AUTO_INCREMENT=1134 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for call_record
-- ----------------------------
CREATE TABLE `call_record` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `task_id` bigint(20) unsigned NOT NULL COMMENT 'the id of the task in table task',
  `call_id` bigint(20) unsigned NOT NULL COMMENT 'the id of callee information in call_list',
  `callout_id` bigint(20) unsigned NOT NULL COMMENT 'the id of phone used to call out',
  `ringing_start_time` bigint(20) NOT NULL DEFAULT '0' COMMENT 'ring start time',
  `ringing_end_time` bigint(20) NOT NULL DEFAULT '0' COMMENT 'ring end time',
  `talking_end_time` bigint(20) NOT NULL DEFAULT '0' COMMENT 'call end time',
  `ringing_duration` int(11) NOT NULL DEFAULT '0' COMMENT 'ring duration',
  `talking_duration` int(11) NOT NULL DEFAULT '0' COMMENT 'talking duration',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 for active hang up, 1 for hangup, 10 for unknown error, 11 for user busy, 12 for no answer, 13 for absent, 14 for gateway down',
  `last_update_time` bigint(20) NOT NULL DEFAULT '0' COMMENT 'update time',
  `freeswitch_uuid` varchar(64) NOT NULL COMMENT 'uudi of freeswitch',
  `call_direction` varchar(20) NOT NULL COMMENT 'inbound or outbound',
  `answer_state` varchar(255) NOT NULL COMMENT 'answer state',
  `caller_number` varchar(20) NOT NULL COMMENT 'call number',
  `call_begin_time` varchar(255) NOT NULL COMMENT 'call begin time',
  `callcenter_record_uuid` varchar(64) NOT NULL COMMENT 'record uuid',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `call_id_idx` (`call_id`),
  KEY `status_idx` (`status`),
  KEY `freeswitch_uuid_idx` (`freeswitch_uuid`),
  KEY `start_time_idx` (`ringing_start_time`),
  KEY `end_time_idx` (`ringing_end_time`),
  KEY `task_id_idx` (`task_id`),
  KEY `callout_id_idx` (`callout_id`),
  KEY `ring_dur_idx` (`ringing_duration`),
  KEY `talking_dur_idx` (`talking_duration`)
) ENGINE=InnoDB AUTO_INCREMENT=267 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for callout_list_name
-- ----------------------------
CREATE TABLE `callout_list_name` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(64) NOT NULL COMMENT 'list name',
  `create_time` bigint(20) NOT NULL COMMENT 'create time of list name',
  `last_update_time` bigint(20) NOT NULL DEFAULT '0' COMMENT 'update time',
  `enterprise` char(32) NOT NULL COMMENT 'enterprise',
  `user` char(32) NOT NULL COMMENT 'user',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_name` (`name`,`enterprise`)
) ENGINE=InnoDB AUTO_INCREMENT=269 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for callout_phone
-- ----------------------------
CREATE TABLE `callout_phone` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `status` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '0 for enable, 1 for disable',
  `phone_prefix` varchar(32) NOT NULL COMMENT 'prefix for call number',
  `max_call` int(10) unsigned NOT NULL COMMENT 'maximum call from this number per day',
  `last_update_time` bigint(20) NOT NULL DEFAULT '0' COMMENT 'update time',
  `calling_num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'num of calls that have already made today',
  `task_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'id of task',
  `create_time` bigint(20) NOT NULL DEFAULT '0' COMMENT 'create time',
  `caller_phone_number` varchar(20) DEFAULT NULL COMMENT 'phone number',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `last_update_time_idx` (`last_update_time`),
  KEY `num_of_call_idx` (`calling_num`),
  KEY `max_call_idx` (`max_call`),
  KEY `task_id_idx` (`task_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for license_user_list
-- ----------------------------
CREATE TABLE `license_user_list` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_uuid` char(32) NOT NULL DEFAULT '' COMMENT 'uuid of user',
  `display_name` varchar(64) NOT NULL DEFAULT '' COMMENT 'display name',
  `user_name` char(64) NOT NULL DEFAULT '' COMMENT 'user name',
  `email` char(255) NOT NULL DEFAULT '' COMMENT 'email',
  `phone` char(20) NOT NULL DEFAULT '' COMMENT 'phone',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '2' COMMENT 'type',
  `ai_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'ai count',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_uuid_UNIQUE` (`user_uuid`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for pre_record
-- ----------------------------
CREATE TABLE `pre_record` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` varchar(40) NOT NULL COMMENT 'user id',
  `robot_id` varchar(40) NOT NULL COMMENT 'robot id',
  `scenario_id` varchar(40) NOT NULL COMMENT 'scenario id',
  `name` varchar(128) NOT NULL COMMENT 'name',
  `md5name` varchar(128) NOT NULL COMMENT 'md5name',
  `update_time` bigint(20) unsigned NOT NULL COMMENT 'update time',
  `duration` int(11) NOT NULL DEFAULT '0' COMMENT 'duration',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for task
-- ----------------------------
CREATE TABLE `task` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `task_uuid` varchar(32) NOT NULL COMMENT 'task uuid',
  `task_name` varchar(64) NOT NULL COMMENT 'task name',
  `callout_list_name_id` bigint(20) unsigned NOT NULL COMMENT 'call out list name',
  `creator` char(32) NOT NULL DEFAULT '' COMMENT 'creator',
  `enterprise` char(32) NOT NULL DEFAULT '' COMMENT 'enterprise',
  `start_date` bigint(20) NOT NULL COMMENT 'only date',
  `end_date` bigint(20) NOT NULL COMMENT 'only date',
  `start_time` int(10) unsigned NOT NULL COMMENT 'seconds from start date\n',
  `end_time` int(10) unsigned NOT NULL COMMENT 'only time',
  `priority` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT 'priority',
  `max_try` tinyint(3) unsigned NOT NULL COMMENT 'max try number',
  `next_try_duration` int(10) unsigned NOT NULL COMMENT 'at least this duration for next try',
  `task_count` int(10) unsigned NOT NULL COMMENT 'number of call needed to make in this task',
  `left_count` int(11) NOT NULL COMMENT 'the call in this task that has not yet finished',
  `last_update_time` bigint(20) NOT NULL DEFAULT '0' COMMENT 'update time',
  `create_time` bigint(20) NOT NULL COMMENT 'create time',
  `status` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '0 for activated, 1 for unactivated, 2 for pause, 3 for abnormal, 4 for finished, 5 for expiration, 6 for cancel, 7 for delete',
  `work_day` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'callout only on working day. 0 for false, 1 for true',
  `use_line` int(10) unsigned NOT NULL COMMENT 'use line',
  `robot_id` char(32) NOT NULL COMMENT 'robot id',
  `scenario_id` varchar(50) NOT NULL COMMENT 'senario id',
  `ignore_err_call` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0  for not ignore, 1 for ignore',
  `barge_in` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'barge in flag',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `unique_task_name` (`enterprise`,`task_name`),
  UNIQUE KEY `unique_task_uuid` (`task_uuid`),
  KEY `start_date_idx` (`start_date`),
  KEY `end_date_idx` (`end_date`),
  KEY `start_time_idx` (`start_time`),
  KEY `end_time_idx` (`end_time`),
  KEY `task_count_idx` (`task_count`),
  KEY `left_count_idx` (`left_count`),
  KEY `last_update_time_idx` (`last_update_time`),
  KEY `enterprise_idx` (`enterprise`),
  KEY `call_list_name` (`callout_list_name_id`)
) ENGINE=InnoDB AUTO_INCREMENT=255 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- View structure for task_call_record
-- ----------------------------
CREATE 
VIEW `outbound`.`task_call_record` AS
    SELECT 
        `a`.`id` AS `task_id`,
        `b`.`call_id` AS `call_id`,
        `a`.`task_uuid` AS `task_uuid`,
        `a`.`status` AS `task_status`,
        `a`.`creator` AS `creator`,
        `b`.`freeswitch_uuid` AS `freeswitch_uuid`,
        `b`.`status` AS `call_status`,
        `b`.`talking_duration` AS `talking_duration`,
        `b`.`call_begin_time` AS `call_begin_time`
    FROM
        (`outbound`.`task` `a`
        JOIN `outbound`.`call_record` `b` ON ((`a`.`id` = `b`.`task_id`)));
        
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `black_list`;
DROP TABLE `black_list_name`;
DROP TABLE `call_list`;
DROP TABLE `call_record`;
DROP TABLE `callout_list_name`;
DROP TABLE `callout_phone`;
DROP TABLE `license_user_list`;
DROP TABLE `pre_record`;
DROP TABLE `task`;
DROP VIEW `task_call_record`;


