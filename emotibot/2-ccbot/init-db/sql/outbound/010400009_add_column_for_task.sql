-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `task` ADD COLUMN `list_filter` TINYINT(4) NOT NULL DEFAULT '0' AFTER `ignore_err_call`;
ALTER TABLE `task` ADD COLUMN `filter_rule` VARCHAR(32) NOT NULL DEFAULT '' AFTER `list_filter`;


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

ALTER TABLE `task` DROP COLUMN `list_filter`;
ALTER TABLE `task` DROP COLUMN `filter_rule`;
