-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `task` ADD COLUMN `additional_call_flag` varchar(8) NOT NULL DEFAULT "" COMMENT 'additional_call_flag value should be "true,false"';
ALTER TABLE `task` ADD COLUMN `additional_call_gap` varchar(16) NOT NULL DEFAULT "" COMMENT 'additional_call_gap value should be "default or second value"';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `task` DROP COLUMN `additional_call_flag`;
ALTER TABLE `task` DROP COLUMN `additional_call_gap`;

