-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `call_list` ADD COLUMN `tts_update_time` bigint(20) NOT NULL DEFAULT '0' COMMENT 'tts update time';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `call_list` DROP COLUMN `tts_update_time`;
