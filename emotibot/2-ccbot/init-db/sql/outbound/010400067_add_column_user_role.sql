-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `user_roles` ADD COLUMN `organization_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT '0' AFTER `user_id`;


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

ALTER TABLE `user_roles` DROP COLUMN `organization_id`;