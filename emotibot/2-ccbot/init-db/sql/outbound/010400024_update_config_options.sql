-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
UPDATE `config_options` SET `range` = '[{"format_name":"高保真模式","channel_type":"双声道","bit_num":"16 bit","bit_rate":"256kbps","sample_rate":"8khz"},{"format_name":"标准模式","channel_type":"双声道","bit_num":"16 bit","bit_rate":"128kbps","sample_rate":"8khz"},{"format_name":"经济模式","channel_type":"单声道","bit_num":"16 bit","bit_rate":"64kbps","sample_rate":"8khz"}]'
WHERE `catalog_name` = 'recording_download' AND `config_item` = 'record_format';

-- +migrate Down
UPDATE `config_options` SET `range` = '[{"format_name":"高保真模式","channel_type":"双声道","bit_num":"16 bit","bit_rate":"256kbps","sample_rate":"8khz"},{"format_name":"标准模式","channel_type":"双声道","bit_num":"16 bit","bit_rate":"128kbps","sample_rate":"8khz"},{"format_name":"经济模式","channel_type":"单声道","bit_num":"16 bit","bit_rate":"128kbps","sample_rate":"8khz"}]'
WHERE `catalog_name` = 'recording_download' AND `config_item` = 'record_format';