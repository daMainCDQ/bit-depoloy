-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for tts_common
-- ----------------------------
DROP TABLE IF EXISTS `tts_common`;
CREATE TABLE `tts_common` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `enterprise_id` varchar(50) NOT NULL COMMENT '企业id',
  `app_id` varchar(50) NOT NULL COMMENT '机器人id',
  `scenario_id` varchar(50) NOT NULL COMMENT '场景id',
  `uuid` varchar(50) NOT NULL COMMENT 'tts_scenario_text表中的uuid',
  `status` tinyint(1) NOT NULL COMMENT '0:未完成，1:已完成',
  `total_cnt` int(11) NOT NULL COMMENT '常量文本总条数',
  `finished_cnt` int(11) NOT NULL COMMENT '已完成常量文本条数',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `udx_eas` (`enterprise_id`,`app_id`,`scenario_id`) USING BTREE,
  UNIQUE KEY `udx_uuid` (`uuid`) USING BTREE,
  KEY `idx_status` (`status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='表tts_common_text数据总揽表';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `tts_common`;
