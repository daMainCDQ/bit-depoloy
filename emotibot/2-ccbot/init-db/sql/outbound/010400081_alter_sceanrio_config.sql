-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `outbound`.`scenario_config` CHANGE `audio_detect_type` `emotion_detection` tinyint(1) NOT NULL DEFAULT '0' COMMENT '情绪检测使能，1是启用，0是禁用';
ALTER TABLE `outbound`.`scenario_config` ADD COLUMN `gender_detection` tinyint(1) NOT NULL DEFAULT '0' COMMENT '性别检测使能，1是启用，0是禁用';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `outbound`.`scenario_config` DROP COLUMN `emotion_detection`;
ALTER TABLE `outbound`.`scenario_config` DROP COLUMN `gender_detection`;
