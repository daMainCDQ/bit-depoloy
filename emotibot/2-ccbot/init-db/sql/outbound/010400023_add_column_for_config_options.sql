-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `config_options` ADD COLUMN `range` varchar(512) DEFAULT '' COMMENT '取值范围 ' AFTER `value`;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

ALTER TABLE `config_options` DROP COLUMN `range`;
