-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
BEGIN;
DELETE FROM `outbound`.`config_options` WHERE `catalog_name`='module_list' and `enterprise_id`='default';
INSERT INTO `outbound`.`config_options` (`enterprise_id`, `catalog_name`, `config_item`, `value`, `range`) VALUES ('default', 'module_list', 'none','[{"module":"unimrcp-emotibot"},{"module":"cc-ext-service"},{"module":"call-center-ui"},{"module":"statistics-service"},{"module":"call-scheduler"},{"module":"config-manager"},{"module":"records-manager"},{"module":"callcenter-manager"},{"module":"api-gateway"},{"module":"freeswitch-emotibot"},{"module":"fs_api"}]','');
COMMIT;
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DELETE FROM `outbound`.`config_options` WHERE `catalog_name`='module_list' and `enterprise_id`='default';
