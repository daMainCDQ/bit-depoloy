-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
UPDATE `outbound`.`privileges` SET `cmd_list` = 'view,create,edit,import,export,delete,audit' WHERE `id` = '6';
UPDATE `outbound`.`modules` SET `cmd_list` = 'view,create,edit,import,export,delete,audit' WHERE `id` = '6';
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back


