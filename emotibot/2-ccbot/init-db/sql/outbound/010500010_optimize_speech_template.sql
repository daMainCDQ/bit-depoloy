-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
UPDATE `outbound`.`fs_config` SET `xml_content` = '<include>
  <!-- UniMRCP Server MRCPv2 -->
  <profile name="alibaba_tts" version="2">
    <param name="server-ip" value="请替换为阿里巴巴TTSMRCP服务器的IP"/>
    <param name="server-port" value="7010"/>

    <param name="client-ip" value="$${local_ip_v4}"/>
    <param name="client-port" value="8097"/>
    <param name="sip-transport" value="tcp"/>

    <!--param name="rtp-ext-ip" value="auto"/-->
    <param name="rtp-ip" value="$${local_ip_v4}"/>
    <param name="rtp-port-min" value="12001"/>
    <param name="rtp-port-max" value="13000"/>
    <!--param name="playout-delay" value="50"/-->
    <!--param name="max-playout-delay" value="200"/-->
    <!--param name="ptime" value="20"/-->
    <param name="codecs" value="PCMU PCMA L16/96/8000 telephone-event/101/8000 PCMU/97/16000 PCMA/98/16000 L16/99/16000 telephone-event/102/16000"/>

    <!-- Add any default MRCP params for SPEAK requests here -->
    <synthparams>
      <voice value="aixia"/>
      <!-- 语速范围0~500，负值可去tts服务器端调整 -->
      <param name="prosody-rate" value="0"/>
      <param name="te-path" value="auto"/>
      <param name="faq-path" value="auto"/>
    </synthparams>
  </profile>
</include>
' WHERE `enterprise_id` = 'template' AND `conf_type`='TTS' and `sub_type`='ALIBABA_TTS';

UPDATE `outbound`.`fs_config` SET `xml_content` = '<include>
  <!-- UniMRCP Server MRCPv2 -->
  <profile name="emotibot_tts" version="2">
    <param name="server-ip" value="$${local_ip_v4}"/>
    <param name="server-port" value="8060"/>

    <param name="client-ip" value="$${local_ip_v4}"/>
    <param name="client-port" value="8062"/>
    <param name="sip-transport" value="tcp"/>

    <!--param name="rtp-ext-ip" value="auto"/-->
    <param name="rtp-ip" value="$${local_ip_v4}"/>
    <param name="rtp-port-min" value="12001"/>
    <param name="rtp-port-max" value="13000"/>
    <!--param name="playout-delay" value="50"/-->
    <!--param name="max-playout-delay" value="200"/-->
    <!--param name="ptime" value="20"/-->
    <param name="codecs" value="PCMU PCMA L16/96/8000 telephone-event/101/8000 PCMU/97/16000 PCMA/98/16000 L16/99/16000 telephone-event/102/16000"/>

    <!-- Add any default MRCP params for SPEAK requests here -->
    <synthparams>
          <param name="te-path" value="auto"/>
          <param name="faq-path" value="auto"/>
          <!-- 请修改以下大写名称所设定的参数，可参考以下阿里和竹间的模板设定-->
          <!-- 阿里语者可选aixia、siqi，语速-500~500，音量0~100 -->
          <!-- param name="tts-param" value="vendor:alibaba|ip:TTS_IP|port:8101|voice:aixia|speed:-200|access-token:default|app-key:default|sample-rate:8000|format:pcm|volume:50"/-->
          <!-- 竹间语速0.5~1.5，音量0.1~2 -->
          <!-- param name="tts-param" value="vendor:emotibot|ip:TTS_IP|port:8100|user-id:speech|speed:1.1|lang:zh-cn|sample-rate:8000"/-->
         <!-- 标贝语者娇娇、楠楠、小金、果果、静静，语速0~9，音量0~9 -->
          <!-- param name="tts-param" value="vendor:biaobei|ip:172.16.102.228|domain:1|port:8100|lang:ZH|voice:xiaojin|speed:5|volume:5|access-token:default|sample-rate:1|format:5"/-->
          <param name="tts-param" value="vendor:VENDOR_NAME|ip:TTS_IP|port:TTS_PORT|voice:TTS_VOICER|speed:VOICE_SPEED|pitch:0|access-token:default|app-key:default|sample-rate:8000|format:pcm|volume:50"/>
    </synthparams>
  </profile>
</include>
' WHERE `enterprise_id` = 'template' AND `conf_type`='TTS' and `sub_type`='EMOTIBOT_TTS';

UPDATE `outbound`.`fs_config` SET `conf_name`='emotibot_asr', `xml_content` = '<include>
  <!-- UniMRCP Server MRCPv2 -->
  <profile name="emotibot_asr" version="2">
    <param name="server-ip" value="请替换为竹间智能ASR MRCP服务器的IP"/>
    <param name="server-port" value="8060"/>

    <param name="client-ip" value="$${local_ip_v4}"/>
    <param name="client-port" value="8062"/>
    <param name="sip-transport" value="tcp"/>

    <!--param name="rtp-ext-ip" value="auto"/-->
    <param name="rtp-ip" value="$${local_ip_v4}"/>
    <param name="rtp-port-min" value="12001"/>
    <param name="rtp-port-max" value="13000"/>
    <!--param name="playout-delay" value="50"/-->
    <!--param name="max-playout-delay" value="200"/-->
    <!--param name="ptime" value="20"/-->
    <param name="codecs" value="PCMU PCMA L16/96/8000 telephone-event/101/8000 PCMU/97/16000 PCMA/98/16000 L16/99/16000 telephone-event/102/16000"/>

    <!-- Add any default MRCP params for RECOGNIZE requests here -->
    <recogparams>
      <param name="speech-language" value="en-emotibot"/>
      <param name="session-id" value="auto"/>
      <param name="start-input-timers" value="true"/>
    </recogparams>
  </profile>
</include>
' WHERE `enterprise_id` = 'template' AND `conf_type`='ASR' and `sub_type`='EMOTIBOT_ASR';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
UPDATE `outbound`.`fs_config` SET `xml_content` = '<include>
  <!-- UniMRCP Server MRCPv2 -->
  <profile name="alibaba_tts" version="2">
    <param name="server-ip" value="请替换为阿里巴巴TTSMRCP服务器的IP"/>
    <param name="server-port" value="7010"/>

    <param name="client-ip" value="$${local_ip_v4}"/>
    <param name="client-port" value="8097"/>
    <param name="sip-transport" value="tcp"/>

    <!--param name="rtp-ext-ip" value="auto"/-->
    <param name="rtp-ip" value="$${local_ip_v4}"/>
    <param name="rtp-port-min" value="12001"/>
    <param name="rtp-port-max" value="13000"/>
    <!--param name="playout-delay" value="50"/-->
    <!--param name="max-playout-delay" value="200"/-->
    <!--param name="ptime" value="20"/-->
    <param name="codecs" value="PCMU PCMA L16/96/8000 telephone-event/101/8000 PCMU/97/16000 PCMA/98/16000 L16/99/16000 telephone-event/102/16000"/>

    <!-- Add any default MRCP params for SPEAK requests here -->
    <synthparams>
      <voice value="aixia"/>
      <!-- 语速范围0~500，负值可去tts服务器端调整 -->
      <param name="prosody-rate" value="0"/>
      <param name="te-path" value="auto"/>
      <param name="faq-path" value="auto"/>
    </synthparams>
  </profile>
</include>
' WHERE `enterprise_id` = 'template' AND `conf_type`='TTS' and `sub_type`='ALIBABA_TTS';