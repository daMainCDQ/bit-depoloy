-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
UPDATE `outbound`.`fs_config` SET `xml_content` = '<include>
  <!-- UniMRCP Server MRCPv2 -->
  <profile name="alibaba_tts" version="2">
    <param name="server-ip" value="请替换为阿里巴巴TTSMRCP服务器的IP"/>
    <param name="server-port" value="7010"/>

    <param name="client-ip" value="$${local_ip_v4}"/>
    <param name="client-port" value="8097"/>
    <param name="sip-transport" value="tcp"/>

    <!--param name="rtp-ext-ip" value="auto"/-->
    <param name="rtp-ip" value="$${local_ip_v4}"/>
    <param name="rtp-port-min" value="12001"/>
    <param name="rtp-port-max" value="13000"/>
    <!--param name="playout-delay" value="50"/-->
    <!--param name="max-playout-delay" value="200"/-->
    <!--param name="ptime" value="20"/-->
    <param name="codecs" value="PCMU PCMA L16/96/8000 telephone-event/101/8000 PCMU/97/16000 PCMA/98/16000 L16/99/16000 telephone-event/102/16000"/>

    <!-- Add any default MRCP params for SPEAK requests here -->
    <synthparams>
      <voice value="aixia"/>
      <param name="te-path" value="auto"/>
      <param name="faq-path" value="auto"/>
      <!--更新TTS_IP为上述的阿里巴巴TTS MRCP服务器IP，阿里语者可选aixia、siqi，语速-500~500，音量0~100，用于一键生成-->
      <param name="tts-param" value="vendor:alibaba|ip:TTS_IP|port:8101|voice:aixia|speed:-200|access-token:default|app-key:default|sample-rate:8000|format:pcm|volume:50"/>
    </synthparams>
  </profile>
</include>
' WHERE `enterprise_id`='template' and `conf_type`='TTS' and `sub_type`='ALIBABA_TTS';


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back