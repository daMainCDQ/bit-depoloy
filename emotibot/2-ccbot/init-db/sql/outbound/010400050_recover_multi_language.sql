-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
BEGIN;
UPDATE `outbound`.`config_options` SET `range` = '[{\"region\":\"cn\",\"zh-cn\":\"中国大陆\",\"zh-tw\":\"中國大陸\",\"en\":\"China Mainland\"},{\"region\":\"tw\",\"zh-cn\":\"中国台湾\",\"zh-tw\":\"中國臺灣\",\"en\":\"Taiwan, China\"},{\"region\":\"hkmacao\",\"zh-cn\":\"中国香港/澳门\",\"zh-tw\":\"中國香港/澳門\",\"en\":\"China Hong Kong / Macau\"}]' WHERE `catalog_name`='region' and `config_item`='region';
COMMIT;
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back