-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `outbound`.`tts_common` DROP INDEX `udx_eas`;
ALTER TABLE `outbound`.`tts_common` DROP INDEX `udx_uuid`;
ALTER TABLE `outbound`.`tts_common` DROP COLUMN `enterprise_id`;
ALTER TABLE `outbound`.`tts_common` DROP COLUMN `app_id`;
ALTER TABLE `outbound`.`tts_common` DROP COLUMN `scenario_id`;
ALTER TABLE `outbound`.`tts_common` DROP COLUMN `uuid`;
ALTER TABLE `outbound`.`tts_common` ADD COLUMN `tts_scenario_id` bigint(11) NOT NULL COMMENT '表tts_scenario主键' AFTER `id`;
ALTER TABLE `outbound`.`tts_common` ADD COLUMN `tts_params` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT 'tts参数' AFTER `tts_scenario_id`;
ALTER TABLE `outbound`.`tts_common` ADD COLUMN `tts_params_md5` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT 'tts参数签名 md5' AFTER `tts_params`;
ALTER TABLE `outbound`.`tts_common` ADD COLUMN `scenario_version` int(11) NULL DEFAULT NULL COMMENT '版本号' AFTER `tts_params_md5`;
ALTER TABLE `outbound`.`tts_common` ADD UNIQUE INDEX `udx_tts`(`tts_scenario_id`, `tts_params_md5`, `scenario_version`) USING BTREE;
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
