
-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for inbound_config
-- ----------------------------

CREATE TABLE `inbound_config` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id must be unique',
  `enterprise_id` varchar(32) NOT NULL DEFAULT '' COMMENT '企业id',
  `inbound_uuid` varchar(32) NOT NULL DEFAULT '' COMMENT '呼入配置表UUID',
  `extension` varchar(255) NOT NULL COMMENT '呼入分机号',
  `gateway_name` varchar(32) NOT NULL DEFAULT '' COMMENT ' 网关名称',
  `line_id` bigint(20) unsigned NOT NULL COMMENT '线路id',
  `inbound_mode` tinyint(2) NOT NULL COMMENT '分机模式',
  `gw_xml_content` text NOT NULL COMMENT '网关xml内容',
  `dialplan_xml` text NOT NULL COMMENT '拨打计划xml',
  `phone_number` varchar(32) NOT NULL DEFAULT '' COMMENT '电话号码',
  `max_call_num` int(3) NOT NULL COMMENT '最大呼叫数量',
  `enable` tinyint(2) NOT NULL COMMENT '是否禁用',
  `fs_ip` varchar(32) NOT NULL COMMENT 'FS 服务器地址',
  `app_id` varchar(32) NOT NULL DEFAULT '' COMMENT '机器人Id',
  `scenario_id` varchar(64) NOT NULL DEFAULT '' COMMENT '场景id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_uuid` (`inbound_uuid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8mb4 COMMENT='free switch dial plan configuration';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `inbound_config`;