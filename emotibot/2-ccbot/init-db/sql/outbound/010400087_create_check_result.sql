-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for check_result
-- ----------------------------
DROP TABLE IF EXISTS `check_result`;
CREATE TABLE `check_result` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `task_id` bigint(11) NOT NULL COMMENT '任务id',
  `call_id` bigint(11) NOT NULL COMMENT '电话id',
  `session_id` varchar(50) NOT NULL COMMENT '会话id',
  `status` tinyint(4) NOT NULL COMMENT '对应call_list表status',
  `is_checked` tinyint(1) NOT NULL COMMENT '记录是否已检测，0：未检测，1:已检测',
  `dialog_check_result` tinyint(4) NOT NULL DEFAULT '0' COMMENT '对话检测异常数量',
  `dialog_detail` text COMMENT '错误对话内容',
  `slots_check_result` tinyint(4) NOT NULL DEFAULT '0' COMMENT '多轮标签检测异常数量',
  `slots_detail` text COMMENT '错误多轮标签明细',
  `tts_check_result` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'tts异常数量',
  `asr_noise` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'asr噪音异常数量',
  `asr_timeout` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'asr超时异常数量',
  `asr_inner_error` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'asr内部错误数量',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `udx_task_call` (`task_id`,`call_id`),
  KEY `idx_status` (`status`),
  KEY `idx_session_id` (`session_id`),
  KEY `idx_is_checked` (`is_checked`)
) ENGINE=InnoDB AUTO_INCREMENT=317338 DEFAULT CHARSET=utf8mb4 COMMENT='检查结果表';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `check_result`;
