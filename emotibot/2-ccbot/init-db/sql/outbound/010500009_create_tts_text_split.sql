-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for tts_text_split
-- ----------------------------
DROP TABLE IF EXISTS `tts_text_split`;
CREATE TABLE `tts_text_split` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `tts_scenario_id` bigint(11) NOT NULL COMMENT 'tts_scenario主键',
  `tts_scenario_text_id` bigint(11) NOT NULL COMMENT 'tts_scenario_text主键',
  `text` text COMMENT '文本',
  `text_md5` varchar(32) NOT NULL COMMENT 'text的md5',
  `type` tinyint(4) NOT NULL COMMENT '0:常量，1:变量',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_tts_scenario_text_id` (`tts_scenario_text_id`) USING BTREE,
  KEY `idx_tts_scenario_id` (`tts_scenario_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='tts文本切片表';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `tts_text_split`;
