-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for tts_common
-- ----------------------------
DROP TABLE IF EXISTS `tts_common`;
CREATE TABLE `tts_common` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `tts_scenario_id` bigint(11) NOT NULL COMMENT '表tts_scenario主键',
  `tts_params` text COMMENT 'tts参数',
  `tts_params_md5` varchar(32) DEFAULT '' COMMENT 'tts参数签名 md5',
  `scenario_version` int(11) DEFAULT NULL COMMENT '版本号',
  `status` tinyint(1) NOT NULL COMMENT '0:未完成，1:已完成',
  `total_cnt` int(11) NOT NULL COMMENT '常量文本总条数',
  `finished_cnt` int(11) NOT NULL COMMENT '已完成常量文本条数',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `udx_tts` (`tts_scenario_id`,`tts_params_md5`,`scenario_version`) USING BTREE,
  KEY `idx_status` (`status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='表tts_common_text数据总揽表';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `tts_common`;
