-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `license_user_list` ADD COLUMN `enterprise` CHAR(32) NOT NULL DEFAULT '' AFTER `id`;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `license_user_list` DROP COLUMN `enterprise`;




