-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for line_assign
-- ----------------------------
CREATE TABLE `line_assign` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `task_id` bigint(20) NOT NULL COMMENT 'which task takes the line',
  `line_id` bigint(20) NOT NULL COMMENT 'which line to assgine',
  `assigned_num` int(8) NOT NULL COMMENT 'how many lines assgined to the task',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `line_assign`;
