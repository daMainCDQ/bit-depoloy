-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for tts_common_text
-- ----------------------------
DROP TABLE IF EXISTS `tts_common_text`;
CREATE TABLE `tts_common_text` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `tts_common_id` bigint(11) NOT NULL COMMENT 'tts_common 主键',
  `text` text NOT NULL COMMENT '常量话术',
  `text_md5` varchar(32) NOT NULL COMMENT 'text的md5值，用来建索引',
  `audio_file_path` varchar(200) NOT NULL COMMENT '音频文件路径',
  `status` tinyint(4) NOT NULL COMMENT '0:未完成，1:已完成',
  `duration` int(11) NOT NULL COMMENT '音频文件时长',
  `expired` tinyint(4) DEFAULT '0' COMMENT '数据是否过期 0: 未过期，1:过期',
  `retry_times` tinyint(4) DEFAULT '0' COMMENT '失败重试次数',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `udx_id_text` (`tts_common_id`,`text_md5`) USING BTREE,
  KEY `idx_retry_times` (`retry_times`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='tts常量话术表';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `tts_common_text`;
