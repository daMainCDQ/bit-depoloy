-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for tts_task_info
-- ----------------------------

CREATE TABLE `tts_task_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `enterprise_id` varchar(50) NOT NULL COMMENT 'enterprise id',
  `task_uuid` varchar(50) NOT NULL COMMENT 'task uuid',
  `task_type` tinyint(1) NOT NULL COMMENT '任务类型 1：FAQ一键生成，2：TE 纯文本一键生成，3：TE 带变量文本一键生成',
  `app_id` varchar(50) NOT NULL COMMENT '机器人id',
  `scenario_id` varchar(50) NOT NULL DEFAULT '' COMMENT '场景id',
  `status` tinyint(1) NOT NULL COMMENT '0:创建，1:执行中，2:暂停，3:取消，4:完成',
  `text_total_cnt` int(10) NOT NULL DEFAULT '0' COMMENT '任务包含的tts总数',
  `synth_cnt` int(10) NOT NULL DEFAULT '0' COMMENT '已完成的tts数',
  `data` text COMMENT '场景 json 中参数变量对应的值对象',
  `tts_service_name` varchar(100) NOT NULL COMMENT 'tts_服务名称',
  `call_id` varchar(30) NOT NULL DEFAULT '' COMMENT '请求api中的参数，用于完成任务后的回调',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uidx_task_uuid` (`task_uuid`) USING BTREE,
  KEY `idx_enterprise_id` (`enterprise_id`) USING BTREE,
  KEY `idx_app_id` (`app_id`) USING BTREE,
  KEY `idx_status` (`status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='tts任务信息表';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `tts_task_info`;