
-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for tts_session_pre_generate_detail_split
-- ----------------------------
CREATE TABLE `tts_session_pre_generate_detail_split` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `detail_id` bigint(11) NOT NULL COMMENT 'tts会话预生成明细表 主键',
  `common_id` bigint(11) NOT NULL COMMENT 'tts_common_text主键',
  `text` text NOT NULL COMMENT '文本',
  `audio_file_path` varchar(200) NOT NULL COMMENT '音频文件路径',
  `text_type` tinyint(4) NOT NULL COMMENT '文本类型，0:common_text， 1：variable_text',
  `status` tinyint(4) NOT NULL COMMENT '0:未完成，1:已完成',
  `duration` int(11) NOT NULL COMMENT '音频文件时长',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_detail_id` (`detail_id`),
  KEY `idx_text` (`text`(50)),
  KEY `idx_common_id` (`common_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='tts会话预生成明细拆分表';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `tts_session_pre_generate_detail_split`;