-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `outbound`.`tts_session_pre_generate` DROP INDEX `udx_union`;
ALTER TABLE `outbound`.`tts_session_pre_generate` DROP COLUMN `uuid`;
ALTER TABLE `outbound`.`tts_session_pre_generate` ADD COLUMN `session_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '会话id' AFTER `scenario_id`;
ALTER TABLE `outbound`.`tts_session_pre_generate` ADD INDEX `idx_session_id`(`session_id`) USING BTREE;
ALTER TABLE `outbound`.`tts_session_pre_generate` MODIFY COLUMN `phone` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '电话' AFTER `session_id`;
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back