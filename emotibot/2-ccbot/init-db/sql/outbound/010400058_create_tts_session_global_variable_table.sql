
-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for tts_session_global_variable
-- ----------------------------
CREATE TABLE `tts_session_global_variable` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pre_generate_id` bigint(11) NOT NULL COMMENT 'tts会话预生成表 主键',
  `scenario_id` varchar(50) NOT NULL COMMENT '场景id',
  `key_text` varchar(50) NOT NULL COMMENT 'global变量的键',
  `value_text` text NOT NULL COMMENT 'global变量的值',
  `audio_file_path` varchar(200) NOT NULL COMMENT '音频文件路径',
  `status` tinyint(4) NOT NULL COMMENT '0:未完成，1:已完成',
  `duration` int(11) NOT NULL COMMENT '音频文件时长',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_pgi` (`pre_generate_id`) USING BTREE,
  KEY `idx_union` (`key_text`,`value_text`(50)) USING BTREE,
  KEY `idx_scenario_id` (`scenario_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='tts会话变量表';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `tts_session_global_variable`;