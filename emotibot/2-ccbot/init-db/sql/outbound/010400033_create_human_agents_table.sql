
-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for human_agents
-- ----------------------------
CREATE TABLE `human_agents` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'unique agent id',
  `enterprise_id` varchar(32) NOT NULL COMMENT '企业id',
  `agent_name` varchar(32) NOT NULL COMMENT '坐席名称',
  `agent_type` tinyint(3) unsigned DEFAULT NULL COMMENT '坐席类型',
  `sip_uri` varchar(255) DEFAULT NULL COMMENT '坐席sip地址',
  `agent_description` varchar(512) DEFAULT NULL COMMENT '坐席描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COMMENT='each row records the agent config';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `human_agents`;