-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for pre_record
-- ----------------------------

DROP TABLE IF EXISTS `pre_record`;
CREATE TABLE `pre_record` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pre_record_uuid` varchar(50) NOT NULL COMMENT 'pre_record_id',
  `solution_uuid` varchar(50) NOT NULL COMMENT '方案id',
  `node_id` varchar(50) DEFAULT '' COMMENT '节点id',
  `node_name` text COMMENT '节点名称',
  `answer_type` tinyint(1) DEFAULT '0' COMMENT '保留字段',
  `faq_question` text COMMENT 'faq 问题',
  `text_original` text COMMENT 'faq 原始答案',
  `text` text COMMENT 'faq 更新后的答案',
  `pre_record_name` varchar(300) NOT NULL COMMENT '录音文件名',
  `duration` int(11) DEFAULT '0' COMMENT '录音时长',
  `create_time` varchar(30) DEFAULT '' COMMENT '生成时间，如果为空表示还没有生成，或还没有上传',
  `status` tinyint(1) DEFAULT '1' COMMENT '1:初始化，2:成功，3:失败',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `source_type` tinyint(1) NOT NULL COMMENT '预录音来源类型，1：一键生成，2：用户上传 ',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_solution_uuid` (`solution_uuid`) USING BTREE,
  KEY `idx_pre_record_uuid` (`pre_record_uuid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='预录音明细表';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `pre_record`;