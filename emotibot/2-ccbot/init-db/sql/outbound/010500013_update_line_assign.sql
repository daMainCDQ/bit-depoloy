-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `outbound`.`line_assign` ADD COLUMN `call_id` bigint NOT NULL DEFAULT '0' COMMENT 'pk of call_list ';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back