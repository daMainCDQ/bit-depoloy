-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
INSERT INTO `outbound`.`config_options` (`enterprise_id`, `catalog_name`, `config_item`, `item_description`, `value`) VALUES ('default', 'audio_detect', 'audio_output', '语音检测功能，是否支持语音对外输出，0值禁用，1为启用', '0');
ALTER TABLE `outbound`.`scenario_config` ADD COLUMN `audio_detect_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '语音检测类型，0禁用，1是ASR对比测试，2是性别情绪检测，3是声纹检测，4待定' AFTER `custom_info`;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `outbound`.`scenario_config` DROP COLUMN `audio_detect_type`;