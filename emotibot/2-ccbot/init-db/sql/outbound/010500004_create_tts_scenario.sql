-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for tts_scenario
-- ----------------------------
DROP TABLE IF EXISTS `tts_scenario`;
CREATE TABLE `tts_scenario` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `enterprise_id` varchar(50) NOT NULL COMMENT '企业id',
  `app_id` varchar(50) NOT NULL COMMENT '机器人id',
  `scenario_id` varchar(50) NOT NULL COMMENT '场景id',
  `version` int(11) DEFAULT NULL COMMENT '版本号',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `udx_eas` (`enterprise_id`,`app_id`,`scenario_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='场景基础表';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `tts_scenario`;
