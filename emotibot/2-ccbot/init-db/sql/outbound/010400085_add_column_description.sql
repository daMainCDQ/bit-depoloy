-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `outbound`.`fs_config` ADD COLUMN `description` varchar(50) NULL COMMENT '说明配置的服务是做什么，用途是什么';
ALTER TABLE `outbound`.`fs_config` CHANGE `conf_uuid` `conf_uuid` char(32) NOT NULL DEFAULT '' COMMENT 'uuid';
ALTER TABLE `outbound`.`fs_config` CHANGE `enterprise_id` `enterprise_id` char(32) NOT NULL DEFAULT '' COMMENT '企业id';
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `outbound`.`fs_config` DROP COLUMN `description`;