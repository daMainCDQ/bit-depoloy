-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `callout_phone` DROP COLUMN `task_id`;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `callout_phone` ADD COLUMN `task_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'id of task';


