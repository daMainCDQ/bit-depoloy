-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
INSERT INTO `fs_config` (`conf_uuid`, `enterprise_id`, `conf_type`, `sub_type`, `conf_name`, `xml_content`, `enable`, `authorized_num`)
SELECT '31f7d4803b984fb58ff12bce0ad475ed', 'global_enterprise_id', 'TTS', 'EMOTIBOT_TTS', 'builtin_emotibot_tts', '<include>
  <!-- UniMRCP Server MRCPv2 -->
  <profile name="builtin_emotibot_tts" version="2">
    <param name="server-ip" value="$${local_ip_v4}"/>
    <param name="server-port" value="8060"/>

    <param name="client-ip" value="$${local_ip_v4}"/>
    <param name="client-port" value="8062"/>
    <param name="sip-transport" value="tcp"/>

    <!--param name="rtp-ext-ip" value="auto"/-->
    <param name="rtp-ip" value="$${local_ip_v4}"/>
    <param name="rtp-port-min" value="12001"/>
    <param name="rtp-port-max" value="13000"/>
    <!--param name="playout-delay" value="50"/-->
    <!--param name="max-playout-delay" value="200"/-->
    <!--param name="ptime" value="20"/-->
    <param name="codecs" value="PCMU PCMA L16/96/8000 telephone-event/101/8000 PCMU/97/16000 PCMA/98/16000 L16/99/16000 telephone-event/102/16000"/>

    <!-- Add any default MRCP params for SPEAK requests here -->
    <synthparams>
          <param name="te-path" value="auto"/>
          <param name="faq-path" value="auto"/>
          <!--  请修改以下大写名称所设定的参数  -->
          <param name="tts-param" value="vendor:emotibot|ip:172.16.101.48|port:8100|user-id:speech|lang:zh-cn|sample-rate:8000"/>
    </synthparams>
  </profile>
</include>
', 1, 1
FROM
    DUAL
WHERE NOT EXISTS (
	SELECT `conf_uuid`, `enterprise_id`, `conf_type`, `sub_type`, `conf_name`
	FROM `fs_config`
	WHERE conf_name='builtin_emotibot_tts' AND sub_type='EMOTIBOT_TTS' AND `enterprise_id` = 'global_enterprise_id'
);

INSERT INTO `fs_config` (`conf_uuid`, `enterprise_id`, `conf_type`, `sub_type`, `conf_name`, `xml_content`, `enable`, `authorized_num`)
SELECT 'a7562de1356e482b8ae2f078b3126221', 'global_enterprise_id', 'ASR', 'EMOTIBOT_ASR', 'builtin_emotibot_asr', '<include>
  <!-- UniMRCP Server MRCPv2 -->
  <profile name="builtin_emotibot_asr" version="2">
    <param name="server-ip" value="172.16.102.83"/>
    <param name="server-port" value="8060"/>

    <param name="client-ip" value="$${local_ip_v4}"/>
    <param name="client-port" value="8062"/>
    <param name="sip-transport" value="tcp"/>

    <!--param name="rtp-ext-ip" value="auto"/-->
    <param name="rtp-ip" value="$${local_ip_v4}"/>
    <param name="rtp-port-min" value="12001"/>
    <param name="rtp-port-max" value="13000"/>
    <!--param name="playout-delay" value="50"/-->
    <!--param name="max-playout-delay" value="200"/-->
    <!--param name="ptime" value="20"/-->
    <param name="codecs" value="PCMU PCMA L16/96/8000 telephone-event/101/8000 PCMU/97/16000 PCMA/98/16000 L16/99/16000 telephone-event/102/16000"/>

    <!-- Add any default MRCP params for RECOGNIZE requests here -->
    <recogparams>
      <param name="speech-language" value="en-emotibot"/>
      <param name="session-id" value="auto"/>
      <param name="start-input-timers" value="true"/>
    </recogparams>
  </profile>
</include>', 1, 1
FROM
    DUAL
WHERE NOT EXISTS (
	SELECT `conf_uuid`, `enterprise_id`, `conf_type`, `sub_type`, `conf_name`
	FROM `fs_config`
	WHERE conf_name='builtin_emotibot_asr' AND sub_type='EMOTIBOT_ASR' AND `enterprise_id` = 'global_enterprise_id'
);

INSERT INTO `fs_config` (`conf_uuid`, `enterprise_id`, `conf_type`, `xml_content`, `enable`, `authorized_num`)
SELECT 'cb6b71b018c74f55861cfe18367f120a', 'global_enterprise_id', 'UNIMRCP', '<configuration name="unimrcp.conf" description="UniMRCP Client">
  <settings>
    <!-- UniMRCP profile to use for TTS -->
    <param name="default-tts-profile" value="builtin_emotibot_tts"/>
    <!-- UniMRCP profile to use for ASR -->
    <param name="default-asr-profile" value="builtin_emotibot_asr"/>
    <!-- UniMRCP logging level to appear in freeswitch.log.  Options are:
         EMERGENCY|ALERT|CRITICAL|ERROR|WARNING|NOTICE|INFO|DEBUG -->
    <param name="log-level" value="DEBUG"/>
    <!-- Enable events for profile creation, open, and close -->
    <param name="enable-profile-events" value="false"/>

    <param name="max-connection-count" value="300"/>
    <param name="offer-new-connection" value="1"/>
    <param name="request-timeout" value="5000"/>
  </settings>

  <profiles>
    <X-PRE-PROCESS cmd="include" data="../mrcp_profiles/*.xml"/>
  </profiles>

</configuration>', 1, 0
FROM
    DUAL
WHERE NOT EXISTS (
	SELECT `conf_uuid`, `enterprise_id`, `conf_type`
	FROM `fs_config`
	WHERE `conf_type`='UNIMRCP' AND `enterprise_id`='global_enterprise_id'
);

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
