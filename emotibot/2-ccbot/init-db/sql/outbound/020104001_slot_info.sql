-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE IF NOT EXISTS `slots_info` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `session_id` varchar(50) NOT NULL COMMENT '会话id',
  `slots` text NOT NULL COMMENT '槽位信息',
  `slots_time` varchar(30) NOT NULL COMMENT '槽位信息时间',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_session_id` (`session_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='槽位信息表';
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back