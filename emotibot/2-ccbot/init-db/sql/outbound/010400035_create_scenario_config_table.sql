-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for scenario_config
-- ----------------------------

CREATE TABLE `scenario_config` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `enterprise_id` varchar(32) NOT NULL COMMENT '企业id',
  `app_id` varchar(32) NOT NULL COMMENT '机器人id',
  `scenario_id` varchar(64) NOT NULL COMMENT '场景id',
  `asr_service` varchar(128) NOT NULL COMMENT 'asr 服务的配置uuid',
  `asr_vsp` text COMMENT 'asr 厂商特定参数',
  `tts_service` varchar(128) NOT NULL COMMENT 'tts 服务的配置uuid',
  `to_human` varchar(128) DEFAULT NULL COMMENT '转人工的地址',
  `barge_in` tinyint(1) NOT NULL COMMENT '是否支持打断',
  `update_time` bigint(10) unsigned NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COMMENT='场景特定参数表';

-- ----------------------------
-- Records of scenario_config
-- ----------------------------
BEGIN;
INSERT INTO `scenario_config` (`enterprise_id`, `app_id`, `scenario_id`, `asr_service`, `asr_vsp`, `tts_service`, `to_human`, `barge_in`, `update_time`) VALUES ('default', 'tempate', 'template', 'ALIBABA_ASR', 'SPEECH_CUSTOM_ID=demo-vocab;SPEECH_VOCAB_ID=demo-vocab;SPEECH_CLASS_VOCAB_ID=demo-vocab', 'template', 'template', 0, 1);
COMMIT;
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `scenario_config`;

