
-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for fs_list
-- ----------------------------

CREATE TABLE `fs_list` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `fs_uuid` varchar(64) NOT NULL,
  `fs_host_ip` varchar(64) NOT NULL,
  `fs_host_port` varchar(5) DEFAULT '',
  `register_time` bigint(20) NOT NULL,
  `status` tinyint(2) unsigned DEFAULT NULL,
  `inbound_capacity` smallint(5) unsigned DEFAULT NULL,
  `inprogress_inbound_calls` smallint(5) unsigned DEFAULT NULL,
  `inprogress_outbound_calls` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `fs_uuid` (`fs_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='free switch list configuration';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `fs_list`;