-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `call_record` ADD COLUMN `call_type` varchar(32) NOT NULL DEFAULT "" COMMENT 'call_type value should be "recall, additonalCall, empty"';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `call_record` DROP COLUMN `call_type`;
