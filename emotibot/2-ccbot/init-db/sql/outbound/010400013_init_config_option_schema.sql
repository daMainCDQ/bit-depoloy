-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for config_options
-- ----------------------------
CREATE TABLE `config_options` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'unique id of each row auto increment',
  `enterprise_id` varchar(32) NOT NULL COMMENT '企业id',
  `catalog_name` varchar(32) NOT NULL COMMENT '类别名称，比如脱敏，区域',
  `config_item` varchar(32) NOT NULL COMMENT '配置项',
  `item_description` varchar(255) DEFAULT NULL COMMENT '配置项的描述',
  `value` varchar(400) DEFAULT NULL COMMENT '最多四百个字符',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COMMENT='each row records the global configration';


-- ----------------------------
-- Records of config_options
-- ----------------------------
BEGIN;
INSERT INTO `config_options` VALUES (1, 'default', '脱敏', 'switch', '脱敏开关', 'off');
INSERT INTO `config_options` VALUES (2, 'default', '脱敏', 'begin_pos', '脱敏开始位置，第几位，从1开始计数', '4');
INSERT INTO `config_options` VALUES (3, 'default', '脱敏', 'length', '脱敏长度', '4');
INSERT INTO `config_options` VALUES (4, 'default', 'TTS预生成', 'switch', 'TTS预生成开关', 'off');
INSERT INTO `config_options` VALUES (5, 'default', '区域', 'country', '国家', '中国');
INSERT INTO `config_options` VALUES (6, 'default', '区域', 'province', '省', '');
INSERT INTO `config_options` VALUES (7, 'default', '区域', 'city', '城市', '');
INSERT INTO `config_options` VALUES (8, 'default', '不呼叫时段', 'dts_periods', 'DTC, Do not call不呼叫时段，支持多个不呼叫时段', '');
COMMIT;
        
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `config_options`;


