-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `outbound`.`fs_config` ADD COLUMN `gw_add_zero` tinyint(3) NULL DEFAULT 0 COMMENT '该字段仅针对网关，0：不加0；1：针对外地号码加拨0；默认为0';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `outbound`.`fs_config` DROP COLUMN `gw_add_zero`;
