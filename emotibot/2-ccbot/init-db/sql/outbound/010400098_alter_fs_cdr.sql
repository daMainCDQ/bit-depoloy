-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `outbound`.`call_list` ADD COLUMN `priority` tinyint(4) NOT NULL DEFAULT '0' COMMENT '通话优先级设定，0-9';

DROP TABLE IF EXISTS `outbound.call_detail_record`;

CREATE TABLE `fs_call_detail_records` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uuid` char(36) NOT NULL COMMENT 'session id or freeswitch uuid',
  `call_id` bigint(20) NOT NULL COMMENT 'call id',
  `call_begin_time` bigint(20) NOT NULL COMMENT 'call begin time',
  `ringing_start_time` bigint(20) NOT NULL COMMENT 'ring start time',
  `ringing_end_time` bigint(20) NOT NULL COMMENT 'ring end time',
  `call_end_time` bigint(20) NOT NULL COMMENT 'call end time',
  `status` tinyint(4) NOT NULL COMMENT 'call status',
  `call_direction` varchar(20) NOT NULL COMMENT 'call direction',
  `answer_state` varchar(255) NOT NULL DEFAULT '' COMMENT 'save answer process',
  `caller_number` varchar(255) NOT NULL COMMENT 'caller number, maybe a sip address',
  `callee_number` varchar(255) NOT NULL COMMENT 'callee number',
  `data` text NOT NULL COMMENT 'extra info abount call',
  `finished_flag` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'call finished flag',
  `fs_id` char(32) NOT NULL COMMENT 'freeswitch id',
  PRIMARY KEY (`id`),
  KEY `idx_all` (`call_id`,`status`,`uuid`)
) ENGINE=InnoDB AUTO_INCREMENT=193 DEFAULT CHARSET=utf8mb4;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back