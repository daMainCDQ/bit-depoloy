-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `callout_phone` ADD COLUMN `enterprise_id` varchar(32) NOT NULL DEFAULT '' COMMENT 'enterprise_id';


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `callout_phone` DROP COLUMN `enterprise_id`;

