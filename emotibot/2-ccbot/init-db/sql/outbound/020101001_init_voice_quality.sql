-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for tts_session_global_variable
-- ----------------------------
DROP TABLE IF EXISTS `voice_quality`;
CREATE TABLE `voice_quality`
(
  `id` bigint
(20) NOT NULL AUTO_INCREMENT COMMENT '呼叫记录id',
  `uuid` varchar
(64) NOT NULL COMMENT 'freeswitch_uuid',
  `cdr_id` bigint
(20) NOT NULL COMMENT 'voip monitor cdr通话记录id',
  `caller` varchar
(32) NOT NULL COMMENT '主叫电话',
  `called` varchar
(32) NOT NULL COMMENT '被叫电话',
  `a_lost` mediumint
(8) DEFAULT NULL COMMENT '主叫端丢包数',
  `a_mos_silence_mult10` mediumint
(8) DEFAULT NULL COMMENT '主叫端mos均值',
  `a_d50` mediumint
(8) DEFAULT NULL COMMENT '\n主叫PDV间隔50~70ms的数量\n\n主叫PDV间隔50~70ms的数量\n主叫PDV间隔50~70ms的数量',
  `a_d70` mediumint
(8) DEFAULT NULL,
  `a_d90` mediumint
(8) DEFAULT NULL,
  `a_d120` mediumint
(8) DEFAULT NULL,
  `a_d200` mediumint
(8) DEFAULT NULL,
  `a_d300` mediumint
(8) DEFAULT NULL,
  `b_lost` mediumint
(8) DEFAULT NULL,
  `b_mos_silence_mult10` mediumint
(8) DEFAULT NULL,
  `b_d50` mediumint
(8) DEFAULT NULL,
  `b_d70` mediumint
(8) DEFAULT NULL,
  `b_d90` mediumint
(8) DEFAULT NULL,
  `b_d120` mediumint
(8) DEFAULT NULL,
  `b_d200` mediumint
(8) DEFAULT NULL,
  `b_d300` mediumint
(8) DEFAULT NULL,
  `created_time` datetime NOT NULL,
  PRIMARY KEY
(`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4;


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `voice_quality`;