-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `outbound`.`config_options` CHANGE `range` `range` text NULL DEFAULT NULL COMMENT '取值范围';

BEGIN;
INSERT INTO `outbound`.`config_options` (`enterprise_id`, `catalog_name`, `config_item`, `value`, `range`) VALUES ('default', 'call_status', 'none', '', '[{\"status\":0,\"cn\":\"接通(用户挂机)\",\"tw\":\"\",\"en\":\"\"},{\"status\":1,\"cn\":\"接通(机器人挂机)\",\"tw\":\"\",\"en\":\"\"},{\"status\":22,\"cn\":\"接通(转人工)\",\"tw\":\"\",\"en\":\"\"},{\"status\":11,\"cn\":\"拒接/忙线\",\"tw\":\"\",\"en\":\"\"},{\"status\":12,\"cn\":\"无人接听\",\"tw\":\"\",\"en\":\"\"},{\"status\":13,\"cn\":\"关机/飞行模式\",\"tw\":\"\",\"en\":\"\"},{\"status\":17,\"cn\":\"不在服务区\",\"tw\":\"\",\"en\":\"\"},{\"status\":18,\"cn\":\"空号\",\"tw\":\"\",\"en\":\"\"},{\"status\":19,\"cn\":\"停机\",\"tw\":\"\",\"en\":\"\"},{\"status\":20,\"cn\":\"外地号码\",\"tw\":\"\",\"en\":\"\"},{\"status\":21,\"cn\":\"未匹配DA\",\"tw\":\"\",\"en\":\"\"},{\"status\":23,\"cn\":\"待识别\",\"tw\":\"\",\"en\":\"\"},{\"status\":10,\"cn\":\"网关异常\",\"tw\":\"\",\"en\":\"\"},{\"status\":100,\"cn\":\"正常结束\",\"tw\":\"\",\"en\":\"\"},{\"status\":101,\"cn\":\"用户听导航菜单时挂机\",\"tw\":\"\",\"en\":\"\"},{\"status\":102,\"cn\":\"排队溢出\",\"tw\":\"\",\"en\":\"\"},{\"status\":103,\"cn\":\"排队时挂机\",\"tw\":\"\",\"en\":\"\"},{\"status\":104,\"cn\":\"排队超时\",\"tw\":\"\",\"en\":\"\"},{\"status\":105,\"cn\":\"振铃时主叫挂机\",\"tw\":\"\",\"en\":\"\"},{\"status\":110,\"cn\":\"监听结束\",\"tw\":\"\",\"en\":\"\"},{\"status\":111,\"cn\":\"播工号时用户挂机\",\"tw\":\"\",\"en\":\"\"},{\"status\":112,\"cn\":\"播工号时坐席挂机\",\"tw\":\"\",\"en\":\"\"},{\"status\":114,\"cn\":\"转移至外部号码（IVR导航转外线）\",\"tw\":\"\",\"en\":\"\"},{\"status\":199,\"cn\":\"未知原因\",\"tw\":\"\",\"en\":\"\"}]');
UPDATE `outbound`.`config_options` SET `value` = '[11,12,13,17]', `range` = '[11,12,13,17]' WHERE `enterprise_id` = 'default' and `catalog_name`='auto_redial' and `config_item`='fail_redial';
COMMIT;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DELETE FROM `outbound`.`config_options` WHERE `catalog_name`='call_status' and `enterprise_id`='default';