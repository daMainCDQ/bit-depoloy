
-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Records of fs_config
-- ----------------------------
BEGIN;
DELETE FROM `fs_config` WHERE `enterprise_id` = 'template';
INSERT INTO `fs_config` (`conf_uuid`, `enterprise_id`, `conf_type`, `sub_type`, `conf_name`, `xml_content`, `enable`, `authorized_num`) VALUES ('b5fea08512b047b49e9031b4432b4bb9', 'template', 'ASR', 'ALIBABA_ASR', 'alibaba_asr', '<include>
  <!-- UniMRCP Server MRCPv2 -->
  <profile name="alibaba_asr" version="2">
    <param name="server-ip" value="请替换为阿里巴巴ASRMRCP服务器的IP"/>
    <param name="server-port" value="7010"/>

    <param name="client-ip" value="FS_HOST_IP_REPLACE"/>
    <param name="client-port" value="8098"/>
    <param name="sip-transport" value="tcp"/>

    <!--param name="rtp-ext-ip" value="auto"/-->
    <param name="rtp-ip" value="FS_HOST_IP_REPLACE"/>
    <param name="rtp-port-min" value="12001"/>
    <param name="rtp-port-max" value="13000"/>
    <!--param name="playout-delay" value="50"/-->
    <!--param name="max-playout-delay" value="200"/-->
    <!--param name="ptime" value="20"/-->
    <param name="codecs" value="PCMU PCMA L16/96/8000 telephone-event/101/8000 PCMU/97/16000 PCMA/98/16000 L16/99/16000 telephone-event/102/16000"/>

    <!-- Add any default MRCP params for RECOGNIZE requests here -->
    <recogparams>
      <param name="start-input-timers" value="true"/>
      <param name="speech-complete-timeout" value="1300"/>
    </recogparams>
  </profile>
</include>
', '0', '0');

INSERT INTO `fs_config` (`conf_uuid`, `enterprise_id`, `conf_type`, `sub_type`, `conf_name`, `xml_content`, `enable`, `authorized_num`) VALUES ('6b588be429ac407da3f608d0d7f55370', 'template', 'TTS', 'ALIBABA_TTS', 'alibaba_tts', '<include>
  <!-- UniMRCP Server MRCPv2 -->
  <profile name="alibaba_tts" version="2">
    <param name="server-ip" value="请替换为阿里巴巴TTSMRCP服务器的IP"/>
    <param name="server-port" value="7010"/>

    <param name="client-ip" value="FS_HOST_IP_REPLACE"/>
    <param name="client-port" value="8097"/>
    <param name="sip-transport" value="tcp"/>

    <!--param name="rtp-ext-ip" value="auto"/-->
    <param name="rtp-ip" value="FS_HOST_IP_REPLACE"/>
    <param name="rtp-port-min" value="12001"/>
    <param name="rtp-port-max" value="13000"/>
    <!--param name="playout-delay" value="50"/-->
    <!--param name="max-playout-delay" value="200"/-->
    <!--param name="ptime" value="20"/-->
    <param name="codecs" value="PCMU PCMA L16/96/8000 telephone-event/101/8000 PCMU/97/16000 PCMA/98/16000 L16/99/16000 telephone-event/102/16000"/>

    <!-- Add any default MRCP params for SPEAK requests here -->
    <synthparams>
      <voice value="siqi"/>
    </synthparams>
  </profile>
</include>
', '0', '0');
INSERT INTO `fs_config` (`conf_uuid`, `enterprise_id`, `conf_type`, `sub_type`, `conf_name`, `xml_content`, `enable`, `authorized_num`) VALUES ('4184b151c81a444ba13c8b2f154271d4', 'template', 'UNIMRCP', '', 'unimrcp.conf', '<configuration name="unimrcp.conf" description="UniMRCP Client">
  <settings>
    <!-- UniMRCP profile to use for TTS -->
    <param name="default-tts-profile" value="请替换为您想默认使用TTS的配置文件名"/>
    <!-- UniMRCP profile to use for ASR -->
    <param name="default-asr-profile" value="请替换为您想默认使用ASR的配置文件名"/>
    <!-- UniMRCP logging level to appear in freeswitch.log.  Options are:
         EMERGENCY|ALERT|CRITICAL|ERROR|WARNING|NOTICE|INFO|DEBUG -->
    <param name="log-level" value="DEBUG"/>
    <!-- Enable events for profile creation, open, and close -->
    <param name="enable-profile-events" value="false"/>

    <param name="max-connection-count" value="100"/>
    <param name="offer-new-connection" value="1"/>
    <param name="request-timeout" value="6000"/>
  </settings>

  <profiles>
    <X-PRE-PROCESS cmd="include" data="../mrcp_profiles/*.xml"/>
  </profiles>

</configuration>', '0', '0');
INSERT INTO `fs_config` (`conf_uuid`, `enterprise_id`, `conf_type`, `sub_type`, `conf_name`, `xml_content`, `enable`, `authorized_num`) VALUES ('95ff6461814b43c9bbc8087a5f902d98', 'template', 'EXTENSION', '', 'EXTENSION_ID_REPLACE', '<include>
  <user id="EXTENSION_ID_REPLACE">
    <params>
      <param name="password" value="$${default_password}"/>
      <param name="vm-password" value="EXTENSION_ID_REPLACE"/>
    </params>
    <variables>
      <variable name="toll_allow" value="domestic,international,local"/>
      <variable name="accountcode" value="EXTENSION_ID_REPLACE"/>
      <variable name="user_context" value="default"/>
      <variable name="effective_caller_id_name" value="Extension EXTENSION_ID_REPLACE"/>
      <variable name="effective_caller_id_number" value="1000"/>
      <variable name="outbound_caller_id_name" value="$${outbound_caller_name}"/>
      <variable name="outbound_caller_id_number" value="$${outbound_caller_id}"/>
      <variable name="callgroup" value="techsupport"/>
    </variables>
  </user>
</include>
', '0', '0');
INSERT INTO `fs_config` (`conf_uuid`, `enterprise_id`, `conf_type`, `sub_type`, `conf_name`, `xml_content`, `enable`, `authorized_num`) VALUES ('e1469aca7292441ea5ad8fac0c43d0ea', 'template', 'VOICE_GATEWAY', '', 'example', '<include>
  <!--/// 请将下面一行example替换为您的网关名称 ///-->
  <gateway name="example">
  <param name="realm" value="请替换为网关的IP或(和)端口形如IP or IP:PORT"/>
  <!--/// 如果需要注册，register的value为true，同时配置 username和password ///-->
  <param name="register" value="false"/>
  <!--param name="username" value="请替换为网关注册的用户名"/-->
  <!--param name="password" value="请替换为网关注册的密码"/-->
  <!--/// 以下配置项为可选项 ///-->
  <!--/// auth realm: *optional* same as gateway name, if blank ///-->
  <!--/// username to use in from: *optional* same as  username, if blank ///-->
  <!--<param name="from-user" value="cluecon"/>-->
  <!--/// domain to use in from: *optional* same as  realm, if blank ///-->
  <!--<param name="from-domain" value="asterlink.com"/>-->
  <!--/// extension for inbound calls: *optional* same as username, if blank ///-->
  <!--<param name="extension" value="cluecon"/>-->
  <!--/// proxy host: *optional* same as realm, if blank ///-->
  <!--<param name="proxy" value="asterlink.com"/>-->
  <!--/// send register to this proxy: *optional* same as proxy, if blank ///-->
  <!--<param name="register-proxy" value="mysbc.com"/>-->
  <!--/// expire in seconds: *optional* 3600, if blank ///-->
  <param name="expire-seconds" value="60"/>
  <!-- which transport to use for register -->
  <!--<param name="register-transport" value="udp"/>-->
  <!--How many seconds before a retry when a failure or timeout occurs -->
  <!--<param name="retry-seconds" value="30"/>-->
  <!--Use the callerid of an inbound call in the from field on outbound calls via this gateway -->
  <!--<param name="caller-id-in-from" value="false"/>-->
  <!--extra sip params to send in the contact-->
  <!--<param name="contact-params" value=""/>-->
  <!-- Put the extension in the contact -->
  <!--<param name="extension-in-contact" value="true"/>-->
  <!--send an options ping every x seconds, failure will unregister and/or mark it down-->
  <!--<param name="ping" value="25"/>-->
  <!--<param name="cid-type" value="rpid"/>-->
  <!--rfc5626 : Abilitazione rfc5626 ///-->
  <!--<param name="rfc-5626" value="true"/>-->
  <!--rfc5626 : extra sip params to send in the contact-->
  <!--<param name="reg-id" value="1"/>-->
  </gateway>
</include>', '0', '0');
INSERT INTO `fs_config` (`conf_uuid`, `enterprise_id`, `conf_type`, `sub_type`, `conf_name`, `xml_content`, `enable`, `authorized_num`) VALUES ('2c8933037ca143fb9d06f028f400f146', 'template', 'ASR', 'IFLYTEK_ASR', 'iflytek-asr', '<include>
<profile name="iflytek-asr" version="2">
    <param name="client-ext-ip" value="FS_HOST_IP_REPLACE"/>
    <param name="client-ip" value="FS_HOST_IP_REPLACE"/>
    <param name="client-port" value="8090"/>

    <param name="server-ip" value="请替换为科大讯飞ASRMRCP服务器的IP"/>
    <param name="server-port" value="5060"/>
    <!--param name="force-destination" value="1"/-->
    <param name="sip-transport" value="tcp"/>
    <param name="ua-name" value="TVP"/>
    <param name="sdp-origin" value="TVP"/>
    <!--
    <param name="rtp-ext-ip" value="auto"/>
    -->
    <param name="rtp-ip" value="FS_HOST_IP_REPLACE"/>
    <param name="rtp-port-min" value="12001"/>
    <param name="rtp-port-max" value="13000"/>
<!-- enable/disable rtcp support -->
    <param name="rtcp" value="1"/>
<!-- rtcp bye policies (rtcp must be enabled first)
     0 - disable rtcp bye
     1 - send rtcp bye at the end of session
     2 - send rtcp bye also at the end of each talkspurt (input)
-->
    <param name="rtcp-bye" value="2"/>
    <!-- rtcp transmission interval in msec (set 0 to disable) -->
    <param name="rtcp-tx-interval" value="5000"/>
    <!-- period (timeout) to check for new rtcp messages in msec (set 0 to disable) -->
    <param name="rtcp-rx-resolution" value="1000"/>
    <!--param name="playout-delay" value="50"/-->
    <!--param name="max-playout-delay" value="200"/-->
    <param name="ptime" value="20"/>
    <param name="codecs" value="PCMA PCMU L16/96/8000"/>
    <param name="jsgf-mime-type" value="application/jsgf"/>

    <!-- Add any default MRCP params for RECOGNIZE requests here -->
    <recogparams>
      <grammar value="ahlt_ats"/>
      <param name="N-Best-List-Length" value="1"/>
      <param name="Recognition-Timeout" value="100000"/>
      <param name="Speech-Complete-Timeout" value="2000"/>
      <param name="Confidence-Threshold" value="0.01"/>
      <param name="Start-Input-Timers" value="true"/>
      <param name="Recognition-Mode" value="normal"/>
      <param name="Speech-language" value="zh-CN"/>
    </recogparams>
  </profile>
</include>
', '0', '0');
INSERT INTO `fs_config` (`conf_uuid`, `enterprise_id`, `conf_type`, `sub_type`, `conf_name`, `xml_content`, `enable`, `authorized_num`) VALUES ('20c588c6be4143f98488d49729761542', 'template', 'TTS', 'IFLYTEK_TTS', 'iflytek-tts', '<include>
<profile name="iflytek-tts" version="2">
    <param name="client-ext-ip" value="FS_HOST_IP_REPLACE"/>
    <param name="client-ip" value="FS_HOST_IP_REPLACE"/>
    <param name="client-port" value="8091"/>

    <param name="server-ip" value="请替换为科大讯飞TTSMRCP服务器的IP"/>
    <param name="server-port" value="5060"/>
    <!--param name="force-destination" value="1"/-->
    <param name="sip-transport" value="tcp"/>
    <param name="ua-name" value="TVP"/>
    <param name="sdp-origin" value="TVP"/>
    <!--
    <param name="rtp-ext-ip" value="auto"/>
    -->
    <param name="rtp-ip" value="FS_HOST_IP_REPLACE"/>
    <param name="rtp-port-min" value="12001"/>
    <param name="rtp-port-max" value="13000"/>
<!-- enable/disable rtcp support -->
    <param name="rtcp" value="1"/>
<!-- rtcp bye policies (rtcp must be enabled first)
     0 - disable rtcp bye
     1 - send rtcp bye at the end of session
     2 - send rtcp bye also at the end of each talkspurt (input)
-->
    <param name="rtcp-bye" value="2"/>
    <!-- rtcp transmission interval in msec (set 0 to disable) -->
    <param name="rtcp-tx-interval" value="5000"/>
    <!-- period (timeout) to check for new rtcp messages in msec (set 0 to disable) -->
    <param name="rtcp-rx-resolution" value="1000"/>
    <!--param name="playout-delay" value="50"/-->
    <!--param name="max-playout-delay" value="200"/-->
    <param name="ptime" value="20"/>
    <param name="codecs" value="PCMA PCMU L16/96/8000"/>
    <param name="jsgf-mime-type" value="application/jsgf"/>
    <!-- Add any default MRCP params for SPEAK requests here -->
    <synthparams>
        <voice value="xiaoyan"/>
    </synthparams>
  </profile>
</include>
', '0', '0');
INSERT INTO `fs_config` (`conf_uuid`, `enterprise_id`, `conf_type`, `sub_type`, `conf_name`, `xml_content`, `enable`, `authorized_num`) VALUES ('1ac2f2c9e86d4c9f8ad7d7d5eeb70451', 'template', 'ASR', 'EMOTIBOT_ASR', 'emotibot', '<include>
  <!-- UniMRCP Server MRCPv2 -->
  <profile name="emotibot" version="2">
    <param name="server-ip" value="请替换为竹间智能MRCP服务器的IP"/>
    <param name="server-port" value="8060"/>

    <param name="client-ip" value="FS_HOST_IP_REPLACE"/>
    <param name="client-port" value="8062"/>
    <param name="sip-transport" value="tcp"/>

    <!--param name="rtp-ext-ip" value="auto"/-->
    <param name="rtp-ip" value="FS_HOST_IP_REPLACE"/>
    <param name="rtp-port-min" value="12001"/>
    <param name="rtp-port-max" value="13000"/>
    <!--param name="playout-delay" value="50"/-->
    <!--param name="max-playout-delay" value="200"/-->
    <!--param name="ptime" value="20"/-->
    <param name="codecs" value="PCMU PCMA L16/96/8000 telephone-event/101/8000 PCMU/97/16000 PCMA/98/16000 L16/99/16000 telephone-event/102/16000"/>

    <!-- Add any default MRCP params for RECOGNIZE requests here -->
    <recogparams>
      <param name="speech-language" value="en-emotibot"/>
      <param name="session-id" value="auto"/>
      <param name="start-input-timers" value="true"/>
    </recogparams>
  </profile>
</include>', '0', '0');
INSERT INTO `fs_config` (`conf_uuid`, `enterprise_id`, `conf_type`, `sub_type`, `conf_name`, `xml_content`, `enable`, `authorized_num`) VALUES ('dd5f4d8010de4bda85be27f56d51267f', 'template', 'TTS', 'EMOTIBOT_TTS', 'emotibot_tts', '<include>
  <!-- UniMRCP Server MRCPv2 -->
  <profile name="emotibot" version="2">
    <param name="server-ip" value="请替换为竹间智能MRCP服务器的IP"/>
    <param name="server-port" value="8060"/>

    <param name="client-ip" value="FS_HOST_IP_REPLACE"/>
    <param name="client-port" value="8062"/>
    <param name="sip-transport" value="tcp"/>

    <!--param name="rtp-ext-ip" value="auto"/-->
    <param name="rtp-ip" value="FS_HOST_IP_REPLACE"/>
    <param name="rtp-port-min" value="12001"/>
    <param name="rtp-port-max" value="13000"/>
    <!--param name="playout-delay" value="50"/-->
    <!--param name="max-playout-delay" value="200"/-->
    <!--param name="ptime" value="20"/-->
    <param name="codecs" value="PCMU PCMA L16/96/8000 telephone-event/101/8000 PCMU/97/16000 PCMA/98/16000 L16/99/16000 telephone-event/102/16000"/>

    <!-- Add any default MRCP params for SPEAK requests here -->
    <synthparams>
    </synthparams>

  </profile>
</include>
', '0', '0');
COMMIT;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DELETE FROM `fs_config` WHERE `enterprise_id` = 'template';