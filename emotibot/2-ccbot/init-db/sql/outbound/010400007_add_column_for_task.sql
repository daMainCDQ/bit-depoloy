-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `task` ADD COLUMN `asr_name` VARCHAR(64) NOT NULL DEFAULT '' AFTER `scenario_id`;
ALTER TABLE `task` ADD COLUMN `tts_name` VARCHAR(64) NOT NULL DEFAULT '' AFTER `asr_name`;


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

ALTER TABLE `task` DROP COLUMN `asr_name`;
ALTER TABLE `task` DROP COLUMN `tts_name`;





