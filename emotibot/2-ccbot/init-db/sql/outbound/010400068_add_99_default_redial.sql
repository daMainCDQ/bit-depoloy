-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
BEGIN;
UPDATE `outbound`.`config_options` SET `value` = '[11,12,13,17,21,99]', `range` = '[11,12,13,17,21,99]' WHERE `enterprise_id` = 'default' and `catalog_name`='auto_redial' and `config_item`='fail_redial';
UPDATE `outbound`.`config_options` SET `range` = '[11,12,13,17,21,99]' WHERE `catalog_name`='auto_redial' and `config_item`='fail_redial';
COMMIT;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
