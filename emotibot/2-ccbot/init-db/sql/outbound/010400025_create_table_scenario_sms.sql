-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for scenario_sms
-- ----------------------------

CREATE TABLE `scenario_sms` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `conf_uuid` varchar(32) NOT NULL,
  `enterprise_id` varchar(32) NOT NULL,
  `app_id` varchar(32) NOT NULL,
  `scenario_id` varchar(64) NOT NULL,
  `sms_platform` tinyint(3) NOT NULL COMMENT 'sms 平台代码',
  `app_key` varchar(256) NOT NULL COMMENT 'json array格式，存储了accesskey id和appkey appsecret',
  `app_secret` varchar(128) NOT NULL,
  `sms_sign` varchar(128) NOT NULL COMMENT '签名名称和短信签名共用',
  `sms_channel` varchar(128) NOT NULL COMMENT '短信通道号',
  `sms_template_id` varchar(128) NOT NULL COMMENT '短信模板id',
  `sms_template_content` text NOT NULL COMMENT '模板内容',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid_index` (`conf_uuid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='场景短信模板';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `scenario_sms`;