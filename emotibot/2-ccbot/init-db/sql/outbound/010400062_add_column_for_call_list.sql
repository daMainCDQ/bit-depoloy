-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `call_list` ADD COLUMN `additional_call_status` varchar(32) NOT NULL DEFAULT "" COMMENT 'additional call status value should be "called, waitToCall, empty"';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `call_list` DROP COLUMN `additional_call_status`;
