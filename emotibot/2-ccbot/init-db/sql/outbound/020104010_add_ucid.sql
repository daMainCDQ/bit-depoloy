-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `outbound`.`call_record` ADD COLUMN `ucid` varchar(64) NOT NULL COMMENT '对话ucid';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
