-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `callout_phone` ADD COLUMN `line_location_province` VARCHAR(50) NOT NULL DEFAULT '';
ALTER TABLE `callout_phone` ADD COLUMN `line_type` TINYINT(2) NOT NULL DEFAULT '0';
ALTER TABLE `callout_phone` ADD COLUMN `line_location_city` VARCHAR(50) NOT NULL DEFAULT '';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

ALTER TABLE `callout_phone` DROP COLUMN `line_location_province`;
ALTER TABLE `callout_phone` DROP COLUMN `line_type`;
ALTER TABLE `callout_phone` DROP COLUMN `line_location_city`;