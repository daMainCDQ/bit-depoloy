
-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for tts_scenario_text
-- ----------------------------
CREATE TABLE `tts_scenario_text` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `common_id` bigint(11) NOT NULL COMMENT 'tts常量话术表 主键',
  `enterprise_id` varchar(50) NOT NULL COMMENT '企业id',
  `app_id` varchar(50) NOT NULL COMMENT '机器人id',
  `scenario_id` varchar(50) NOT NULL COMMENT '场景id',
  `text` text NOT NULL COMMENT '常量话术',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `udx_union` (`enterprise_id`,`app_id`,`scenario_id`,`text`(50)) USING BTREE,
  KEY `idx_common_id` (`common_id`) USING BTREE,
  KEY `idx_text` (`text`(50))
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='tts场景话术表';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `tts_scenario_text`;