# 功能更新

## 2020-05-19

- 任务实时监控后端接口

- 任务自动追打后端接口

- 通时报表流水改造

- 新增通时图表功能

- 任务一键复制功能
 
## 2020-05-22                
- 外呼改为使用bf license

## 2020-06-22                
- cc-ext更新：tts支持多任务优化  

- 支持录音分句回听

- 增加regression服务            
                        
                        
#bug 修复
                                                   
## 2020-05-12   
- 修复多轮槽位信息数量超过1万条时，无法获取完整多轮槽位数据的错误    yuhanyang

- BFTWO-2495 配置好的线路，获取不了线路资源                      quanqigu

## 2020-05-18 

- 修复oppo项目反馈的xml文本格式解析问题                           wenkong

## 2020-05-22                
- 解决上传预录音文件大于1M时的报错                                yuhanyang

## 2020-05-25               
- 解决预录音上传时长为0的bug                                     xiaogezhu

- cc-ext去除同步标准问时appid参数强制校验                         xiaogezhu

- 解决前端任务修改的报错                                         yuhanyang

- 解决前端任务创建时下载模版错误                                  yuhanyang

- 解决任务追打信息统计错误                                        yuhanyang

- 去除fs编译选项o3;mod_audio_send添加caller与callee两个字段;luascript的url改为v2（需要config-manager配合);tts请求添加task-id字段     xideng

- 变更呼入时获取的分机号变量                                      quanqigu

- 获取线路问题处理                                               quanqigu

- config-manager服务更新                                        quanqigu

## 2020-05-26 
- 解决bug BFTWO-2723                                           zhuxiaoge

## 2020-05-27
- fs加入curl的超时时间，设为10秒;修复tts请求添加task-id字段导致电话挂断的bug dengxi

## 2020-05-28
- call-center-api 解决通话结果流水导出数据不一致的问题              yuhanyang

## 2020-06-01
- mrcp更新，修复了通话过程中出现语音合成失败                         kongwen


## 2020-06-12
- BFTWO-2954、BFTWO-3042、BFTWO-3136                             quanqigu


## 2020-06-15
- 获取任务详情接口追打时间统一显示为分钟                             yuhanyang
 
- 数据展板任务信息 接通数按照call_id去重                            yuhanyang

-  FIX BFTWO-2608                                               quanqigu

## 2020-06-22

- mrcp 更新到1.6.0版本                                            kongwen

#其他修改

## 2020-05-26 
- init-db 更新到010400085                                       yuhanyang

