#!/bin/bash

# 根据ASR_HOST_INFO节点信息生成asr前端代理haproxy的后端配置
gen_yaml_project (){

  ASR_MODULE_YAML="emotibot/11-asr_recognize/.module.yaml"

  for i in `echo $ASR_HOST_INFO | awk -F ',' '{for(i=1;i<=NF;i++){print $i}}'`
  do

    ASR_HOST=`echo $i | awk -F '#' '{print $1}'`
    ASR_HOST_THREAD=`echo $i | awk -F '#' '{print $2}'`

    sed -i "s/timeout http-request 10m/&\\\\n  server ${ASR_HOST} ${ASR_HOST}:${VOICE_MASTER_PORT} check weight 50 maxconn ${ASR_HOST_THREAD}/" $ASR_MODULE_YAML

  done

}

if [ $# -eq 1 ]; then
    mode=$1
    if  [[ $mode == "option" ]]; then
        echo "11 "
    elif [[ $mode == "11" ]]; then
        echo "asr_recognize"
    fi
fi
