#!/bin/bash
set -e

# Standalone QABOT deployemnt for temporary use

WORKDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
ROOTDIR="$(cd "${WORKDIR}"/../../ && pwd)"
COLOR_RED=$'\e[1;31m'
COLOR_GREEN=$'\e[1;32m'
COLOR_END=$'\e[0m'

INFRA_YAML_FILEPATH="${ROOTDIR}/infra/infra.yaml"
INFRA_PORT_YAML_FILEPATH="${ROOTDIR}/infra/port.yaml"
DB_LIST="mysql redis consul license-hardware minio rabbitmq"
DEPLOY_INFRA_OPTS="-f ${INFRA_YAML_FILEPATH} -f ${INFRA_PORT_YAML_FILEPATH}"

# 1-bf yaml
BF_MODULE_YAML_FILEPATH="${ROOTDIR}/emotibot/1-bf/module.yaml"
BF_PORT_YAML_FILEPATH="${ROOTDIR}/emotibot/1-bf/port.yaml"
BF_LIST="license-controller admin-api common-resources-ui admin-ui load-balance permission-managment admin-auth common-help-docs"
DEPLOY_BF_OPTS="-f ${BF_MODULE_YAML_FILEPATH} -f ${BF_PORT_YAML_FILEPATH}"

trap handler EXIT

function handler() {
  local rc="$?"
  if [[ "${rc}" != '0' ]]; then
    echo "error occurs" 1>&2
    if [[ -n "${ERR_MSG}" ]]; then
      echo "${ERR_MSG}" 1>&2
    fi
  fi

  exit "${rc}"
}

function format_banner () {
  local length1="${#1}"
  let length1-=6
  local length2="${#2}"
  let length2-=42
  printf "[ -------- ${1}%${length1#-}s ${2}%${length2#-}s --------] \n"
}


function loadenv(){
  set -o allexport
  source "${ROOTDIR}"/dev.env
  source "${ROOTDIR}"/emotibot/1-bf/dev.env
  source "${ROOTDIR}"/emotibot/9-asr/sa.env
  set +o allexport
}


function deploy(){
  cd ${ROOTDIR}

  ## custom setting
  sed -i 's/ZABBIX=deploy/ZABBIX=deploy-no/g' "${ROOTDIR}"/dev.env
  sed -i 's/EFK_DEPLOY=true/EFK_DEPLOY=false/g' "${ROOTDIR}"/dev.env

  for i in `ls ${ROOTDIR}/script/*.sh` ; do source $i ; done
  refresh_env
  deploy_befor
  loadenv
  echo 'Start container in infra...'
  cmd="docker-compose \
    ${DEPLOY_INFRA_OPTS} \
    up -d ${DB_LIST}"
  echo $cmd && eval $cmd

  initdb_yaml_filepath="${ROOTDIR}"/infra/.init-db.yaml
  cp "${ROOTDIR}"/infra/init-db.yaml $initdb_yaml_filepath
  if [[ -f "${initdb_yaml_filepath}" ]]; then
    echo 'Init common DB schema...'
    sed -i 's/INIT_ES_INIT: "true"/INIT_ES_INIT: "false"/g' "${initdb_yaml_filepath}"
    sed -i 's/INIT_ES_INIT=true/INIT_ES_INIT=false/g' "${initdb_yaml_filepath}"
    sed -i 's/INIT_ES_LOG_INIT: "true"/INIT_ES_LOG_INIT: "false"/g' "${initdb_yaml_filepath}"
    sed -i 's/INIT_ES_LOG_INIT=true/INIT_ES_LOG_INIT=false/g' "${initdb_yaml_filepath}"
    sed -i 's/INIT_MINIO_INIT: "true"/INIT_MINIO_INIT: "false"/g' "${initdb_yaml_filepath}"
    sed -i 's/INIT_MINIO_INIT=true/INIT_MINIO_INIT=false/g' "${initdb_yaml_filepath}"
    cmd="docker-compose -f ${initdb_yaml_filepath} run --rm 1-bf"
    echo $cmd && eval $cmd
  fi
  
  echo 'Start container in 1-bf...'
  cmd="docker-compose \
    ${DEPLOY_BF_OPTS} \
    up -d ${BF_LIST}"
  echo $cmd && eval $cmd
  
  cmd="echo \"9,11,12\" | bash ${ROOTDIR}/deploy.sh skip infra"
  echo $cmd && eval $cmd

  sed -i 's/ZABBIX=deploy-no/ZABBIX=deploy/g' "${ROOTDIR}"/dev.env
  sed -i 's/EFK_DEPLOY=false/EFK_DEPLOY=true/g' "${ROOTDIR}"/dev.env
}

function stop(){
  loadenv
  echo 'stop infra'
  cmd="docker-compose \
    -f ${INFRA_YAML_FILEPATH} \
    rm -f -s"
  echo $cmd && eval $cmd

  echo 'stop container in 1-bf...'
  cmd="docker-compose \
    ${DEPLOY_BF_OPTS} \
    rm -f -s"
  echo $cmd && eval $cmd

  cmd="bash ${ROOTDIR}/deploy.sh stop 9,11,12"
  echo $cmd && eval $cmd
}

function show_help() {
  local USAGE="Usage: ${0##*/} [command] [options...]

  Stop services but keep data
  e.g. ${0##*/} stop

Command:
     stop                               Stop services but keep data

Options:
     --help                             Show this message
     --option                           Set the deployment option (e.g. '9, 11, 12')
"

  echo "${USAGE}"
}

function handler_missing_arugment(){
  echo "error: misssing arugment of option $1" 1>&2
}

function get_opts() {
  while :; do
    case "$1" in
      -h|-\?|--help)
        show_help
        exit 0
        ;;
      -o|--option)
        if [[ -n "$2" ]]; then
          DEPLOY_OPTION="$2"
          shift
        else
          handler_missing_arugment '--option'
          return 1
        fi
        ;;
      --)
        shift
        break
        ;;
      *)
        break
    esac
    shift
  done

  LIST_PROFILES="${LIST_PROFILES:-false}"
}

function main(){
  local cmd="$1"
  get_opts "$@"

  if [[ "${cmd}" == 'stop' ]]; then
    stop
  else
    deploy "${DEPLOY_OPTION}"
  fi
}
main "$@"
