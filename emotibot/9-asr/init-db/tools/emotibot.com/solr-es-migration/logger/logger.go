package logger

import (
	"io"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

const (
	LogLevelDebug = iota
	LogLevelInfo
	LogLevelWarn
	LogLevelError
)

type Logger struct {
	level  int
	prefix string
	flag   int
	logger *log.Logger
}

var (
	Debug = log.New(ioutil.Discard, "[DEBUG]", log.LstdFlags|log.Lshortfile)
	Info  = log.New(ioutil.Discard, "[INFO]", log.LstdFlags|log.Lshortfile)
	Warn  = log.New(ioutil.Discard, "[WARN]", log.LstdFlags|log.Lshortfile)
	Error = log.New(ioutil.Discard, "[ERROR]", log.LstdFlags|log.Lshortfile)
)

func init() {
	logLevel := os.Getenv("LOG_LEVEL")
	var level int

	switch strings.ToUpper(logLevel) {
	case "DEBUG":
		level = LogLevelDebug
	case "INFO":
		level = LogLevelInfo
	case "WARN":
		level = LogLevelWarn
	case "ERROR":
		level = LogLevelError
	default:
		level = LogLevelDebug
	}

	loggers := []*Logger{
		&Logger{
			level:  LogLevelDebug,
			logger: Debug,
		},
		&Logger{
			level:  LogLevelInfo,
			logger: Info,
		},
		&Logger{
			level:  LogLevelWarn,
			logger: Warn,
		},
		&Logger{
			level:  LogLevelError,
			logger: Error,
		},
	}

	for _, l := range loggers {
		var out io.Writer
		if l.level >= level {
			out = os.Stdout
		} else {
			out = ioutil.Discard
		}
		l.logger.SetOutput(out)
	}
}
