#!/bin/bash

COLOR_REST='\033[0m'
COLOR_RED='\033[0;31m'
COLOR_BOLD='\033[0;32m'
# build a image based on folder
function build_docker() {
    echo -e "${COLOR_BOLD}"
    printf '=%.0s' {1..15}
    printf "Building image from $1"
    printf '=%.0s' {1..15}
    printf '\n'
    echo -e "${COLOR_REST}"
    cd "$1" || return 1
    if [ ! -f "./build.env" ]; then
        echo -e "${COLOR_RED}need build.env for building image${COLOR_REST}" >&2
        return 1
    fi
    # shellcheck source=/dev/null
    source ./build.env
    buildArg=""
    # Caputre the build-arg from var start with ARG_*
    for e in $(set -o posix ; set | less);
    do
        if [[ $e =~ ^ARG_* ]]; then
            buildArg="$buildArg --build-arg $e"
        fi
    done
    valid_config "$1/docker-compose.yml"
    #Build the image
    docker-compose -f "$1/docker-compose.yml" build $buildArg || return 1
    echo $IMAGE_NAME

    return 0
}


# Validate the Compose file to ensure no environment is missing
function valid_config() {
    if [ -z "$1" ]; then
        echo "Please specify the path of compose file."
        return 64
    fi
    set +e
    text=$(docker-compose -f $1 config 2>&1)
    exitCode=$?
    set -e
    if [[ $exitCode -ne 0 || $(echo "$text" | grep -c 'Defaulting to a blank string') -ne 0 ]]; then
        echo -e "${COLOR_RED}something wrong in your dockercompose file:\n${COLOR_REST} $text" >&2
        return 1
    fi
}


# build a image based on folder
function prepare_build() {
    echo -e "${COLOR_BOLD}"
    printf '=%.0s' {1..15}
    printf "Prepare material from $1"
    printf '=%.0s' {1..15}
    printf '\n'
    echo -e "${COLOR_REST}"
    cd "$1" || return 1
    if [ ! -f "./build.env" ]; then
        echo -e "${COLOR_RED}need build.env for building image${COLOR_REST}" >&2
        return 1
    fi

    # shellcheck source=/dev/null
    source ./build.env

    # create dst dir
    BASIC_DIR="$PWD/.models/asr-models"
    SIM_RESCORE_DIR="$PWD/.models/rescore-models/${ARG_BASIC_SIM_MODEL}/emotibot"
    TRA_RESCORE_DIR="$PWD/.models/rescore-models/${ARG_BASIC_TRA_MODEL}/emotibot"
    SIM_PUNCTUATION_DIR="$PWD/.models/punctuation/zh_CN"
    TRA_PUNCTUATION_DIR="$PWD/.models/punctuation/zh_TW"
    mkdir -p $BASIC_DIR
    mkdir -p $SIM_RESCORE_DIR
    mkdir -p $TRA_RESCORE_DIR
    mkdir -p $SIM_PUNCTUATION_DIR
    mkdir -p $TRA_PUNCTUATION_DIR

    buildArg=""
    # Caputre the build-arg from var start with ARG_*
    for e in $(set -o posix ; set | less);
    do
        if [[ $e =~ ^DOWNLOAD_URL_* ]]; then
            IFS='=' read -ra URL <<< "$e"
            for i in "${URL[@]}";
            do
                if [[ ! $i =~ ^DOWNLOAD_URL_* ]]; then
                    if [[ $e =~ ^DOWNLOAD_URL_BASIC_* ]]; then
                        printf "download basic model: $i\n"
                        eval "wget -q -c $i -P ${BASIC_DIR}"
                    elif [[ $e =~ ^DOWNLOAD_URL_RESCORE_TRA* ]]; then
                        printf "download rescore model: $i\n"
                        eval "wget -q -c $i -P ${TRA_RESCORE_DIR}"
                    elif [[ $e =~ ^DOWNLOAD_URL_PUNCTUATION_TRA* ]]; then
                        printf "download puntuation model: $i\n"
                        eval "wget -q -c $i -P ${TRA_PUNCTUATION_DIR}"
                    elif [[ $e =~ ^DOWNLOAD_URL_RESCORE_SIM* ]]; then
                        printf "download rescore model: $i\n"
                        eval "wget -q -c $i -P ${SIM_RESCORE_DIR}"
                    elif [[ $e =~ ^DOWNLOAD_URL_PUNCTUATION_SIM* ]]; then
                        printf "download puntuation model: $i\n"
                        eval "wget -q -c $i -P ${SIM_PUNCTUATION_DIR}"
                    else
                        printf "ignore: $i"
                    fi
                fi
            done
            # buildArg="$buildArg --build-arg $e"
        fi

    done
    return 0
}
