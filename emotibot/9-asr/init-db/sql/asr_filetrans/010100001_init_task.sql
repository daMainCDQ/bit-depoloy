-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE `task` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `task_uuid` char(32) CHARACTER SET utf8mb4 NOT NULL,
  `status` int(11) NOT NULL,
  `code` int(11) NOT NULL,
  `create_time` bigint(20) NOT NULL,
  `finish_time` bigint(20) DEFAULT NULL,
  `crc32` int(10) unsigned DEFAULT NULL,
  `custom_key` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `file_link` varchar(1000) CHARACTER SET utf8mb4 NOT NULL,
  `file_link_crc32` int(10) unsigned NOT NULL,
  `callback_url` varchar(1000) CHARACTER SET utf8mb4 DEFAULT NULL,
  `enable_sample_rate_adaptive` tinyint(1) DEFAULT '1',
  `stereo_mode` varchar(255) DEFAULT 'both',
  `spkr_model` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `vad_combine_sec` double DEFAULT NULL,
  `sample_rate` int(10) unsigned DEFAULT '8000',
  `digit_convert` int(10) unsigned DEFAULT NULL,
  `do_punctuation` tinyint(1) DEFAULT '0',
  `custom_rescore` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `left_speed` double DEFAULT NULL,
  `left_quiet` double DEFAULT NULL,
  `left_emotion` double DEFAULT NULL,
  `right_speed` double DEFAULT NULL,
  `right_quiet` double DEFAULT NULL,
  `right_emotion` double DEFAULT NULL,
  `length` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `task_uuid` (`task_uuid`),
  KEY `createtime` (`create_time`),
  KEY `customkey` (`custom_key`),
  KEY `filelinkcrc32` (`file_link_crc32`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- +migrate Down
DROP TABLE `task`;