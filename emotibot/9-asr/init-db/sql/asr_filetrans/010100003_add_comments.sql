-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `task` MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID';
ALTER TABLE `task` MODIFY `task_uuid` char(32) CHARACTER SET utf8mb4 NOT NULL COMMENT 'Task UUID';
ALTER TABLE `task` MODIFY `status` int(11) NOT NULL COMMENT 'status: FAILED=-1,INIT=0,CREATED=1,WAITTING=2,ENQUEUE=3,RECOGNIZED=4,FINISHED=5';
ALTER TABLE `task` MODIFY `code` int(11) NOT NULL COMMENT 'return code, SUCCESS=11200000';
ALTER TABLE `task` MODIFY `create_time` bigint(20) NOT NULL COMMENT 'unix timestamp in second';
ALTER TABLE `task` MODIFY `finish_time` bigint(20) DEFAULT NULL COMMENT 'unix timestamp in second';
ALTER TABLE `task` MODIFY `crc32` int(10) unsigned DEFAULT NULL COMMENT 'checksum of the audio file';
ALTER TABLE `task` MODIFY `custom_key` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT 'custom key';
ALTER TABLE `task` MODIFY `file_link` varchar(1000) CHARACTER SET utf8mb4 NOT NULL COMMENT 'the download link of audio file';
ALTER TABLE `task` MODIFY `file_link_crc32` int(10) unsigned NOT NULL COMMENT 'crc32 or the download link';
ALTER TABLE `task` MODIFY `callback_url` varchar(1000) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT 'the callback url';
ALTER TABLE `task` MODIFY `enable_sample_rate_adaptive` tinyint(1) DEFAULT '1' COMMENT 'resample to [sample-rate]';
ALTER TABLE `task` MODIFY `stereo_mode` varchar(255) DEFAULT 'both' COMMENT 'the track of stereo audio, both, left, right';
ALTER TABLE `task` MODIFY `spkr_model` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT 'the spkr model';
ALTER TABLE `task` MODIFY `vad_combine_sec` double DEFAULT NULL COMMENT 'combine the segments between the vad_combine_sec';
ALTER TABLE `task` MODIFY `sample_rate` int(10) unsigned DEFAULT '8000' COMMENT 'the sample-rate of request data';
ALTER TABLE `task` MODIFY `digit_convert` int(10) unsigned DEFAULT NULL COMMENT 'the minimal amount of number to trigger
the utf82digit';
ALTER TABLE `task` MODIFY `do_punctuation` tinyint(1) DEFAULT '0' COMMENT 'enable punctuation append';
ALTER TABLE `task` MODIFY `custom_rescore` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT 'the rescore model';
ALTER TABLE `task` MODIFY `left_speed` double DEFAULT NULL COMMENT 'emotion scroe of the left channel';
ALTER TABLE `task` MODIFY `left_quiet` double DEFAULT NULL COMMENT 'silence time of the left channel';
ALTER TABLE `task` MODIFY `left_emotion` double DEFAULT NULL COMMENT 'speak speed of the left channel';
ALTER TABLE `task` MODIFY `right_speed` double DEFAULT NULL COMMENT 'speak speed of the right channel';
ALTER TABLE `task` MODIFY `right_quiet` double DEFAULT NULL COMMENT 'silence time of the right channel';
ALTER TABLE `task` MODIFY `right_emotion` double DEFAULT NULL COMMENT 'emotion scroe of the right channel';
ALTER TABLE `task` MODIFY `length` double DEFAULT NULL COMMENT 'the lenght of audio file';

ALTER TABLE `segment` MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID';
ALTER TABLE `segment` MODIFY `task_id` int(10) unsigned NOT NULL COMMENT 'task ID';
ALTER TABLE `segment` MODIFY `channel` bigint(20) DEFAULT NULL COMMENT 'the channel this segment in';
ALTER TABLE `segment` MODIFY `order` int(10) unsigned NOT NULL COMMENT 'the order of this segment';
ALTER TABLE `segment` MODIFY `s_ret` bigint(20) DEFAULT NULL COMMENT 'the return code of ASR';
ALTER TABLE `segment` MODIFY `start` double DEFAULT NULL COMMENT 'the start time of segment in the audio file';
ALTER TABLE `segment` MODIFY `end` double DEFAULT NULL COMMENT 'the end time of segment in the audio file';
ALTER TABLE `segment` MODIFY `text` varchar(1000) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT 'the lenght of audio file';
ALTER TABLE `segment` MODIFY `emotion` double DEFAULT NULL COMMENT 'the emotion score';
ALTER TABLE `segment` MODIFY `create_time` bigint(20) NOT NULL COMMENT 'unix timestamp in second';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
