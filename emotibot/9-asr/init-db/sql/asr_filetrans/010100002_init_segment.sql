-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE `segment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `task_id` int(10) unsigned NOT NULL,
  `channel` bigint(20) DEFAULT NULL,
  `order` int(10) unsigned NOT NULL,
  `s_ret` bigint(20) DEFAULT NULL,
  `start` double DEFAULT NULL,
  `end` double DEFAULT NULL,
  `text` varchar(1000) CHARACTER SET utf8mb4 DEFAULT NULL,
  `emotion` double DEFAULT NULL,
  `create_time` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `taskid` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- +migrate Down
DROP TABLE `segment`;