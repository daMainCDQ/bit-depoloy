-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE DATABASE IF NOT EXISTS `asr_filetrans` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- +migrate Down
DROP DATABASE asr_filetrans;