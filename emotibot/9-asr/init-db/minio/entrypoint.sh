#!/bin/bash


if [[ ${#INIT_MINIO_HOST} == 0 ]]; then
	INIT_MINIO_HOST=172.17.0.1
fi
if [[ ${#INIT_MINIO_PORT} == 0 ]]; then
	INIT_MINIO_PORT=9000
fi

# wait minio
until curl -sS "$INIT_MINIO_HOST:$INIT_MINIO_PORT" &> /dev/null; do echo "wait minio" && sleep 3; done

python -u migration.py
