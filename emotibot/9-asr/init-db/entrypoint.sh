#!/bin/bash

if [ "${INIT_MYSQL_INIT}" == "true" ] || [ "${INIT_MYSQL_INIT}" == "TRUE" ] ; then
    echo "# =================================================================="
    echo "# sql ${INIT_MYSQL_HOST} ${INIT_MYSQL_PORT} ${INIT_MYSQL_USER}"
    echo "# =================================================================="

    cd /usr/bin/app/sql && ./up.sh development
    
    if [[ $? -ne 0 ]]; then
        echo "# Fail to migrate sql"
        exit -1
    fi
fi


if [ "${INIT_ES_INIT}" == "true" ] || [ "${INIT_ES_INIT}" == "TRUE" ] ; then
    echo "# =================================================================="
    echo "# elasticsearch"
    echo "# =================================================================="
    cd /usr/bin/app/elasticsearch && ./entrypoint.sh
    
    if [[ $? -ne 0 ]]; then
        echo "# Fail to migrate elasticsearch"
        exit -1
    fi
fi


if [ "${INIT_MINIO_INIT}" == "true" ] || [ "${INIT_MINIO_INIT}" == "TRUE" ] ; then
    echo "# =================================================================="
    echo "# minio"
    echo "# =================================================================="
    cd /usr/bin/app/minio && ./entrypoint.sh
    
    if [[ $? -ne 0 ]]; then
        echo "# Fail to migrate minio"
        exit -1
    fi
fi

if [ "${INIT_MODULE_INIT}" == "true" ] || [ "${INIT_MODULE_INIT}" == "TRUE" ] ; then
    echo "# =================================================================="
    echo "# module"
    echo "# =================================================================="
    cd /usr/bin/app/module && ./entrypoint.sh
    
    if [[ $? -ne 0 ]]; then
        echo "# Fail to migrate modules"
        exit -1
    fi
fi



