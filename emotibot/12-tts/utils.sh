#!/bin/bash


# 根据TTS_HOST_INFO节点信息生成tts前端代理haproxy的后端配置
gen_yaml_project (){

  TTS_MODULE_YAML="emotibot/12-tts/.module.yaml"

  for i in `echo $TTS_HOST_INFO | awk -F ',' '{for(i=1;i<=NF;i++){print $i}}'`
  do

    TTS_HOST=`echo $i | awk -F '#' '{print $1}'`
    TTS_HOST_THREAD=`echo $i | awk -F '#' '{print $2}'`

    sed -i "s/timeout http-request 10m/&\\\\n  server TTS_VOICE_MASTER_HOST ${TTS_HOST}:${TTS_VOICE_MASTER_PORT} check weight 50 maxconn ${TTS_HOST_THREAD}/" $TTS_MODULE_YAML

  done

}

if [ $# -eq 1 ]; then
    mode=$1
    if  [[ $mode == "option" ]]; then
        echo "12 "
    elif [[ $mode == "12" ]]; then
        echo "tts"
    fi
fi
