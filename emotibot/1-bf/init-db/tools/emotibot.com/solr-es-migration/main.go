package main

import (
	"strings"

	"emotibot.com/solr-es-migration/data"
	"emotibot.com/solr-es-migration/elasticsearch"
	"emotibot.com/solr-es-migration/solr"
	"emotibot.com/solr-es-migration/utils"
	"emotibot.com/solr-es-migration/logger"
)

func main() {
	err := elasticsearch.Setup()
	if err != nil {
		logger.Error.Fatalln(err.Error())
	}

	err = solr.Setup()
	if err != nil {
		logger.Error.Fatalln(err.Error())
	}

	collections := []string{
		data.COLLECTION_3RD_CORE,
		data.COLLECTION_MERGE_6_25,
	}

	for _, collection := range collections {
		logger.Info.Printf("Start migrating collection: %s documents", collection)
		databases, err := solr.GetDatabaseTerms(collection)
		if err != nil {
			logger.Error.Fatalln(err.Error())
		}

		for _, database := range databases {
			cursor := "*"
            if strings.HasSuffix(database, "_edit") || database == "editorial" || database == "robot_profile" || strings.HasPrefix(database, "domain_chat") {
                // The four databases migrated by other scripts.
                continue
            }
			for {
				solrDocs, nextCursor, err := solr.DumpAllData(collection, database, cursor, 30)
				if err != nil {
					logger.Error.Fatalln(err.Error())
				}

				esDocs, err := utils.SolrToESQACore(solrDocs, collection, database)
				if err != nil {
					logger.Error.Fatalln(err.Error())
				}

				err = elasticsearch.BulkInsertQACoreData(esDocs)
				if err != nil {
					logger.Error.Fatalln(err.Error())
				}

				if nextCursor == cursor {
					break
				}

				cursor = nextCursor
			}
		}
	}
}
