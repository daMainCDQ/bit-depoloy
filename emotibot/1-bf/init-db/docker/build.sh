#!/bin/bash
set -e

COLOR_REST='\033[0m'
COLOR_RED='\033[0;31m'

BUILD_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
if [ $# -gt 0 ]; then
    echo "detect input $*"
    dockers=$*
elif [ -f "$BUILD_DIR/BUILD_FLOW" ]; then
    echo "use DEFAULT BUILD_FLOW FILE as input"
    dockers=$(cat "$BUILD_DIR/BUILD_FLOW")
else
    echo -e "${COLOR_RED}need stdin or BUILD_FLOW file to build${COLOR_REST}" >&2
    exit 1
fi

for docker in $dockers
do
    echo $docker
    bash -e -c "source $BUILD_DIR/util.sh && build_docker $BUILD_DIR/$docker"
done
