import pymongo
import pymysql
import logging
import os
import json
import sys
import time
from urllib.parse import quote
from urllib import request
import hashlib
from pymongo.errors import (ServerSelectionTimeoutError)



mongo_url = os.environ.get('INIT_MONGO_URI')
mysql_host = os.environ.get('INIT_MYSQL_HOST')
mysql_port = int(os.environ.get('INIT_MYSQL_PORT'))
mysql_user = os.environ.get('INIT_MYSQL_USER')
mysql_password = os.environ.get('INIT_MYSQL_PASSWORD')
mysql_db = 'faq3'

source_mysql_host = os.environ.get('INIT_MYSQL_HOST')
source_mysql_port = int(os.environ.get('INIT_MYSQL_PORT'))
source_mysql_user = os.environ.get('INIT_MYSQL_USER')
source_mysql_password = os.environ.get('INIT_MYSQL_PASSWORD')
source_mysql_db = 'emotibot'




def mysql_query_list(query_sql):
    db = pymysql.connect(host=mysql_host,port=mysql_port, user=mysql_user, passwd=mysql_password,db= mysql_db)
    cursor = db.cursor()
    try:
        cursor.execute(query_sql)
        ret = cursor.fetchall()
        return ret
    except Exception as e:
        logging.exception(e)
    return None

def patch_sq_cat(query_condition):
    print('start patch sq cat')
    mongo_client = pymongo.MongoClient(mongo_url)
    db = mongo_client['dataprocess']
    sq = db['sq']

    sq_content_id_map = {}
    sq_id_sql = """select id,content,app_id from tbl_sq where status >= 0"""
    sq_id_tuple = mysql_query_list(sq_id_sql)
    for i in sq_id_tuple:
        sq_content_id_map[str(i[2]) + ':' + i[1]] = i[0]

    cat_sql = "select id,app_id from tbl_folder"
    cat_id_set = set()
    cat_id_tuple = mysql_query_list(cat_sql)
    for i in cat_id_tuple:
        cat_id_set.add(str(i[1]) + ':' + str(i[0]))

    all_sq = sq.find(query_condition, no_cursor_timeout=True)
    db = pymysql.connect(host=mysql_host,port=mysql_port, user=mysql_user, passwd=mysql_password,db= mysql_db)
    cursor = db.cursor()

    for item in all_sq:
        app_id = item['appid']
        sq_content = item['content']
        if str(app_id) + ':' + sq_content not in sq_content_id_map: continue
        sq_id = sq_content_id_map[str(app_id) + ':' + sq_content]
        label = item['labels'] if 'labels' in item else ''
        if len(label) > 0:
            lable_list = label.split(';')
            cat_list = [int(i[4:]) for i in lable_list if i.startswith('cat_')]
            if len(cat_list) > 1:
                print('update sq cat, app_id : %s, sq: %s' % (app_id, sq_content))
                folder_id = max(cat_list)
                if str(app_id) + ':' + str(folder_id) not in cat_id_set: continue

                update_sq_cat_sql = "update tbl_sq set folder_id = %s where app_id = %s and  id = %s "
                cursor.execute(update_sq_cat_sql, (folder_id, app_id, sq_id))
        db.commit()
    db.close()

def mysql_query_source_list(query_sql):
    db = pymysql.connect(host=source_mysql_host,port=source_mysql_port, user=source_mysql_user, passwd=source_mysql_password, db=source_mysql_db)
    cursor = db.cursor()
    try:
        cursor.execute(query_sql)
        ret = cursor.fetchall()
        return ret
    except Exception as e:
        print(e)
    return None

def patch_upload_test_history():
    source_upload_list_sql = """select user_id,app_id,is_part,type,file_path,rows,valid_rows,date_time,done_rows from tbl_upload_corpus_history """
    source_upload_list = mysql_query_source_list(source_upload_list_sql)
    if source_upload_list:
        target_db = pymysql.connect(host=mysql_host,port=mysql_port, user=mysql_user,passwd= mysql_password, db=mysql_db)
        target_cursor = target_db.cursor()
        target_cursor.execute('delete from tbl_upload_testset_history where  id > 0')
        target_cursor.execute('delete from tbl_upload_sq_history where  id > 0')
        target_cursor.execute('delete from tbl_upload_corpus_history where  id > 0')
        target_db.commit()
        for i in source_upload_list:
            user_id = i[0]
            app_id = i[1]
            is_part = i[2]
            is_part = '1' if is_part == b'\x01' else '2'
            type = i[3]
            file_path = i[4]
            file_name = file_path
            if len(file_path) > 60: file_name = file_path[0:50]
            file_path = str(app_id) + '/' + file_path
            rows = i[5]
            valid_rows = i[6]
            date_time = i[7]
            done_rows = i[8]

            insert_sql = ''
            insert_args = ()
            if type == 'SQ-Answer' or type == 'SQ-Answer-New':
                insert_sql = """insert into tbl_upload_sq_history (user_id,app_id,file_path,rows,valid_rows,finish_rows,create_user,file_name,upload_type,create_time)
                            values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) """
                insert_args = (
                user_id, app_id, file_path, rows, valid_rows, done_rows, user_id, file_name, is_part, date_time)
            elif type == 'LQ-SQ':
                insert_sql = """insert into tbl_upload_corpus_history (user_id,app_id,file_path,rows,valid_rows,finish_rows,create_user,file_name,upload_type,create_time)
                    values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) """
                insert_args = (
                user_id, app_id, file_path, rows, valid_rows, done_rows, user_id, file_name, is_part, date_time)
            if type == 'TEST_CASE':
                insert_sql = """insert into tbl_upload_testset_history (user_id,app_id,file_path,rows,valid_rows,finish_rows,create_user,file_name,upload_type,create_time)
                    values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) """
                insert_args = (
                user_id, app_id, file_path, rows, valid_rows, done_rows, user_id, file_name, is_part, date_time)

            if insert_sql == '': continue
            target_cursor.execute(insert_sql, insert_args)
            target_cursor.execute('select last_insert_id();')
            last_insert = target_cursor.fetchone()
            target_db.commit()
            if not last_insert:
                print('error on file_path :%s' % (file_path))

    return

def patch_ans_online():
    print('start patch_ans_online')
    start_time = time.time()
    db = pymysql.connect(host=mysql_host,port=mysql_port, user=mysql_user,passwd= mysql_password, db=mysql_db)
    cursor = db.cursor()
    try:
        cursor.execute('delete from tbl_ans_online where id > 0')
        db.commit()

        query_sql = """insert into tbl_ans_online(app_id,ans_id,sq_id,dimension,content,create_user,start_time,end_time,rqs,tags)
                        select a.app_id,a.id ans_id,a.sq_id,a.dimension,a.content,a.create_user,a.start_time,a.end_time,group_concat(distinct rq.sq_id separator ',') rqs,group_concat(distinct ang.tag_id separator ',') tags
                        from tbl_ans a left join tbl_ans_related_sq rq on a.id = rq.ans_id  left join  tbl_ans_tag ang on a.id = ang.ans_id where  a.status >= 0 group by  a.id;"""

        cursor.execute(query_sql)
        db.commit()
    except Exception as e:
        logging.exception(e)

    print('end patch_ans_online cost time : %s' % (time.time() - start_time))
    return

def handler_all(app_id):
    """
    patch_sq_cat(condition)
    patch_upload_test_history()
    patch_ans_online()

    """
    condition = {'disable': 0}
    if app_id:
        condition['appid'] = app_id
    patch_sq_cat(condition)
    patch_upload_test_history()
    patch_ans_online()

    return



if __name__ == '__main__':
    # app_id = None时迁移所有app_id的数据
    try:
        app_id = None
        print('开始数据迁移,默认appId为:%s' % (app_id))
        print('mongo source server : %s' % (mongo_url))
        print('mysql source server : %s, database : %s' % (source_mysql_host, source_mysql_db))
        print('mysql target server : %s, database : %s' % (mysql_host, mysql_db))
        start_time = time.time()
        handler_all(app_id)
        print('handler all cost time:%s' % (time.time() - start_time))
    except ServerSelectionTimeoutError as err:
        print("Not found mongo service, skip this migration.")
        sys.exit()
    

