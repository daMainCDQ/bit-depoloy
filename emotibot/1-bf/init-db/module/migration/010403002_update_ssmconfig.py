import pymysql
import logging
import os
import json
import sys
import time

mysql_host = os.environ.get('INIT_MYSQL_HOST')
mysql_port = int(os.environ.get('INIT_MYSQL_PORT'))
mysql_user = os.environ.get('INIT_MYSQL_USER')
mysql_password = os.environ.get('INIT_MYSQL_PASSWORD')
mysql_db = 'faq3'

source_mysql_host = os.environ.get('INIT_MYSQL_HOST')
source_mysql_port = int(os.environ.get('INIT_MYSQL_PORT'))
source_mysql_user = os.environ.get('INIT_MYSQL_USER')
source_mysql_password = os.environ.get('INIT_MYSQL_PASSWORD')
source_mysql_db = 'faq3'


def replace_ssm_config(value):
    """
    替换原有的ssm_config
    :return:
    """
    value_json = json.loads(value)
    for item in value_json['items']:
        if item['name'] == "rank":
            for di in item['value']:
                if di['source'] == 'qq':
                    for depend in di['dependency']:
                        if depend['name'] == 'se':
                            depend['weight'] = 0.6
                        if depend['name'] == 'w2v':
                            depend['weight'] = 0.3
    return value_json


def handler_update_ssm_config_template(name='ssm_config'):
    """
    更新ssm_config模板
    :param name: 可选ssm_config, ssm_config_rc
    :return:
    """
    try:
        query_ssm_config_sql = """select name,module,value,description,zhtw,enus from ent_config where name = '%s'""" % (
            name)
        db = pymysql.connect(mysql_host, mysql_user, mysql_password, mysql_db, mysql_port)
        cursor = db.cursor()
        cursor.execute(query_ssm_config_sql)
        ret = cursor.fetchone()
        ssm_config_str = ret[2]
        new_ssm_config_str = json.dumps(replace_ssm_config(ssm_config_str))
        insert_ssm_config_sql = """update ent_config set value = '%s' where name = '%s' """ % (new_ssm_config_str, name)
        update_ret = mysql_update_target(insert_ssm_config_sql)
        if not update_ret:
            print('update %s template fail' % (name))
            return False
        return True
    except Exception:
        print('update %s template fail' % (name))
        return False


def handler_update_app_ssm_config(app_id, name='ssm_config'):
    try:
        if app_id is None:
            query_old_app_ssmconfig_sql = """select name,app_id,value from ent_config_appid_customization where name = '%s' """ % (
                name)
        else:
            query_old_app_ssmconfig_sql = """select name,app_id,value from ent_config_appid_customization where app_id = '%s' and name = '%s'""" % (
                app_id, name)
        old_app_configs = mysql_query_source_list(query_old_app_ssmconfig_sql)
        for i in old_app_configs:
            app = i[1]
            value = i[2]
            value_json = replace_ssm_config(value)

            update_app_ssmconfig_sql = """update ent_config_appid_customization set value = '%s' where app_id = '%s' and name = '%s'""" % (
                json.dumps(value_json), str(app), name)
            update_ret = mysql_update_target(update_app_ssmconfig_sql)
            if not update_ret:
                print('update app_id %s: %s fail' % (name, str(app)))
            else:
                print('update app_id %s success : %s' % (name, str(app)))
    except Exception:
        print('update app_id %s: %s fail' % (name, str(app)))


def handler_app_ssm_config(app_id):
    if handler_update_ssm_config_template('ssm_config'):
        print('update ssm_config template success')
    if handler_update_ssm_config_template('ssm_config_rc'):
        print('update ssm_config_rc template success')

    handler_update_app_ssm_config(app_id, 'ssm_config')
    handler_update_app_ssm_config(app_id, 'ssm_config_rc')
    return


def mysql_update_target(sql):
    db = pymysql.connect(mysql_host, mysql_user, mysql_password, mysql_db, mysql_port)
    cursor = db.cursor()
    try:
        cursor.execute(sql)
        db.commit()
        db.close()
        return True
    except Exception as  e:
        db.rollback()
        print(e)
        return False


def mysql_query_source_list(query_sql):
    db = pymysql.connect(source_mysql_host, source_mysql_user, source_mysql_password, source_mysql_db, source_mysql_port)
    cursor = db.cursor()
    try:
        cursor.execute(query_sql)
        ret = cursor.fetchall()
        return ret
    except Exception as e:
        print(e)
    return None


if __name__ == '__main__':
    # 默认app_id None，迁移全部
    app_id = None

    print('start handler app ssm config migrate')
    handler_app_ssm_config(app_id)
    print('end handler app ssm config migrate')


