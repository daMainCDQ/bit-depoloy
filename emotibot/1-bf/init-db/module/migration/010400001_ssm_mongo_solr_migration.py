import pymongo
import pymysql
import logging
import os
import json
import sys
from elasticsearch import helpers
import time
from elasticsearch import Elasticsearch
from urllib.parse import quote
from urllib import request
import hashlib
from pymongo.errors import (ServerSelectionTimeoutError)



mongo_url = os.environ.get('INIT_MONGO_URI')
mysql_host = os.environ.get('INIT_MYSQL_HOST')
mysql_port = int(os.environ.get('INIT_MYSQL_PORT'))
mysql_user = os.environ.get('INIT_MYSQL_USER')
mysql_password = os.environ.get('INIT_MYSQL_PASSWORD')
mysql_db = 'faq3'

source_mysql_host = os.environ.get('INIT_MYSQL_HOST')
source_mysql_port = int(os.environ.get('INIT_MYSQL_PORT'))
source_mysql_user = os.environ.get('INIT_MYSQL_USER')
source_mysql_password = os.environ.get('INIT_MYSQL_PASSWORD')
source_mysql_db = 'emotibot'

solr_url = 'http://' + os.environ.get('INIT_SOLR_HOST') + ':' + os.environ.get('INIT_SOLR_PORT') + '/solr/'
solr_db = '3rd_core'

es_server = os.environ.get('INIT_ES_HOST')
es_port = os.environ.get('INIT_ES_PORT')
es_rc_index_name = 'emotibot-qa-core-dac-rc'
es_gold_index_name = 'emotibot-qa-core-dac-gold'
es_type = 'doc'
es_user = os.environ.get('INIT_ES_USER')
es_password = os.environ.get('INIT_ES_PASS')

#consul
consul_host = os.environ.get('INIT_CONSUL_HOST')
consul_port = os.environ.get('INIT_CONSUL_PORT')


def handler_sq_category():
    print('start handler sq category')
    try:
        source_db = pymysql.connect(host = source_mysql_host,port = source_mysql_port, user = source_mysql_user, passwd=source_mysql_password, db=source_mysql_db)
        source_cursor = source_db.cursor()
        source_cursor.execute("select id,pid,level,name,path ,app_id from tbl_sq_category where is_del = 0")
        sq_cats = source_cursor.fetchall()
        sq_cat_length = len(sq_cats)
        sq_cat_success = 0
        print('sq cat length :%s' % (sq_cat_length))
        if sq_cats:
            target_db = pymysql.connect(host = mysql_host,port = mysql_port, user = mysql_user, passwd = mysql_password,db= mysql_db)
            target_cursor = target_db.cursor()

            root = [i for i in list(sq_cats) if i[2] == 0]
            root_map = {}  ## app_id => root id
            for i in root: root_map[i[5]] = i[0]
            for i in sq_cats:
                id = i[0]
                pid = i[1]
                level = i[2]
                name = i[3]
                path = i[4]
                app_id = i[5]
                path_list = path.split(',')
                path_list = [pi[4:] for pi in path_list]
                path_list.insert(0, str(root_map[app_id]))
                target_path = ','.join(path_list)
                insert_folder_sql = """insert into tbl_folder (id,app_id,parent_folder_id,name,fullname,level,create_user)
                 values (%s,'%s',%s,'%s','%s',%s,'%s')
                """ % (id, app_id, pid, name, target_path, level , 'wsp')
                target_cursor.execute(insert_folder_sql)
                target_db.commit()
                sq_cat_success += 1

            target_cursor.execute('delete from tbl_folder where level = 0 and parent_folder_id = 0 and name = "暂无分类" and id > 0')
            target_cursor.execute('update tbl_folder set parent_folder_id = 0 where level = 1')

            target_db.commit()

            print('sq cat success :%s' % (sq_cat_success))
    except Exception as e:
        print(e)
    print('end handler sq category')
    return


def handler_sq_ans(query_condition):
    print('start handler sq ans')
    mongo_client = pymongo.MongoClient(mongo_url)
    db = mongo_client['dataprocess']
    sq = db['sq']
    answer = db['answer']
    cnt = 0
    allcnt = 0

    # appid:ansid => sq recordid for save tbl_ans_related_sq
    answer_related_question_map = {}

    # appid:recordid => sqid
    sq_id_record_map = {}
    ans_cnt_success = 0
    ans_exist_cnt = 0
    all_sq = sq.find(query_condition, no_cursor_timeout=True)
    for item in all_sq:
        allcnt += 1
        app_id = item['appid']
        sq_content = item['content']
        sq_record = item['recordId']
        user_id = 'wsp'
        sq_record_id = item['recordId']
        label = item['labels'] if 'labels' in item else ''
        folder_id = 0
        tag_list = []
        if len(label) > 0:
            lable_list = label.split(';')
            tag_list = [int(i[4:]) for i in lable_list if i.startswith('tag_')]
            cat_list = [int(i[4:]) for i in lable_list if i.startswith('cat_')]
            if len(cat_list) > 0:
                folder_id = cat_list[0]
        # sq_content = sq_content.replace("'", '"')
        insert_sq_sql = """insert into tbl_sq ( app_id,folder_id,status,content,create_user) values ( %s,%s,%s,%s,%s)"""
        # print(insert_sq_sql)
        sq_id = mysql_insert(insert_sq_sql, (app_id, folder_id, 0, sq_content, user_id) )
        if sq_id:
            sq_id_record_map[str(app_id) + ':' + str(sq_record)] = sq_id

            if len(tag_list) > 0:
                for i in tag_list:
                    insert_sq_tag_sql = """insert into tbl_sq_tag (sq_id,tag_id) values (%s,%s)""" % (sq_id, i)
                    sq_tag_id = mysql_insert(insert_sq_tag_sql)
                    if not sq_tag_id:
                        print("insert sq_tag false")

            # 插入标准问对应答案
            cnt += 1
            sq_ans = answer.find({'appid': app_id, 'parentRecordId': sq_record_id, 'disable': 0},
                                 no_cursor_timeout=True)
            for ansi in sq_ans:
                ans_exist_cnt += 1
                ans_content = ansi['content']
                # ans_content = ans_content.replace("'", '"')
                start_time, end_time = None, None
                if 'starttime' in ansi:
                    start_time = ansi['starttime']
                if 'endtime' in ansi:
                    end_time = ansi['endtime']
                label = ansi['labels'] if 'labels' in ansi else ''
                relatedQuestions = ansi['relatedQuestions'] if 'relatedQuestions' in ansi else []
                tag_list = []
                if len(label) > 0:
                    lable_list = label.split(';')
                    tag_list = [int(i[4:]) for i in lable_list if i.startswith('tag_')]
                dstr = ''
                if 'dimension' in ansi and len(ansi['dimension']) > 0:
                    dimension = ansi['dimension']
                    dl = []
                    for di in dimension:
                        dl.extend(di['data'])
                    dstr = ','.join([str(di) for di in dl])
                insert_ans_sql = 'insert into tbl_ans (app_id,sq_id,status,content,create_user,dimension'
                insert_value_sql = """ ) values ( %s,%s,%s,%s,%s,%s """
                insert_ans_condition = (app_id, sq_id, 0, ans_content, user_id, dstr)
                if start_time is None or len(start_time) == 0:
                    start_time = None
                else:
                    insert_ans_sql += ' ,start_time'
                    insert_value_sql += """,%s """
                    insert_ans_condition += (start_time,)
                if end_time is None or len(end_time) == 0:
                    end_time = None
                else:
                    insert_ans_sql += ',end_time'
                    insert_value_sql += """,%s """
                    insert_ans_condition += (end_time,)
                insert_value_sql += """ ); """
                insert_ans_sql = insert_ans_sql + insert_value_sql
                # print(insert_ans_sql)
                ans_id = mysql_insert(insert_ans_sql, insert_ans_condition)
                if ans_id:
                    if len(relatedQuestions) > 0:
                        answer_related_question_map[str(app_id) + ':' + str(ans_id)] = relatedQuestions
                    ans_cnt_success += 1
                    if len(tag_list) > 0:
                        for i in tag_list:
                            insert_ans_tag_sql = """insert into tbl_ans_tag (ans_id,tag_id) values (%s,%s)""" % (
                                ans_id, i)
                            ans_tag_id = mysql_insert(insert_ans_tag_sql)
                            if not ans_tag_id:
                                print("insert ans_tag false")
                else:
                    print('insert ans failed')
    insert_rqs_cnt = 0
    for appansid, rqs in answer_related_question_map.items():
        keys = appansid.split(":")
        app_id = keys[0]
        ans_id = int(keys[1])
        for sq_record_id in rqs:
            if str(app_id) + ':' + str(sq_record_id) in sq_id_record_map:
                sq_id = sq_id_record_map[str(app_id) + ':' + str(sq_record_id)]
                insert_rqs_sql = """insert into tbl_ans_related_sq (app_id,ans_id,sq_id,create_user) values ('%s',%s,%s,'%s')""" % (
                    app_id, ans_id, sq_id, 'wsp')
                rqs_ret = mysql_insert(insert_rqs_sql)
                insert_rqs_cnt += 1
                if not rqs_ret:
                    print('insert tbl_ans_related_sq failed')
    print('insert rqs cnt : %s' % (insert_rqs_cnt))

    all_sq.close()
    print('insert sq success :%s' % (cnt))
    print('insert sq length :%s' % (allcnt))
    print('ans exist number :%s' % (ans_exist_cnt))
    print('insert ans length success:%s' % (ans_cnt_success))
    print('end handler sq ans')


def handler_lq(query_condition):
    print('start handler lq')
    mongo_client = pymongo.MongoClient(mongo_url)
    db = mongo_client['dataprocess']
    lq = db['lq']
    sq = db['sq']

    app_id = query_condition['appid'] if 'appid' in query_condition else None
    sq_content_id_map = {}
    if app_id is not None:
        sq_id_sql = """select id,content,app_id from tbl_sq where app_id = '%s'  """ % (app_id)
    else:
        sq_id_sql = """select id,content,app_id from tbl_sq """
    sq_id_tuple = mysql_query_list(sq_id_sql)
    for i in sq_id_tuple:
        sq_content_id_map[str(i[2]) + ':' +i[1]] = i[0]

    parent_record_sqid_map = {}
    sq_cursor = sq.find(query_condition, no_cursor_timeout=True)
    for sqi in sq_cursor:
        sq_record_id = sqi['recordId']
        sq_content = sqi['content']
        # sq_content = sq_content.replace("'", '"')
        sq_appid = sqi['appid']
        if str(sq_appid) + ':' + sq_content in sq_content_id_map:
            parent_record_sqid_map[str(sq_appid) + ':' + str(sq_record_id)] = sq_content_id_map[str(sq_appid) + ':' + sq_content]

    success_cnt = all_cnt = 0
    lq_cursor = lq.find(query_condition, no_cursor_timeout=True)
    insert_lq_batch = []
    for item in lq_cursor:
            all_cnt += 1
            app_id = item['appid']
            sq_parent_record = item['parentRecordId']

            if str(app_id) + ':' + str(sq_parent_record) in parent_record_sqid_map:
                insert_lq_batch.append(
                    {'app_id': app_id, 'sq_id': parent_record_sqid_map[str(app_id) + ':' + str(sq_parent_record)], 'status': 0,
                     'content': item['content'],
                     'create_user': 'wsp'})
            else:
                print('sq parent id : %s ,app_id :%s do not exist' % (sq_parent_record,app_id))

            if len(insert_lq_batch) >= 100:
                lq_insert_batch_sql = """insert into tbl_corpus (app_id,sq_id,status,content,create_user) values (%s,%s,%s,%s,%s)"""
                insert_args = []
                for i in insert_lq_batch:
                    insert_args.append((i['app_id'], i['sq_id'], i['status'], i['content'], i['create_user']))
                ret = mysql_insert_batch(lq_insert_batch_sql,insert_args)
                if ret:
                    success_cnt += len(insert_lq_batch)
                insert_lq_batch = []
    if len(insert_lq_batch) > 0:
        lq_insert_batch_sql = """insert into tbl_corpus (app_id,sq_id,status,content,create_user) values """
        for i in insert_lq_batch:
            value_sql = """ ('%s',%s,%s,'%s','%s'),""" % (
                i['app_id'], i['sq_id'], i['status'], i['content'], i['create_user'])
            lq_insert_batch_sql += value_sql
        if lq_insert_batch_sql.endswith(','): lq_insert_batch_sql = lq_insert_batch_sql[0:-1] + ';'
        ret = mysql_insert(lq_insert_batch_sql)
        if ret:
            success_cnt += len(insert_lq_batch)

    lq_cursor.close()
    print('insert lq allcnt :%s' % (all_cnt))
    print('insert lq success cnt :%s' % (success_cnt))
    print('start handler lq')
    return


def add_batch(db, list):
    # print('start add batch to es index:%s' % (db))
    start = time.time()
    update_list = []
    es = Elasticsearch(['%s:%s@%s:%s' % (es_user, es_password, es_server, '9200')])
    if list and len(list) > 0:
        for i in list:
            lq = i['lq']
            sq = i['sq']
            segments_str = i['segment_lq']
            update_list.append({'_index': db,
                                '_type': es_type,
                                '_source': {'app_id': str(i['app_id']),
                                            'module': 'faq',
                                            'doc_id': i['id'],
                                            'std_q_id': i['sq_id'],
                                            'std_q_content': sq,
                                            'answers': [{'sentence': sq,
                                                        'creator': 'page'}],
                                            'sentence': segments_str,
                                            'sentence_original': lq,
                                            'source': i['source']}
                                })

    else:
        return False

    if len(update_list) > 0:
        res = helpers.bulk(es, update_list)
        # print(res)
        update_list = []
    # print(' es add batch cost time :%s' % (time.time() - start))
    return True


def from_solr_2_es(app_id):
    print('start migrate from solr to es')

    es = Elasticsearch(['%s:%s@%s:%s' % (es_user, es_password, es_server, '9200')])

    if not es.indices.exists(index=es_gold_index_name):
        es.indices.create(index=es_gold_index_name)
    if not es.indices.exists(index=es_rc_index_name):
        es.indices.create(index=es_rc_index_name)

    sq_content_id_map = {}
    if app_id is not None:
        sq_id_sql = """select id,content,app_id from tbl_sq where app_id = '%s'  """ % (app_id)
        query_app_sql = """select distinct app_id from tbl_sq where app_id = '%s' """%(app_id)
    else:
        sq_id_sql = """select id, content,app_id  from tbl_sq """
        query_app_sql = """select distinct app_id from tbl_sq """


    all_all_list = mysql_query_list(query_app_sql)

    sq_id_tuple = mysql_query_list(sq_id_sql)
    for i in sq_id_tuple:
        sq_content_id_map[str(i[2]) + ':' + str(i[1])] = i[0]

    for appi in all_all_list:
        query_start = 0
        solr_query_batch = 1000
        while True:
            app = appi[0]
            url = solr_url + solr_db + '/select?q=database:' + str(app) + '_edit&wt=json&indent=true'
            url += '&start=' + str(query_start) + '&rows=' + str(solr_query_batch)
            req = request.Request(url)
            res = request.urlopen(req)
            if not res: break
            res = bytes.decode(res.read())
            res = json.loads(res)
            if res['responseHeader'] is None or res['responseHeader']['status'] != 0: break
            if len(res['response']['docs']) == 0: break
            solr_list = res['response']['docs']
            es_insert_batch = []
            md5 = hashlib.md5()
            for i in solr_list:
                if 'related_sentences' not in i: continue
                related_sentence = i['related_sentences'][0]
                try:
                    sq_content = json.loads(related_sentence)['answer']
                except Exception as e:
                    print(related_sentence)
                    end_point = related_sentence.find('\", \"cu\"')
                    sq_content = related_sentence[12:end_point]

                lq_content = i['sentence_original']
                if 'sentence' not in i:
                    print('---------------- sentent null :%s' % (lq_content))
                    continue
                segment_lq = i['sentence']
                source = i['source']

                key = str(app) +':'+ str(sq_content)
                if str(app) + ':' + str(sq_content) not in sq_content_id_map:

                    if str(app) + ':' + str(sq_content).replace('\\', '')  in sq_content_id_map:
                        sq_content = str(sq_content).replace('\\', '')
                    elif str(app) + ':' + str(sq_content).encode('utf-8').decode('unicode_escape')  in sq_content_id_map:
                        sq_content = sq_content.encode('utf-8').decode('unicode_escape')



                if str(app) + ':' + str(sq_content) in sq_content_id_map:
                    md5.update(lq_content.encode('utf-8'))
                    lq_content_md = md5.hexdigest()
                    es_insert_batch.append({'lq': lq_content, 'sq': sq_content, 'segment_lq': segment_lq, 'app_id': app,
                                            'id': lq_content_md,
                                            'sq_id': sq_content_id_map[str(app) + ':' + str(sq_content)],
                                            'source': source})
                else:
                    print('-----------------------------------error where query ,app_id : %s,sqlq id :%s' % (
                        app, lq_content))
            add_batch(es_rc_index_name, es_insert_batch)
            add_batch(es_gold_index_name, es_insert_batch)
            query_start += solr_query_batch

    print('start migrate from solr to es')
    return


def handler_ans_online():
    print('start handler_ans_online')
    start_time = time.time()
    db = pymysql.connect(host = mysql_host,port=mysql_port, user=mysql_user,passwd= mysql_password, db=mysql_db)
    cursor = db.cursor()
    try:
        query_sql = """insert into tbl_ans_online(app_id,ans_id,sq_id,dimension,content,create_user,start_time,end_time,rqs,tags)
                        select a.app_id,a.id ans_id,a.sq_Id,a.dimension,a.content,a.create_user,a.start_time,a.end_time,group_concat(rq.sq_id separator ',') rqs,group_concat(ang.tag_id separator ',') tags
                        from tbl_ans a left join tbl_ans_related_sq rq on a.id = rq.ans_id  left join  tbl_ans_tag ang on a.id = ang.ans_id group by  a.id ;"""
        cursor.execute(query_sql)
        cursor.execute('update tbl_sq sq set sq.corpus_cnt = (select count(*) from tbl_corpus cp where app_id = sq.app_id and cp.sq_id = sq.id ) where sq.id > 0')
        
        db.commit()
    except Exception as e:
        logging.exception(e)

    print('end handler_ans_online cost time : %s' % (time.time() - start_time))
    return


def mysql_query(query_sql):
    db = pymysql.connect(host=mysql_host,port=mysql_port, user=mysql_user, passwd=mysql_password,db= mysql_db)
    cursor = db.cursor()
    try:
        cursor.execute(query_sql)
        ret = cursor.fetchone()
        return ret
    except Exception as e:
        logging.exception(e)
    return None


def mysql_query_list(query_sql):
    db = pymysql.connect(host=mysql_host,port=mysql_port, user=mysql_user, passwd=mysql_password, db=mysql_db)
    cursor = db.cursor()
    try:
        cursor.execute(query_sql)
        ret = cursor.fetchall()
        return ret
    except Exception as e:
        logging.exception(e)
    return None


def mysql_insert_batch(sql,argc):
    db = pymysql.connect(host=mysql_host,port=mysql_port, user=mysql_user, passwd=mysql_password, db=mysql_db)
    cursor = db.cursor()
    try:
        cursor.executemany(sql, argc)
        db.commit()
        db.close()
        return True
    except Exception as  e:
        db.rollback()
        logging.exception(e)
        return False


def mysql_insert(sql,argc = ()):
    db = pymysql.connect(host=mysql_host,port=mysql_port, user=mysql_user,passwd= mysql_password, db=mysql_db)
    cursor = db.cursor()
    try:
        if len(argc) == 0:
            cursor.execute(sql)
        else:
            cursor.execute(sql,argc)
        cursor.execute('select last_insert_id();')
        last_insert = cursor.fetchall()
        db.commit()
        db.close()
        return last_insert[0][0]
    except Exception as  e:
        db.rollback()
        logging.exception(e)
        return False


from urllib import request
def update_consul_config():
    rc_key_pre = ['idc/bf2_config_by_appid_rc','idc/bf2_config_by_appid','idc/ssm']
    for keyi in rc_key_pre:

        url = 'http://' + consul_host + ':' + consul_port + '/v1/kv/' + keyi
        req = request.Request(url, data=bytes(str(int(time.time())), encoding='utf-8'), method='PUT')
        response = request.urlopen(req)

        url = 'http://'+consul_host+':'+consul_port +'/v1/kv/' + keyi + '?recurse'
        req = request.Request(url=url)
        res = request.urlopen(req)
        res = res.read()
        if res and len(res) > 0:
            res = json.loads(res)
            for vi in res:
                vk = vi['Key']
                url = 'http://'+consul_host+':'+consul_port +'/v1/kv/' + vk
                req = request.Request(url,data =bytes(str(int(time.time())),encoding='utf-8'),  method ='PUT')
                response = request.urlopen(req)


    return

def handler_all(app_id):
    """
    Sq,lq,ans,related_ans,
    folder,tag,sqtag,anstag

    # handler_dimension()
    # handler_robot_tag()

    """
    condition = {'disable': 0}
    if app_id:
        condition['appid'] = app_id
    handler_sq_category()
    handler_sq_ans(condition)
    handler_lq(condition)
    handler_ans_online()

    from_solr_2_es(app_id)

    update_consul_config()
    return


if __name__ == '__main__':
    app_id = None
    print('mongo source server : %s' % (mongo_url))
    print('mysql source server : %s, database : %s' % (source_mysql_host, source_mysql_db))
    print('mysql target server : %s, database : %s' % (mysql_host, mysql_db))
    start_time = time.time()

    try:
        handler_all(app_id)
    except ServerSelectionTimeoutError as err:
        print("Not found mongo service, skip this migration.")
        sys.exit()
    print('handler all cost time:%s' % (time.time() - start_time))
