import logging
import os
import time
import pymysql
from urllib import request
import requests
import json
mysql_host = os.environ.get('INIT_MYSQL_HOST',default='172.17.0.1')
mysql_port = int(os.environ.get('INIT_MYSQL_PORT',default='3306'))
mysql_user = os.environ.get('INIT_MYSQL_USER',default='root')
mysql_password = os.environ.get('INIT_MYSQL_PASSWORD',default='password')

#consul
consul_host = os.environ.get('INIT_CONSUL_HOST', default = '172.17.0.1')
consul_port = os.environ.get('INIT_CONSUL_PORT', default = '8500')

esHost = os.environ.get('INIT_ES_HOST', default = '172.17.0.1')
esPort = os.environ.get('INIT_ES_PORT', default = '9200')
esUser = os.environ.get('INIT_ES_USER')
esPass = os.environ.get('INIT_ES_PASS')
esIndex = "emotibot-records*"

def update():
    start_time = time.time()
    db = pymysql.connect(host = mysql_host, port = mysql_port,user = mysql_user, passwd = mysql_password)
    cursor = db.cursor()
    try:
        #
        # create id to tag dict
        #
        query_sql = """
            SELECT id, name, type, category, description
            FROM emotibot.tbl_robot_tag
            WHERE id <= 7 AND app_id = 'system'
        """
        id_to_tag = {}
        cursor.execute(query_sql)
        items = cursor.fetchall()
        for row in items:
            tag_id = str(row[0])
            id_to_tag[tag_id] = row
        #
        # insert be use tag (ans, cmd, sq)
        #
        query_sql = """
            SELECT id, system_tag.app_id, name, type, category, description 
            FROM emotibot.tbl_robot_tag INNER JOIN ( 
                SELECT ans.app_id, tag.tag_id 
                FROM faq3.tbl_ans ans 
                INNER JOIN faq3.tbl_ans_tag tag ON ans.id = tag.ans_id AND tag.tag_id <= 7 
                GROUP BY ans.app_id, tag.tag_id 
                    UNION 
                SELECT cmd.appid, tag.robot_tag_id 
                FROM emotibot.cmd cmd 
                INNER JOIN emotibot.cmd_robot_tag tag ON cmd.cmd_id = tag.cmd_id AND tag.robot_tag_id <= 7 
                GROUP BY cmd.appid, tag.robot_tag_id 
                    UNION
                SELECT sq.app_id, tag.tag_id
                FROM faq3.tbl_sq sq
                INNER JOIN faq3.tbl_sq_tag tag ON sq.id = tag.sq_id AND tag.tag_id <= 7
                GROUP BY sq.app_id, tag.tag_id
            ) system_tag ON system_tag.tag_id = tbl_robot_tag.id
            WHERE tbl_robot_tag.app_id = 'system';
        """
        cursor.execute(query_sql)
        items = cursor.fetchall()
        updated_appid_set = set()
        for row in items:
            tag_id = row[0]
            app_id = row[1]
            name = row[2]
            tag_type = row[3]
            category = row[4]
            description = row[5]
            insert_sql = """
                INSERT INTO emotibot.tbl_robot_tag (app_id, name, type, category, description) 
                SELECT %s, %s, %s, %s, %s WHERE NOT EXISTS (
		    SELECT * FROM emotibot.tbl_robot_tag
		    WHERE app_id = %s AND name = %s
	        );
            """
            cursor.execute(insert_sql, (app_id, name, "userdefine", category, description, app_id, name))
            updated_appid_set.add(app_id)
        #
        # insert be use tag (ans_online)
        #
        query_sql = """
            SELECT app_id, tags
            FROM faq3.tbl_ans_online
            WHERE tags != ''
        """
        cursor.execute(query_sql)
        items = cursor.fetchall()

        for row in items:
            app_id = row[0]
            tags = row[1]
            tag_list = tags.split(",")
            for tag in tag_list:
                row = id_to_tag.get(tag, None)
                if row != None:
                    insert_sql = """
                        INSERT INTO emotibot.tbl_robot_tag (app_id, name, type, category, description) 
                        SELECT %s, %s, %s, %s, %s WHERE NOT EXISTS (
		            SELECT * FROM emotibot.tbl_robot_tag
		            WHERE app_id = %s AND name = %s
	                );
                    """
                    cursor.execute(insert_sql, (app_id, row[1], "userdefine", row[3], row[4], app_id, row[1]))
                    updated_appid_set.add(app_id)
        #
        # create <appid + name, id> map
        #
        query_sql = """
            SELECT id, app_id, name
            FROM emotibot.tbl_robot_tag
        """
        cursor.execute(query_sql)
        items = cursor.fetchall()

        appid_name_to_id = {}
        for row in items:
            tag_id = row[0]
            app_id = row[1]
            name = row[2]
            appid_name_to_id[app_id +'@@'+ name] = tag_id

        appid_id_map = {}
        for app_id in updated_appid_set:
            id_map = appid_id_map.get(app_id, None)
            if id_map == None:
                appid_id_map[app_id] = {}
            for tag in id_to_tag.values():
                tag_id = tag[0]
                name = tag[1]
                new_id = appid_name_to_id.get(app_id +'@@'+ name, None)
                if new_id != None:
                    appid_id_map[app_id][tag_id] = new_id

        #
        # update ans
        #
        query_sql = """
            SELECT ans.id, ans.app_id, ans_tag.tag_id, tag.name
            FROM faq3.tbl_ans ans
            INNER JOIN faq3.tbl_ans_tag ans_tag ON ans.id = ans_tag.ans_id AND ans_tag.tag_id <= 7
            INNER JOIN emotibot.tbl_robot_tag tag ON tag.id = ans_tag.tag_id AND tag.app_id = 'system'
        """
        cursor.execute(query_sql)
        items = cursor.fetchall()
        
        for row in items:
            ans_id = row[0]
            app_id = row[1]
            tag_id = row[2]
            name = row[3]
            new_id = appid_name_to_id.get(app_id + '@@' + name, None)
            if new_id is None:
                print("[ERROR] not found " + app_id + name)
                return
            update_sql = """
                UPDATE faq3.tbl_ans_tag
                SET tag_id = %s
                WHERE ans_id = %s AND tag_id = %s
            """
            cursor.execute(update_sql, (new_id, ans_id, tag_id))

        #
        # update cmd_robot_tag
        #
        query_sql = """
            SELECT cmd.cmd_id, cmd.appid, cmd_tag.robot_tag_id, tag.name
            FROM emotibot.cmd cmd
            INNER JOIN emotibot.cmd_robot_tag cmd_tag ON cmd.cmd_id = cmd_tag.cmd_id AND cmd_tag.robot_tag_id <= 7
            INNER JOIN emotibot.tbl_robot_tag tag ON tag.id = cmd_tag.robot_tag_id AND tag.app_id = 'system'
        """
        cursor.execute(query_sql)
        items = cursor.fetchall()
        
        for row in items:
            cmd_id = row[0]
            app_id = row[1]
            tag_id = row[2]
            name = row[3]
            new_id = appid_name_to_id.get(app_id + '@@' + name, None)
            if new_id is None:
                print("[ERROR] not found " + app_id + name)
                return
            update_sql = """
                UPDATE emotibot.cmd_robot_tag
                SET robot_tag_id = %s
                WHERE cmd_id = %s AND robot_tag_id = %s
            """
            cursor.execute(update_sql, (new_id, cmd_id, tag_id))
        #
        # update sq
        #
        query_sql = """
            SELECT sq.id, sq.app_id, sq_tag.tag_id, tag.name
            FROM faq3.tbl_sq sq
            INNER JOIN faq3.tbl_sq_tag sq_tag ON sq.id = sq_tag.sq_id AND sq_tag.tag_id <= 7
            INNER JOIN emotibot.tbl_robot_tag tag ON tag.id = sq_tag.tag_id AND tag.app_id = 'system'
        """
        cursor.execute(query_sql)
        items = cursor.fetchall()
        
        for row in items:
            sq_id = row[0]
            app_id = row[1]
            tag_id = row[2]
            name = row[3]
            new_id = appid_name_to_id.get(app_id + '@@' + name, None)
            if new_id is None:
                print("[ERROR] not found " + app_id + name)
                return
            update_sql = """
                UPDATE faq3.tbl_sq_tag
                SET tag_id = %s
                WHERE sq_id = %s AND tag_id = %s
            """
            cursor.execute(update_sql, (new_id, sq_id, tag_id))

        #
        # update ans online
        #
        query_sql = """
            SELECT ans_id, app_id, tags
            FROM faq3.tbl_ans_online
            WHERE tags != ''
        """
        cursor.execute(query_sql)
        items = cursor.fetchall()

        for row in items:
            ans_id = row[0]
            app_id = row[1]
            tags = row[2]
            tag_list = tags.split(",")
            new_tags = []
            seq = ","
            is_update = False
            for tag in tag_list:
                row = id_to_tag.get(tag, None)
                if row != None:
                    name = row[1]
                    new_id = appid_name_to_id.get(app_id + '@@' + name, None)
                    new_tags.append(str(new_id))
                    is_update = True
                else:
                    new_tags.append(tag)
                new_tags_str = seq.join(new_tags)

                if is_update is True:
                    update_sql = """
                        UPDATE faq3.tbl_ans_online
                        SET tags = %s
                        WHERE ans_id = %s
                    """
                    cursor.execute(update_sql, (new_tags_str, ans_id))
        #
        # update tbl_ans_audit_op
        #
        query_sql = """
            SELECT id, origin_info, modify_info
            FROM faq3.tbl_ans_audit_op
            WHERE app_id = %s AND ((origin_info != "" AND NOT origin_info LIKE '%%"tags": ""%%')
            OR (modify_info != "" AND NOT modify_info LIKE '%%"tags": ""%%'))
        """

        for app_id in updated_appid_set:
            id_map = appid_id_map.get(app_id, None)
            if id_map is None or len(id_map) == 0:
                continue
            cursor.execute(query_sql, app_id)
            items = cursor.fetchall()

            for row in items:

                row_id = row[0]
                info1 = row[1]
                info2 = row[2]

                if len(info1) == 0:
                    info1 = "{}"

                if len(info2) == 0:
                    info2 = "{}"
                try:
                    info1 = json.loads(info1)
                    info2 = json.loads(info2)

                    info2_tags = info2.get('tags', '')
                    info1_tags = info1.get('tags', '')
                    info1_tags_list = info1_tags.split(",")
                    info2_tags_list = info2_tags.split(",")

                    info1_new_tags = []
                    info2_new_tags = []
                    seq = ","
                    is_update = False

                    for tag in info1_tags_list:
                        if len(tag) <= 0:
                            continue
                        new_id = id_map.get(int(tag), None)
                        if new_id != None:
                            info1_new_tags.append(str(new_id))
                            is_update = True
                        else:
                            info1_new_tags.append(tag)
                    info1_new_tags_str = seq.join(info1_new_tags)
                    if len(info1_new_tags_str) > 0:
                        info1['tags'] = info1_new_tags_str

                    for tag in info2_tags_list:
                        if len(tag) <= 0:
                            continue
                        new_id = id_map.get(int(tag), None)
                        if new_id != None:
                            info2_new_tags.append(str(new_id))
                            is_update = True
                        else:
                            info2_new_tags.append(tag)
                    info2_new_tags_str = seq.join(info2_new_tags)
                    if len(info2_new_tags_str) > 0:
                        info2['tags'] = info2_new_tags_str

                    if is_update is True:
                        update_sql = """
                            UPDATE faq3.tbl_ans_audit_op
                            SET origin_info = %s, modify_info = %s
                            WHERE id = %s
                        """
                        info1_str = json.dumps(info1)
                        info2_str = json.dumps(info2)
                        if len(info1_str) == 2:
                            info1_str = ""

                        if len(info2_str) == 2:
                            info2_str = ""
                        cursor.execute(update_sql, (info1_str, info2_str, row_id))
                except Exception as e:
                    raise e

        #
        # remove system tag
        #
        delete_sql = """
            DELETE FROM emotibot.tbl_robot_tag
            WHERE id <=7 AND app_id = 'system'
        """
        cursor.execute(delete_sql)

        #
        # update cmd, ssm consul key
        #
        update_consul_config(updated_appid_set)

        #
        # update es record faq tag id
        #
        update_es_tag(updated_appid_set, appid_id_map)
        db.commit()
        db.close()
    except Exception as e:
        logging.exception(e)

    return


def update_consul_config(appid_set):
    rc_key_pre = ['idc/cmd/','idc/ssm/']
    for keyi in rc_key_pre:
        for appid in appid_set:
            url = 'http://' + consul_host + ':' + consul_port + '/v1/kv/' + keyi + appid
            req = request.Request(url, data=bytes(str(int(time.time())), encoding='utf-8'), method='PUT')
            response = request.urlopen(req)

def update_es_tag(appid_set, appid_id_map):

    esURL = "http://" + esUser + ":" + esPass + "@" + esHost + ":" + esPort + "/" + esIndex + "/_update_by_query"
    for app_id in appid_set:
        id_map = appid_id_map.get(app_id, None)
        if id_map is None or len(id_map) == 0:
            continue
        script = "for (int i=0;i<ctx._source.faq_robot_tag_id.size();i++) {"
        for old_id, new_id in id_map.items():
            script += "if (ctx._source.faq_robot_tag_id[i] == " + str(old_id) + ")"
            script += "{ ctx._source.faq_robot_tag_id[i] = " + str(new_id) + "}"
        script += "}"
        post_json = {
            "script": script,
            "query": {
                "bool": {
                    "filter": [
                        {
                            "term": {
                                "app_id": app_id
                            }
                        },
                        {
                            "terms": {
                                "faq_robot_tag_id": ["1", "2", "3", "4", "5", "6", "7"]
                            }
                        }
                    ]
                }
            }
        }

        r = requests.post(esURL, json=post_json)
        print("update:"+ app_id)
        print(r.json())
if __name__ == '__main__':
    try:
        update()
    except Exception as e:
        print('mysql db do not exists')
