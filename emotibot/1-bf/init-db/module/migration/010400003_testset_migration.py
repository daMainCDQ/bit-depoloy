import pymongo
import pymysql
import logging
import os
import json
import sys
import time
from urllib.parse import quote
from urllib import request
import hashlib
from pymongo.errors import (ServerSelectionTimeoutError)



mongo_url = os.environ.get('INIT_MONGO_URI')
mysql_host = os.environ.get('INIT_MYSQL_HOST')
mysql_port = int(os.environ.get('INIT_MYSQL_PORT'))
mysql_user = os.environ.get('INIT_MYSQL_USER')
mysql_password = os.environ.get('INIT_MYSQL_PASSWORD')
mysql_db = 'faq3'

source_mysql_host = os.environ.get('INIT_MYSQL_HOST')
source_mysql_port = int(os.environ.get('INIT_MYSQL_PORT'))
source_mysql_user = os.environ.get('INIT_MYSQL_USER')
source_mysql_password = os.environ.get('INIT_MYSQL_PASSWORD')
source_mysql_db = 'emotibot'





def handler_train_history():
    print('start handler_train_history')
    start_time = time.time()
    db = pymysql.connect(host=mysql_host,port=mysql_port, user=mysql_user, passwd=mysql_password,db= mysql_db)
    cursor = db.cursor()
    try:
        update_train_history_sql = """insert into faq3.tbl_ml_train_history (app_id,status,model_id,result,create_datetime,update_datetime,algo_kind_id,algo_id,algo_solution_id,train_url,config,train_type,is_released)
            select app_id,100 as status,model_id,result,create_datetime as create_datetime,create_datetime as update_datetime, algo_kind_id,algo_id,algo_solution_id,train_url,config,1 astrain_type,1 as is_released  from emotibot.tbl_ml_train_history
            where status = 'done' """
        cursor.execute(update_train_history_sql)
        db.commit()
    except Exception as e:
        logging.exception(e)

    print('end handler_train_history cost time : %s' % (time.time() - start_time))
    return


def mysql_query(query_sql):
    db = pymysql.connect(host=mysql_host,port=mysql_port, user=mysql_user, passwd=mysql_password, db=mysql_db)
    cursor = db.cursor()
    try:
        cursor.execute(query_sql)
        ret = cursor.fetchone()
        return ret
    except Exception as e:
        logging.exception(e)
    return None


def mysql_query_list(query_sql):
    db = pymysql.connect(host=mysql_host,port=mysql_port, user=mysql_user, passwd=mysql_password,db= mysql_db)
    cursor = db.cursor()
    try:
        cursor.execute(query_sql)
        ret = cursor.fetchall()
        return ret
    except Exception as e:
        logging.exception(e)
    return None


def mysql_insert_batch(sql,argc):
    db = pymysql.connect(host=mysql_host,port=mysql_port, user=mysql_user, passwd=mysql_password, db=mysql_db)
    cursor = db.cursor()
    try:
        cursor.executemany(sql, argc)
        db.commit()
        db.close()
        return True
    except Exception as  e:
        db.rollback()
        logging.exception(e)
        return False


def mysql_insert(sql,argc = ()):
    db = pymysql.connect(host=mysql_host,port=mysql_port, user=mysql_user, passwd=mysql_password, db=mysql_db)
    cursor = db.cursor()
    try:
        if len(argc) == 0:
            cursor.execute(sql)
        else:
            cursor.execute(sql,argc)
        cursor.execute('select last_insert_id();')
        last_insert = cursor.fetchall()
        db.commit()
        db.close()
        return last_insert[0][0]
    except Exception as  e:
        db.rollback()
        logging.exception(e)
        return False



def handler_testset(query_condition):
    print('start handler testset')
    mongo_client = pymongo.MongoClient(mongo_url)
    db = mongo_client['dataprocess']
    sq = db['sq']
    app_id = query_condition['appid'] if 'appid' in query_condition else None
    sq_content_id_map = {}
    if app_id is not None:
        sq_id_sql = """select id,content,app_id from tbl_sq where app_id = '%s'  """ % (app_id)
    else:
        sq_id_sql = """select id,content,app_id from tbl_sq """
    sq_id_tuple = mysql_query_list(sq_id_sql)
    for i in sq_id_tuple:
        sq_content_id_map[str(i[2]) + ':' + i[1]] = i[0]

    parent_record_sqid_map = {}
    sq_cursor = sq.find(query_condition, no_cursor_timeout=True)
    for sqi in sq_cursor:
        sq_record_id = sqi['recordId']
        sq_content = sqi['content']
        sq_appid = sqi['appid']
        if str(sq_appid) + ':' + sq_content in sq_content_id_map:
            parent_record_sqid_map[str(sq_appid) + ':' + str(sq_record_id)] = sq_content_id_map[str(sq_appid) + ':' + sq_content]
        else:
            print('parent_record_sq_map not found, sq_content:%s'%(sq_content))

    source_db = pymysql.connect(host=source_mysql_host,port=source_mysql_port, user=source_mysql_user, passwd=source_mysql_password, db=source_mysql_db)
    source_cursor = source_db.cursor()
    if app_id is not None:
        test_sql = "select app_id,question,label_sq,create_datetime,update_datetime from ent_ml_test_case where app_id = '%s' "%(str(app_id))
    else:
        test_sql = "select app_id,question,label_sq,create_datetime,update_datetime from ent_ml_test_case"
    source_cursor.execute(test_sql)
    sq_testsets = source_cursor.fetchall()
    if sq_testsets:
        target_db = pymysql.connect(host=mysql_host,port=mysql_port, user=mysql_user, passwd=mysql_password,db= mysql_db)
        target_cursor = target_db.cursor()
        insert_testset_suc_cnt = 0
        insert_testset_fail_cnt = 0
        for i in sq_testsets:
            app = i[0]
            test_content = i[1]
            sq_recordid = i[2]
            if str(app) + ':' + str(sq_recordid) not in parent_record_sqid_map:
                insert_testset_fail_cnt += 1
                print('test set not found, sq_record_id :%s , app: %s'%(str(sq_recordid),str(app)))
                continue
            sq_id = parent_record_sqid_map[str(app) + ':' + str(sq_recordid)]
            create_time = i[3]
            update_time = i[4]
            insert_testset_sql = """insert into tbl_testset (app_id,sq_id,content,create_user,create_time,update_user,update_time) values (%s,%s,%s,%s,%s,%s,%s)"""
            insert_testset_sql_condition = (app, sq_id,test_content,'csbotadmin',create_time,'',update_time)
            target_cursor.execute(insert_testset_sql, insert_testset_sql_condition)
            insert_testset_suc_cnt += 1
        target_db.commit()
        target_db.close()
        print('success cnt :%s, fail cnt :%s'%(insert_testset_suc_cnt,insert_testset_fail_cnt))
    print('end handler testset')
    return
def handler_all(app_id):
    """

    """
    condition = {'disable': 0}
    if app_id:
        condition['appid'] = app_id
    handler_train_history()
    handler_testset(condition)
    return


if __name__ == '__main__':
    app_id = None
    print('mongo source server : %s' % (mongo_url))
    print('mysql source server : %s, database : %s' % (source_mysql_host, source_mysql_db))
    print('mysql target server : %s, database : %s' % (mysql_host, mysql_db))
    start_time = time.time()

    try:
        handler_all(app_id)
    except ServerSelectionTimeoutError as err:
        print("Not found mongo service, skip this migration.")
        sys.exit()
    print('handler all cost time:%s' % (time.time() - start_time))
