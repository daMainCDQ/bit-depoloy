import requests
import pymysql
import pymongo
import json
import os
import time
import sys
from pymongo.errors import (ServerSelectionTimeoutError)

admin_url = os.environ.get('INIT_ADMIN_URL')
mongo_url = os.environ.get('INIT_MONGO_URI')
mysql_host = os.environ.get('INIT_MYSQL_HOST')
mysql_port = int(os.environ.get('INIT_MYSQL_PORT'))
mysql_user = os.environ.get('INIT_MYSQL_USER')
mysql_password = os.environ.get('INIT_MYSQL_PASSWORD')
mysql_emotibot_db = 'emotibot'
mysql_auth_db = 'auth'


def remove_appid_not_exist():

    delete_sql = "DELETE FROM {}.ie_load_map WHERE app_id in ( SELECT app_id FROM (SELECT loadmap.app_id app_id FROM {}.apps apps RIGHT JOIN {}.ie_load_map loadmap ON apps.uuid = loadmap.app_id WHERE apps.uuid is NULL) a )".format(mysql_emotibot_db, mysql_auth_db, mysql_emotibot_db)
    print(delete_sql)
    db = pymysql.connect(host=mysql_host, port=mysql_port, user=mysql_user, passwd=mysql_password)
    cursor = db.cursor()
    try:
        cursor.execute(delete_sql)
        db.commit()
    except Exception as e:
        print(e)
    db.close()
    return None

def find_need_train_appid():

    mongo_client = pymongo.MongoClient(mongo_url, serverSelectionTimeoutMS=3000)

    db = mongo_client['intent_engine']
    load_map = db['load_map']

    appid_list = []
    docs = load_map.find({}, no_cursor_timeout=True)

    for doc in docs:
        appid_list.append(doc['app_id'])
    docs.close()
    need_train_appid_list = []

    if len(appid_list) > 0:
        db = pymysql.connect(host=mysql_host, port=mysql_port, user=mysql_user, passwd=mysql_password)
        cursor = db.cursor()
        try:
            ret = cursor.execute('SELECT uuid FROM {}.apps WHERE uuid in %s'.format(mysql_auth_db), (tuple(appid_list),))
            records = cursor.fetchall()
            for record in records:
                need_train_appid_list.append(record[0])
        except Exception as e:
            print(e)
            raise e
        cursor.close()
        db.close()
    return need_train_appid_list

def train_appid_list(appid_list):
    for appid in appid_list:
        print('----------------------- appid:' + appid + ' --------------------------')
        train(appid)
    
def train(appid):
    unix_time = int(time.time()) 
    sql="UPDATE {}.intents SET updatetime = $unix_time WHERE appid = '$appid' and version is NULL"

    # Modify the intent status, otherwise cannot train.

    db = pymysql.connect(host=mysql_host, port=mysql_port, user=mysql_user, passwd=mysql_password)
    cursor = db.cursor()
    try:
        cursor.execute('UPDATE {}.intents SET updatetime = %s WHERE appid = %s and version is NULL'.format(mysql_emotibot_db), (unix_time, appid))
        db.commit()
    except Exception as e:
        print(e)
        raise e
    cursor.close()
    db.close()
    headers = {'Authorization': 'Bearer EMOTIBOTDEBUGGER', 'X-Appid': appid, 'X-UserID': 'system'}
 
    try:
        # post admin api to training.
        ret = requests.post(admin_url + '/api/v2/intents/train?engine=intent_engine', headers=headers)
        print(ret.json())

        # get admin api for check training status.
        while True:
            ret = requests.get(admin_url + '/api/v2/intents/status', headers=headers)
            print(ret.json())
            status = ret.json()['result']['status']
            if status == 'TRAIN_FAILED':
                raise Exception('Failed to train intent model appid: ' + appid)
            if status != 'TRAINING':
                break
            time.sleep(3)
    except Exception as e:
        print(e)
        raise e
if __name__ == '__main__':

    try:
        print('find train appid')
        appid_list = find_need_train_appid()
        print('training')
        train_appid_list(appid_list)
    except ServerSelectionTimeoutError as err:
        print("Not found mongo service, skip this migration.")
        sys.exit()
