import logging
import os
import time
import pymysql

mysql_host = os.environ.get('INIT_MYSQL_HOST',default='172.16.101.98')
mysql_port = int(os.environ.get('INIT_MYSQL_PORT',default='3306'))
mysql_user = os.environ.get('INIT_MYSQL_USER',default='root')
mysql_password = os.environ.get('INIT_MYSQL_PASSWORD',default='password')
mysql_db = 'faq3'

source_mysql_host = os.environ.get('INIT_MYSQL_HOST',default='172.16.101.98')
source_mysql_port = int(os.environ.get('INIT_MYSQL_PORT',default='3306'))
source_mysql_user = os.environ.get('INIT_MYSQL_USER',default='root')
source_mysql_password = os.environ.get('INIT_MYSQL_PASSWORD',default='password')
source_mysql_db = 'emotibot'


def patch_migrate_no_cat():
    print('start patch_migrate_no_cat')
    start_time = time.time()
    db = pymysql.connect(host = mysql_host, port = mysql_port,user = mysql_user, passwd = mysql_password,db = mysql_db)
    cursor = db.cursor()
    try:

        update_sql = """update faq3.tbl_sq sq set sq.folder_id = -1
                where sq.folder_id in ( select id from emotibot.tbl_sq_category ct where ct.level = 0 and ct.app_id = sq.app_id );"""
        cursor.execute(update_sql)
        db.commit()
    except Exception as e:
        logging.exception(e)

    print('end patch_migrate_no_cat cost time : %s' % (time.time() - start_time))
    return


def handler_all():
    """

    """
    patch_migrate_no_cat()

    return


if __name__ == '__main__':
    # app_id = None时迁移所有app_id的数据
    try:
        print('mysql source server : %s, database : %s' % (source_mysql_host, source_mysql_db))
        print('mysql target server : %s, database : %s' % (mysql_host, mysql_db))
        start_time = time.time()
        handler_all()
        print('handler all cost time:%s' % (time.time() - start_time))
    except Exception as e:
        print('mysql db do not exists')
