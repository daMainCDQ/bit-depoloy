
# bfop1.5.0版本 elasticsearch部分不支持执行.py文件  故暂时将这部分放在Minio处
import os
from elasticsearch import Elasticsearch
import pymysql


def updateDimensionalityForHistory():
    esHost = os.environ.get('INIT_ES_HOST')
    esPort = int(os.environ.get('INIT_ES_PORT'))
    esUser = os.environ.get('INIT_ES_USER')
    esPass = os.environ.get('INIT_ES_PASS')
    mysqlHost = os.environ.get('INIT_MYSQL_HOST')
    mysqlPort = int(os.environ.get('INIT_MYSQL_PORT'))
    mysqlUser = os.environ.get('INIT_MYSQL_USER')
    mysqlPassword = os.environ.get('INIT_MYSQL_PASSWORD')

    flag = False
    esClient = Elasticsearch(hosts="http://{}:{}/".format(esHost, esPort),
                             http_auth=(esUser, esPass))
    try:
        esClient.ping()
        flag = True
    except Exception:
        print("dimension info migration exit for with es user:{} : http://{}:{}/".format(esUser, esHost, esPort))

    if flag is True:
        tagInfoMap = constrctTagMap(mysqlHost, mysqlPort, mysqlUser, mysqlPassword)

        assemblyUpdateData = assemblyUpdateRecords(esClient, tagInfoMap)

        updateToElasticDb(esClient, assemblyUpdateData)

        print("dimension info migration done ")




def updateToElasticDb(esClient, updateData):

    for record in updateData:
        try:
            custom_info_new_Dict = record.get('_source').get('custom_info_new')
            if custom_info_new_Dict is not None and len(custom_info_new_Dict) > 0:
                for key in custom_info_new_Dict.keys():
                    tagType = 'dim_' + key
                    tagValue = 'dim_' + custom_info_new_Dict.get(key)
                    body = {
                        "script": {
                            "source": "ctx._source.custom_info[params.tagType]=params.tagValue",
                            "lang": "painless",
                            "params" : {
                                "tagType": tagType,
                                "tagValue": tagValue
                            }
                        }
                    }

                    indexName = record.get('_index')
                    docType = record.get('_type')
                    id = record.get('_id')
                    esClient.update(index=indexName, doc_type=docType, id=id, body=body)
        except Exception:
            print('update fail _id :' + str(record.get('_id')))


def assemblyUpdateRecords(esClient, tagInfoMap):
    totalRecords = getAllRecordsWithSexOrPlatform(esClient)

    totalRecordsFilterd = filterRepeatRecord(totalRecords)

    for record in totalRecordsFilterd:
        hasSex = isExistFieldSex(record)
        hasPlatform = isExistFieldPlatform(record)

        appid = record.get('_source').get('app_id')
        source = record.get('_source')
        source['custom_info_new'] = {}
        if hasSex is True:
            oldValue = record.get('_source').get('custom_info').get('sex')
            if oldValue is not None:
                tagTypeId = tagInfoMap.get(appid + '-type-sex')
                tagValueId = '' if tagInfoMap.get(appid + '-tag-' + oldValue) is None else tagInfoMap.get(appid + '-tag-' + oldValue)
                if tagTypeId is not None and tagValueId is not None:
                    customInfoDict = record.get('_source').get('custom_info_new')
                    customInfoDict[tagTypeId] = tagValueId
        if hasPlatform is True:
            oldValue = record.get('_source').get('custom_info').get('platform')
            try:
                if oldValue is not None:
                    tagTypeId = tagInfoMap.get(appid + '-type-platform')
                    tagValueId = '' if tagInfoMap.get(appid + '-tag-' + oldValue) is None else tagInfoMap.get(appid + '-tag-' + oldValue)
                    if tagTypeId is not None and tagValueId is not None:
                        customInfoDict = record.get('_source').get('custom_info_new')
                        customInfoDict[tagTypeId] = tagValueId
            except Exception:
                print('bad case: ' + record.get('_id'))

    print(len(totalRecords))
    return totalRecordsFilterd

def isExistFieldSex(record):
    if '_source' in record.keys():
        sourceDict = record.get('_source')
        if 'custom_info' in sourceDict.keys():
            customInfoDict = sourceDict.get('custom_info')
            if 'sex' in customInfoDict.keys():
                return True
    return False

def isExistFieldPlatform(record):
    if '_source' in record.keys():
        sourceDict = record.get('_source')
        if 'custom_info' in sourceDict.keys():
            customInfoDict = sourceDict.get('custom_info')
            if 'platform' in customInfoDict.keys():
                return True
    return False

def filterRepeatRecord(totalRecords):
    filterDict = {}
    for item in totalRecords:
        id = item.get('_id')
        if id not in filterDict.keys():
           filterDict[id] = item
    if len(filterDict) == 0:
        return list()
    else:
        return list(filterDict.values())

def constrctTagMap(mysqlHost, mysqlPort, mysqlUser, mysqlPassword):
    mysqlConnect = pymysql.connect(host=mysqlHost, port=mysqlPort, user=mysqlUser,
                                   password=mysqlPassword, database='emotibot', charset='utf8mb4')
    # mysqlConnect = pymysql.connect('172.16.101.116:3306', 'root', 'password', 'emotibot')
    sqlTagType = "select id, code, name, appid from tag_type where code in ('sex', 'platform');"

    cur = mysqlConnect.cursor()
    cur.execute(sqlTagType)
    tagTypeRows = cur.fetchall()

    sqlTags = "select id, name, type, app_id from tags where type in (select id from tag_type where code in ('sex', 'platform'));"
    cur.execute(sqlTags)
    tagRows = cur.fetchall()

    tagInfoMap = {}
    if tagTypeRows is not None and tagRows is not None \
            and len(tagTypeRows) > 0 and len(tagRows) > 0:
        for item in tagTypeRows:
            if item is not None:
                typeId = str(item[0])
                typeCode = item[1]
                appid = item[3]

                key = appid + '-type-' + typeCode
                if key not in tagInfoMap.keys():
                    tagInfoMap[key] = typeId

        for item in tagRows:
            tagId = str(item[0])
            tagName = item[1]
            appid = item[3]

            key = appid + '-tag-' + tagName
            if key not in tagInfoMap.keys():
                tagInfoMap[key] = tagId

    return tagInfoMap

def getAllRecordsWithSexOrPlatform(esClient):
    querySex = {
        "query": {
            "exists": {
                "field": "custom_info.sex"
            }
        }
    }
    sexRecords = getAllRecordsByQuery(esClient, "emotibot-*", querySex)

    queryPlatform = {
        "query": {
            "exists": {
                "field": "custom_info.platform"
            }
        }
    }
    platformRecords = getAllRecordsByQuery(esClient, "emotibot-*", queryPlatform)

    totalRecords = sexRecords + platformRecords

    return totalRecords

def getAllRecordsByQuery(esClient, indexStr, queryContent):

    res = esClient.search(index=indexStr, scroll='5m', timeout='10s', size=100, body=queryContent)

    resRecords = res.get('hits').get('hits')
    if not resRecords:
        resRecords = list()
        return resRecords

    scrollId = res["_scroll_id"]
    totalRecords = res['hits']['total']

    for index in range(int(totalRecords/100) + 1):
        rs = esClient.scroll(scroll_id=scrollId, scroll='5m')
        resRecords += rs.get('hits').get('hits')

    print("Got %s Records:" % len(resRecords))
    return resRecords

if __name__ == "__main__":
    print("start update dimension in es")
    updateDimensionalityForHistory()
    print("end update dimension in es")