# Migration tool

希望能透過具有版本紀錄的 Migration Tool 來控管開發環境上的ES状态，目前還是一個很陽春的版本，不支持rollback，歡迎大神修改腳本。

# 懶人包
進去migration目錄下，根據檔名去新增shell, 或template json文件。

# Migration.py 說明

migration.py分成以下兩部分個別執行。

## migration (template 更新, 或其他migration腳本)
請參考migration目錄，一般migration腳本皆放於目錄下。
目前支持shell script, template.json

### shell script or python (.sh or .py)
設計目的為當有比較複雜的migration，例如solr搬移至es, 數據複雜的轉換。

#### 檔名規則：
<主版號><次版號><修訂號><序號>_<說明>.sh
e.g. 1.3.5的第一個腳本為 010305001_example.sh

### template.json
設計目的為template的更新

#### 檔名規則：
<主版號><次版號><修訂號><序號>_<模版名稱>.json
e.g. 010400001_emotibot_qa_core_template.json代表
1.4.0 模版名稱為 'emotibot_qa_core_template' 

### emotibot-qa-core (閒聊語料)
設計目的為語料的全量替換，版本定義在args.env內文件名
e.g. ARG_ES_EMOTIBOT_QA_CORE_FILE=http://nas-sh.emotibot.com.cn:50019/BFOP/es/010400001_emotibot_qa_core.zip
代表現在版本為1.4.0的語料版本。
