#!/bin/bash


if [[ ${#INIT_ES_LOG_HOST} == 0 ]]; then
	INIT_ES_LOG_HOST=172.17.0.1
fi
if [[ ${#INIT_ES_LOG_PORT} == 0 ]]; then
	INIT_ES_LOG_PORT=9200
fi

# wait elasticsearch
until curl -sS "$INIT_ES_LOG_HOST:$INIT_ES_LOG_PORT" &> /dev/null; do echo "wait elasticsearch-log" && sleep 3; done

python -u migration.py
