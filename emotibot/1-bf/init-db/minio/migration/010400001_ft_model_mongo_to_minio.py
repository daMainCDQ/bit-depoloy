from pymongo import MongoClient
import gridfs
from minio import Minio
from minio.error import (ResponseError, BucketAlreadyOwnedByYou, BucketAlreadyExists)
from pymongo.errors import (ServerSelectionTimeoutError)
import os
import json
import sys

current_dir = os.path.dirname(__file__)
file_dir = os.path.join(current_dir,"mongo_data")
if not os.path.exists(file_dir):
    os.mkdir(file_dir)

file_down_path = os.path.join(current_dir, "mongo_download_files.json")

class MongoDBStorage():
    def __init__(self, mongo_config):
        print('MongoDBStorage init...')
        self.database = mongo_config['database']
        self.mongo_url = mongo_config['mongodb_uri']
        self.client = MongoClient(self.mongo_url, serverSelectionTimeoutMS=3000)

        bucket_name = mongo_config.get('bucket_name','ft_model')
        collection = mongo_config.get('collection','ft_model')
        self.fs = gridfs.GridFSBucket(self.client[self.database], bucket_name=bucket_name)
        self.gridfs = gridfs.GridFS(self.client[self.database], collection=collection)

        self.file_list = self.gridfs.list()

    def get_data(self, file_list):

        file_down_list = list()
        for f in file_list:
            data_path = os.path.join(file_dir, f)
            if os.path.exists(data_path):
                file_down_list.append(f)
                continue
            if self.gridfs.exists(filename=f):
                print("downloading datas from mongo...")
                des_file = open(data_path, "wb")
                self.fs.download_to_stream_by_name(f,des_file)
                des_file.close()
                file_down_list.append(f)
            else:
                print("[{}] is not exist...".format(f))
        return file_down_list


class MinIOStorage():
    def __init__(self,minio_config):
        print('MinIOStorage init...')
        self.databse = minio_config['minio_database']
        self.minioClient = Minio('{0}:{1}'.format(minio_config['minio_host'], minio_config['minio_port']),
                                 access_key=minio_config['minio_access_key'],
                                 secret_key=minio_config['minio_secret_key'],
                                 secure=minio_config['secure'])

    def put_data(self, up_file_list, file_down_list):
        print("uploading datas to minio...")
        for f in up_file_list:
            file_path = os.path.join(file_dir,f)
            self.delete_data(f)

            if os.path.exists(file_path):
                try:
                    self.minioClient.fput_object(self.databse, f, file_path)
                except ResponseError as err:
                    print("put file {} failed.".format(f))
                    print(err)
                file_down_list.append(f)
                os.remove(file_path)
            else:
                print("[{}] is not exist...".format(file_path))
        with open(file_down_path,'w') as f:
            json.dump(file_down_list,f)

    def delete_data(self, filename):
        get_name = lambda object: object.object_name
        names = map(get_name, self.minioClient.list_objects_v2(self.databse, filename, recursive=True))
        for err in self.minioClient.remove_objects(self.databse, names):
            print("Deletion Error: {}".format(err))


if __name__ == "__main__":
    mongo_config = {
        "mongodb_uri": os.environ.get('INIT_MONGO_URI'),
        "database":"model",
        "bucket_name": "ft_model",
        "collection": "ft_model"
    }

    minio_config = {
        "minio_host": os.environ.get('INIT_MINIO_HOST'),
        "minio_port": os.environ.get('INIT_MINIO_PORT'),
        "minio_access_key": os.environ.get('INIT_MINIO_ACCESS_KEY'),
        "minio_secret_key": os.environ.get('INIT_MINIO_SECRET_KEY'),
        "minio_database": os.environ.get('INIT_MINIO_BUCKET_FT_MODEL'),
        "secure": False
    }

    secureStr = os.environ.get('INIT_MINIO_SECURE')
    if 'true' == secureStr.lower():
        minio_config['secure'] = True
    batch_size = 100
    try:
        mongo = MongoDBStorage(mongo_config)
    except ServerSelectionTimeoutError as err:
        print("Not found mongo service, skip this migration.")
        sys.exit()
    minio = MinIOStorage(minio_config)

    file_down_list = list()
    if os.path.exists(file_down_path):
        with open(file_down_path, 'r') as f:
            file_down_list = json.load(f)

    list_tmp = list(set(mongo.file_list).difference(set(file_down_list)))
    
    while len(list_tmp) > 0:
        print("\rchecking files to migration...")
        list_tmp = list(set(mongo.file_list).difference(set(file_down_list)))

        if len(list_tmp) > batch_size:
            file_list = list_tmp[0:batch_size]
        else:
            file_list = list_tmp
        up_file_list = mongo.get_data(file_list)

        if len(up_file_list) > 0:
            print("%d files to migration, " % len(mongo.file_list), "%d files left..." % len(list_tmp))
            minio.put_data(up_file_list, file_down_list)

    print("finish migrating")

