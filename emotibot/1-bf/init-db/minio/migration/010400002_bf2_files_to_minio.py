#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import sys

from minio import Minio
from minio.error import (ResponseError, BucketAlreadyOwnedByYou, BucketAlreadyExists)

current_dir = os.path.dirname(__file__)

file_down_path = os.path.join(current_dir, "mongo_download_files.json")

class File2MinIO():
    def __init__(self, config):
        print('BF2FilesStorage init...')
        self.base_dir = config["bf2_dir"]
        self.image_path = os.path.join(self.base_dir, "sq", "images")
        self.upload_history = os.path.join(self.base_dir, "ssm")

        print('MinIOStorage init...')
        self.minioClient = Minio('{0}:{1}'.format(config['minio_host'], config['minio_port']),
                                 access_key=config['minio_access_key'],
                                 secret_key=config['minio_secret_key'],
                                 secure=config['secure'])
        self.media_bucket = config["minio_images_db"]
        self.ssm_bucket = config["minio_ssm_db"]

    def image_transfer(self):
        print("[Image] File path : {}".format(self.image_path))
        if os.path.exists(self.image_path):
            for appid in os.listdir(self.image_path):
                appid_dir = os.path.join(self.image_path, appid)
                if os.path.isdir(appid_dir):
                    print("[Image] Tranfer [{}] data...".format(appid))
                    self.__minio_put_data(
                        self.media_bucket, 
                        self.image_path,
                        appid
                    )
        else:
            print("[Image] File path : {} not exists...".format(self.image_path))

        print("Transfer images done!")


    def upload_history_transfer(self):
        print("[SSM_History] File path : {}".format(self.upload_history))
        if os.path.exists(self.upload_history):
            for appid in os.listdir(self.upload_history):
                appid_dir = os.path.join(self.upload_history, appid)
                if os.path.isdir(appid_dir):
                    print("[SSM_History] Tranfer [{}] data...".format(appid))
                    self.__minio_put_data(
                        self.ssm_bucket, 
                        self.upload_history,
                        appid
                    )
        else:
            print("[SSM_History] File path : {} not exists...".format(self.upload_history))

        print("Transfer images done!")


    def __minio_put_data(self, bucket, dir_base, appid=None):
        print("==> Uploading [{}/{}] data to minio : [{}]".format(dir_base, appid, bucket))
        file_dir = dir_base
        if appid is not None:
            file_dir = os.path.join(dir_base, appid)

        for f in os.listdir(file_dir):
            f_path = os.path.join(file_dir, f)
            if not os.path.isfile(f_path):
                continue
            try:
                if appid is not None:
                    f = os.path.join(appid, f)
                self.minioClient.fput_object(bucket, f, f_path)
            except ResponseError as err:
                print("put file {} failed.".format(f))
                print(err)


if __name__ == "__main__":
    '''
    Tranfer BF2 images / histroy upload file to minio
    '''
    transfer_config = {
        "bf2_dir": '/usr/bin/app/volumes/bf2',
        "minio_host": os.environ.get('INIT_MINIO_HOST'),
        "minio_port": os.environ.get('INIT_MINIO_PORT'),
        "minio_access_key": os.environ.get('INIT_MINIO_ACCESS_KEY'),
        "minio_secret_key": os.environ.get('INIT_MINIO_SECRET_KEY'),
        "minio_images_db": os.environ.get('INIT_MINIO_BUCKET_MIDIA'),
        "minio_ssm_db": os.environ.get('INIT_MINIO_BUCKET_SSM_DAC'),
        "secure": True if os.environ.get('INIT_MINIO_SECURE','False').lower()=='true' else False
    }

    transfer = File2MinIO(transfer_config)
    transfer.image_transfer()
    transfer.upload_history_transfer()

    print("finish migrating")

