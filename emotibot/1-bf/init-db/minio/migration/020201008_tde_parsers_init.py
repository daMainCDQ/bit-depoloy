import io
import os
import sys
import zipfile
from minio import Minio
from os import listdir
from os import walk
from os.path import isfile, join

from minio.error import (ResponseError, BucketAlreadyOwnedByYou, BucketAlreadyExists, NoSuchKey)


def isFolderEmpty(minioClient, bucketName, prefix):
    objects = minioClient.list_objects(bucketName, prefix=prefix, recursive=True)
    isEmpty = True
    for obj in objects:
        isEmpty = False
        break
    return isEmpty


def cleanFolder(minioClient, bucketName, prefix):
    objects = minioClient.list_objects(bucketName, prefix=prefix, recursive=True)
    try:
        for obj in objects:
            minioClient.remove_object(bucketName, obj.object_name)

    except ResponseError as err:
        print(err)
        raise err


def listAllFiles(dirName):
    listOfFile = os.listdir(dirName)
    allFiles = list()
    # Iterate over all the entries
    for entry in listOfFile:
        # Create full path
        fullPath = os.path.join(dirName, entry)
        # If entry is a directory then get the list of files in this directory
        if os.path.isdir(fullPath):
            allFiles = allFiles + listAllFiles(fullPath)
        else:
            allFiles.append(fullPath)

    return allFiles


def upload_folder(minioClient, bucketName, dir, override):
    print("begin to upload files under " + dir)
    count = 0
    for filename in listAllFiles(dir):
        try:
            minioFileName = filename[len(dir) + 1:]
            if (not minioFileName.startswith("__MACOSX")) and (not minioFileName.endswith(".DS_Store")):
                minioClient.fput_object(bucketName, minioFileName, filename)
                print(filename + " is uploaded")
                count = count + 1
        except ResponseError as err:
            print("Failed to upload " + filename)
            print(err)

    print(str(count) + " files have been uploaded")


def main():
    print("update overridden by 020201009_tde_parsers_init.py")

if __name__ == "__main__":
    main()
