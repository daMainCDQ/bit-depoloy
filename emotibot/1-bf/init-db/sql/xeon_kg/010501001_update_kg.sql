-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `property_sandbox` ADD (
                    `speechType` int null COMMENT '话术类型');

ALTER TABLE `property` ADD (
                    `speechType` int null COMMENT '话术类型');


ALTER TABLE `extra_property_sandbox` ADD (
                    `speechType` int null COMMENT '话术类型');

ALTER TABLE `extra_property` ADD (
                    `speechType` int null COMMENT '话术类型');


ALTER TABLE `property_sandbox` ADD (
                    `rawSpeech` varchar(2048) null COMMENT '完整富文本话术');

ALTER TABLE `property` ADD (
                    `rawSpeech` varchar(2048) null COMMENT '完整富文本话术');


ALTER TABLE `property_sandbox` ADD (
                    `raw_default_value` varchar(2048) null COMMENT '完整属性简介');

ALTER TABLE `property` ADD (
                    `raw_default_value` varchar(2048) null COMMENT '完整属性简介');


ALTER TABLE `extra_property_sandbox` ADD (
                    `rawSpeech` varchar(2048) null COMMENT '完整富文本话术');

ALTER TABLE `extra_property` ADD (
                    `rawSpeech` varchar(2048) null COMMENT '完整富文本话术');


ALTER TABLE `entity_sandbox` ADD (
                    `rawName` text null COMMENT '完整富文本实体名称');

ALTER TABLE `entity` ADD (
                    `rawName` text null COMMENT '完整富文本实体名称');

Drop table if exists sync_record;

create table if not exists sync_record
(
  id           int auto_increment  primary key COMMENT '同步记录ID',
  appid        varchar(64)                            null COMMENT '机器人ID',
  training_status    int                              null COMMENT '训练状态',
  sync_status        int                              null COMMENT '同步状态',
  revert_status      int                              null COMMENT '回退状态',
  last_success_sync  int                              null COMMENT '是否曾经有同步成功',
  created_time timestamp default CURRENT_TIMESTAMP    not null COMMENT '建立时间',
  updated_time timestamp default CURRENT_TIMESTAMP    not null
  on update CURRENT_TIMESTAMP COMMENT '修改时间'
) COMMENT '同步记录表';
