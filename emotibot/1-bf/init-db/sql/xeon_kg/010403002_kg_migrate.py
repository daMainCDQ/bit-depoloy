import pymysql
import json
import os
import time
import collections

production_table = 'production'
sandbox_table = 'sandbox'

def get_connect_db():
    source_mysql_host = os.environ.get('INIT_MYSQL_HOST')
    source_mysql_port = int(os.environ.get('INIT_MYSQL_PORT'))
    source_mysql_user = os.environ.get('INIT_MYSQL_USER')
    source_mysql_password = os.environ.get('INIT_MYSQL_PASSWORD')
    # source_mysql_host = "172.16.105.217"
    # source_mysql_port = 3306
    # source_mysql_user = "root"
    # source_mysql_password = "password"

    source_mysql_db = 'xeon_kg'
    source_db = pymysql.connect(host=source_mysql_host, port=source_mysql_port, user=source_mysql_user,
                                passwd=source_mysql_password, db=source_mysql_db)
    return source_db


def new_property_speech_category_unit(table_type):
    production_table = 'production'
    sandbox_table = 'sandbox'
    if table_type != production_table and table_type != sandbox_table:
        return
    try:
        source_db = get_connect_db()

        if table_type == production_table:
            prop_speech_sql = "select content, entity_id, property_id, appid from speech where property_id is not null"
        if table_type == sandbox_table:
            prop_speech_sql = "select content, entity_id, property_id, s.appid from speech_sandbox as s " \
                              "left join property_sandbox as p on s.property_id=p.id " \
                              "where s.property_id is not null and s.sync_flag in (0, 1, 2) and p.sync_flag != 3"

        prop_speech_cursor = source_db.cursor()
        prop_speech_cursor.execute(prop_speech_sql)
        speech_infos = prop_speech_cursor.fetchall()

        property_speech_list = []
        extra_property_speech_dict = {}
        property_appid_dict = {}
        for info in speech_infos:
            speech, entity_id, property_id, appid = info

            property_appid_dict[property_id] = appid

            if entity_id is None:
                property_speech_list.append((speech, property_id))
            else:
                speech_key = str(property_id) + "_" + str(entity_id)
                extra_property_speech_dict[speech_key] = speech

        if len(property_speech_list) > 0:
            if table_type == production_table:
                insert_speech_sql = "update property set speech = %s where id = %s"
            if table_type == sandbox_table:
                insert_speech_sql = "update property_sandbox set speech = %s, sync_flag = '2' where id = %s and sync_flag != 3"
            insert_speech_cursor = source_db.cursor()
            insert_speech_cursor.executemany(insert_speech_sql, property_speech_list)

        if table_type == production_table:
            relation_info_sql = "select r.property_id, r.id_to, e.unit, e.type, r.appid " \
                                "from relation as r " \
                                "left join entity as e on r.id_to=e.id " \
                                "left join property as p on r.property_id=p.id " \
                                "where p.modifiable=1"
        if table_type == sandbox_table:
            relation_info_sql = "select r.property_id, r.id_to, e.unit, e.type, r.appid " \
                                "from relation_sandbox as r " \
                                "left join entity_sandbox as e on r.id_to=e.id " \
                                "left join property_sandbox as p on r.property_id=p.id " \
                                "where p.modifiable=1 and r.sync_flag in (0, 1, 2) " \
                                "and p.sync_flag != 3 and e.sync_flag != 3 "

        all_relation_cursor = source_db.cursor()
        all_relation_cursor.execute(relation_info_sql)
        all_relation_info = all_relation_cursor.fetchall()

        property_unit_category_dict = {}

        for info in all_relation_info:
            property_id, id_to, unit, category, appid = info

            property_appid_dict[property_id] = appid

            unit_category = json.dumps({
                "unit": str(unit) if unit is not None else None,
                "category": category
            })

            if property_id not in property_unit_category_dict:
                property_unit_category_dict[property_id] = {}
            if unit_category not in property_unit_category_dict[property_id]:
                property_unit_category_dict[property_id][unit_category] = set()

            property_unit_category_dict[property_id][unit_category].add(id_to)


        default_unit_category_list = []
        extra_property_list = []

        for property_id in property_unit_category_dict.keys():

            default_unit_category = max(property_unit_category_dict[property_id].items()
                                        , key=lambda x: len(x[1]))[0]

            default_unit_category_dict = json.loads(default_unit_category)

            unit = default_unit_category_dict['unit']
            category = default_unit_category_dict['category']
            default_unit_category_list.append((unit, category, property_id))

            for unit_category in property_unit_category_dict[property_id].keys():
                if unit_category == default_unit_category:
                    continue

                unit_category_dict = json.loads(unit_category)
                unit = unit_category_dict['unit']
                category = unit_category_dict['category']
                appid = property_appid_dict[property_id]

                for id_to in property_unit_category_dict[property_id][unit_category]:
                    speech_key = str(property_id) + "_" + str(id_to)
                    speech = extra_property_speech_dict.pop(speech_key, None)
                    extra_property_list.append((property_id, id_to, unit, category, speech, appid))

        for speech_key in extra_property_speech_dict.keys():
            property_id = int(speech_key.split("_")[0])
            entity_id = int(speech_key.split("_")[1])
            speech = extra_property_speech_dict[speech_key]
            appid = property_appid_dict[property_id]
            extra_property_list.append((property_id, entity_id, None, None, speech, appid))

        if len(default_unit_category_list) > 0:
            if table_type == production_table:
                default_unit_category_sql = "update property set unit = %s, category = %s where id = %s"
            if table_type == sandbox_table:
                default_unit_category_sql = "update property_sandbox set unit = %s, category = %s, sync_flag = '2' where id = %s and sync_flag != 3"
            default_unit_category_cursor = source_db.cursor()
            default_unit_category_cursor.executemany(default_unit_category_sql, default_unit_category_list)

        if table_type == production_table:
            default_unit_category_string_sql = "update property set category = 2 " \
                                               "where modifiable=1 and category is null and unit is null"
            default_unit_category_float_sql = "update property set category = 3 " \
                                              "where modifiable=1 and category is null and unit is not null"
        if table_type == sandbox_table:
            default_unit_category_string_sql = "update property_sandbox set category = 2, sync_flag = '2' " \
                                               " where modifiable=1 and category is null and unit is null and sync_flag != 3"
            default_unit_category_float_sql = "update property_sandbox set category = 3, sync_flag = '2' " \
                                              "where modifiable=1 and category is null and unit is not null and sync_flag != 3"
        default_unit_category_string_cursor = source_db.cursor()
        default_unit_category_string_cursor.execute(default_unit_category_string_sql)
        default_unit_category_float_cursor = source_db.cursor()
        default_unit_category_float_cursor.execute(default_unit_category_float_sql)

        if len(extra_property_list) > 0:
            if table_type == production_table:
                insert_extra_sql = "insert into extra_property (property_id, entity_id, unit, category, speech, appid) " \
                                   "values(%s, %s, %s, %s, %s, %s)"
                insert_extra_cursor = source_db.cursor()
                insert_extra_cursor.executemany(insert_extra_sql, extra_property_list)

            if table_type == sandbox_table:
                sandbox_extra_property_list = []
                max_id = 0
                production_extra_sql = "select id, appid from extra_property"
                production_extra_cursor = source_db.cursor()
                production_extra_cursor.execute(production_extra_sql)
                production_extra_infos = production_extra_cursor.fetchall()
                production_extra_dict = collections.defaultdict(list)
                for production_info in production_extra_infos:
                    t_id, appid = production_info
                    production_extra_dict[appid].append(t_id)
                    max_id = max(max_id, t_id)

                for extra_item in extra_property_list:
                    property_id, entity_id, unit, category, speech, appid = extra_item
                    if production_extra_dict[appid]:
                        t_id = production_extra_dict[appid].pop(0)
                    else:
                        max_id += 1
                        t_id = max_id
                    sandbox_extra_property_list.append(
                        (t_id, property_id, entity_id, unit, category, speech, appid, '2')
                    )

                for appid, id_list in production_extra_dict.items():
                    for t_id in id_list:
                        sandbox_extra_property_list.append(
                            (t_id, -1, -1, '', -1, '', appid, '3')
                        )

                insert_extra_sql = "insert into extra_property_sandbox (id, property_id, entity_id, unit, category, speech, appid, sync_flag) values(%s, %s, %s, %s, %s, %s, %s, %s)"
                insert_extra_cursor = source_db.cursor()
                insert_extra_cursor.executemany(insert_extra_sql, sandbox_extra_property_list)
        # 3. 提交变更
        source_db.commit()
    except Exception as e:
        print(e)
        if source_db is not None:
            source_db.rollback()
    finally:
        if source_db is not None:
            source_db.close()

def new_handler_property_intro(table_type):
    production_table = 'production'
    sandbox_table = 'sandbox'
    if table_type != production_table and table_type != sandbox_table:
        return
    try:
        source_db = get_connect_db()
        # 遍历property表中所有intro数据
        if table_type == production_table:
            prop_intro_sql = "select id, default_value from property where modifiable = 1 and default_value is not NULL"
        if table_type == sandbox_table:
            prop_intro_sql = "select id, default_value from property_sandbox where modifiable = 1 and default_value is not NULL and sync_flag != 3"
        prop_intro_cursor = source_db.cursor()
        prop_intro_cursor.execute(prop_intro_sql)
        prop_intros = prop_intro_cursor.fetchall()

        update_prop_condition_list = []
        if prop_intros is not None and len(prop_intros) > 0:
            for prop_intro in prop_intros:
                prop_id = prop_intro[0]
                intro = prop_intro[1]
                if intro is not None:
                    # 将intro的内容转换成新版KG的格式
                    new_intro = json.dumps([{
                        "type": "INTRODUCTION",
                        "entity": [],
                        "property": [],
                        "value": intro
                    }])
                    # 将新数据回写进property表
                    update_prop_condition_list.append((new_intro, prop_id))
            if table_type == production_table:
                update_prop_sql = "update property set default_value=%s where id = %s"
            if table_type == sandbox_table:
                update_prop_sql = "update property_sandbox set default_value=%s, sync_flag = '2' where id = %s and sync_flag != 3"
            update_prop_cursor = source_db.cursor()
            update_prop_cursor.executemany(update_prop_sql, update_prop_condition_list)
        source_db.commit()
    except Exception as e:
        print(e)
        if source_db is not None:
            source_db.rollback()
    finally:
        if source_db is not None:
            source_db.close()


def new_handler_entity_intro(table_type):
    production_table = 'production'
    sandbox_table = 'sandbox'
    if table_type != production_table and table_type != sandbox_table:
        return
    try:
        source_db = get_connect_db()
        # 遍历db中what is 属性的属性id
        if table_type == production_table:
            what_is_sql = "select e.id, e.name " \
                          "from relation as r " \
                          "left join entity as e on r.id_to=e.id " \
                          "left join property as p on r.property_id=p.id " \
                          "where p.type='what_is';"
        if table_type == sandbox_table:
            # 清理脏数据
            select_clear_sql = "select r.id " \
                               "from relation_sandbox as r " \
                               "left join entity_sandbox as e on r.id_to = e.id " \
                               "left join property_sandbox as p on r.property_id = p.id " \
                               "where p.type = 'what_is' and r.sync_flag in (0, 1, 2) and " \
                               "(e.id is null or p.id is null or e.sync_flag = 3 or p.sync_flag = 3) "
            select_clear_cursor = source_db.cursor()
            select_clear_cursor.execute(select_clear_sql)
            clear_id_list = select_clear_cursor.fetchall()

            if clear_id_list is not None and len(clear_id_list) > 0:
                clear_sql = "update relation_sandbox set sync_flag = '3' where id = %s"
                clear_cursor = source_db.cursor()
                clear_cursor.executemany(clear_sql, clear_id_list)

            what_is_sql = "select e.id, e.name " \
                          "from relation_sandbox as r " \
                          "left join entity_sandbox as e on r.id_to=e.id " \
                          "left join property_sandbox as p on r.property_id=p.id " \
                          "where p.type='what_is' and r.sync_flag in (0, 1, 2)"

        what_is_cursor = source_db.cursor()
        what_is_cursor.execute(what_is_sql)
        what_is_list = what_is_cursor.fetchall()

        introduction_list = []
        if what_is_list is not None and len(what_is_list) > 0:
            for what_is_info in what_is_list:
                entity_id, entity_name = what_is_info

                # 将intro的内容转换成新版KG的格式
                new_intro = json.dumps([{
                    "type": "INTRODUCTION",
                    "entity": [],
                    "property": [],
                    "value": entity_name
                }])

                introduction_list.append((new_intro, entity_id))

            # 将新数据回写进entity表
            if table_type == production_table:
                update_prop_sql = "update entity set name=%s where id = %s"
            if table_type == sandbox_table:
                update_prop_sql = "update entity_sandbox set name=%s, sync_flag = 2 where id = %s and sync_flag != 3"
            update_prop_cursor = source_db.cursor()
            update_prop_cursor.executemany(update_prop_sql, introduction_list)

        source_db.commit()
    except Exception as e:
        print(e)
        if source_db is not None:
            source_db.rollback()
    finally:
        if source_db is not None:
            source_db.close()


def data_sync_record():
    try:
        source_db = get_connect_db()

        appid_sql = "select distinct appid " \
                    "from ( " \
                    "select distinct appid from entity " \
                    "union all " \
                    "select distinct appid from property " \
                    ") as a"
        select_appid_cursor = source_db.cursor()
        select_appid_cursor.execute(appid_sql)
        appid_list = select_appid_cursor.fetchall()

        insert_record_set = set()
        for appid in appid_list:
            insert_record_set.add(appid)

        insert_record_list = list(insert_record_set)

        insert_record_sql = "insert into data_sync_update_record (sync_stamp, appid, version) " \
                            "values('init_stamp', %s, '1.7.3')"
        insert_record_cursor = source_db.cursor()
        insert_record_cursor.executemany(insert_record_sql, insert_record_list)

        source_db.commit()
    except Exception as e:
        print(e)
        if source_db is not None:
            source_db.rollback()
    finally:
        if source_db is not None:
            source_db.close()


if __name__ == '__main__':
    production_table = 'production'
    sandbox_table = 'sandbox'

    print('start handler propert_speech_category_unit migrate')
    new_property_speech_category_unit(production_table)
    new_property_speech_category_unit(sandbox_table)
    print('end handler propert_speech migrate')

    print('start handler prop_intro migrate')
    new_handler_property_intro(production_table)
    new_handler_property_intro(sandbox_table)
    print('end handler_upload_history migrate')

    print('start handler entity_intro migrate')
    new_handler_entity_intro(production_table)
    new_handler_entity_intro(sandbox_table)
    print('end handler_upload_history migrate')

    print('start handler_data_sync_recprd migrate')
    data_sync_record()
    print('end handler_data_sync_recprd migrate')