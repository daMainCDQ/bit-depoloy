-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `entity` ADD (
                    `parent_id` int null COMMENT '实体所属父实体ID',
                    `layer` int null COMMENT '实体所属层级');

ALTER TABLE `entity_sandbox` ADD (`parent_id` int null COMMENT '实体所属父实体ID',
                                 `layer` int null COMMENT '实体所属层级');

ALTER TABLE `property` ADD (
                      `category` varchar(10) null COMMENT '属性值所属类型',
                      `unit` varchar(32) null COMMENT '属性值单位',
                      `speech` varchar(1024) null COMMENT '属性默认话术',
                      `parent_id` int default '-1' COMMENT '父级属性ID');

ALTER TABLE property MODIFY default_value text COMMENT '属性简介';
UPDATE property set name='简介' where type='what_is';

ALTER TABLE `property_sandbox` ADD (
                      `category` varchar(10) null COMMENT '属性值所属类型',
                      `unit` varchar(32) null COMMENT '属性值单位',
                      `speech` varchar(1024) null COMMENT '属性默认话术',
                      `parent_id` int default '-1' COMMENT '父级属性ID');

ALTER TABLE property_sandbox MODIFY default_value text COMMENT '属性简介';
UPDATE property_sandbox set name='简介' where type='what_is';

ALTER TABLE `test_case` ADD COLUMN `datasetname` VARCHAR(128) NULL COMMENT '测试集名称';

-- ALTER TABLE `data_sync_update_record` 
--             ADD COLUMN `version` VARCHAR(128) NULL COMMENT '知识图谱版本名称';


-- UPDATE `data_sync_update_record` set version = '1.8.3'

create table edition_manage
(
  id                int              auto_increment  primary key COMMENT '类型自增ID',
  appid             varchar(64)                         null COMMENT '机器人ID',
  current_edition   varchar(64)                         not null COMMENT '当前生产环境版本ID',
  last_edition      varchar(64)                         not null COMMENT '上一次成功导入生产环境版本ID',
  current_sandbox   varchar(64)                         not null COMMENT '当前沙箱环境ID',
  created_time      timestamp default CURRENT_TIMESTAMP not null COMMENT '建立时间',
  updated_time      timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP COMMENT '修改时间'
) COMMENT '知识图谱版本管理表';

create table audit_record
(
  id                int              auto_increment  primary key COMMENT '变更记录产生时的当前沙箱版本号',
  appid             varchar(64)                         null COMMENT '机器人ID',
  sandbox_edition   varchar(64)                         not null COMMENT '当前生产环境版本ID',
  operation_id      varchar(64)                         not null COMMENT '操作ID',
  operation_type    int                                 null     COMMENT '操作类型',
  operation_object  int                                 null     COMMENT '操作涉及对象类型',
  operation_sub_object  int                             null     COMMENT '操作涉及',
  audit_status      int                                 null     COMMENT '审核记录情况',
  created_time      timestamp default CURRENT_TIMESTAMP not null COMMENT '建立时间',
  updated_time      timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP COMMENT '修改时间'
) COMMENT '知识图谱变更审核记录表';

create table audit_record_detail
(
  id                int              auto_increment  primary key COMMENT '变更记录产生时的当前沙箱版本号',
  appid             varchar(64)                         null COMMENT '机器人ID',
  operation_id      varchar(64)                         not null COMMENT '操作ID',
  refer_table_name  varchar(128)                        null     COMMENT '操作涉及的表格名称',
  refer_table_key   varchar(128)                        null     COMMENT '操作涉及的表格的主键',
  refer_table_operation     int                         null     COMMENT '操作涉及的表格的对数据的操作',
  created_time  timestamp default CURRENT_TIMESTAMP not null COMMENT '建立时间',
  updated_time  timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP COMMENT '修改时间'
) COMMENT '知识图谱变更审核记录详细记录表';

create table data_sync_update_record
(
  id           int auto_increment  primary key COMMENT '类型自增ID',
  sync_stamp   varchar(36)                         not null COMMENT 'ES数据同步记录ID',
  appid         varchar(64)                         null COMMENT '机器人ID',
  version       varchar(64)                         null COMMENT '知识图谱版本名称',
  created_time  timestamp default CURRENT_TIMESTAMP not null COMMENT '建立时间',
  updated_time  timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP COMMENT '修改时间'
) COMMENT 'ES数据同步记录表';

create table extra_property
(
  id           int auto_increment primary key COMMENT '类型自增ID',
  entity_id    int                                 null COMMENT '关联实体ID',
  property_id  int                                 null COMMENT '关联属性ID',
  category     varchar(10)                         null COMMENT '补充属性类别',
  unit         varchar(32)                         null COMMENT '补充属性值单位',
  speech       varchar(1024)                       null COMMENT '补充默认话术',
  appid         varchar(64)                         null COMMENT '机器人ID',
  created_time  timestamp default CURRENT_TIMESTAMP not null COMMENT '建立时间',
  updated_time  timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP COMMENT '修改时间'
) COMMENT '自定义属性补充表';

create table extra_property_sandbox
(
  sync_flag    varchar(36) default 1                not null COMMENT '同步状态 0已同步；1添加；2更新；3删除',
  id           int auto_increment primary key COMMENT '类型自增ID',
  entity_id    int                                 null COMMENT '关联实体ID',
  property_id  int                                 null COMMENT '关联属性ID',
  category     varchar(10)                         null COMMENT '补充属性类别',
  unit         varchar(32)                         null COMMENT '补充属性值单位',
  speech       varchar(1024)                       null COMMENT '补充默认话术',
  appid         varchar(64)                         null COMMENT '机器人ID',
  created_time  timestamp default CURRENT_TIMESTAMP not null COMMENT '建立时间',
  updated_time  timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP COMMENT '修改时间'
) COMMENT '自定义属性补充沙箱表';

create table interface_property_slot
(
  id           int auto_increment primary key COMMENT '类型自增ID',
  property_id  int                                 not null COMMENT '接口类型属性ID',
  slot_name    varchar(64)                         not null COMMENT '槽位名称',
  slot_type    varchar(64)                         not null COMMENT '槽位类型ID',
  url          varchar(255)                        null COMMENT '槽位对应的URL',
  target_id    int                                 null COMMENT '该槽位所属实体Id',
  appid         varchar(64)                         null COMMENT '机器人ID',
  created_time  timestamp default CURRENT_TIMESTAMP not null COMMENT '建立时间',
  updated_time  timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP COMMENT '修改时间'
) COMMENT '接口类型属性实例与槽位关系表';

create table interface_property_slot_sandbox
(
  sync_flag    varchar(36) default 1                not null COMMENT '同步状态 0已同步；1添加；2更新；3删除',
  id           int auto_increment primary key COMMENT '类型自增ID',
  property_id  int                                 not null COMMENT '接口类型属性ID',
  slot_name    varchar(64)                         not null COMMENT '槽位名称',
  slot_type    varchar(64)                         not null COMMENT '槽位类型ID',
  url          varchar(255)                        null COMMENT '槽位对应的URL',
  target_id    int                                 null COMMENT '该槽位所属实体Id',
  appid         varchar(64)                         null COMMENT '机器人ID',
  created_time  timestamp default CURRENT_TIMESTAMP not null COMMENT '建立时间',
  updated_time  timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP COMMENT '修改时间'
) COMMENT '接口类型属性实例与槽位关系沙箱表';

create table interface_property_slot_type
(
  id           int auto_increment primary key COMMENT '类型自增ID',
  slot_type_id   int                                 not null COMMENT '槽位类型ID',
  slot_type_name varchar(64)                         not null COMMENT '槽位类型名称',
  appid         varchar(64)                         null COMMENT '机器人ID',
  created_time  timestamp default CURRENT_TIMESTAMP not null COMMENT '建立时间',
  updated_time  timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP COMMENT '修改时间'
) COMMENT '槽位类型表';

create table keyword_synonym
(
  id           int auto_increment primary key COMMENT '类型自增ID',
  name         varchar(1024)                       null COMMENT '关键词内容',
  type         varchar(32)                         null COMMENT '关键词类型',
  synonym      varchar(255)                        null COMMENT '同义词',
  appid        varchar(64)                         null COMMENT '机器人ID',
  created_time timestamp default CURRENT_TIMESTAMP not null COMMENT '建立时间',
  updated_time timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP COMMENT '修改时间'
) COMMENT '关键词同义词关系表';

create table keyword_synonym_sandbox
(
  sync_flag    varchar(36) default 1                not null COMMENT '同步状态 0已同步；1添加；2更新；3删除',
  id           int auto_increment primary key COMMENT '类型自增ID',
  name         varchar(1024)                       null COMMENT '关键词内容',
  type         varchar(32)                         null COMMENT '关键词类型',
  synonym      varchar(255)                        null COMMENT '同义词',
  appid        varchar(64)                         null COMMENT '机器人ID',
  created_time timestamp default CURRENT_TIMESTAMP not null COMMENT '建立时间',
  updated_time timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP COMMENT '修改时间'
) COMMENT '关键词同义词关系沙箱表';

create table kg_files
(
  id           int auto_increment primary key COMMENT '类型自增ID',
  filename     varchar(64) default ''             not null COMMENT '文件名称',
  appid        varchar(64)                           not null COMMENT '机器人ID',
  created_time  timestamp default CURRENT_TIMESTAMP not null COMMENT '建立时间'
) COMMENT 'kg上传数据文件表';
create index appid on kg_files (appid);

create table import_data
(
  id           int auto_increment primary key COMMENT '自增id',
  appid        varchar(64)                         not null COMMENT '机器人id',
  sheet_name   varchar(64)                         not null COMMENT '页名称',
  entities     mediumtext                                null COMMENT '实体id列表',
  properties   mediumtext                                null COMMENT '属性id列表',
  created_time timestamp default CURRENT_TIMESTAMP not null COMMENT '建立时间',
  updated_time timestamp default CURRENT_TIMESTAMP not null COMMENT '修改时间'
) COMMENT '导入信息表';

INSERT INTO xeon_kg.type (name, type_id, appid) VALUES ('keyword', 5, '');
INSERT INTO xeon_kg.type (name, type_id, appid) VALUES ('date', 7, '');
INSERT INTO xeon_kg.type (name, type_id, appid) VALUES ('range_float', 8, '');
-- INSERT INTO xeon_kg.type (name, type_id, appid) VALUES ('interface', 9, '');

INSERT INTO xeon_kg.interface_property_slot_type (slot_type_id, slot_type_name, appid) VALUES (1, 'entity', '');
INSERT INTO xeon_kg.interface_property_slot_type (slot_type_id, slot_type_name, appid) VALUES (2, 'float', '');
INSERT INTO xeon_kg.interface_property_slot_type (slot_type_id, slot_type_name, appid) VALUES (3, 'property', '');
INSERT INTO xeon_kg.interface_property_slot_type (slot_type_id, slot_type_name, appid) VALUES (4, 'date', '');
INSERT INTO xeon_kg.interface_property_slot_type (slot_type_id, slot_type_name, appid) VALUES (5, 'range_float', '');

-- create index for tables

CREATE INDEX entity_sandbox_appid_type_syncflag_index on entity_sandbox (appid(40), type, sync_flag(2));
CREATE INDEX relation_sandbox_id_from_index ON relation_sandbox (id_from);
CREATE INDEX relation_sandbox_id_to_index ON relation_sandbox (id_to);


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
USE `xeon_kg`;

START TRANSACTION;

ALTER TABLE `property` DROP `category`;
ALTER TABLE `property` DROP `unit`;
ALTER TABLE `property` DROP `speech`;
ALTER TABLE `property` DROP `parent_id`;
ALTER TABLE property MODIFY default_value varchar(1024) COMMENT '属性简介';
UPDATE property set name='是什么' where type='what_is';

ALTER TABLE `property_sandbox` DROP `category`;
ALTER TABLE `property_sandbox` DROP `unit`;
ALTER TABLE `property_sandbox` DROP `speech`;
ALTER TABLE `property_sandbox` DROP `parent_id`;
ALTER TABLE property_sandbox MODIFY default_value varchar(1024) COMMENT '属性简介';
UPDATE property_sandbox set name='是什么' where type='what_is';

DROP TABLE `data_sync_update_record`;
DROP TABLE `extra_property`;
DROP TABLE `extra_property_sandbox`;
DROP TABLE `interface_property_slot`;
DROP TABLE `interface_property_slot_sandbox`;
DROP TABLE `interface_property_slot_type`;
DROP TABLE `keyword_synonym`;
DROP TABLE `keyword_synonym_sandbox`;
DROP TABLE `kg_files`;

DELETE FROM `type` WHERE `name` = 'keyword';
DELETE FROM `type` WHERE `name` = 'date';
DELETE FROM `type` WHERE `name` = 'range_float';
DELETE FROM `type` WHERE `name` = 'interface';

-- drop index
ALTER TABLE entity_sandbox DROP INDEX entity_sandbox_appid_type_syncflag_index;
ALTER TABLE relation_sandbox DROP INDEX relation_sandbox_id_from_index;
ALTER TABLE relation_sandbox DROP INDEX relation_sandbox_id_to_index;