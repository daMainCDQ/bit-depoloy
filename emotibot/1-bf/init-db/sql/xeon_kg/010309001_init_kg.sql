-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE DATABASE IF NOT EXISTS `xeon_kg` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `xeon_kg`;

START TRANSACTION;
create table entity
(
  id           int auto_increment  primary key COMMENT '实体ID',
  name         text                                   null COMMENT '实体名称',
  type         int                                    not null COMMENT '实体类型',
  appid        varchar(64)                            null COMMENT '机器人ID',
  unit         varchar(16)                            null COMMENT '实体单位',
  created_time timestamp default CURRENT_TIMESTAMP    not null COMMENT '建立时间',
  updated_time timestamp default CURRENT_TIMESTAMP    not null
  on update CURRENT_TIMESTAMP COMMENT '修改时间'
) COMMENT '实体表';
create index entity_appid_index on entity (appid);

create table entity_sandbox
(
  sync_flag    varchar(36) default 1               not null COMMENT '同步状态 0已同步；1添加；2更新；3删除',
  id           int auto_increment  primary key COMMENT '实体ID',
  name         text                                   null COMMENT '实体名称',
  type         int                                    not null COMMENT '实体类型',
  appid        varchar(64)                            null COMMENT '机器人ID',
  unit         varchar(16)                            null COMMENT '实体单位',
  created_time timestamp default CURRENT_TIMESTAMP    not null COMMENT '建立时间',
  updated_time timestamp default CURRENT_TIMESTAMP    not null
  on update CURRENT_TIMESTAMP COMMENT '修改时间'
) COMMENT '实体沙箱表';
create index entity_sandbox_appid_index on entity_sandbox (appid);

create table entity_tag
(
  id            int auto_increment primary key COMMENT '实体标签ID',
  entity_set_id int                                 not null COMMENT '实体集合ID',
  entity_id     int                                 not null COMMENT '实体ID',
  tag_id        int                                 not null COMMENT '标签ID',
  tag_value     varchar(255)                        null COMMENT '标签值',
  tag_type      int                                 null COMMENT '标签类型',
  tag_unit      varchar(32)                         null COMMENT '标签单位',
  appid         varchar(64)                         null COMMENT '机器人ID',
  created_time  timestamp default CURRENT_TIMESTAMP not null COMMENT '建立时间',
  updated_time  timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP COMMENT '修改时间'
) COMMENT '实体标签表 ';

create table entity_tag_sandbox
(
  sync_flag    varchar(36) default 1                not null COMMENT '同步状态 0已同步；1添加；2更新；3删除',
  id            int auto_increment primary key COMMENT '实体标签ID',
  entity_set_id int                                 not null COMMENT '实体集合ID',
  entity_id     int                                 not null COMMENT '实体ID',
  tag_id        int                                 not null COMMENT '标签ID',
  tag_value     varchar(255)                        null COMMENT '标签值',
  tag_type      int                                 null COMMENT '标签类型',
  tag_unit      varchar(32)                         null COMMENT '标签单位',
  appid         varchar(64)                         null COMMENT '机器人ID',
  created_time  timestamp default CURRENT_TIMESTAMP not null COMMENT '建立时间',
  updated_time  timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP COMMENT '修改时间'
) COMMENT '实体标签沙箱表';

create table ft_service_task
(
  id            int auto_increment
    primary key COMMENT '训练任务自增ID',
  training_task varchar(64)                         null COMMENT '训练任务ID',
  status        int                                 null COMMENT '训练状态',
  type          varchar(32)                         null COMMENT '模型类型',
  model_name    varchar(128)                        null COMMENT '模型名称',
  appid         varchar(64)                         null COMMENT '机器人ID',
  created_time  timestamp default CURRENT_TIMESTAMP not null COMMENT '建立时间',
  updated_time  timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP COMMENT '修改时间'
) COMMENT '模型训练表';

create table ft_service_task_sandbox
(
  sync_flag    varchar(36) default 1                not null COMMENT '同步状态 0已同步；1添加；2更新；3删除',
  id            int auto_increment
    primary key COMMENT '训练任务自增ID',
  training_task varchar(64)                         null COMMENT '训练任务ID',
  status        int                                 null COMMENT '训练状态',
  type          varchar(32)                         null COMMENT '模型类型',
  model_name    varchar(128)                        null COMMENT '模型名称',
  appid         varchar(64)                         null COMMENT '机器人ID',
  created_time  timestamp default CURRENT_TIMESTAMP not null COMMENT '建立时间',
  updated_time  timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP COMMENT '修改时间'
) COMMENT '模型训练沙箱表';

create table intent_corpus
(
  id           int auto_increment primary key COMMENT '意图语料ID',
  data         varchar(255) not null COMMENT '意图语料',
  intent       varchar(64)                             null COMMENT '意图标签',
  appid        varchar(64)                             null COMMENT '机器人ID',
  created_time timestamp default CURRENT_TIMESTAMP     not null COMMENT '建立时间',
  updated_time  timestamp default CURRENT_TIMESTAMP not null COMMENT '修改时间'
) COMMENT '意图语料表';

create table intent_corpus_sandbox
(
  sync_flag    varchar(36) default 1                     not null COMMENT '同步状态 0已同步；1添加；2更新；3删除',
  id           int auto_increment primary key COMMENT '意图语料ID',
  data         varchar(255) not null COMMENT '意图语料',
  intent       varchar(64)                             null COMMENT '意图标签',
  appid        varchar(64)                             null COMMENT '机器人ID',
  created_time timestamp default CURRENT_TIMESTAMP     not null COMMENT '建立时间',
  updated_time  timestamp default CURRENT_TIMESTAMP not null COMMENT '修改时间'
) COMMENT '意图语料沙箱表';

create table multi_corpus
(
  id           int auto_increment primary key COMMENT '属性语料ID',
  data         varchar(255)                        not null COMMENT '属性语料',
  entities     varchar(255)                        null COMMENT '实体标签',
  properties   varchar(255)                        null COMMENT '属性标签',
  intent       varchar(64)                         null COMMENT '意图标签',
  `condition`  varchar(255)                        null COMMENT '条件标签',
  appid        varchar(64)                         null COMMENT '机器人ID',
  created_time  timestamp default CURRENT_TIMESTAMP not null COMMENT '建立时间',
  updated_time  timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP COMMENT '修改时间'
) COMMENT '属性语料表';

create table multi_corpus_sandbox
(
  sync_flag    varchar(36) default 1                     not null COMMENT '同步状态 0已同步；1添加；2更新；3删除',
  id           int auto_increment primary key COMMENT '属性语料ID',
  data         varchar(255)                        not null COMMENT '属性语料',
  entities     varchar(255)                        null COMMENT '实体标签',
  properties   varchar(255)                        null COMMENT '属性标签',
  intent       varchar(64)                         null COMMENT '意图标签',
  `condition`  varchar(255)                        null COMMENT '条件标签',
  appid        varchar(64)                         null COMMENT '机器人ID',
  created_time  timestamp default CURRENT_TIMESTAMP not null COMMENT '建立时间',
  updated_time  timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP COMMENT '修改时间'
) COMMENT '属性语料沙箱表';

create table property
(
  id            int auto_increment primary key COMMENT '属性ID',
  name          varchar(255) null COMMENT '属性名称',
  modifiable    tinyint                                 null COMMENT '是否可修改',
  type          varchar(32)                             not null COMMENT '属性类型',
  appid         varchar(64)                             null COMMENT '机器人ID',
  default_value varchar(1024)                           null COMMENT '属性简介',
  created_time  timestamp default CURRENT_TIMESTAMP not null COMMENT '建立时间',
  updated_time  timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP COMMENT '修改时间'
) COMMENT '属性表';
create index property_appid_index on property (appid);

create table property_sandbox
(
  sync_flag    varchar(36) default 1                not null COMMENT '同步状态 0已同步；1添加；2更新；3删除',
  id            int auto_increment primary key COMMENT '属性ID',
  name          varchar(255) null COMMENT '属性名称',
  modifiable    tinyint                                 null COMMENT '是否可修改',
  type          varchar(32)                             not null COMMENT '属性类型',
  appid         varchar(64)                             null COMMENT '机器人ID',
  default_value varchar(1024)                           null COMMENT '属性简介',
  created_time  timestamp default CURRENT_TIMESTAMP not null COMMENT '建立时间',
  updated_time  timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP COMMENT '修改时间'
) COMMENT '属性沙箱表';
create index property_sandbox_appid_index on property_sandbox (appid);

create table relation
(
  id                  int auto_increment primary key COMMENT '关系ID',
  id_from             int                                 not null COMMENT '关系起始ID',
  property_id         int                                 not null COMMENT '关系属性ID',
  id_to               int                                 not null COMMENT '关系终点ID',
  relation_type       varchar(32)                         not null COMMENT '关系类型',
  appid               varchar(64)                         null COMMENT '机器人ID',
  created_time  timestamp default CURRENT_TIMESTAMP not null COMMENT '建立时间',
  updated_time  timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP COMMENT '修改时间',
  inherited_entity_id int default '0'                     null COMMENT '属性继承'
) COMMENT '实体关系表';
CREATE INDEX relation_property_id_appid_index ON relation (property_id, appid);
CREATE INDEX relation_appid_id_from_id_to_index ON relation (appid, id_from, id_to);
CREATE INDEX relation_appid_id_from_property_id_index ON relation (appid, id_from, property_id);


create table relation_sandbox
(
  sync_flag    varchar(36) default 1                     not null COMMENT '同步状态 0已同步；1添加；2更新；3删除',
  id                  int auto_increment primary key COMMENT '关系ID',
  id_from             int                                 not null COMMENT '关系起始ID',
  property_id         int                                 not null COMMENT '关系属性ID',
  id_to               int                                 not null COMMENT '关系终点ID',
  relation_type       varchar(32)                         not null COMMENT '关系类型',
  appid               varchar(64)                         null COMMENT '机器人ID',
  created_time  timestamp default CURRENT_TIMESTAMP not null COMMENT '建立时间',
  updated_time  timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP COMMENT '修改时间',
  inherited_entity_id int default '0'                     null COMMENT '属性继承'
) COMMENT '实体关系沙箱表';
CREATE INDEX relation_sandbox_property_id_appid_index ON relation_sandbox (property_id, appid);
CREATE INDEX relation_sandbox_appid_id_from_id_to_index ON relation_sandbox (appid, id_from, id_to);
CREATE INDEX relation_sandbox_appid_id_from_property_id_index ON relation_sandbox (appid, id_from, property_id);

create table speech
(
  id           int auto_increment primary key COMMENT '话术ID',
  entity_id    int                                     null COMMENT '话术对应实体ID',
  property_id  int                                     null COMMENT '话术对应属性ID',
  content      varchar(255) null COMMENT '话术内容',
  tags         varchar(255)                            null COMMENT '话术标签',
  begin_time   datetime default '1970-01-01 00:00:00'  null COMMENT '话术有效开始时间',
  end_time     datetime default '2999-12-31 23:59:59'  null COMMENT '话术有效结束时间',
  appid        varchar(64)                             null COMMENT '机器人ID',
  created_time  timestamp default CURRENT_TIMESTAMP not null COMMENT '建立时间',
  updated_time  timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP COMMENT '修改时间'
) COMMENT '实体属性对应话术表';
create index speech_property_id_index on speech (property_id);

create table speech_sandbox
(
  sync_flag    varchar(36) default 1                     not null COMMENT '同步状态 0已同步；1添加；2更新；3删除',
  id           int auto_increment primary key COMMENT '话术ID',
  entity_id    int                                     null COMMENT '话术对应实体ID',
  property_id  int                                     null COMMENT '话术对应属性ID',
  content      varchar(255) null COMMENT '话术内容',
  tags         varchar(255)                            null COMMENT '话术标签',
  begin_time   datetime default '1970-01-01 00:00:00'  null COMMENT '话术有效开始时间',
  end_time     datetime default '2999-12-31 23:59:59'  null COMMENT '话术有效结束时间',
  appid        varchar(64)                             null COMMENT '机器人ID',
  created_time  timestamp default CURRENT_TIMESTAMP not null COMMENT '建立时间',
  updated_time  timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP COMMENT '修改时间'
) COMMENT '实体属性对应话术沙箱表';
create index speech_sandbox_property_id_index on speech_sandbox (property_id);

create table synonym
(
  id           int auto_increment primary key COMMENT '同义词ID',
  object_id    int                                     not null COMMENT '目标ID',
  type         varchar(32)                             not null COMMENT '目标类型',
  synonym      varchar(255)                            not null COMMENT '同义词内容',
  appid        varchar(64)                             null COMMENT '机器人ID',
  created_time  timestamp default CURRENT_TIMESTAMP not null COMMENT '建立时间',
  updated_time  timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP COMMENT '修改时间'
) COMMENT '实体和属性同义词表';
CREATE INDEX synonym_type_object_id_index ON synonym (type, object_id);
CREATE INDEX synonym_object_id_index ON synonym (object_id);

create table synonym_sandbox
(
  sync_flag    varchar(36) default 1               not null COMMENT '同步状态 0已同步；1添加；2更新；3删除',
  id           int auto_increment primary key COMMENT '同义词ID',
  object_id    int                                     not null COMMENT '目标ID',
  type         varchar(32)                             not null COMMENT '目标类型',
  synonym      varchar(255)                            not null COMMENT '同义词内容',
  appid        varchar(64)                             null COMMENT '机器人ID',
  created_time  timestamp default CURRENT_TIMESTAMP not null COMMENT '建立时间',
  updated_time  timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP COMMENT '修改时间'
) COMMENT '实体和属性同义词沙箱表';
CREATE INDEX synonym_sandbox_type_object_id_index ON synonym_sandbox (type, object_id);
CREATE INDEX synonym_sandbox_object_id_index ON synonym_sandbox (object_id);

create table tag
(
  id           int auto_increment primary key COMMENT '标签ID',
  name         varchar(255)                        null COMMENT '标签名称',
  appid        varchar(64)                         null COMMENT '机器人ID',
  created_time  timestamp default CURRENT_TIMESTAMP not null COMMENT '建立时间',
  updated_time  timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP COMMENT '修改时间'
) COMMENT '标签表';

create table tag_sandbox
(
  sync_flag    varchar(36) default 1               not null COMMENT '同步状态 0已同步；1添加；2更新；3删除',
  id           int auto_increment primary key COMMENT '标签ID',
  name         varchar(255)                        null COMMENT '标签名称',
  appid        varchar(64)                         null COMMENT '机器人ID',
  created_time  timestamp default CURRENT_TIMESTAMP not null COMMENT '建立时间',
  updated_time  timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP COMMENT '修改时间'
) COMMENT '标签沙箱表';

create table task_result
(
  id           int auto_increment primary key COMMENT '测试任务结果ID',
  case_id      int                                      not null COMMENT '测试案例ID',
  refer_result varchar(1024)                            null COMMENT '测试结果',
  task_id      varchar(64)                              not null COMMENT '测试任务ID',
  check_result varchar(64)                              null COMMENT '案例测试结果',
  appid        varchar(64)                              null COMMENT '机器人ID',
  created_time  timestamp default CURRENT_TIMESTAMP not null COMMENT '建立时间',
  updated_time  timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP COMMENT '修改时间'
) COMMENT '测试结果表';

create table test_case
(
  id           int auto_increment primary key COMMENT '测试案例ID',
  query        varchar(255)                        null COMMENT '测试案例内容',
  target       varchar(1024)                       null COMMENT '测试案例预期结果',
  intent       varchar(255)                        null COMMENT '测试案例预期意图',
  dataset      varchar(64)                         null COMMENT '测试集ID',
  appid        varchar(64)                         null COMMENT '机器人ID',
  created_time  timestamp default CURRENT_TIMESTAMP not null COMMENT '建立时间',
  updated_time  timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP COMMENT '修改时间'
) COMMENT '测试集表';

create table test_task
(
  id           int auto_increment primary key COMMENT '测试任务自增ID',
  dataset      varchar(64)                         not null COMMENT '测试集ID',
  task_id      varchar(64)                         not null COMMENT '测试任务ID',
  result       varchar(255)                        null COMMENT '测试集结果',
  appid        varchar(64)                         null COMMENT '机器人ID',
  created_time  timestamp default CURRENT_TIMESTAMP not null COMMENT '建立时间',
  updated_time  timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP COMMENT '修改时间'
) COMMENT '测试结果详情表';

create table type
(
  id           int auto_increment primary key COMMENT '类型自增ID',
  name         varchar(64)                         not null COMMENT '类型名称',
  type_id      int                                 not null COMMENT '类型ID',
  appid        varchar(64)                         null COMMENT '机器人ID',
  created_time  timestamp default CURRENT_TIMESTAMP not null COMMENT '建立时间',
  updated_time  timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP COMMENT '修改时间'
) COMMENT 'KG数据类型表';

#### 初始化数据 ######################

INSERT INTO xeon_kg.type (name, type_id, appid) VALUES ('entity', 1, '');
INSERT INTO xeon_kg.type (name, type_id, appid) VALUES ('float', 3, '');
INSERT INTO xeon_kg.type (name, type_id, appid) VALUES ('string', 2, '');
INSERT INTO xeon_kg.type (name, type_id, appid) VALUES ('none', 6, '');
INSERT INTO xeon_kg.type (name, type_id, appid) VALUES ('anonymousEntity', 4, '');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (2, 'eee和eee的ppp一样吗', 'same', null, '2018-09-17 23:27:06');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (3, 'eee和eee那个ppp高', 'rank', null, '2018-09-17 23:27:06');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (4, 'eee和eee那个ppp低', 'rank', null, '2018-09-17 23:27:06');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (5, 'eee和eee那个ppp大', 'rank', null, '2018-09-17 23:27:06');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (6, 'eee和eee那个ppp小', 'rank', null, '2018-09-17 23:27:06');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (7, 'eee和eee的ppp相同吗', 'same', null, '2018-09-17 23:28:36');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (8, '哪些产品有ppp', 'enum', null, '2018-09-18 13:02:37');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (9, '有ppp的有哪些', 'enum', null, '2018-09-18 13:03:07');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (10, 'eee与eee的ppp相同吗', 'same', null, '2018-09-18 14:52:40');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (11, 'eee和eee的ppp谁高', 'rank', null, '2018-09-18 15:17:18');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (12, 'eee和eee的ppp谁大', 'rank', null, '2018-09-18 15:17:18');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (13, 'eee和eee的ppp谁晚', 'rank', null, '2018-09-20 19:44:10');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (14, 'eee和eee的ppp谁早', 'rank', null, '2018-09-20 19:44:10');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (15, 'eee和eee的ppp那个比较早', 'rank', null, '2018-09-20 19:44:10');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (16, 'eee和eee的ppp那个比较晚', 'rank', null, '2018-09-20 19:44:10');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (17, 'eee和eee的ppp', 'NoneIntent', null, '2018-09-26 22:40:15');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (18, 'eee与eee的ppp', 'NoneIntent', null, '2018-09-26 22:42:01');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (19, 'eee和eeeppp', 'NoneIntent', null, '2018-09-26 22:42:01');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (20, 'ppp', 'NoneIntent', null, '2018-10-26 03:01:04');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (21, 'eee的ppp是vvv吗', 'same', null, '2018-11-11 22:05:39');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (22, 'eee是eee吗', 'same', null, '2018-11-11 22:06:13');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (23, 'eeepppeee吗', 'same', null, '2018-11-12 00:21:48');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (24, 'eeepppvvv', 'same', null, '2018-11-12 23:08:06');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (25, 'eee的ppp和eee谁早', 'rank', null, '2018-11-17 02:16:52');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (26, 'eee的ppp和eee晚', 'rank', null, '2018-11-17 02:16:52');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (27, 'eee的ppp和eee谁高', 'rank', null, '2018-11-17 02:16:52');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (28, 'eee的ppp和eee低', 'rank', null, '2018-11-17 02:16:52');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (29, 'eee的ppp和eee大', 'rank', null, '2018-11-17 02:16:52');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (30, 'eee的ppp和eee谁小', 'rank', null, '2018-11-17 02:16:52');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (31, 'eee的ppp与eee的相同吗', 'same', null, '2018-11-17 02:17:28');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (32, 'eee的ppp比eee的小吗', 'rank', null, '2018-11-17 02:19:36');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (33, 'eee的ppp比eee的大吧', 'rank', null, '2018-11-17 02:19:36');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (34, 'eee与eee有什么相同点', 'same', null, '2018-11-17 05:24:54');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (35, 'eee', 'NoneIntent', null, '2018-11-19 19:22:09');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (36, '咨询下eee', 'enum', null, '2018-11-19 19:22:09');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (37, 'eee哪些eeeppp', 'NoneIntent', null, '2018-11-20 05:53:58');
INSERT INTO xeon_kg.intent_corpus (id, data, intent, appid, created_time) VALUES (38, 'eee哪些eee有ppp', 'NoneIntent', null, '2018-11-20 05:56:55');

INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (2, null, 'eee和eee的ppp一样吗', 'same', '0', '2018-09-17 23:27:06');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (3, null, 'eee和eee那个ppp高', 'rank', '0', '2018-09-17 23:27:06');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (4, null, 'eee和eee那个ppp低', 'rank', '0', '2018-09-17 23:27:06');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (5, null, 'eee和eee那个ppp大', 'rank', '0', '2018-09-17 23:27:06');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (6, null, 'eee和eee那个ppp小', 'rank', '0', '2018-09-17 23:27:06');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (7, null, 'eee和eee的ppp相同吗', 'same', '0', '2018-09-17 23:28:36');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (8, null, '哪些产品有ppp', 'enum', '0', '2018-09-18 13:02:37');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (9, null, '有ppp的有哪些', 'enum', '0', '2018-09-18 13:03:07');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (10, null, 'eee与eee的ppp相同吗', 'same', '0', '2018-09-18 14:52:40');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (11, null, 'eee和eee的ppp谁高', 'rank', '0', '2018-09-18 15:17:18');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (12, null, 'eee和eee的ppp谁大', 'rank', '0', '2018-09-18 15:17:18');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (13, null, 'eee和eee的ppp谁晚', 'rank', '0', '2018-09-20 19:44:10');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (14, null, 'eee和eee的ppp谁早', 'rank', '0', '2018-09-20 19:44:10');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (15, null, 'eee和eee的ppp那个比较早', 'rank', '0', '2018-09-20 19:44:10');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (16, null, 'eee和eee的ppp那个比较晚', 'rank', '0', '2018-09-20 19:44:10');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (17, null, 'eee和eee的ppp', 'NoneIntent', '0', '2018-09-26 22:40:15');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (18, null, 'eee与eee的ppp', 'NoneIntent', '0', '2018-09-26 22:42:01');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (19, null, 'eee和eeeppp', 'NoneIntent', '0', '2018-09-26 22:42:01');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (20, null, 'ppp', 'NoneIntent', '0', '2018-10-26 03:01:04');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (21, null, 'eee的ppp是vvv吗', 'same', '0', '2018-11-11 22:05:39');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (22, null, 'eee是eee吗', 'same', '0', '2018-11-11 22:06:13');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (23, null, 'eeepppeee吗', 'same', '0', '2018-11-12 00:21:48');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (24, null, 'eeepppvvv', 'same', '0', '2018-11-12 23:08:06');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (25, null, 'eee的ppp和eee谁早', 'rank', '0', '2018-11-17 02:16:52');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (26, null, 'eee的ppp和eee晚', 'rank', '0', '2018-11-17 02:16:52');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (27, null, 'eee的ppp和eee谁高', 'rank', '0', '2018-11-17 02:16:52');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (28, null, 'eee的ppp和eee低', 'rank', '0', '2018-11-17 02:16:52');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (29, null, 'eee的ppp和eee大', 'rank', '0', '2018-11-17 02:16:52');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (30, null, 'eee的ppp和eee谁小', 'rank', '0', '2018-11-17 02:16:52');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (31, null, 'eee的ppp与eee的相同吗', 'same', '0', '2018-11-17 02:17:28');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (32, null, 'eee的ppp比eee的小吗', 'rank', '0', '2018-11-17 02:19:36');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (33, null, 'eee的ppp比eee的大吧', 'rank', '0', '2018-11-17 02:19:36');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (34, null, 'eee与eee有什么相同点', 'same', '0', '2018-11-17 05:24:54');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (35, null, 'eee', 'NoneIntent', '0', '2018-11-19 19:22:09');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (36, null, '咨询下eee', 'enum', '0', '2018-11-19 19:22:09');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (37, null, 'eee哪些eeeppp', 'NoneIntent', '0', '2018-11-20 05:53:58');
INSERT INTO xeon_kg.intent_corpus_sandbox (id, appid, data, intent, sync_flag, created_time) VALUES (38, null, 'eee哪些eee有ppp', 'NoneIntent', '0', '2018-11-20 05:56:55');
COMMIT;


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back


