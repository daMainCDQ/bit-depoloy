-- +migrate Up
INSERT INTO `modules` (`code`, `name`, `enterprise`, `cmd_list`, `description`, `status`, `created_time`)
VALUES
  ('customized_skill', '', NULL, 'intent_view,intent_edit,parser_view,parser_edit', '', 1, CURRENT_TIMESTAMP());

INSERT INTO `modules_cmds` (`id`, `parent_id`, `parent_cmd`, `code`, `cmd`, `sort`, `position`, `icon`, `route`, `is_link`, `is_show`) VALUES
(131, 124, '', 'customized_skill', 'intent_view', 3, 'body_left', '', 'skill-list', 1, 1),
(132, 124, 'customized_skill_intent_view', 'customized_skill', 'intent_edit', 4, 'body_left', '', '', 0, 0),
(133, 124, '', 'customized_skill', 'parser_view', 5, 'body_left', '', 'analysis', 0, 0),
(134, 124, 'customized_skill_parser_view', 'customized_skill', 'parser_edit', 6, 'body_left', '', '', 0, 0);

update `modules_cmds` set `sort` = 1, `parent_id` = 124 where `id` = 47;
update `modules_cmds` set `sort` = 2, `parent_id` = 124 where `id` = 48;

DELETE FROM `modules_cmds` WHERE id = 125;
-- +migrate Down