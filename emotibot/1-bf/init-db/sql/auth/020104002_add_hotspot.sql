-- +migrate Up
INSERT INTO `modules` (`code`,`name`,`enterprise`,`cmd_list`,`description`,`status`,`created_time`) VALUES ('hotspot_issues', '', NULL, 'view,edit,delete', '', '1', '2020-07-06 15:50:59');

INSERT INTO `auth`.`modules_cmds` (`id`, `parent_id`, `parent_cmd`, `code`, `cmd`, `sort`, `position`, `icon`, `route`, `is_link`, `is_show`, `create_time`) VALUES ('150', '17', '', 'hotspot_issues', 'view', '13', 'body_left', '', 'hotspot_issues', '0', '1', '2020-07-06 15:50:59');
INSERT INTO `auth`.`modules_cmds` (`id`, `parent_id`, `parent_cmd`, `code`, `cmd`, `sort`, `position`, `icon`, `route`, `is_link`, `is_show`, `create_time`) VALUES ('151', '17', 'hotspot_issues_view', 'hotspot_issues', 'edit', '14', 'body_left', '', '', '0', '0', '2020-07-06 15:50:59');
INSERT INTO `auth`.`modules_cmds` (`id`, `parent_id`, `parent_cmd`, `code`, `cmd`, `sort`, `position`, `icon`, `route`, `is_link`, `is_show`, `create_time`) VALUES ('152', '17', 'hotspot_issues_view', 'hotspot_issues', 'delete', '15', 'body_left', '', '', '0', '0', '2020-07-06 15:50:59');

-- +migrate Down
