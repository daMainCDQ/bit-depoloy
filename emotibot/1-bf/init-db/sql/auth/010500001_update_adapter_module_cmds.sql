-- +migrate Up

INSERT INTO `modules` (`code`, `name`, `enterprise`, `cmd_list`, `description`, `status`) VALUES
('api_adapter', '', NULL, 'view', '', 1),
('api_private', '', NULL, 'view', '', 1);

INSERT INTO `modules_cmds` (`id`, `parent_id`, `parent_cmd`, `code`, `cmd`, `sort`, `position`, `icon`, `route`, `is_link`, `is_show`) VALUES
(119, 0, '', 'api_adapter', '', 11, 'body_left', 'menu_adapter', '', 0, 1),
(120, 119, '', 'api_adapter', 'view', 1, 'body_left', '', 'api-adapter', 0, 1),
(121, 119, '', 'api_private', 'view', 2, 'body_left', '', 'api-private', 0, 1),
(122, 0, '', 'api_private', '', 0, 'body_left', '', '', 0, 0);

-- +migrate Down
DELETE FROM `modules` WHERE `enterprise` is null and `code` in ('api_adapter', 'api_private');
DELETE FROM `modules_cmds` WHERE `id` in (119, 120, 121, 122);
