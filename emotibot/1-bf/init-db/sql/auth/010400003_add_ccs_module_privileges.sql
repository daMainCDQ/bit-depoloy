-- +migrate Up
INSERT INTO `modules` (`code`, `name`, `enterprise`, `cmd_list`, `description`, `status`, `created_time`)
VALUES
  ('ccs_config', '', NULL, 'view,edit,export,import', '', 1, CURRENT_TIMESTAMP()),
    ('ccs_config', '', 'bb3e3925f0ad11e7bd860242ac120003', 'view,edit,export,import', '', 1, CURRENT_TIMESTAMP());
-- +migrate Down
