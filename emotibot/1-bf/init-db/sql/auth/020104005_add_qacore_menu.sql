-- +migrate Up
INSERT INTO `modules` (`code`, `cmd_list`, `status`, `enterprise`) VALUES ('qacore', 'view', '1',  NULL);

INSERT INTO `modules_cmds` (`id`, `parent_id`, `parent_cmd`, `code`, `sort`, `position`, `is_link`, `is_show`) VALUES ('310', '0', '', 'qacore', '1', 'body_left', '1', '0');

INSERT INTO `modules_cmds` (`id`, `parent_id`, `parent_cmd`, `code`, `cmd`, `sort`, `position`, `route`, `is_link`, `is_show`) VALUES ('311', '240', 'mlp_view', 'qacore',  'view', '5', 'body_left', 'qacore', '1', '0');

-- +migrate Down
