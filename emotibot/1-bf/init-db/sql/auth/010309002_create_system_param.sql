-- +migrate Up
CREATE TABLE `system_param` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '定制系统参数id',
  `enterprise_id` char(32) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '企业id',
  `name` varchar(255) CHARACTER SET utf8mb4 NOT NULL COMMENT '参数名字',
  `value` text CHARACTER SET utf8mb4 NOT NULL COMMENT '参数值',
  PRIMARY KEY (`id`,`enterprise_id`,`name`) USING BTREE,
  KEY `enterprise_id` (`enterprise_id`) USING BTREE COMMENT '企业id索引',
  KEY `name` (`name`) USING BTREE COMMENT '参数名字索引'
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

-- +migrate Down

