-- +migrate Up

insert ignore into `modules_cmds` (`id`,`parent_id`,`parent_cmd`,`code`,`cmd`,`sort`,`position`,`icon`,`route`,`is_link`,`is_show`)
values (223,126,'','robot_chat','test_view',5,'body_left','','chat_test',1,1);
update modules_cmds set is_show = 0 where id = 127 and cmd = 'qa_view';

-- +migrate Down
update modules_cmds set is_show = 1 where id = 127 and cmd = 'qa_view';
delete from modules_cmds where id = 223 and cmd = 'test_view';
