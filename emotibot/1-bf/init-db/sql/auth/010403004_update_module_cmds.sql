-- +migrate Up

UPDATE `modules` SET `description` = '', `cmd_list` = 'view,edit,create,delete' WHERE `code` = 'material_library';
UPDATE `privileges` SET `cmd_list` = 'view,edit,create,delete'
WHERE `module` IN (
  SELECT `id`
  FROM `modules`
  WHERE `code` = "material_library"
);

-- +migrate Down

UPDATE `modules` SET `description` = '', `cmd_list` = 'view' WHERE `code` = 'material_library';
UPDATE `privileges` SET `cmd_list` = 'view'
WHERE `module` IN (
  SELECT `id`
  FROM `modules`
  WHERE `code` = "material_library"
);
