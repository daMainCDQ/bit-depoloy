-- +migrate Up
INSERT INTO `modules` (`code`, `name`, `enterprise`, `cmd_list`, `description`, `status`, `created_time`)
VALUES
  ('ner_factory', '', NULL, 'view,edit,export,import', '', 1, CURRENT_TIMESTAMP()),
    ('ner_factory', '', 'bb3e3925f0ad11e7bd860242ac120003', 'view,edit,export,import', '', 1, CURRENT_TIMESTAMP());
-- +migrate Down
