import os
import sys
import time
import pymysql

mysql_host = os.environ.get('INIT_MYSQL_HOST', '172.16.102.135')
mysql_port = int(os.environ.get('INIT_MYSQL_PORT', '3306'))
mysql_user = os.environ.get('INIT_MYSQL_USER', 'root')
mysql_password = os.environ.get('INIT_MYSQL_PASSWORD', 'password')
mysql_db = 'auth'


def handler_all():
    db = pymysql.connect(host=mysql_host, port=mysql_port, user=mysql_user, passwd=mysql_password, db=mysql_db)
    cursor = db.cursor()

    try:
        cursor.execute('select distinct enterprise from modules where enterprise is not NULL')
        exist_ents = cursor.fetchall()
        print(exist_ents)

        for ent in exist_ents:
            sql = 'select ' \
                  '(case when ee.`code` is NULL then en.`code` else ee.`code` end) as `code` ,' \
                  '\'\' as `name`, ' \
                  '(case when ee.`enterprise` is NULL then en.`enterprise` else ee.`enterprise` end) as `code` ,' \
                  '(case when ee.`cmd_list` is NULL then en.`cmd_list` else ee.`cmd_list` end) as `cmd_list`,' \
                  '(case when ee.`description` !=\'\' then ee.`description` else \'\' end) as `description`,' \
                  '(case when ee.`status` is NULL then en.`status` else ee.`status` end) as `status`' \
                  'from (' \
                  'select * from modules where enterprise is NULL and status = 1' \
                  ') as en left join (' \
                  'select * from modules where enterprise is not NULL and enterprise = %s ' \
                  ') as ee on en.code = ee.code' \
                  ''

            cursor.execute(sql, ent[0])
            exist_modules = cursor.fetchall()

            sql = 'insert into modules (code, name, enterprise, cmd_list, description, status) values '
            need_update = False
            for m in exist_modules:
                ml = list(m)

                if ml[2] is None:
                    ml[2] = ent[0]
                    if ent[0] == "bb3e3925f0ad11e7bd860242ac120003":
                        ml[5] = 1
                    else:
                        ml[5] = 0
                    need_update = True

                    sql += '('
                    for val in ml:
                        if isinstance(val, int):
                            sql += str(val) + ','
                        else:
                            sql += '\'' + str(val) + '\','
                    sql = sql[:len(sql)-1]
                    sql += '),'

            sql = sql[:len(sql) - 1]

            if need_update:
                print(sql)
                res = cursor.execute(sql)
                print(res)

        db.commit()
        db.close()

    except Exception as e:
        print(e)

    return


if __name__ == '__main__':
    try:
        start_time = time.time()
        handler_all()
        print('handler all cost time:%s' % (time.time() - start_time))
    except Exception as err:
        print(err)
        sys.exit()
