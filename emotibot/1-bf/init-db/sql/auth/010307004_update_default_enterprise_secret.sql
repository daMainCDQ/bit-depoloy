-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
UPDATE `enterprises` SET `secret` = concat(md5(concat(now(), `uuid`)), sha1(rand()));

-- +migrate Down
UPDATE `enterprises` SET `secret` = "";
