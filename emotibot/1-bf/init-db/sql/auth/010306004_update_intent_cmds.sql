-- +migrate Up
UPDATE `modules` SET `cmd_list` = 'view,edit,export,import' WHERE `code` = 'intent_manage';

-- +migrate Down
UPDATE `modules` SET `cmd_list` = 'view,export,import' WHERE `code` = 'intent_manage';

