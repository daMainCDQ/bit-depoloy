-- +migrate Up
INSERT INTO `modules` (`code`, `name`, `enterprise`, `cmd_list`, `description`, `status`, `created_time`) VALUES
('ai_tools', '', NULL, 'view,edit,chat', 'ai工具', 1, '2019-07-17 09:04:52'),
('material_library', '', NULL, 'view', '素材库', 1, '2018-07-13 07:52:57');

ALTER TABLE `modules_cmds` ADD `position` VARCHAR(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'body_left' COMMENT '页面位置' AFTER `sort`;

INSERT INTO `modules_cmds` (`id`, `parent_id`, `parent_cmd`, `code`, `cmd`, `sort`, `position`, `icon`, `route`, `is_link`, `is_show`) VALUES
(95, 0, '', 'ai_tools', '', 1, 'nav_left_3', '', '', 0, 1),
(96, 95, '', 'ai_tools', 'view', 1, 'nav_left_3', '', 'chat-tools', 0, 1),
(97, 0, '', 'material_library', '', 6, 'body_left', 'menu_material', '', 0, 1),
(98, 97, '', 'material_library', 'view', 1, 'body_left', '', 'material', 0, 1);

update `modules_cmds` set `sort` = 7 where `id` = 18;
update `modules_cmds` set `sort` = 8 where `id` = 90;
update `modules_cmds` set `sort` = 9 where `id` = 6;
update `modules_cmds` set `sort` = 10 where `id` = 17;



-- +migrate Down
DELETE FROM `modules` WHERE `enterprise` is null and `code` in ('ai_tools', 'material_library');

ALTER TABLE `modules_cmds` DROP `position`;

DELETE FROM `modules_cmds` WHERE `id` in (95, 96, 97, 98);

update `modules_cmds` set `sort` = 6 where `id` = 18;
update `modules_cmds` set `sort` = 7 where `id` = 90;
update `modules_cmds` set `sort` = 8 where `id` = 6;
update `modules_cmds` set `sort` = 9 where `id` = 17;
update `modules_cmds` set `parent_id` = 22, `sort` = 0, `position` = 'body_left', `route` = 'ccs-manage', `is_link` = 0, `is_show` = 0 where `id` = 86;
