-- +migrate Up

ALTER TABLE `privileges` CHANGE `cmd_list` `cmd_list` VARCHAR(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '拥有模组的哪些权限';

-- +migrate Down
