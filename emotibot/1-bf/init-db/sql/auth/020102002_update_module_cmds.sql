-- +migrate Up
UPDATE `modules` SET `status` = 1 WHERE `code` = 'mlp';

UPDATE `modules_cmds` SET `is_show` = 1, `sort` = 14 WHERE `modules_cmds`.`code` = 'mlp' and `modules_cmds`.`parent_id` = 0;
-- +migrate Down
