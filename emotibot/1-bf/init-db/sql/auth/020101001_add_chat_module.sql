-- +migrate Up
update modules_cmds set is_link = 1, is_show = 1 where id in (127, 128);
-- +migrate Down
update modules_cmds set is_link = 0, is_show = 0 where id in (127, 128);
