-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `apps` ADD `secret` char(72) NOT NULL DEFAULT '' COMMENT '机器人所属 secret key' AFTER `enterprise`;

-- +migrate Down
ALTER TABLE `apps` DROP COLUMN `secret`;

