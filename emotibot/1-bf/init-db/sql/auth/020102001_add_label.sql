-- +migrate Up

INSERT INTO `modules` (`code`, `name`, `enterprise`, `cmd_list`, `description`, `status`, `created_time`)
VALUES ('personas', '', null, 'view', '', 1, CURRENT_TIMESTAMP());

INSERT INTO `modules_cmds` (`id`, `parent_id`, `parent_cmd`, `code`, `cmd`, `sort`, `position`, `icon`, `route`, `is_link`, `is_show`) VALUES
(301, 0, '', 'personas', '', 5, 'body_left', 'icon-client-manage', '', 0, 1),
(302, 301, '', 'personas', 'view', 1, 'body_left', '', 'personas_tag', 1, 1);

-- +migrate Down
DELETE FROM `modules` WHERE `enterprise` is null and `code` in ('personas');
DELETE FROM `modules_cmds` WHERE `id` in (301, 302);

