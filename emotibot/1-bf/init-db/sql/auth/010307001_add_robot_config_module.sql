-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
INSERT INTO `modules` (`code`, `name`, `enterprise`, `cmd_list`, `description`, `status`)
VALUES
	('robot_config', '', NULL, 'view,edit', '', 1);

-- +migrate Down
DELETE FROM modules WHERE code = 'robot_config';
