-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

--
-- Table structure for table `ccs_bot`
--

UPDATE modules set cmd_list = 'view,edit,export,import,create,delete,cm,tm'  where code = 'ccs_config';
DELETE FROM modules_cmds WHERE code='ccs_config' and cmd = 'is';

-- +migrate Down
