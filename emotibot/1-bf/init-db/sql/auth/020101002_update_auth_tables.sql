-- +migrate Up

ALTER TABLE `enterprises` ADD `level` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '组织层级' AFTER `uuid`;
ALTER TABLE `enterprises` ADD `parent_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0' COMMENT '上级组织序号' AFTER `uuid`;

ALTER TABLE `ent_prods` MODIFY `ent_id` BIGINT(20) NOT NULL COMMENT '企业序号';
ALTER TABLE `ent_prods` MODIFY `prod_id` BIGINT(20) NOT NULL COMMENT '企业序号';

ALTER TABLE `role_prods` MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色产品序号';
ALTER TABLE `role_prods` MODIFY `role_id` bigint(20) NOT NULL COMMENT '角色序号';
ALTER TABLE `role_prods` MODIFY `prod_id` bigint(20) NOT NULL COMMENT '产品序号';
ALTER TABLE `role_prods` ADD `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '状态';

ALTER TABLE `users` ADD `organization` BIGINT(21) UNSIGNED NOT NULL DEFAULT 0 COMMENT '直属组织';

CREATE TABLE `role_users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色用户序号',
  `role_id` bigint(20) NOT NULL COMMENT '角色序号',
  `user_id` bigint(20) NOT NULL COMMENT '用户序号',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色用户关系表';

CREATE TABLE `user_ent_rel` (
`id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `ent_id` bigint(20) NOT NULL COMMENT '组织序号',
  `user_id` bigint(20) NOT NULL COMMENT '用户序号',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '状态',
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='关联组织表';

CREATE TABLE `user_ent_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '组织角色用户序号',
  `ent_id` bigint(20) NOT NULL COMMENT '组织序号',
  `role_id` bigint(20) NOT NULL COMMENT '角色序号',
  `user_id` bigint(20) NOT NULL COMMENT '用户序号',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '状态',
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='组织角色用户关系表';

ALTER TABLE `user_privileges` ADD `org_id` bigint(20)	NOT NULL DEFAULT '0' COMMENT '组织序号';

-- +migrate Down
