import pymysql
import os

mysql_host = os.environ.get('INIT_MYSQL_HOST', '172.16.102.130')
mysql_port = int(os.environ.get('INIT_MYSQL_PORT', '3306'))
mysql_user = os.environ.get('INIT_MYSQL_USER', 'root')
mysql_password = os.environ.get('INIT_MYSQL_PASSWORD', 'password')
mysql_db = 'auth'

def handler():
    db = pymysql.connect(host=mysql_host, port=mysql_port, user=mysql_user, passwd=mysql_password, db=mysql_db)

    sql = 'select u.id, e.id ' \
          'from users as u ' \
          'left join enterprises as e on u.enterprise = e.uuid ' \
          'where u.type != 0 and u.organization = 0 and u.enterprise is not null and e.parent_id = 0'

    cursor = db.cursor()
    cursor.execute(sql)
    user_ent_ids = cursor.fetchall()

    if len(user_ent_ids) == 0:
        return

    sql = "SET @@session.sql_mode= '';"
    res = cursor.execute(sql)
    print(res)

    print(user_ent_ids)
    for i in user_ent_ids:
        sql = 'update users set organization = %s where id = %s'
        res = cursor.execute(sql, (i[1], i[0]))
        print(sql, i[0], i[1])
        print(res)

    res = db.commit()
    print(res)

    return

if __name__ == '__main__':
    handler()
