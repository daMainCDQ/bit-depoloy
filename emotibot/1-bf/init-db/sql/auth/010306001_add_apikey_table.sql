-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE `api_key` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `appid` char(36) NOT NULL COMMENT '机器人appid',
  `api_key` char(255) NOT NULL DEFAULT '' COMMENT 'api_key',
  `expire_time` bigint(20) NOT NULL COMMENT '失效时间',
  `create_time` bigint(20) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `token_token` (`api_key`),
  KEY `token_expire_time` (`expire_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='API Key';

-- +migrate Down
DROP TABLE `api_key`;

