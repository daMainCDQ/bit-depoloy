-- +migrate Up
ALTER TABLE `modules` CHANGE `cmd_list` `cmd_list` CHAR(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '该模组所有的操作列表 ex: view, create, edit, export, import, delete, 该栏位会影响前端权限群组显示';
INSERT INTO `modules` (`id`, `code`, `name`, `enterprise`, `cmd_list`, `description`, `status`) VALUES (NULL, 'ssm3', '', NULL, 'view,export,import,create-sq,del-sq,edit-sq,edit-answer,edit-lq,train,test,publish,edit-label', '', '1');
INSERT INTO `modules` (`id`, `code`, `name`, `enterprise`, `cmd_list`, `description`, `status`) VALUES (NULL, 'qa_records', '', NULL, 'view', '', '1');
ALTER TABLE `privileges` CHANGE `cmd_list` `cmd_list` CHAR(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '拥有模组的哪些权限';
-- +migrate Down
