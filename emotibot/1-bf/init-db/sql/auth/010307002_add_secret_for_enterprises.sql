-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `enterprises` ADD `secret` char(72) NOT NULL DEFAULT '' COMMENT '企业所属 secret key' AFTER `name`;

-- +migrate Down
ALTER TABLE `enterprises` DROP COLUMN `secret`;

