import os
import sys
import time
import pymysql

mysql_host = os.environ.get('INIT_MYSQL_HOST','172.16.102.121')
mysql_port = int(os.environ.get('INIT_MYSQL_PORT','3306'))
mysql_user = os.environ.get('INIT_MYSQL_USER','root')
mysql_password = os.environ.get('INIT_MYSQL_PASSWORD','password')


def handler_all():
    db = pymysql.connect(host=mysql_host, port=mysql_port, user=mysql_user, passwd=mysql_password, db='auth')
    cursor = db.cursor()
    try:
        cursor.execute('select org_id from annotation.enterprise')
        exist_ent = cursor.fetchall()
        exist_ent_set = set()
        for eni in exist_ent:
            exist_ent_set.add(eni[0])
        print('exist ent :%s'%(len(exist_ent_set)))
        print('start migration ent')
        cursor.execute('select id,uuid,parent_id,name,status from auth.enterprises')
        ret = cursor.fetchall()
        for ent in ret:
            if ent[0] in exist_ent_set:continue
            print(ent[3])
            cursor.execute('insert into annotation.enterprise(org_id,uuid,name,parent_id,status) values (%s,%s,%s,%s,%s)',(ent[0],ent[1],ent[3],ent[2],ent[4]))

        cursor.execute('select user_id from annotation.user')
        exist_user = cursor.fetchall()
        exist_user_set = set()
        for ui in exist_user:exist_user_set.add(ui[0])

        print('exist user :%s' % (len(exist_user_set)))
        print('start migration user')
        cursor.execute('select id,display_name,user_name,organization from auth.users')
        auth_users = cursor.fetchall()
        for user in auth_users:
            if user[0] in exist_user_set:continue
            print(user[1])
            cursor.execute('insert into annotation.user(user_id,user_name) values (%s,%s)',(user[0],user[1]))
            cursor.execute('insert into annotation.user_role_ent(user_id,ent_id,role_id) values (%s,%s,%s)',(user[0],user[3],8))
        db.commit()
        db.close()
    except Exception as e:
        print(e)
    return



if __name__ == '__main__':
    try:
        start_time = time.time()
        handler_all()
        print('handler all cost time:%s' % (time.time() - start_time))
    except Exception as err:
        print(err)
        sys.exit()