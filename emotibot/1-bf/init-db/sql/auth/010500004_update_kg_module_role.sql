-- +migrate Up

-- update `modules` set `status` = 0 where `code` = 'domain_kg' and `enterprise` is NULL;
--
-- INSERT INTO `modules` (`code`, `name`, `enterprise`, `cmd_list`, `description`, `status`) VALUES
-- ('kg_data_manage', '', NULL, 'view,edit,delete,import', '知识图谱-数据管理', 1),
-- ('kg_audit_manage', '', NULL, 'view,edit,delete,import', '知识图谱-变更审核', 1),
-- ('kg_test_report', '', NULL, 'view,edit,delete,import', '知识图谱-测试报告', 1);

-- update `modules_cmds` set is_show = 0 where `code` = 'domain_kg';

update `modules` set `cmd_list` = 'view,edit,export,import' where `id` = 42;
update `modules` set `cmd_list` = 'view,edit,test,import' where `id` = 43;

update `modules_cmds` set cmd = 'export' where `id` = 104;
update `modules_cmds` set cmd = 'test' where `id` = 108;

delete from `modules_cmds` where `id` in (113);

-- INSERT INTO `modules_cmds` (`id`, `parent_id`, `parent_cmd`, `code`, `cmd`, `sort`, `position`, `icon`, `route`, `is_link`, `is_show`) VALUES
-- -- (99, 0, '', 'kg_data_manage', '', 7, 'body_left', 'menu_te', '', 0, 1),
-- -- (100, 0, '', 'kg_audit_manage', '', 0, 'body_left', '', '', 0, 0),
-- -- (101, 0, '', 'kg_test_report', '', 0, 'body_left', '', '', 0, 0),
-- -- (102, 99, '', 'kg_data_manage', 'view', 1, 'body_left', '', 'data-manage', 0, 1),
-- -- (103, 99, 'kg_data_manage_view', 'kg_data_manage', 'edit', 2, 'body_left', '', '', 0, 0),
-- -- (104, 99, 'kg_data_manage_view', 'kg_data_manage', 'delete', 3, 'body_left', '', '', 0, 0),
-- -- (105, 99, 'kg_data_manage_view', 'kg_data_manage', 'import', 4, 'body_left', '', '', 0, 0),
-- -- (106, 99, '', 'kg_test_report', 'view', 5, 'body_left', '', 'test-report', 0, 1),
-- -- (107, 99, 'kg_test_report_view', 'kg_test_report', 'edit', 6, 'body_left', '', '', 0, 0),
-- -- (108, 99, 'kg_test_report_view', 'kg_test_report', 'delete', 7, 'body_left', '', '', 0, 0),
-- -- (109, 99, 'kg_test_report_view', 'kg_test_report', 'import', 8, 'body_left', '', '', 0, 0),
-- -- (110, 99, '', 'kg_audit_manage', 'view', 9, 'body_left', '', 'audit-manage', 0, 1),
-- -- (111, 99, 'kg_audit_manage_view', 'kg_audit_manage', 'edit', 10, 'body_left', '', '', 0, 0),
-- -- (112, 99, 'kg_audit_manage_view', 'kg_audit_manage', 'delete', 11, 'body_left', '', '', 0, 0),
-- -- (114, 99, 'kg_data_manage_view', 'kg_data_manage', 'export', 13, 'body_left', '', '', 0, 0),
-- (115, 99, 'kg_data_manage_view', 'kg_test_report', 'test', 14, 'body_left', '', '', 0, 0);


-- +migrate Down
INSERT INTO `modules_cmds` (`id`, `parent_id`, `parent_cmd`, `code`, `cmd`, `sort`, `position`, `icon`, `route`, `is_link`, `is_show`) VALUES
(113, 99, 'kg_audit_manage_view', 'kg_audit_manage', 'import', 12, 'body_left', '', '', 0, 0);

update `module_cmds` set cmd = 'delete' where id = 104;
update `module_cmds` set cmd = 'delete' where id = 108;
