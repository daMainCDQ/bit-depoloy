-- +migrate Up
update `modules` set `status` = 0 where `code` = 'api_private' and `enterprise` is NULL;

update `modules_cmds` set `is_show` = 0, `sort` = 0 where `id` = 119;
update `modules_cmds` set `is_show` = 0, `sort` = 0 where `id` = 122;
update `modules_cmds` set `is_show` = 0 where `id` = 121;
update `modules_cmds` set `is_show` = 0, `parent_id` = 95, `position` = 'nav_left_3' where `id` = 120;
-- +migrate Down