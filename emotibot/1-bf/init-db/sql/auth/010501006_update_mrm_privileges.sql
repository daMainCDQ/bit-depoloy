-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

--
-- Table structure for table `ccs_bot`
--



UPDATE modules SET  cmd_list = 'view,edit,export,import,create,delete,cm,tm,rms,i' where code = 'multi_robot_manage';

INSERT INTO auth.modules_cmds (id, parent_id, parent_cmd, code, cmd, sort, position, icon, route, is_link, is_show) VALUES
 (230, 211, 'multi_robot_manage_view,multi_robot_manage_edit', 'multi_robot_manage', 'rms', 3, 'body_left', '', 'robot-manage-statistic', 1, 1);

DELETE FROM modules_cmds WHERE code='multi_robot_manage' and cmd in ('im', 'is');
UPDATE modules_cmds SET sort = 1 where code='multi_robot_manage' and cmd ='cm';
UPDATE modules_cmds SET sort = 2 where code='multi_robot_manage' and cmd ='tm';
UPDATE modules_cmds SET sort = 3 where code='multi_robot_manage' and cmd ='rms';
UPDATE modules_cmds SET sort = 4 where code='multi_robot_manage' and cmd ='i';

-- +migrate Down
