-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `users` 
MODIFY COLUMN `user_name` char(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户帐号' AFTER `display_name`;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back