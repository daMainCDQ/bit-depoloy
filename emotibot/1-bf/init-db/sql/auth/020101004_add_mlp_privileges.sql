-- +migrate Up
INSERT INTO `modules` (`code`, `name`, `enterprise`, `cmd_list`, `description`, `status`, `created_time`)
VALUES ('mlp', '', NULL, 'view,edit,export,import,create,delete,pm,am', '', 1, CURRENT_TIMESTAMP()),
  ('mlp', '', 'bb3e3925f0ad11e7bd860242ac120003', 'view,edit,export,import,create,delete,pm,am', '', 1, CURRENT_TIMESTAMP());

INSERT INTO `modules_cmds` (id, parent_id, parent_cmd, code, cmd, sort, position, icon, route, is_link, is_show)
VALUES  (240, 0, '', 'mlp', '', 1, 'body_left', 'icon-dialogue', '', 0, 0),
        (241, 240, '', 'mlp', 'view', 0, 'body_left', '', '', 0, 0),
        (242, 240, '', 'mlp', 'edit', 0, 'body_left', '', '', 0, 0),
        (243, 240, '', 'mlp', 'export', 0, 'body_left', '', '', 0, 0),
        (244, 240, '', 'mlp', 'import', 0, 'body_left', '', '', 0, 0),
        (245, 240, '', 'mlp', 'create', 0, 'body_left', '', '', 0, 0),
        (246, 240, '', 'mlp', 'delete', 0, 'body_left', '', '', 0, 0),
        (247, 240, 'mlp_view,mlp_edit', 'mlp', 'pm', 1, 'body_left', '', 'problem-manage', 1, 1),
        (248, 240, 'mlp_view,mlp_edit', 'mlp', 'am', 2, 'body_left', '', 'algorithm-manage', 1, 1);

-- +migrate Down
DELETE FROM `modules` WHERE `code` = 'mlp';
DELETE FROM `modules_cmds` WHERE `id` in (240, 241, 242, 243, 244, 245, 246, 247, 248);