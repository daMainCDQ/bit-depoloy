-- +migrate Up

INSERT INTO auth.modules_cmds (id, parent_id, parent_cmd, code, cmd, sort, position, icon, route, is_link, is_show, create_time) VALUES (300, 4, '', 'task_engine', 'act_view', 3, 'body_left', '', 'dialogue-act-classifier', 0, 1, '2019-07-03 14:04:04');
UPDATE `modules_cmds` SET `is_show` = 0 WHERE `modules_cmds`.`id` = 130;
UPDATE `modules_cmds` SET `is_show` = 0 WHERE `modules_cmds`.`id` = 123;

-- +migrate Down
