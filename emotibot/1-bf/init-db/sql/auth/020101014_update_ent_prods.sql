-- +migrate Up

insert into ent_prods (ent_id, prod_id)
select id, 1 from enterprises where id > 1 and level = 0;

update user_privileges as up set org_id = (
    select e.id
    from enterprises as e
    left join users as u on u.enterprise = e.uuid
    where e.id is not NULL
    and e.level = 0
    and u.uuid = up.human
);

-- +migrate Down
delete from ent_prods where id > 1;
