-- +migrate Up

update modules set cmd_list='view,edit,export,import,create,delete,pm,am,mb' where code='mlp';

INSERT INTO `modules_cmds` (`id`, `parent_id`, `parent_cmd`, `code`, `cmd`, `sort`, `position`, `icon`, `route`, `is_link`, `is_show`) VALUES
(309, 240, 'mlp_view,mlp_edit', 'mlp', 'mb', 3, 'body_left', '', 'model_library', 1, 1);

-- +migrate Down
DELETE FROM `modules_cmds` WHERE id = 309;
update modules set cmd_list='view,edit,export,import,create,delete,pm,am,mb' where code='mlp';

