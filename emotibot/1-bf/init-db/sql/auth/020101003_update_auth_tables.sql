-- +migrate Up
ALTER TABLE `enterprises` DROP INDEX `uuid`;
CREATE INDEX `idx_uuid` ON `enterprises`(`uuid`);

ALTER TABLE `ent_prods` MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色产品序号';
ALTER TABLE `ent_prods` ADD `status` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '状态' AFTER `prod_id`;

INSERT INTO `ent_prods`(`ent_id`, `prod_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10);


UPDATE `users` SET `organization` = '1' WHERE `user_name` = 'csbotadmin';

INSERT INTO `products` (`id`, `code`, `sort`, `position`, `icon`, `route`, `is_link`, `status`, `create_time`) VALUES ('10', 'asrtts', '1', 'menu_product', 'color-voice-01', 'asrtts', '1', '1', '2020-02-22 13:46:52');

-- +migrate Down
