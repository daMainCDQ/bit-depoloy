-- +migrate Up

UPDATE `modules_cmds` SET `sort` = 0 WHERE `code` = 'wordbank' and `cmd` in ('edit', 'create', 'delete', 'export', 'import');
UPDATE `modules_cmds` SET `sort` = 0, `is_show`=0 WHERE `id` in (44, 112);

-- +migrate Down
