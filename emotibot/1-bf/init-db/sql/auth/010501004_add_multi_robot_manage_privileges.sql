-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

--
-- Table structure for table `ccs_bot`
--

UPDATE `modules` set cmd_list = 'view,edit,export,import,create,delete,cm,is,tm'  where code = 'ccs_config';
update `modules_cmds` set icon='menu_intent', is_show=1 where id=22;
INSERT INTO modules_cmds (id, parent_id, parent_cmd, code, cmd, sort, position, icon, route, is_link, is_show) VALUES
(200, 22, '', 'ccs_config', 'create', 0, 'body_left', '', '', 0, 0)
,(201, 22, '', 'ccs_config', 'delete', 0, 'body_left', '', '', 0, 0)
,(202, 22, 'ccs_config_view,ccs_config_edit', 'ccs_config', 'cm', 1, 'body_left', '', 'ccs_config_management', 1, 1)
,(203, 22, 'ccs_config_view,ccs_config_edit', 'ccs_config', 'is', 6, 'body_left', '', 'ccs_info_share', 1, 1)
,(204, 22, 'ccs_config_view,ccs_config_edit', 'ccs_config', 'tm', 9, 'body_left', '', 'ccs-test', 1, 1);


INSERT INTO `modules` (`code`, `name`, `enterprise`, `cmd_list`, `description`, `status`, `created_time`)
VALUES
('multi_robot_manage', '', NULL, 'view,edit,export,import,create,delete,cm,is,im,tm,i', '', 1, CURRENT_TIMESTAMP()),
('multi_robot_manage', '', 'bb3e3925f0ad11e7bd860242ac120003', 'view,edit,export,import,create,delete,is,im,tm,i', '', 1, CURRENT_TIMESTAMP());

INSERT INTO auth.modules_cmds (id, parent_id, parent_cmd, code, cmd, sort, position, icon, route, is_link, is_show) VALUES
 (211, 0, '', 'multi_robot_manage', '', 1, 'body_left', 'menu_dashboard', '', 0, 0)
,(212, 211, '', 'multi_robot_manage', 'view', 0, 'body_left', '', '', 0, 0)
,(213, 211, '', 'multi_robot_manage', 'edit', 0, 'body_left', '', '', 0, 0)
,(214, 211, '', 'multi_robot_manage', 'export', 0, 'body_left', '', '', 0, 0)
,(215, 211, '', 'multi_robot_manage', 'import', 0, 'body_left', '', '', 0, 0)
,(216, 211, '', 'multi_robot_manage', 'create', 0, 'body_left', '', '', 0, 0)
,(217, 211, '', 'multi_robot_manage', 'delete', 10, 'body_left', '', '', 0, 0)
,(218, 211, 'multi_robot_manage_view,multi_robot_manage_edit', 'multi_robot_manage', 'cm', 1, 'body_left', '', 'robot-manage-config', 1, 1)
,(219, 211, 'multi_robot_manage_view,multi_robot_manage_edit', 'multi_robot_manage', 'is', 2, 'body_left', '', 'robot-manage-info-share', 1, 1)
,(220, 211, 'multi_robot_manage_view,multi_robot_manage_edit', 'multi_robot_manage', 'im', 3, 'body_left', '', 'robot-manage-intent', 1, 1)
,(221, 211, 'multi_robot_manage_view,multi_robot_manage_edit', 'multi_robot_manage', 'tm', 4, 'body_left', '', 'robot-manage-test', 1, 1)
,(222, 211, 'multi_robot_manage_view,multi_robot_manage_edit', 'multi_robot_manage', 'i', 5, 'body_left', '', 'robot-manage-integration', 1, 1);
INSERT INTO auth.products (id, code, sort, position, icon, route, is_link, status) VALUES (9, 'mrm', 1, 'menu_product', 'vca', 'mrm', 1, 1);
