-- +migrate Up

UPDATE `modules` set `description` = '' where `code` = 'kg_data_manage';
UPDATE `modules` set `description` = '' where `code` = 'kg_audit_manage';
UPDATE `modules` set `description` = '' where `code` = 'kg_test_report';

-- +migrate Down

UPDATE `modules` set `description` = '知识图谱-数据管理' where `code` = 'kg_data_manage';
UPDATE `modules` set `description` = '知识图谱-变更审核' where `code` = 'kg_audit_manage';
UPDATE `modules` set `description` = '知识图谱-测试报告' where `code` = 'kg_test_report';

