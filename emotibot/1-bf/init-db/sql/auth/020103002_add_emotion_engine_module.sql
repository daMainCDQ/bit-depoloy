-- +migrate Up

INSERT INTO auth.modules (code, name, enterprise, cmd_list, description, status,
created_time) VALUES ('emotion_manage', '', NULL, 'view,edit,export,import', '', 1, '2020-04-29 16:03:15');

INSERT INTO auth.modules_cmds (id, parent_id, parent_cmd, code, cmd, sort, position, icon, route, is_link, is_show, create_time) VALUES (304, 0, '', 'emotion_manage', '', 6, 'body_left', 'icon-emotion', '', 0, 1, '2020-04-29 16:01:26');
INSERT INTO auth.modules_cmds (id, parent_id, parent_cmd, code, cmd, sort, position, icon, route, is_link, is_show, create_time) VALUES (305, 304, '', 'emotion_manage', 'view', 1, 'body_left', '', 'mood-manage', 1, 1, '2020-04-29 16:01:26');
INSERT INTO auth.modules_cmds (id, parent_id, parent_cmd, code, cmd, sort, position, icon, route, is_link, is_show, create_time) VALUES (306, 304, '', 'emotion_manage', 'edit', 2, 'body_left', '', '', 0, 0, '2020-04-29 16:01:26');
INSERT INTO auth.modules_cmds (id, parent_id, parent_cmd, code, cmd, sort, position, icon, route, is_link, is_show, create_time) VALUES (307, 304, '', 'emotion_manage', 'export', 3, 'body_left', '', '', 0, 0, '2020-04-29 16:01:26');
INSERT INTO auth.modules_cmds (id, parent_id, parent_cmd, code, cmd, sort, position, icon, route, is_link, is_show, create_time) VALUES (308, 304, '', 'emotion_manage', 'import', 4, 'body_left', '', '', 0, 0, '2020-04-29 16:01:26');


-- +migrate Down
DELETE FROM auth.modules WHERE code = 'emotion_manage';
DELETE FROM auth.modules_cmds WHERE id in (304, 305, 306, 307, 308);

