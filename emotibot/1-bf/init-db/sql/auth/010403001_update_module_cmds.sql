-- +migrate Up

UPDATE `modules` SET `description` = '' WHERE `code` = 'ai_tools' and enterprise is null;
UPDATE `modules` SET `description` = '', `cmd_list` = 'view,edit,create,delete' WHERE `code` = 'material_library' and enterprise is null;

INSERT INTO `modules` (`code`, `name`, `enterprise`, `cmd_list`, `description`, `status`) VALUES ('statistic_records', '', NULL, 'view,export', '', '1');

INSERT INTO `modules_cmds` (`id`, `parent_id`, `parent_cmd`, `code`, `cmd`, `sort`, `position`, `icon`, `route`, `is_link`, `is_show`) VALUES
(114, 97, 'material_library_view', 'material_library', 'edit', 2, 'body_left', '', '', 0, 0),
(115, 97, 'material_library_view', 'material_library', 'create', 3, 'body_left', '', '', 0, 0),
(116, 97, 'material_library_view', 'material_library', 'delete', 4, 'body_left', '', '', 0, 0),
(117, '6', '', 'statistic_records', 'view', '5', 'body_left', '', 'statistic-session', '0', '1'),
(118, '6', 'statistic_records_view', 'statistic_records', 'view', '6', 'body_left', '', '', '0', '0');

update `modules_cmds` set `sort` = 7 where `id` = 84;
update `modules_cmds` set `sort` = 8 where `id` = 85;

-- +migrate Down
DELETE FROM `modules` WHERE `enterprise` is null and `code` in ('statistic_records');
DELETE FROM `modules_cmds` WHERE `id` in (114, 115, 116, 117, 118);
