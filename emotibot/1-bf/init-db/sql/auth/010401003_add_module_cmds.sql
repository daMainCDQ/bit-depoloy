-- +migrate Up

UPDATE `modules_cmds` SET `route` = 'statistic-daily' WHERE `modules_cmds`.`id` = 37;
UPDATE `modules_cmds` SET `route` = 'statistic-analysis' WHERE `modules_cmds`.`id` = 39;
UPDATE `modules_cmds` SET `route` = 'statistic-audit', `is_show` = 0 WHERE `modules_cmds`.`id` = 41;
UPDATE `modules_cmds` SET `route` = 'health-check' WHERE `modules_cmds`.`id` = 84;

-- +migrate Down
