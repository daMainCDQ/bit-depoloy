-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `enterprises` ADD `status` TINYINT(1) NOT NULL DEFAULT 1 COMMENT '企业状态 0: disable, 1: active' AFTER `description`;

-- +migrate Down
ALTER TABLE `enterprises` DROP COLUMN `status`;
