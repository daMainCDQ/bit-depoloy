-- +migrate Up
UPDATE `modules_cmds` SET `is_show` = 0 WHERE `code`='wordbank' AND `cmd`='import';

-- +migrate Down
UPDATE `modules_cmds` SET `is_show` = 1 WHERE `code`='wordbank' AND `cmd`='import';
