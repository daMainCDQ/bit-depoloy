-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
INSERT INTO `modules` (`code`, `name`, `enterprise`, `cmd_list`, `description`, `status`) 
VALUES
	('domain_kg', '', NULL, 'view,edit,delete,import', '', 1);


-- +migrate Down
DELETE FROM `modules` WHERE `code` = 'domain_kg';

