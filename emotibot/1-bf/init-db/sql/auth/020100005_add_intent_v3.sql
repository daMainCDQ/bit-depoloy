-- +migrate Up
INSERT INTO `modules` (`code`, `name`, `enterprise`, `cmd_list`, `description`, `status`, `created_time`)
VALUES ('joint_test', '', NULL, 'view,edit,import,export', '', 1, CURRENT_TIMESTAMP());

INSERT INTO `modules_cmds` (`id`, `parent_id`, `parent_cmd`, `code`, `cmd`, `sort`, `position`, `icon`, `route`, `is_link`, `is_show`) VALUES
(231, 13, '', 'intent_manage', 'joint_test_view', 5, 'body_left', '', 'joint-test', 0, 1),
(232, 13, 'joint_test_view', 'intent_manage', 'joint_test_import', 6, 'body_left', '', '', 0, 0),
(233, 13, 'joint_test_view', 'intent_manage', 'joint_test_export', 7, 'body_left', '', '', 0, 0),
(234, 13, 'joint_test_view', 'intent_manage', 'joint_test_edit', 8, 'body_left', '', '', 0, 0);
-- +migrate Down
DELETE FROM `modules` WHERE `enterprise` is null and `code` in ('joint_test');
DELETE FROM `modules_cmds` WHERE `id` in (231, 232, 233, 234);