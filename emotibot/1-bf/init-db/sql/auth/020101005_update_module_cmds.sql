-- +migrate Up

UPDATE `modules` SET `status`=0 WHERE code in ('multi_robot_manage','mlp');

UPDATE `modules_cmds` SET `is_show` = 1 WHERE `modules_cmds`.`id` = 130;

-- +migrate Down
