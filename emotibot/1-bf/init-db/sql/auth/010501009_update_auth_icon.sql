-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

--
-- Table structure for table `ccs_bot`
--
update products set icon = 'color-robot-platform' where code = 'bf';
update products set icon = 'color-phone-robot' where code = 'ccbot';
update products set icon = 'color-quality-inspection' where code = 'qabot';
update products set icon = 'color-sales-assistant' where code = 'sa';
update products set icon = 'color-customer-service-assistant' where code = 'csa';
update products set icon = 'color-aquarius-active' where code = 'aquarius';
update products set icon = 'color-customer-service' where code = 'vca';
update products set icon = 'color-NLU' where code = 'nlu';
update products set icon = 'color-multi-robot' where code = 'mrm';

update modules_cmds set icon = 'icon-ai-learning' where id = 124;
update modules_cmds set icon = 'icon-robot-chat' where id = 126;
update modules_cmds set icon = 'icon-text-parse' where id = 90;
update modules_cmds set icon = 'icon-word-bank' where id = 5;
update modules_cmds set icon = 'icon-multi-round-task' where id = 4;
update modules_cmds set icon = 'icon-knowledge-graph' where id = 18;
update modules_cmds set icon = 'icon-knowledge-graph' where id = 99;
update modules_cmds set icon = 'icon-data-statistics' where id = 6;
update modules_cmds set icon = 'icon-standard-question' where id = 19;
update modules_cmds set icon = 'icon-robot-setting' where id = 17;
update modules_cmds set icon = 'icon-material-library' where id = 97;
update modules_cmds set icon = 'icon-intent' where id = 13;
update modules_cmds set icon = 'icon-dialogue' where id = 22;
update modules_cmds set icon = 'icon-data-statistics' where id = 1;
update modules_cmds set icon = 'icon-dialogue' where id = 211;
update modules_cmds set icon = 'icon-data-statistics' where id = 119;

-- +migrate Down

update products set icon = 'bf' where code = 'bf';
update products set icon = 'ccbot' where code = 'ccbot';
update products set icon = 'qabot' where code = 'qabot';
update products set icon = 'sa' where code = 'sa';
update products set icon = 'csa' where code = 'csa';
update products set icon = 'aquarius' where code = 'aquarius';
update products set icon = 'vca' where code = 'vca';
update products set icon = 'nlu' where code = 'nlu';
update products set icon = 'mrm' where code = 'mrm';

update modules_cmds set icon = 'icon-ai-learning' where id = 124;
update modules_cmds set icon = 'icon-robot-chat' where id = 126;
update modules_cmds set icon = 'icon-text-parse' where id = 90;
update modules_cmds set icon = 'icon-word-bank' where id = 5;
update modules_cmds set icon = 'icon-multi-round-task' where id = 4;
update modules_cmds set icon = 'icon-knowledge-graph' where id = 18;
update modules_cmds set icon = 'icon-knowledge-graph' where id = 99;
update modules_cmds set icon = 'icon-data-statistics' where id = 6;
update modules_cmds set icon = 'icon-standard-questions' where id = 19;
update modules_cmds set icon = 'icon-robot-setting' where id = 17;
update modules_cmds set icon = 'icon-material-library' where id = 97;
update modules_cmds set icon = 'icon-intent' where id = 13;
update modules_cmds set icon = 'icon-dialogue' where id = 22;
update modules_cmds set icon = 'icon-data-statistics' where id = 1;
update modules_cmds set icon = 'icon-dialogue' where id = 211;
update modules_cmds set icon = 'icon-data-statistics' where id = 119;