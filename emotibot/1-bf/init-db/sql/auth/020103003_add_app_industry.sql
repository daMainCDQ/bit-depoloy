-- +migrate Up
INSERT INTO `app_props` (`id`, `p_key`, `p_value`) VALUES
(3, 'app_industry', 'general'),
(4, 'app_industry', 'ecommerce'),
(5, 'app_industry', 'insurance'),
(6, 'app_industry', 'bank'),
(7, 'app_industry', 'securities'),
(8, 'app_industry', 'government'),
(9, 'app_industry', 'enterprise'),
(10, 'app_industry', 'logistics');

-- +migrate Down
delete from app_props where id in (3,4,5,6,7,8,9,10);
