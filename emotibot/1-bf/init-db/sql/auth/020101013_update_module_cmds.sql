-- +migrate Up
UPDATE `modules` SET `status` = 1 WHERE `code` = 'mlp';

UPDATE `modules_cmds` SET `is_show` = 1, `sort` = 14 WHERE `modules_cmds`.`code` = 'mlp' and `modules_cmds`.`parent_id` = 0;

update `modules_cmds` set `sort` = 0, `is_show` = 0 where `id` = 123;

-- +migrate Down
update `modules_cmds` set `sort` = 2, `is_show` = 1 where `id` = 123;
