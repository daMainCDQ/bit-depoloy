-- +migrate Up

UPDATE `modules_cmds` SET `parent_cmd` = 'ner_factory_view' WHERE `modules_cmds`.`id` = 92;
UPDATE `modules_cmds` SET `parent_cmd` = 'ner_factory_view' WHERE `modules_cmds`.`id` = 93;
UPDATE `modules_cmds` SET `parent_cmd` = 'ner_factory_view,ner_factory_edit' WHERE `modules_cmds`.`id` = 94;

DELETE FROM `modules_cmds` WHERE `modules_cmds`.`id` = 8;
DELETE FROM `modules_cmds` WHERE `modules_cmds`.`id` = 41;
DELETE FROM `modules_cmds` WHERE `modules_cmds`.`id` = 42;

-- +migrate Down
