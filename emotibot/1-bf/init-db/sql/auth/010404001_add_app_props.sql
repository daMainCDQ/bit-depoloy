-- +migrate Up

CREATE TABLE `app_props` (
  `id` int(10) UNSIGNED NOT NULL COMMENT '机器人属性id',
  `p_key` varchar(50) NOT NULL COMMENT '属性键',
  `p_value` varchar(100) NOT NULL COMMENT '属性值',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='机器人属性表';

INSERT INTO `app_props` (`id`, `p_key`, `p_value`) VALUES
(1, 'chat_lang', 'cn'),
(2, 'chat_lang', 'en');

CREATE TABLE `rel_app_props` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '机器人属性关系id',
  `app_id` varchar(100) NOT NULL COMMENT '机器人id',
  `prop_id` int(10) UNSIGNED NOT NULL COMMENT '属性id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='机器人属性关系表';

INSERT INTO `rel_app_props` (`app_id`, `prop_id`)
  SELECT `uuid` AS `app_id`, 1
  FROM `apps`
  WHERE `status` = 1
  AND `app_type` = 0
;

-- +migrate Down

DROP TABLE `app_props`;
DROP TABLE `rel_app_props`;
