-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
UPDATE `apps` SET `secret` = concat(md5(concat(now(), `uuid`)), sha1(rand()));

-- +migrate Down
UPDATE `apps` SET `secret` = "";
