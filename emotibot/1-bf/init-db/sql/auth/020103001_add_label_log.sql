-- +migrate Up

update modules set cmd_list='view,log' where id=56;

INSERT INTO `modules_cmds` (`id`, `parent_id`, `parent_cmd`, `code`, `cmd`, `sort`, `position`, `icon`, `route`, `is_link`, `is_show`) VALUES
(303, 301, '', 'personas', 'log', 2, 'body_left', '', 'personas_log', 1, 1);

-- +migrate Down
DELETE FROM `modules_cmds` WHERE `id` in (303);
update modules_cmds set cmd_list='view' where id=56;

