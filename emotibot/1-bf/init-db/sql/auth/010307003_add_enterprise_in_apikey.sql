-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applie
ALTER TABLE `api_key` ADD `enterprise` char(36) NOT NULL DEFAULT '' COMMENT '企业 ID' AFTER `id`;
-- +migrate Down
ALTER TABLE `api_key` DROP COLUMN `enterprise`;
