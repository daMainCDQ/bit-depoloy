-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
UPDATE `intents` SET `name` = REPLACE (`name`,'谘询','咨询');
UPDATE `intents` SET `description` = REPLACE (`description`,'谘询','咨询');
UPDATE `intent_group` SET `name` = REPLACE (`name`,'谘询','咨询');
UPDATE `intent_parser` SET `parser_name` = REPLACE (`parser_name`,'谘询','咨询');
UPDATE `intent_test_intents` SET `intent_name` = REPLACE (`intent_name`,'谘询','咨询');
UPDATE `intent_test_sentences` SET `sentence` = REPLACE (`sentence`,'谘询','咨询');
UPDATE `intent_test_sentences` SET `answer` = REPLACE (`answer`,'谘询','咨询');
UPDATE `intent_train_sets` SET `sentence` = REPLACE (`sentence`,'谘询','咨询');
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back