-- +migrate Up
INSERT INTO `bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`) VALUES
('', 'statsd', 'controller', 'off', '设置是否要开启statsd统计，on: 开启, off: 关闭', 0),
('', 'faq-similar-question-range', 'controller', '10', '设置相似问阀值范围，范围0~100。', 0),
('', 'faq-recommand-question-range', 'controller', '10', '设置推荐问阀值范围，范围0~100。', 0);
-- +migrate Down
