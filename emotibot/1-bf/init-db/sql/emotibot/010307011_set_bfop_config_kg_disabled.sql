-- +migrate Up
UPDATE `bfop_config` SET `status` = '-1' WHERE `bfop_config`.`code` = 'knowledge';
UPDATE `bfop_config` SET `status` = '-1' WHERE `bfop_config`.`code` = 'knowledge-threshold';

-- +migrate Down
UPDATE `bfop_config` SET `status` = '0' WHERE `bfop_config`.`code` = 'knowledge';
UPDATE `bfop_config` SET `status` = '0' WHERE `bfop_config`.`code` = 'knowledge-threshold';
