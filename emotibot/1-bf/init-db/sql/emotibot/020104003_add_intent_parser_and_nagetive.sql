-- +migrate Up
CREATE TABLE `intent_negative_sets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_id` varchar(64) NOT NULL COMMENT '机器人id',
  `group_id` varchar(64) NOT NULL COMMENT '意图组id',
  `negative` varchar(500) NOT NULL COMMENT '意图组反例句子',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE intent_group CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
ALTER TABLE intents CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
ALTER TABLE intent_parser CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
ALTER TABLE intent_parser CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
ALTER TABLE union_intent_quizzes_info CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
ALTER TABLE union_intent_test_result CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;


-- +migrate Down