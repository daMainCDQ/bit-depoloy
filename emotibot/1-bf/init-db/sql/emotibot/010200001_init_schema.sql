-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- --------------------------------------------------------

--
-- 資料表結構 `audit_record`
--

CREATE TABLE `audit_record` (
  `id` bigint(11) UNSIGNED NOT NULL COMMENT 'audit id',
  `appid` varchar(36) NOT NULL COMMENT '机器人id',
  `user_id` varchar(64) NOT NULL DEFAULT '' COMMENT '进行修改的登录用户id',
  `ip_source` varchar(32) NOT NULL DEFAULT '' COMMENT '使用者IP',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改记录创建时间',
  `module` varchar(32) NOT NULL DEFAULT '' COMMENT '操作模组',
  `operation` varchar(32) NOT NULL DEFAULT '' COMMENT '操作类型',
  `content` mediumtext COMMENT '操作变更纪录',
  `result` tinyint(1) NOT NULL DEFAULT '0' COMMENT '操作结果'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='记录管理界面上所有新增、编辑、删除、汇入、汇出动作';

-- --------------------------------------------------------

--
-- 資料表結構 `api_userkey`
--

CREATE TABLE `api_userkey` (
  `UserId` varchar(50) NOT NULL COMMENT '用户id',
  `Count` int(11) NOT NULL,
  `Version` int(11) NOT NULL,
  `CreatedTime` datetime NOT NULL COMMENT '创建时间',
  `PreductName` varchar(255) NOT NULL,
  `ApiKey` varchar(50) NOT NULL COMMENT '机器人id',
  `Status` int(11) NOT NULL COMMENT '机器人状态',
  `MaxCount` int(11) DEFAULT '500',
  `AutoUserID` int(11) DEFAULT '0',
  `NickName` varchar(255) DEFAULT NULL COMMENT '机器人昵称',
  `CommonFunctionIds` varchar(200) DEFAULT NULL,
  `AreaIds` varchar(200) DEFAULT NULL,
  `Type` int(11) DEFAULT '1' COMMENT '类型',
  `MsgType` int(11) DEFAULT '1',
  `Msg` varchar(5000) DEFAULT '你好，我是你的机器人XX，我可以陪你聊天，为你答疑解惑哦！',
  `MsgJson` text,
  `template_api_key` varchar(50) DEFAULT '54f59223a3591795e91f9d786cd36184',
  `industry_id` int(11) DEFAULT '1',
  `order_no` int(11) DEFAULT '0',
  `description` varchar(300) DEFAULT NULL COMMENT '描述'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='机器人表';

-- --------------------------------------------------------

--
-- 資料表結構 `bf_controller_config`
--

CREATE TABLE `bf_controller_config` (
  `id` varchar(50) NOT NULL,
  `code` varchar(50) NOT NULL COMMENT '配置的key',
  `value` varchar(50) NOT NULL COMMENT '配置的value',
  `category` varchar(20) NOT NULL COMMENT '配置的类别',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(50) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user` varchar(50) DEFAULT NULL COMMENT '修改人',
  `is_remove` char(1) NOT NULL COMMENT '逻辑删除位 1为删除，0为正常'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='bf2.0 controller 配置表';

-- --------------------------------------------------------

--
-- 資料表結構 `bf_controller_reply`
--

CREATE TABLE `bf_controller_reply` (
  `id` varchar(50) NOT NULL,
  `text` varchar(200) NOT NULL COMMENT '返回的文本',
  `code` varchar(20) NOT NULL COMMENT '返回的key',
  `category` varchar(20) NOT NULL COMMENT '返回的文本的业务类别',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(50) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user` varchar(50) DEFAULT NULL COMMENT '修改人',
  `is_remove` char(1) NOT NULL COMMENT '逻辑删除位 1为删除，0为正常'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='bf2.0 controller 回复表';

-- --------------------------------------------------------

--
-- 資料表結構 `cmd`
--

CREATE TABLE `cmd` (
  `cmd_id` int(11) NOT NULL,
  `appid` varchar(36) NOT NULL COMMENT '机器人id',
  `cid` int(11) DEFAULT NULL COMMENT '指令类别id',
  `name` varchar(64) NOT NULL DEFAULT '' COMMENT '指令名字',
  `target` int(4) DEFAULT NULL COMMENT '0:问题1:答案',
  `rule` text COMMENT 'json [{"type":"keyword", "value": ["kw1","kw2"]}, {"type":"regex", "value": ["[1-5]"]}',
  `answer` text COMMENT 'json, 同问答库answer格式',
  `response_type` int(4) DEFAULT NULL COMMENT '0:取代, 1: 附加至前, 2:附加至后',
  `status` int(4) DEFAULT '0' COMMENT '0: 关闭1: 开启',
  `begin_time` datetime DEFAULT NULL COMMENT '生效时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='指令设定';

-- --------------------------------------------------------

--
-- 資料表結構 `cmd_class`
--

CREATE TABLE `cmd_class` (
  `id` int(11) NOT NULL,
  `appid` char(36) NOT NULL DEFAULT '' COMMENT '机器人id',
  `name` varchar(64) NOT NULL DEFAULT '' COMMENT '指令类别名称',
  `parent` int(11) DEFAULT NULL COMMENT '父类别id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='指令类别';

-- --------------------------------------------------------

--
-- 資料表結構 `cmd_robot_tag`
--

CREATE TABLE `cmd_robot_tag` (
  `cmd_id` int(11) NOT NULL COMMENT '指令id',
  `robot_tag_id` int(11) NOT NULL COMMENT '维度标签id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='指令与维度标签关连';

-- --------------------------------------------------------
-- --------------------------------------------------------

--
-- 資料表結構 `entities`
--

CREATE TABLE `entities` (
  `id` int(20) NOT NULL,
  `appid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '机器人id',
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '类别名称',
  `editable` tinyint(1) DEFAULT NULL COMMENT '是否可编辑',
  `cid` int(20) DEFAULT NULL COMMENT '类别id',
  `similar_words` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '同义词',
  `answer` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '敏感词回复',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='词库';

-- --------------------------------------------------------

--
-- 資料表結構 `entity_class`
--

CREATE TABLE `entity_class` (
  `id` int(20) NOT NULL,
  `appid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '机器人id',
  `name` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '类别名称',
  `pid` int(20) DEFAULT NULL COMMENT '父类别名称',
  `editable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否可编辑',
  `intent_engine` tinyint(1) NOT NULL DEFAULT '1',
  `rule_engine` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='词库类别';

-- --------------------------------------------------------

--
-- 資料表結構 `entity_files`
--

CREATE TABLE `entity_files` (
  `id` int(18) NOT NULL COMMENT 'id',
  `appid` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '机器人id',
  `filename` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '词库上传档案名称',
  `content` longblob NOT NULL COMMENT '词库上传档案',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='词库档案上传';

-- --------------------------------------------------------

--
-- 資料表結構 `ent_config`
--

CREATE TABLE `ent_config` (
  `name` varchar(50) NOT NULL COMMENT '配置名称',
  `module` varchar(45) NOT NULL DEFAULT 'default' COMMENT '配置模块',
  `value` varchar(8000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '配置值',
  `enabled` bit(1) DEFAULT b'1' COMMENT '是否启用',
  `description` varchar(500) DEFAULT NULL COMMENT '描述',
  `zhtw` varchar(4000) DEFAULT NULL COMMENT '台湾繁体中文',
  `enus` varchar(4000) DEFAULT NULL COMMENT '英文美国'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='配置表';

--
-- 資料表結構 `ent_config_appid_customization`
--

CREATE TABLE `ent_config_appid_customization` (
  `name` varchar(50) NOT NULL COMMENT '配置名称',
  `app_id` varchar(50) NOT NULL COMMENT '机器人id',
  `value` varchar(8000) NOT NULL COMMENT '配置值'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='机器人定制化配置表';

--
-- 資料表結構 `ent_config_industry_customization`
--

CREATE TABLE `ent_config_industry_customization` (
  `name` varchar(50) NOT NULL,
  `industry_id` varchar(50) NOT NULL,
  `value` varchar(4000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- 資料表結構 `ent_config_user_customization`
--

CREATE TABLE `ent_config_user_customization` (
  `name` varchar(50) NOT NULL COMMENT '配置key',
  `user_id` varchar(50) NOT NULL COMMENT '用户id',
  `value` varchar(4000) NOT NULL COMMENT '配置值'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户定制化配置';

-- --------------------------------------------------------

--
-- 資料表結構 `ent_config_version`
--

CREATE TABLE `ent_config_version` (
  `user_id` varchar(50) NOT NULL DEFAULT 'system' COMMENT '用户id',
  `app_id` varchar(50) NOT NULL DEFAULT 'system' COMMENT '机器人id',
  `version_value` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '版本信息'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='用户机器人版本信息';

-- --------------------------------------------------------

--
-- 資料表結構 `function_switch`
--

CREATE TABLE `function_switch` (
  `function_id` int(11) NOT NULL COMMENT '技能id',
  `appid` char(36) NOT NULL DEFAULT '' COMMENT '机器人id',
  `module_name` varchar(20) NOT NULL COMMENT '模组名称',
  `module_name_zh` varchar(20) NOT NULL COMMENT '模组中文名称',
  `third_url` text COMMENT '第三方url',
  `on_off` tinyint(1) NOT NULL DEFAULT '0' COMMENT '开关(0: 关, 1: 开)',
  `remark` varchar(50) DEFAULT NULL COMMENT '备注',
  `intent` varchar(20) NOT NULL COMMENT '触发意图',
  `share` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否共享',
  `type` varchar(20) NOT NULL COMMENT '类型',
  `status` int(11) DEFAULT '0' COMMENT '状态(0: 显示, -1: 隐藏)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='机器人技能开关';

-- --------------------------------------------------------

--
-- 資料表結構 `locker`
--

CREATE TABLE `locker` (
  `lock_id` bigint(20) UNSIGNED NOT NULL COMMENT 'id',
  `get_by` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '得到锁的使用者',
  `updated_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- 資料表結構 `robot_answer`
--

CREATE TABLE `robot_answer` (
  `a_id` int(4) NOT NULL COMMENT '形象答案id',
  `appid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '机器人id',
  `parent_q_id` int(4) NOT NULL COMMENT '形象问题id',
  `content` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '答案内容',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='机器人形象答案';

-- --------------------------------------------------------

--
-- 資料表結構 `robot_question`
--

CREATE TABLE `robot_question` (
  `q_id` int(10) NOT NULL COMMENT '形象问题id',
  `content` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '形象问题内容',
  `appid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '机器人id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `answer_count` smallint(5) DEFAULT '0' COMMENT '答案数量',
  `content2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '相关问题内容2',
  `content3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '相关问题内容3',
  `content4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '相关问题内容4',
  `content5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '相关问题内容5',
  `content6` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '相关问题内容6',
  `content7` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '相关问题内容7',
  `content8` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '相关问题内容8',
  `content9` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '相关问题内容9',
  `content10` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '相关问题内容10',
  `status` int(2) DEFAULT '0' COMMENT '状态(0: 正常, 1: 待更新)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 資料表結構 `robot_profile_answer`
--

CREATE TABLE IF NOT EXISTS `robot_profile_answer` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '答案id',
  `appid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '机器人id',
  `qid` int(10) NOT NULL COMMENT '关连问题id',
  `content` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '答案内容',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `content` (`content`),
  KEY `IDX_a_id` (`id`),
  KEY `answer_parent_q_id` (`qid`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 資料表結構 `robot_profile_extend`
--

CREATE TABLE IF NOT EXISTS `robot_profile_extend` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '扩展问id',
  `appid` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '机器人id',
  `qid` int(10) NOT NULL COMMENT '关连问题id',
  `content` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '问题内容',
  `createdtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `content` (`content`),
  KEY `IDX_q_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='机器人形象扩展问题';

-- --------------------------------------------------------

--
-- 資料表結構 `robot_profile_question`
--

CREATE TABLE IF NOT EXISTS `robot_profile_question` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '机器人形象问题',
  `content` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '问题内容',
  `createdtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `content` (`content`),
  KEY `IDX_q_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='机器人形象问题';

-- --------------------------------------------------------

--
-- 資料表結構 `robot_words`
--

CREATE TABLE `robot_words` (
  `id` int(11) NOT NULL COMMENT '机器人话述id',
  `appid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '机器人id',
  `content` text COLLATE utf8mb4_unicode_ci COMMENT '话述内容',
  `type` int(4) DEFAULT '1' COMMENT '机器人话述类型id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 資料表結構 `robot_words_type`
--

CREATE TABLE `robot_words_type` (
  `type` int(4) NOT NULL COMMENT '话述类型id',
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '话述名称',
  `comment` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '话述备注'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 資料表結構 `state_machine`
--

CREATE TABLE `state_machine` (
  `state_id` bigint(20) UNSIGNED NOT NULL COMMENT 'id',
  `content` longblob COMMENT '档案内容',
  `action` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '执行动作',
  `status` varchar(16) COLLATE utf8_unicode_ci NOT NULL COMMENT '状态',
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT '使用者id',
  `created_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `extra_info` text COLLATE utf8_unicode_ci COMMENT '系统讯息'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- 資料表結構 `tags`
--

CREATE TABLE `tags` (
  `id` int(20) NOT NULL COMMENT 'id',
  `code` char(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '代码',
  `name` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '标签显示名称',
  `type` int(20) DEFAULT NULL COMMENT '标签类型',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `app_id` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'system' COMMENT '机器人id (system: 共用)',
  `label_id` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'dim' COMMENT '标签id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='ssm维度标签表';

-- --------------------------------------------------------

--
-- 資料表結構 `tag_type`
--

CREATE TABLE `tag_type` (
  `id` int(4) NOT NULL COMMENT '维度id',
  `code` char(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '维度代码',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '维度显示名称',
  `appid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '机器人id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='ssm维度表';

-- --------------------------------------------------------

--
-- 資料表結構 `taskengineapp`
--

CREATE TABLE `taskengineapp` (
  `pk` varchar(90) NOT NULL COMMENT '主键',
  `appID` varchar(50) NOT NULL COMMENT '机器人id',
  `scenarioID` varchar(50) NOT NULL COMMENT '场景id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='多轮引擎机器人场景关连表';

-- --------------------------------------------------------

--
-- 資料表結構 `taskenginemappingtable`
--

CREATE TABLE `taskenginemappingtable` (
  `mapping_table_name` varchar(50) NOT NULL COMMENT '转换数据表名称',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `update_user` varchar(50) DEFAULT NULL COMMENT '更新使用者',
  `appID` varchar(50) DEFAULT NULL COMMENT '机器人id',
  `content` mediumtext NOT NULL COMMENT '转换数据表内容'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='任务引擎转换数据表';

-- --------------------------------------------------------

--
-- 資料表結構 `taskenginescenario`
--

CREATE TABLE `taskenginescenario` (
  `scenarioID` varchar(50) NOT NULL COMMENT '场景id',
  `userID` varchar(50) NOT NULL COMMENT '使用者id',
  `appID` varchar(50) DEFAULT NULL COMMENT '机器人id',
  `content` mediumtext COMMENT '场景内容',
  `layout` mediumtext COMMENT '场景节点位置',
  `public` int(11) NOT NULL DEFAULT '0' COMMENT '是否为公用的场景',
  `editing` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否为编辑中',
  `editingContent` mediumtext COMMENT '编辑中的场景内容',
  `editingLayout` mediumtext COMMENT '编辑中的场景节点位置',
  `updatetime` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '更新时间',
  `onoff` int(11) DEFAULT '1' COMMENT '场景开关'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='任务引擎场景表';

-- --------------------------------------------------------

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_chat_log_manual_clustering_request_log`
--

CREATE TABLE `tbl_chat_log_manual_clustering_request_log` (
  `appid` varchar(50) NOT NULL,
  `request_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pendIndex` varchar(50) NOT NULL,
  `status` enum('pending','running','done','failed') DEFAULT NULL,
  `done_datetime` datetime DEFAULT NULL,
  `conditions` varchar(8000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_chat_log_manual_clustering_tag_group`
--

CREATE TABLE `tbl_chat_log_manual_clustering_tag_group` (
  `id` int(11) NOT NULL,
  `report_id` varchar(500) NOT NULL,
  `tag` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------



-- --------------------------------------------------------

--
-- 資料表結構 `tbl_chat_optimization_report_tag_group`
--

CREATE TABLE `tbl_chat_optimization_report_tag_group` (
  `id` int(11) NOT NULL,
  `report_id` int(11) NOT NULL,
  `tag` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_chat_optimization_report_tag_group_content`
--

CREATE TABLE `tbl_chat_optimization_report_tag_group_content` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `content` varchar(8000) NOT NULL,
  `labeled` bit(1) NOT NULL DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_exec_sqlq_semblance_status`
--

CREATE TABLE `tbl_exec_sqlq_semblance_status` (
  `appid` varchar(50) NOT NULL,
  `status` enum('idle','running') NOT NULL DEFAULT 'idle',
  `updatetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_function_group`
--

CREATE TABLE `tbl_function_group` (
  `function_name` varchar(50) NOT NULL,
  `function_group_id` int(11) NOT NULL,
  `order_no` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_industry_function_group`
--

CREATE TABLE `tbl_industry_function_group` (
  `industry_id` int(11) NOT NULL,
  `function_group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_lq_semblance_map`
--

CREATE TABLE `tbl_lq_semblance_map` (
  `lq1` varchar(250) NOT NULL,
  `lq2` varchar(250) NOT NULL,
  `score` float NOT NULL DEFAULT '0',
  `sq_map_id` varchar(200) NOT NULL,
  `updatetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `appid` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------


-- --------------------------------------------------------

--
-- 資料表結構 `tbl_robot_ssm_stage`
--

CREATE TABLE `tbl_robot_ssm_stage` (
  `app_id` varchar(50) NOT NULL,
  `module` varchar(50) NOT NULL,
  `stage` int(11) NOT NULL,
  `stage_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_robot_tag`
--

CREATE TABLE `tbl_robot_tag` (
  `id` bigint(11) NOT NULL COMMENT 'tag的ID标识',
  `app_id` varchar(100) NOT NULL COMMENT 'tag所属的机器人id',
  `name` varchar(100) NOT NULL COMMENT 'tag的名字',
  `type` enum('system','userdefine') NOT NULL DEFAULT 'userdefine' COMMENT 'tag类型，系统默认或用户定义',
  `category` enum('sq','other') NOT NULL DEFAULT 'sq' COMMENT 'tag类别，用于区分tag的类别属性',
  `createtime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'tag创建时间',
  `description` varchar(500) NOT NULL DEFAULT 'no' COMMENT 'tag描述信息'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------


--
-- 資料表結構 `tbl_semblance_sqlq_calc_status`
--

CREATE TABLE `tbl_semblance_sqlq_calc_status` (
  `appid` varchar(50) NOT NULL,
  `request_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pendIndex` int(11) NOT NULL,
  `status` enum('pending','running','done','failed') DEFAULT 'pending',
  `done_datetime` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------


--
-- 資料表結構 `tbl_sq_category`
--

CREATE TABLE `tbl_sq_category` (
  `id` int(11) NOT NULL,
  `pid` int(11) NOT NULL DEFAULT '0',
  `app_id` varchar(50) NOT NULL,
  `level` smallint(5) NOT NULL DEFAULT '0',
  `name` varchar(45) DEFAULT NULL,
  `label` varchar(30) DEFAULT NULL,
  `path` varchar(200) DEFAULT NULL,
  `is_del` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_sq_semblance_map`
--

CREATE TABLE `tbl_sq_semblance_map` (
  `sq1` varchar(250) NOT NULL,
  `sq2` varchar(250) NOT NULL,
  `score` float NOT NULL DEFAULT '0',
  `appid` varchar(50) NOT NULL,
  `updatetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_upload_result_details`
--

CREATE TABLE `tbl_upload_result_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `upload_id` int(11) NOT NULL,
  `action` varchar(45) NOT NULL,
  `row_id` int(11) NOT NULL DEFAULT '0',
  `code` varchar(45) NOT NULL,
  `message` varchar(2000) DEFAULT NULL,
  `date_time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------


--
-- 已匯出資料表的索引
--

--
-- Table structure for table `intent_versions`
--

DROP TABLE IF EXISTS `intent_versions`;

CREATE TABLE `intent_versions` (
  `intent_version_id` int(11) NOT NULL AUTO_INCREMENT,
  `app_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `ie_model_id` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `re_model_id` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orig_file_name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `file_name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `in_used` tinyint(1) NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`intent_version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `intents`
--

DROP TABLE IF EXISTS `intents`;

CREATE TABLE `intents` (
  `intent_id` int(11) NOT NULL AUTO_INCREMENT,
  `app_id` varchar(128) NOT NULL,
  `name` varchar(256) NOT NULL,
  `intent_version_id` int(11) NOT NULL,
  PRIMARY KEY (`intent_id`),
  KEY `intent_version_id` (`intent_version_id`),
  CONSTRAINT `intents_ibfk_1` FOREIGN KEY (`intent_version_id`) REFERENCES `intent_versions` (`intent_version_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `intent_train_sets`;

--
-- Table structure for table `intent_train_sets`
--

CREATE TABLE `intent_train_sets` (
  `intent_train_set_id` int(11) NOT NULL AUTO_INCREMENT,
  `sentence` varchar(256) NOT NULL,
  `intent_id` int(11) NOT NULL,
  `intent_version_id` int(11) NOT NULL,
  PRIMARY KEY (`intent_train_set_id`),
  KEY `intent_id` (`intent_id`),
  KEY `intent_version_id` (`intent_version_id`),
  CONSTRAINT `intent_train_sets_ibfk_1` FOREIGN KEY (`intent_id`) REFERENCES `intents` (`intent_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `intent_train_sets_ibfk_2` FOREIGN KEY (`intent_version_id`) REFERENCES `intent_versions` (`intent_version_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6080 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表索引 `api_userkey`
--
ALTER TABLE `api_userkey`
  ADD PRIMARY KEY (`UserId`,`PreductName`),
  ADD KEY `idx_template_api_key` (`template_api_key`),
  ADD KEY `idx_type` (`Type`),
  ADD KEY `idx_status` (`Status`),
  ADD KEY `idx_industryid` (`industry_id`),
  ADD KEY `idx_apikey` (`ApiKey`),
  ADD KEY `api_userkey_ApiKey_IDX` (`ApiKey`(10)) USING BTREE;

--
-- 資料表索引 `audit_record`
--
ALTER TABLE `audit_record`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_appid` (`appid`);

--
-- 資料表索引 `bf_controller_config`
--
ALTER TABLE `bf_controller_config`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `bf_controller_reply`
--
ALTER TABLE `bf_controller_reply`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `cmd`
--
ALTER TABLE `cmd`
  ADD PRIMARY KEY (`cmd_id`);

--
-- 資料表索引 `cmd_class`
--
ALTER TABLE `cmd_class`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `cmd_robot_tag`
--
ALTER TABLE `cmd_robot_tag`
  ADD PRIMARY KEY (`cmd_id`,`robot_tag_id`);



--
-- 資料表索引 `entities`
--
ALTER TABLE `entities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_cid` (`cid`);

--
-- 資料表索引 `entity_class`
--
ALTER TABLE `entity_class`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_appid` (`appid`);

--
-- 資料表索引 `entity_files`
--
ALTER TABLE `entity_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `appid` (`appid`);


--
-- 資料表索引 `ent_config`
--
ALTER TABLE `ent_config`
  ADD PRIMARY KEY (`name`),
  ADD KEY `idx_name_module` (`name`(20),`module`(20));

--
-- 資料表索引 `ent_config_appid_customization`
--
ALTER TABLE `ent_config_appid_customization`
  ADD PRIMARY KEY (`name`,`app_id`);

--
-- 資料表索引 `ent_config_industry_customization`
--
ALTER TABLE `ent_config_industry_customization`
  ADD PRIMARY KEY (`name`,`industry_id`);

--
-- 資料表索引 `ent_config_user_customization`
--
ALTER TABLE `ent_config_user_customization`
  ADD PRIMARY KEY (`name`,`user_id`);

--
-- 資料表索引 `ent_config_version`
--
ALTER TABLE `ent_config_version`
  ADD PRIMARY KEY (`user_id`,`app_id`);




--
-- 資料表索引 `function_switch`
--
ALTER TABLE `function_switch`
  ADD PRIMARY KEY (`function_id`),
  ADD KEY `appid_Idx` (`appid`);

--
-- 資料表索引 `locker`
--
ALTER TABLE `locker`
  ADD PRIMARY KEY (`lock_id`),
  ADD UNIQUE KEY `lock_id_UNIQUE` (`lock_id`);


--
-- 資料表索引 `robot_answer`
--
ALTER TABLE `robot_answer`
  ADD PRIMARY KEY (`a_id`),
  ADD KEY `content` (`content`),
  ADD KEY `IDX_a_id` (`a_id`),
  ADD KEY `answer_parent_q_id` (`parent_q_id`);

--
-- 資料表索引 `robot_question`
--
ALTER TABLE `robot_question`
  ADD PRIMARY KEY (`q_id`),
  ADD KEY `content` (`content`),
  ADD KEY `IDX_q_id` (`q_id`);

--
-- 資料表索引 `robot_words`
--
ALTER TABLE `robot_words`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `robot_words_type`
--
ALTER TABLE `robot_words_type`
  ADD PRIMARY KEY (`type`);

--
-- 資料表索引 `state_machine`
--
ALTER TABLE `state_machine`
  ADD PRIMARY KEY (`state_id`),
  ADD KEY `action` (`action`),
  ADD KEY `state` (`status`),
  ADD KEY `user` (`user_id`);

--
-- 資料表索引 `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `tag_type`
--
ALTER TABLE `tag_type`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `taskengineapp`
--
ALTER TABLE `taskengineapp`
  ADD PRIMARY KEY (`pk`),
  ADD KEY `appID` (`appID`);

--
-- 資料表索引 `taskenginemappingtable`
--
ALTER TABLE `taskenginemappingtable`
  ADD PRIMARY KEY (`mapping_table_name`);

--
-- 資料表索引 `taskenginescenario`
--
ALTER TABLE `taskenginescenario`
  ADD PRIMARY KEY (`scenarioID`),
  ADD KEY `userID` (`userID`),
  ADD KEY `public` (`public`);



--
-- 資料表索引 `tbl_chat_log_manual_clustering_request_log`
--
ALTER TABLE `tbl_chat_log_manual_clustering_request_log`
  ADD PRIMARY KEY (`pendIndex`);

--
-- 資料表索引 `tbl_chat_log_manual_clustering_tag_group`
--
ALTER TABLE `tbl_chat_log_manual_clustering_tag_group`
  ADD PRIMARY KEY (`id`);








--
-- 資料表索引 `tbl_chat_optimization_report_tag_group`
--
ALTER TABLE `tbl_chat_optimization_report_tag_group`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_REP_ID_idx` (`report_id`);

--
-- 資料表索引 `tbl_chat_optimization_report_tag_group_content`
--
ALTER TABLE `tbl_chat_optimization_report_tag_group_content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK2ASDFASDF_idx` (`group_id`);

--
-- 資料表索引 `tbl_exec_sqlq_semblance_status`
--
ALTER TABLE `tbl_exec_sqlq_semblance_status`
  ADD PRIMARY KEY (`appid`),
  ADD UNIQUE KEY `appid_UNIQUE` (`appid`);

--
-- 資料表索引 `tbl_function_group`
--
ALTER TABLE `tbl_function_group`
  ADD PRIMARY KEY (`function_name`,`function_group_id`),
  ADD KEY `fk_function_group_id_idx` (`function_group_id`);

--
-- 資料表索引 `tbl_industry_function_group`
--
ALTER TABLE `tbl_industry_function_group`
  ADD PRIMARY KEY (`industry_id`,`function_group_id`),
  ADD KEY `fk_funtion_group_id_idx` (`function_group_id`),
  ADD KEY `fk_solution_function_solution_id_idx` (`industry_id`);

--
-- 資料表索引 `tbl_lq_semblance_map`
--
ALTER TABLE `tbl_lq_semblance_map`
  ADD PRIMARY KEY (`lq1`,`lq2`,`sq_map_id`);





--
-- 資料表索引 `tbl_robot_ssm_stage`
--
ALTER TABLE `tbl_robot_ssm_stage`
  ADD PRIMARY KEY (`app_id`,`module`);

--
-- 資料表索引 `tbl_robot_tag`
--
ALTER TABLE `tbl_robot_tag`
  ADD PRIMARY KEY (`id`,`app_id`);


--
-- 資料表索引 `tbl_semblance_sqlq_calc_status`
--
ALTER TABLE `tbl_semblance_sqlq_calc_status`
  ADD PRIMARY KEY (`pendIndex`);


--
-- 資料表索引 `tbl_sq_category`
--
ALTER TABLE `tbl_sq_category`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `tbl_sq_semblance_map`
--
ALTER TABLE `tbl_sq_semblance_map`
  ADD PRIMARY KEY (`appid`,`sq2`,`sq1`);



--
-- 使用資料表 AUTO_INCREMENT `audit_record`
--
ALTER TABLE `audit_record`
  MODIFY `id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'audit id', AUTO_INCREMENT=3;

--
-- 使用資料表 AUTO_INCREMENT `cmd`
--
ALTER TABLE `cmd`
  MODIFY `cmd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- 使用資料表 AUTO_INCREMENT `cmd_class`
--
ALTER TABLE `cmd_class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表 AUTO_INCREMENT `entities`
--
ALTER TABLE `entities`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- 使用資料表 AUTO_INCREMENT `entity_class`
--
ALTER TABLE `entity_class`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- 使用資料表 AUTO_INCREMENT `entity_files`
--
ALTER TABLE `entity_files`
  MODIFY `id` int(18) NOT NULL AUTO_INCREMENT COMMENT 'id', AUTO_INCREMENT=2;


--
-- 使用資料表 AUTO_INCREMENT `function_switch`
--
ALTER TABLE `function_switch`
  MODIFY `function_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '技能id', AUTO_INCREMENT=61;


--
-- 使用資料表 AUTO_INCREMENT `robot_answer`
--
ALTER TABLE `robot_answer`
  MODIFY `a_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '形象答案id', AUTO_INCREMENT=79;

--
-- 使用資料表 AUTO_INCREMENT `robot_question`
--
ALTER TABLE `robot_question`
  MODIFY `q_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '形象问题id', AUTO_INCREMENT=51;

--
-- 使用資料表 AUTO_INCREMENT `robot_words`
--
ALTER TABLE `robot_words`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '机器人话述id', AUTO_INCREMENT=31;

--
-- 使用資料表 AUTO_INCREMENT `state_machine`
--
ALTER TABLE `state_machine`
  MODIFY `state_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id';

--
-- 使用資料表 AUTO_INCREMENT `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT COMMENT 'id', AUTO_INCREMENT=31;

--
-- 使用資料表 AUTO_INCREMENT `tag_type`
--
ALTER TABLE `tag_type`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT COMMENT '维度id', AUTO_INCREMENT=5;



--
-- 使用資料表 AUTO_INCREMENT `tbl_chat_log_manual_clustering_tag_group`
--
ALTER TABLE `tbl_chat_log_manual_clustering_tag_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;




--
-- 使用資料表 AUTO_INCREMENT `tbl_chat_optimization_report_tag_group`
--
ALTER TABLE `tbl_chat_optimization_report_tag_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表 AUTO_INCREMENT `tbl_chat_optimization_report_tag_group_content`
--
ALTER TABLE `tbl_chat_optimization_report_tag_group_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;



--
-- 使用資料表 AUTO_INCREMENT `tbl_robot_tag`
--
ALTER TABLE `tbl_robot_tag`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'tag的ID标识', AUTO_INCREMENT=463;

--
-- 使用資料表 AUTO_INCREMENT `tbl_semblance_sqlq_calc_status`
--
ALTER TABLE `tbl_semblance_sqlq_calc_status`
  MODIFY `pendIndex` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表 AUTO_INCREMENT `tbl_sq_category`
--
ALTER TABLE `tbl_sq_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=292;


-- -----------------------------------------------------
-- Placeholder table for view `emotibot`.`question_answer_view`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `emotibot`.`question_answer_view` (`q_id` INT, `content` INT, `content2` INT, `content3` INT, `content4` INT, `content5` INT, `content6` INT, `content7` INT, `content8` INT, `content9` INT, `content10` INT, `a_id` INT, `answer` INT);



