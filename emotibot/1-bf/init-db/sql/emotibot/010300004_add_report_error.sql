-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE `report_errors` (
    `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
    `report_id` bigint UNSIGNED NOT NULL COMMENT '工作id',
    `cause` TEXT NOT NULL COMMENT '錯誤發生詳細訊息',
    `created_time` bigint(11) NOT NULL COMMENT '錯誤發生時間點的UTC Timestamp',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='聚類工作錯誤資訊';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `report_errors`;