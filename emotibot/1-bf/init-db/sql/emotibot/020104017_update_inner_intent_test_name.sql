-- +migrate Up
CREATE TABLE intents_tmp LIKE intents;
CREATE TABLE intent_test_intents_tmp LIKE intent_test_intents;
CREATE TABLE intent_group_tmp LIKE intent_group;
delete from intents where LENGTH(group_id) < 32;
delete from intent_test_intents where LENGTH(group_id) < 32;
delete from intent_group where LENGTH(group_id) < 32;
UPDATE intent_test_intents set intent_name = '退出，服务' WHERE intent_name = '退出' and group_id = 'generalintent';
UPDATE `intents` SET `name` = REPLACE (`name`,'谘询','咨询');
UPDATE `intent_test_intents` SET `intent_name` = REPLACE (`intent_name`,'谘询','咨询');
-- +migrate Down