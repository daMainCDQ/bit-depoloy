-- +migrate Up
-- ----------------------------
-- backup intent table
-- ----------------------------
RENAME TABLE intents TO intents_v2, intent_versions TO intent_versions_v2, intent_test_intents TO intent_test_intents_v2, intent_test_versions TO intent_test_versions_v2;

-- ----------------------------
-- Table structure for intent_test_intents
-- ----------------------------
CREATE TABLE `intent_test_intents` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '测试意图 ID',
  `app_id` char(64) NOT NULL DEFAULT '' COMMENT '机器人 ID',
  `intent_name` varchar(256) DEFAULT NULL COMMENT '意图名称，若为 NULL，代表为负类测试意图',
  `version` int(11) DEFAULT NULL COMMENT '对应的测试版本，若为 NULL，代表为目前正在编辑中的测试集意图',
  `updated_time` int(11) NOT NULL COMMENT '意图最后被测试/更新的时间',
  PRIMARY KEY (`id`),
  KEY `app_id` (`app_id`),
  KEY `intent_name` (`intent_name`),
  KEY `version` (`version`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='測試意圖';

-- ----------------------------
-- Table structure for intent_test_versions
-- ----------------------------
CREATE TABLE `intent_test_versions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '测试版本',
  `app_id` char(64) NOT NULL DEFAULT '' COMMENT '机器人 ID',
  `name` varchar(128) NOT NULL DEFAULT '' COMMENT '测试集名称',
  `tester` char(32) NOT NULL DEFAULT '' COMMENT '发起测试者 UUID',
  `ie_model_id` char(64) NOT NULL DEFAULT '' COMMENT '测试意图时所使用的意图模型 ID',
  `intents_count` int(11) NOT NULL COMMENT '测试意图数',
  `sentences_count` int(11) NOT NULL COMMENT '测试语句数',
  `true_positives` int(11) NOT NULL DEFAULT '0' COMMENT '真阳个数 (测试语料意图与所指定的意图相符)',
  `false_positives` int(11) NOT NULL DEFAULT '0' COMMENT '伪阳个数 (测试语料意图与所指定的意图不相符)',
  `true_negatives` int(11) NOT NULL DEFAULT '0' COMMENT '真阴个数 (负类测试语料不符合任意的意图)',
  `false_negatives` int(11) NOT NULL DEFAULT '0' COMMENT '伪阴个数 (负类测试语料符合了任意的意图)',
  `in_used` tinyint(1) NOT NULL DEFAULT '0' COMMENT '测试集是否正在使用中',
  `start_time` int(11) NOT NULL COMMENT '测试开始时间',
  `end_time` int(11) DEFAULT NULL COMMENT '测试结束时间',
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '测试状态；0:測試任務進行中、1:测试完成、2:测试失败',
  `progress` int(11) NOT NULL DEFAULT '0' COMMENT '已完成测试语句数',
  `saved` tinyint(1) NOT NULL DEFAULT '0' COMMENT '意图测试集是否被保存',
  `message` text COMMENT '测试任务相关讯息，包含错误讯息... etc',
  PRIMARY KEY (`id`),
  KEY `app_id` (`app_id`),
  KEY `ie_model_id` (`ie_model_id`),
  KEY `in_used` (`in_used`),
  KEY `status` (`status`),
  KEY `saved` (`saved`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='測試版本';

-- ----------------------------
-- Table structure for intent_versions
-- ----------------------------
CREATE TABLE `intent_versions` (
  `version` int(11) NOT NULL AUTO_INCREMENT COMMENT '意图版本',
  `appid` char(36) CHARACTER SET utf8mb4 DEFAULT NULL,
  `ie_model_id` char(64) CHARACTER SET utf8mb4 DEFAULT NULL,
  `re_model_id` char(64) CHARACTER SET utf8mb4 DEFAULT NULL,
  `in_used` tinyint(1) NOT NULL DEFAULT '0',
  `commit_time` int(20) NOT NULL COMMENT '加入时间',
  `start_train` int(20) DEFAULT NULL COMMENT '开始训练时间',
  `end_train` int(20) DEFAULT NULL COMMENT '完成训练时间',
  `sentence_count` int(20) NOT NULL DEFAULT '0' COMMENT '语料数量',
  `result` tinyint(1) NOT NULL DEFAULT '0' COMMENT '训练状态',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='意图训练版本';

-- ----------------------------
-- Table structure for intents
-- ----------------------------
CREATE TABLE `intents` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `appid` varchar(128) CHARACTER SET utf8mb4 DEFAULT NULL,
  `name` varchar(256) CHARACTER SET utf8mb4 DEFAULT NULL,
  `version` int(11) DEFAULT NULL COMMENT '意图版本',
  `updatetime` int(20) NOT NULL COMMENT '更新日期',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `IDX_version` (`version`),
  KEY `idx_appid` (`appid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='意图列表';

-- ----------------------------
-- copy data to backup table
-- ----------------------------
SET AUTOCOMMIT=0;
INSERT INTO `intents` (`id`, `appid`, `name`, `version`, `updatetime`, `status`)
SELECT `id`, `appid`, `name`, `version`, `updatetime`, `status`
FROM `intents_v2`;

INSERT INTO `intent_versions` (`version`, `appid`, `ie_model_id`, `re_model_id`, `in_used`, `commit_time`, `start_train`, `end_train`, `sentence_count`, `result`)
SELECT `version`, `appid`, `ie_model_id`, `re_model_id`, `in_used`, `commit_time`, `start_train`, `end_train`, `sentence_count`, `result`
FROM `intent_versions_v2`;

INSERT INTO `intent_test_intents` (`id`, `app_id`, `intent_name`, `version`, `updated_time`)
SELECT `id`, `app_id`, `intent_name`, `version`, `updated_time`
FROM `intent_test_intents_v2`;

INSERT INTO `intent_test_versions` (`id`, `app_id`, `name`, `tester`, `ie_model_id`, `intents_count`, `sentences_count`,
  `true_positives`, `false_positives`, `true_negatives`, `false_negatives`, `in_used`, `start_time`, `end_time`, `status`, `progress`, `saved`, `message`)
SELECT `id`, `app_id`, `name`, `tester`, `ie_model_id`, `intents_count`, `sentences_count`,
  `true_positives`, `false_positives`, `true_negatives`, `false_negatives`, `in_used`, `start_time`, `end_time`, `status`, `progress`, `saved`, `message`
FROM `intent_test_versions_v2`;
SET AUTOCOMMIT=1;

-- ----------------------------
-- Table structure for intent_group
-- ----------------------------
DROP TABLE IF EXISTS `intent_group`;
CREATE TABLE `intent_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `group_id` varchar(64) NOT NULL COMMENT '意图组ID，唯一索引',
  `name` varchar(256) NOT NULL COMMENT '意图组名称',
  `app_id` char(64) NOT NULL COMMENT '机器人ID',
  `group_type` tinyint(2) NOT NULL COMMENT '意图组类型，0-所有意图；1-内置意图；2-自定义意图；',
  `group_status` tinyint(2) NOT NULL COMMENT '意图组状态，0-未训练；1-训练成功；2-训练中；3-训练失败',
  `on_off` tinyint(2) NOT NULL DEFAULT 0 COMMENT '意图组开关：0-关；1-开；默认关，训练后开',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='意图3.0新增表1 ：意图组表';

-- ----------------------------
-- Table structure for union_intent_quizzes_info
-- ----------------------------
DROP TABLE IF EXISTS `union_intent_quizzes_info`;
CREATE TABLE `union_intent_quizzes_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `app_id` char(64) NOT NULL COMMENT '机器人ID',
  `sentence` varchar(256) NOT NULL COMMENT '测试语料',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='意图3.0新增表2 ：意图组联合测试题表';

-- ----------------------------
-- Table structure for union_intent_test_result
-- ----------------------------
DROP TABLE IF EXISTS `union_intent_test_result`;
CREATE TABLE `union_intent_test_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `group_id` varchar(64) NOT NULL COMMENT '组id',
  `intent_id` int(11) DEFAULT NULL COMMENT '意图id',
  `union_intent_quizzes_info_id` int(11) NOT NULL COMMENT '联合意图测试题ID',
  `app_id` varchar(64) NOT NULL COMMENT '机器人ID',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `app_id` (`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='意图3.0新增表3 ：联合意图测试结果表';

-- ----------------------------
-- Table structure for union_intent_test_version
-- ----------------------------
DROP TABLE IF EXISTS `union_intent_test_version`;
CREATE TABLE `union_intent_test_version` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `test_group_ids` varchar(1024) NOT NULL COMMENT '本次训练的模型Ids，英文逗号分隔',
  `app_id` varchar(64) NOT NULL COMMENT '机器人ID',
  `progress` varchar(6) DEFAULT NULL COMMENT '进度百分比',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='意图3.0新增表4 ：联合测试意图组ID';

-- ----------------------------
-- init inner intent group
-- ----------------------------
LOCK TABLES `intent_group` WRITE;
INSERT INTO `intent_group` VALUES (1, 'chat', '日常闲聊', 'system', 1, 1, 0,'2019-11-28 21:18:33', '2019-11-28 23:33:49');
INSERT INTO `intent_group` VALUES (2, 'entertainment', '娱乐领域', 'system', 1, 1, 0, '2019-11-28 21:18:33', '2019-11-28 23:33:49');
INSERT INTO `intent_group` VALUES (3, 'tv', '电视助理', 'system', 1, 1, 0, '2019-11-28 21:18:33', '2019-11-28 23:33:49');
INSERT INTO `intent_group` VALUES (4, 'smartspeaker', '智能音箱', 'system', 1, 1, 0, '2019-11-28 21:18:33', '2019-11-28 23:33:49');
INSERT INTO `intent_group` VALUES (5, 'life', '生活助手', 'system', 1, 1, 0, '2019-11-28 21:18:33', '2019-11-28 23:33:49');
INSERT INTO `intent_group` VALUES (6, 'travel', '旅游出行', 'system', 1, 1, 0, '2019-11-28 21:18:33', '2019-11-28 23:33:49');
INSERT INTO `intent_group` VALUES (7, 'hr', 'HR领域', 'system', 1, 1, 0, '2019-11-28 21:18:33', '2019-11-28 23:33:49');
INSERT INTO `intent_group` VALUES (8, 'mobile', '手机助手', 'system', 1, 1, 0, '2019-11-28 21:18:33', '2019-11-28 23:33:49');
INSERT INTO `intent_group` VALUES (9, 'aiot', '智能家居', 'system', 1, 1, 0, '2019-11-28 21:18:33', '2019-11-28 23:33:49');
INSERT INTO `intent_group` VALUES (10, 'bank', '银行业务', 'system', 1, 1, 0, '2019-11-28 21:18:33', '2019-11-28 23:33:49');
INSERT INTO `intent_group` VALUES (11, 'finance', '财经资讯', 'system', 1, 1, 0, '2019-11-28 21:18:33', '2019-11-28 23:33:49');
INSERT INTO `intent_group` VALUES (12, 'insurance', '保险领域', 'system', 1, 1, 0, '2019-11-28 21:18:33', '2019-11-28 23:33:49');
INSERT INTO `intent_group` VALUES (13, 'news', '新闻领域', 'system', 1, 1, 0, '2019-11-28 21:18:33', '2019-11-28 23:33:49');
INSERT INTO `intent_group` VALUES (14, 'sports', '体育领域', 'system', 1, 1, 0, '2019-11-28 21:18:33', '2019-11-28 23:33:49');
INSERT INTO `intent_group` VALUES (15, 'encyclopedia', '生活百科', 'system', 1, 1, 0, '2019-11-28 21:18:33', '2019-11-28 23:33:49');
INSERT INTO `intent_group` VALUES (16, 'logistics', '物流领域', 'system', 1, 1, 0, '2019-11-28 21:18:33', '2019-11-28 23:33:49');
INSERT INTO `intent_group` VALUES (17, 'generalintent', '通用', 'system', 1, 1, 0, '2019-11-28 21:18:33', '2019-11-28 23:33:49');
UNLOCK TABLES;
COMMIT;

-- ----------------------------
-- init intent group by appId
-- ----------------------------
-- LOCK TABLES `intent_group` READ;
-- LOCK TABLES auth.`apps` READ;
INSERT INTO emotibot.`intent_group` ( `group_id`, `name`, `app_id`, `group_type`, `group_status`, `on_off`, `create_time`, `update_time`)
SELECT `uuid`, '默认组', `uuid`, 2, 0, 1, '2019-11-28 21:18:33', '2019-11-28 23:33:49'
FROM auth.`apps`;
-- UNLOCK TABLES;
-- COMMIT;

-- ----------------------------
-- update table's structure
-- ----------------------------
ALTER TABLE `intents` ADD (
  `group_id` varchar(64) COMMENT '意图组id',
  `description` varchar(256) COMMENT '内置意图描述',
  `inner_intent_id` int(11)  COMMENT '继承的内置意图id');
ALTER TABLE `intents` ADD INDEX(`group_id`);

ALTER TABLE `intent_versions` ADD
  `group_id` varchar(64) COMMENT '意图组id';
ALTER TABLE `intent_versions` ADD INDEX(`group_id`);

ALTER TABLE `intent_test_intents` ADD
  `group_id` varchar(64) COMMENT '意图组id';
ALTER TABLE `intent_test_intents` ADD INDEX(`group_id`);

ALTER TABLE `intent_test_versions` ADD
  `group_id` varchar(64) COMMENT '意图组id';
ALTER TABLE `intent_test_versions` ADD INDEX(`group_id`);

-- ----------------------------
-- update table's data
-- ----------------------------
UPDATE `intents` SET `group_id`=`appid` WHERE `group_id` = '';
UPDATE `intent_versions` SET `group_id`=`appid` WHERE `group_id` = '';
UPDATE `intent_test_intents` SET `group_id`=`app_id` WHERE `group_id` = '';
UPDATE `intent_test_versions` SET `group_id`=`app_id` WHERE `group_id` = '';

-- +migrate Down
UNLOCK TABLES;
DROP TABLE `intent_test_intents`;
DROP TABLE `intent_test_versions`;
DROP TABLE `intents`;
DROP TABLE `intent_versions`;
DROP TABLE `intent_group`;
DROP TABLE `union_intent_quizzes_info`;
DROP TABLE `union_intent_test_result`;
DROP TABLE `union_intent_test_version`;
RENAME TABLE intents_v2 TO intents, intent_versions_v2 TO intent_versions, intent_test_intents_v2 TO intent_test_intents, intent_test_versions_v2 TO intent_test_versions;
