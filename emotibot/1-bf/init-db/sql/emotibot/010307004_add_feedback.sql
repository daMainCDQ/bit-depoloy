-- +migrate Up
CREATE TABLE `feedback_reason` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '不满意原因id',
  `appid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '机器人id',
  `content` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '答案内容',
  `created_at` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间 timestamp',
  PRIMARY KEY (`id`),
  KEY `appid` (`appid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='不满意原因';

-- +migrate Down
DROP TABLE `feedback_reason`;
