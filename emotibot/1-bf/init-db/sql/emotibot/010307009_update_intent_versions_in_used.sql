-- +migrate Up
UPDATE `intent_versions`
SET `in_used` = 1
WHERE `version` IN (
	SELECT t.v
	FROM (
		SELECT MAX(`version`) AS v
		FROM `intent_versions`
		GROUP BY `appid`
	) AS t
);

-- +migrate Down
UPDATE `intent_versions`
SET `in_used` = 0
WHERE `in_used` = 1;
