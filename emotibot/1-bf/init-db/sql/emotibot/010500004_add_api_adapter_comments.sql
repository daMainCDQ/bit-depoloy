-- +migrate Up
ALTER TABLE `adapter_call_log` COMMENT '接口适配器调用日志';
ALTER TABLE `adapter_inner_api_definition` COMMENT '接口适配器内部接口声明';
ALTER TABLE `adapter_outer_api_definition` COMMENT '接口适配器外部接口声明';
ALTER TABLE `adapter_outer_api_mock` COMMENT '接口适配器内部接口mock数据';
ALTER TABLE `adapter_outer_api_mock_condition` COMMENT '接口适配器内部接口mock条件';

-- +migrate Down
