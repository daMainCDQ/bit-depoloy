-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `entities` ADD INDEX `IDX_appid` (`appid`);

-- +migrate Down
ALTER TABLE `entities` DROP INDEX `IDX_appid`;
