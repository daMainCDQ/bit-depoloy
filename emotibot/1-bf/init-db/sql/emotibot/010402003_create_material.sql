-- +migrate Up
CREATE TABLE `material` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_id` varchar(128) NOT NULL COMMENT '机器人id',
  `name` varchar(255) NOT NULL COMMENT '文件名',
  `file_size` int(11) NOT NULL COMMENT '文件大小',
  `upload_user` varchar(128) NOT NULL COMMENT '上传用户名',
  `type` int(11) NOT NULL COMMENT '文件类型',
  `catalog_id` int(11) NOT NULL COMMENT '目录id',
  `hash_code` varchar(128) NOT NULL COMMENT '哈希值',
  `random_code` varchar(128) NOT NULL COMMENT '随机值',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间, 格式为unix_time',
  `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间, 格式为unix_time',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='素材库-素材表';

CREATE TABLE `material_catalog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_id` varchar(128) NOT NULL COMMENT '机器人id',
  `name` varchar(255) NOT NULL COMMENT '目录名',
  `layer` int(11) NOT NULL COMMENT '层级',
  `parent_id` int(11) NOT NULL COMMENT '父目录id',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间, 格式为unix_time',
  `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间, 格式为unix_time',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='素材库-目录表';

CREATE TABLE `material_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_id` varchar(128) NOT NULL COMMENT '机器人id',
  `name` varchar(255) NOT NULL COMMENT '目录名',
  `progress` int(11) NOT NULL COMMENT '进度',
  `material_id` int(11) NOT NULL COMMENT '素材id',
  `upload_user` varchar(128) NOT NULL COMMENT '上传用户名',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间, 格式为unix_time',
  `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间, 格式为unix_time',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='素材库-任务表';
-- +migrate Down
DROP TABLE `material`;
DROP TABLE `material_catalog`;
DROP TABLE `material_task`;