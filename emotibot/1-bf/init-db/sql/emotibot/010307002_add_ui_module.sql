-- +migrate Up
CREATE TABLE `ui_modules` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appid` varchar(36) NOT NULL COMMENT '机器人id',
  `code` char(36) NOT NULL COMMENT '模组 id',
  `url` char(255) NOT NULL COMMENT '模组 UI 位置',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '模组开关',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='UI 模组设置';

-- +migrate Down
DROP TABLE `ui_modules`;
