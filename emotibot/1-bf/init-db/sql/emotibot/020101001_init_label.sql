-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
-- ----------------------------
-- Table structure for label
-- ----------------------------

-- ----------------------------
DROP TABLE IF EXISTS `label`;
CREATE TABLE `label` (
                       `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
                       `name` varchar(100) NOT NULL COMMENT '标签名称，多个层级用","隔开',
                       `label_type` tinyint(2) NOT NULL COMMENT '标签类型，1基础标签，2自定义标签，3行为时间标签，4行为频次标签',
                       `value_type` tinyint(2) NOT NULL COMMENT '值类型，1布尔（枚举）类型，2整形，3浮点，4字符串',
                       `encode` varchar(100) NOT NULL COMMENT '标签编码',
                       `status` tinyint(2) NOT NULL COMMENT '状态  -1已删除 0 训练中 1在线',
                       `is_test_able` tinyint(1) NOT NULL COMMENT '测试开关 0 关闭，1开启',
                       `description` varchar(100) NOT NULL COMMENT '标签简介',
                       `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
                       `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                       PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='标签表';

-- ----------------------------
-- Records of label
-- ----------------------------
BEGIN;
INSERT INTO `label` VALUES (1, '姓名', 1, 4, 'name', 0, 0, '姓名', '2020-02-07 15:11:59', '2020-02-07 15:12:03');
INSERT INTO `label` VALUES (2, '性别', 1, 1, 'sex', 0, 0, '性别', '2020-02-07 16:12:19', '2020-02-07 16:12:19');
INSERT INTO `label` VALUES (3, '年龄', 1, 2, 'age', 0, 0, '年龄', '2020-02-07 17:33:03', '2020-02-07 17:33:03');
INSERT INTO `label` VALUES (4, '身高', 1, 3, 'height', 0, 0, '身高', '2020-02-11 14:35:48', '2020-02-11 14:35:48');
INSERT INTO `label` VALUES (5, '生肖', 1, 1, 'animal', 0, 0, '生肖', '2020-02-11 13:57:10', '2020-02-11 13:57:10');
INSERT INTO `label` VALUES (6, '职业', 1, 4, 'job', 0, 0, '职业', '2020-02-11 14:03:45', '2020-02-11 14:03:45');
INSERT INTO `label` VALUES (7, '星座', 1, 1, 'constellation', 0, 0, '星座', '2020-02-15 09:42:37', '2020-02-15 09:42:37');
INSERT INTO `label` VALUES (8, '公司名', 1, 4, 'company', 0, 0, '公司名', '2020-02-11 14:09:48', '2020-02-11 14:09:48');
COMMIT;

-- ----------------------------
-- Table structure for label_answer_corpus
-- ----------------------------
DROP TABLE IF EXISTS `label_answer_corpus`;
CREATE TABLE `label_answer_corpus` (
                                     `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
                                     `label_value_id` int(11) NOT NULL COMMENT '标签Id',
                                     `is_use_ask` tinyint(2) NOT NULL COMMENT '是否关联上文 0不关联 1 关联',
                                     `name` varchar(255) NOT NULL COMMENT '语料名称',
                                     `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
                                     `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                     PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='下文语料表';

-- ----------------------------
-- Table structure for label_ask_corpus
-- ----------------------------
DROP TABLE IF EXISTS `label_ask_corpus`;
CREATE TABLE `label_ask_corpus` (
                                  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自动递增',
                                  `label_value_id` int(11) DEFAULT NULL COMMENT '标签Id',
                                  `name` varchar(255) DEFAULT NULL COMMENT '语料名称',
                                  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
                                  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='上文语料表';

-- ----------------------------
-- Table structure for label_condition
-- ----------------------------
DROP TABLE IF EXISTS `label_condition`;
CREATE TABLE `label_condition` (
                                 `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
                                 `label_value_id` int(11) NOT NULL COMMENT '标签值Id',
                                 `content` longtext NOT NULL COMMENT '条件json数组',
                                 `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
                                 `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                 PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='自定义标签条件表';

-- ----------------------------
-- Table structure for label_corpus_test
-- ----------------------------
DROP TABLE IF EXISTS `label_corpus_test`;
CREATE TABLE `label_corpus_test` (
                                   `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
                                   `name` varchar(255) NOT NULL COMMENT '语料，如果是多个对话，考虑用json存储（待定）',
                                   `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
                                   `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                   PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='测试语料表';

-- ----------------------------
-- Table structure for label_corpus_test_result
-- ----------------------------
DROP TABLE IF EXISTS `label_corpus_test_result`;
CREATE TABLE `label_corpus_test_result` (
                                          `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
                                          `label_corpus_test_id` int(11) NOT NULL COMMENT '测试语料id',
                                          `lable_result` varchar(0) NOT NULL COMMENT '标签匹配结果，规则是 "标签名-标签值"',
                                          `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
                                          `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                          PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='测试结果表';

-- ----------------------------
-- Table structure for label_model_map
-- ----------------------------
DROP TABLE IF EXISTS `label_model_map`;
CREATE TABLE `label_model_map` (
                                 `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
                                 `label_id` int(11) DEFAULT NULL COMMENT '标签id',
                                 `model_id` varchar(100) DEFAULT NULL COMMENT '类型id',
                                 `is_able` tinyint(1) DEFAULT NULL COMMENT '是否可用',
                                 `status` tinyint(2) DEFAULT NULL COMMENT '训练状态  0 成功 1 进行中',
                                 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for label_parser
-- ----------------------------
DROP TABLE IF EXISTS `label_parser`;
CREATE TABLE `label_parser` (
                              `parser_id` varchar(100) NOT NULL DEFAULT '' COMMENT '解析器ID',
                              `name` varchar(100) NOT NULL DEFAULT '' COMMENT '解析器名称',
                              `type` tinyint(2) unsigned zerofill NOT NULL COMMENT '解析器类型 0 基础解析器  1 行为解析器 2 自定义解析器',
                              `status` tinyint(2) NOT NULL COMMENT '状态 -1不可用 1 可用',
                              PRIMARY KEY (`parser_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for label_parser_map
-- ----------------------------
DROP TABLE IF EXISTS `label_parser_map`;
CREATE TABLE `label_parser_map` (
                                  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
                                  `label_id` int(11) DEFAULT NULL COMMENT '标签id',
                                  `parser_id` varchar(100) DEFAULT NULL COMMENT '解析器id',
                                  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for label_value
-- ----------------------------
DROP TABLE IF EXISTS `label_value`;
CREATE TABLE `label_value` (
                             `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
                             `label_id` int(11) NOT NULL COMMENT '标签Id',
                             `value` varchar(100) NOT NULL COMMENT '标签值',
                             `status` tinyint(2) NOT NULL COMMENT '状态  -1已删除 0 训练中 1在线',
                             `description` varchar(100) NOT NULL COMMENT '标签值描述',
                             `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
                             `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COMMENT='标签值';

-- ----------------------------
-- Records of label_value
-- ----------------------------
BEGIN;
INSERT INTO `label_value` VALUES (1, 2, '1', 1, '男', '2020-02-11 22:17:25', '2020-02-11 22:17:25');
INSERT INTO `label_value` VALUES (2, 2, '2', 1, '女', '2020-02-11 22:17:27', '2020-02-11 22:17:27');
INSERT INTO `label_value` VALUES (3, 2, '3', 1, '未知', '2020-02-11 22:17:31', '2020-02-11 22:17:31');
INSERT INTO `label_value` VALUES (4, 5, '1', 1, '鼠', '2020-02-11 22:19:32', '2020-02-11 22:19:32');
INSERT INTO `label_value` VALUES (5, 5, '2', 1, '牛', '2020-02-11 22:19:36', '2020-02-11 22:19:36');
INSERT INTO `label_value` VALUES (7, 5, '3', 1, '虎', '2020-02-11 22:20:51', '2020-02-11 22:20:51');
INSERT INTO `label_value` VALUES (8, 5, '4', 1, '兔', '2020-02-11 22:20:53', '2020-02-11 22:20:53');
INSERT INTO `label_value` VALUES (9, 5, '5', 1, '龙', '2020-02-11 22:20:55', '2020-02-11 22:20:55');
INSERT INTO `label_value` VALUES (10, 5, '6', 1, '蛇', '2020-02-11 22:20:56', '2020-02-11 22:20:56');
INSERT INTO `label_value` VALUES (11, 5, '7', 1, '马', '2020-02-11 22:20:58', '2020-02-11 22:20:58');
INSERT INTO `label_value` VALUES (12, 5, '8', 1, '羊', '2020-02-11 22:21:00', '2020-02-11 22:21:00');
INSERT INTO `label_value` VALUES (13, 5, '9', 1, '猴', '2020-02-11 22:21:02', '2020-02-11 22:21:02');
INSERT INTO `label_value` VALUES (14, 5, '10', 1, '鸡', '2020-02-11 22:21:05', '2020-02-11 22:21:05');
INSERT INTO `label_value` VALUES (15, 5, '11', 1,  '狗', '2020-02-11 22:21:07', '2020-02-11 22:21:07');
INSERT INTO `label_value` VALUES (16, 5, '12', 1, '猪', '2020-02-11 22:21:09', '2020-02-11 22:21:09');
INSERT INTO `label_value` VALUES (17, 5, '13', 1, '未知', '2020-02-11 22:21:41', '2020-02-11 22:21:41');
INSERT INTO `label_value` VALUES (18, 7, '1', 1, '白羊', '2020-02-11 22:22:22', '2020-02-11 22:22:22');
INSERT INTO `label_value` VALUES (19, 7, '2', 1, '金牛', '2020-02-11 22:22:26', '2020-02-11 22:22:26');
INSERT INTO `label_value` VALUES (20, 7, '3', 1, '双子', '2020-02-11 22:22:34', '2020-02-11 22:22:34');
INSERT INTO `label_value` VALUES (21, 7, '4', 1, '巨蟹', '2020-02-11 22:22:39', '2020-02-11 22:22:39');
INSERT INTO `label_value` VALUES (22, 7, '5', 1, '狮子', '2020-02-11 22:23:59', '2020-02-11 22:23:59');
INSERT INTO `label_value` VALUES (23, 7, '6', 1, '处女', '2020-02-11 22:24:04', '2020-02-11 22:24:04');
INSERT INTO `label_value` VALUES (24, 7, '7', 1, '天秤', '2020-02-11 22:24:09', '2020-02-11 22:24:09');
INSERT INTO `label_value` VALUES (25, 7, '8', 1, '天蝎', '2020-02-11 22:24:14', '2020-02-11 22:24:14');
INSERT INTO `label_value` VALUES (26, 7, '9', 1, '射手', '2020-02-11 22:24:18', '2020-02-11 22:24:18');
INSERT INTO `label_value` VALUES (27, 7, '10', 1, '摩羯', '2020-02-11 22:24:23', '2020-02-11 22:24:23');
INSERT INTO `label_value` VALUES (28, 7, '11', 1, '水瓶', '2020-02-11 22:24:27', '2020-02-11 22:24:27');
INSERT INTO `label_value` VALUES (29, 7, '12', 1, '双鱼', '2020-02-11 22:24:28', '2020-02-11 22:24:28');
INSERT INTO `label_value` VALUES (30, 7, '13', 1, '未知', '2020-02-11 22:26:04', '2020-02-11 22:26:04');
COMMIT;


-- ----------------------------
-- Table structure for label_app_map
-- ----------------------------
DROP TABLE IF EXISTS `label_app_map`;
CREATE TABLE `label_app_map` (
                               `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
                               `label_id` int(11) NOT NULL COMMENT '标签id',
                               `app_id` varchar(200) NOT NULL COMMENT '机器人id',
                               PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

DROP TABLE `label`;
DROP TABLE `label_answer_corpus`;
DROP TABLE `label_ask_corpus`;
DROP TABLE `label_condition`;
DROP TABLE `label_corpus_test`;
DROP TABLE `label_corpus_test_result`;
DROP TABLE `label_value`;
DROP TABLE `label_model_map`;
DROP TABLE `label_parser`;
DROP TABLE `label_parser_map`;
DROP TABLE `label_app_map`;
