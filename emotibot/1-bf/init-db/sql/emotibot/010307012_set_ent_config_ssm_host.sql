-- +migrate Up
UPDATE `ent_config` SET `value` = 'http://faq-module:11015' WHERE `ent_config`.`name` = 'host_ssm';
UPDATE `ent_config` SET `value` = 'http://faq-module:11015' WHERE `ent_config`.`name` = 'host_ssm_mutimodel';
UPDATE `ent_config_appid_customization` SET `value` = 'http://faq-module:11015' WHERE `ent_config_appid_customization`.`name` = 'host_ssm';
-- +migrate Down
UPDATE `ent_config` SET `value` = 'http://faq-module:8080' WHERE `ent_config`.`name` = 'host_ssm';
UPDATE `ent_config` SET `value` = 'http://faq-module:8080' WHERE `ent_config`.`name` = 'host_ssm_mutimodel';
UPDATE `ent_config_appid_customization` SET `value` = 'http://faq-module:8080' WHERE `ent_config_appid_customization`.`name` = 'host_ssm';
