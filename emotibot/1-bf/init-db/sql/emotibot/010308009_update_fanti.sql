-- +migrate Up
INSERT INTO `emotibot`.`ent_config` (`name`, `module`, `value`, `enabled`, `description`, `zhtw`) VALUES ('common-desc-lq-add', 'helper', '新增语料', 1, 'SSM头部', '新增語料');
INSERT INTO `emotibot`.`ent_config` (`name`, `module`, `value`, `enabled`, `description`, `zhtw`) VALUES ('sidebar-desc-train-status', 'helper', '可以训练', 1, 'ssm侧边栏', '可以訓練');
INSERT INTO `emotibot`.`ent_config` (`name`, `module`, `value`, `enabled`, `description`, `zhtw`) VALUES ('sidebar-sq-0', 'helper', '有标准问题的语料为0', 1, 'ssm侧边栏', '有標準問題的語料為0');
INSERT INTO `emotibot`.`ent_config` (`name`, `module`, `value`, `enabled`, `description`, `zhtw`) VALUES ('sidebar-cannot-train', 'helper', '不可训练', 1, 'ssm侧边栏', '不可訓練');
INSERT INTO `emotibot`.`ent_config` (`name`, `module`, `value`, `enabled`, `description`, `zhtw`) VALUES ('sidebar-no-sqlq', 'helper', '需要标准问题和训练语料', 1, 'ssm侧边栏', '需要標準問題和訓練語料');
INSERT INTO `emotibot`.`ent_config` (`name`, `module`, `value`, `enabled`, `description`, `zhtw`) VALUES ('sidebar-sq-notenough', 'helper', '标准问题少于两条', 1, 'ssm侧边栏', '標準問題少於兩條');
INSERT INTO `emotibot`.`ent_config` (`name`, `module`, `value`, `enabled`, `description`, `zhtw`) VALUES ('sidebar-sq-had-changed', 'helper', '请重新训练', 1, 'ssm侧边栏', '請重新訓練');
INSERT INTO `emotibot`.`ent_config` (`name`, `module`, `value`, `enabled`, `description`, `zhtw`) VALUES ('sidebar-had-changes', 'helper', '有数据修改，请重新训练', 1, 'ssm侧边栏', '有數據修改，請重新訓練');
INSERT INTO `emotibot`.`ent_config` (`name`, `module`, `value`, `enabled`, `description`, `zhtw`) VALUES ('sidebar-test-need-train', 'helper', '训练后才可测试', 1, 'ssm侧边栏', '訓練後才可測試');
INSERT INTO `emotibot`.`ent_config` (`name`, `module`, `value`, `enabled`, `description`, `zhtw`) VALUES ('sidebar-cannot-test', 'helper', '不可测试', 1, 'ssm侧边栏', '不可測試');
INSERT INTO `emotibot`.`ent_config` (`name`, `module`, `value`, `enabled`, `zhtw`) VALUES ('sidebar-testing', 'helper', '测试中', 1, '測試中');
INSERT INTO `emotibot`.`ent_config` (`name`, `module`, `value`, `enabled`, `description`) VALUES ('sidebar', 'helper', '尚未测试', 1, 'ssm侧边栏');
UPDATE `emotibot`.`ent_config` SET `zhtw`='尚未測試' WHERE `name`='wordbank-page-wb-relative';
UPDATE `emotibot`.`ent_config` SET `zhtw`='' WHERE `name`='wordbank-page-wb-relative';
UPDATE `emotibot`.`ent_config` SET `name`='sidebar-nerver-test', `zhtw`='尚未測試' WHERE `name`='sidebar';

-- +migrate Down
-- No need