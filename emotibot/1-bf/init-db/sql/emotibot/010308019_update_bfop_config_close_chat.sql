-- +migrate Up
INSERT IGNORE INTO bfop_config (appid, code, module, value, description, update_time, status)
SELECT t1.appid, t2.code, 'controller', 'off', '', '0', '0'  FROM bfop_config t1
LEFT JOIN (
SELECT code, appid FROM bfop_config where appid = '' AND code LIKE 'CHAT-%' AND (value = 'on' OR value = 'off')) t2 
ON 1
WHERE t1.code = 'chat' AND t1.value != 'on';

UPDATE bfop_config AS b1, 
(SELECT appid FROM bfop_config WHERE code = 'chat' AND value != 'on' ) AS b2
SET value = 'off'
WHERE b1.appid = b2.appid AND b1.code LIKE 'CHAT-%' AND b1.value = 'on';

UPDATE bfop_config SET value = 'on'
WHERE code = 'chat';

-- +migrate Down

