-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
-- ----------------------------

-- 更新label表
alter table label modify column status tinyint(2) comment '状态  -1已删除 1在线';
alter table label add column is_need_train tinyint(1) NOT NULL COMMENT '是否需要训练 0不需要 1 需要';

-- 更新label表数据
update label set status = 1 where encode in ('name','sex','animal','job','constellation','company');
update label set status = -1 where encode in ('age','height');
update label set encode = "zodiac" where encode = "animal";
update label set is_test_able = 1;
INSERT INTO `label` (`name`, `label_type`, `value_type`, `encode`, `status`, `is_test_able`, `description`, `create_time`, `update_time`, `is_need_train`)
VALUES ('出生年份', 1, 2, 'birth', 1, 1, '出生年份', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 0);

-- 新增label_model表
DROP TABLE IF EXISTS `label_model`;
CREATE TABLE `label_model` (
                             `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
                             `model_id` varchar(100) DEFAULT NULL COMMENT '类型id',
                             `type` tinyint(1) DEFAULT NULL COMMENT '类型 1上文语料模型  2关联上文的下文语料模型  3不关联上文的下文语料模型 4基础类标签',
                             `status` tinyint(2) DEFAULT NULL COMMENT '训练状态  0 训练中 1 在线',
                             `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
                             `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- 更新 label_model_map 表
alter table label_model_map drop column status;
alter table label_model_map drop column is_able;
alter table label_model_map add column  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间';
alter table label_model_map add column  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间';

-- 更新 label_parser 表
alter table label_parser add column  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间';
alter table label_parser add column  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间';

-- 更新 label_model_map 表
alter table label_parser_map add column  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间';
alter table label_parser_map add column  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间';

-- 更新 label_corpus_test_result 表
alter table label_corpus_test_result change column lable_result label_result varchar(150);

-- 更新 label_corpus_test 表
alter table label_corpus_test add column app_id varchar(200);

CREATE TABLE `label_model_app_map` (
                                 `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
                                 `model_id` varchar(100) DEFAULT NULL COMMENT '类型id',
                                 `app_id` varchar(200) NOT NULL COMMENT '机器人id',
                                 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 comment '用于保存第一组模型id和appid的关联';

-- 更新label_value表
alter table label_value add column encode varchar(100) NOT NULL COMMENT '标签值编码';
update label_value set encode = "man" where description = "男";
update label_value set encode = "woman" where description = "女";
update label_value set encode = "sex_unknow" where description = "未知" and label_id = 2 and value = 3;
update label_value set encode = "mouse" where description = "鼠";
update label_value set encode = "cow" where description = "牛";
update label_value set encode = "tiger" where description = "虎";
update label_value set encode = "rabbit" where description = "兔";
update label_value set encode = "dragon" where description = "龙";
update label_value set encode = "snake" where description = "蛇";
update label_value set encode = "horse" where description = "马";
update label_value set encode = "sheep" where description = "羊";
update label_value set encode = "monkey" where description = "猴";
update label_value set encode = "chicken" where description = "鸡";
update label_value set encode = "dog" where description = "狗";
update label_value set encode = "pig" where description = "猪";
update label_value set encode = "zodiac_unkonw" where description = "未知" and label_id = 5 and value = 13;
update label_value set encode = "aries" where description = "白羊";
update label_value set encode = "taurus" where description = "金牛";
update label_value set encode = "gemini" where description = "双子";
update label_value set encode = "cancer" where description = "巨蟹";
update label_value set encode = "leo" where description = "狮子";
update label_value set encode = "virgo" where description = "处女";
update label_value set encode = "libra" where description = "天秤";
update label_value set encode = "acrab" where description = "天蝎";
update label_value set encode = "sagittarius" where description = "射手";
update label_value set encode = "capricornus" where description = "摩羯";
update label_value set encode = "aquarius" where description = "水瓶";
update label_value set encode = "pisces" where description = "双鱼";
update label_value set encode = "constellation_unknow" where description = "未知" and label_id = 7 and value = 13;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

DROP TABLE `label_model`;
delete from label where encode = 'birth';
