-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
DROP TABLE
IF
	EXISTS emotion_talk_main_info;
CREATE TABLE emotion_talk_main_info (
	id BIGINT auto_increment PRIMARY KEY,
	talk_id VARCHAR ( 40 ) NOT NULL COMMENT '情绪话术uuid',
	talk_name VARCHAR ( 50 ) NOT NULL COMMENT '情绪话术名称',
	talk_switch INT ( 2 ) DEFAULT '0' NOT NULL COMMENT '情绪话术开关——0：表示不开启，1：表示开启',
	talk_type VARCHAR ( 200 ) NOT NULL COMMENT '情绪话术类型',
	user_id VARCHAR ( 40 ) NOT NULL COMMENT '情绪组所属用户Id——appid',
	created_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
	updated_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
	UNIQUE ( talk_id )
) COMMENT '情绪话术';

DROP TABLE
IF
	EXISTS talk_emotion_info;
CREATE TABLE talk_emotion_info (
	id BIGINT auto_increment PRIMARY KEY,
	emotion_name VARCHAR ( 50 ) NOT NULL COMMENT '情绪名称',
	domain_name VARCHAR ( 50 ) NOT NULL COMMENT '场景名称',
	talk_id VARCHAR ( 40 ) NOT NULL COMMENT '情绪话术ID',
	created_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
	updated_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
) COMMENT '情绪话术-情绪';

DROP TABLE
IF
	EXISTS talk_corpus_info;
CREATE TABLE talk_corpus_info (
	id BIGINT auto_increment PRIMARY KEY,
	corpus_content VARCHAR ( 200 ) NOT NULL COMMENT '语料内容',
	talk_id VARCHAR ( 40 ) NOT NULL COMMENT '情绪话术ID',
	created_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
) COMMENT '情绪话术-语料';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `emotion_talk_main_info`;

DROP TABLE `talk_emotion_info`;

DROP TABLE `talk_corpus_info`;