-- +migrate Up
UPDATE audit_record
  SET `operation` = (
    CASE
      WHEN `operation` = '0' THEN 'add'
      WHEN `operation` = '1' THEN 'edit'
      WHEN `operation` = '2' THEN 'delete'
      WHEN `operation` = '3' THEN 'import'
      WHEN `operation` = '4' THEN 'export'
      WHEN `operation` = '6' THEN 'login'
      WHEN `operation` = '7' THEN 'publish'
      WHEN `operation` = '8' THEN 'on'
      WHEN `operation` = '9' THEN 'off'
      ELSE `operation`
    END
  ),
  `module` = (
    CASE
      WHEN `module` = '0' THEN 'robot_chat_skill'
      WHEN `module` = '1' THEN 'robot_function'
      WHEN `module` = '2' THEN 'ssm'
      WHEN `module` = '3' THEN 'robot_profile'
      WHEN `module` = '5' THEN 'wordbank'
      WHEN `module` = '6' THEN 'statistic_analysis'
      WHEN `module` = '7' THEN 'manage_user'
      WHEN `module` = '8' THEN 'manage_user'
      WHEN `module` = '9' THEN 'task_engine'
      WHEN `module` = '10' THEN 'intent_manage'
      ELSE `module`
    END
  );
-- +migrate Down