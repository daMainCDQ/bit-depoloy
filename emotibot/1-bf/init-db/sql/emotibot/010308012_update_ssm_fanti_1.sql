-- +migrate Up
UPDATE `emotibot`.`ent_config` SET `zhtw`='您是否有現成的訓練語料呢？' WHERE `name`='ssm-help-add-train-log-whether-corpus-body1';
UPDATE `emotibot`.`ent_config` SET `zhtw`='現成的語料可以來自用戶日誌，也可以是客戶或者內部員工在excel表格中的手動擴寫，這些都可以通過上傳來批量導入系統中。上傳之前，請先下載範本，按照範本提供的格式整理語料。' WHERE `name`='ssm-help-add-train-log-have-corpus-body1';
UPDATE `emotibot`.`ent_config` SET `zhtw`='如果沒有現成的訓練語料，您可以通過“手動擴寫”的管道逐條添加。 ' WHERE `name`='ssm-help-add-train-log-no-corpus-body1';

-- +migrate Down
-- No need