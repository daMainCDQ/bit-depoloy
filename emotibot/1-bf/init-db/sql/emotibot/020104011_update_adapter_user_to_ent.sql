-- +migrate Up
-- adapter 2.1.4表新增enterpriseId字段：
ALTER TABLE `adapter_script` ADD (`enterprise_id` varchar(64) default NULL COMMENT '所属企业id');
ALTER TABLE `adapter_script_call_log` ADD (`enterprise_id` varchar(64) default NULL COMMENT '所属企业id');
ALTER TABLE `adapter_inner_api_definition` ADD (`enterprise_id` varchar(64) default NULL COMMENT '所属企业id');
ALTER TABLE `adapter_outer_api_definition` ADD (`enterprise_id` varchar(64) default NULL COMMENT '所属企业id');
ALTER TABLE `adapter_call_log` ADD (`enterprise_id` varchar(64) default NULL COMMENT '所属企业id');

UPDATE adapter_script LEFT JOIN (SELECT uuid userId,IFNULL(enterprise,(SELECT uuid FROM auth.enterprises where status = 1 ORDER BY id ASC LIMIT 1)) enterpriseId FROM `auth`.`users` WHERE uuid IN (SELECT user_id FROM adapter_script GROUP BY user_id)) AS t ON t.userId=adapter_script.user_id SET adapter_script.enterprise_id=t.enterpriseId;
UPDATE adapter_script_call_log LEFT JOIN (SELECT uuid userId,IFNULL(enterprise,(SELECT uuid FROM auth.enterprises where status = 1 ORDER BY id ASC LIMIT 1)) enterpriseId FROM `auth`.`users` WHERE uuid IN (SELECT user_id FROM adapter_script_call_log GROUP BY user_id)) AS t ON t.userId=adapter_script_call_log.user_id SET adapter_script_call_log.enterprise_id=t.enterpriseId;
UPDATE adapter_inner_api_definition LEFT JOIN (SELECT uuid userId,IFNULL(enterprise,(SELECT uuid FROM auth.enterprises where status = 1 ORDER BY id ASC LIMIT 1)) enterpriseId FROM `auth`.`users` WHERE uuid IN (SELECT user_id FROM adapter_inner_api_definition GROUP BY user_id)) AS t ON t.userId=adapter_inner_api_definition.user_id SET adapter_inner_api_definition.enterprise_id=t.enterpriseId;
UPDATE adapter_outer_api_definition LEFT JOIN (SELECT uuid userId,IFNULL(enterprise,(SELECT uuid FROM auth.enterprises where status = 1 ORDER BY id ASC LIMIT 1)) enterpriseId FROM `auth`.`users` WHERE uuid IN (SELECT user_id FROM adapter_outer_api_definition GROUP BY user_id)) AS t ON t.userId=adapter_outer_api_definition.user_id SET adapter_outer_api_definition.enterprise_id=t.enterpriseId;
UPDATE adapter_call_log LEFT JOIN (SELECT uuid userId,IFNULL(enterprise,(SELECT uuid FROM auth.enterprises where status = 1 ORDER BY id ASC LIMIT 1)) enterpriseId FROM `auth`.`users` WHERE uuid IN (SELECT user_id FROM adapter_call_log GROUP BY user_id)) AS t ON t.userId=adapter_call_log.user_id SET adapter_call_log.enterprise_id=t.enterpriseId;

-- +migrate Down
alter table `adapter_script` drop column `enterprise_id`;
alter table `adapter_script_call_log` drop column `enterprise_id`;
alter table `adapter_inner_api_definition` drop column `enterprise_id`;
alter table `adapter_outer_api_definition` drop column `enterprise_id`;
alter table `adapter_call_log` drop column `enterprise_id`;