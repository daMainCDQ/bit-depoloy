-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

UPDATE ent_config_appid_customization
SET value = REPLACE(value, '172.17.0.1:8895', 'simple-ft-service:8888')
WHERE name = 'ssm_config';
UPDATE ent_config
SET value = REPLACE(value, '172.17.0.1:8895', 'simple-ft-service:8888')
WHERE name = 'ssm_config';

UPDATE ent_config_appid_customization
SET value = REPLACE(value, '172.17.0.1:15601', 'simple-ft-service:8888')
WHERE name = 'ssm_config';
UPDATE ent_config
SET value = REPLACE(value, '172.17.0.1:15601', 'simple-ft-service:8888')
WHERE name = 'ssm_config';

UPDATE ent_config_appid_customization
SET value = REPLACE(value, 'custom-inference-service:8888', 'simple-ft-service:8888')
WHERE name = 'ssm_config';
UPDATE ent_config
SET value = REPLACE(value, 'custom-inference-service:8888', 'simple-ft-service:8888')
WHERE name = 'ssm_config';

UPDATE ent_config_appid_customization
SET value = REPLACE(value, 'simple-ft-service:8888', 'simple-ft-service:8895')
WHERE name = 'ssm_config';
UPDATE ent_config
SET value = REPLACE(value, 'simple-ft-service:8888', 'simple-ft-service:8895')
WHERE name = 'ssm_config';





UPDATE ent_config_version
SET version_value = CURRENT_TIME;



-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
