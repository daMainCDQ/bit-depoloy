-- +migrate Up
UPDATE bfop_config set value = 'off', status = -1 WHERE code = 'chat-editorial-sport';
UPDATE bfop_config set status = -1 WHERE code = 'chat-editorial-sport-threshold';
-- +migrate Down
