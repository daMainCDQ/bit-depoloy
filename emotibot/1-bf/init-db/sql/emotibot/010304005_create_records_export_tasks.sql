-- +migrate Up

CREATE TABLE IF NOT EXISTS `records_export_tasks` (
  `enterprise_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '企业ID',
  `task_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '日志导出任务ID',
  PRIMARY KEY (`enterprise_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='导出任务工作表';

-- +migrate Down
DROP TABLE IF EXISTS `records_export_tasks`;
