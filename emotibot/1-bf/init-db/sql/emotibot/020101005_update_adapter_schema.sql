-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `adapter_inner_api_definition` MODIFY COLUMN `request_mapping_definition` text NULL COMMENT '请求数据映射声明，存json';
ALTER TABLE `adapter_inner_api_definition` MODIFY COLUMN `response_mapping_definition` text NULL COMMENT '响应数据映射声明，存json'; 

ALTER TABLE `adapter_outer_api_definition` MODIFY COLUMN `request_data_definition` text NULL COMMENT '请求数据的声明，存json';
ALTER TABLE `adapter_outer_api_definition` MODIFY COLUMN `response_data_definition` text NULL COMMENT '响应数据的声明，存json'; 
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
