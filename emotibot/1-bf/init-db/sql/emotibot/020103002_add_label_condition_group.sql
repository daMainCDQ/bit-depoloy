-- +migrate Up

DROP TABLE IF EXISTS `label_condition_group`;
CREATE TABLE `label_condition_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `label_value_id` int(11) DEFAULT NULL COMMENT '标签值Id',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='标签条件组表';

DROP TABLE IF EXISTS `label_condition_item`;
CREATE TABLE `label_condition_item` (
    `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
    `group_id` int(11) DEFAULT NULL COMMENT '条件组ID',
    `label_type` tinyint COMMENT '标签类型',
    `label_id` int(11) COMMENT '标签ID',
    `relation` tinyint COMMENT '键值比较关系,大于(0),小于(1),等于(2),大于等于(3),小于等于(4)',
    `value` varchar(50) COMMENT '条件值',
    `unit` tinyint COMMENT '条件设定单位，年(0),月(1),日(2),时(3),分(4),秒(5)',
    `start_time` timestamp NULL COMMENT '开始时间',
    `end_time` timestamp NULL COMMENT '结束时间',
    `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='标签条件表';

alter table label add column condition_type tinyint COMMENT '用于条件设定中条件选择的类型, 0(有选项文字类:性别/生肖/星座,1(无选项的文字类：公司名/职业/姓名),2(数字类：出生年份))';
update label set condition_type=0 where encode in ('sex','zodiac','constellation');
update label set condition_type=1 where encode in ('name','job','company');
update label set condition_type=2 where encode in ('birth');

alter table label_corpus_test add column predict_type tinyint COMMENT '预测类型，0单条预测，1批量预测';
alter table label_corpus_test add column expected_result varchar(100) COMMENT '预期命中的标签-标签值';
alter table label_corpus_test add column batch_sequence int COMMENT '批量导入的顺序';

-- +migrate Down
DROP TABLE `label_condition_group`;
DROP TABLE `label_condition_item`;
alter table label drop column condition_type;
alter table label_corpus_test drop column predict_type;
alter table label_corpus_test drop column expected_result;
alter table label_corpus_test drop column batch_sequence;