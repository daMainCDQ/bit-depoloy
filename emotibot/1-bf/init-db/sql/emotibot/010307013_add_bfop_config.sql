-- +migrate Up
INSERT IGNORE INTO `bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`) VALUES ('', 'task-engine-web-api-timeout', 'controller', '1000', '任务引擎web api最长等待时间。', '0');
-- +migrate Down
DELETE FROM `bfop_config` WHERE `bfop_config`.`appid` = '' AND `bfop_config`.`code` = 'task-engine-web-api-timeout';
