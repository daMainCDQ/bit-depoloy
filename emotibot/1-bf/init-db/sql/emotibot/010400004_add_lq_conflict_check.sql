-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
UPDATE `bfop_config` SET `value` = '[{\"label\":\"0-5\",\"from\":0,\"to\":5,\"sq_num\":0,\"sq_rate_score\":20,\"sq_rate\":5},{\"label\":\"6-10\",\"from\":6,\"to\":10,\"sq_num\":0,\"sq_rate_score\":40,\"sq_rate\":10},{\"label\":\"11-20\",\"from\":11,\"to\":20,\"sq_num\":0,\"sq_rate_score\":65,\"sq_rate\":25},{\"label\":\"21-30\",\"from\":21,\"to\":30,\"sq_num\":0,\"sq_rate_score\":85,\"sq_rate\":40},{\"label\":\"31+\",\"from\":31,\"to\":0,\"sq_num\":0,\"sq_rate_score\":90,\"sq_rate\":20}]' WHERE `bfop_config`.`appid` = '' AND `bfop_config`.`code` = 'lq_distribution_recommended';

UPDATE `bfop_config` SET `value` = '[\"10\",\"30\"]' WHERE `bfop_config`.`appid` = '' AND `bfop_config`.`code` = 'lq_sq_rate_remark';

INSERT INTO `bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`, `status`) VALUES ('', 'lq_sq_rate_range', 'health_check', '[{\"from\":0,\"to\":5,\"score\":20},{\"from\":5,\"to\":10,\"score\":40},{\"from\":10,\"to\":20,\"score\":65},{\"from\":20,\"to\":30,\"score\":85},{\"from\":30,\"to\":0,\"score\":90}]', '语料健康检查语料占比得分范围', '0', '0');

INSERT INTO `bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`, `status`) VALUES ('', 'lq_conflict_range', 'health_check', '[{\"score\":0,\"from\":0.95},{\"score\":10,\"from\":0.9,\"to\":0.95},{\"score\":20,\"from\":0.75,\"to\":0.9},{\"score\":30,\"from\":0.6,\"to\":0.75},{\"score\":40,\"from\":0.45,\"to\":0.6},{\"score\":50,\"from\":0.3,\"to\":0.4},{\"score\":60,\"from\":0.15,\"to\":0.3},{\"score\":70,\"from\":0.1,\"to\":0.15},{\"score\":80,\"from\":0.05,\"to\":0.1},{\"score\":85,\"from\":0.01,\"to\":0.05},{\"score\":90,\"from\":0.005,\"to\":0.01},{\"score\":95,\"from\":0,\"to\":0.005},{\"score\":100}]', '语料健康检查语料冲突得分范围', '0', '0');

INSERT INTO `bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`, `status`) VALUES ('', 'health_report_score_weight', 'health_check', '{\"lq_conflict_score\":{\"score\":0,\"weight\":0.7},\"lq_sq_rate_score\":{\"score\":0,\"weight\":0.1},\"lq_distribution_score\":{\"score\":0,\"weight\":0.2}}', '语料健康检查报告得分权重', '0', '0');

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
