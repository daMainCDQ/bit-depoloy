-- +migrate Up
-- 意图2.1.4表新增字段：
ALTER TABLE `intents` ADD (`is_negative` int (4) default 0 COMMENT '是否是反例意图');

ALTER TABLE `intent_group` ADD (`is_publish` int (2) default 0 COMMENT '是否发布');

ALTER TABLE `intent_train_sets` ADD (`version` int (2) default 0 COMMENT '版本');

-- +migrate Down
alter table `intents` drop column `is_negative`;
alter table `intent_group` drop column `is_publish`;
alter table `intent_train_sets` drop column `version`;