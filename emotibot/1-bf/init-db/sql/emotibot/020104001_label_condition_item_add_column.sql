-- +migrate Up
-- 标签条件表新增字段：
ALTER TABLE `label_condition_item` ADD (
  `module` varchar(32) COMMENT '模块：通用基础；问答管理；知识图谱；情绪引擎等',
  `sub_type` varchar(100) COMMENT '子分类ID',
  `has_value` tinyint(1) COMMENT '是否有值',
  `is_extract_value` tinyint(1) COMMENT '是否将值存为标签值名称',
  `value_relation` tinyint(1) COMMENT '值关系：0-等于；1-不等于',
  `count` int(11) COMMENT '触发次数');

-- 标签条件表改字段长度
alter table `label_condition_item` modify column `value` varchar(3000) comment '条件值';
ALTER TABLE `label_value` ADD `default_label_value` tinyint COMMENT '是否是默认标签值，1是，0否';

-- +migrate Down
alter table `label_condition_item` drop column `module`;
alter table `label_condition_item` drop column `sub_type`;
alter table `label_condition_item` drop column `has_value`;
alter table `label_condition_item` drop column `is_extract_value`;
alter table `label_condition_item` drop column `value_relation`;
alter table `label_condition_item` drop column `count`;
alter table `label_value` drop column `default_label_value`;
