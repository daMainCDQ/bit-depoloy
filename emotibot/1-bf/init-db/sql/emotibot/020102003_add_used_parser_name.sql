-- +migrate Up

CREATE TABLE `intent_used_parser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `intent_id` int(11) DEFAULT NULL,
  `app_id` varchar(64) DEFAULT NULL,
  `intent_train_sets_id` int(11) DEFAULT '0' COMMENT 'intent_train_sets对应的ID',
  `used_parser_name` varchar(64) DEFAULT NULL COMMENT '用到的抽取器',
  `used_parser_id` varchar(64) DEFAULT NULL,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
	
-- +migrate Down
DROP TABLE `intent_used_parser`;
