-- +migrate Up
DROP TABLE IF EXISTS `alarm_email_info`;
CREATE TABLE `alarm_email_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rec_name` varchar(50) NOT NULL COMMENT '收件人姓名',
  `rec_address` varchar(50) NOT NULL COMMENT '收件人地址',
  `send_switch` int(4) DEFAULT '1' COMMENT '是否发送开关',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '最近更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
-- +migrate Down
DROP TABLE `alarm_email_info`;