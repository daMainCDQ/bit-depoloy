-- +migrate Up
INSERT INTO `integration` (`appid`, `platform`, `pkey`, `pvalue`)
VALUES
  ('', 'workweixin', 'token', ''),
  ('', 'workweixin', 'encoded-aes', ''),
  ('', 'workweixin', 'corpid', ''),
  ('', 'workweixin', 'secret', ''),
  ('', 'workweixin', 'agentid', '');

-- +migrate Down
DELETE FROM `integration` WHERE `platform` = 'workweixin';

