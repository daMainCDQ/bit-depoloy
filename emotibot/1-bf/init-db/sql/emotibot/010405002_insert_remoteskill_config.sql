-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
INSERT IGNORE INTO `bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`) VALUES ('', 'remote-skill', 'controller', 'on', '设置是否要开启远程技能，on: 开启, off: 关闭', '0');
INSERT IGNORE INTO `bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`) VALUES ('', 'remote-skill-threshold', 'controller', '75', '设置远程技能最低信心分数阀值，范围0~100。', '0');

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DELETE FROM `bfop_config` WHERE code='remote-skill';
DELETE FROM `bfop_config` WHERE code='remote-skill-threshold';
