-- +migrate Up
DROP TABLE IF EXISTS `rank_menu`;
CREATE TABLE `rank_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `user_id` varchar(64) NOT NULL DEFAULT '' COMMENT '用户ID',
  `app_id` varchar(64) NOT NULL DEFAULT '' COMMENT '机器人ID',
  `menu_id` int(11) NOT NULL COMMENT '菜单id',
  `menu_name` varchar(64) NOT NULL DEFAULT '' COMMENT '菜单名称',
  `rank_cnt` int(11) NOT NULL DEFAULT 0 COMMENT '菜单点击次数',
  `daily_date` date NOT NULL DEFAULT '2099-01-01' COMMENT '每天时间记录',
  `update_time` timestamp(6) NOT NULL DEFAULT current_timestamp(6) COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY (`user_id`,`app_id`,`menu_id`,`daily_date`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='点击次数菜单表';

-- +migrate Down
DROP TABLE `rank_menu`;
