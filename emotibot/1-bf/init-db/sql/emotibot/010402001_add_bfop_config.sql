-- +migrate Up
INSERT IGNORE INTO `bfop_config`(`code`, `module`, `value`, `description`) VALUES ('faq-recommand-question-count', 'controller', '3', '推荐问题数量');
INSERT IGNORE INTO `bfop_config`(`code`, `module`, `value`, `description`) VALUES ('faq-similar-question-count', 'controller', '3', '近似问题数量');
INSERT IGNORE INTO `bfop_config`(`code`, `module`, `value`, `description`) VALUES ('faq-related-question-count', 'controller', '6', '相关问题数量');
UPDATE      `bfop_config` SET `value` = '25' WHERE `appid` = '' AND `code` = 'faq-recommand-question-range';
UPDATE      `bfop_config` SET `value` = '5' WHERE `appid` = '' AND `code` = 'faq-similar-question-range';
-- +migrate Down
