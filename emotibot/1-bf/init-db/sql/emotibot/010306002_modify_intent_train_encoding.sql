-- +migrate Up
ALTER TABLE `intent_train_sets` CHANGE `sentence` `sentence` VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '';

-- +migrate Down
ALTER TABLE `intent_train_sets` CHANGE `sentence` `sentence` VARCHAR(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '';

