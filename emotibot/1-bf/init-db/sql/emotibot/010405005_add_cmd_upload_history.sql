-- +migrate Up
CREATE TABLE `tbl_upload_cmd_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `user_id` varchar(64) NOT NULL DEFAULT '' COMMENT '用户ID',
  `app_id` varchar(64) NOT NULL DEFAULT '' COMMENT '机器人ID',
  `file_path` varchar(255) DEFAULT '' COMMENT '文件地址',
  `rows` int(11) DEFAULT '0' COMMENT '总行数',
  `valid_rows` int(11) DEFAULT '0' COMMENT '有效行数',
  `finish_rows` int(11) DEFAULT '0' COMMENT '完成行数',
  `comment` varchar(100) NOT NULL DEFAULT '' COMMENT '说明',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user` varchar(64) NOT NULL DEFAULT '' COMMENT '创建用户',
  `file_name` varchar(60) DEFAULT '' COMMENT '上传文件名',
  `upload_type` varchar(2) DEFAULT '' COMMENT '1:增量，2:全量',
  `upload_detail_file_path` varchar(255) DEFAULT '' COMMENT '下载详情文件地址',
  PRIMARY KEY (`id`),
  KEY `idx_app` (`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COMMENT='指令批量上传历史记录表';

-- +migrate Down
DROP TABLE `tbl_upload_cmd_history`;
