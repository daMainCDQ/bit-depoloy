-- +migrate Up
-- 情绪指令表新增字段：
ALTER TABLE `cmd_emotion` ADD `uuid` VARCHAR (100) default NULL COMMENT 'UUID';
-- +migrate Down
alter table `cmd_emotion` drop column `uuid`;