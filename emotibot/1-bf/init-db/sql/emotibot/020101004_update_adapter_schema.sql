-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `adapter_call_log` ADD `inner_api_id` int(11) DEFAULT NULL COMMENT '内部api的id';
CREATE INDEX idx_inner_api_id ON adapter_call_log(inner_api_id);

ALTER TABLE `adapter_inner_api_definition` ADD `request_body_template` text COMMENT 'request body模板' ;
ALTER TABLE `adapter_inner_api_definition` ADD `response_body_template` text COMMENT 'response body模板';
ALTER TABLE `adapter_inner_api_definition` ADD `uuid` varchar(100) DEFAULT NULL COMMENT '后台生成的uuid，用于作为调用接口的唯一标识符';
ALTER TABLE `adapter_inner_api_definition` MODIFY COLUMN `request_mapping_definition` text NOT NULL COMMENT '请求数据映射声明，存json';
ALTER TABLE `adapter_inner_api_definition` MODIFY COLUMN `response_mapping_definition` text NOT NULL COMMENT '响应数据映射声明，存json'; 

ALTER TABLE `adapter_outer_api_definition` ADD `request_body_type` varchar(10) DEFAULT NULL COMMENT '请求体类型，值为json或form' ;
ALTER TABLE `adapter_outer_api_definition` ADD `response_body_type` varchar(10) DEFAULT NULL COMMENT '响应体类型，值为json';
ALTER TABLE `adapter_outer_api_definition` ADD `request_body_schema` text COMMENT '请求体schema';
ALTER TABLE `adapter_outer_api_definition` ADD `response_body_schema` text COMMENT '响应体schema';
ALTER TABLE `adapter_outer_api_definition` MODIFY COLUMN `request_data_definition` text NOT NULL COMMENT '请求数据的声明，存json';
ALTER TABLE `adapter_outer_api_definition` MODIFY COLUMN `response_data_definition` text NOT NULL COMMENT '响应数据的声明，存json'; 

CREATE TABLE `adapter_inner_api_mapping_param` (
`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`param_name` varchar(100) NOT NULL COMMENT '参数名',
`param_location` varchar(10) NOT NULL COMMENT '参数位置，值为header, query, path, form',
`value` varchar(100) DEFAULT NULL COMMENT '值，当前写json path',
`inner_api_id` int(11) DEFAULT NULL COMMENT '内部api，id',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='内部接口映射参数';

CREATE TABLE `adapter_outer_api_request_param` (
`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`param_name` varchar(100) NOT NULL COMMENT '参数名',
`param_location` varchar(10) NOT NULL COMMENT '参数位置，值为header, query, path, form',
`required` tinyint(1) NOT NULL COMMENT '是否必须',
`default_value` varchar(1000) DEFAULT NULL COMMENT '默认值',
`comment` varchar(100) DEFAULT NULL COMMENT '备注',
`outer_api_id` int(11) DEFAULT NULL COMMENT '外部api id',
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='外部接口请求参数';

DROP TABLE `adapter_outer_api_mock`;
DROP TABLE `adapter_outer_api_mock_condition`;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
