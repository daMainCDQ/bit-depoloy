#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pymysql
import os


switch = 'chat-editorial-domain'
to_change_value = 'off'


def turn_chat_switch_off():
    mysqlHost = os.environ.get('INIT_MYSQL_HOST', '172.17.0.1')
    mysqlPort = int(os.environ.get('INIT_MYSQL_PORT', 3306))
    mysqlUser = os.environ.get('INIT_MYSQL_USER', 'root')
    mysqlPassword = os.environ.get('INIT_MYSQL_PASSWORD', 'password')
    auth_db = pymysql.connect(mysqlHost, mysqlUser, mysqlPassword, 'auth', mysqlPort)
    emotibot_db = pymysql.connect(mysqlHost, mysqlUser, mysqlPassword, 'emotibot', mysqlPort)

    sql = "select uuid from apps"
    cursor = auth_db.cursor()
    try:
        cursor.execute(sql)
        rows = cursor.fetchall()
        for row in rows:
            if row[0]:
                _generate(row[0], switch, emotibot_db)
        emotibot_db.commit()
    except Exception as e:
        emotibot_db.rollback()
        print(e)
        return False
    finally:
        cursor.close()

    return True

update_sql_template = "update bfop_config set value = 'off' where appid = '{}' and module = 'controller' and code='{}';"


def _generate(robot_id, chat_switch, emotibot_db):
    sql = "select count(1) from bfop_config where module='controller' and appid='{}' and code='{}'".format(robot_id, chat_switch)
    cursor = emotibot_db.cursor()
    try:
        cursor.execute(sql)
        count = cursor.fetchone()[0]
        if count == 1:
            update_sql = update_sql_template.format(robot_id, switch)
            cursor.execute(update_sql)
    except Exception as e:
        emotibot_db.rollback()
        print(e)
        return False
    finally:
        cursor.close()
    return True

if __name__ == '__main__':
    turn_chat_switch_off()
