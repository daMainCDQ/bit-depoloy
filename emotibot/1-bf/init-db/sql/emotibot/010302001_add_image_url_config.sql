-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
INSERT INTO `ent_config` (`name`, `module`, `value`, `description`) VALUES ('uploadimg_server', 'user', ' ', '上传图片服务器');
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DELETE FROM `ent_config` WHERE `name` = 'uploadimg_server';
