-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

CREATE TABLE `ie_load_map` (
  `_id` varchar(24) NOT NULL DEFAULT '' COMMENT 'id',
  `model_id` varchar(24) NOT NULL DEFAULT '' COMMENT 'model存在minio中的檔名',
  `app_id` varchar(100) NOT NULL DEFAULT '' COMMENT 'app_id',
  `queue_id` varchar(24) NOT NULL DEFAULT '' COMMENT 'app_id所使用的model對應到queue table中的ID',
  `model_version` varchar(30) DEFAULT NULL COMMENT '對應model是否為最新版使用',
  PRIMARY KEY (`_id`),
  KEY `app_id` (`app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT 'load map代表在service中最終用的model';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `ie_load_map`;
