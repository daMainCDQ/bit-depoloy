-- +migrate Up
ALTER TABLE `intent_train_sets` MODIFY `sentence` VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

-- +migrate Down
ALTER TABLE `intent_train_sets` MODIFY `sentence` VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
