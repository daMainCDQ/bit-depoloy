-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
-- ----------------------------
-- Table structure for skill_library
-- ----------------------------
DROP TABLE IF EXISTS `skill_library`;
CREATE TABLE `skill_library` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `category` int(11) NOT NULL COMMENT '技能分类id',
  `category_name` varchar(255) NOT NULL COMMENT '技能分类名称',
  `create_time` bigint(20) DEFAULT NULL COMMENT '数据库记录创建时间',
  `demo` text COMMENT '技能调用示例',
  `description` varchar(255) NOT NULL COMMENT '技能说明',
  `icon` text COMMENT '技能图标base64',
  `name` varchar(255) NOT NULL COMMENT '技能名称',
  `release_time` bigint(20) NOT NULL COMMENT '技能发布时间',
  `skilluuid` int(11) NOT NULL COMMENT '技能唯一标识',
  `update_time` bigint(20) DEFAULT NULL COMMENT '数据库记录更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_qj45vgmn9ees7yi209lkrlcqv` (`skilluuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci COMMENT='远程技能总表';

-- ----------------------------
-- Table structure for robot_skill
-- ----------------------------
DROP TABLE IF EXISTS `robot_skill`;
CREATE TABLE `robot_skill` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `app_id` varchar(255) DEFAULT NULL COMMENT '机器人id',
  `create_time` bigint(20) DEFAULT NULL COMMENT '数据库记录创建时间',
  `is_enabled` bit(1) DEFAULT NULL COMMENT '对应机器人该技能是否打开',
  `update_time` bigint(20) DEFAULT NULL COMMENT '数据库记录更新时间',
  `skill_id` bigint(20) DEFAULT NULL COMMENT '远程技能库外键id',
  PRIMARY KEY (`id`),
  KEY `idx_appId_skill` (`app_id`,`skill_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci COMMENT='机器人技能关联表';


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `robot_skill`;
DROP TABLE `skill_library`;
