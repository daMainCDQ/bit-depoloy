-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- 情绪引擎业务数据结构 --
drop table if exists emotibot.emotion_group_info;
create table emotibot.emotion_group_info
(
  id                bigint auto_increment
    primary key,
  group_id     varchar(40)         not null         comment '情绪组uuid',
  group_name   varchar(50)         not null         comment '情绪组名称',
  description  varchar(200)     default '暂无 '   null    comment '情绪组功能描述',
  group_switch       int(2) default '0'    not null       comment '是否开启——0：表示不开启，1：表示开启',
  user_id           varchar(40)      not null       comment '情绪组所属用户Id——appid',
  created_time      timestamp default CURRENT_TIMESTAMP not null,
  updated_time      timestamp default CURRENT_TIMESTAMP not null,
  constraint emotion_info_group_id_uindex
  unique (group_id)
)
  comment '情绪组';
create index emotion_group_info_group_id_user_id_index
    on emotibot.emotion_group_info (group_id, user_id);


drop table if exists emotibot.emotion_group_map_classifier;
create table emotibot.emotion_group_map_classifier
(
	id          bigint auto_increment    primary key,
    group_id  varchar(40)     not null    comment '情绪组id',
	classifier_id     varchar(40)     not null    comment 'emotibot分类器id',
	user_id           varchar(40)     not null    comment '情绪组所属用户Id——appid',
	created_time timestamp default CURRENT_TIMESTAMP not null,
	updated_time timestamp default CURRENT_TIMESTAMP not null
)
comment '情绪组与emotibot分类器关系表';
create index emotion_group_map_classifier_index
    on emotibot.emotion_group_map_classifier (group_id, user_id);


drop table if exists emotibot.emotion_group_test_data;
create table emotibot.emotion_group_test_data
(
	id             bigint auto_increment
        primary key,
    user_id        varchar(32)                            not null comment '关联用户的id',
    group_id      varchar(32)                            not null comment '情绪组的id',
    content        varchar(255)                           not null comment '测试题内容',
    expect_answer  varchar(255)                           not null comment '预期自定义情绪',
    actual_answer  varchar(255) default ''                not null comment '实际答案',
    compare_result int(2)       default 0                 not null comment '0 表示未测试，1 表示符合预期，-1 表示不符合预期',
	created_time timestamp default CURRENT_TIMESTAMP not null,
	updated_time timestamp default CURRENT_TIMESTAMP not null
)
comment 'emotibot分类器训练数据表';
create index emotion_group_test_data_index
    on emotibot.emotion_group_test_data (user_id, group_id);

-- 通用分类器数据结构 --
drop table if exists emotibot.emotibot_classifier;
create table emotibot.emotibot_classifier
(
	id                  bigint auto_increment            primary key,
    classifier_id       varchar(40) not null comment '分类器id',
	classifier_domain   int(2)      not null comment '分类器适用领域 0:表示意图, 1:表示情绪',
	classifier_desc     varchar(200)     default '暂无 '   null    comment '分类器描述',
	classifier_status   int(2)      not null comment '分类器状态  0：表示待训练， 1：表示训练完成， 2：表示训练中， 3：表示训练失败',
	user_id             varchar(40) not null comment '分类器所属用户Id——appid',
	model_id            varchar(40)     null comment '分类器算法模型Id',
	created_time timestamp default CURRENT_TIMESTAMP not null,
	updated_time timestamp default CURRENT_TIMESTAMP not null,
	constraint emotibot_classifier_id_uindex
    unique (classifier_id)
)
comment 'emotibot分类器主信息表';
create index emotibot_classifier_index
    on emotibot.emotibot_classifier (classifier_id, user_id);

drop table if exists emotibot.emotibot_classifier_tag;
create table emotibot.emotibot_classifier_tag
(
	id                  bigint auto_increment            primary key,
	tag_id              varchar(40) not null comment '分类器标签id',
	tag_name        varchar(50) not null comment '分类器标签名称：用于展示',
	logic_name          varchar(50) not null comment '分类器标签名称：用于计算，默认与display_name一致',
	tag_desc            varchar(200)     default '暂无 '   null    comment '标签描述',
	use_sys_classifier   int(2)      default '0' not null comment '是否关联系统分类器 0：表示不是，1：表示是',
    classifier_id       varchar(40) not null comment '分类器标签所属分类器id',
	user_id             varchar(40) not null comment '分类器所属用户Id——appid',
	created_time timestamp default CURRENT_TIMESTAMP not null,
	updated_time timestamp default CURRENT_TIMESTAMP not null,
	constraint emotibot_classifier_tag_id_uindex
    unique (tag_id)
)
comment 'emotibot分类器标签信息表';
create index emotibot_classifier_tag_index
    on emotibot.emotibot_classifier_tag (tag_id, classifier_id, user_id);

drop table if exists emotibot.tag_map_sys_classifier;
create table emotibot.tag_map_sys_classifier
(
	id                      bigint auto_increment            primary key,
	tag_id                  varchar(40) not null comment '分类器标签id',
	sys_classifier_id       varchar(40) not null comment '系统分类器id',
	sys_classifier_name     varchar(50) not null comment '系统分类器名称',
	description             varchar(200) default '暂无 '  null     comment '系统情绪功能描述',
	classifier_id       varchar(40) not null comment '分类器标签所属分类器id',
	user_id             varchar(40) not null comment '分类器所属用户Id——appid',
	created_time timestamp default CURRENT_TIMESTAMP not null,
	updated_time timestamp default CURRENT_TIMESTAMP not null
)
comment 'emotibot分类器标签与系统分类器对应关系表';
create index tag_map_sys_index
    on emotibot.tag_map_sys_classifier (tag_id, sys_classifier_id, classifier_id, user_id);

drop table if exists emotibot.emotibot_classifier_train_data;
create table emotibot.emotibot_classifier_train_data
(
	id                      bigint auto_increment            primary key,
	td_content              varchar(250)    not null comment '数据内容',
	pro_con_type            int(2)          default '0' not null comment '正反例 0：表示正例，1：表示反例',
    data_type               int(3)          default '0' not null comment '数据领域类型 0：表示文本，1：表示语音，2：表示图像，3：表示视频',
	tag_id                  varchar(40)     not null comment '训练数据所属分类器标签id',
	classifier_id           varchar(40)     not null comment '训练数据所属分类器id',
	user_id                 varchar(40)     not null comment '训练数据所属用户Id——appid',
	created_time timestamp default CURRENT_TIMESTAMP not null,
	updated_time timestamp default CURRENT_TIMESTAMP not null
)
comment 'emotibot分类器训练数据表';
create index emotibot_classifier_train_data_index
    on emotibot.emotibot_classifier_train_data (tag_id, classifier_id, user_id);

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

drop table emotibot.emotion_group_info;
drop table emotibot.emotion_group_map_classifier;
drop table emotibot.emotion_group_test_data;
drop table emotibot.emotibot_classifier;
drop table emotibot.emotibot_classifier_tag;
drop table emotibot.tag_map_sys_classifier;
drop table emotibot.emotibot_classifier_train_data;







