-- +migrate Up
CREATE TABLE `adapter_outer_api_definition` (
  `id` int NOT NULL AUTO_INCREMENT,
  `api_name` varchar(100) NOT NULL COMMENT '外部api名称',
  `api_url` varchar(1000) NOT NULL COMMENT '外部API url',
  `method` varchar(10) NOT NULL COMMENT '请求方法，GET POST等',
  `request_data_definition` text NOT NULL COMMENT '请求数据的声明，存json',
  `response_data_definition` text NOT NULL COMMENT '响应数据的声明，存json',
  `is_mock` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否开启mock',
  `definition_version` varchar(10) NOT NULL DEFAULT '1' COMMENT '声明信息版本',
  `timeout` int NOT NULL COMMENT '超时时间，单位ms',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `adapter_outer_api_mock` (
  `id` int NOT NULL AUTO_INCREMENT,
  `mock_data` text NOT NULL COMMENT 'mock时返回的数据',
  `enabled` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否启用该数据',
  `order` int NOT NULL COMMENT '排序',
  `condition_type` varchar(20) NOT NULL DEFAULT 'and' COMMENT '比较条件，and，or等',
  `outer_api_id` int NOT NULL COMMENT 'outer_api_definition 主键',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `adapter_outer_api_mock_condition` (
  `id` int NOT NULL AUTO_INCREMENT,
  `param` varchar(50) NOT NULL COMMENT '参数名，可用json的取值表达式',
  `compare` varchar(5) NOT NULL COMMENT '比较符',
  `value` varchar(1000) NOT NULL COMMENT '比较的值',
  `mock_id` int NOT NULL COMMENT 'outer_api_mock 主键',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `adapter_inner_api_definition` (
  `id` int NOT NULL AUTO_INCREMENT,
  `api_adapter_name` varchar(100) NOT NULL COMMENT 'api适配器名称',
  `api_type` varchar(20) NOT NULL COMMENT 'api类型，暂时为te或common',
  `request_mapping_definition` text NOT NULL COMMENT '请求数据映射声明，存json',
  `response_mapping_definition` text NOT NULL COMMENT '响应数据映射声明，存json',
  `outer_api_id` int NOT NULL COMMENT 'outer_api_definition 主键',
  `definition_version` varchar(10) NOT NULL DEFAULT '1' COMMENT '描述信息版本',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime NULL,
  `url` varchar(100) NULL COMMENT '请求url',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `adapter_call_log` (
  `id` int NOT NULL AUTO_INCREMENT,
  `unique_id` varchar(32) NULL COMMENT 'BFOP请求时携带的UUID',
  `inner_api_url` varchar(200) NOT NULL COMMENT '内部接口请求url',
  `log_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '记录日志的时间',
  `response_duration` int NOT NULL COMMENT '响应时间，单位毫秒',
  `http_response_duration` int NULL COMMENT '外部api http请求响应时间，单位毫秒',
  `is_mock` tinyint(1) NOT NULL COMMENT '是否返回了mock数据',
  `raw_request_data` text NULL COMMENT '内部api接收到的请求数据',
  `final_response_data` text NULL COMMENT '内部api最终返回的响应数据',
  `api_request_data` text NULL COMMENT '外部api接收到的请求数据',
  `api_response_data` text NULL COMMENT '外部api的响应数据',
  `result_code` varchar(4) NOT NULL COMMENT '调用结果状态码',
  `result_msg` varchar(1000) NULL COMMENT '调用结果描述信息',
  `api_detail` text NULL COMMENT 'api配置的详情信息，debug用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


create index idx_api_name on adapter_outer_api_definition(api_name);
create index idx_order on adapter_outer_api_mock(`order`);
create index idx_api_adapter_name on adapter_inner_api_definition(api_adapter_name);
create index idx_outer_api_id on adapter_inner_api_definition(outer_api_id);
create index idx_log_time on adapter_call_log(log_time);
create index idx_unique_id on adapter_call_log(unique_id);

-- +migrate Down
drop index idx_api_name on adapter_outer_api_definition;
drop index idx_order on adapter_outer_api_mock;
drop index idx_api_adapter_name on adapter_inner_api_definition;
drop index idx_outer_api_id on adapter_inner_api_definition;
drop index idx_log_time on adapter_call_log;

DROP TABLE `adapter_outer_api_definition`;
DROP TABLE `adapter_outer_api_mock`;
DROP TABLE `adapter_outer_api_mock_condition`;
DROP TABLE `adapter_inner_api_definition`;
DROP TABLE `adapter_call_log`;
