-- +migrate Up
INSERT IGNORE INTO `emotibot`.`bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`) VALUES ('', 'task-engine-total-timeout', 'controller', '1000', '任务引擎timeout', '0');
INSERT IGNORE INTO `emotibot`.`bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`) VALUES ('', 'robot-name', 'controller', '我', '机器人自称', '0');
-- +migrate Down
