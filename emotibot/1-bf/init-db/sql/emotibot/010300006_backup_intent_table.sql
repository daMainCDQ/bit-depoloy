-- +migrate Up
RENAME TABLE intent_train_sets TO intent_train_sets_v1, intent_versions TO intent_versions_v1, intents TO intents_v1;
-- +migrate Down
RENAME TABLE intent_train_sets_v1 TO intent_train_sets, intent_versions_v1 TO intent_versions, intents_v1 TO intents;
