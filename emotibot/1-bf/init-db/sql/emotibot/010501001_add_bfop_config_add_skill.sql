-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
INSERT INTO `bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`) VALUES('', 'skill', 'controller', 'on', '设置是否要开启自定义技能，on: 开启, off: 关闭', '0');
INSERT INTO `bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`) VALUES('', 'skill-threshold', 'controller', '90', '设置自定义技能意图最低信心分数阀值，范围0~100。', '0');
-- +migrate Down
