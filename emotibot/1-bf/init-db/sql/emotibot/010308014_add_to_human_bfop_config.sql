-- +migrate Up
INSERT IGNORE INTO `bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`, `status`) VALUES ('', 'to-human', 'controller', 'off', '转人工开关。', '0', '0');

INSERT IGNORE INTO `bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`, `status`) VALUES ('', 'to-human-intent', 'controller', 'off', '意图转人工开关。', '0', '0');

INSERT IGNORE INTO `bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`, `status`) VALUES ('', 'to-human-backfill', 'controller', 'off', '未知问题转人工开关。', '0', '0');

INSERT IGNORE INTO `bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`, `status`) VALUES ('', 'to-human-backfill-cnt', 'controller', '3', '未知问题转人工次数。', '0', '0');

INSERT IGNORE INTO `bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`, `status`) VALUES ('', 'to-human-emotion', 'controller', 'off', '情绪转人工开关。', '0', '0');

INSERT IGNORE INTO `bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`, `status`) VALUES ('', 'to-human-keyword', 'controller', 'off', '关键词转人工开关。', '0', '0');

INSERT IGNORE INTO `bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`, `status`) VALUES ('', 'to-human-faq-repeat-q', 'controller', 'off', '多次匹配同一标准问转人工开关。', '0', '0');

INSERT IGNORE INTO `bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`, `status`) VALUES ('', 'to-human-faq-repeat-q-cnt', 'controller', '3', '多次匹配同一标准问转人工次数。', '0', '0');

INSERT IGNORE INTO `bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`, `status`) VALUES ('', 'to-human-faq-label', 'controller', 'off', '标准问转人工开关。', '0', '0');

INSERT IGNORE INTO `bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`, `status`) VALUES ('', 'to-human-work', 'controller', 'on', '人工客服上班日开关', '0', '0');

INSERT IGNORE INTO `bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`, `status`) VALUES ('', 'to-human-begin-time', 'controller', '00:00', '人工客服上班时间', '0', '0');

INSERT IGNORE INTO `bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`, `status`) VALUES ('', 'to-human-end-time', 'controller', '23:59', '人工客服下班时间', '0', '0');

UPDATE `bfop_config` SET `value` = 'off' WHERE `bfop_config`.`appid` = '' AND `bfop_config`.`code` = 'human-intent';


-- +migrate Down
