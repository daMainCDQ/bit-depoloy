-- +migrate Up
DROP TABLE IF EXISTS `detect_freq`;
CREATE TABLE `detect_freq` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `freq_name` varchar(50) NOT NULL COMMENT '检测项',
  `detect_time` int(11) NOT NULL COMMENT '检测频率，单位分钟',
  `switch` int(4) NOT NULL DEFAULT '1' COMMENT '是否触发报警',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
-- +migrate Down
DROP TABLE `detect_freq`;