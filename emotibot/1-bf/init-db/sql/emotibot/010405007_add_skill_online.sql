-- +migrate Up
ALTER TABLE `emotibot`.`skill_library` ADD COLUMN `online` TINYINT(1) NULL DEFAULT NULL COMMENT '该技能是否已上线' AFTER `skilluuid`;

-- +migrate Down
ALTER TABLE `emotibot`.`skill_library` DROP COLUMN `online`;
