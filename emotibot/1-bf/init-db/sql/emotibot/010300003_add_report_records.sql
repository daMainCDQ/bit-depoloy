-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE `report_records` (
    `id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
    `report_id` bigint(11) UNSIGNED NOT NULL COMMENT '分類紀錄所屬的聚類工作ID',
    `cluster_id` bigint(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '聚類日誌所屬分群ID, 若無分群則為-1, 因為policy關係不設為NULL',
    `chat_record_id` varchar(255) NOT NULL COMMENT '對話日誌所屬ID',
    `content` TEXT NOT NULL COMMENT '日誌的分類內容(使用者問)',
    `created_time` bigint(11) UNSIGNED NOT NULL COMMENT '建立時間',
    `is_central_node` boolean DEFAULT false NOT NULL COMMENT '是否為中心節點之一',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='聚類節點';
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `report_records`;