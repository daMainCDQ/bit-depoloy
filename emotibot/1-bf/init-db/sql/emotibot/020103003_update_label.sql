-- +migrate Up

DROP TABLE IF EXISTS `label_batch_predict_status`;
CREATE TABLE `label_batch_predict_status` (
      `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
      `status` tinyint COMMENT '1预测中，0预测完成',
      `app_id` varchar(200) NOT NULL COMMENT '机器人id',
      PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 comment '批量预测状态';

-- +migrate Down
DROP TABLE `label_batch_predict_status`;
