-- +migrate Up
SET AUTOCOMMIT=0;
INSERT INTO `intent_train_sets` (`id`, `sentence`, `intent`, `type`)
SELECT `intent_train_set_id`, `sentence`, `intent_id`, `intent_version_id`
FROM `intent_train_sets_v1`;

INSERT INTO `intent_versions` (`version`, `appid`, `ie_model_id`, `re_model_id`, `in_used`, `commit_time`, `start_train`, `end_train`, `sentence_count`, `result`)
SELECT `intent_version_id`, `app_id`, `ie_model_id`, `re_model_id`, `in_used`, CAST(UNIX_TIMESTAMP(`created_time`) AS UNSIGNED), CAST(UNIX_TIMESTAMP(`created_time`) AS UNSIGNED), CAST(UNIX_TIMESTAMP(`created_time`) AS UNSIGNED), 0, 1
FROM `intent_versions_v1`;

INSERT INTO `intents` (`id`, `appid`, `name`, `version`, `updatetime`)
SELECT `intent_id`, `app_id`, `name`, `intent_version_id`, 0
FROM `intents_v1`;

INSERT INTO `intents` (`appid`, `name`, `version`, `updatetime`)
SELECT `intents_v1`.`app_id`, `intents_v1`.`name`, NULL, CAST(UNIX_TIMESTAMP() as UNSIGNED) FROM `intents_v1`, (
SELECT max(`intent_version_id`) as `latest`, `app_id` FROM `intents_v1` GROUP BY `app_id`
) as `max` WHERE `intents_v1`.`intent_version_id` = `max`.`latest` AND `intents_v1`.`app_id` = `max`.`app_id`;

-- insert train sets of intents not commited
INSERT INTO `intent_train_sets` (`sentence`, `intent`, `type`)
SELECT `sets`.`sentence`, `id_mapping`.`new_id`, `sets`.`type` FROM `intent_train_sets` as `sets`,
(
  -- get intents inserted mapping from last INSERT
  SELECT `new_intent`.`id` as `new_id`, `old_intent`.`id` as `old_id` FROM
    -- get intents not commited
    (SELECT `id`, `name`, `appid` FROM `intents` WHERE `version` is NULL) as `new_intent`,
    (SELECT `intents_v1`.`intent_id` as `id`, `intents_v1`.`name` as name, `intents_v1`.`app_id` as `appid` FROM `intents_v1`, (
    -- get latest version id
      SELECT max(`intent_version_id`) as `latest`, `app_id` FROM `intents_v1` GROUP BY `app_id`
    ) as `max` WHERE `intents_v1`.`intent_version_id` = `max`.`latest` AND `intents_v1`.`app_id` = `max`.`app_id`) as `old_intent`
  WHERE `new_intent`.`appid` = `old_intent`.`appid` AND `new_intent`.`name` = `old_intent`.`name`
) as `id_mapping` WHERE `sets`.`intent` = `id_mapping`.`old_id`;

SET AUTOCOMMIT=1;
-- +migrate Down
TRUNCATE TABLE `intent_train_sets`;
TRUNCATE TABLE `intent_versions`;
TRUNCATE TABLE `intents`;