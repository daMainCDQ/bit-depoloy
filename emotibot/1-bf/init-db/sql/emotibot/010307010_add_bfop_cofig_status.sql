-- +migrate Up
ALTER TABLE `bfop_config` ADD `status` INT NOT NULL DEFAULT '0' COMMENT '状态, 0代表正常' AFTER `update_time`, ADD INDEX `status_index` (`status`);

-- +migrate Down
ALTER TABLE `bfop_config` DROP `status`;
