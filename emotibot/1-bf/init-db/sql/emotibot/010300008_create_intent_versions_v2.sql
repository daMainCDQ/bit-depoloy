-- +migrate Up
CREATE TABLE `intent_versions` (
  `version` int(11) NOT NULL AUTO_INCREMENT,
  `appid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `ie_model_id` char(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `re_model_id` char(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `in_used` tinyint(1) NOT NULL DEFAULT '0',
  `commit_time` int(20) NOT NULL,
  `start_train` int(20) DEFAULT NULL,
  `end_train` int(20) DEFAULT NULL,
  `sentence_count` int(20) NOT NULL DEFAULT '0',
  `result` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
-- +migrate Down
DROP TABLE `intent_versions`;