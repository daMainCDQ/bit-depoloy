-- +migrate Up
CREATE TABLE `intent_test_intents` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '测试意图 ID',
  `app_id` char(64) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '机器人 ID',
  `intent_name` varchar(256) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '意图名称，若为 NULL，代表为负类测试意图',
  `version` int(11) DEFAULT NULL COMMENT '对应的测试版本，若为 NULL，代表为目前正在编辑中的测试集意图',
  `updated_time` int(11) NOT NULL COMMENT '意图最后被测试/更新的时间',
  PRIMARY KEY (`id`),
  KEY `app_id` (`app_id`),
  KEY `intent_name` (`intent_name`),
  KEY `version` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='測試意圖';

CREATE TABLE `intent_test_sentences` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '测试语句 ID',
  `sentence` varchar(256) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '测试语句',
  `test_intent` int(11) NOT NULL COMMENT '所属测试意图 ID',
  `result` tinyint(1) NOT NULL DEFAULT '0' COMMENT '测试结果；0:未测试、1:正确、2:错误',
  `score` int(11) DEFAULT NULL COMMENT '意图分数',
  `answer` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '机器人推测意图',
  PRIMARY KEY (`id`),
  KEY `test_intent` (`test_intent`),
  KEY `result` (`result`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='測試語句';

CREATE TABLE `intent_test_versions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '测试版本',
  `app_id` char(64) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '机器人 ID',
  `name` varchar(128) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '测试集名称',
  `tester` char(32) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '发起测试者 UUID',
  `ie_model_id` char(64) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '测试意图时所使用的意图模型 ID',
  `intents_count` int(11) NOT NULL COMMENT '测试意图数',
  `sentences_count` int(11) NOT NULL COMMENT '测试语句数',
  `true_positives` int(11) NOT NULL DEFAULT '0' COMMENT '真阳个数 (测试语料意图与所指定的意图相符)',
  `false_positives` int(11) NOT NULL DEFAULT '0' COMMENT '伪阳个数 (测试语料意图与所指定的意图不相符)',
  `true_negatives` int(11) NOT NULL DEFAULT '0' COMMENT '真阴个数 (负类测试语料不符合任意的意图)',
  `false_negatives` int(11) NOT NULL DEFAULT '0' COMMENT '伪阴个数 (负类测试语料符合了任意的意图)',
  `in_used` tinyint(1) NOT NULL DEFAULT '0' COMMENT '测试集是否正在使用中',
  `start_time` int(11) NOT NULL COMMENT '测试开始时间',
  `end_time` int(11) DEFAULT NULL COMMENT '测试结束时间',
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '测试状态；0:測試任務進行中、1:测试完成、2:测试失败',
  `progress` int(11) NOT NULL DEFAULT '0' COMMENT '已完成测试语句数',
  `saved` tinyint(1) NOT NULL DEFAULT '0' COMMENT '意图测试集是否被保存',
  `message` text COMMENT '测试任务相关讯息，包含错误讯息... etc',
  PRIMARY KEY (`id`),
  KEY `app_id` (`app_id`),
  KEY `ie_model_id` (`ie_model_id`),
  KEY `in_used` (`in_used`),
  KEY `status` (`status`),
  KEY `saved` (`saved`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='測試版本';

-- +migrate Down
DROP TABLE `intent_test_intents`;
DROP TABLE `intent_test_sentences`;
DROP TABLE `intent_test_versions`;
