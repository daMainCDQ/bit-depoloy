-- +migrate Up
CREATE TABLE `integration` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appid` varchar(36) NOT NULL COMMENT '机器人id',
  `platform` char(32) NOT NULL DEFAULT '' COMMENT '整合平台',
  `pkey` char(32) NOT NULL DEFAULT '' COMMENT '整合平台对应 key',
  `pvalue` char(255) NOT NULL DEFAULT '' COMMENT '整合平台对应 key 的值',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='整合设定';

-- +migrate Down
DROP TABLE `integration`;

