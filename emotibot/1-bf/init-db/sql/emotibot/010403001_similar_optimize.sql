-- +migrate Up
-- 先改编码, 不然不能join
ALTER TABLE `robot_words` CHANGE `appid` `appid` CHAR(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '机器人id';

ALTER TABLE `robot_words` CHANGE `content` `content` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '话述内容';

ALTER TABLE `bfop_config` CHANGE `appid` `appid` VARCHAR(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '机器人id';

ALTER TABLE `bfop_config` CHANGE `code` `code` VARCHAR(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '辨识代码';

ALTER TABLE `bfop_config` CHANGE `module` `module` VARCHAR(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'default' COMMENT '配置模块';

ALTER TABLE `bfop_config` CHANGE `value` `value` VARCHAR(6000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '设定值';

ALTER TABLE `bfop_config` CHANGE `description` `description` VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '描述';

ALTER TABLE `robot_words_type` CHANGE `name` `name` VARCHAR(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '话述名称';

ALTER TABLE `robot_words_type` CHANGE `comment` `comment` VARCHAR(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '话述备注';


-- 预设设定, 都是关闭
INSERT IGNORE INTO `bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`, `status`) VALUES ('', 'faq-similar-question-switch', 'controller', 'off', '近似问开关', '0', '0');

INSERT IGNORE INTO `bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`, `status`) VALUES ('', 'faq-recommand-question-switch', 'controller', 'off', '推荐问开关', '0', '0');

INSERT IGNORE INTO `bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`, `status`) VALUES ('', 'faq-related-question-switch', 'controller', 'off', '指定相关问开关', '0', '0');

-- 如果已经有话术, 开关打开
INSERT IGNORE INTO `bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`, `status`)
SELECT appid, 'faq-similar-question-switch', 'controller', 'on', '近似问开关', '0', '0' FROM `robot_words` WHERE type = 4;

INSERT IGNORE INTO `bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`, `status`)
SELECT appid, 'faq-recommand-question-switch', 'controller', 'on', '推荐问开关', '0', '0' FROM `robot_words` WHERE type = 2;

INSERT IGNORE INTO `bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`, `status`)
SELECT appid, 'faq-related-question-switch', 'controller', 'on', '指定相关问开关', '0', '0' FROM `robot_words` WHERE type = 5;



-- 预设话术
INSERT INTO `robot_words` (`id`, `appid`, `content`, `type`) 
SELECT NULL, '', '你好，很高兴为你服务。', '1'
WHERE NOT EXISTS (
		SELECT * FROM robot_words 
		WHERE appid = '' AND type = '1'
	);
INSERT INTO `robot_words` (`id`, `appid`, `content`, `type`) 
SELECT NULL, '', '您是不是想问：', '2'
WHERE NOT EXISTS (
		SELECT * FROM robot_words 
		WHERE appid = '' AND type = '2'
	);
INSERT INTO `robot_words` (`id`, `appid`, `content`, `type`) 
SELECT NULL, '', '这对我来说有点难呢，还需要继续努力，再换种问法试试呗～', '3'
WHERE NOT EXISTS (
		SELECT * FROM robot_words 
		WHERE appid = '' AND type = '3'
	);
INSERT INTO `robot_words` (`id`, `appid`, `content`, `type`) 
SELECT NULL, '', '您是不是还想了解：', '4'
WHERE NOT EXISTS (
		SELECT * FROM robot_words 
		WHERE appid = '' AND type = '4'
	);
INSERT INTO `robot_words` (`id`, `appid`, `content`, `type`) 
SELECT NULL, '', '您是不是还想了解：', '5'
WHERE NOT EXISTS (
		SELECT * FROM robot_words 
		WHERE appid = '' AND type = '5'
	);
INSERT INTO `robot_words` (`id`, `appid`, `content`, `type`) 
SELECT NULL, '', '这个敏感话题我们不讨论哦', '12'
WHERE NOT EXISTS (
		SELECT * FROM robot_words 
		WHERE appid = '' AND type = '12'
	);

-- 找出哪些appid 没有设话术, 再来insert
INSERT INTO `robot_words` (`id`, `appid`, `content`, `type`) 
SELECT NULL, apps.uuid, '你好，很高兴为你服务。', '1'
FROM auth.apps apps
LEFT JOIN (
    SELECT appid
    FROM emotibot.robot_words
    WHERE type = 1
) robot_words_new
ON apps.uuid = robot_words_new.appid
WHERE robot_words_new.appid IS NULL;

INSERT INTO `robot_words` (`id`, `appid`, `content`, `type`) 
SELECT NULL, apps.uuid, '您是不是想问：', '2'
FROM auth.apps apps
LEFT JOIN (
    SELECT appid
    FROM emotibot.robot_words
    WHERE type = 2
) robot_words_new
ON apps.uuid = robot_words_new.appid
WHERE robot_words_new.appid IS NULL;

INSERT INTO `robot_words` (`id`, `appid`, `content`, `type`) 
SELECT NULL, apps.uuid, '这对我来说有点难呢，还需要继续努力，再换种问法试试呗～', '3'
FROM auth.apps apps
LEFT JOIN (
    SELECT appid
    FROM emotibot.robot_words
    WHERE type = 3
) robot_words_new
ON apps.uuid = robot_words_new.appid
WHERE robot_words_new.appid IS NULL;

INSERT INTO `robot_words` (`id`, `appid`, `content`, `type`) 
SELECT NULL, apps.uuid, '您是不是还想了解：', '4'
FROM auth.apps apps
LEFT JOIN (
    SELECT appid
    FROM emotibot.robot_words
    WHERE type = 4
) robot_words_new
ON apps.uuid = robot_words_new.appid
WHERE robot_words_new.appid IS NULL;

INSERT INTO `robot_words` (`id`, `appid`, `content`, `type`) 
SELECT NULL, apps.uuid, '您是不是还想了解：', '5'
FROM auth.apps apps
LEFT JOIN (
    SELECT appid
    FROM emotibot.robot_words
    WHERE type = 5
) robot_words_new
ON apps.uuid = robot_words_new.appid
WHERE robot_words_new.appid IS NULL;

INSERT INTO `robot_words` (`id`, `appid`, `content`, `type`) 
SELECT NULL, apps.uuid, '这个敏感话题我们不讨论哦', '12'
FROM auth.apps apps
LEFT JOIN (
    SELECT appid
    FROM emotibot.robot_words
    WHERE type = 12
) robot_words_new
ON apps.uuid = robot_words_new.appid
WHERE robot_words_new.appid IS NULL;

-- 修改话术解释信息
UPDATE robot_words_type
SET comment = '机器人会在每次对话开始时主动和用户打招呼。<br\>欢迎语的内容可以包含自我介绍、能力介绍、推荐的问题，让用户快速了解机器人能帮他做些什么。<br\>最多可以添加5条话术。当有多条话术时，机器人会随机出话。<br\>' WHERE `type` = 1;
UPDATE robot_words_type
SET comment = '当机器人找不到任何合适的回答时，就会说出\"未知回复\"的内容。在\"未知回复\"中：可以请用户重新描述他的需求，可以提示用户应该如何提问。<br\>最多可以添加5条话术。当有多条话术时，机器人会随机出话。<br\>' WHERE `type` = 3;
UPDATE robot_words_type 
SET comment = '开启后，标准问题中设置的相关问题会跟随回答一起出现。优先级高于\"近似问\"和\"推荐问\"。<br\>最多可以添加5条话术。当有多条话术时，机器人会随机出话。<br\>' WHERE `type` = 5;
UPDATE robot_words_type 
SET comment = '开启后，当多个回答的信心分接近时，机器人会提供信心分最高的回答，并推荐其他多个问题供用户参考，以提高回答命中率。优先级高于\"推荐问\"。<br\>最多可以添加5条话术。当有多条话术时，机器人会随机出话。<br\>' WHERE `type` = 4;
UPDATE robot_words_type 
SET comment = '开启后，机器人会在回答后或回答信心不足时，推荐多个问题供用户选择，以提高回答命中率。<br\>最多可以添加5条话术。当有多条话术时，机器人会随机出话。<br\>' WHERE `type` = 2;
UPDATE robot_words_type 
SET comment = '在没有特定敏感词回复时，用户输入的句子包含敏感词，机器人会回复通用敏感词话术。<br\>最多可以添加5条话术。当有多条话术时，机器人会随机出话。<br\>' WHERE `type` = 12;

-- 繁简处理, 如果之前语系设繁体, 预设话术更新成繁体
UPDATE robot_words
SET content = CASE 
	WHEN (content = '你好，很高兴为你服务。' AND type = '1') THEN '你好，很高興為你服務。'
    WHEN (content = '您是不是想问：' AND type = '2') THEN '您是不是想問：'
    WHEN (content = '这对我来说有点难呢，还需要继续努力，再换种问法试试呗～' AND type = '3') THEN  '這對我來說有點難呢，還需要繼續努力，再換種問法試試唄～'
    WHEN (content = '您是不是还想了解：' AND type = '4') THEN '您是不是還想了解：'
    WHEN (content = '您是不是还想了解：' AND type = '5') THEN '您是不是還想了解：'
    WHEN (content = '这个敏感话题我们不讨论哦' AND type = '12') THEN '這個敏感話題我們不討論哦'
    ELSE content
END
WHERE appid in (
	SELECT t1.uuid
	FROM (
		SELECT uuid, code, value 
		FROM auth.apps apps
		LEFT JOIN (
			SELECT appid, code, value FROM emotibot.bfop_config WHERE code = 'language'
		) custom_config
		ON apps.uuid = custom_config.appid
	) t1
	LEFT JOIN
	(
		SELECT code, value FROM emotibot.bfop_config WHERE appid = '' AND code = 'language'
	) t2
	ON true
	WHERE t1.value = 'zh-tw' OR (t1.value IS NULL AND t2.value = 'zh-tw')
) AND id >= 0;
-- +migrate Down
