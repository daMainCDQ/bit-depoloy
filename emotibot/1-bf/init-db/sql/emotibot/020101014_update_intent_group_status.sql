-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
UPDATE emotibot.`intent_group` SET `group_status` = 1 WHERE `group_id` in (SELECT `uuid` FROM auth.`apps`);
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back