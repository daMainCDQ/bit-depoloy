-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `intents` ADD `status` tinyint(1) NOT NULL DEFAULT '0';
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `intents` DROP `status`;
