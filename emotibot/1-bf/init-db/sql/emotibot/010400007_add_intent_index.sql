-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `intents` ADD INDEX `idx_appid` (`appid`);
-- +migrate Down
ALTER TABLE `intents` DROP INDEX `idx_appid`;
