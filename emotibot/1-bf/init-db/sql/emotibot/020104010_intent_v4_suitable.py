#-*- coding: UTF-8 -*-
import time
import pymysql
import os

mysql_host = os.environ.get('INIT_MYSQL_HOST', '127.0.0.1')
mysql_port = int(os.environ.get('INIT_MYSQL_PORT', '3306'))
mysql_user = os.environ.get('INIT_MYSQL_USER', 'root')
mysql_password = os.environ.get('INIT_MYSQL_PASSWORD', 'password')

def make_intent_suitable():
    print("start create intent_v3")
    try:
        db = pymysql.connect(host=mysql_host, port=mysql_port, user=mysql_user, passwd=mysql_password, db='emotibot', charset='utf8')
        cursor = db.cursor()
        sql = "CREATE TABLE intents_v3 LIKE intents;"
        cursor.execute(sql)
        sql = "INSERT INTO intents_v3 SELECT * FROM intents;"
        cursor.execute(sql)
        sql = "CREATE TABLE intent_train_sets_v3 LIKE intent_train_sets;"
        cursor.execute(sql)
        sql = "INSERT INTO intent_train_sets_v3 SELECT * FROM intent_train_sets;"
        cursor.execute(sql)
        sql = "CREATE TABLE intent_group_v3 LIKE intent_group;"
        cursor.execute(sql)
        sql = "INSERT INTO intent_group_v3 SELECT * FROM intent_group;"
        cursor.execute(sql)
        sql = "select app_id,group_id from emotibot.intent_group where group_type = 2 order by id desc"
        cursor.execute(sql)
        group_ids = cursor.fetchall()
        for i in range(len(group_ids)):
            app_id = group_ids[i][0]
            group_id = group_ids[i][1]
            t = time.time()
            int(t)
            sql = "insert into intents(appid,`name`,updatetime,`status`,group_id,is_negative) " \
                  "values('{}','意图反例','{}',0,'{}',1)".format(app_id, int(t), group_id)
            cursor.execute(sql)
            negative_id = cursor.lastrowid

            sql = "update intent_train_sets set intent = '{}' where intent in (SELECT id FROM intents WHERE group_id = '{}' and status = 0 and version is NULL) and type = 1".format(negative_id, group_id)
            cursor.execute(sql)
        cursor.close()
        db.commit()
    except Exception as e:
        print(e)
        print ("intent suitable error,rollback")
        db.rollback()
    finally:
        db.close()

if __name__ == '__main__':
    make_intent_suitable()