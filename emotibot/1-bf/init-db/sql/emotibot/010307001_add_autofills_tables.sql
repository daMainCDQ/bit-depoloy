-- +migrate Up
CREATE TABLE `autofill_sync` (
  `app_id` char(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '机器人ID',
  `start_time` bigint(20) NOT NULL COMMENT '联想同步任务开始时间',
  `rerun` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否需要重新同步联想资料',
  `task_id` int(11) DEFAULT NULL COMMENT '联想同步任务详细资讯关连ID',
  `mode` int(11) NOT NULL COMMENT '联想同步任务模式: 0: Reset (重设联想语料); 1: Update (更新联想语料)',
  PRIMARY KEY (`app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='联想同步任务锁';

CREATE TABLE `autofill_sync_tasks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '联想同步任务识别码',
  `start_time` bigint(20) NOT NULL COMMENT '联想同步任务开始时间',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '联想同步任务执行状态: 0: Running (执行中); 1: Finished (完成); 2: Expired (超时); 3: Error (错误)',
  `message` text COMMENT '联想同步任务相关讯息，包含错误讯息',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='联想同步任务详细资讯';

CREATE TABLE `taskengine_intents` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `app_id` char(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '机器人ID',
  `scenario_id` char(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Task Engine场景ID',
  `name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '意图名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Task Engine使用到的意图资讯';

CREATE TABLE `taskengine_intents_prev` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `app_id` char(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '机器人ID',
  `name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Task Engine场景ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Task Engine上一次使用的意图资讯，用来比较有哪些语料需要更新';

-- +migrate Down
DROP TABLE `autofill_sync`;
DROP TABLE `autofill_sync_tasks`;
DROP TABLE `taskengine_intents`;
DROP TABLE `taskengine_intents_prev`;
