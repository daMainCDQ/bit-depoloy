-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE `report_clusters` (
    `id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
    `report_id` bigint(11) UNSIGNED NOT NULL COMMENT '該分群所屬的聚類工作ID',
    `tags` JSON NOT NULL COMMENT '分群的標籤, 因為只是顯示用途，就沒有做normalized，必須為一個JSON Array',
    `created_time` bigint(11) UNSIGNED NOT NULL COMMENT '建立時間',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='聚類工作所產生的分群';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `report_clusters`;