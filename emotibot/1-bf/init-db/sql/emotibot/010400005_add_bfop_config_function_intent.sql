-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
INSERT IGNORE INTO `bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`, `status`) VALUES ('', 'function-intent', 'controller', 'on', 'on/off: 开启或关闭预设意图', '0', '0');

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
