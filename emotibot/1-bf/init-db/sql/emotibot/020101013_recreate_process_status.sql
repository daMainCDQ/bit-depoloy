-- +migrate Up
CREATE TABLE IF NOT EXISTS `process_status` (
  `id` int(18) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `app_id` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '机器人id',
  `module` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '处理模块名称',
  `start_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '开始时间',
  `end_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '结束时间',
  `status` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '状态',
  `message` text COLLATE utf8mb4_unicode_ci COMMENT '处理结果讯息',
  `entity_file_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '档案名称',
  PRIMARY KEY (`id`),
  KEY `IDX_app_id` (`app_id`),
  KEY `IDX_app_module` (`app_id`,`module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='非同步模块处理状态';

-- +migrate Down
DROP TABLE IF EXISTS `process_status`;
