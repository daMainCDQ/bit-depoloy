-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE `reports` (
    `id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
    `app_id` char(36) NOT NULL COMMENT '發起工作的所屬機器人id',
    `user_id` varchar(64) NOT NULL COMMENT '發起工作的使用者',
    `condition` mediumtext NOT NULL COMMENT '用JSON表達的聚類工作條件式，符合該條件的日誌才會進行',
    `created_time` bigint(11) NOT NULL COMMENT '發起工作的UTC Timestamp',
    `updated_time` bigint(11) NOT NULL COMMENT '聚類工作的最後狀態更新時間',
    `status` tinyint(1) DEFAULT 0 COMMENT '工作狀態,0 為執行中',
    `ignored_size` int(11) UNSIGNED DEFAULT 0 COMMENT '被忽略的日誌條數',
    `marked_size` int(11) UNSIGNED DEFAULT 0 COMMENT '已被標註的日誌條數',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='聚類工作資訊';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `reports`;