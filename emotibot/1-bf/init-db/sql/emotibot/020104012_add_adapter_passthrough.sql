-- +migrate Up

ALTER TABLE `adapter_inner_api_definition`
ADD COLUMN `default_resp_body_template` TEXT NULL COMMENT '返回为空等默认模板' AFTER `response_body_template`,
ADD COLUMN `req_pass_through_flag` TINYINT(1) DEFAULT 0 COMMENT '是否开启内部api请求透传' AFTER `response_body_template`,
ADD COLUMN `res_pass_through_flag` TINYINT(1) DEFAULT 0 COMMENT '是否开启内部api响应透传' AFTER `response_body_template`;

-- +migrate Down