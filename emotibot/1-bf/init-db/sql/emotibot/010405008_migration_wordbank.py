import pymysql
import json
import os
import time

mysql_host = os.environ.get('INIT_MYSQL_HOST')
mysql_port = int(os.environ.get('INIT_MYSQL_PORT'))
mysql_user = os.environ.get('INIT_MYSQL_USER')
mysql_password = os.environ.get('INIT_MYSQL_PASSWORD')
mysql_db = 'emotibot'
nersql_db = 'ner_factory'

TOPCATALOGS = ['敏感词库',
               '转人工词库',
               '通用词库',
               '标准问题词库',
               '任务引擎词库',
               '意图引擎词库',
               '解析器词库']

OLDTOPCATALOGS = ['任务引擎词库',
                  '转人工词库',
                  '敏感词库',
                  '自定义NER词库',
                  '任務引擎詞庫',
                  '轉人工詞庫',
                  '敏感詞庫'
                  ]

MapCataLogs = {
    '任務引擎詞庫':'任务引擎词库',
    '轉人工詞庫':'转人工词库',
    '敏感詞庫':'敏感词库'
}

def updateNer(appid, oldid, newid, cursor, db):
  #  update_dictionary_sql = """update dictionary set dictionary_id = '%s' where dictionary_id = '%s' """ % (newid, oldid)
    update_dictionary_sql = "update dictionary set dictionary_id = %s where dictionary_id = %s"

  #  update_dictionary_map_sql = """update dictionary_relation_map set dictionary_id = '%s' where dictionary_id = '%s' """ % (newid, oldid)
    update_dictionary_map_sql = "update dictionary_relation_map set dictionary_id = %s where dictionary_id = %s"
    print(update_dictionary_sql)
    print(update_dictionary_map_sql)
    cursor.execute(update_dictionary_sql, (newid, oldid))
    cursor.execute(update_dictionary_map_sql, (newid, oldid))
    db.commit()

def addCatalog(appid, name, pid, fullpath, rootid, cursor, souce_db):
 #   insert_second_catalog_sql = """insert ignore into wordbank_catalog(appid, displayname, pid, editable, full_path, rootid)
 #               values ('%s', '%s', %d, 1, '%s', %d)""" % (appid, name, pid, fullpath, rootid)
    insert_second_catalog_sql = "insert ignore into wordbank_catalog(appid, displayname, pid, editable, full_path, rootid) values (%s, %s, %s, 1, %s, %s)"

    print(insert_second_catalog_sql)
    cursor.execute(insert_second_catalog_sql, (appid, name, pid, fullpath, rootid))
    id = souce_db.insert_id()
    return id

def addOtherWord(appid, pid, rootid, cursor, souce_db):
  #  query_other_word_sql = """SELECT name, similar_words FROM entities WHERE appid = '%s' AND cid is NULL""" % (appid)
    query_other_word_sql = "SELECT name, similar_words FROM entities WHERE appid = %s AND cid is NULL"
    cursor.execute(query_other_word_sql, (appid))
    otherWord = cursor.fetchall()
    for item in otherWord:
        subject = item[0]
        similars = item[1]
        similarList = similars.split(',')
    #    insert_word_sql = """insert ignore into wordbank_subject(appid, wordname, is_effect, is_spell, catalogid, rootid)
    #            values ('%s', '%s', 1, 0, %d, %d)""" % (appid, subject, pid, rootid)

        insert_word_sql = "insert ignore into wordbank_subject(appid, wordname, is_effect, is_spell, catalogid, rootid) values (%s, %s, 1, 0, %s, %s)"
        print(insert_word_sql, (appid, subject, pid, rootid))
        cursor.execute(insert_word_sql)
        sid = souce_db.insert_id()
        for similar in similarList:
            if similar != '':
            #    insert_similar_sql = """insert ignore into wordbank_similar(appid, similarname, wordid, is_spell, catalogid, rootid)
            #        values ('%s', '%s', %d, 0, %d, %d)""" % (appid, similar, sid, pid, rootid)

                insert_similar_sql = "insert ignore into wordbank_similar(appid, similarname, wordid, is_spell, catalogid, rootid) values (%s, %s, %s, 0, %s, %s)"
                print(insert_similar_sql)
                cursor.execute(insert_similar_sql, (appid, similar, sid, pid, rootid))

def addWord(appid, oldpid, pid, rootid, cursor, souce_db):
  #  query_toplevel_word_sql = """SELECT name, cid, similar_words, answer FROM entities WHERE cid = '%s'""" % (oldpid)

    query_toplevel_word_sql = "SELECT name, cid, similar_words, answer FROM entities WHERE cid = %s"
    print(query_toplevel_word_sql)
    cursor.execute(query_toplevel_word_sql, (oldpid))
    toplevelList = cursor.fetchall()
    for item in toplevelList:
        subject = item[0]
        similars = item[2]
        answer = item[3]
        similarList = similars.split(',')
        # 新的目录id怎么获取

        if answer == "":
          #  insert_toplevel_word_sql = """insert ignore into wordbank_subject(appid, wordname, is_effect, is_spell, catalogid, rootid)
          #      values ('%s', '%s', 1, 0, %d, %d)""" % (appid, subject, pid, rootid)

            insert_toplevel_word_sql = "insert ignore into wordbank_subject(appid, wordname, is_effect, is_spell, catalogid, rootid) values (%s, %s, 1, 0, %s, %s)"
            cursor.execute(insert_toplevel_word_sql, (appid, subject, pid, rootid))
        else:
        #    insert_toplevel_word_sql = """insert ignore into wordbank_subject(appid, wordname, is_effect, is_spell, catalogid, rootid, answer)
        #            values ('%s', '%s', 1, 0, %d, %d, '%s')""" % (appid, subject, pid, rootid, answer)

            insert_toplevel_word_sql = "insert ignore into wordbank_subject(appid, wordname, is_effect, is_spell, catalogid, rootid, answer) values (%s, %s, 1, 0, %s, %s, %s)"
            cursor.execute(insert_toplevel_word_sql, (appid, subject, pid, rootid, answer))

        print(insert_toplevel_word_sql)
       # cursor.execute(insert_toplevel_word_sql)
        sid = souce_db.insert_id()
        for similar in similarList:
            if similar != '':
             #   insert_toplevel_similar_sql = """insert ignore into wordbank_similar(appid, similarname, wordid, is_spell, catalogid, rootid)
             #       values ('%s', '%s', %d, 0, %d, %d)""" % (appid, similar, sid, pid, rootid)

                insert_toplevel_similar_sql = "insert ignore into wordbank_similar(appid, similarname, wordid, is_spell, catalogid, rootid) values (%s, %s, %s, 0, %s, %s)"
                print(insert_toplevel_similar_sql)
                cursor.execute(insert_toplevel_similar_sql,  (appid, similar, sid, pid, rootid))
#  souce_db.commit()

def addHistory(appid, cursor, souce_db):
    insert_history_sql = """insert ignore into wordbank_import_history(appid, document_original_name,  content, status, upload_type, created_time, updated_time )
        SELECT appid, filename, content, 1, 1, created_time, created_time FROM entity_files WHERE appid = '%s' """ % (appid)

    print(insert_history_sql)
    cursor.execute(insert_history_sql)

def insert_wordbank(appid, cursor, souce_db, nercursor, ner_db):
    # 老的机器人模板无需迁移
    if appid == '':
        return
    if appid == 'templateadmin':
        return

    # 导入词库上传历史记录
    addHistory(appid, cursor, souce_db)
    souce_db.commit()

    for catalog in TOPCATALOGS:
     #   insert_topcatalog_sql = """insert ignore into wordbank_catalog(appid, displayname, pid, editable, full_path)
     #           values ('%s', '%s', -1, 0, '%s')""" % (appid, catalog, catalog)

        insert_topcatalog_sql = "insert ignore into wordbank_catalog(appid, displayname, pid, editable, full_path) values (%s, %s, -1, 0, %s)"
        print(insert_topcatalog_sql)
        cursor.execute(insert_topcatalog_sql, (appid, catalog, catalog))
    souce_db.commit()

    # 获取 displayname -> id映射，再设置rootid
    query_new_topcatalog_sql = """select id, displayname, rootid from wordbank_catalog where appid = '%s'""" % (
        appid)
    print(query_new_topcatalog_sql)
    cursor.execute(query_new_topcatalog_sql)
    top_catalog_list = cursor.fetchall()
    nameToIdMap = {}
    for item in top_catalog_list:
        nameToIdMap[item[1]] = item[0]
        update_catalog_sql = """update wordbank_catalog set rootid=%d where id = %d""" % (item[0], item[0])
        print(update_catalog_sql)
        cursor.execute(update_catalog_sql)
    souce_db.commit()

    # 查询指定appid下面的目录信息
    # 因为要设置pid, 现在打算是逐级处理
    # 先找出一级目录
    query_top_catalog_sql = """SELECT name, id FROM entity_class WHERE appid = '%s' AND pid is NULL""" % (appid)
    print(query_top_catalog_sql)
    cursor.execute(query_top_catalog_sql)
    top_catalog_name_list = cursor.fetchall()

    pid = nameToIdMap['通用词库']
    rootid = nameToIdMap['通用词库']
    # 所有cid is null的主词都插入到通用词库下面，待完成
    addOtherWord(appid, pid, rootid, cursor, souce_db)

    for item in top_catalog_name_list:
        # 用户自定义一级目录，移动到新的通用目录下面
        oldpid = item[1]
        if item[0] not in OLDTOPCATALOGS:
            pid = nameToIdMap['通用词库']
            path = '通用词库/' + item[0]
            rootid = nameToIdMap['通用词库']
            # addOtherWord(appid, pid, rootid, cursor, souce_db)

            secondId = addCatalog(appid, item[0], pid, path, rootid, cursor, souce_db)
            # 插入自定义一级目录下面的主词和同义词
            addWord(appid, item[1], secondId, rootid, cursor, souce_db)

            secondCatalogList = findNextCatalog(oldpid, cursor)
            # 插入三级
            for secondItem in secondCatalogList:
                secondFullPath = path + '/' + secondItem[1]
                thirdId = addCatalog(appid, secondItem[1], secondId, secondFullPath, rootid, cursor, souce_db)
                # 插入自定义二级目录下面的主词和同义词
                addWord(appid, secondItem[0], thirdId, rootid, cursor, souce_db)

                # 查找三级
                thirdCatalogList = findNextCatalog(secondItem[0], cursor)
                for thirdItem in thirdCatalogList:
                    # 插入四级
                    thirdFullPath = secondFullPath + '/' + thirdItem[1]
                    fouthId = addCatalog(appid, thirdItem[1], thirdId, thirdFullPath, rootid, cursor, souce_db)
                    # 插入自定义三级目录下面的主词和同义词
                    addWord(appid, thirdItem[0], fouthId, rootid, cursor, souce_db)

                    # 查找四级
                    fouthCatalogList = findNextCatalog(thirdItem[0], cursor)
                    for fouthItem in fouthCatalogList:
                        # 插入五级
                        fouthFullPath = thirdFullPath + '/' + fouthItem[1]
                        fifthId = addCatalog(appid, fouthItem[1], fouthId, fouthFullPath, rootid, cursor, souce_db)
                        # 插入自定义四级目录下面的主词和同义词
                        addWord(appid, fouthItem[0], fifthId, rootid, cursor, souce_db)

        else:
            topcatalog = item[0]
            if item[0] == '自定义NER词库':
                topcatalog = '解析器词库'
                pid = nameToIdMap['解析器词库']
                rootid = nameToIdMap['解析器词库']
            else:
                if item[0] in MapCataLogs.keys():
                    topcatalog = MapCataLogs[item[0]]
                pid = nameToIdMap[topcatalog]
                rootid = nameToIdMap[topcatalog]
            # 插入一级目录下面的主词和同义词
            addWord(appid, oldpid, pid, rootid, cursor, souce_db)

            # 查找该一级目录下面的二级目录
            secondCatalogList = findNextCatalog(oldpid, cursor)
            for secondItem in secondCatalogList:
                # 插入二级目录
                secondFullPath = topcatalog + '/' + secondItem[1]
                secondId = addCatalog(appid, secondItem[1], pid, secondFullPath, rootid, cursor, souce_db)
                # 更新ner表中的目录id
                updateNer(appid, secondItem[0], secondId, nercursor, ner_db)
                # 插入二级目录下面的主词和同义词
                addWord(appid, secondItem[0], secondId, rootid, cursor, souce_db)
                # 查找三级目录
                thirdCatalogList = findNextCatalog(secondItem[0], cursor)
                for thirdItem in thirdCatalogList:
                    # 插入三级目录
                    thirdFullPath = secondFullPath + '/' + thirdItem[1]
                    thirdId = addCatalog(appid, thirdItem[1], secondId, thirdFullPath, rootid, cursor, souce_db)
                    # 插入三级目录下面的主词和同义词
                    addWord(appid, thirdItem[0], thirdId, rootid, cursor, souce_db)
                    # 查找四级目录
                    fouthCatalogList = findNextCatalog(thirdItem[0], cursor)
                    for fouthItem in fouthCatalogList:
                        fouthFullPath = thirdFullPath + '/' + fouthItem[1]
                        fouthId = addCatalog(appid, fouthItem[1], thirdId, fouthFullPath, rootid, cursor, souce_db)
                        # 插入四级目录下面的主词和同义词
                        addWord(appid, fouthItem[0], fouthId, rootid, cursor, souce_db)
        souce_db.commit()

def backupNer(ner_db, nercursor):
    init_del_dictionary_backup = """DROP table IF EXISTS dictionary_backup"""
    init_schema_dictionary_sql = """CREATE TABLE dictionary_backup LIKE dictionary"""
    init_data_dictionary_sql = """INSERT INTO dictionary_backup SELECT * FROM dictionary"""
    nercursor.execute(init_del_dictionary_backup)
    nercursor.execute(init_schema_dictionary_sql)
    nercursor.execute(init_data_dictionary_sql)

    init_del_dictionary_map_backup = """DROP table IF EXISTS dictionary_relation_map_backup"""
    init_schema_dictionary_map_sql = """CREATE TABLE dictionary_relation_map_backup LIKE dictionary_relation_map"""
    init_data_dictionary_map_sql = """INSERT INTO dictionary_relation_map_backup SELECT * FROM dictionary_relation_map"""
    nercursor.execute(init_del_dictionary_map_backup)
    nercursor.execute(init_schema_dictionary_map_sql)
    nercursor.execute(init_data_dictionary_map_sql)

    ner_db.commit()

def handler_wordbank():
    souce_db = pymysql.connect(host = mysql_host, port = mysql_port, user = mysql_user, passwd = mysql_password, db = mysql_db)
    cursor = souce_db.cursor()

    ner_db = pymysql.connect(host = mysql_host, port = mysql_port, user = mysql_user, passwd = mysql_password, db = nersql_db)
    nercursor = ner_db.cursor()
    # 找出所有待更新appId
    query_catalog = """SELECT DISTINCT(appid) FROM entity_class"""
    try:
        # 备份ner解析器相关数据
        backupNer(ner_db, nercursor)
        cursor.execute(query_catalog)
        appidList = cursor.fetchall()
        for appid in appidList:
            # 创建 appid 新的一级目录信息
            print(appid[0] + 'begin')
            try:
                insert_wordbank(appid[0], cursor, souce_db, nercursor, ner_db)
            except Exception as e:
                print(e)
                print(appid[0])
            print(appid[0] + 'end')
    except Exception as e:
        souce_db.rollback()
        print(e)




def findNextCatalog(pid, cursor):
    query_next_catalog_sql = """SELECT c2.id, c2.name
            FROM entity_class as c2
            JOIN entity_class as c1
            ON (c2.pid = c1.id)
            WHERE c1.id=%d""" % (pid)
    print(query_next_catalog_sql)
    cursor.execute(query_next_catalog_sql)
    catalogList = cursor.fetchall()
    return catalogList

if __name__ == '__main__':
    handler_wordbank()


