#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pymysql
import os
import time
chat_switch_default_value = {
    'chat-robot-custom': 'on',
    'chat-editorial-custom': 'on',
    'chat-domain-greeting': 'on',
    'chat-editorial': 'on',
    'chat-robot': 'off',
    'chat-editorial-domain': 'off'
}
turn_to_value = 'off'
'''
steps
1. if robot don't have switch itself,  add data row for default switch config

2. turn off all default switch


'''

def get_unix_time():
    t = time.time()
    return int(t) + 10 * 60

update_default_switch_sql = "update bfop_config set value = 'off' where appid = '' and module = 'controller' and code='{}';"

def turn_chat_switch_off():
    mysqlHost = os.environ.get('INIT_MYSQL_HOST', '172.17.0.1')
    mysqlPort = int(os.environ.get('INIT_MYSQL_PORT', 3306))
    mysqlUser = os.environ.get('INIT_MYSQL_USER', 'root')
    mysqlPassword = os.environ.get('INIT_MYSQL_PASSWORD', 'password')
    auth_db = pymysql.connect(mysqlHost, mysqlUser, mysqlPassword, 'auth', mysqlPort)
    emotibot_db = pymysql.connect(mysqlHost, mysqlUser, mysqlPassword, 'emotibot', mysqlPort)

    sql = "select uuid, name from apps"
    cursor = auth_db.cursor()
    try:
        cursor.execute(sql)
        rows = cursor.fetchall()
        for row in rows:
            if row[0]:
                for switch in chat_switch_default_value:
                    _generate(row[0], row[1], switch, emotibot_db)
                print("do execute add chat switch config: {}-{}".format(row[0], row[1]))
        for chat_switch in chat_switch_default_value:
            emotibot_cursor = emotibot_db.cursor()
            update_sql = update_default_switch_sql.format(chat_switch)
            emotibot_cursor.execute(update_sql)
        print("do change default value of chat switch")
        emotibot_db.commit()
    except Exception as e:
        emotibot_db.rollback()
        print(e)
        return False
    finally:
        cursor.close()

    return True

insert_sql_template = "insert into bfop_config(appid, code, module, value, description, status, update_time) values ('{}', '{}', 'controller', '{}', '', 0, {});"

def _generate(robot_id, robot_name, chat_switch, emotibot_db):
    sql = "select count(1) from bfop_config where module='controller' and appid='{}' and code='{}'".format(robot_id, chat_switch)
    cursor = emotibot_db.cursor()
    try:
        cursor.execute(sql)
        count = cursor.fetchone()[0]
        if count == 0:
            default_switch_value = chat_switch_default_value.get(chat_switch)
            insert_sql = insert_sql_template.format(robot_id, chat_switch, default_switch_value, get_unix_time())
            cursor.execute(insert_sql)
    except Exception as e:
        emotibot_db.rollback()
        print(e)
        return False
    finally:
        cursor.close()
    return True

if __name__ == '__main__':
    turn_chat_switch_off()
