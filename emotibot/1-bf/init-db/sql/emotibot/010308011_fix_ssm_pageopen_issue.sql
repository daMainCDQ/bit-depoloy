-- +migrate Up
UPDATE `emotibot`.`ent_config` SET `zhtw`='在我們將真實用戶問題放進範本時（<a href=\"标准问题和回答上传.xlsx\" style=\"text-decoration: underline\" download=\"标准问题和回答上传.xlsx\">標準問題範本下載</a>），需要注意以下幾點：' WHERE `name`='ssm-help-page-sq-download-template-tips';
UPDATE `emotibot`.`ent_config` SET `zhtw`='請下載訓練語料範本（<a href=\"语料上传.xlsx\" style=\"text-decoration: underline\" download=\"语料上传.xlsx\">訓練語料範本下載</a>），並按照範本格式，將標準問題清單放入範本中，請注意以下幾點：' WHERE `name`='ssm-help-page-add-train-log-download-template-tips';
UPDATE `emotibot`.`ent_config` SET `zhtw`='[\"1.請確認您的問題中沒有完全相同的問題；\", \"2.請確認您的每個標準問題和語料都沒有超過50個字； \", \"3.上傳的文件大小不能超過\"]' WHERE `name`='ssm-lq-pop1';

-- +migrate Down
-- No need