-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
INSERT INTO `integration` (`appid`, `platform`, `pkey`, `pvalue`) VALUES
('', 'line', 'token', ''),
('', 'line', 'secret', ''),
('', 'messenger', 'page-id', ''),
('', 'messenger', 'verify-token', '2H4zDBRkBEg67MgS4xpmCxYvdWqrURPV'),
('', 'messenger', 'access-token', '');
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DELETE FROM integration where appid = '' AND (platform = 'messenger' OR platform = 'line');
