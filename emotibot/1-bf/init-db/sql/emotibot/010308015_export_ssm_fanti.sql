-- +migrate Up
UPDATE `emotibot`.`ent_config` SET `zhtw`='在我們將真實用戶問題放進範本時（<a href=\"標準問題和回答上傳.xlsx\" style=\"text-decoration: underline\" download=\"標準問題和回答上傳.xlsx\">標準問題範本下載</a>），需要注意以下幾點：' WHERE `name`='ssm-help-page-sq-download-template-tips';
INSERT INTO `emotibot`.`ent_config` (`name`, `module`, `value`, `enabled`, `description`, `zhtw`) VALUES ('ssm-upload-lq-template', 'helper', '语料上传.xlsx', 1, 'addTrain_log', '語料上傳.xlsx');
UPDATE `emotibot`.`ent_config` SET `zhtw`='請下載訓練語料範本（<a href=\"語料上傳.xlsx\" style=\"text-decoration: underline\" download=\"語料上傳.xlsx\">訓練語料範本下載</a>），並按照範本格式，將標準問題清單放入範本中，請注意以下幾點：' WHERE `name`='ssm-help-page-add-train-log-download-template-tips';

-- +migrate Down