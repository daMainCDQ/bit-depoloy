-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

INSERT INTO `bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`, `status`) VALUES ('', 'score_standard', 'health_check', '{\"label\":[\"较差\",\"一般\",\"良好\"],\"score\":[33,67]}', '语料健康检查分值标准', 0, '0');
INSERT INTO `bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`, `status`) VALUES ('', 'lq_sq_rate_remark', 'health_check', '[\"建议比例：1:10 (即建议1条标准问题至少有10条语料)\",\"良好比例：1:10 (即建议1条标准问题至少有30条语料)\"]', '语料健康检查占比说明', '0', '0');
INSERT INTO `bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`, `status`) VALUES ('', 'lq_distribution_recommended', 'health_check', '[{\"label\":\"0-5\",\"sq_num\":0,\"sq_rate\":10},{\"label\":\"6-10\",\"sq_num\":0,\"sq_rate\":20},{\"label\":\"11-20\",\"sq_num\":0,\"sq_rate\":20},{\"label\":\"21-30\",\"sq_num\":0,\"sq_rate\":30},{\"label\":\"31+\",\"sq_num\":0,\"sq_rate\":20}]', '语料健康检查语料分布参考值', '0', '0');
INSERT INTO `bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`, `status`) VALUES ('', 'score_weight', 'health_check', '{\"lq_quality_weight\":0.7,\"lq_sq_rate_weight\":0.1,\"lq_distribution_weight\":0.2}', '语料健康检查健康度分值权重', '0', '0');

CREATE TABLE `health_check_status` (
    `task_id` VARCHAR(50) NOT NULL COMMENT '任务id',
    `appid` VARCHAR(50) NOT NULL COMMENT '机器人id',
    `status` INT NOT NULL DEFAULT '-1' COMMENT '状态',
    `progress` INT NOT NULL DEFAULT '0' COMMENT '进度',
    `message` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '消息',
    `download_url` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '报告文件路径',
    `update_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`task_id`)
)  ENGINE=INNODB CHARSET=UTF8MB4 COLLATE UTF8MB4_GENERAL_CI COMMENT='语料冲突检查任务记录';

CREATE TABLE `health_check_report` (
  `task_id` varchar(50) NOT NULL COMMENT '任务id',
  `appid` varchar(50) NOT NULL DEFAULT '' COMMENT '机器人id',
  `report` text NOT NULL COMMENT '报告结果json',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='语料冲突检查任务报告';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `health_check_status`;
DROP TABLE `health_check_report`;
