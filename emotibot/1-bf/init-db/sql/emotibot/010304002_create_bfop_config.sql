-- +migrate Up
CREATE TABLE `bfop_config` (
  `id` int(11) NOT NULL,
  `appid` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '机器人id',
  `code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '辨识代码',
  `module` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default' COMMENT '配置模块',
  `value` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '设定值',
  `description` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '描述',
  `update_time` bigint(20) NOT NULL DEFAULT '4102358400' COMMENT '更新时间, 格式为unix_time',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='BFOP配置表';
-- +migrate Down
DROP TABLE `bfop_config`;
