-- +migrate Up
ALTER TABLE `audit_record` ADD `enterprise` CHAR(36) NOT NULL DEFAULT '' AFTER `id`;
-- +migrate Down
ALTER TABLE `audit_record` DROP `enterprise`;
