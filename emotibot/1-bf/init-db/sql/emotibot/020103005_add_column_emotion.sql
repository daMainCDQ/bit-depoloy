-- +migrate Up

alter table `emotion_group_info`
   add `group_type` int default 0 null comment '情绪组类型：0表示自定义情绪组，1表示系统情绪组';

-- +migrate Down
ALTER TABLE `emotion_group_info` DROP COLUMN `group_type` ;