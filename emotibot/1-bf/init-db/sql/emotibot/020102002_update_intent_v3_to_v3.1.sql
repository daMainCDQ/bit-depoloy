-- +migrate Up

DROP TABLE IF EXISTS `intent_parser`;
CREATE TABLE `intent_parser` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
	`intent_id` int(11) NOT NULL COMMENT '意图ID',
  `parser_id` varchar(64) NOT NULL COMMENT '抽取器ID',
  `parser_name` varchar(256) NOT NULL COMMENT '抽取器名称',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='意图3.1新增表1 ：意图-抽取器关系表';

-- ----------------------------
-- update table's structure
-- ----------------------------
ALTER TABLE `intent_train_sets` ADD (
  `has_slot` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '语料状态：0-初始值；1-已抽取词槽',
  `slot_sentence` varchar(256) COMMENT '已抽取后的语料');
	
	
-- +migrate Down
DROP TABLE `intent_parser`;
ALTER TABLE `intent_train_sets` DROP COLUMN `has_slot` , DROP COLUMN `slot_sentence` ;
