-- +migrate Up
DROP TABLE IF EXISTS `cmd_emotion`;
CREATE TABLE `cmd_emotion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appid` varchar(36) NOT NULL COMMENT '机器人id',
  `name` varchar(64) NOT NULL DEFAULT '' COMMENT '指令名字',
  `target` int(4) DEFAULT NULL COMMENT '0:问题1:答案',
  `emotion` varchar(64) NOT NULL DEFAULT '' COMMENT '情绪',
  `response_type` int(4) DEFAULT NULL COMMENT '0:取代, 1: 附加至前, 2:附加至后',
  `status` int(4) DEFAULT '0' COMMENT '0: 关闭1: 开启',
  `begin_time` datetime DEFAULT NULL COMMENT '生效时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='情绪指令设定';

SET FOREIGN_KEY_CHECKS = 1;
-- +migrate Down
DROP TABLE `cmd_emotion`;