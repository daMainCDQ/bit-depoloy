-- +migrate Up

ALTER TABLE `intent_test_sentences` ADD COLUMN  `app_id` varchar(64) NOT NULL COMMENT '机器人ID';
	
-- +migrate Down
ALTER TABLE `intent_test_sentences` DROP COLUMN `app_id` ;
