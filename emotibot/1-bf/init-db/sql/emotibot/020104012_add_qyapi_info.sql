-- +migrate Up
DROP TABLE IF EXISTS `qyapi_info`;
CREATE TABLE `qyapi_info` (
  `dbid` int(4) NOT NULL AUTO_INCREMENT,
  `qyapi_url` varchar(255) NOT NULL COMMENT '企业微信告警机器人url',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dbid`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
-- +migrate Down
DROP TABLE `qyapi_info`;