-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
insert into tag_type(code,name,appid)
select tt.code,tt.name,app.uuid from tag_type as tt,(select uuid from auth.apps where uuid COLLATE utf8mb4_general_ci not in (select distinct appid from tag_type)) as app
where tt.appid='system' and tt.code!='classfree';

insert into tags(code,name,type,created_time,app_id,label_id)
select t.code,t.name,temp.ttid,t.created_time,temp.appid,t.label_id from tags t inner join (SELECT tt.id as ttid,tt.appid as appid,tt2.id as id FROM tag_type tt inner join tag_type tt2 on tt.name=tt2.name where tt.appid!='system') as temp on temp.id=t.type;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DELETE FROM tag_type where appid!='system';
DELETE FROM tags where appid!='system';
