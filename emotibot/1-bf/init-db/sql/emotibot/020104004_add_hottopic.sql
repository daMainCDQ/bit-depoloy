-- +migrate Up
CREATE TABLE IF NOT EXISTS `hot_topic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` varchar(64) NOT NULL COMMENT '热点问题组id',
  `hot_topic` varchar(512) NOT NULL COMMENT '热点问题',
  `classification_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='热点问题';

CREATE TABLE IF NOT EXISTS `hot_topic_classification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='热点问题分类表';

CREATE TABLE IF NOT EXISTS `hot_topic_dimension` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` varchar(64) NOT NULL COMMENT '热点问题组id',
  `dimension_type_id` int(20) NOT NULL COMMENT '标签id',
  `dimension_id` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='热点问题组和标签关系';

CREATE TABLE IF NOT EXISTS `hot_topic_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` varchar(64) NOT NULL COMMENT '热点问题组id',
  `name` varchar(64) NOT NULL COMMENT '热点问题组名',
  `app_id` varchar(45) DEFAULT 'system',
  `type` int(11) DEFAULT 0 COMMENT '0-simple,1-classification',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='热点问题组信息';

-- +migrate Down
DROP TABLE `hot_topic`;
DROP TABLE `hot_topic_classification`;
DROP TABLE `hot_topic_dimension`;
DROP TABLE `hot_topic_group`;
