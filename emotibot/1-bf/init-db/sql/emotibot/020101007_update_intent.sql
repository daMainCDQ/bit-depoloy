-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
UPDATE `intents` SET `group_id`=`appid` WHERE `group_id` = '' OR `group_id` IS null;
UPDATE `intent_versions` SET `group_id`=`appid` WHERE `group_id` = '' OR `group_id` IS null;
UPDATE `intent_test_intents` SET `group_id`=`app_id` WHERE `group_id` = '' OR `group_id` IS null;
UPDATE `intent_test_versions` SET `group_id`=`app_id` WHERE `group_id` = '' OR `group_id` IS null;
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back