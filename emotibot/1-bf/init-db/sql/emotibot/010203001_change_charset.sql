-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `entities` CHANGE `name` `name` VARCHAR(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '类别名称';
ALTER TABLE `api_userkey` CHANGE `PreductName` `PreductName` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '';
