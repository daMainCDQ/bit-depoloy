#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pymysql
import os

sql_switch_off = '''INSERT INTO `bfop_config` (`appid`, `code`, `module`, `value`, `description`, `update_time`, `status`)
                    VALUES ('$robot', 'domain-kg-semantic', 'controller', 'off', '', 0, 0);'''

def skg_switch_off():
    mysqlHost = os.environ.get('INIT_MYSQL_HOST')
    mysqlPort = int(os.environ.get('INIT_MYSQL_PORT'))
    mysqlUser = os.environ.get('INIT_MYSQL_USER')
    mysqlPassword = os.environ.get('INIT_MYSQL_PASSWORD')
    auth_db = pymysql.connect(mysqlHost, mysqlUser, mysqlPassword, 'auth', mysqlPort)
    sql = "select uuid, name from apps where app_type = 0;"
    qa_manage_db = pymysql.connect(mysqlHost, mysqlUser, mysqlPassword, 'emotibot', mysqlPort)
    cursor = auth_db.cursor()
    try:
        cursor.execute(sql)
        rows = cursor.fetchall()
        for row in rows:
            if row[0]:
                _switch_off(row[0], row[1], qa_manage_db)
        qa_manage_db.commit()
    except Exception as e:
        auth_db.rollback()
        qa_manage_db.rollback()
        print(e)
        return False
    finally:
        cursor.close()
    return True


def _switch_off(robot_id, robot_name, qa_manage_db):
    cursor = qa_manage_db.cursor()
    cursor.execute("select appid from bfop_config where appid='{}' and code='{}';".format(robot_id, 'domain-kg-semantic'))
    check_result = cursor.fetchone()
    if check_result is None:
        sql_switch_off_modules = sql_switch_off.replace('$robot', robot_id)
        cursor.execute(sql_switch_off_modules)
        print('switch off sql module for robot 【{}】 success'.format(robot_name))
    else:
        print("robot 【{}】 has config, skip".format(robot_name))


if __name__ == '__main__':
    skg_switch_off()
