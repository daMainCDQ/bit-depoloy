-- +migrate Up
CREATE TABLE `intent_train_sets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sentence` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `intent` int(11) NOT NULL,
  `type` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `intent_id` (`intent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
-- +migrate Down
DROP TABLE `intent_train_sets`;