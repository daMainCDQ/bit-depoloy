-- +migrate Up
INSERT IGNORE INTO `robot_words_type` (`type`, `name`, `comment`) VALUES ('13', '通用转人工话术', '机器人识别用户有转人工需求的通用话术。');
INSERT IGNORE INTO `robot_words_type` (`type`, `name`, `comment`) VALUES ('14', '意图转人工话述', '机器人识别用户有转人工意图触发转人工的话术。');
INSERT IGNORE INTO `robot_words_type` (`type`, `name`, `comment`) VALUES ('15', '未知问题转人工话术', '机器人回答不了用户问题触发转人工的话术。');
INSERT IGNORE INTO `robot_words_type` (`type`, `name`, `comment`) VALUES ('16', '情绪转人工话术', '机器人识别用户有愤怒、不满情绪触发转人工的话术。');
INSERT IGNORE INTO `robot_words_type` (`type`, `name`, `comment`) VALUES ('17', '关键词转人工话术', '机器人识别用户问题包含已设定关键词触发转人工的话术。');
INSERT IGNORE INTO `robot_words_type` (`type`, `name`, `comment`) VALUES ('18', '多次匹配同一标准问转人工话术', '机器人识别用户问题多次命中同一个标准问触发转人工的话术。');
INSERT IGNORE INTO `robot_words_type` (`type`, `name`, `comment`) VALUES ('19', '标准问转人工话术', '机器人识别用户问题命中已设置的标准问触发转人工的话术。');
INSERT IGNORE INTO `robot_words_type` (`type`, `name`, `comment`) VALUES ('20', '非工作时间话术', '机器人识别用户有转人工需求但属于非工作时间的话术');
-- +migrate Down
