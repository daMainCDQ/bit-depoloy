-- +migrate Up
ALTER TABLE `intent_versions` MODIFY `appid` CHAR(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
ALTER TABLE `intent_versions` MODIFY `ie_model_id` CHAR(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
ALTER TABLE `intent_versions` MODIFY `re_model_id` CHAR(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

-- +migrate Down
ALTER TABLE `intent_versions` MODIFY `appid` CHAR(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE `intent_versions` MODIFY `ie_model_id` CHAR(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE `intent_versions` MODIFY `re_model_id` CHAR(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
