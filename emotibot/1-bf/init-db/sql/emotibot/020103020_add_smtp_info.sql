-- +migrate Up
DROP TABLE IF EXISTS `smtp_info`;
CREATE TABLE `smtp_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `smtp_server` varchar(100) NOT NULL COMMENT 'smtp 服务地址',
  `smtp_port` int(11) NOT NULL COMMENT 'smtp 服务端口',
  `account` varchar(50) NOT NULL COMMENT '发件人账号',
  `password` varchar(50) NOT NULL COMMENT '发件人密码',
  `encrypt_switch` int(4) DEFAULT NULL COMMENT '是否加密',
  `mod_time` datetime DEFAULT NULL COMMENT '最近修改时间',
  `email_prex` varchar(50) DEFAULT NULL COMMENT '邮件前缀',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
-- +migrate Down
DROP TABLE `smtp_info`;
