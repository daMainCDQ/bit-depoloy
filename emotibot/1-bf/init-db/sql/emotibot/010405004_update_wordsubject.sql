-- +migrate Up
ALTER TABLE `wordbank_subject`
MODIFY COLUMN `wordname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '主词名' AFTER `appid`;
-- +migrate Down
