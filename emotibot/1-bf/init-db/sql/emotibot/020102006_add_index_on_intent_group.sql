-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `intent_group` add index appIds_with_time(`app_id`,`create_time`);
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `intent_group` drop index appIds_with_time;