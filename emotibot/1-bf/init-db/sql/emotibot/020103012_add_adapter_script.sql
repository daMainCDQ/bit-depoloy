-- +migrate Up
create table adapter_script(
	id bigint unsigned primary key AUTO_INCREMENT comment 'id',
	uuid varchar(64) not null default '' comment 'uuid',
	user_id varchar(64) not null default '' comment '用户id',
	appid varchar(64) not null default '' comment '机器人id',
	name varchar(64) not null default '' comment '功能名称',
	script blob comment '代码内容',
	deleted int(2) not null default 0 comment '是否删除',
	in_release int(2) not null default 0 comment '是否发布',
	created_time datetime comment '创建时间',
	update_time	datetime comment '修改时间',
	release_script blob comment '已发布的代码',
	release_time datetime comment '发布时间',
	in_used int(2) not null default 0 comment '是否正在使用'
) comment 'adapter-python-ide';

create index idx_uuid on adapter_script(appid, uuid);
create index idx_created_time on adapter_script(created_time);

create table adapter_script_call_log(
	id bigint unsigned primary key AUTO_INCREMENT comment 'id',
	unique_id varchar(64) not null default '' comment 'unique_id',
	user_id varchar(64) not null default '' comment '用户id',
	appid varchar(64) not null default '' comment '机器人id',
	script_uuid varchar(64) not null default '' comment '代码uuid',
	execution_time datetime comment '调用时间',
	execution_status int(2) default 0 comment '执行状态',
	response_duration int(11) default 0 comment '响应时间(毫秒)',
	request_url varchar(200) comment '请求的url',
	request_data text comment '接到的请求参数',
	response_data text comment '请求的响应结果'
) comment 'python-ide调用日志';

create index idx_uniqueId on adapter_script_call_log(unique_id);
create index idx_execution_time on adapter_script_call_log(execution_time);


-- +migrate Down