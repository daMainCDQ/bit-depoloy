-- +migrate Up
CREATE TABLE `intents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appid` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` int(11) DEFAULT NULL,
  `updatetime` int(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `intent_version_id` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
-- +migrate Down
DROP TABLE `intents`;