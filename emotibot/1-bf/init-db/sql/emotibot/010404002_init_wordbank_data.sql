-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
/*!40101 SET NAMES utf8mb4 */;
INSERT IGNORE INTO `wordbank_catalog`(`id`, `appid`, `displayname`, `logicalname`, `pid`, `editable`,  `full_path`, `rootid`) VALUES (1, ' ', '敏感词库', NULL, -1, b'0',  '敏感词库', 1);
INSERT IGNORE INTO `wordbank_catalog`(`id`, `appid`, `displayname`, `logicalname`, `pid`, `editable`,  `full_path`, `rootid`) VALUES (2, ' ', '转人工词库', NULL, -1, b'0',  '转人工词库', 2);
INSERT IGNORE INTO `wordbank_catalog`(`id`, `appid`, `displayname`, `logicalname`, `pid`, `editable`,  `full_path`, `rootid`) VALUES (3, ' ', '通用词库', NULL, -1, b'0',  '通用词库', 3);
INSERT IGNORE INTO `wordbank_catalog`(`id`, `appid`, `displayname`, `logicalname`, `pid`, `editable`,  `full_path`, `rootid`) VALUES (4, ' ', '标准问题词库', NULL, -1, b'0',  '标准问题词库', 4);
INSERT IGNORE INTO `wordbank_catalog`(`id`, `appid`, `displayname`, `logicalname`, `pid`, `editable`,  `full_path`, `rootid`) VALUES (5, ' ', '任务引擎词库', NULL, -1, b'0',  '任务引擎词库', 5);
INSERT IGNORE INTO `wordbank_catalog`(`id`, `appid`, `displayname`, `logicalname`, `pid`, `editable`,  `full_path`, `rootid`) VALUES (6, ' ', '意图引擎词库', NULL, -1, b'0', '意图引擎词库', 6);
INSERT IGNORE INTO `wordbank_catalog`(`id`, `appid`, `displayname`, `logicalname`, `pid`, `editable`,  `full_path`, `rootid`) VALUES (7, ' ', '解析器词库', NULL, -1, b'0',  '解析器词库', 7);

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
TRUNCATE TABLE `wordbank_catalog`;