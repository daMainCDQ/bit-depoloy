-- +migrate Up
ALTER TABLE `taskenginescenario` CHANGE COLUMN `appID` `appID` VARCHAR(50) NOT NULL COMMENT '机器人id' , DROP PRIMARY KEY, ADD PRIMARY KEY (`scenarioID`, `appID`);
-- +migrate Down
