-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

CREATE TABLE `kw_queue` (
  `_id` varchar(24) NOT NULL DEFAULT '' COMMENT 'id',
  `app_id` varchar(100) DEFAULT NULL COMMENT 'app_id',
  `trainset_id` varchar(24) DEFAULT NULL COMMENT 'training data存在minio中的檔名',
  `status` varchar(20) DEFAULT NULL COMMENT '訓練的狀態，有pending, error, ready, training四種',
  `model_id` varchar(24) DEFAULT NULL COMMENT 'model存在minio中的檔名',
  `create_time` datetime DEFAULT NULL COMMENT '建立訓練任務的時間',
  `config` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '訓練模型用的config，為JSON string或是NULL',
  `auto_reload` tinyint(1) DEFAULT NULL COMMENT '決定模型訓連完後要不要直接在service上load',
  `model_version` varchar(30) DEFAULT NULL COMMENT '對應model是否為最新版使用',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT '每次建立訓練任務時的job queue，包含訓練前的trainig data位置與訓練完後的model檔位置';

CREATE TABLE `kw_load_map` (
  `_id` varchar(24) NOT NULL DEFAULT '' COMMENT 'id',
  `model_id` varchar(24) NOT NULL DEFAULT '' COMMENT 'model存在minio中的檔名',
  `app_id` varchar(100) NOT NULL DEFAULT '' COMMENT 'app_id',
  `queue_id` varchar(24) NOT NULL DEFAULT '' COMMENT 'app_id所使用的model對應到queue table中的ID',
  `model_version` varchar(30) DEFAULT NULL COMMENT '對應model是否為最新版使用',
  PRIMARY KEY (`_id`),
  KEY `app_id` (`app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT 'load map代表在service中最終用的model';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `kw_queue`;
DROP TABLE `kw_load_map`;