-- +migrate Up

ALTER TABLE adapter_script CONVERT TO CHARACTER SET "utf8mb4" COLLATE "utf8mb4_general_ci";

ALTER TABLE adapter_script_call_log CONVERT TO CHARACTER SET "utf8mb4" COLLATE "utf8mb4_general_ci";

-- +migrate Down