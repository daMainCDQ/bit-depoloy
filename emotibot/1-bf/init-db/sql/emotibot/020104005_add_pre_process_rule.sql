-- +migrate Up
CREATE TABLE IF NOT EXISTS `pre_process_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_id` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rule_content` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '规则内容',
  `rule_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '规则类型',
  `substitute` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '替换文字',
  PRIMARY KEY (`id`),
  KEY `idx_app_id_rule_type` (`app_id`,`rule_type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='用户问题预处理规则';

-- +migrate Down
DROP TABLE `pre_process_rule`;
