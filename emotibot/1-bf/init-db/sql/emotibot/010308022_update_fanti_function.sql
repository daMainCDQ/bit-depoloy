-- +migrate Up
ALTER TABLE `emotibot`.`function_switch`
ADD COLUMN `module_name_tw` VARCHAR(20) NULL DEFAULT NULL COMMENT '模组繁体名称' AFTER `status`,
ADD COLUMN `remark_tw` VARCHAR(50) NULL DEFAULT NULL COMMENT '备注繁体' AFTER `module_name_tw`;
UPDATE `emotibot`.`function_switch` SET `module_name_tw`='打開APP', `remark_tw`='幫我打開相機' WHERE `module_name`='AppControllerModule';
UPDATE `emotibot`.`function_switch` SET `module_name_tw`='有聲書', `remark_tw`='講一段郭德綱的相聲' WHERE `module_name`='AudioModule';
UPDATE `emotibot`.`function_switch` SET `module_name_tw`='成語接龍', `remark_tw`='來玩成語接龍' WHERE `module_name`='ChengYuModule';
UPDATE `emotibot`.`function_switch` SET `module_name_tw`='計算', `remark_tw`='1+1等於幾？' WHERE `module_name`='ComputationModule';
UPDATE `emotibot`.`function_switch` SET `module_name_tw`='演唱會', `remark_tw`='上海最近有什麼演唱會' WHERE `module_name`='ConcertModule';
UPDATE `emotibot`.`function_switch` SET `module_name_tw`='食譜', `remark_tw`='川菜怎麼做' WHERE `module_name`='CookbookModule';
UPDATE `emotibot`.`function_switch` SET `module_name_tw`='時間查詢', `remark_tw`='你知道現在幾點麼？' WHERE `module_name`='DatetimeModule';
UPDATE `emotibot`.`function_switch` SET `module_name_tw`='快遞査詢', `remark_tw`='我要查快遞' WHERE `module_name`='ExpressSearchModule';
UPDATE `emotibot`.`function_switch` SET `module_name_tw`='美食', `remark_tw`='附近有什麼好吃的美食' WHERE `module_name`='FoodModule';
UPDATE `emotibot`.`function_switch` SET `module_name_tw`='星座運勢', `remark_tw`='查白羊座星座運勢' WHERE `module_name`='HoroscopeModule';
UPDATE `emotibot`.`function_switch` SET `module_name_tw`='笑話', `remark_tw`='講個笑話' WHERE `module_name`='JokeModule';
UPDATE `emotibot`.`function_switch` SET `module_name_tw`='足球比賽', `remark_tw`='利物浦比賽結果' WHERE `module_name`='MatchModule';
UPDATE `emotibot`.`function_switch` SET `module_name_tw`='電影', `remark_tw`='推薦好看的電影' WHERE `module_name`='MovieModule';
UPDATE `emotibot`.`function_switch` SET `module_name_tw`='音樂', `remark_tw`='我要聽周杰倫的歌' WHERE `module_name`='MusicModule';
UPDATE `emotibot`.`function_switch` SET `module_name_tw`='NBA比賽査詢', `remark_tw`='查NBA比賽結果' WHERE `module_name`='NBAModule';
UPDATE `emotibot`.`function_switch` SET `module_name_tw`='新聞', `remark_tw`='今天有什麼娛樂新聞？' WHERE `module_name`='NewsModule';
UPDATE `emotibot`.`function_switch` SET `module_name_tw`='打電話', `remark_tw`='打電話給爸爸' WHERE `module_name`='PhoneCallModule';
UPDATE `emotibot`.`function_switch` SET `module_name_tw`='電話查詢', `remark_tw`='查下蘋果售後電話' WHERE `module_name`='PhoneQueryModule';
UPDATE `emotibot`.`function_switch` SET `module_name_tw`='圖片', `remark_tw`='發張美女圖片' WHERE `module_name`='PictureModule';
UPDATE `emotibot`.`function_switch` SET `module_name_tw`='匯率', `remark_tw`='台幣兌美元匯率是多少' WHERE `module_name`='QueryExchangeModule';
UPDATE `emotibot`.`function_switch` SET `module_name_tw`='股票', `remark_tw`='幫我查一下聯發科的股票' WHERE `module_name`='QueryStockModule';
UPDATE `emotibot`.`function_switch` SET `module_name_tw`='火車票査詢', `remark_tw`='我要查台北到新竹的火車票' WHERE `module_name`='QueryTicketModule';
UPDATE `emotibot`.`function_switch` SET `module_name_tw`='猜謎語', `remark_tw`='我要玩猜謎語' WHERE `module_name`='RiddleModule';
UPDATE `emotibot`.`function_switch` SET `module_name_tw`='發郵件', `remark_tw`='發郵件給媽媽' WHERE `module_name`='SendMailModule';
UPDATE `emotibot`.`function_switch` SET `module_name_tw`='發短信', `remark_tw`='發短信給爸爸' WHERE `module_name`='SendMessageModule';
UPDATE `emotibot`.`function_switch` SET `module_name_tw`='自動作曲', `remark_tw`='小影給我唱首歌' WHERE `module_name`='SingModule';
UPDATE `emotibot`.`function_switch` SET `module_name_tw`='講故事', `remark_tw`='給我講一個鬼故事' WHERE `module_name`='StoryModule';
UPDATE `emotibot`.`function_switch` SET `module_name_tw`='打車', `remark_tw`='我要打專車' WHERE `module_name`='TaxiModule';
UPDATE `emotibot`.`function_switch` SET `module_name_tw`='天氣', `remark_tw`='台北今天天氣怎麼樣？' WHERE `module_name`='WeatherModule';
UPDATE `emotibot`.`function_switch` SET `module_name_tw`='記帳', `remark_tw`='吃早飯花了100塊' WHERE `module_name`='module_accounting';
-- +migrate Down