import pymysql
import json
import os

mysql_host = os.environ.get('INIT_MYSQL_HOST', '172.16.101.98')
mysql_port = int(os.environ.get('INIT_MYSQL_PORT', '3306'))
mysql_user = os.environ.get('INIT_MYSQL_USER', 'root')
mysql_password = os.environ.get('INIT_MYSQL_PASSWORD', 'password')
mysql_db = 'emotibot'


def new_content(aid, answer):
    content = {
        "answer": answer,
        "property": {
            "dimension_id_list": []
        }
    }
    p = {
        "id": aid,
        "content": json.dumps(content, ensure_ascii=False, separators=(',', ':'))
    }
    return p


def handler():
    db = pymysql.connect(host=mysql_host, port=mysql_port, user=mysql_user, passwd=mysql_password, db=mysql_db)

    sql = 'select * ' \
          'from robot_profile_answer ' \
          'where 1 '

    cursor = db.cursor()
    cursor.execute(sql)
    res = cursor.fetchall()

    lines = []
    for i in res:
        if str.strip(i[3]).startswith("answer="):
            a = str.strip(i[3]).split("=")
            lines.append(new_content(i[0], a[1]))
        elif not str.strip(i[3]).startswith("{\"answer"):
            lines.append(new_content(i[0], str.strip(i[3])))

    # print(lines)
    sql = "SET @@session.sql_mode= '';"
    res = cursor.execute(sql)
    print(res)

    for i in lines:
        sql = "update robot_profile_answer set content = %s where id = %s"
        res = cursor.execute(sql, (i["content"], i["id"]))
        print(sql)
        print(i["id"], i["content"])
        print(res)

    res = db.commit()
    print(res)


if __name__ == '__main__':
    handler()
