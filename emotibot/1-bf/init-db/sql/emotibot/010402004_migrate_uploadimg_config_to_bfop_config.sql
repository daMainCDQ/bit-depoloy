-- +migrate Up
INSERT IGNORE INTO bfop_config (appid, code, module, value, description, update_time, status)
SELECT '', 'uploadimg_server', 'controller', value, description, 0, 0
FROM ent_config
WHERE name = 'uploadimg_server';

INSERT IGNORE INTO bfop_config (appid, code, module, value, description, update_time, status)
SELECT app_id, 'uploadimg_server', 'controller', value, '上传图片服务器', 0, 0
FROM ent_config_appid_customization
WHERE name = 'uploadimg_server';

INSERT IGNORE INTO bfop_config (appid, code, module, value, description, update_time, status)
VALUES ('', 'uploadimg_server', 'controller', '', '上传图片服务器', 0, 0);

DELETE FROM ent_config
WHERE name = 'uploadimg_server';

DELETE FROM ent_config_appid_customization
WHERE name = 'uploadimg_server';
-- +migrate Down

