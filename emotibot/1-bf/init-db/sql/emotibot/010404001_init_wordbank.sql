-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
-- ----------------------------
-- Table structure for wordbank_catalog
-- ----------------------------
DROP TABLE IF EXISTS `wordbank_catalog`;
CREATE TABLE `wordbank_catalog` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '目录id',
  `appid` varchar(36) DEFAULT NULL COMMENT '机器人id',
  `displayname` varchar(255) DEFAULT NULL COMMENT '目录名称',
  `logicalname` varchar(255) DEFAULT NULL COMMENT '目录逻辑名称',
  `pid` bigint(20) DEFAULT NULL COMMENT '父目录id(一级目录pid -1)',
  `editable` bit(1) DEFAULT b'1' COMMENT '能否编辑',
  `created_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `full_path` varchar(512) DEFAULT NULL COMMENT '完整路径',
  `rootid` bigint(20) DEFAULT NULL COMMENT '一级目录id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_id` (`id`) USING BTREE,
  KEY `idx_appid` (`appid`) USING BTREE,
  KEY `idx_pid` (`pid`) USING BTREE,
  KEY `idx_rootid` (`rootid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci COMMENT='词库目录表';

-- ----------------------------
-- Table structure for wordbank_import_history
-- ----------------------------
DROP TABLE IF EXISTS `wordbank_import_history`;
CREATE TABLE `wordbank_import_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '上传记录id',
  `appid` varchar(36) DEFAULT NULL COMMENT '机器人id',
  `document_original_name` varchar(255) DEFAULT NULL COMMENT '文档原始名称',
  `document_store_path` varchar(255) DEFAULT NULL COMMENT '文档存储路径',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `error_document_store_path` varchar(255) DEFAULT NULL COMMENT '错误报告存储路径',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0-表示待处理，1—表示处理完成，2-表示Db插入失败，3-文件被覆盖，4-表示上传文件中找不到目标页，5-表示部分数据导入成功, 6-表示导入模板版本不适配',
  `content` longblob COMMENT '文档内容',
  `upload_type` int(11) NOT NULL DEFAULT '1' COMMENT '1-表示全量上传，0——表示增量上传',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci COMMENT='词库上传历史记录';

-- ----------------------------
-- Table structure for wordbank_similar
-- ----------------------------
DROP TABLE IF EXISTS `wordbank_similar`;
CREATE TABLE `wordbank_similar` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '同义词id',
  `appid` varchar(36) DEFAULT NULL COMMENT '机器人id',
  `similarname` varchar(255) DEFAULT NULL COMMENT '同义词名称',
  `wordid` bigint(20) DEFAULT NULL COMMENT '所属主词',
  `is_spell` bit(1) DEFAULT b'1' COMMENT '是否开启拼音纠错',
  `created_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `catalogid` bigint(20) DEFAULT NULL COMMENT '所属目录id',
  `rootid` bigint(20) DEFAULT NULL COMMENT '所属一级目录id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_id` (`id`) USING BTREE,
  KEY `idx_appid` (`appid`,`wordid`,`catalogid`),
  KEY `idx_wordid` (`wordid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci COMMENT='词库同义词表';

-- ----------------------------
-- Table structure for wordbank_subject
-- ----------------------------
DROP TABLE IF EXISTS `wordbank_subject`;
CREATE TABLE `wordbank_subject` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主词id',
  `appid` varchar(36) DEFAULT NULL COMMENT '机器人id',
  `wordname` varchar(255) DEFAULT NULL COMMENT '主词名',
  `is_effect` bit(1) DEFAULT b'1' COMMENT '是否启用',
  `is_spell` bit(1) DEFAULT b'1' COMMENT '是否开启拼音纠错',
  `catalogid` bigint(20) DEFAULT NULL COMMENT '所属目录id',
  `created_time` timestamp DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `rootid` bigint(20) DEFAULT NULL COMMENT '一级目录id',
  `answer` varchar(255) DEFAULT NULL COMMENT '自定义回答（目前只有敏感词是用）',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_id` (`id`) USING BTREE,
  KEY `idx_catalogid` (`catalogid`) USING BTREE,
  KEY `idx_appid` (`appid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1849230 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci COMMENT='词库主词表';
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `wordbank_catalog`;
DROP TABLE `wordbank_import_history`;
DROP TABLE `wordbank_similar`;
DROP TABLE `wordbank_subject`;
