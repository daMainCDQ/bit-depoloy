-- +migrate Up
ALTER TABLE `autofill_sync` MODIFY `app_id` CHAR(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
ALTER TABLE `autofill_sync` DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

ALTER TABLE `autofill_sync_tasks` MODIFY `uuid` CHAR(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
ALTER TABLE `autofill_sync_tasks` MODIFY `message` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
ALTER TABLE `autofill_sync_tasks` DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

ALTER TABLE `taskengine_intents`MODIFY `app_id` CHAR(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
ALTER TABLE `taskengine_intents` MODIFY `scenario_id` CHAR(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
ALTER TABLE `taskengine_intents` MODIFY `name` VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
ALTER TABLE `taskengine_intents` DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

ALTER TABLE `taskengine_intents_prev` MODIFY `app_id` CHAR(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
ALTER TABLE `taskengine_intents_prev` MODIFY `name` VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
ALTER TABLE `taskengine_intents_prev` DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- +migrate Down
ALTER TABLE `autofill_sync` MODIFY `app_id` CHAR(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE `autofill_sync` DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

ALTER TABLE `autofill_sync_tasks` MODIFY `uuid` CHAR(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE `autofill_sync_tasks` MODIFY `message` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE `autofill_sync_tasks` DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `taskengine_intents`MODIFY `app_id` CHAR(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE `taskengine_intents` MODIFY `scenario_id` CHAR(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE `taskengine_intents` MODIFY `name` VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE `taskengine_intents` DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `taskengine_intents_prev` MODIFY `app_id` CHAR(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE `taskengine_intents_prev` MODIFY `name` VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE `taskengine_intents_prev` DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
