-- +migrate Up
ALTER TABLE `intents` MODIFY `appid` VARCHAR(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
ALTER TABLE `intents` MODIFY `name` VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

-- +migrate Down
ALTER TABLE `intents` MODIFY `appid` VARCHAR(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE `intents` MODIFY `name` VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
