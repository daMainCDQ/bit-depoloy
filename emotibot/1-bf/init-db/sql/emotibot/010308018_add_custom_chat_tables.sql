-- +migrate Up
CREATE TABLE `custom_chat_question` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '闲聊标准问题id',
  `appid` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '机器人id',
  `category` varchar(255) NOT NULL DEFAULT '未分类' COMMENT '分类',
  `content` varchar(255) NOT NULL COMMENT '闲聊标准问题',
  `updatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '操作状态',
  PRIMARY KEY (`id`),
  KEY `id` (`id`) USING BTREE,
  KEY `appid` (`appid`) USING BTREE,
  KEY `content` (`content`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='闲聊问题';

CREATE TABLE `custom_chat_answer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '闲聊问题回答id',
  `qid` bigint(20) NOT NULL COMMENT '闲聊问题id',
  `content` varchar(2000) NOT NULL COMMENT '闲聊回答',
  `updatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '操作状态',
  PRIMARY KEY (`id`),
  KEY `id` (`id`) USING BTREE,
  KEY `qid` (`qid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='闲聊回答';

CREATE TABLE `custom_chat_extend` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '闲聊扩写id',
  `qid` bigint(20) NOT NULL COMMENT '闲聊问题id',
  `content` varchar(255) NOT NULL COMMENT '闲聊扩写',
  `updatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '操作状态',
  PRIMARY KEY (`id`),
  KEY `id` (`id`) USING BTREE,
  KEY `qid` (`qid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='闲聊语料';


-- +migrate Down

