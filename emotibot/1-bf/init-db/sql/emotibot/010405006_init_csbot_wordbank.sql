-- +migrate Up
INSERT IGNORE INTO `wordbank_catalog`(`id`, `appid`, `displayname`, `logicalname`, `pid`, `editable`,  `full_path`, `rootid`)
SELECT  8, 'csbot', '敏感词库', NULL, -1, b'0',  '敏感词库', 8 FROM DUAL
WHERE NOT EXISTS (SELECT `id` FROM `wordbank_catalog` WHERE `appid`='csbot' AND `displayname`='敏感词库');

INSERT IGNORE INTO `wordbank_catalog`(`id`, `appid`, `displayname`, `logicalname`, `pid`, `editable`,  `full_path`, `rootid`)
SELECT  9, 'csbot', '转人工词库', NULL, -1, b'0',  '转人工词库', 9 FROM DUAL
WHERE NOT EXISTS (SELECT `id` FROM `wordbank_catalog` WHERE `appid`='csbot' AND `displayname`='转人工词库');

INSERT IGNORE INTO `wordbank_catalog`(`id`, `appid`, `displayname`, `logicalname`, `pid`, `editable`,  `full_path`, `rootid`)
SELECT  10, 'csbot', '通用词库', NULL, -1, b'0',  '通用词库', 10 FROM DUAL
WHERE NOT EXISTS (SELECT `id` FROM `wordbank_catalog` WHERE `appid`='csbot' AND `displayname`='通用词库');

INSERT IGNORE INTO `wordbank_catalog`(`id`, `appid`, `displayname`, `logicalname`, `pid`, `editable`,  `full_path`, `rootid`)
SELECT  11, 'csbot', '标准问题词库', NULL, -1, b'0',  '标准问题词库', 11 FROM DUAL
WHERE NOT EXISTS (SELECT `id` FROM `wordbank_catalog` WHERE `appid`='csbot' AND `displayname`='标准问题词库');

INSERT IGNORE INTO `wordbank_catalog`(`id`, `appid`, `displayname`, `logicalname`, `pid`, `editable`,  `full_path`, `rootid`)
SELECT  12, 'csbot', '任务引擎词库', NULL, -1, b'0',  '任务引擎词库', 12 FROM DUAL
WHERE NOT EXISTS (SELECT `id` FROM `wordbank_catalog` WHERE `appid`='csbot' AND `displayname`='任务引擎词库');

INSERT IGNORE INTO `wordbank_catalog`(`id`, `appid`, `displayname`, `logicalname`, `pid`, `editable`,  `full_path`, `rootid`)
SELECT  13, 'csbot', '意图引擎词库', NULL, -1, b'0',  '意图引擎词库', 13 FROM DUAL
WHERE NOT EXISTS (SELECT `id` FROM `wordbank_catalog` WHERE `appid`='csbot' AND `displayname`='意图引擎词库');

INSERT IGNORE INTO `wordbank_catalog`(`id`, `appid`, `displayname`, `logicalname`, `pid`, `editable`,  `full_path`, `rootid`)
SELECT  14, 'csbot', '解析器词库', NULL, -1, b'0',  '解析器词库', 14 FROM DUAL
WHERE NOT EXISTS (SELECT `id` FROM `wordbank_catalog` WHERE `appid`='csbot' AND `displayname`='解析器词库');

-- +migrate Down
