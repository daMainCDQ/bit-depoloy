-- +migrate Up
alter table adapter_call_log add column user_id varchar(100) null COMMENT '用户id';
alter table adapter_inner_api_definition add column user_id varchar(100) null COMMENT '用户id';
alter table adapter_outer_api_definition add column user_id varchar(100) null COMMENT '用户id';

-- +migrate Down
