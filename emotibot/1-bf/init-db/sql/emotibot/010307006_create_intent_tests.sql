-- +migrate Up
INSERT INTO intent_test_intents (app_id, intent, updated_time)
SELECT appid, id, UNIX_TIMESTAMP()
FROM intents
WHERE version IS NULL;

INSERT INTO intent_test_intents(app_id, intent, updated_time)
SELECT appid, NULL, UNIX_TIMESTAMP()
FROM intents
WHERE version IS NULL
GROUP BY appid;

-- +migrate Down
TRUNCATE TABLE intent_test_intents;
