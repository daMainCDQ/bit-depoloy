-- +migrate Up

CREATE TABLE IF NOT EXISTS `records_export` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '日志导出任务ID',
  `path` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT '日志导出档案存放路径',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '日志导出任务状态；0: 导出任务执行中 / 1: 完成 / 2: 失败 / 3: 搜寻结果为空，无日志导出档案',
  `err_reason` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT '日志导出失败原因 (status 为 2 的情况)',
  `created_at` int(20) NOT NULL COMMENT '日志导出任务建立时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='导出日志状态表';

-- +migrate Down
DROP TABLE IF EXISTS `records_export`;
