-- +migrate Up
ALTER TABLE `reports` ADD COLUMN `skipped_size` int(11) UNSIGNED DEFAULT 0 COMMENT '当时是标准问的日志条数';
-- +migrate Down
ALTER TABLE `reports` DROP COLUMN `skipped_size`;