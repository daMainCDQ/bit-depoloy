-- +migrate Up
CREATE TABLE `memory_access_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `app_id` varchar(50) DEFAULT NULL COMMENT '机器人id',
  `module_name` varchar(20) DEFAULT NULL COMMENT '模块名称',
  `category` varchar(50) DEFAULT NULL COMMENT '缓存类别',
  `last_visit` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '最会一次访问时间',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '数据创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '数据更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- +migrate Down
DROP TABLE `memory_access_history`;
