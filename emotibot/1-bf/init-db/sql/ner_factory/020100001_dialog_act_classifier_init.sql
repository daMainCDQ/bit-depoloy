-- +migrate Up
drop table if exists ner_factory.dac_classifier_info;
create table ner_factory.dac_classifier_info
(
  id                bigint auto_increment
    primary key,
  classifier_id     varchar(40)                         not null
  comment '分类器业务uuid',
  classifier_name   varchar(50)                         not null
  comment '分类器名称',
  description       varchar(200) default '暂无 '          null
  comment '分类器功能描述',
  use_general_model int(2) default '0'                  not null
  comment '是否开启通用模型——0：表示不开启，1：表示开启',
  classifier_status int(2) default '0'                  not null
  comment '分类器状态  0：表示待训练， 1：表示训练完成， 2：表示训练中， 3：表示训练失败',
  predict_slot_name varchar(20)                         not null
  comment '分类器预测槽位名称',
  predict_slot_desc varchar(100) default '暂无'           null
  comment '分类器预测槽位说明信息',
  user_id           varchar(40)                         not null
  comment '对话行为分类器所属用户Id',
  created_time      timestamp default CURRENT_TIMESTAMP not null,
  updated_time      timestamp default CURRENT_TIMESTAMP not null,
  constraint dac_classifier_info_classifier_id_uindex
  unique (classifier_id)
)
  comment 'dac : dialogue action classifier  对话行为分类器';

create index dac_classifier_info_user_id_index
  on ner_factory.dac_classifier_info (user_id);


drop table if exists ner_factory.dac_classifier_tag;
create table ner_factory.dac_classifier_tag
(
	id bigint auto_increment
		primary key,
	classifier_id varchar(40) not null comment '所属分类器id',
	tag_name varchar(16) not null comment '标签名称',
	tag_desc varchar(100) default '暂无' null comment '标签描述说明',
	is_system_tag int(2) default '0' not null comment '是否是系统默认标签 0：表示不是，1：表示是',
	created_time timestamp default CURRENT_TIMESTAMP not null,
	updated_time timestamp default CURRENT_TIMESTAMP not null
)
comment '对话行为分类器标签表';

create index dac_tag_id_classifier_id_index
	on ner_factory.dac_classifier_tag (id, classifier_id);


drop table if exists ner_factory.dac_tag_map_system_act;
create table ner_factory.dac_tag_map_system_act
(
  id                              bigint                          auto_increment                  primary key,
  sys_act_name                    varchar(50)                     not null                        comment '系统Act中文名称',
  sys_act_logic_name              varchar(50)                     not null                        comment '系统Act业务调用名称',
  description                     varchar(200) default '暂无 '     null                            comment '系统Act功能描述',
  trigger_example                  varchar(200) default '暂无 '     null                           comment '系统Act使用示例',
  classifier_tag_id               bigint                          not null                        comment '系统Act对应的自定义Act的标签Id',
  classifier_id                   varchar(40)                     not null                        comment '所属自定义Act的id'
)
comment '对话行为分类器标签与系统自带act的映射关系表';

create index dac_tag_map_system_act_index
	on ner_factory.dac_tag_map_system_act (id, classifier_id, classifier_tag_id);

drop table if exists ner_factory.dac_classifier_corpus;
create table ner_factory.dac_classifier_corpus
(
	id bigint auto_increment comment '语料id'
		primary key,
	classifier_tag_id bigint not null comment '语料所属分类标签id',
	classifier_id varchar(40) not null comment '语料所属分类器id',
	content varchar(200) not null comment '语料内容',
	is_context_sensitive int(2) default '0' not null comment '该语料是否上文敏感   0：表示不敏感  1表示敏感'
)
comment '对话行为分类器训练语料表';

create index dac_classifier_corpus_id_classifier_id_classifier_tag_id_index
	on ner_factory.dac_classifier_corpus (id, classifier_id, classifier_tag_id);


-- +migrate Down
drop table ner_factory.dac_classifier_info;
drop table ner_factory.dac_classifier_tag;
drop table ner_factory.dac_tag_map_system_act;
drop table ner_factory.dac_classifier_corpus;