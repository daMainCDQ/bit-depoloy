-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied


drop table if exists ner_factory.common_parser;
create table ner_factory.common_parser
(
  id                    bigint auto_increment
    primary key,
  logic_id              varchar(40)                                           not null
  comment '解析器唯一性逻辑标识 parser_id + @ + source_code',
  parser_id             varchar(36)                                           not null
  comment '解析器id',
  parser_name           varchar(50)                                           null
  comment '解析器的名称',
  description           varchar(512)                                          null
  comment '解析器简介',
  version               varchar(20)                                           null
  comment '解析器版本号',
  source_code           int(3)                                                not null
  comment '解析器来源 1: tde, 2: commonParser',
  request_format        varchar(255)                                          not null
  comment '解析器请求模板',
  parser_id_placeholder varchar(20)                                           not null
  comment '注册进来的parser 其request中标识parserId的字段名称',
  query_placeholder     varchar(100)                                          not null
  comment '注册进来的parser 其request中标识query的字段名称',
  service_name          varchar(50)                                           not null
  comment '解析器所在docker服务名称',
  service_port          varchar(10)                                           not null
  comment '解析器所在docker服务端口号',
  service_url           varchar(100)                                          not null
  comment '解析器资源路径',
  content_type          varchar(50) default 'application/json; charset=utf-8' null
  comment '解析器http调用Content-Type',
  created_time          timestamp default CURRENT_TIMESTAMP                   not null,
  user_id               varchar(40) default 'sys'                             not null,
  updated_time          timestamp                                             null
  on update CURRENT_TIMESTAMP,
  parser_type           int(3)                                                not null
  comment '解析器类型：0 表示系统默认提供common parser，1 表示普通用户基于系统parser注册的common parser'
)
  comment '通用解析器主信息表';

create index common_parser_index
  on ner_factory.common_parser (user_id, parser_id, logic_id);


drop table if exists ner_factory.common_slot_info;
create table ner_factory.common_slot_info
(
  id                bigint                                auto_increment               primary key,

  p_logic_id         varchar(40)                         not null            comment '解析器唯一性逻辑标识 parser_id + @ + source_code',

  slot_name         varchar(20)                         not null            comment '槽位标识符名称',

  slot_desc         varchar(200)                         null                comment '槽位中文名称以及其值内容说明',

  json_data_type    varchar(20)            null         default 'string'    comment '槽位值的Json数据类型',

  data_format       varchar(20)            null         default 'string'    comment '槽位值的具体业务数据类型',

  slot_value_path     varchar(100)                        not null            comment '从解析器Response中获取槽位值的get路径'

) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4  comment '通用解析器槽位信息表';

INSERT INTO ner_factory.common_parser (id, logic_id, parser_id, parser_name, description, version, source_code, request_format, parser_id_placeholder, query_placeholder, service_name, service_port, service_url, content_type, created_time, user_id, updated_time, parser_type) VALUES (1, 'number@1', 'number', '数字', '解析输入文本中的数字', '1', 1, '{"id": "","hasContext": true,"arguments":{},"query":""}', 'id', 'query', '', '10999', '/tde/usp/parse', 'application/json; charset=utf-8', '2019-12-26 20:20:26', 'sys', '2020-02-25 23:09:18', 0);
INSERT INTO ner_factory.common_parser (id, logic_id, parser_id, parser_name, description, version, source_code, request_format, parser_id_placeholder, query_placeholder, service_name, service_port, service_url, content_type, created_time, user_id, updated_time, parser_type) VALUES (2, 'bank_card@1', 'bank_card', '银行卡', '解析输入文本中的银行卡号', '1', 1, '{"id": "","hasContext": true,"arguments":{},"query":""}', 'id', 'query', '', '10999', '/tde/usp/parse', 'application/json; charset=utf-8', '2019-12-26 20:20:26', 'sys', '2020-02-25 23:09:18', 0);
INSERT INTO ner_factory.common_parser (id, logic_id, parser_id, parser_name, description, version, source_code, request_format, parser_id_placeholder, query_placeholder, service_name, service_port, service_url, content_type, created_time, user_id, updated_time, parser_type) VALUES (3, 'car_plate@1', 'car_plate', '车牌号', '解析输入文本中的车牌号', '1', 1, '{"id": "","hasContext": true,"arguments":{},"query":""}', 'id', 'query', '', '10999', '/tde/usp/parse', 'application/json; charset=utf-8', '2019-12-26 20:20:26', 'sys', '2020-02-25 23:09:18', 0);
INSERT INTO ner_factory.common_parser (id, logic_id, parser_id, parser_name, description, version, source_code, request_format, parser_id_placeholder, query_placeholder, service_name, service_port, service_url, content_type, created_time, user_id, updated_time, parser_type) VALUES (4, 'id_number@1', 'id_number', '身份证号', '解析输入文本中的身份证号', '1', 1, '{"id": "","hasContext": true,"arguments":{},"query":""}', 'id', 'query', '', '10999', '/tde/usp/parse', 'application/json; charset=utf-8', '2019-12-26 20:20:26', 'sys', '2020-02-25 23:09:17', 0);
INSERT INTO ner_factory.common_parser (id, logic_id, parser_id, parser_name, description, version, source_code, request_format, parser_id_placeholder, query_placeholder, service_name, service_port, service_url, content_type, created_time, user_id, updated_time, parser_type) VALUES (5, 'quantity@1', 'quantity', '数量/重量', '解析文本中的数量词和重量词', '1', 1, '{"id": "","hasContext": true,"arguments":{},"query":""}', 'id', 'query', '', '10999', '/tde/usp/parse', 'application/json; charset=utf-8', '2019-12-26 20:20:26', 'sys', '2020-02-25 23:09:17', 0);
INSERT INTO ner_factory.common_parser (id, logic_id, parser_id, parser_name, description, version, source_code, request_format, parser_id_placeholder, query_placeholder, service_name, service_port, service_url, content_type, created_time, user_id, updated_time, parser_type) VALUES (6, 'person-number@1', 'person-number', '人数', '解析文本中的人数信息', '1', 1, '{"id": "","hasContext": true,"arguments":{},"query":""}', 'id', 'query', '', '10999', '/tde/usp/parse', 'application/json; charset=utf-8', '2019-12-26 20:20:26', 'sys', '2020-02-25 23:09:17', 0);
INSERT INTO ner_factory.common_parser (id, logic_id, parser_id, parser_name, description, version, source_code, request_format, parser_id_placeholder, query_placeholder, service_name, service_port, service_url, content_type, created_time, user_id, updated_time, parser_type) VALUES (7, 'money@1', 'money', '金额', '解析文本中的金额信息', '1', 1, '{"id": "","hasContext": true,"arguments":{},"query":""}', 'id', 'query', '', '10999', '/tde/usp/parse', 'application/json; charset=utf-8', '2019-12-26 20:20:26', 'sys', '2020-02-25 23:09:18', 0);
INSERT INTO ner_factory.common_parser (id, logic_id, parser_id, parser_name, description, version, source_code, request_format, parser_id_placeholder, query_placeholder, service_name, service_port, service_url, content_type, created_time, user_id, updated_time, parser_type) VALUES (8, 'age@1', 'age', '年龄', '解析文本中的年龄信息', '1', 1, '{"id": "","hasContext": true,"arguments":{},"query":""}', 'id', 'query', '', '10999', '/tde/usp/parse', 'application/json; charset=utf-8', '2019-12-26 20:20:26', 'sys', '2020-02-25 23:09:17', 0);
INSERT INTO ner_factory.common_parser (id, logic_id, parser_id, parser_name, description, version, source_code, request_format, parser_id_placeholder, query_placeholder, service_name, service_port, service_url, content_type, created_time, user_id, updated_time, parser_type) VALUES (9, 'province@1', 'province', '省份', '解析文本中所存在的中国省级行政区区名', '1', 1, '{"id": "","hasContext": true,"arguments":{},"query":""}', 'id', 'query', '', '10999', '/tde/usp/parse', 'application/json; charset=utf-8', '2019-12-26 20:20:26', 'sys', '2020-02-25 23:09:17', 0);
INSERT INTO ner_factory.common_parser (id, logic_id, parser_id, parser_name, description, version, source_code, request_format, parser_id_placeholder, query_placeholder, service_name, service_port, service_url, content_type, created_time, user_id, updated_time, parser_type) VALUES (10, 'ccity@1', 'ccity', '市级城市', '解析文本中所存在的中国城市名', '1', 1, '{"id": "","hasContext": true,"arguments":{},"query":""}', 'id', 'query', '', '10999', '/tde/usp/parse', 'application/json; charset=utf-8', '2019-12-26 20:20:26', 'sys', '2020-02-25 23:09:17', 0);
INSERT INTO ner_factory.common_parser (id, logic_id, parser_id, parser_name, description, version, source_code, request_format, parser_id_placeholder, query_placeholder, service_name, service_port, service_url, content_type, created_time, user_id, updated_time, parser_type) VALUES (11, 'organization@1', 'organization', '机构', '解析文本中所存在的组织机构名', '1', 1, '{"id": "","hasContext": true,"arguments":{},"query":""}', 'id', 'query', '', '10999', '/tde/usp/parse', 'application/json; charset=utf-8', '2019-12-26 20:20:26', 'sys', '2020-02-25 23:09:17', 0);
INSERT INTO ner_factory.common_parser (id, logic_id, parser_id, parser_name, description, version, source_code, request_format, parser_id_placeholder, query_placeholder, service_name, service_port, service_url, content_type, created_time, user_id, updated_time, parser_type) VALUES (12, 'university@1', 'university', '中国大学', '解析文本中所存在的中国大学名', '1', 1, '{"id": "","hasContext": true,"arguments":{},"query":""}', 'id', 'query', '', '10999', '/tde/usp/parse', 'application/json; charset=utf-8', '2019-12-26 20:20:27', 'sys', '2020-02-25 23:09:18', 0);
INSERT INTO ner_factory.common_parser (id, logic_id, parser_id, parser_name, description, version, source_code, request_format, parser_id_placeholder, query_placeholder, service_name, service_port, service_url, content_type, created_time, user_id, updated_time, parser_type) VALUES (13, 'surname@1', 'surname', '姓氏', '解析文本中的中文姓氏', '1', 1, '{"id": "","hasContext": true,"arguments":{},"query":""}', 'id', 'query', '', '10999', '/tde/usp/parse', 'application/json; charset=utf-8', '2019-12-26 20:20:27', 'sys', '2020-02-25 23:09:18', 0);
INSERT INTO ner_factory.common_parser (id, logic_id, parser_id, parser_name, description, version, source_code, request_format, parser_id_placeholder, query_placeholder, service_name, service_port, service_url, content_type, created_time, user_id, updated_time, parser_type) VALUES (14, 'person-name@1', 'person-name', '姓名', '解析文本中的中文人名', '1', 1, '{"id": "","hasContext": true,"arguments":{},"query":""}', 'id', 'query', '', '10999', '/tde/usp/parse', 'application/json; charset=utf-8', '2019-12-26 20:20:27', 'sys', '2020-02-25 23:09:18', 0);
INSERT INTO ner_factory.common_parser (id, logic_id, parser_id, parser_name, description, version, source_code, request_format, parser_id_placeholder, query_placeholder, service_name, service_port, service_url, content_type, created_time, user_id, updated_time, parser_type) VALUES (15, 'kinsfolk@1', 'kinsfolk', '亲属关系', '解析文本中所存在的亲属关系', '1', 1, '{"id": "","hasContext": true,"arguments":{},"query":""}', 'id', 'query', '', '10999', '/tde/usp/parse', 'application/json; charset=utf-8', '2019-12-26 20:20:27', 'sys', '2020-02-25 23:09:18', 0);
INSERT INTO ner_factory.common_parser (id, logic_id, parser_id, parser_name, description, version, source_code, request_format, parser_id_placeholder, query_placeholder, service_name, service_port, service_url, content_type, created_time, user_id, updated_time, parser_type) VALUES (16, 'mailing_address@1', 'mailing_address', '邮寄地址', '解析文本中详细的邮寄地址', '1', 1, '{
	"id": "",
	"hasContext":true,
	"arguments":{
	"needDetail" : true
	},
	"query": ""
}', 'id', 'query', '', '10999', '/tde/usp/parse', 'application/json; charset=utf-8', '2019-12-30 18:21:27', 'sys', '2020-02-25 23:09:18', 0);
INSERT INTO ner_factory.common_parser (id, logic_id, parser_id, parser_name, description, version, source_code, request_format, parser_id_placeholder, query_placeholder, service_name, service_port, service_url, content_type, created_time, user_id, updated_time, parser_type) VALUES (17, 'transfer@1', 'transfer', '转账信息', '解析一句文本中涉及转账的相关的元素，包括出账方、入账方、金额等信息。


', '1', 1, '{"id": "","hasContext": true,"arguments":{},"query":""}', 'id', 'query', '', '10999', '/tde/usp/parse', 'application/json; charset=utf-8', '2020-01-06 11:18:19', 'sys', '2020-02-25 23:09:18', 0);
INSERT INTO ner_factory.common_parser (id, logic_id, parser_id, parser_name, description, version, source_code, request_format, parser_id_placeholder, query_placeholder, service_name, service_port, service_url, content_type, created_time, user_id, updated_time, parser_type) VALUES (18, 'chrono@1', 'chrono', '超级时间', '本解析器用于识别一句话中间的时间信息。
包括“时间点”和“时间段”。
1.时间点同时给出ISO_DATE和ISO_TIME格式的规范形式。
2.“时间点”会区分“起始”和“截至”的类别，如识别“从...到...”
3.支持“年、月、日、时、分、秒”，也支持“世纪、年代、季度、旬、刻、周”等的识别。
4.支持简单计算，如：“半个小时后”，“晚上8点，还是提前半小时吧”
5. 支持农历时间识别，如“农历12月初八”
6. 支持节日的识别，如“国庆节”、“清明节”等', '1', 1, '{"id": "","hasContext": true,"arguments":{},"query":""}', 'id', 'query', '', '10999', '/tde/usp/parse', 'application/json; charset=utf-8', '2020-01-06 11:34:08', 'sys', '2020-02-25 23:09:18', 0);
INSERT INTO ner_factory.common_parser (id, logic_id, parser_id, parser_name, description, version, source_code, request_format, parser_id_placeholder, query_placeholder, service_name, service_port, service_url, content_type, created_time, user_id, updated_time, parser_type) VALUES (19, 'phone@1', 'phone', '大陆手机号码', '解析文本中的移动手机号码（限大陆）。', '1', 1, '{
"id": "",
"hasContext":true,
"arguments":{
"regions" : ["mainland"],
"types" : ["mobile"]
},
"query": ""
}', 'id', 'query', '', '10999', '/tde/usp/parse', 'application/json; charset=utf-8', '2020-01-06 11:41:09', 'sys', '2020-02-25 23:09:18', 0);
INSERT INTO ner_factory.common_parser (id, logic_id, parser_id, parser_name, description, version, source_code, request_format, parser_id_placeholder, query_placeholder, service_name, service_port, service_url, content_type, created_time, user_id, updated_time, parser_type) VALUES (20, 'transport@1', 'transport', '出发到达地', '解析出发与到达地', '1', 1, '{"id": "","hasContext": true,"arguments":{},"query":""}', 'id', 'query', '', '10999', '/tde/usp/parse', 'application/json; charset=utf-8', '2020-02-12 11:32:13', 'sys', '2020-02-25 23:09:18', 0);
INSERT INTO ner_factory.common_parser (id, logic_id, parser_id, parser_name, description, version, source_code, request_format, parser_id_placeholder, query_placeholder, service_name, service_port, service_url, content_type, created_time, user_id, updated_time, parser_type) VALUES (21, 'abc7e46a540011ea8d580242c0a8d012', 'transport', '测试只解出发地', '解析出发地', '1', 1, '{"id": "","hasContext": true,"arguments":{},"query":""}', 'id', 'query', '', '10999', '/tde/usp/parse', 'application/json; charset=utf-8', '2020-02-22 03:28:00', 'sys', '2020-02-25 23:09:18', 0);

INSERT INTO ner_factory.common_slot_info (id, p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path) VALUES (1, 'number@1', 'number', 'NUMBER：数字类型，支持整数与小数，小数建议在10位以内。例：3.14159', 'string', 'string', '$.informs[0].value.displayText');
INSERT INTO ner_factory.common_slot_info (id, p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path) VALUES (2, 'bank_card@1', 'bank_card', 'BANK_CARD：数字类型', 'string', 'string', '$.informs[0].value.displayText');
INSERT INTO ner_factory.common_slot_info (id, p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path) VALUES (3, 'car_plate@1', 'car_plate', 'car_plate：字符串类型。例：沪A12345', 'string', 'string', '$.informs[0].value.displayText');
INSERT INTO ner_factory.common_slot_info (id, p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path) VALUES (4, 'id_number@1', 'id_number', '身份证号', 'string', 'string', '$.informs[0].value.displayText');
INSERT INTO ner_factory.common_slot_info (id, p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path) VALUES (5, 'quantity@1', 'quantity', 'QUANTITY: 可以识别一个具体的数量词，如：”吃了三个包子“，识别”3个“；2. 识别出重量词，如：“一本书两公斤”，识别出“两公斤”，并自动转为2公斤.（注：后续根据需求增加）', 'string', 'string', '$.informs[0].value.displayText');
INSERT INTO ner_factory.common_slot_info (id, p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path) VALUES (6, 'person-number@1', 'person-number', 'PERSON_NUMBER：可以识别文本中的人数信息。如：“我们5个人，7点到”，识别为“5位”。', 'string', 'string', '$.informs[0].value.displayText');
INSERT INTO ner_factory.common_slot_info (id, p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path) VALUES (7, 'money@1', 'money', 'MONEY：识别文本中存在的金额信息，并支持模糊说法。
金额值由两部分组成：Value+Range
value格式为xxx元；
Range的值有4种情况：（1）不存在；（2）R，代表“不少于”；（3）L，代表“不多于”；（4）LR，代表“左右”
如：
1."办了一张会员卡花了九百多“，识别结果为："900R",表示实际所指比900元多。
', 'string', 'string', '$.informs[0].value.money.entities[0]');
INSERT INTO ner_factory.common_slot_info (id, p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path) VALUES (8, 'age@1', 'age', 'AGE: 识别文本中存在的年龄信息，并支持模糊说法。
金额值由两部分组成：Value+Range
value格式为xxx岁；
Range的值有4种情况：（1）不存在；（2）R，代表“不少于”；（3）L，代表“不多于”；（4）LR，代表“左右”如：
1.“表妹今年四岁半了”，识别为“四岁R“，表示比四岁大。
2. “还不到18岁”，识别“18L"，表示实际所指比18岁小。', 'string', 'string', '$.informs[0].value.displayText');
INSERT INTO ner_factory.common_slot_info (id, p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path) VALUES (9, 'province@1', 'province', 'PROVINCE: 识别文本中的中国省级行政区名称，包括23个省、5个自治区、4个直辖市、2个特别行政区。如：“阿里巴巴在香港上市”，识别“香港”', 'string', 'string', '$.informs[0].value.displayText');
INSERT INTO ner_factory.common_slot_info (id, p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path) VALUES (10, 'ccity@1', 'ccity', 'CCITY：识别文本中的中国城市名称，包含地级市和县级市。如：“河南省安阳市曲沟镇”，识别“安阳”。
', 'string', 'string', '$.informs[0].value.displayText');
INSERT INTO ner_factory.common_slot_info (id, p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path) VALUES (11, 'organization@1', 'organization', 'ORGANIZATION：识别文本中存在的组织机构名称，既包括公共事业机构，也包括商业机构。如：“小张28岁，在华为工作”，抽取“华为”。
', 'string', 'string', '$.informs[0].value.displayText');
INSERT INTO ner_factory.common_slot_info (id, p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path) VALUES (12, 'university@1', 'university', 'UNIVERSITY: 识别文本中存在的中国大学的名字。如：“来自复旦大学的辩手”，抽取“复旦大学”。暂不支持大学简称。', 'string', 'string', '$.informs[0].value.displayText');
INSERT INTO ner_factory.common_slot_info (id, p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path) VALUES (13, 'surname@1', 'surname', 'SURNAME: 识别文本中存在的中文姓氏。如："我是欧若拉”，抽取“欧”。
', 'string', 'string', '$.informs[0].value.displayText');
INSERT INTO ner_factory.common_slot_info (id, p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path) VALUES (14, 'person-name@1', 'person-name', 'person-name: 识别文本中的中文人名，如：“新加坡总理李光耀”，识别出“李光耀。当文本中出现多个人名时，识别靠后的那个。', 'string', 'string', '$.informs[0].value.displayText');
INSERT INTO ner_factory.common_slot_info (id, p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path) VALUES (15, 'kinsfolk@1', 'kinsfolk', 'KINSFOLK： 识别53种亲属关系。如：“李四是我表兄，“抽取“表兄”。”张玲是我前妻“，抽取”前妻“', 'string', 'string', '$.informs[0].value.displayText');
INSERT INTO ner_factory.common_slot_info (id, p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path) VALUES (16, 'mailing_address@1', 'province', '省份', 'string', 'string', '$.informs[0].value.mailing-address.l1');
INSERT INTO ner_factory.common_slot_info (id, p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path) VALUES (17, 'mailing_address@1', 'city', '市区', 'string', 'string', '$.informs[0].value.mailing-address.l2');
INSERT INTO ner_factory.common_slot_info (id, p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path) VALUES (18, 'mailing_address@1', 'counties', '区县', 'string', 'string', '$.informs[0].value.mailing-address.l3');
INSERT INTO ner_factory.common_slot_info (id, p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path) VALUES (19, 'mailing_address@1', 'details', '其它详情', 'string', 'string', '$.informs[0].value.mailing-address.detail');
INSERT INTO ner_factory.common_slot_info (id, p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path) VALUES (20, 'transfer@1', 'source
', '出账方
', 'string', 'string', '$.informs[0].value.transfer.source');
INSERT INTO ner_factory.common_slot_info (id, p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path) VALUES (21, 'transfer@1', 'target
', '入账方
', 'string', 'string', '$.informs[0].value.transfer.target');
INSERT INTO ner_factory.common_slot_info (id, p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path) VALUES (22, 'transfer@1', 'amount
', '金额
', 'string', 'string', '$.informs[0].value.transfer.amount');
INSERT INTO ner_factory.common_slot_info (id, p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path) VALUES (23, 'transfer@1', 'unit', '金额单位：元', 'string', 'string', '$.informs[0].value.transfer.unit');
INSERT INTO ner_factory.common_slot_info (id, p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path) VALUES (24, 'chrono@1', 'from_time', '起始时间点', 'string', 'string', '$.informs[0].value.chrono.from_time.items[0].ISO_TIME');
INSERT INTO ner_factory.common_slot_info (id, p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path) VALUES (25, 'chrono@1', 'to_time', '截至时间点', 'string', 'string', '$.informs[0].value.chrono.to_time.items[0].ISO_TIME');
INSERT INTO ner_factory.common_slot_info (id, p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path) VALUES (26, 'chrono@1', 'displayText', '基本解析', 'string', 'string', '$.informs[0].value.displayText');
INSERT INTO ner_factory.common_slot_info (id, p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path) VALUES (27, 'phone@1', 'phoneNumber', '电话号码', 'string', 'string', '$.informs[0].value.displayText');
INSERT INTO ner_factory.common_slot_info (id, p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path) VALUES (28, 'transport@1', 'fromPlace', '出发地', 'string', 'string', '$.informs[0].value.transport.from');
INSERT INTO ner_factory.common_slot_info (id, p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path) VALUES (29, 'transport@1', 'toPlace', '到达地', 'string', 'string', '$.informs[0].value.transport.to');
INSERT INTO ner_factory.common_slot_info (id, p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path) VALUES (30, 'abc7e46a540011ea8d580242c0a8d012', 'from', '出发点', 'string', 'string', '$.informs[0].value.transport.from');




-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

drop table if exists ner_factory.common_parser;
create table ner_factory.common_parser
(
  id              bigint auto_increment
    primary key,
  parser_id       varchar(36)                         not null
  comment '解析器id',
  name            varchar(50)                         null
  comment '解析器的名称',
  source_id       int(3)                              not null
  comment '解析器来源 1: tde, 2: commonParser',
  description     varchar(255)                        null
  comment '解析器简介',
  version         varchar(20)                         null
  comment '解析器版本号',
  service_name    varchar(50)                         null
  comment '解析器所在docker服务名称',
  request_format  varchar(255)                        not null
  comment '解析器请求模板',
  response_format varchar(255)                        not null
  comment '解析器响应解析路径',
  created_time    timestamp default CURRENT_TIMESTAMP not null,
  updated_time    timestamp                           null
  on update CURRENT_TIMESTAMP,
  sort            int                                 null,
  type            int                                 null
)
  comment '公共解析器';

INSERT INTO ner_factory.common_parser (id, parser_id, name, source_id, description, version, service_name, request_format, response_format, created_time, updated_time, sort, type) VALUES (1, 'number', '数字', 1, '数据通用解析器', '', 'tde', '{"id": "","hasContext": true,"arguments":{},"query":""}', 'informs@^@value@^@displayText', '2019-11-18 13:16:15', '2019-11-18 13:50:19', null, null);
INSERT INTO ner_factory.common_parser (id, parser_id, name, source_id, description, version, service_name, request_format, response_format, created_time, updated_time, sort, type) VALUES (2, 'bank_card', '银行卡', 1, '银行卡解析器', '', 'tde', '{"id": "","hasContext": true,"arguments":{},"query":""}', 'informs@^@value@^@displayText', '2019-11-18 13:48:23', '2019-11-18 18:24:30', null, null);
INSERT INTO ner_factory.common_parser (id, parser_id, name, source_id, description, version, service_name, request_format, response_format, created_time, updated_time, sort, type) VALUES (3, 'car_plate', '车牌号', 1, '车牌号解析器', '', 'tde', '{"id": "","hasContext": true,"arguments":{},"query":""}', 'informs@^@value@^@displayText', '2019-11-18 13:49:28', '2019-11-18 18:24:30', null, null);
INSERT INTO ner_factory.common_parser (id, parser_id, name, source_id, description, version, service_name, request_format, response_format, created_time, updated_time, sort, type) VALUES (4, 'id_number', '身份证号', 1, '身份证号解析器', '', 'tde', '{"id": "","hasContext": true,"arguments":{},"query":""}', 'informs@^@value@^@displayText', '2019-11-18 13:49:28', '2019-11-18 18:24:30', null, null);
INSERT INTO ner_factory.common_parser (id, parser_id, name, source_id, description, version, service_name, request_format, response_format, created_time, updated_time, sort, type) VALUES (5, 'quantity', '数量/重量', 1, '数量/重量解析器', '', 'tde', '{"id": "","hasContext": true,"arguments":{},"query":""}', 'informs@^@value@^@displayText', '2019-11-18 13:49:28', '2019-11-18 18:24:30', null, null);
INSERT INTO ner_factory.common_parser (id, parser_id, name, source_id, description, version, service_name, request_format, response_format, created_time, updated_time, sort, type) VALUES (6, 'person-number', '人数', 1, '人数解析器', '', 'tde', '{"id": "","hasContext": true,"arguments":{},"query":""}', 'informs@^@value@^@displayText', '2019-11-18 13:49:29', '2019-11-18 18:24:30', null, null);
INSERT INTO ner_factory.common_parser (id, parser_id, name, source_id, description, version, service_name, request_format, response_format, created_time, updated_time, sort, type) VALUES (7, 'money', '金额', 1, '金额解析器', '', 'tde', '{"id": "","hasContext": true,"arguments":{},"query":""}', 'informs@^@value@^@displayText', '2019-11-18 13:49:29', '2019-11-18 18:24:30', null, null);
INSERT INTO ner_factory.common_parser (id, parser_id, name, source_id, description, version, service_name, request_format, response_format, created_time, updated_time, sort, type) VALUES (8, 'age', '年龄', 1, '年龄解析器', '', 'tde', '{"id": "","hasContext": true,"arguments":{},"query":""}', 'informs@^@value@^@displayText', '2019-11-18 13:49:29', '2019-11-18 18:24:30', null, null);
INSERT INTO ner_factory.common_parser (id, parser_id, name, source_id, description, version, service_name, request_format, response_format, created_time, updated_time, sort, type) VALUES (9, 'province', '省份', 1, '省份解析器', '', 'tde', '{"id": "","hasContext": true,"arguments":{},"query":""}', 'informs@^@value@^@displayText', '2019-11-18 13:49:29', '2019-11-18 18:24:30', null, null);
INSERT INTO ner_factory.common_parser (id, parser_id, name, source_id, description, version, service_name, request_format, response_format, created_time, updated_time, sort, type) VALUES (10, 'ccity', '市级城市', 1, '市级城市解析器', '', 'tde', '{"id": "","hasContext": true,"arguments":{},"query":""}', 'informs@^@value@^@displayText', '2019-11-18 13:49:29', '2019-11-18 18:24:30', null, null);
INSERT INTO ner_factory.common_parser (id, parser_id, name, source_id, description, version, service_name, request_format, response_format, created_time, updated_time, sort, type) VALUES (11, 'organization', '机构', 1, '机构解析器', '', 'tde', '{"id": "","hasContext": true,"arguments":{},"query":""}', 'informs@^@value@^@displayText', '2019-11-18 13:49:29', '2019-11-18 18:24:30', null, null);
INSERT INTO ner_factory.common_parser (id, parser_id, name, source_id, description, version, service_name, request_format, response_format, created_time, updated_time, sort, type) VALUES (12, 'university', '中国大学', 1, '中国大学解析器', '', 'tde', '{"id": "","hasContext": true,"arguments":{},"query":""}', 'informs@^@value@^@displayText', '2019-11-18 13:49:29', '2019-11-18 18:24:30', null, null);
INSERT INTO ner_factory.common_parser (id, parser_id, name, source_id, description, version, service_name, request_format, response_format, created_time, updated_time, sort, type) VALUES (13, 'surname', '姓氏', 1, '姓氏解析器', '', 'tde', '{"id": "","hasContext": true,"arguments":{},"query":""}', 'informs@^@value@^@displayText', '2019-11-18 13:49:29', '2019-11-18 18:24:30', null, null);
INSERT INTO ner_factory.common_parser (id, parser_id, name, source_id, description, version, service_name, request_format, response_format, created_time, updated_time, sort, type) VALUES (14, 'person-name', '姓名', 1, '姓名解析器', '', 'tde', '{"id": "","hasContext": true,"arguments":{},"query":""}', 'informs@^@value@^@displayText', '2019-11-18 13:49:29', '2019-11-18 18:24:30', null, null);
INSERT INTO ner_factory.common_parser (id, parser_id, name, source_id, description, version, service_name, request_format, response_format, created_time, updated_time, sort, type) VALUES (15, 'kinsfolk', '亲属关系', 1, '亲属关系解析器', '', 'tde', '{"id": "","hasContext": true,"arguments":{},"query":""}', 'informs@^@value@^@displayText', '2019-11-18 13:49:29', '2019-11-18 18:24:30', null, null);

