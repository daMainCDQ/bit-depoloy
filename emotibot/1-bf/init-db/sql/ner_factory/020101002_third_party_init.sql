-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied


drop table if exists ner_factory.third_party_request_info;
create table ner_factory.third_party_request_info
(
	id bigint auto_increment comment '第三方request记录id'
		primary key,
	parser_id varchar(40) not null comment '外部解析器id',
	http_method varchar(16) default 'post' not null comment 'http请求方法',
	http_url  varchar(128)  not null comment '请求链接',
	body_template varchar(1500) not null comment '请求体内容',
	content_type varchar(100) default 'application/json' not null comment '文本类型',
	query_key_name varchar(20) not null comment '存储用户输入文本的字段名称',
	query_key_req_path varchar(128) not null comment 'query_key在request中的位置路径'
)
comment '外部解析器请求体信息表';

create index request_info_index
	on ner_factory.third_party_request_info (id, parser_id);


drop table if exists ner_factory.third_party_dynamic_req_key;
create table ner_factory.third_party_dynamic_req_key
(
	id bigint auto_increment comment '第三方请求动态key id'
		primary key,
	parser_id         varchar(40) not null comment '外部解析器id',
	key_name          varchar(20) not null comment '请求体中动态字段名称',
	reqest_path       varchar(128)  not null comment '动态字段值填写位置',
	key_default_value varchar(200) not null comment '动态字段默认填写值'
)
comment '外部解析器请求体动态键信息表';

create index dynamic_req_key
	on ner_factory.third_party_dynamic_req_key (id, parser_id);


drop table if exists ner_factory.third_party_slot_resp_map;
create table ner_factory.third_party_slot_resp_map
(
	id bigint auto_increment comment 'id'
		primary key,
	slot_id bigint not null comment '外部解析器槽位id',
	parser_id varchar(40) not null comment '外部解析器id',
	response_path varchar(128) not null comment '槽位值在response中所在路径',
  data_type varchar (16) default 'string' not null comment '槽位值数据类型'
)
comment '外部解析器槽位与Response路径映射关系表';

create index slot_resp_index
	on ner_factory.third_party_slot_resp_map (id, slot_id, parser_id);


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

drop table ner_factory.third_party_request_info;
drop table ner_factory.third_party_dynamic_req_key;
drop table ner_factory.third_party_slot_resp_map;