-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
create table rule_default_slot
(
  id           bigint auto_increment
    primary key,
  parser_id    varchar(32)                            null comment '关联解析器的id',
  slot_name    varchar(64)                            null comment '关联解析器的槽位名称',
  created_time timestamp    default CURRENT_TIMESTAMP not null,
  updated_time timestamp                              null on update CURRENT_TIMESTAMP
)
  comment '规则解析器的默认槽位';

create index rule_default_slot_i_parser_id
  on rule_default_slot (parser_id);


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
