-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
alter table parser
	add use_extractor int(2) default 0 not null comment '是否使用预处理解析器 0: 使用 1: 不使用';


create table rule_v2
(
  id           bigint auto_increment
    primary key,
  parser_id    varchar(32)                         not null comment '关联解析器的id',
  content      varchar(1024)                       not null comment '规则的内容',
  type         int                                 not null comment '正反例类型',
  sort         int                                 not null comment '规则的顺序',
  created_time timestamp default CURRENT_TIMESTAMP not null,
  updated_time timestamp                           null on update CURRENT_TIMESTAMP
);


-- auto-generated definition
create table rule_group_slot_relation_map
(
  id           int auto_increment
    primary key,
  parser_id    varchar(32)                         not null,
  rule_id      int                                 not null comment '规则的id',
  group_index  int(4)                              not null comment '规则组的顺序',
  slot_name     varchar(128)                        not null comment '关联的槽位名称',
  created_time timestamp default CURRENT_TIMESTAMP not null,
  updated_time timestamp                           null on update CURRENT_TIMESTAMP
);

create index parser_id
  on rule_group_slot_relation_map (parser_id);


-- auto-generated definition
create table assembly_model_extractor
(
  id                  bigint auto_increment
    primary key,
  parser_id           varchar(32)                         not null comment '关联解析器的id',
  extractor_parser_id varchar(32)                         not null comment '关联解析器的id',
  extractor_type      int(4)                              not null comment '预处理解析器类型 0: 命名实体模型(parser) 1: 解析编辑器 (自定义parser)',
  extractor_parser_type  int(4)                           not null comment '预处理解析器的解析器类型',
  extractor_slot_name   varchar(128)                      null comment '预处理解析器的槽位名称',
  slot_alias           varchar(128)                       null comment '槽位的别名',
  replacement          varchar(128)                       null comment '槽位的占位符',
  created_time        timestamp default CURRENT_TIMESTAMP not null,
  updated_time        timestamp                           null on update CURRENT_TIMESTAMP,
  index(parser_id)
);


alter table corpus
	add type int(4) default 0 not null comment '0: 正例 1: 反例' after flag;

alter table corpus modify updated_time timestamp null on update CURRENT_TIMESTAMP after type;

alter table corpus_history
	add type int(4) default 0 not null comment '0 正例 1反例' after parser_id;

alter table corpus_history modify updated_time timestamp null on update CURRENT_TIMESTAMP after type;


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
