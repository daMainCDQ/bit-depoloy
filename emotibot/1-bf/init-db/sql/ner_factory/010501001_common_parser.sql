-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

drop table if exists ner_factory.common_parser;
create table ner_factory.common_parser
(
  id              bigint auto_increment
    primary key,
  parser_id       varchar(36)                         not null
  comment '解析器id',
  name            varchar(50)                         null
  comment '解析器的名称',
  source_id       int(3)                              not null
  comment '解析器来源 1: tde, 2: commonParser',
  description     varchar(255)                        null
  comment '解析器简介',
  version         varchar(20)                         null
  comment '解析器版本号',
  service_name    varchar(50)                         null
  comment '解析器所在docker服务名称',
  request_format  varchar(255)                        not null
  comment '解析器请求模板',
  response_format varchar(255)                        not null
  comment '解析器响应解析路径',
  created_time    timestamp default CURRENT_TIMESTAMP not null,
  updated_time    timestamp                           null
  on update CURRENT_TIMESTAMP,
  sort            int                                 null,
  type            int                                 null
)
  comment '公共解析器';

INSERT INTO ner_factory.common_parser (id, parser_id, name, source_id, description, version, service_name, request_format, response_format, created_time, updated_time, sort, type) VALUES (1, 'number', '数字', 1, '数据通用解析器', '', 'tde', '{"id": "","hasContext": true,"arguments":{},"query":""}', 'informs@^@value@^@displayText', '2019-11-18 13:16:15', '2019-11-18 13:50:19', null, null);
INSERT INTO ner_factory.common_parser (id, parser_id, name, source_id, description, version, service_name, request_format, response_format, created_time, updated_time, sort, type) VALUES (2, 'bank_card', '银行卡', 1, '银行卡解析器', '', 'tde', '{"id": "","hasContext": true,"arguments":{},"query":""}', 'informs@^@value@^@displayText', '2019-11-18 13:48:23', '2019-11-18 18:24:30', null, null);
INSERT INTO ner_factory.common_parser (id, parser_id, name, source_id, description, version, service_name, request_format, response_format, created_time, updated_time, sort, type) VALUES (3, 'car_plate', '车牌号', 1, '车牌号解析器', '', 'tde', '{"id": "","hasContext": true,"arguments":{},"query":""}', 'informs@^@value@^@displayText', '2019-11-18 13:49:28', '2019-11-18 18:24:30', null, null);
INSERT INTO ner_factory.common_parser (id, parser_id, name, source_id, description, version, service_name, request_format, response_format, created_time, updated_time, sort, type) VALUES (4, 'id_number', '身份证号', 1, '身份证号解析器', '', 'tde', '{"id": "","hasContext": true,"arguments":{},"query":""}', 'informs@^@value@^@displayText', '2019-11-18 13:49:28', '2019-11-18 18:24:30', null, null);
INSERT INTO ner_factory.common_parser (id, parser_id, name, source_id, description, version, service_name, request_format, response_format, created_time, updated_time, sort, type) VALUES (5, 'quantity', '数量/重量', 1, '数量/重量解析器', '', 'tde', '{"id": "","hasContext": true,"arguments":{},"query":""}', 'informs@^@value@^@displayText', '2019-11-18 13:49:28', '2019-11-18 18:24:30', null, null);
INSERT INTO ner_factory.common_parser (id, parser_id, name, source_id, description, version, service_name, request_format, response_format, created_time, updated_time, sort, type) VALUES (6, 'person-number', '人数', 1, '人数解析器', '', 'tde', '{"id": "","hasContext": true,"arguments":{},"query":""}', 'informs@^@value@^@displayText', '2019-11-18 13:49:29', '2019-11-18 18:24:30', null, null);
INSERT INTO ner_factory.common_parser (id, parser_id, name, source_id, description, version, service_name, request_format, response_format, created_time, updated_time, sort, type) VALUES (7, 'money', '金额', 1, '金额解析器', '', 'tde', '{"id": "","hasContext": true,"arguments":{},"query":""}', 'informs@^@value@^@displayText', '2019-11-18 13:49:29', '2019-11-18 18:24:30', null, null);
INSERT INTO ner_factory.common_parser (id, parser_id, name, source_id, description, version, service_name, request_format, response_format, created_time, updated_time, sort, type) VALUES (8, 'age', '年龄', 1, '年龄解析器', '', 'tde', '{"id": "","hasContext": true,"arguments":{},"query":""}', 'informs@^@value@^@displayText', '2019-11-18 13:49:29', '2019-11-18 18:24:30', null, null);
INSERT INTO ner_factory.common_parser (id, parser_id, name, source_id, description, version, service_name, request_format, response_format, created_time, updated_time, sort, type) VALUES (9, 'province', '省份', 1, '省份解析器', '', 'tde', '{"id": "","hasContext": true,"arguments":{},"query":""}', 'informs@^@value@^@displayText', '2019-11-18 13:49:29', '2019-11-18 18:24:30', null, null);
INSERT INTO ner_factory.common_parser (id, parser_id, name, source_id, description, version, service_name, request_format, response_format, created_time, updated_time, sort, type) VALUES (10, 'ccity', '市级城市', 1, '市级城市解析器', '', 'tde', '{"id": "","hasContext": true,"arguments":{},"query":""}', 'informs@^@value@^@displayText', '2019-11-18 13:49:29', '2019-11-18 18:24:30', null, null);
INSERT INTO ner_factory.common_parser (id, parser_id, name, source_id, description, version, service_name, request_format, response_format, created_time, updated_time, sort, type) VALUES (11, 'organization', '机构', 1, '机构解析器', '', 'tde', '{"id": "","hasContext": true,"arguments":{},"query":""}', 'informs@^@value@^@displayText', '2019-11-18 13:49:29', '2019-11-18 18:24:30', null, null);
INSERT INTO ner_factory.common_parser (id, parser_id, name, source_id, description, version, service_name, request_format, response_format, created_time, updated_time, sort, type) VALUES (12, 'university', '中国大学', 1, '中国大学解析器', '', 'tde', '{"id": "","hasContext": true,"arguments":{},"query":""}', 'informs@^@value@^@displayText', '2019-11-18 13:49:29', '2019-11-18 18:24:30', null, null);
INSERT INTO ner_factory.common_parser (id, parser_id, name, source_id, description, version, service_name, request_format, response_format, created_time, updated_time, sort, type) VALUES (13, 'surname', '姓氏', 1, '姓氏解析器', '', 'tde', '{"id": "","hasContext": true,"arguments":{},"query":""}', 'informs@^@value@^@displayText', '2019-11-18 13:49:29', '2019-11-18 18:24:30', null, null);
INSERT INTO ner_factory.common_parser (id, parser_id, name, source_id, description, version, service_name, request_format, response_format, created_time, updated_time, sort, type) VALUES (14, 'person-name', '姓名', 1, '姓名解析器', '', 'tde', '{"id": "","hasContext": true,"arguments":{},"query":""}', 'informs@^@value@^@displayText', '2019-11-18 13:49:29', '2019-11-18 18:24:30', null, null);
INSERT INTO ner_factory.common_parser (id, parser_id, name, source_id, description, version, service_name, request_format, response_format, created_time, updated_time, sort, type) VALUES (15, 'kinsfolk', '亲属关系', 1, '亲属关系解析器', '', 'tde', '{"id": "","hasContext": true,"arguments":{},"query":""}', 'informs@^@value@^@displayText', '2019-11-18 13:49:29', '2019-11-18 18:24:30', null, null);


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE ner_factory.common_parser;