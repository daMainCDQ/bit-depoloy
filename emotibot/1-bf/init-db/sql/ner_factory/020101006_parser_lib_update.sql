-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

INSERT INTO ner_factory.common_slot_info (p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path)
VALUES ('chrono@1', 'hour_point', '小时（时间点）', 'string', 'string', '$.informs[0].value.chrono.time.items[0].hour');

INSERT INTO ner_factory.common_slot_info (id, p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path)
VALUES (null, 'chrono@1', 'time_point', '时间（时间点）', 'string', 'string', '$.informs[0].value.chrono.time.items[0].ISO_TIME');

INSERT INTO ner_factory.common_slot_info (id, p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path)
VALUES (null, 'chrono@1', 'date_point', '日期（时间点）', 'string', 'string', '$.informs[0].value.chrono.time.items[0].ISO_DATE.single');


INSERT INTO ner_factory.common_slot_info (p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path)
VALUES ('chrono@1', 'from_hour', '开始小时', 'string', 'string', '$.informs[0].value.chrono.from_time.items[0].hour');

INSERT INTO ner_factory.common_slot_info (id, p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path)
VALUES (null, 'chrono@1', 'from_date', '开始日期', 'string', 'string', '$.informs[0].value.chrono.from_time.items[0].ISO_DATE.single');

INSERT INTO ner_factory.common_slot_info (p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path)
VALUES ('chrono@1', 'to_date', '截止日期', 'string', 'string', '$.informs[0].value.chrono.to_time.items[0].ISO_DATE.single');

INSERT INTO ner_factory.common_slot_info (p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path)
VALUES ('chrono@1', 'to_hour', '截止小时', 'string', 'string', '$.informs[0].value.chrono.to_time.items[0].hour');

INSERT INTO ner_factory.common_slot_info (p_logic_id, slot_name, slot_desc, json_data_type, data_format, slot_value_path)
VALUES ('chrono@1', 'hours_last', '小时(持续时间)', 'string', 'string', '$.informs[0].value.chrono.duration.items[0].hours');

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
