-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

UPDATE ner_factory.common_parser t SET logic_id = 'bank-card@1', parser_id = 'bank-card' WHERE logic_id = 'bank_card@1';
UPDATE ner_factory.common_parser t SET logic_id = 'id_num@1', parser_id = 'id_num' WHERE logic_id = 'id_number@1';

UPDATE ner_factory.common_slot_info t SET p_logic_id = 'id_num@1' WHERE p_logic_id = 'id_number@1';

UPDATE ner_factory.common_slot_info t SET p_logic_id = 'bank-card@1' WHERE p_logic_id = 'bank_card@1';

alter table ner_factory.common_parser modify request_format varchar(1024) not null comment '解析器请求模板';

alter table ner_factory.common_slot_info modify slot_value_path varchar(255) not null comment '从解析器Response中获取槽位值的get路径';

alter table ner_factory.dac_classifier_tag modify tag_name varchar(64) not null comment '标签名称';

alter table ner_factory.dac_classifier_tag modify tag_desc varchar(255) default '暂无' null comment '标签描述说明';

alter table ner_factory.third_party_dynamic_req_key modify key_name varchar(64) not null comment '请求体中动态字段名称';

alter table ner_factory.third_party_dynamic_req_key modify reqest_path varchar(255) not null comment '动态字段值填写位置';

alter table ner_factory.third_party_request_info modify http_url varchar(255) not null comment '请求链接';

alter table ner_factory.third_party_slot_resp_map modify response_path varchar(255) not null comment '槽位值在response中所在路径';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
