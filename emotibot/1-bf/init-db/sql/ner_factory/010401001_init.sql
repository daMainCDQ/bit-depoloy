-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

drop table if exists ner_factory.base_parser;
create table base_parser
(
  id             bigint auto_increment
    primary key,
  base_parser_id varchar(32)                         not null comment '基础解析器的id',
  parser_id      varchar(32)                         null comment '所关联解析器的id',
  type           varchar(64)                         null comment '基础解析器抽取槽位替换的类型',
  created_time   timestamp default CURRENT_TIMESTAMP not null,
  updated_time   timestamp                           null on update CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='解析器槽位关联baseParser表';
create index base_parser_id_index on base_parser (id);

drop table if exists ner_factory.corpus;
create table corpus
(
  id           bigint auto_increment
    primary key,
  content      varchar(255)                            null comment '语料内容',
  parser_id    varchar(32)                             null,
  flag         int(4)    default 0                     not null comment '语料的状态 0:未训练，1:已训练， 2:训练异常',
  created_time timestamp default CURRENT_TIMESTAMP     not null,
  updated_time timestamp                               null on update CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='解析器语料表';
create index corpus_parser_id_index on corpus (parser_id);

drop table if exists ner_factory.corpus_history;
create table corpus_history
(
  id           bigint auto_increment
    primary key,
  version_id   bigint                                  not null comment '关联的语料版本id',
  content      varchar(255)                            null comment '历史语料的内容',
  parser_id    varchar(32)                             null  comment '关联解析器的id',
  created_time timestamp default CURRENT_TIMESTAMP     not null,
  updated_time timestamp                               null on update CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='解析器语料历史记录表';
create index corpus_history_version_id_index on corpus_history (version_id);

drop table if exists ner_factory.corpus_version;
create table corpus_version
(
  id           bigint auto_increment
    primary key  comment '语料历史版本的id',
  parser_id    varchar(32)                            null  comment '关联解析器的id',
  version      varchar(64)                            null comment '版本名称',
  created_time timestamp default CURRENT_TIMESTAMP    not null,
  updated_time timestamp                              null on update CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='解析器语料版本管理表';
create index corpus_version_parser_id_index on corpus_version (parser_id);

drop table if exists ner_factory.dictionary;
create table dictionary
(
  id            bigint auto_increment
    primary key,
  dictionary_id bigint                              null comment '关联bfop词库的词典id',
  parser_id     varchar(32)                         null comment '关联解析器的id',
  created_time  timestamp default CURRENT_TIMESTAMP not null,
  updated_time  timestamp                           null on update CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='解析器槽位关联字典表';

drop table if exists ner_factory.parser;
create table parser
(
  id           bigint auto_increment
    primary key,
  parser_id    varchar(32)                          not null comment '解析器的id',
  user_id      varchar(32)                          null comment '关联用户的id',
  name         varchar(255)                         null comment '解析器的名称',
  description  text                                 null comment '解析器的描述',
  model_id     varchar(64)                          null comment '解析器训练所得模型id',
  flag         int(4)     default 0                 null comment '0 表示未发布，1表示已经发布',
  enable       tinyint(1) default 1                 not null  comment '0表示不可用，1表示可用',
  created_time timestamp  default CURRENT_TIMESTAMP not null,
  updated_time timestamp                            null on update CURRENT_TIMESTAMP,
  used_num     int        default 0                 null comment '被使用的次数'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='解析器基本信息表';
create index parser_user_id_index on parser (user_id);

drop table if exists ner_factory.rule;
create table rule
(
  id           bigint auto_increment
    primary key,
  rule_id      bigint                              null comment '关联用户规则库下的规则id',
  parser_id    varchar(32)                         null comment '关联解析器的id',
  created_time timestamp default CURRENT_TIMESTAMP not null,
  updated_time timestamp                           null on update CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='解析器槽位关联抽取规则表';

drop table if exists ner_factory.rule_library;
create table rule_library
(
  id           bigint auto_increment
    primary key,
  user_id      varchar(32)                             null comment '关联用户的id',
  content      varchar(255)                             null comment '规则内容',
  created_time timestamp default CURRENT_TIMESTAMP     not null,
  updated_time timestamp                               null on update CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户下规则库';
create index rule_library_user_id_index on parser (user_id);


drop table if exists ner_factory.slot;
create table slot
(
  id           bigint auto_increment
    primary key,
  parser_id    varchar(32)                            null comment '关联解析器的id',
  name         varchar(64)                             null comment '槽位名称',
  tag          varchar(64)                            null comment '槽位替换测标签名',
  replacement  varchar(64)                            null comment '槽位替换的占位符',
  created_time timestamp default CURRENT_TIMESTAMP    not null,
  updated_time timestamp                              null on update CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='解析器关联槽位基本信息';
create index slot_parser_id_index on slot (parser_id);


drop table if exists ner_factory.slot_extractor;
create table slot_extractor
(
  id           bigint auto_increment
    primary key,
  slot_id      bigint                              null comment '槽位id',
  type         int(4)                              null comment '槽位对应的类型，0 baseParser，1 dict,2 rule',
  extractor_id bigint                              null  comment '关联各类型抽取器的id',
  created_time timestamp default CURRENT_TIMESTAMP not null,
  updated_time timestamp                           null on update CURRENT_TIMESTAMP,
  parser_id    varchar(64)                         not null comment '关联解析器的id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='解析器槽位和其对应抽取器 关系表';
create index slot_extractor_slot_id_index on slot_extractor (slot_id);

drop table if exists ner_factory.test_data;
create table test_data
(
  id             bigint auto_increment
    primary key,
  user_id        varchar(32)                                                       not null comment '关联用户的id',
  parser_id      varchar(32)                                                       not null comment '关联解析器的id',
  content        varchar(255)                                                      not null comment '测试题内容',
  expect_answer  varchar(255)                                                      not null comment '预期答案',
  actual_answer  varchar(255)                             default ''               not null comment '实际答案',
  compare_result int(2)                                  default 0                 not null comment '0 表示未测试，1 表示符合预期，-1 表示不符合预期',
  created_time   timestamp                               default CURRENT_TIMESTAMP not null,
  updated_time   timestamp                                                         null on update CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='解析器对应测试题';
create index test_data_parser_id_index on test_data (parser_id);


drop table if exists ner_factory.test_data_schedule;
create table test_data_schedule
(
  id           bigint auto_increment
    primary key,
  user_id      varchar(32)                         not null comment '关联用户的id',
  parser_id    varchar(32)                         not null comment '关联解析器的id',
  finish_flag  int(2)    default 0                 not null comment '完成标记 0表示未完成，1表示完成',
  created_time timestamp default CURRENT_TIMESTAMP not null,
  updated_time timestamp                           null on update CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='解析器对应测试任务状态';
create index test_data_schedule_parser_id_index on test_data_schedule (parser_id);

drop table if exists ner_factory.dictionary_relation_map;
create table dictionary_relation_map
(
  id           bigint auto_increment
    primary key,
  user_id      varchar(32)                             not null comment '用户的id',
  dictionary_id bigint   not null comment 'bfop词典的id',
  dictionary_en_name         varchar(32)                             null comment 'bfop词库中词典名对应ner的英文名',
  created_time timestamp  default CURRENT_TIMESTAMP    not null,
  updated_time timestamp                               null on update CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='bfop词库和ner词库英文名映射关系表';
create index dictionary_relation_map_index on dictionary_relation_map (user_id, dictionary_en_name);

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

DROP TABLE `base_parser`;
DROP TABLE `corpus`;
DROP TABLE `corpus_history`;
DROP TABLE `corpus_version`;
DROP TABLE `dictionary`;
DROP TABLE `parser`;
DROP TABLE `rule`;
DROP TABLE `rule_library`;
DROP TABLE `slot`;
DROP TABLE `slot_extractor`;
DROP TABLE `test_data`;
DROP TABLE `test_data_schedule`;
DROP TABLE `dictionary_relation_map`;
