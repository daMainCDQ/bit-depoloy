import os

import pymysql


def migrate_user_id():
    mysqlHost = os.environ.get('INIT_MYSQL_HOST','172.16.100.130')
    mysqlPort = int(os.environ.get('INIT_MYSQL_PORT','3306'))
    mysqlUser = os.environ.get('INIT_MYSQL_USER','root')
    mysqlPassword = os.environ.get('INIT_MYSQL_PASSWORD','password')

    mysqlConnect = pymysql.connect(host=mysqlHost, port=mysqlPort, user=mysqlUser,
                                   password=mysqlPassword, database='ner_factory', charset='utf8mb4')
    try:
        all_classifier_info = "select classifier_id, user_id from dac_classifier_info"
        cur = mysqlConnect.cursor()
        cur.execute(all_classifier_info)
        parser_rows = cur.fetchall()
        for row in parser_rows:
            cur.execute("update dac_classifier_corpus set user_id='{}' where classifier_id='{}';".format(row[1],row[0]))
            cur.execute(
                "update dac_classifier_tag set user_id='{}' where classifier_id='{}';".format(row[1], row[0]))
            cur.execute(
                "update dac_tag_map_system_act set user_id='{}' where classifier_id='{}';".format(row[1], row[0]))

        mysqlConnect.commit()
    except Exception as e:
        print("020105002_classifier_migration.py execute fail")
        print(str(e))
        mysqlConnect.rollback()
    finally:
        mysqlConnect.close()


if __name__ == "__main__":
    migrate_user_id()
