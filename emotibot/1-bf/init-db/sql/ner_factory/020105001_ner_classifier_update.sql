-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE dac_classifier_corpus ADD user_id varchar(40) NULL;
ALTER TABLE dac_classifier_tag ADD user_id varchar(40) NULL;
ALTER TABLE dac_tag_map_system_act ADD user_id varchar(40) NULL;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE dac_classifier_corpus DROP user_id;
ALTER TABLE dac_classifier_tag DROP user_id;
ALTER TABLE dac_tag_map_system_act DROP user_id;