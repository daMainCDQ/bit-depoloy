import os

import pymysql


def addColumns():
    mysqlHost = os.environ.get('INIT_MYSQL_HOST')
    mysqlPort = int(os.environ.get('INIT_MYSQL_PORT'))
    mysqlUser = os.environ.get('INIT_MYSQL_USER')
    mysqlPassword = os.environ.get('INIT_MYSQL_PASSWORD')

    mysqlConnect = pymysql.connect(host=mysqlHost, port=mysqlPort, user=mysqlUser,
                                   password=mysqlPassword, database='ner_factory', charset='utf8mb4')
    # mysqlConnect = pymysql.connect(host='172.16.100.136', port=3306, user='root',
    #                                password='password', database='ner_factory', charset='utf8mb4')
    try:
        isTypeExistInParser = "select count(*) from information_schema.columns where table_schema = 'ner_factory' and table_name = 'parser' and column_name = 'type';"

        cur = mysqlConnect.cursor()
        cur.execute(isTypeExistInParser)
        parserRows = cur.fetchall()

        if parserRows is not None:
            count = parserRows[0][0]
            if count > 0:
                udpateParserSql = "update ner_factory.parser set type = 0;"
                cur.execute(udpateParserSql)
            else:
                udpateParserSql = "alter table ner_factory.parser add column type int(11) default 0 null;"
                cur.execute(udpateParserSql)

        isTypeExistInSlot = "select count(*) from information_schema.columns where table_schema = 'ner_factory' and table_name = 'slot' and column_name = 'type';"
        cur.execute(isTypeExistInSlot)
        slotRows = cur.fetchall()
        if slotRows is not None:
            count = slotRows[0][0]
            if count > 0:
                updateSlotSql = "update ner_factory.slot set type = 0;"
                cur.execute(updateSlotSql)
            else:
                updateSlotSql = "alter table ner_factory.slot add column type int(11) default 0 null ;"
                cur.execute(updateSlotSql)

        mysqlConnect.commit()

        print('010501002_parser_add_column.py execute success')
    except Exception as e:
        print("010501002_parser_add_column.py execute fail")
        print(str(e))
        mysqlConnect.rollback()
    finally:
        mysqlConnect.close()

if __name__ == "__main__":
    addColumns()