-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE ner_factory.slot ADD description varchar(200) DEFAULT '暂无' NULL;

ALTER TABLE ner_factory.slot
  MODIFY COLUMN description varchar(200) DEFAULT '暂无' AFTER name;

drop table if exists ner_factory.dictionary_v2;
create table ner_factory.dictionary_v2
(
  id            bigint auto_increment
    primary key,
  dictionary_id bigint                              null
  comment '关联bfop词库的词典id',
  parser_id     varchar(32)                         null
  comment '关联解析器的id',
  created_time  timestamp default CURRENT_TIMESTAMP not null,
  updated_time  timestamp                           null
  on update CURRENT_TIMESTAMP
)
  comment '词典解析器关联词库字典表';

create index dict_v2_parser_id_dict_id_index
  on ner_factory.dictionary_v2 (parser_id, dictionary_id);


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

drop table ner_factory.dictionary_v2;
ALTER TABLE ner_factory.slot drop column description;