-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

alter table ner_factory.common_parser modify parser_name varchar(128) null comment '解析器的名称';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
