
-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE DATABASE IF NOT EXISTS `faq3` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
drop database `faq3` IF EXISTS `faq3`;
