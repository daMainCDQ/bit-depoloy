-- +migrate Up
CREATE DATABASE IF NOT EXISTS `chat` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

-- +migrate Down
DROP DATABASE `chat`;
