-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE DATABASE IF NOT EXISTS `QISYS_dialogue_act` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP DATABASE `QISYS_dialogue_act`;

