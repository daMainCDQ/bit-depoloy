-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `model` ADD COLUMN `builtin` tinyint(1) default 0 NOT NULL COMMENT '是否内置'  AFTER `type`;
ALTER TABLE `model_lib` ADD COLUMN `train_config_uid` varchar(32)   COMMENT '训练uid'  DEFAULT NULL AFTER `predict_config_uid`;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

ALTER TABLE `model` DROP COLUMN `builtin`;
ALTER TABLE `model_lib` DROP COLUMN `train_config_uid`;
