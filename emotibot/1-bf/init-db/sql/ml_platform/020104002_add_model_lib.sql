-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

DROP TABLE IF EXISTS `model_lib`;
CREATE TABLE `model_lib` (
                           `id` int(11) NOT NULL AUTO_INCREMENT,
                           `uid` varchar(32) NOT NULL COMMENT 'uuid',
                           `app_id` varchar(64) NOT NULL COMMENT '机器人id',
                           `name` varchar(256) NOT NULL COMMENT '模型名称',
                           `type` varchar(32) NOT NULL COMMENT '模型类型，单体，集成',
                           `problem_uid` varchar(64) DEFAULT NULL COMMENT '问题id，没有则为Null',
                           `model_id` varchar(64) NOT NULL COMMENT '算法服务内模型id',
                           `algorithm_uid` varchar(32) NOT NULL COMMENT '算法uid，会存单体或集成算法uid',
                           `predict_config_uid` varchar(32) DEFAULT NULL COMMENT '预测配置',
                           `builtin` tinyint(1) NOT NULL COMMENT '是否算法内建',
                           `size` int(11) DEFAULT NULL COMMENT '模型大小',
                           `parent_uid` varchar(32) DEFAULT NULL COMMENT '父模型id',
                           `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                           `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                           `system_builtin` tinyint(1) NOT NULL COMMENT '是否系统内置',
                           PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;


CREATE UNIQUE INDEX model_lib_uid_uindex ON model_lib (uid);
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

drop table model_lib;