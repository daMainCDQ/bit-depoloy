-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

alter table `model` modify column uid varchar(64) not null comment '模型id';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
