import os
import uuid

import pymysql

mysql_host = os.environ.get('INIT_MYSQL_HOST', '172.16.102.121')
mysql_port = int(os.environ.get('INIT_MYSQL_PORT', '3306'))
mysql_user = os.environ.get('INIT_MYSQL_USER', 'root')
mysql_password = os.environ.get('INIT_MYSQL_PASSWORD', 'password')
mysql_db_ml_platform = 'ml_platform'

init_intent_list = [
    {"modelId": "chat", "name": "日常闲聊"},
    {"modelId": "entertainment", "name": "娱乐领域"},
    {"modelId": "tv", "name": "电视助理"},
    {"modelId": "smartspeaker", "name": "智能音箱"},
    {"modelId": "life", "name": "生活助手"},
    {"modelId": "travel", "name": "旅游出行"},
    {"modelId": "hr", "name": "HR领域"},
    {"modelId": "mobile", "name": "手机助手"},
    {"modelId": "aiot", "name": "智能家居"},
    {"modelId": "bank", "name": "银行业务"},
    {"modelId": "finance", "name": "财经资讯"},
    {"modelId": "insurance", "name": "保险领域"},
    {"modelId": "news", "name": "新闻领域"},
    {"modelId": "sports", "name": "体育领域"},
    {"modelId": "encyclopedia", "name": "生活百科"},
    {"modelId": "logistics", "name": "物流领域"},
    {"modelId": "generalintent", "name": "通用"}
]


def migrate():
    db = pymysql.connect(host=mysql_host, port=mysql_port, user=mysql_user, passwd=mysql_password,
                         db=mysql_db_ml_platform)

    db.set_charset('utf8')

    insert_cursor = db.cursor()

    for intent in init_intent_list:
        model_id = intent.get("modelId")
        name = intent.get("name")

        # 新增问题表
        problem_uid = str(uuid.uuid4()).replace('-', '')
        add_problem_sql = "insert into problem(uid, app_id, name, alg_type, data_config, train_datasets, test_datasets) " \
                          "values('{}', '{}','{}', '{}' ,'{}', '{}' ,'{}')".format(
            problem_uid, "model_lib", name, "text_classification", '{"sources":["sentence"],"target":"label"}', "null",
            "null")

        # 新增训练表
        train_uid = str(uuid.uuid4()).replace('-', '')
        add_train_sql = "insert into train_config(uid, app_id, algorithm_uid, template, parameters, data_config, other_config) " \
                        "values('{}', '{}', '{}', '{}', '{}', '{}', '{}')".format(
            train_uid, "model_lib", "intent-in", 0, '{}', "null", "null")

        model_uid = str(uuid.uuid4()).replace('-', '')
        strategy = '[{"action": "single", "model_id":' + model_uid + ', "threshold":0}]'

        # 新增预测表
        predict_uid = str(uuid.uuid4()).replace('-', '')
        add_predict_sql = "insert into predict_config(uid, app_id, config, data_config, strategy) " \
                          "values('{}', '{}', '{}', '{}' ,'{}')".format(
            predict_uid, "model_lib", "{}",
            '{"sources":["sentence"],"dataset_uid":"bd0a5caca02e41a1ad2bb5a4acddb394","target":"label"}', strategy)

        # 新增模型库表
        add_model_sql = "insert into model(uid, app_id, model_id, name, train_config_uid, status, problem_uid, loaded, type, predict_config_uid, algorithm, builtin) " \
                        "values('{}', '{}', '{}', '{}' ,'{}', '{}', '{}', '{}' ,'{}', '{}', '{}', '{}')".format(
            model_uid, "model_lib", model_id, name, train_uid, "active", problem_uid, 1, "single", predict_uid,
            "intent-in", 1)

        # 新增模型库表
        model_lib_uid = str(uuid.uuid4()).replace('-', '')
        add_model_lib_sql = "insert into model_lib(uid, app_id, name, type, problem_uid, model_id, algorithm_uid, predict_config_uid, train_config_uid, builtin, system_builtin) " \
                            "values('{}', '{}','{}', '{}' ,'{}', '{}' ,'{}', '{}' ,'{}', '{}', '{}')".format(
            model_lib_uid, "system", name, "single", problem_uid, model_id, "intent-in", predict_uid, train_uid, 1, 1)

        insert_cursor.execute(add_problem_sql)
        insert_cursor.execute(add_train_sql)
        insert_cursor.execute(add_predict_sql)
        insert_cursor.execute(add_model_sql)
        insert_cursor.execute(add_model_lib_sql)
    db.commit()
    db.close()


if __name__ == '__main__':
    migrate()
