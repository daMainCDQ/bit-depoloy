# import os
# import requests
# import json
# import pymysql
#
# mysql_host = os.environ.get('INIT_MYSQL_HOST', '172.16.102.121')
# mysql_port = int(os.environ.get('INIT_MYSQL_PORT', '3306'))
# mysql_user = os.environ.get('INIT_MYSQL_USER', 'root')
# mysql_password = os.environ.get('INIT_MYSQL_PASSWORD', 'password')
# mysql_db_ml_platform = 'ml_platform'
# clone_url = "http://172.17.0.1:8848/mlp/v1/api/problem/clone"
# faq_offline = "_faq_offline"
# faq_online = "_faq_online"
# ssm_dac_url = "http://172.17.0.1:8686/ssm/dac/openapi/model/update"
#
# def migrate():
#
#     db = pymysql.connect(host=mysql_host, port=mysql_port, user=mysql_user, passwd=mysql_password,
#                              db=mysql_db_ml_platform)
#
#     # 查询所有的faq相关问题
#     problem_query_sql = 'select id, uid, app_id from problem where uid like "%faq"'
#     insert_cursor = db.cursor()
#     insert_cursor.execute(problem_query_sql)
#     app_ids = insert_cursor.fetchall()
#
#     for id, uid, app_id in app_ids:
#
#         # 新问题uid
#         newUid = app_id + faq_offline
#
#         post_headers = {'Content-Type': 'application/json'}
#
#         # 手动调用问题clone接口
#         params = {"source_problem_id": uid, "target_problem_id": app_id + faq_online, "app_id": app_id}
#         clone_respose = requests.post(url=clone_url,headers=post_headers, data=json.dumps(params)).json()
#         if clone_respose['code'] is not 0:
#             print("faq_clone migration clone fail!")
#             return
#
#         # 更新问题id
#         problem_update_sql = 'update problem set uid = "{}" where id = "{}"'.format(newUid, id)
#         insert_cursor.execute(problem_update_sql)
#
#         problem_update_sql = 'update model set problem_uid = "{}" where problem_uid = "{}"'.format(newUid, uid)
#         insert_cursor.execute(problem_update_sql)
#
#         # 手动调用ssm-dac接口
#         params_ssm_dac = {"appId": app_id}
#         ssm_dac_response = requests.post(ssm_dac_url, headers=post_headers, data=json.dumps(params_ssm_dac)).json()
#         if ssm_dac_response['code'] is not 0:
#             print("faq_clone migration ssm_dac fail!")
#             return
#         db.commit()
#         db.close()
#
# if __name__ == '__main__':
#     migrate()
