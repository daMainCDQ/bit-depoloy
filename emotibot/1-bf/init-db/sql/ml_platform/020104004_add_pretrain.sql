-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for model_lib_category
-- ----------------------------
DROP TABLE IF EXISTS `model_lib_category`;
CREATE TABLE `model_lib_category` (
                                      `id` int(11) NOT NULL AUTO_INCREMENT,
                                      `uid` varchar(32) NOT NULL,
                                      `app_id` varchar(64) NOT NULL,
                                      `name` varchar(100) NOT NULL,
                                      `builtin` tinyint(1) NOT NULL,
                                      `parent_uid` varchar(32) DEFAULT NULL,
                                      `level` int(11) NOT NULL,
                                      `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                      `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                                      PRIMARY KEY (`id`),
                                      UNIQUE KEY `model_lib_category_uuid_uindex` (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for pretrain_model
-- ----------------------------
DROP TABLE IF EXISTS `pretrain_model`;
CREATE TABLE `pretrain_model` (
                                  `id` int(11) NOT NULL AUTO_INCREMENT,
                                  `uid` varchar(32) DEFAULT NULL,
                                  `app_id` varchar(64) DEFAULT NULL,
                                  `name` varchar(100) DEFAULT NULL,
                                  `builtin` tinyint(1) NOT NULL COMMENT '是否内建',
                                  `algorithm_uid` varchar(32) DEFAULT NULL,
                                  `category_uid` varchar(32) DEFAULT NULL,
                                  `path` varchar(1024) DEFAULT NULL,
                                  `progress` varchar(5) NOT NULL COMMENT '上传进度',
                                  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                                  PRIMARY KEY (`id`) USING BTREE,
                                  UNIQUE KEY `pretrain_model_id_uindex` (`id`),
                                  UNIQUE KEY `pretrain_model_uuid_uindex` (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

drop table pretrain_model;
drop table model_lib_category;