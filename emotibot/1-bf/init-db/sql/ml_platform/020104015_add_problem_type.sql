-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE problem ADD type varchar(32) COMMENT '问题类型' AFTER name;
alter table `model` modify column parent_uid varchar(64) comment '父模型id';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `problem` DROP COLUMN `type`;
