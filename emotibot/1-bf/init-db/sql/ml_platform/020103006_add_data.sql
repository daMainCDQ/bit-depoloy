-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

INSERT INTO algorithm(uid, app_id, name, url, parameters, algorithm, type, is_train_able, builtin, extend) VALUES('semantic_search', 'system', '相似度算法v2', 'http://semantic-search-cpu:19931', '{}', 'ss_service', 'text_classification', 1, 1, '{}');
INSERT INTO algorithm(uid, app_id, name, url, parameters, algorithm, type, is_train_able, builtin, extend) VALUES('ft_similar', 'system', '相似度算法v3', 'http://ft-similar:1123', '{}', 'FTSimilar_service', 'text_classification', 1, 1, '{}');

INSERT INTO ensemble(uid, app_id, name, type, parameters, builtin) VALUES('faq_new', 'system', 'faq_new', 'order', '{}', 1);

INSERT INTO logic(uid, threshold, ensemble_uid, algorithm_uid) VALUES('faq_new_ft', 97, 'faq_new', 'ft_service');
INSERT INTO logic(uid, threshold, ensemble_uid, algorithm_uid) VALUES('faq_new_similar', 0, 'faq_new', 'ft_similar');

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
