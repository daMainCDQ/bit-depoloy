-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

drop table if exists `load_record`;
create table `load_record`
(
  id        int auto_increment
    primary key,
  algorithm varchar(32)                         not null comment '算法名称',
  load_time timestamp default CURRENT_TIMESTAMP not null comment '加载变化时间'
) engine=InnoDB default charset=utf8mb4 collate utf8mb4_general_ci comment='加载记录';

ALTER TABLE `model` ADD COLUMN `algorithm` varchar(32) COMMENT '模型类型' AFTER `predict_config_uid`;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
drop table load_record;
ALTER TABLE `model` DROP COLUMN `algorithm`;
