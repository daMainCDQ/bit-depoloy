-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied


ALTER TABLE `problem` ADD COLUMN `data_config` text not null COMMENT '数据配置' AFTER `test_datasets`;
update problem set data_config = '{"sources":["sentence"],"target":"label"}';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `problem` DROP COLUMN `data_config`;
