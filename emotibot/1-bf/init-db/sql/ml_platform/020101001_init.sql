-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

drop table if exists `algorithm`;
create table algorithm
(
  id          int auto_increment
    primary key,
  uid         varchar(32)                         not null
  comment '算法uuid',
  app_id      varchar(64)                         not null
  comment '机器人id',
  name        varchar(256)                        not null
  comment '算法名称',
  url         varchar(1024)                       not null
  comment '算法地址',
  parameters  text                                not null
  comment '超参定义',
  algorithm   varchar(1024)                       null
  comment '子算法名称',
  type        varchar(32)                         not null
  comment '算法类型',
  builtin     tinyint(1)                          not null
  comment '是否内建',
  extend      text                                null
  comment '扩展字段',
  create_time timestamp default CURRENT_TIMESTAMP not null,
  update_time timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP,
  constraint algorithm_uid_uindex
  unique (uid)
) engine=InnoDB default charset=utf8mb4 collate utf8mb4_general_ci comment='算法';

create index uid_index
  on algorithm (uid);

drop table if exists `dataset`;
create table dataset
(
  id          int auto_increment
    primary key,
  uid         varchar(32)                         not null
  comment '数据集uuid',
  app_id      varchar(64)                         not null
  comment '机器人id',
  name        varchar(100)                        null
  comment '数据集名称',
  storage     varchar(32)                         not null
  comment '存储介质',
  location    varchar(1024)                       null
  comment '存储位置',
  type        varchar(32)                         not null
  comment '数据集类型',
  description text                                null
  comment '数据集描述',
  problem_uid varchar(32)                         not null
  comment '问题id',
  alg_type    varchar(32)                         not null
  comment '算法类型',
  header      text                                null
  comment '数据集字段',
  label       text                                null
  comment '预测字段',
  create_time timestamp default CURRENT_TIMESTAMP not null,
  update_time timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP,
  constraint dataset_uid_uindex
  unique (uid)
) engine=InnoDB default charset=utf8mb4 collate utf8mb4_general_ci comment='数据集';

create index uid_index
  on dataset (uid);

drop table if exists `mem_req_record`;
create table mem_req_record
(
  id             bigint unsigned auto_increment
  comment 'DB自增ID'
    primary key,
  enterprise_id  varchar(64)         null
  comment '企业ID',
  app_id         varchar(64)         null
  comment '机器人ID',
  model_group_id varchar(64)         null
  comment '模型组ID',
  model_id       varchar(64)         not null
  comment '模型ID',
  period_type    tinyint(8) unsigned null
  comment '统计周期类型',
  start_time     timestamp           null
  comment '开始时间',
  end_time       timestamp           null
  comment '结束时间',
  predict_cnt    int(32) unsigned    null
  comment '预测请求数量',
  status         double unsigned  null
  comment '模型状态'
) engine=InnoDB default charset=utf8mb4 collate utf8mb4_general_ci comment='请求数据';

create index period_type_index
  on mem_req_record (period_type);

create index start_time_index
  on mem_req_record (start_time);

drop table if exists `mem_stats`;
create table mem_stats
(
  id                     bigint unsigned auto_increment
  comment 'DB自增ID'
    primary key,
  period_type            tinyint(8) null
  comment '统计周期，1：过去1小时，时间粒度为1分钟；2：过去一天，时间粒度1小时，3：过去7天，时间粒度8小时；4：过去30天，时间粒度1天',
  start_time             timestamp  null
  comment '开始时间',
  end_time               timestamp  null
  comment '结束时间',
  fs_model_cnt           int(16)    null,
  loaded_model_cnt_max   int(16)    null
  comment '统计周期内，内存中的模型数量max值',
  loaded_model_cnt_min   int(16)    null
  comment '统计周期内，内存中的模型数量min值',
  loaded_model_size_max  int(16)    null
  comment '统计周期内，内存中的模型大小max值',
  loaded_model_size_min  int(16)    null
  comment '统计周期内，内存中的模型数量min值',
  unloaded_model_cnt_max int(16)    null
  comment '统计周期内，未加载模型数量max值',
  unloaded_model_cnt_min int(16)    null
  comment '统计周期内，未加载模型数量min值',
  total_mem_assigned     int(16)    null
  comment '分配可用的内存总大小'
) engine=InnoDB default charset=utf8mb4 collate utf8mb4_general_ci comment='内存状态';

drop table if exists `metric`;
create table metric
(
  id          int auto_increment
    primary key,
  uid         varchar(32)                         not null
  comment '测试id',
  app_id      varchar(64)                         not null
  comment '机器人id',
  type        varchar(32)                         not null
  comment '测试类型',
  target_uid  varchar(32)                         not null
  comment '测试对象id',
  dataset_uid varchar(32)                         not null
  comment '数据集id',
  status      varchar(32)                         not null
  comment '测试状态',
  result      text                                null
  comment '测试结果',
  create_time timestamp default CURRENT_TIMESTAMP not null,
  update_time timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP,
  constraint metric_uid_uindex
  unique (uid)
) engine=InnoDB default charset=utf8mb4 collate utf8mb4_general_ci comment='模型度量';

create index uid_index
  on metric (uid);

drop table if exists `model`;
create table model
(
  id               int auto_increment
    primary key,
  uid              varchar(32)                         not null
  comment '模型id',
  app_id           varchar(64)                         not null
  comment '机器人id',
  model_id         varchar(64)                         null
  comment '算法服务内模型id',
  name             varchar(100) charset utf8mb4        null,
  train_config_uid varchar(32)                         null
  comment '训练配置id',
  train_desc_uid   varchar(32)                         null
  comment '训练状态描述id',
  status           varchar(32)                         null
  comment '训练状态',
  problem_uid      varchar(32)                         not null
  comment '问题id',
  path             varchar(1024)                       null
  comment '存储路径',
  loaded           tinyint(1)                          null
  comment '是否加载',
  size             int                                 null
  comment '模型大小',
  create_time      timestamp default CURRENT_TIMESTAMP not null,
  update_time      timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP,
  constraint model_uid_uindex
  unique (uid)
) engine=InnoDB default charset=utf8mb4 collate utf8mb4_general_ci comment='模型';

create index uid_index
  on model (uid);

drop table if exists `problem`;
create table problem
(
  id             int auto_increment
    primary key,
  uid            varchar(32)                         not null
  comment '问题id',
  app_id         varchar(64)                         not null
  comment '机器人id',
  name           varchar(100) charset utf8mb4        null,
  alg_type       varchar(32)                         not null
  comment '算法类型',
  train_datasets text                                null
  comment '训练集',
  test_datasets  text                                null
  comment '测试集',
  create_time    timestamp default CURRENT_TIMESTAMP not null,
  update_time    timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP,
  constraint problem_uid_uindex
  unique (uid)
) engine=InnoDB default charset=utf8mb4 collate utf8mb4_general_ci comment='问题';

create index uid_index
  on problem (uid);

drop table if exists `solution`;
create table solution
(
  id          int auto_increment
    primary key,
  uid         varchar(32)                         not null
  comment '解决方案id',
  app_id      varchar(64)                         not null
  comment '机器人id',
  name        varchar(256)                        not null,
  models      varchar(1024)                       null
  comment '模型',
  strategy    text                                not null
  comment '策略定义',
  problem_uid varchar(32)                         null
  comment '问题id',
  active      tinyint(1)                          null
  comment '是否激活',
  create_time timestamp default CURRENT_TIMESTAMP not null,
  update_time timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP,
  constraint solution_uid_uindex
  unique (uid)
) engine=InnoDB default charset=utf8mb4 collate utf8mb4_general_ci comment='解决方案';

create index uid_index
  on solution (uid);

drop table if exists `train_config`;
create table train_config
(
  id            int auto_increment
    primary key,
  uid           varchar(32)                         not null
  comment '训练配置id',
  app_id        varchar(64)                         null
  comment '机器人id',
  algorithm_uid varchar(32)                         not null
  comment '算法id',
  parameters    text                                null
  comment '超参',
  data_config   text                                null
  comment '数据配置',
  other_config  text                                null
  comment '其他配置',
  template      tinyint(1)                          not null
  comment '是否是模板',
  create_time   timestamp default CURRENT_TIMESTAMP not null,
  update_time   timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP,
  constraint train_config_uid_uindex
  unique (uid)
) engine=InnoDB default charset=utf8mb4 collate utf8mb4_general_ci comment='训练配置';

create index uid_index
  on train_config (uid);

drop table if exists `train_desc`;
create table train_desc
(
  id          int auto_increment
    primary key,
  uid         varchar(32)                         not null
  comment '训练描述id',
  app_id      varchar(64)                         not null
  comment '机器人id',
  status      varchar(32)                         not null
  comment '状态',
  statistics  text                                null
  comment '训练统计数据',
  create_time timestamp default CURRENT_TIMESTAMP not null,
  update_time timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP,
  constraint train_desc_uid_uindex
  unique (uid)
) engine=InnoDB default charset=utf8mb4 collate utf8mb4_general_ci comment='训练描述';

create index uid_index
  on train_desc (uid);

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
drop table algorithm;
drop table dataset;
drop table mem_req_record;
drop table mem_stats;
drop table metric;
drop table model;
drop table problem;
drop table solution;
drop table train_config;
drop table train_desc;
