-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `metric` ADD COLUMN `problem_uid` varchar(32) COMMENT '问题id' AFTER `result`;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

ALTER TABLE `metric` DROP COLUMN `problem_uid`;
