-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

insert into algorithm(uid, app_id, name, url, parameters, algorithm, type, builtin, extend, is_train_able) values('intent-in', 'system', '内置意图算法', 'http://domain-intent:15004', '{}', 'intent-in', 'text_classification', 1, '{}', 1);

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
delete from algorithm where uid = 'intent-in';
