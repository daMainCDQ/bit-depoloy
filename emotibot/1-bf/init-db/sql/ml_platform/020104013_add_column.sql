-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied


ALTER TABLE `problem` ADD COLUMN `algorithm_uid` varchar(32)   COMMENT '默认算法uid，会存单体或集成算法uid'  DEFAULT NULL AFTER `data_config`;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `problem` DROP COLUMN `algorithm_uid`;

