import os
import sys
import time

import pymysql
from urllib import request


mysql_host = os.environ.get('INIT_MYSQL_HOST','172.16.102.121')
mysql_port = int(os.environ.get('INIT_MYSQL_PORT','3306'))
mysql_user = os.environ.get('INIT_MYSQL_USER','root')
mysql_password = os.environ.get('INIT_MYSQL_PASSWORD','password')

consul_host = os.environ.get('INIT_CONSUL_HOST','172.16.102.121')
consul_port = os.environ.get('INIT_CONSUL_PORT','8500')


def handler_all():
    db = pymysql.connect(host=mysql_host, port=mysql_port, user=mysql_user, passwd=mysql_password, db='faq3')
    cursor = db.cursor()
    try:
        cursor.execute('select app_id,name from faq3.ent_config_appid_customization where name in (%s,%s)',('ssm_config','ssm_config_rc'))
        exist_ssmconfig= cursor.fetchall()
        rc_appid = set()
        gold_appid = set()
        for sc in exist_ssmconfig:
            if sc[1] == "ssm_config":gold_appid.add(sc[0])
            if sc[1] == "ssm_config_rc":rc_appid.add(sc[0])

        print('rc app num:%s '% (len(rc_appid)))
        print('gold app num:%s ' % (len(gold_appid)))
        for appid in rc_appid:
            cursor.execute('insert ignore into faq3.ent_config_appid_customization(app_id,name,value) values (%s,%s,%s)',(appid,'ssm_mlp_index_rc','emotibot-qa-core-dac-rc'))
            cursor.execute('insert ignore into faq3.ent_config_appid_customization(app_id,name,value) values (%s,%s,%s)',(appid,'ssm_mlp_model_id_rc',appid + '_rc'))



        for appid in gold_appid:
            cursor.execute('insert ignore into faq3.ent_config_appid_customization(app_id,name,value) values (%s,%s,%s)',
                           (appid, 'ssm_mlp_index_gold', 'emotibot-qa-core-dac-gold'))
            cursor.execute('insert ignore into faq3.ent_config_appid_customization(app_id,name,value) values (%s,%s,%s)',
                           (appid, 'ssm_mlp_model_id_gold', appid + '_gold'))

        update_consul_config(rc_appid,['idc/ssm_mlp_index_rc','idc/ssm_mlp_model_id_rc'])
        update_consul_config(gold_appid,['idc/ssm_mlp_index_gold','idc/ssm_mlp_model_id_gold'])

        db.commit()
        db.close()
    except Exception as e:
        print(e)
    return



def update_consul_config(appset,rc_key_pre):
    if len(appset) <= 0:return
    for keyi in rc_key_pre:
        url = 'http://' + consul_host + ':' + consul_port + '/v1/kv/' + keyi
        for app in appset:
            print(url + '/' + app)
            req = request.Request(url + '/' + app, data=bytes(str(int(time.time())), encoding='utf-8'), method='PUT')
            response = request.urlopen(req)

    return



if __name__ == '__main__':
    try:
        start_time = time.time()
        handler_all()
        print('handler all cost time:%s' % (time.time() - start_time))
    except Exception as err:
        print(err)
        sys.exit()
