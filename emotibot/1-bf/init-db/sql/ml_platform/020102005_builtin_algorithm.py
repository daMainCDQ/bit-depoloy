import os

import pymysql

mysql_host = os.environ.get('INIT_MYSQL_HOST', '172.17.0.1')
mysql_port = int(os.environ.get('INIT_MYSQL_PORT', '3306'))
mysql_user = os.environ.get('INIT_MYSQL_USER', 'root')
mysql_password = os.environ.get('INIT_MYSQL_PASSWORD', 'password')
mysql_db_ml_platform = 'ml_platform'


def migrate():
    mlp_db = pymysql.connect(host=mysql_host, port=mysql_port, user=mysql_user, passwd=mysql_password,
                             db=mysql_db_ml_platform)

    sql1 = 'select uid, algorithm ' \
           'from algorithm '

    insert_cursor = mlp_db.cursor()
    insert_cursor.execute(sql1)
    algorithms = insert_cursor.fetchall()

    for uid, alg in algorithms:
        insert_cursor.execute(
            "update train_config set algorithm_uid = '{}' where algorithm_uid = '{}'".format(alg, uid))

    insert_cursor.execute("delete from algorithm")

    insert_cursor.execute("insert into algorithm(uid, app_id, name, url, parameters, algorithm, type, builtin, extend)"
                          " values('intent', 'system', '意图算法', 'http://intent-engine-ml-adapter', '{}', "
                          "'intent', 'text_classification', 1, '{}')")

    insert_cursor.execute("insert into algorithm(uid, app_id, name, url, parameters, algorithm, type, builtin, extend)"
                          " values('ft_service', 'system', 'ft算法', 'http://simple-ft-service:8895', '{}', "
                          "'ft_service', 'text_classification', 1, '{}')")

    insert_cursor.execute("insert into algorithm(uid, app_id, name, url, parameters, algorithm, type, builtin, extend)"
                          " values('qq', 'system', 'qq算法', 'http://faq-module-qq:11025/qq', '{}', "
                          "'qq', 'text_classification', 1, '{}')")

    mlp_db.commit()
    return


if __name__ == '__main__':
    migrate()
