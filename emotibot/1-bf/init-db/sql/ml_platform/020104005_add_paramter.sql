-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for parameter
-- ----------------------------
DROP TABLE IF EXISTS `parameter`;
CREATE TABLE `parameter` (
                           `id` int(11) NOT NULL AUTO_INCREMENT,
                           `uid` varchar(32) NOT NULL,
                           `app_id` varchar(32) NOT NULL,
                           `algorithm_uid` varchar(64) NOT NULL,
                           `name` varchar(64) NOT NULL,
                           `type` varchar(32) NOT NULL,
                           `default_value` text NOT NULL,
                           `enumeration` text,
                           `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                           `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                           PRIMARY KEY (`id`),
                           UNIQUE KEY `parameter_uid_uindex` (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
drop table parameter;
