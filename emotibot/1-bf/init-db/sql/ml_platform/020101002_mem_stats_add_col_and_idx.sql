-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `mem_stats` ADD COLUMN `create_time` timestamp NULL DEFAULT NULL COMMENT '记录创建时间' AFTER `total_mem_assigned`;
ALTER  TABLE  `mem_stats`  ADD  INDEX `period_type_idx` (  `period_type`  ) USING BTREE;
ALTER  TABLE  `mem_stats`  ADD  INDEX `start_time_idx` (  `start_time`  ) USING BTREE;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `mem_stats` DROP INDEX `period_type_idx`;
ALTER TABLE `mem_stats` DROP INDEX `start_time_idx`;
ALTER TABLE `mem_stats` DROP COLUMN `create_time`;
