import json
import math
import os
import uuid

from minio import Minio
import pymysql

mysql_host = os.environ.get('INIT_MYSQL_HOST', '127.0.0.1')
mysql_port = int(os.environ.get('INIT_MYSQL_PORT', '3306'))
mysql_user = os.environ.get('INIT_MYSQL_USER', 'root')
mysql_password = os.environ.get('INIT_MYSQL_PASSWORD', 'password')
mysql_db_ml_platform = 'ml_platform'
mysql_db_emotibot = 'emotibot'
mysql_db_auth = 'auth'

minio_host = os.environ.get('INIT_MINIO_HOST', '127.0.0.1')
minio_port = os.environ.get('INIT_MINIO_PORT', '9000')
access_key = os.environ.get('INIT_MINIO_ACCESS_KEY', '24WTCKX23M0MRI6BA72Y')
secret_key = os.environ.get('INIT_MINIO_SECRET_KEY', '6mxoQMf1Lha2q1H3fp3fjNHEZRJJ7LvbC5k3+CQX')
secure = os.environ.get('INIT_MINIO_SECURE', 'false')
secure = True if secure == 'True' or secure == 'true' else False


intent_url = os.environ.get('INIT_IE_ML_ADAPTER_URL', 'http://intent-engine-ml-adapter:15004')


def migrate():
    minio_client_instance = Minio('{}:{}'.format(minio_host, minio_port), access_key=access_key, secret_key=secret_key, secure=secure)
    mlp_db = pymysql.connect(host=mysql_host, port=mysql_port, user=mysql_user, passwd=mysql_password, db=mysql_db_ml_platform)
    emotibot_db = pymysql.connect(host=mysql_host, port=mysql_port, user=mysql_user, passwd=mysql_password, db=mysql_db_emotibot)
    auth_db = pymysql.connect(host=mysql_host, port=mysql_port, user=mysql_user, passwd=mysql_password, db=mysql_db_auth)

    sql1 = 'select uuid, name ' \
           'from apps ' \
           'where app_type = 0'

    sql2 = 'select group_id, name, app_id ' \
           'from intent_group ' \
           'where group_type = 2 and group_status = 1'

    sql3 = 'select model_id, create_time ' \
           'from ie_queue ' \
           'where app_id = \'{}\' order by create_time'

    insert_cursor = mlp_db.cursor()

    cursor_auth = auth_db.cursor()
    cursor_auth.execute(sql1)
    app_ids = cursor_auth.fetchall()

    mapping = {}
    for app_id, name in app_ids:
        algorithm_uid = str(uuid.uuid4()).replace('-', '')
        mapping[app_id] = algorithm_uid
        insert_cursor.execute("insert into algorithm(uid, app_id, name, url, parameters, algorithm, type, builtin, extend) "
                              "values('{}', '{}', '{}', '{}' ,'{}', '{}', '{}', '{}' ,'{}')".format(
            algorithm_uid, app_id, '意图算法', intent_url, '{}', 'intent', 'text_classification', 1, '{}'))

    cursor_emotibot = emotibot_db.cursor()
    cursor_emotibot.execute(sql2)
    groups = cursor_emotibot.fetchall()

    cursor_emotibot.execute(sql2)

    data_config = {
        "type": "minio",
        "location": "unknown",
        "dataset_uid": None,
        "sources": ['sentence'],
        "target": "label"
    }
    for group_id, name, app_id in groups:
        if app_id not in mapping:
            continue

        cursor_emotibot.execute(sql3.format(group_id))
        model_ids = list(cursor_emotibot.fetchall())
        if len(model_ids) == 0:
            continue

        model_id, create_time = model_ids[-1]
        models = list(minio_client_instance.list_objects('intent-engine', model_id))
        if len(models) == 0:
            continue
        size = math.ceil(models[0].size / 1024 / 1024)

        config_id = str(uuid.uuid4()).replace('-', '')
        insert_cursor.execute("insert into problem(uid, app_id, name, alg_type, train_datasets, test_datasets) "
                       "values('{}', '{}', '{}', 'text_classification', '[]', '[]')".format(group_id, app_id, name))
        insert_cursor.execute(
            "insert into train_config(uid, app_id, algorithm_uid, parameters, data_config, other_config, template)" +
            "values('{}', '{}', '{}', '{{}}', '{}', '{}', '0')".format(config_id, app_id, mapping[app_id],
                                                                           json.dumps(data_config),
                                                                           json.dumps({'group_id': group_id})))

        insert_cursor.execute(
            "insert into model(uid, app_id, model_id, name, train_config_uid, status, problem_uid, path, loaded, size)" +
            "values('{}', '{}', '{}', '{}', '{}', '{}', '{}', 'mock_path', 0, {})".format(group_id, app_id, group_id, name + '模型', config_id, 'active', group_id, size))

    mlp_db.commit()
    return


if __name__ == '__main__':
    migrate()
