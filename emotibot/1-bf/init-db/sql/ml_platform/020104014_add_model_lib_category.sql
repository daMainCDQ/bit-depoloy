-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE model_lib ADD category_uid varchar(32) default 'common' COMMENT '模型库类型id' AFTER parent_uid;
ALTER TABLE model_lib ADD progress varchar(32) COMMENT '上传进度' AFTER category_uid;
ALTER TABLE model_lib ADD path varchar(1024) COMMENT '上传路径' AFTER progress;

INSERT INTO model_lib_category(uid, app_id, name, builtin, level) values('finance', 'system', '金融', 1, 0);
INSERT INTO model_lib_category(uid, app_id, name, builtin, level) values('common', 'system', '通用', 1, 0);

INSERT INTO model_lib_category(uid, app_id, name, builtin, parent_uid, level) values('bank', 'system', '银行', 1, 'finance', 1);
INSERT INTO model_lib_category(uid, app_id, name, builtin, parent_uid, level) values('insurance', 'system', '保险', 1, 'finance', 1);
INSERT INTO model_lib_category(uid, app_id, name, builtin, parent_uid, level) values('security', 'system', '证券', 1, 'finance', 1);


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `model_lib` DROP COLUMN `category_uid`;
