import os
import pymysql
import json

mysql_host = os.environ.get('INIT_MYSQL_HOST', '172.16.102.135')
mysql_port = int(os.environ.get('INIT_MYSQL_PORT', '3306'))
mysql_user = os.environ.get('INIT_MYSQL_USER', 'root')
mysql_password = os.environ.get('INIT_MYSQL_PASSWORD', 'password')
mysql_db_ml_platform = 'ml_platform'
mysql_db_emotibot = 'emotibot'
faq_offline = "_faq_offline"
faq_online = "_faq_online"


def migrate():

    db = pymysql.connect(host=mysql_host, port=mysql_port, user=mysql_user, passwd=mysql_password,
                         db=mysql_db_ml_platform)

    sql1 = 'select uid, predict_config_uid from model where type = "ensemble";'
    sql2 = 'select uid, strategy from predict_config where uid = "{}";'
    sql3 = 'update model set parent_uid = "{}" where uid = "{}";'

    insert_cursor = db.cursor()
    insert_cursor.execute(sql1)
    models = insert_cursor.fetchall()

    for model_uid, predict_config_uid in models:
        insert_cursor.execute(sql2.format(predict_config_uid))
        predict_configs = insert_cursor.fetchall()
        _, strategy = predict_configs[0]
        strategy = json.loads(strategy)
        for each in strategy:
            sub_model_uid = each['model_id']
            insert_cursor.execute(sql3.format(model_uid, sub_model_uid))

    db.commit()
    db.close()


if __name__ == '__main__':
    migrate()
