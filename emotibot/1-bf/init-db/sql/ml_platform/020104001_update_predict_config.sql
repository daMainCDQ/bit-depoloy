-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

alter table predict_config drop column model_uids;
ALTER TABLE `predict_config` ADD COLUMN `data_config` text COMMENT '数据配置' AFTER `config`;
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

ALTER TABLE `predict_config` ADD COLUMN `model_uids` varchar(1024) COMMENT '子模型id' AFTER `type`;
alter table predict_config drop column data_config;