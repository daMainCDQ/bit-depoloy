-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

alter table `dataset` modify column problem_uid varchar(64) not null comment '问题id';
alter table `metric` modify column problem_uid varchar(64) not null comment '问题id';
alter table `model` modify column problem_uid varchar(64) not null comment '问题id';
alter table `problem` modify column uid varchar(64) not null comment '问题id';
alter table `solution` modify column problem_uid varchar(64) not null comment '问题id';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
