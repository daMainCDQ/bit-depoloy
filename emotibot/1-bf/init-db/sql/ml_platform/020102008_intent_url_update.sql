-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

UPDATE `algorithm` SET url = 'http://intent-engine-ml-adapter:15004' WHERE url = 'http://intent-engine-ml-adapter';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
