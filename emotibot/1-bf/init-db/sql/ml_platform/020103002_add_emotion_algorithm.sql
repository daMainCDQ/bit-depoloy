-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

insert into algorithm(uid, app_id, name, url, parameters, algorithm, type, builtin, extend, is_train_able) values('emotion', 'system', '情绪算法', 'http://emotion-engine:15010/ml-platform', '{}', 'emotion', 'text_classification', 1, '{}', 1);

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
delete from algorithm where uid = 'emotion';
