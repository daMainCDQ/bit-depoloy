-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `mem_req_record` ADD COLUMN `fail_predict` int(32) DEFAULT NULL COMMENT '失败的predict次数' AFTER `predict_cnt`;
ALTER TABLE `mem_req_record` ADD INDEX `model_id_idx` (`model_id`) USING BTREE;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `mem_req_record` DROP COLUMN `fail_predict`;
ALTER TABLE `mem_req_record` DROP INDEX `model_id_idx`;

