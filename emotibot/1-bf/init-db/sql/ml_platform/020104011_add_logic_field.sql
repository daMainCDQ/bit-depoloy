-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE logic ADD type varchar(32) DEFAULT 'order' NOT NULL COMMENT '集成逻辑类型' AFTER algorithm_uid;
ALTER TABLE logic ADD value text NULL COMMENT '集成逻辑值' AFTER type;

-- +migrate Down
-- SQL in section 'Down' is executed when this migration is applied

ALTER TABLE `logic` DROP COLUMN `type`;
ALTER TABLE `logic` DROP COLUMN `value`;
