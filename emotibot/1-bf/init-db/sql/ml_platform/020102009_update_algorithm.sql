-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `algorithm` ADD COLUMN `is_train_able` tinyint(1) default 1 NOT NULL COMMENT '是否可训练'  AFTER `type`;
ALTER TABLE `algorithm` ADD COLUMN `offline_url` varchar(1024)   COMMENT '下线url'  DEFAULT NULL AFTER `type`;
update algorithm set offline_url = "http://simple-ft-train-service:8896" where uid = "ft_service";
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

alter table algorithm drop column is_train_able;
alter table algorithm drop column offline_url;
