-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

insert ignore into ensemble(uid, app_id, name, type, parameters, builtin) values('faq', 'system', 'faq', 'order', '{}', 1);

insert ignore into logic(uid, threshold, ensemble_uid, algorithm_uid) values('faq-ft', 97, 'faq', 'ft_service');
insert ignore into logic(uid, threshold, ensemble_uid, algorithm_uid) values('faq-qq', 0, 'faq', 'qq');

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
delete from ensemble where uid = 'faq' and app_id = 'system';
delete from logic where uid in ('faq-ft', 'faq-qq');
