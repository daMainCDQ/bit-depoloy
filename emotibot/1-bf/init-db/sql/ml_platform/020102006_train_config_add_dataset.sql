-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `train_config` ADD COLUMN `dataset_uid` varchar(32) COMMENT '数据集id' AFTER `parameters`;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

ALTER TABLE `train_config` DROP COLUMN `dataset_uid`;
