-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
-- ----------------------------
-- Table structure for app_model
-- ----------------------------
DROP TABLE IF EXISTS `app_model`;
CREATE TABLE `app_model` (
                           `id` int(11) NOT NULL AUTO_INCREMENT,
                           `app_id` varchar(64) NOT NULL COMMENT '机器人id',
                           `model_uid` varchar(64) NOT NULL COMMENT '模型id',
                           `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                           `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                           PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB  default charset=utf8mb4 collate utf8mb4_general_ci comment='已训练模型表';

-- ----------------------------
-- Table structure for app_model_select
-- ----------------------------
DROP TABLE IF EXISTS `app_model_select`;
CREATE TABLE `app_model_select` (
                                  `id` int(11) NOT NULL AUTO_INCREMENT,
                                  `app_id` varchar(64) NOT NULL COMMENT '机器人id',
                                  `model_uid` varchar(64) NOT NULL COMMENT '模型id',
                                  `type` int(11) NOT NULL,
                                  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB  default charset=utf8mb4 collate utf8mb4_general_ci comment='发布模型记录表';

-- ----------------------------
-- Table structure for app_select
-- ----------------------------
DROP TABLE IF EXISTS `app_select`;
CREATE TABLE `app_select` (
                            `id` int(11) NOT NULL AUTO_INCREMENT,
                            `app_id` varchar(64) NOT NULL COMMENT '机器人id',
                            `metric_uid` varchar(64) NOT NULL COMMENT '已选择的测试uid',
                            `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                            `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                            PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB   default charset=utf8mb4 collate utf8mb4_general_ci comment='选择模型记录表';

alter table app_model add index app_id_index (app_id);
alter table app_model_select add index app_id_index (app_id);
alter table app_select add index app_id_index (app_id);
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
drop table app_model;
drop table app_model_select;
drop table app_select;
