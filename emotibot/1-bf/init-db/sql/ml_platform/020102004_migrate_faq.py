import json
import math
import os
import uuid

import pymysql
from minio import Minio

mysql_host = os.environ.get('INIT_MYSQL_HOST', '172.16.102.135')
mysql_port = int(os.environ.get('INIT_MYSQL_PORT', '3306'))
mysql_user = os.environ.get('INIT_MYSQL_USER', 'root')
mysql_password = os.environ.get('INIT_MYSQL_PASSWORD', 'password')
mysql_db_ml_platform = 'ml_platform'
mysql_db_faq = 'faq3'
mysql_db_auth = 'auth'

minio_host = os.environ.get('INIT_MINIO_HOST', '172.16.102.135')
minio_port = os.environ.get('INIT_MINIO_PORT', '9000')
access_key = os.environ.get('INIT_MINIO_ACCESS_KEY', '24WTCKX23M0MRI6BA72Y')
secret_key = os.environ.get('INIT_MINIO_SECRET_KEY', '6mxoQMf1Lha2q1H3fp3fjNHEZRJJ7LvbC5k3+CQX')
secure = os.environ.get('INIT_MINIO_SECURE', 'false')
secure = True if secure == 'True' or secure == 'true' else False

ft_url = 'http://simple-ft-service:8895'
qq_url = 'http://faq-module-qq:11025/qq'


def migrate():
    minio_client_instance = Minio('{}:{}'.format(minio_host, minio_port), access_key=access_key, secret_key=secret_key,
                                  secure=secure)

    mlp_db = pymysql.connect(host=mysql_host, port=mysql_port, user=mysql_user, passwd=mysql_password,
                             db=mysql_db_ml_platform)
    faq_db = pymysql.connect(host=mysql_host, port=mysql_port, user=mysql_user, passwd=mysql_password,
                             db=mysql_db_faq)

    sql2 = 'select app_id, name, value from ent_config_appid_customization'

    sql3 = 'select uid, name from problem where uid = "{}"'

    insert_cursor = mlp_db.cursor()

    cursor_emotibot = faq_db.cursor()
    cursor_emotibot.execute(sql2)
    models = cursor_emotibot.fetchall()

    data_config = {
        "type": "minio",
        "location": "unknown",
        "dataset_uid": None,
        "sources": ['sentence'],
        "target": "label"
    }

    for app_id, name, value in models:
        if name not in ['ssm_config', 'ssm_config_rc']:
            continue

        is_released = name == 'ssm_config'
        ssm_config = json.loads(value)
        model_id = get_model_id(ssm_config)

        if not model_id or '_' not in model_id:
            continue

        model_uid = app_id + '_gold' if is_released else app_id + '_rc'
        qq_model_id = 'dac-gold' if is_released else 'dac-rc'
        qq_model_uid = str(uuid.uuid4()).replace('-', '')
        ft_model_id = model_id
        ft_model_uid = str(uuid.uuid4()).replace('-', '')
        models = list(minio_client_instance.list_objects('ft-model', model_id + '.ftz.dsc'))
        if len(models) == 0:
            continue
        ft_size = math.ceil(models[0].size / 1024 / 1024)
        qq_size = 10
        problem_id = app_id[:28] + 'faq'

        insert_cursor.execute(sql3.format(problem_id))
        problems = insert_cursor.fetchall()
        if len(problems) > 0:
            continue

        insert_cursor.execute("insert into problem(uid, app_id, name, alg_type, train_datasets, test_datasets) "
                              "values('{}', '{}', 'faq', 'text_classification', '[]', '[]') ON DUPLICATE KEY UPDATE name='faq'".format(problem_id, app_id))
        qq_config_id = str(uuid.uuid4()).replace('-', '')
        insert_cursor.execute(
            "insert into train_config(uid, app_id, algorithm_uid, parameters, data_config, other_config, template)" +
            "values('{}', '{}', '{}', '{{}}', '{}', '{}', '0')".format(qq_config_id, app_id, 'qq',
                                                                       json.dumps(data_config),
                                                                       json.dumps({'app_id': app_id})))
        ft_config_id = str(uuid.uuid4()).replace('-', '')
        insert_cursor.execute(
            "insert into train_config(uid, app_id, algorithm_uid, parameters, data_config, other_config, template)" +
            "values('{}', '{}', '{}', '{{}}', '{}', '{}', '0')".format(ft_config_id, app_id, 'ft_service',
                                                                       json.dumps(data_config),
                                                                       json.dumps({'app_id': app_id})))

        ft_predict_id = str(uuid.uuid4()).replace('-', '')
        qq_predict_id = str(uuid.uuid4()).replace('-', '')
        predict_id = str(uuid.uuid4()).replace('-', '')
        insert_cursor.execute(
            "insert into model(uid, app_id, model_id, name, train_config_uid, status, problem_uid, path, loaded, size, type, predict_config_uid, algorithm)" +
            "values('{}', '{}', '{}', '{}', '{}', '{}', '{}', 'mock_path', 0, '{}', '{}', '{}', '{}')"
            .format(ft_model_uid, app_id,
                    ft_model_id,
                    'faq模型-1',
                    ft_config_id,
                    'inactive',
                    problem_id, ft_size,
                    'single',
                    ft_predict_id,
                    'ft_service'))

        insert_cursor.execute(
            "insert into model(uid, app_id, model_id, name, train_config_uid, status, problem_uid, path, loaded, size, type, predict_config_uid, algorithm)" +
            "values('{}', '{}', '{}', '{}', '{}', '{}', '{}', 'mock_path', 0, '{}', '{}', '{}', '{}')"
            .format(qq_model_uid, app_id,
                    qq_model_id,
                    'faq模型-2', qq_config_id,
                    'active' if is_released else 'inactive', problem_id,
                    qq_size, 'single',
                    qq_predict_id, 'qq'))

        insert_cursor.execute(
            "insert into model(uid, app_id, model_id, name, status, problem_uid, path, loaded, type, predict_config_uid) "
            "values('{}', '{}', '{}', '{}', '{}', '{}', 'mock_path', 0, '{}', '{}')"
                .format(model_uid, app_id,
                        model_uid,
                        'faq模型',
                        'active', problem_id,
                        'ensemble',
                        predict_id))

        ft_single_model_strategy = [{"action": "single", "model_id": ft_model_uid, "threshold": 0}]
        qq_single_model_strategy = [{"action": "single", "model_id": qq_model_uid, "threshold": 0}]
        ensemble_strategy = [{"action": "single", "model_id": ft_model_uid, "threshold": 97},
                             {"action": "single", "model_id": qq_model_uid, "threshold": 0}]
        ssm_config = {
            "items": [{"name": "candidate", "value": 5.0},
                      {"name": "seg_url", "value": "http://snlu:13901"},
                      {"name": "rank",
                       "value": [{"dependency": [
                           {"name": "se", "weight": 0.6},
                           {"name": "w2v", "weight": 0.3},
                           {"name": "es", "weight": 0.1}
                       ],
                           "order": 1.0,
                           "source": "qq", "threadholds": 85.0}]},
                      {"name": "dependency",
                       "value": [
                           {"candidate": 30.0,
                            "clazz": "SSMDependencyES",
                            "enabled": True,
                            "method": "get",
                            "name": "es",
                            "order": 1.0,
                            "parameters": "p",
                            "result": [{
                                "answer": "@[d]/answer_list[0]",
                                "keywords": "@[d]/keywords",
                                "matchQuestion": "@[d]/question",
                                "question_id": "@[d]/question_id:int",
                                "score": "@[d]/score:~toPercent",
                                "source": "es",
                                "stop_words": "@[d]/stop_words",
                                "tokens": "@[d]/tokens"}],
                            "timeout": 60000.0,
                            "url": "@es_url"},
                           {"enabled": True,
                            "formData": False,
                            "method": "POST",
                            "name": "se",
                            "order": 2.0,
                            "parameters": {
                                "match_q": [
                                    "@es[d]/matchQuestion"],
                                "user_q": "@Text"},
                            "result": [{
                                "answer": "@es[d]/answer",
                                "matchQuestion": "@es[d]/matchQuestion",
                                "score": "@Msg/{d}:~toPercent",
                                "source": "SE"}],
                            "timeout": 60000.0,
                            "url": "http://ml:8895/similar"},
                           {"enabled": True,
                            "formData": False,
                            "method": "POST",
                            "name": "w2v",
                            "order": 2.0,
                            "parameters": [{
                                "src": {
                                    "src_kw": "@nlp/key_words:~joinByComma",
                                    "src_seg": "@nlp/tokens:~joinByComma",
                                    "stoplist": "@nlp/stop_words:~joinByComma"},
                                "tar": [
                                    {
                                        "id": "{d}",
                                        "tar_kw": "@es[d]/keywords:~joinByComma",
                                        "tar_seg": "@es[d]/tokens:~joinByComma"}]}],
                            "result": [{
                                "answer": "@es[d]/answer",
                                "matchQuestion": "@es[d]/matchQuestion",
                                "score": "@scores/{d}:~toPercent",
                                "source": "w2v"}],
                            "timeout": 60000.0,
                            "url": "http://rc-word2vec:11501/partial_w2v"}]}]}
        config = {ft_model_uid: {"app_id": app_id},
                  qq_model_uid: {"app_id": app_id}}

        insert_cursor.execute(
            "insert into predict_config(uid, app_id, model_uids, config, strategy)" +
            "values('{}', '{}', '{}', '{}', '{}')".format(ft_predict_id, app_id, json.dumps([ft_model_uid]), '{}',
                                                          json.dumps(ft_single_model_strategy))
        )

        insert_cursor.execute(
            "insert into predict_config(uid, app_id, model_uids, config, strategy)" +
            "values('{}', '{}', '{}', '{}', '{}')".format(qq_predict_id, app_id, json.dumps([qq_model_uid]),
                                                          json.dumps(ssm_config),
                                                          json.dumps(qq_single_model_strategy))
        )

        insert_cursor.execute(
            "insert into predict_config(uid, app_id, model_uids, config, strategy)" +
            "values('{}', '{}', '{}', '{}', '{}')".format(predict_id, app_id, json.dumps([ft_model_uid, qq_model_uid]),
                                                          json.dumps(config),
                                                          json.dumps(ensemble_strategy))
        )

    mlp_db.commit()


def get_model_id(ssm_config):
    for item in ssm_config['items']:
        if item['name'] == 'dependency':
            for vi in item['value']:
                if vi['name'] == 'ml':
                    return vi['parameters']['model']
    return None


if __name__ == '__main__':
    migrate()
