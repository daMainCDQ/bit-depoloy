-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

update algorithm set url = "http://intent-engine:15001/ml-platform" where uid = "intent";

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
update algorithm set url = "http://intent-engine-ml-adapter" where uid = "intent";
