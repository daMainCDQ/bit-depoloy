import os
import pymysql

mysql_host = os.environ.get('INIT_MYSQL_HOST', '127.0.0.1')
mysql_port = int(os.environ.get('INIT_MYSQL_PORT', '3306'))
mysql_user = os.environ.get('INIT_MYSQL_USER', 'root')
mysql_password = os.environ.get('INIT_MYSQL_PASSWORD', 'password')
mysql_db_ml_platform = 'ml_platform'
mysql_db_emotibot = 'emotibot'
faq_offline = "_faq_offline"
faq_online = "_faq_online"


def migrate():

    db = pymysql.connect(host=mysql_host, port=mysql_port, user=mysql_user, passwd=mysql_password,
                         db=mysql_db_ml_platform)

    emotibot_db = pymysql.connect(host=mysql_host, port=mysql_port, user=mysql_user, passwd=mysql_password,
                                  db=mysql_db_emotibot)

    problem_query_sql = 'select uid, app_id from problem;'
    problem_update_sql = 'update problem set type = "{}" where uid = "{}"'
    intent_group_sql = 'select group_id from intent_group;'
    emotion_group_sql = 'select group_id from emotion_group_info;'
    cursor = emotibot_db.cursor()

    cursor.execute(intent_group_sql)
    intent_group_ids = set(cursor.fetchall())
    cursor.execute(emotion_group_sql)
    emotion_group_ids = set(cursor.fetchall())

    insert_cursor = db.cursor()
    insert_cursor.execute(problem_query_sql)
    problem_uids = insert_cursor.fetchall()

    for each in problem_uids:
        problem_uid = each[0]
        app_id = each[1]
        # 新问题uid
        if '_faq_' in problem_uid:
            insert_cursor.execute(problem_update_sql.format('system', problem_uid))
        elif '_emotibot_label' in problem_uid:
            insert_cursor.execute(problem_update_sql.format('system', problem_uid))
        elif problem_uid in intent_group_ids:
            insert_cursor.execute(problem_update_sql.format('system', problem_uid))
        elif problem_uid in emotion_group_ids:
            insert_cursor.execute(problem_update_sql.format('system', problem_uid))
        elif app_id == 'model_lib':
            insert_cursor.execute(problem_update_sql.format('internal', problem_uid))
        else:
            insert_cursor.execute(problem_update_sql.format('custom', problem_uid))

    db.commit()
    db.close()


if __name__ == '__main__':
    migrate()
