-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

drop table if exists `predict_config`;
create table `predict_config`
(
  id          int auto_increment
    primary key,
  uid         varchar(32)                         not null  comment '预测配置uuid',
  app_id      varchar(32)                         not null  comment '机器人id',
  type        varchar(32)                         null      comment '配置类型',
  model_uids  varchar(1024)                       null      comment '子模型id',
  config      text                                null      comment '出话配置',
  strategy    text                                null      comment '多模型策略',
  create_time timestamp default CURRENT_TIMESTAMP not null,
  update_time timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP,
  constraint predict_config_uid_uindex
  unique (uid)
) engine=InnoDB default charset=utf8mb4 collate utf8mb4_general_ci comment='预测配置';

create index uid_index
  on predict_config (uid);

ALTER TABLE `model` ADD COLUMN `type` varchar(32) NOT NULL COMMENT '模型类型' AFTER `size`;
ALTER TABLE `model` ADD COLUMN `parent_uid` varchar(32) COMMENT '父模型id' AFTER `type`;
ALTER TABLE `model` ADD COLUMN `predict_config_uid` varchar(32) COMMENT '预测配置' AFTER `parent_uid`;
UPDATE model set `type` = 'single';


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
drop table predict_config;

ALTER TABLE `model` DROP COLUMN `type`;
ALTER TABLE `model` DROP COLUMN `parent_uid`;
ALTER TABLE `model` DROP COLUMN `predict_config_uid`;
