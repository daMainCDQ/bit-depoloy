-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied


ALTER TABLE `algorithm` ADD COLUMN `description` varchar(1000)   COMMENT '描述'  DEFAULT NULL AFTER `extend`;
ALTER TABLE `model_lib` ADD COLUMN `description` varchar(1000)   COMMENT '描述'  DEFAULT NULL AFTER `parent_uid`;
ALTER TABLE `pretrain_model` ADD COLUMN `description` varchar(1000)   COMMENT '描述'  DEFAULT NULL AFTER `progress`;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE `algorithm` DROP COLUMN `description`;
ALTER TABLE `model_lib` DROP COLUMN `description`;
ALTER TABLE `pretrain_model` DROP COLUMN `description`;
