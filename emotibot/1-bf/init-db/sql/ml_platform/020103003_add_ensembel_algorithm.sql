-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

-- ----------------------------
-- Table structure for ensemble
-- ----------------------------
DROP TABLE IF EXISTS `ensemble`;
CREATE TABLE `ensemble` (
                          `id` int(11) NOT NULL AUTO_INCREMENT,
                          `uid` varchar(32) NOT NULL COMMENT '算法uuid',
                          `app_id` varchar(64) NOT NULL COMMENT '机器人id',
                          `name` varchar(256) NOT NULL COMMENT '集成算法名称',
                          `type` varchar(32) NOT NULL COMMENT '集成算法类型',
                          `parameters` text NOT NULL COMMENT '超参定义',
                          `builtin` tinyint(1)  NOT NULL COMMENT '是否内建',
                          `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                          `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                          PRIMARY KEY (`id`)
) engine=InnoDB default charset=utf8mb4 collate utf8mb4_general_ci comment='集成算法表';

CREATE UNIQUE INDEX ensemble_uid_uindex ON ensemble (uid);

DROP TABLE IF EXISTS `logic`;
CREATE TABLE `logic` (
                       `id` int(11) NOT NULL AUTO_INCREMENT,
                       `uid` varchar(32) NOT NULL COMMENT 'uuid',
                       `threshold` int(11) NOT NULL,
                       `ensemble_uid` varchar(32) NOT NULL COMMENT '集成算法uuid',
                       `algorithm_uid` varchar(32) NOT NULL COMMENT '算法uuid',
                       `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                       `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                       PRIMARY KEY (`id`)
) engine=InnoDB default charset=utf8mb4 collate utf8mb4_general_ci comment='集成逻辑表';

CREATE UNIQUE INDEX logic_uid_uindex ON logic (uid);

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
drop table ensemble;
drop table logic;
