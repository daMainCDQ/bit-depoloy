-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for schedule_policy
-- ----------------------------
DROP TABLE IF EXISTS `schedule_policy`;
CREATE TABLE `schedule_policy` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'DB自增ID',
  `access_key` varchar(64) DEFAULT NULL COMMENT '类似于公有云服务的accessKey\n',
  `uuid` varchar(64) DEFAULT NULL COMMENT '一项策略的唯一id',
  `policy` text COMMENT '策略内存，json格式字符串',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '创建或更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of schedule_policy
-- ----------------------------
BEGIN;
INSERT INTO `schedule_policy` VALUES (1, 'sys_default', NULL, '{\n  \"time_unit\":\"hour\",\n  \"predict_threshold\":1,\n  \"no_predict_time\":12,\n  \"observe_period\":\"12,24,36\"\n}', NULL);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `schedule_policy`;
