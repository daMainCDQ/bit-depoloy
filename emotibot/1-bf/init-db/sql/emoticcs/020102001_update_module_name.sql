-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
update ccs_bot set name = '未知回复', nickname = '未知回复' where name = '万金油话术' or nickname = '万金油话术';
update ccs_bot set name = '未知回復', nickname = '未知回復' where name = '萬金油話術' or nickname = '萬金油話術';
update ccs_phase set name = '未知回复出话' where name = '万金油出话';
update ccs_phase set name = '未知回復出話' where name = '萬金油出話';

update task_result set bot_result = 'backfill' where bot_result = '万金油话术';
update task_result set bot_result = 'emotion' where bot_result = '情绪';
update task_result set bot_result = 'kg' where bot_result = '通用知识库';
update task_result set bot_result = 'chat' where bot_result = '闲聊';
update task_result set bot_result = 'faq' where bot_result = '常用标准问题';
update task_result set bot_result = 'domain_kg' where bot_result = '知识推理引擎';
update task_result set bot_result = 'remote_skill' where bot_result = '技能发现';
update task_result set bot_result = 'task_engine' where bot_result = '任务流引擎';
update task_result set bot_result = 'tde_task_engine' where bot_result = '多轮填槽引擎';
update task_result set bot_result = 'to_human' where bot_result = '转人工';
update task_result set bot_result = 'keyword' where bot_result = '关键字模块';
update task_result set bot_result = 'chat_story' where bot_result = '聊天小故事';
update task_result set bot_result = 'function' where bot_result = '本地技能';
update task_result set bot_result = 'custom_skill' where bot_result = '定制技能';





update task_result set module_result = 'backfill' where module_result = '万金油话术';
update task_result set module_result = 'emotion' where module_result = '情绪';
update task_result set module_result = 'kg' where module_result = '通用知识库';
update task_result set module_result = 'chat' where module_result = '闲聊';
update task_result set module_result = 'faq' where module_result = '常用标准问题';
update task_result set module_result = 'domain_kg' where module_result = '知识推理引擎';
update task_result set module_result = 'remote_skill' where module_result = '技能发现';
update task_result set module_result = 'task_engine' where module_result = '任务流引擎';
update task_result set module_result = 'tde_task_engine' where module_result = '多轮填槽引擎';
update task_result set module_result = 'to_human' where module_result = '转人工';
update task_result set module_result = 'keyword' where module_result = '关键字模块';
update task_result set module_result = 'chat_story' where module_result = '聊天小故事';
update task_result set module_result = 'function' where module_result = '本地技能';
update task_result set module_result = 'custom_skill' where module_result = '定制技能';



update task_result set target_module = 'backfill' where target_module = '万金油话术';
update task_result set target_module = 'emotion' where target_module = '情绪';
update task_result set target_module = 'kg' where target_module = '通用知识库';
update task_result set target_module = 'chat' where target_module = '闲聊';
update task_result set target_module = 'faq' where target_module = '常用标准问题';
update task_result set target_module = 'domain_kg' where target_module = '知识推理引擎';
update task_result set target_module = 'remote_skill' where target_module = '技能发现';
update task_result set target_module = 'task_engine' where target_module = '任务流引擎';
update task_result set target_module = 'tde_task_engine' where target_module = '多轮填槽引擎';
update task_result set target_module = 'to_human' where target_module = '转人工';
update task_result set target_module = 'keyword' where target_module = '关键字模块';
update task_result set target_module = 'chat_story' where target_module = '聊天小故事';
update task_result set target_module = 'function' where target_module = '本地技能';
update task_result set target_module = 'custom_skill' where target_module = '定制技能';


update test_case set target_module = 'backfill' where target_module = '万金油话术';
update test_case set target_module = 'emotion' where target_module = '情绪';
update test_case set target_module = 'kg' where target_module = '通用知识库';
update test_case set target_module = 'chat' where target_module = '闲聊';
update test_case set target_module = 'faq' where target_module = '常用标准问题';
update test_case set target_module = 'domain_kg' where target_module = '知识推理引擎';
update test_case set target_module = 'remote_skill' where target_module = '技能发现';
update test_case set target_module = 'task_engine' where target_module = '任务流引擎';
update test_case set target_module = 'tde_task_engine' where target_module = '多轮填槽引擎';
update test_case set target_module = 'to_human' where target_module = '转人工';
update test_case set target_module = 'keyword' where target_module = '关键字模块';
update test_case set target_module = 'chat_story' where target_module = '聊天小故事';
update test_case set target_module = 'function' where target_module = '本地技能';
update test_case set target_module = 'custom_skill' where target_module = '定制技能';
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back