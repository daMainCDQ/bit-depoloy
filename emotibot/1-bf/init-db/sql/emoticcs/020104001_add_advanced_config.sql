-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

--
-- Table structure for table `ccs_bot`
--
USE `emoticcs`;

START TRANSACTION;

ALTER TABLE ccs_conf ADD advanced_config text NULL COMMENT '高级配置，是否启用一句多问等高级配置';

COMMIT;
-- +migrate Down