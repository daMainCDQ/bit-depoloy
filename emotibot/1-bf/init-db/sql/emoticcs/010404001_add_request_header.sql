-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

--
-- Table structure for table `ccs_bot`
--
USE `emoticcs`;

START TRANSACTION;

ALTER TABLE ccs_bot ADD request_header text NULL COMMENT '请求头';

COMMIT;
