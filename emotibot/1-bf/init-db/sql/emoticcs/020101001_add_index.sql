-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
create index uid_index
  on ccs_action (uid);

create index uid_index
  on ccs_bot (uid);

create index uid_index
  on ccs_arg (uid);

create index uid_index
  on ccs_conf (uid);

create index uid_index
  on ccs_info_standard_key (uid);

create index uid_index
  on ccs_info_publish (uid);

create index uid_index
  on ccs_info_subscribe (uid);

create index uid_index
  on ccs_model (uid);

create index ccs_id_index
  on ccs_action (ccs_id);

create index ccs_id_index
  on ccs_bot (ccs_id);

create index ccs_id_index
  on ccs_arg (ccs_id);

create index ccs_id_index
  on ccs_conf (ccs_id);

create index ccs_id_index
  on ccs_info_standard_key (ccs_id);

create index ccs_id_index
  on ccs_info_publish (ccs_id);

create index ccs_id_index
  on ccs_info_subscribe (ccs_id);

create index ccs_id_index
  on ccs_model (ccs_id);

create index ccs_id_uid_index
  on ccs_phase (ccs_id, uid);

create index ccs_id_uid_index
  on conf_unit (ccs_id, uid);

create index ccs_test_result_index
on task_result (ccs_id, task_id);

create index ccs_test_case_index
on test_case (ccs_id, dataset);

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back