-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

--
-- Table structure for table `ccs_bot`
--
USE `emoticcs`;

START TRANSACTION;

CREATE TABLE `ccs_action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ccs_id` varchar(64) DEFAULT NULL COMMENT '中控id',
  `name` varchar(255) DEFAULT NULL COMMENT '名字',
  `url` varchar(1024) DEFAULT NULL COMMENT '地址',
  `method` varchar(32) DEFAULT NULL COMMENT 'http方法',
  `request_body` text COMMENT 'http请求体',
  `header` text COMMENT 'http请求头部',
  `app_id` varchar(64) DEFAULT NULL COMMENT 'appid',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COMMENT='中控action';

COMMIT;