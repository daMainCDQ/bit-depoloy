-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

--
-- Table structure for table `ccs_arg`
--
USE `emoticcs`;

START TRANSACTION;

-- auto-generated definition
create table ccs_info_publish
(
  id           int auto_increment
    primary key,
  ccs_id       varchar(64)                         not null comment '中控id',
  module_id    int                                 not null comment '发布模块的id',
  module_key   varchar(128)                        not null comment '发布模块key名',
  standard_key_id  int                        not null comment '模块发布的标准key名称',
  created_time timestamp default CURRENT_TIMESTAMP not null comment '创建时间',
  updated_time timestamp default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP comment '修改时间'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='中控信息共享发布表';
create index ccs_info_publish_index on ccs_info_publish (ccs_id, module_id);

create table ccs_info_subscribe
(
  id               int auto_increment
    primary key,
  ccs_id           varchar(64)                         not null comment '中控id',
  module_id        int                                 not null comment '模块id',
  module_key       varchar(128)                        not null comment '订阅模块key名',
  standard_key_id  int                        not null comment '订阅的标准key id',
  created_time     timestamp default CURRENT_TIMESTAMP not null comment '创建时间',
  updated_time     timestamp default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP comment '修改时间'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='中控信息共享订阅表';
create index ccs_info_subscribe_index on ccs_info_subscribe (ccs_id, module_id);

create table ccs_info_standard_key
(
  id               int auto_increment
    primary key,
  ccs_id           varchar(64)                         not null comment '中控id',
  name             varchar(128)                        not null comment '标准key名称',
  created_time     timestamp default CURRENT_TIMESTAMP not null comment '创建时间',
  updated_time     timestamp default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP comment '修改时间'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='中控信息共享 标准key表';
create index ccs_info_standard_key_index on ccs_info_standard_key (ccs_id, name);

COMMIT;
