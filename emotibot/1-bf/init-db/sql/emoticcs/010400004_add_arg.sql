-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

--
-- Table structure for table `ccs_arg`
--
USE `emoticcs`;

START TRANSACTION;

CREATE TABLE `ccs_arg` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `ccs_id` varchar(64) DEFAULT NULL COMMENT '中控id',
  `name` varchar(255) DEFAULT NULL COMMENT '参数名称',
  `path` varchar(255) DEFAULT NULL COMMENT '参数路径',
  `value` varchar(1024) DEFAULT NULL COMMENT '参数内容',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='参数';

COMMIT;
