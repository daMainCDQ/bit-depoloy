-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
alter table test_case modify target_reply text not null comment '期望回复';
alter table test_case modify query text null comment '用户问询';
alter table test_case
	add `index` int not null comment '该测试题在测试集中的顺序' after dataset;
alter table test_case
	add dimension varchar(255) null comment '对话时此测试题携带的维度' after must_pass;
alter table test_case
	add mode varchar(64) default 'offline' null comment '模式 线上/线下' after dimension;
alter table test_case
	add extend text null comment '扩展参数' after mode;
alter table task_result
	add case_index int not null comment '该测试题在测试集中的位置' after must_pass;
alter table task_result
	add dimension varchar(255) null comment '对话时此测试题携带的维度' after case_index;
alter table task_result
	add mode varchar(64) null comment '模式 线上/线下' after dimension;
alter table task_result
	add extend text null comment '扩展参数' after mode;
alter table task_result modify target_reply text null comment '期望答案';