-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

--
-- Table structure for table `ccs_phase`
--
USE `emoticcs`;

START TRANSACTION;

create table ccs_phase
(
  id           int auto_increment
    comment '自增id' primary key,
  ccs_id       varchar(64)                         not null comment '中控id',
  config_id    int                                 not null comment '配置id',
  name         varchar(64)                         not null comment '阶段名字',
  config_units varchar(1024)                       not null comment '配置单元',
  created_time timestamp default CURRENT_TIMESTAMP not null comment '创建时间',
  updated_time timestamp default CURRENT_TIMESTAMP not null comment '更新时间'
)ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='中控阶段';

create index ccs_phase_config_id_index
  on ccs_phase (config_id);

ALTER TABLE ccs_conf CHANGE conf_units phases text NOT NULL COMMENT '配置单元';
ALTER TABLE conf_unit ADD config_id int NOT NULL COMMENT '配置id';
ALTER TABLE conf_unit ADD phase_id int COMMENT '阶段id';
ALTER TABLE ccs_bot ADD scenario_id varchar(64) COMMENT '场景id';

COMMIT;
