-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE ccs_model ADD enable_cache tinyint(1) NOT NULL DEFAULT '1' COMMENT '接入服务是否使用缓存';

-- +migrate Down