-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
alter table task_result modify trace mediumtext null comment '调用信息';
alter table task_result modify reply_result text null comment '机器人实际回复';
alter table task_result modify response text null comment '实际回复';
