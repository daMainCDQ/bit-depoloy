-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied


USE `emoticcs`;

START TRANSACTION;

ALTER TABLE `ccs_bot` ADD COLUMN `active` tinyint DEFAULT NULL COMMENT '是否激活' after `app_id`;
ALTER TABLE `ccs_bot` ADD COLUMN `timeout` int DEFAULT NULL COMMENT '超时时间' after `active`;

ALTER TABLE `ccs_model` ADD COLUMN `request_body` text DEFAULT NULL COMMENT 'http请求体' after `category`;
ALTER TABLE `ccs_model` ADD COLUMN `app_id` varchar(64) DEFAULT NULL COMMENT '模型id' after `request_body`;

ALTER TABLE `task_result` DROP COLUMN `dataset`;
ALTER TABLE `task_result` DROP COLUMN `config_id`;
ALTER TABLE `task_result` ADD COLUMN `query` text DEFAULT NULL COMMENT '用户问题' after `task_id`;
ALTER TABLE `task_result` ADD COLUMN `target_reply` varchar(4096) DEFAULT NULL COMMENT '期望答案' after `module_correct`;
ALTER TABLE `task_result` ADD COLUMN `target_bot` varchar(255) DEFAULT NULL COMMENT '期望机器人' after `target_reply`;
ALTER TABLE `task_result` ADD COLUMN `target_module` varchar(255) DEFAULT NULL COMMENT '期望模块' after `target_bot`;
ALTER TABLE `task_result` ADD COLUMN `expect_score` float DEFAULT NULL COMMENT '期望模块分数' after `score`;
ALTER TABLE `task_result` ADD COLUMN `trace` text DEFAULT NULL COMMENT '调用信息' after `response`;
ALTER TABLE `task_result` ADD COLUMN `session_id` varchar(64) DEFAULT NULL COMMENT '会话id' after `trace`;
ALTER TABLE `task_result` ADD COLUMN `must_pass` tinyint DEFAULT NULL COMMENT '是否必过' after `session_id`;

ALTER TABLE `test_case` ADD COLUMN `session_id` varchar(64) DEFAULT NULL COMMENT '会话id' after `target_module`;
ALTER TABLE `test_case` ADD COLUMN `must_pass` tinyint DEFAULT NULL COMMENT '是否必过' after `session_id`;

ALTER TABLE `test_task` ADD COLUMN `config` text DEFAULT NULL COMMENT '具体配置' after `config_id`;
ALTER TABLE `test_task` ADD COLUMN `must_pass_reply_precision` float DEFAULT NULL COMMENT '必过测试回复准确率' after `module_precision`;
ALTER TABLE `test_task` ADD COLUMN `must_pass_bot_precision` float DEFAULT NULL COMMENT '必过测试机器人准确率' after `must_pass_reply_precision`;
ALTER TABLE `test_task` ADD COLUMN `must_pass_module_precision` float DEFAULT NULL COMMENT '必过测试模块准确率' after `must_pass_bot_precision`;

COMMIT;