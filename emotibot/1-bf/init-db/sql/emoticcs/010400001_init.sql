-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

--
-- Table structure for table `ccs_bot`
--
USE `emoticcs`;

START TRANSACTION;

CREATE TABLE `ccs_bot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ccs_id` varchar(64) NOT NULL COMMENT '中控id',
  `name` varchar(255) NOT NULL COMMENT '机器人名字',
  `nickname` varchar(255) DEFAULT NULL COMMENT '机器人昵称',
  `url` varchar(1024) DEFAULT NULL COMMENT '机器人地址',
  `request_body` text COMMENT '请求体',
  `modules` varchar(255) DEFAULT NULL COMMENT '模块名称',
  `app_id` varchar(64) NOT NULL COMMENT '机器人appid',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb4 COMMENT='中控AI模块';

--
-- Table structure for table `ccs_conf`
--

CREATE TABLE `ccs_conf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ccs_id` varchar(64) NOT NULL COMMENT '中控id',
  `name` varchar(255) NOT NULL COMMENT '配置名称',
  `conf_units` text NOT NULL COMMENT '配置单元',
  `ranker_config` text NOT NULL COMMENT 'ranker配置',
  `extra_config` text COMMENT '附加配置',
  `status` varchar(32) NOT NULL COMMENT '状态',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=utf8mb4 COMMENT='中控配置';

--
-- Table structure for table `ccs_model`
--

CREATE TABLE `ccs_model` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ccs_id` varchar(64) NOT NULL COMMENT '中控id',
  `name` varchar(255) NOT NULL COMMENT '模型名称',
  `url` varchar(1024) DEFAULT NULL COMMENT '模型地址',
  `method` varchar(32) NOT NULL COMMENT 'http方法',
  `category` varchar(32) NOT NULL COMMENT '模型类别',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COMMENT='模型定义';

--
-- Table structure for table `conf_unit`
--

CREATE TABLE `conf_unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ccs_id` varchar(64) NOT NULL COMMENT '中控id',
  `name` varchar(255) DEFAULT NULL COMMENT '配置名称',
  `category` varchar(32) NOT NULL COMMENT '配置类别',
  `content` text NOT NULL COMMENT '配置内容',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1402 DEFAULT CHARSET=utf8mb4 COMMENT='配置单元';

--
-- Table structure for table `task_result`
--

CREATE TABLE `task_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ccs_id` varchar(64) DEFAULT NULL COMMENT '中控id',
  `case_id` int(11) NOT NULL COMMENT '测试用例id',
  `task_id` varchar(64) NOT NULL COMMENT '测试任务id',
  `dataset` varchar(64) DEFAULT NULL COMMENT '数据集',
  `config_id` varchar(64) DEFAULT NULL COMMENT '配置id',
  `reply_result` varchar(1024) DEFAULT NULL COMMENT '机器人实际回复',
  `bot_result` varchar(64) DEFAULT NULL COMMENT '机器人名称',
  `module_result` varchar(64) DEFAULT NULL COMMENT '模块名称',
  `reply_correct` tinyint(1) DEFAULT NULL COMMENT '期望回复与实际回复是否一致',
  `bot_correct` tinyint(1) DEFAULT NULL COMMENT '期望机器人名称与实际是否一致',
  `module_correct` tinyint(1) DEFAULT NULL COMMENT '期望模块名称与实际是否一致',
  `score` float DEFAULT NULL COMMENT '实际回复分数',
  `response` text COMMENT '实际回复',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2176 DEFAULT CHARSET=utf8mb4 COMMENT='测试结果';
create index task_result_case_id_index on task_result (case_id);
create index task_result_task_id_index on task_result (task_id);

--
-- Table structure for table `test_case`
--

CREATE TABLE `test_case` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ccs_id` varchar(64) DEFAULT NULL COMMENT '中控id',
  `dataset` varchar(64) DEFAULT NULL COMMENT '数据集',
  `query` varchar(255) DEFAULT NULL COMMENT '用户问询',
  `target_reply` varchar(1024) DEFAULT NULL COMMENT '期望回复',
  `target_bot` varchar(255) DEFAULT NULL COMMENT '期望机器人',
  `target_module` varchar(255) DEFAULT NULL COMMENT '期望模块',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3181 DEFAULT CHARSET=utf8mb4 COMMENT='测试用例';

--
-- Table structure for table `test_dataset`
--

CREATE TABLE `test_dataset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ccs_id` varchar(64) DEFAULT NULL COMMENT '中控id',
  `dataset_id` varchar(64) DEFAULT NULL COMMENT '数据集id',
  `dataset_name` varchar(1024) DEFAULT NULL COMMENT '数据集名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COMMENT='测试数据集';

--
-- Table structure for table `test_task`
--

CREATE TABLE `test_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ccs_id` varchar(64) DEFAULT NULL COMMENT '中控id',
  `dataset` varchar(64) NOT NULL COMMENT '数据集',
  `task_id` varchar(64) NOT NULL COMMENT '任务id',
  `config_id` varchar(64) DEFAULT NULL COMMENT '配置id',
  `complete` tinyint(1) DEFAULT NULL COMMENT '任务是否完成',
  `reply_precision` float DEFAULT NULL COMMENT '回复准确率',
  `bot_precision` float DEFAULT NULL COMMENT '机器人准确率',
  `module_precision` float DEFAULT NULL COMMENT '模块准确率',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8mb4 COMMENT='测试任务';

COMMIT;
