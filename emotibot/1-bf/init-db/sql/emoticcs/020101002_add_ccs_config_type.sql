-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
alter table ccs_conf
	add config_type varchar(64) default 'general' null comment '配置类型， 默认为general, 为bf标准的机器人' after status;

alter table ccs_conf modify updated_time timestamp default CURRENT_TIMESTAMP not null comment '修改时间' after config_type;


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back