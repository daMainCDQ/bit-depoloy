-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

--
-- Table structure for table `ccs_bot`
--
USE `emoticcs`;

START TRANSACTION;

ALTER TABLE ccs_action CHANGE header request_header text COMMENT 'http请求头部';
ALTER TABLE ccs_action ADD COLUMN uid varchar(64) AFTER id;
ALTER TABLE ccs_arg ADD COLUMN uid varchar(64) AFTER id;
ALTER TABLE ccs_bot ADD COLUMN uid varchar(64) AFTER id;
ALTER TABLE ccs_conf ADD COLUMN uid varchar(64) AFTER id;
ALTER TABLE ccs_info_publish ADD COLUMN uid varchar(64) AFTER id;
ALTER TABLE ccs_info_standard_key ADD COLUMN uid varchar(64) AFTER id;
ALTER TABLE ccs_info_subscribe ADD COLUMN uid varchar(64) AFTER id;
ALTER TABLE ccs_model ADD COLUMN uid varchar(64) AFTER id;
ALTER TABLE conf_unit ADD COLUMN uid varchar(64) AFTER id;
ALTER TABLE ccs_phase ADD COLUMN uid varchar(64) AFTER id;

UPDATE ccs_action SET uid=id;
UPDATE ccs_arg SET uid=id;
UPDATE ccs_bot SET uid=id;
UPDATE ccs_action SET uid=id;
UPDATE ccs_conf SET uid=id;
UPDATE ccs_info_publish SET uid=id;
UPDATE ccs_info_standard_key SET uid=id;
UPDATE ccs_info_subscribe SET uid=id;
UPDATE ccs_model SET uid=id;
UPDATE conf_unit SET uid=id;
UPDATE ccs_phase SET uid=id;

ALTER TABLE ccs_info_publish MODIFY COLUMN standard_key_id  varchar(64) not null comment '订阅的标准key id';
ALTER TABLE ccs_info_publish MODIFY COLUMN module_id varchar(64) not null comment '发布模块的id';
ALTER TABLE ccs_info_subscribe MODIFY COLUMN standard_key_id varchar(64) not null comment '订阅的标准key id';
ALTER TABLE ccs_info_subscribe MODIFY COLUMN module_id varchar(64) not null comment '订阅的模块id';
ALTER TABLE ccs_phase MODIFY COLUMN config_id varchar(64) not null comment '配置id';
ALTER TABLE conf_unit MODIFY COLUMN phase_id varchar(64) null comment '策略id';
ALTER TABLE conf_unit MODIFY COLUMN config_id varchar(64) not null comment '配置id';

COMMIT;
