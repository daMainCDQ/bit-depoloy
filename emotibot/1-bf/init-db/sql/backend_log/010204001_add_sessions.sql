-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

CREATE TABLE IF NOT EXISTS `sessions` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `session_id` VARCHAR(128) NOT NULL COMMENT 'session的唯一辨識碼',
  `start_time` bigint NOT NULL DEFAULT 0 COMMENT 'UTC Timestamp of first conversation time',
  `end_time` bigint NOT NULL DEFAULT 0 COMMENT 'UTC Timestamp of last conversation time',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `session_id_UNIQUE` (`session_id` ASC))
ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;