-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `chat_record` ADD `tspan` INT NOT NULL DEFAULT '0' COMMENT '花费时间' AFTER `feedback_time`, ADD INDEX `tspan_index` (`tspan`);
-- +migrate Down
ALTER TABLE `chat_record` DROP `tspan`;
