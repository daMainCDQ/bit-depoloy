-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE `taskengine_record` (
  `taskengine_session_id` char(64) CHARACTER SET utf8mb4 NOT NULL COMMENT '任務引擎日志id',
  `app_id` char(64) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '機器人id',
  `user_id` char(64) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '使用者id',
  `session_id` char(64) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '会话id',
  `scenario_id` char(64) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '場景id',
  `scenario_name` char(30) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '場景名稱',
  `last_node_id` char(64) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '最後節點id',
  `last_node_name` char(64) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '最後節點名稱',
  `trigger_time` bigint(20) DEFAULT NULL COMMENT '场景触发时间',
  `finish_time` bigint(20) DEFAULT NULL COMMENT '场景完成时间',
  `custom_info` text CHARACTER SET utf8mb4 COMMENT '客制化讯息',
  `feedback` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '用户反馈',
  `custom_feedback` text CHARACTER SET utf8mb4 COMMENT '客制化反馈',
  `feedback_time` bigint(20) DEFAULT NULL COMMENT '反馈时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='任务引擎日志表';

ALTER TABLE `taskengine_record`
  ADD PRIMARY KEY (`taskengine_session_id`),
  ADD KEY `appid_index` (`app_id`);
-- +migrate Down
DROP TABLE `taskengine_record`;
