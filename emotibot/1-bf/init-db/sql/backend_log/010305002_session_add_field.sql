-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `sessions` ADD `app_id` char(64) NOT NULL DEFAULT '' COMMENT '机器人id' AFTER `id`;
ALTER TABLE `sessions` ADD `user_id` char(64) NOT NULL DEFAULT '' COMMENT '使用者id';
ALTER TABLE `sessions` ADD `rating` TINYINT(1) NULL DEFAULT NULL COMMENT '用户满意度' AFTER `custom_info`
, ADD `feedback` VARCHAR(200) NULL DEFAULT NULL COMMENT '用户反馈内容' AFTER `rating`
, ADD `custom_feedback` TEXT NULL DEFAULT NULL COMMENT '保留可以讓用戶輸入其他額外建議的欄位' AFTER `feedback`
, ADD `feedback_time` BIGINT(20) NULL DEFAULT NULL COMMENT '用户反馈时间' AFTER `custom_feedback`;
-- +migrate Down
ALTER TABLE `sessions` DROP `app_id`;
ALTER TABLE `sessions` DROP `user_id`;
ALTER TABLE `sessions` DROP `rating`;
ALTER TABLE `sessions` DROP `feedback`;
ALTER TABLE `sessions` DROP `custom_feedback`;
ALTER TABLE `sessions` DROP `feedback_time`;
