-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `sessions` ADD `custom_info` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'json 客制化栏位' AFTER `session_id`;

-- +migrate Down
ALTER TABLE `sessions` DROP `custom_info`;
