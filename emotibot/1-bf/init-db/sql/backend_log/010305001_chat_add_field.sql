-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `chat_record` ADD `taskengine_session_id` CHAR(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '任务引擎场景id' AFTER `session_id`;
ALTER TABLE `chat_record` ADD `source` CHAR(32) NULL DEFAULT '' COMMENT '出话模组来源' AFTER `module`;
ALTER TABLE `chat_record` ADD `faq_cat_id` int(11) DEFAULT NULL COMMENT '标准问分类id' AFTER `intent_score`;
ALTER TABLE `chat_record` ADD `faq_robot_tag_id` varchar(100) DEFAULT NULL COMMENT '标准问标签id阵列' AFTER `faq_cat_id`;
ALTER TABLE `chat_record` ADD `threshold` int(11) DEFAULT '0' COMMENT '模组出话阀值' AFTER `faq_robot_tag_id`;
ALTER TABLE `chat_record` ADD `feedback` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '用户反馈' AFTER `threshold`;
ALTER TABLE `chat_record` ADD `custom_feedback` text CHARACTER SET utf8mb4 COMMENT '客制化用户反馈' AFTER `feedback`;
ALTER TABLE `chat_record` ADD `feedback_time` bigint(20) DEFAULT NULL COMMENT '反馈时间' AFTER `custom_feedback`;

-- +migrate Down
ALTER TABLE `chat_record` DROP `taskengine_session_id`;
ALTER TABLE `chat_record` DROP `source`;
ALTER TABLE `chat_record` DROP `faq_cat_id`;
ALTER TABLE `chat_record` DROP `faq_robot_tag_id`;
ALTER TABLE `chat_record` DROP `threshold`;
ALTER TABLE `chat_record` DROP `feedback`;
ALTER TABLE `chat_record` DROP `custom_feedback`;
ALTER TABLE `chat_record` DROP `feedback_time`;
