-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `sessions` ADD `chat_cnt` INT NOT NULL DEFAULT '1' COMMENT '该会话之对话数量' AFTER `end_time`, ADD `is_to_human` BOOLEAN NOT NULL DEFAULT FALSE COMMENT '会话过程中是否有转人工' AFTER `chat_cnt`, ADD `to_human_source` VARCHAR(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '转人工的来源' AFTER `is_to_human`, ADD `to_human_time` BIGINT NOT NULL DEFAULT '0' COMMENT '转人工的时间' AFTER `to_human_source`, ADD INDEX `chat_cnt_index` (`chat_cnt`), ADD INDEX `is_to_human_index` (`is_to_human`), ADD INDEX `to_human_source_index` (`to_human_source`);
-- +migrate Down
ALTER TABLE `sessions`
  DROP `chat_cnt`,
  DROP `is_to_human`,
  DROP `to_human_source`,
  DROP `to_human_time`;
