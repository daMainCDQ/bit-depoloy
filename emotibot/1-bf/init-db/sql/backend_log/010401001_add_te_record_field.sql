-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `taskengine_record` ADD `slots_all` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '栏位讯息' AFTER `feedback_time`;
ALTER TABLE `taskengine_record` ADD `statistics_info` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '统计资讯' AFTER `slots_all`;
-- +migrate Down
ALTER TABLE `taskengine_record` DROP `slots_all`;
ALTER TABLE `taskengine_record` DROP `statistics_info`;
