-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `backend_log`.`chat_record` ADD INDEX `APP_ID_INDEX` (`app_id`);
ALTER TABLE `backend_log`.`chat_record` ADD INDEX `USER_ID_INDEX` (`user_id`);
ALTER TABLE `backend_log`.`chat_record` ADD INDEX `SESSION_ID_INDEX` (`session_id`);
ALTER TABLE `backend_log`.`chat_record` ADD INDEX `TASKENGINE_SESSION_ID_INDEX` (`taskengine_session_id`);
ALTER TABLE `backend_log`.`chat_record` ADD INDEX `CREATE_TIME_INDEX` (`created_time`);





-- +migrate Down
ALTER TABLE `chat_record` DROP INDEX `APP_ID_INDEX`;
ALTER TABLE `chat_record` DROP INDEX `USER_ID_INDEX`;
ALTER TABLE `chat_record` DROP INDEX `SESSION_ID_INDEX`;
ALTER TABLE `chat_record` DROP INDEX `TASKENGINE_SESSION_ID_INDEX`;
ALTER TABLE `chat_record` DROP INDEX `CREATE_TIME_INDEX`;
