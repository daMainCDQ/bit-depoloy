-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `chat_record` ADD INDEX `unique_id_index` (`unique_id`);

-- +migrate Down
ALTER TABLE `chat_record` DROP INDEX `unique_id_index`;
