-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
START TRANSACTION;
DROP TABLE IF EXISTS `ent_x_cloud_scenario`;
CREATE TABLE `ent_x_cloud_scenario` (
  `id` BIGINT(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `scenario_id` BIGINT(11) NOT NULL,
  `scenario_uuid` varchar(32) NOT NULL COMMENT 'uuid',
  `scenario_name` varchar(256) default '' COMMENT '场景名称',
  `ref_count` BIGINT(11) NOT NULL DEFAULT 0 COMMENT '被下载次数',
  `ref_status` smallint  DEFAULT 0 COMMENT '是否已经下载，0：未下载，1：已下载',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COMMENT='场景表';

COMMIT;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `ent_x_cloud_scenario`;
