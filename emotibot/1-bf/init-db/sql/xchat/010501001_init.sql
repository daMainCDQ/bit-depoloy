
-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

START TRANSACTION;
DROP TABLE IF EXISTS `ent_x_scenario`;
CREATE TABLE `ent_x_scenario` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `app_id` varchar(64) NOT NULL COMMENT '机器人ID',
  `scenario_uuid` varchar(32) NOT NULL COMMENT 'uuid',
  `scenario_name` varchar(256) DEFAULT '' COMMENT '场景名称',
  `entry_node_id` bigint(11) NOT NULL COMMENT '入口结点',
  `trigger_type` varchar(16) DEFAULT '' COMMENT '枚举，qq,',
  `status` varchar(10) NOT NULL DEFAULT '' COMMENT '状态枚举，on:在线，off:离线',
  `priority` smallint(6) DEFAULT '0' COMMENT '优先级0-5，值越大优先级越高',
  `tag` varchar(256) DEFAULT '' COMMENT '场景的标签',
  `scenario_json` mediumtext COMMENT '场景json数据',
  `scenario_iterator_times` smallint(6) DEFAULT '3' COMMENT '场景节点最大自循环次数',
  `create_datetime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user` varchar(64) DEFAULT '' COMMENT '创建用户',
  `update_datetime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_user` varchar(64) DEFAULT '' COMMENT '更新用户',
  `need_update` smallint(6) DEFAULT '0' COMMENT '是否需要更新 1：需要，0：不需要',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=853 DEFAULT CHARSET=utf8mb4 COMMENT='场景表';


DROP TABLE IF EXISTS `ent_x_answer`;
CREATE TABLE `ent_x_answer` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `node_id` bigint(11) NOT NULL COMMENT '边结点',
  `answer` varchar(5000) DEFAULT '' COMMENT '回答文本',
  `create_datetime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_datetime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `scenario_id` bigint(11) NOT NULL COMMENT '场景id',
  `app_id` varchar(64) NOT NULL DEFAULT '' COMMENT '机器人ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69211 DEFAULT CHARSET=utf8mb4 COMMENT='回答表';

DROP TABLE IF EXISTS `ent_x_edge`;
CREATE TABLE `ent_x_edge` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `scenario_id` bigint(11) NOT NULL COMMENT '所属场景id',
  `edge_uuid` varchar(32) NOT NULL COMMENT 'uuid',
  `node_id` bigint(11) NOT NULL COMMENT '边起始结点',
  `node_to_id` bigint(11) DEFAULT NULL COMMENT '边结束结点',
  `scenario_to_id` bigint(11) DEFAULT NULL COMMENT '边进入其他场景ID',
  `entry_conditions` text COMMENT '入口条件,json字符串表示',
  `create_datetime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `app_id` varchar(64) NOT NULL DEFAULT '' COMMENT '机器人ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49964 DEFAULT CHARSET=utf8mb4 COMMENT='边表';

DROP TABLE IF EXISTS `ent_x_node`;
CREATE TABLE `ent_x_node` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `scenario_id` bigint(11) NOT NULL COMMENT '所属场景id',
  `node_uuid` varchar(32) NOT NULL COMMENT 'uuid',
  `node_type` char(16) NOT NULL DEFAULT 'NORMAL' COMMENT '结点类型，ENTRY，NORMAL两种',
  `node_name` varchar(256) DEFAULT '' COMMENT '结点名称',
  `entry_conditions` text COMMENT '入口条件,json字符串表示',
  `iterator_times` smallint(6) DEFAULT '0' COMMENT '节点最大自循环次数',
  `create_datetime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `app_id` varchar(64) NOT NULL DEFAULT '' COMMENT '机器人ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34456 DEFAULT CHARSET=utf8mb4 COMMENT='结点表';

COMMIT;


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `ent_x_scenario`;
DROP TABLE `ent_x_answer`;
DROP TABLE `ent_x_edge`;
DROP TABLE `ent_x_node`;



