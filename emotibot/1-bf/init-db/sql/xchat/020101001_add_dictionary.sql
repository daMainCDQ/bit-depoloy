-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
START TRANSACTION;
DROP TABLE IF EXISTS `ent_x_dictionary`;
CREATE TABLE `ent_x_dictionary` (
  `id` BIGINT(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `dictionary_uuid` varchar(64) NOT NULL COMMENT '词典uuid',
  `dictionary_name` varchar(256) NOT NULL COMMENT '词典名',
  `app_id` varchar(64) NOT NULL  COMMENT '机器人ID',
  `superior_dictionary` varchar(1000) DEFAULT '' COMMENT '上级词典id，多个词典id用半角逗号隔开',
  `junior_dictionary` varchar(1000) DEFAULT ''  COMMENT '下级词典id，多个词典id用半角逗号隔开',
  `status` smallint NOT NULL DEFAULT 1 COMMENT '是否逻辑删除，0：删除，1：未删除',
  `update_datetime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新词典的时间',
  `create_datetime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_user` varchar(64) COMMENT '更新用户',
  `create_user` varchar(64) COMMENT '创建用户',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COMMENT='词典表';

DROP TABLE IF EXISTS `ent_x_words`;
CREATE TABLE `ent_x_words` (
  `id` BIGINT(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `word_uuid` varchar(64) NOT NULL COMMENT '词条uuid',
  `dictionary_id` BIGINT(11) NOT NULL COMMENT '词条所属的词典id',
  `key_word` varchar(50) NOT NULL COMMENT '主词',
  `similar_words` varchar(3000) COMMENT '同义词，多个同义词用半角逗号隔开',
  `status` smallint NOT NULL DEFAULT 1 COMMENT '是否逻辑删除，0：删除，1：未删除',
  `update_datetime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新词典的时间',
  `create_datetime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_user` varchar(64) COMMENT '更新用户',
  `create_user` varchar(64) COMMENT '创建用户',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COMMENT='词条表';

COMMIT;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `ent_x_dictionary`;
DROP TABLE `ent_x_words`;


