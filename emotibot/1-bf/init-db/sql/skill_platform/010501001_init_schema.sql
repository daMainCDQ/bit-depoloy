-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

--
-- 表结构初始化— `t_config_resource`
--

DROP TABLE IF EXISTS `t_config_resource`;

CREATE TABLE `t_config_resource` (
                                     `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                     `keywords` varchar(255)  DEFAULT NULL COMMENT '被配置的属性',
                                     `code` int(20) DEFAULT NULL COMMENT '代号',
                                     `value` varchar(255)  DEFAULT NULL COMMENT '值',
                                     `desc` varchar(255)  DEFAULT NULL COMMENT '描述',
                                     PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;


--
-- 表结构初始化— `t_developer`
--

DROP TABLE IF EXISTS `t_developer`;

CREATE TABLE `t_developer` (
                               `id` bigint(20) NOT NULL AUTO_INCREMENT,
                               `developer_name` varchar(255) DEFAULT NULL COMMENT '开发者名称',
                               `flag` int(2) DEFAULT NULL,
                               `developer_type` int(2) DEFAULT '0' COMMENT '开发者类型1生活，2分类',
                               `user_id` varchar(255) DEFAULT NULL COMMENT '用户id',
                               PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;


--
-- 表结构初始化— `t_dictionary`
--

DROP TABLE IF EXISTS `t_dictionary`;

CREATE TABLE `t_dictionary` (
                                `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                `zh_name` varchar(255) DEFAULT NULL COMMENT '中文名',
                                `eng_name` varchar(255) DEFAULT NULL COMMENT '英文名',
                                `flag` int(2) DEFAULT NULL,
                                `user_id` varchar(255) DEFAULT NULL COMMENT '用户id',
                                `cid` mediumtext COMMENT '词典解析器id',
                                `correct_flag` int(2) DEFAULT '0',
                                PRIMARY KEY (`id`),
                                KEY `idx_eng_name_user_id_flag` (`eng_name`,`user_id`,`flag`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;


--
-- 表结构初始化— `t_dictionary_status`
--

DROP TABLE IF EXISTS `t_dictionary_status`;

CREATE TABLE `t_dictionary_status` (
                                       `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                       `dictionary_id` int(10) unsigned NOT NULL COMMENT '词典表主键',
                                       `skill_id` int(10) unsigned NOT NULL COMMENT '技能表主键',
                                       `switch_status` tinyint(1) DEFAULT '0' COMMENT '词典开关，1为开，0为关',
                                       PRIMARY KEY (`id`),
                                       KEY `idx_skill_id` (`skill_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COMMENT='词典状态表';


--
-- 表结构初始化— `t_entity_parser`
--

DROP TABLE IF EXISTS `t_entity_parser`;

CREATE TABLE `t_entity_parser` (
                                   `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                   `category` varchar(255) DEFAULT NULL COMMENT '1通用解析器 2字典解析器',
                                   `name` varchar(255) DEFAULT NULL COMMENT '名称',
                                   `type` int(8) DEFAULT NULL COMMENT '类型',
                                   `user_id` varchar(255) DEFAULT NULL COMMENT '用户id',
                                   `parser_url` varchar(255) DEFAULT NULL COMMENT '解析器api',
                                   `flag` int(2) DEFAULT NULL,
                                   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


--
-- 表结构初始化— `t_entry`
--

DROP TABLE IF EXISTS `t_entry`;

CREATE TABLE `t_entry` (
                           `id` bigint(20) NOT NULL AUTO_INCREMENT,
                           `dictionary_id` bigint(20) DEFAULT NULL COMMENT '字典id',
                           `entry_name` varchar(255) DEFAULT NULL COMMENT '词条名',
                           `flag` int(2) DEFAULT NULL,
                           `wid` bigint(20) DEFAULT NULL COMMENT '解析器词条id',
                           `skill_id` bigint(20) DEFAULT NULL,
                           `fork_id` bigint(20) DEFAULT NULL,
                           `user_id` varchar(255) DEFAULT NULL,
                           `correct_flag` int(2) DEFAULT '0',
                           PRIMARY KEY (`id`),
                           KEY `index_entry` (`dictionary_id`),
                           KEY `index_user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;


--
-- 表结构初始化— `t_intent`
--

DROP TABLE IF EXISTS `t_intent`;

CREATE TABLE `t_intent` (
                            `id` bigint(20) NOT NULL AUTO_INCREMENT,
                            `skill_id` bigint(20) DEFAULT NULL COMMENT '技能表id',
                            `zh_name` varchar(255) DEFAULT NULL COMMENT '意图中文名',
                            `eng_name` varchar(255) DEFAULT NULL COMMENT '意图英文名',
                            `task_id` varchar(255) DEFAULT NULL COMMENT '多轮任务id',
                            `task_json` longtext COMMENT '场景文件json',
                            `flag` int(2) DEFAULT '1',
                            `status` int(5) DEFAULT '0' COMMENT '状态0新建 1已完成 2编辑中\n',
                            `confirm_flag` int(2) DEFAULT '0' COMMENT '意图确认',
                            `default_flag` int(2) DEFAULT '0' COMMENT '是否为默认意图 0不是 ，1是',
                            `fork_id` bigint(20) DEFAULT NULL,
                            PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;


--
-- 表结构初始化— `t_intent_response`
--

DROP TABLE IF EXISTS `t_intent_response`;

CREATE TABLE `t_intent_response` (
                                     `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                     `intent_id` bigint(20) DEFAULT NULL COMMENT '意图id',
                                     `reply` text COMMENT '回复内容',
                                     `reply_type` int(2) DEFAULT NULL COMMENT '回复类型(0文字回复 1内部应用 2 外部应用)',
                                     `reply_index` int(2) DEFAULT '0' COMMENT '回复顺序',
                                     PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;


--
-- 表结构初始化— `t_intent_slot`
--

DROP TABLE IF EXISTS `t_intent_slot`;

CREATE TABLE `t_intent_slot` (
                                 `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                 `intent_id` bigint(20) DEFAULT NULL COMMENT '意图表id',
                                 `slot_name` varchar(255) DEFAULT NULL COMMENT '槽位名称',
                                 `slot_value` varchar(255) DEFAULT NULL COMMENT '槽值',
                                 `analyzer` varchar(255) DEFAULT NULL COMMENT '解析器',
                                 `flag` int(2) DEFAULT NULL,
                                 `add_words` varchar(255) DEFAULT NULL COMMENT '追问话术',
                                 `confirm_words` varchar(255) DEFAULT NULL COMMENT '确认话术',
                                 `add_flag` tinyint(1) DEFAULT NULL COMMENT '是否追问',
                                 `confirm_flag` int(2) DEFAULT NULL COMMENT '是否确认',
                                 `slot_sort` int(11) DEFAULT NULL COMMENT '槽位排序',
                                 `eng_name` varchar(255) DEFAULT NULL COMMENT '英文名',
                                 `zh_name` varchar(255) DEFAULT NULL COMMENT '中文名称',
                                 `slot_sign` varchar(255) DEFAULT NULL COMMENT '槽位标示',
                                 `material_slot_id` bigint(20) DEFAULT NULL COMMENT '语料槽位id',
                                 PRIMARY KEY (`id`),
                                 KEY `idx_intent_id_slot_sign` (`intent_id`,`slot_sign`),
                                 KEY `idx_material_slot_id` (`material_slot_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;


--
-- 表结构初始化— `t_material`
--

DROP TABLE IF EXISTS `t_material`;

CREATE TABLE `t_material` (
                              `id` bigint(20) NOT NULL AUTO_INCREMENT,
                              `intent_id` bigint(20) DEFAULT NULL COMMENT '意图id',
                              `user_id` varchar(255) DEFAULT NULL COMMENT '用户id',
                              `content` varchar(255) DEFAULT NULL COMMENT '语料内容',
                              `content_flag` int(2) DEFAULT NULL COMMENT '语料标记（0:反例 1:正例5',
                              `flag` int(2) DEFAULT NULL,
                              `sentence_id` varchar(255) DEFAULT NULL COMMENT 'ES语料id',
                              PRIMARY KEY (`id`),
                              KEY `idx_intent_id` (`intent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;


--
-- 表结构初始化— `t_material_parse_status`
--

DROP TABLE IF EXISTS `t_material_parse_status`;

CREATE TABLE `t_material_parse_status` (
                                           `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                           `intent_id` int(10) unsigned NOT NULL COMMENT '意图表主键',
                                           `parse_type` tinyint(4) DEFAULT NULL COMMENT '解析类型,全局解析(1),批量导入(2)',
                                           `parse_completed` char(1) DEFAULT NULL COMMENT '解析完成状态',
                                           `complete_percent` tinyint(4) DEFAULT NULL COMMENT '解析完成百分比',
                                           PRIMARY KEY (`id`),
                                           KEY `idx_intent_id_parse_type` (`intent_id`,`parse_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='语料解析状态表';


--
-- 表结构初始化— `t_material_slot`
--

DROP TABLE IF EXISTS `t_material_slot`;

CREATE TABLE `t_material_slot` (
                                   `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                   `intent_id` bigint(20) DEFAULT NULL COMMENT '意图id',
                                   `material_id` bigint(20) DEFAULT NULL COMMENT '语料id',
                                   `slot_name` varchar(255) DEFAULT NULL COMMENT '槽名',
                                   `slot_value` varchar(255) DEFAULT NULL COMMENT '槽值',
                                   `parser_id` bigint(20) DEFAULT NULL COMMENT '解析器id',
                                   `flag` int(2) DEFAULT NULL,
                                   `parser_name` varchar(255) DEFAULT NULL COMMENT '解析器名称',
                                   `zh_name` varchar(255) DEFAULT NULL COMMENT '中文名',
                                   `slot_sign` varchar(255) DEFAULT NULL COMMENT '槽位标示',
                                   PRIMARY KEY (`id`),
                                   KEY `idx_intent_id_slot_sign` (`intent_id`,`slot_sign`),
                                   KEY `idx_material_id` (`material_id`),
                                   KEY `idx_intent_id_parser_name` (`intent_id`,`parser_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;


--
-- 表结构初始化— `t_material_test`
--

DROP TABLE IF EXISTS `t_material_test`;

CREATE TABLE `t_material_test` (
                                   `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                   `intent_id` bigint(20) DEFAULT NULL COMMENT '意图id',
                                   `material` varchar(255) DEFAULT NULL COMMENT '语料',
                                   `result` int(2) DEFAULT NULL COMMENT '语料测试结果(0表示失败，1表示成功)',
                                   `skill_id` bigint(20) DEFAULT NULL COMMENT '技能Id',
                                   `msg` varchar(100)  DEFAULT NULL,
                                   `latency` bigint(20) DEFAULT NULL,
                                   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


--
-- 表结构初始化— `t_model_parser`
--

DROP TABLE IF EXISTS `t_model_parser`;

CREATE TABLE `t_model_parser` (
                                  `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                  `zh_name` varchar(255) DEFAULT NULL COMMENT '中文名',
                                  `eng_name` varchar(255) DEFAULT NULL COMMENT '英文名',
                                  `model` varchar(255) DEFAULT NULL COMMENT '使用训练模型',
                                  `first_slot_sign` varchar(255) DEFAULT NULL COMMENT '槽位一标识',
                                  `first_slot_related` bigint(20) DEFAULT NULL COMMENT '槽位一关联词典',
                                  `first_slot_name` varchar(255) DEFAULT NULL COMMENT '槽位一中文名',
                                  `second_slot_sign` varchar(255) DEFAULT NULL COMMENT '槽位二标识',
                                  `second_slot_related` bigint(20) DEFAULT NULL COMMENT '槽位二关联词典',
                                  `second_slot_name` varchar(255) DEFAULT NULL COMMENT '槽位二中文名',
                                  `third_slot_sign` varchar(255) DEFAULT NULL COMMENT '槽位三标识',
                                  `third_slot_related` bigint(20) DEFAULT NULL COMMENT '槽位三关联词典',
                                  `third_slot_name` varchar(255) DEFAULT NULL COMMENT '槽位三中文名',
                                  `user_id` varchar(255) DEFAULT NULL COMMENT '用户id',
                                  `user_parser_id` varchar(255) DEFAULT NULL COMMENT '用户解析器id',
                                  `tags` varchar(255) DEFAULT NULL COMMENT '拼接的槽位英文名',
                                  `parser_id` varchar(255) DEFAULT NULL COMMENT '每个解析器训练结果',
                                  `en_name` varchar(255) DEFAULT NULL,
                                  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;


--
-- 表结构初始化— `t_model_parser_material`
--

DROP TABLE IF EXISTS `t_model_parser_material`;

CREATE TABLE `t_model_parser_material` (
                                           `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                           `model_id` bigint(20) DEFAULT NULL COMMENT '模型解析器id',
                                           `content` varchar(255) DEFAULT NULL COMMENT '训练语料内容',
                                           PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;


--
-- 表结构初始化— `t_model_parser_status`
--

DROP TABLE IF EXISTS `t_model_parser_status`;

CREATE TABLE `t_model_parser_status` (
                                         `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                         `model_id` bigint(20) DEFAULT NULL COMMENT '关联模型解析id',
                                         `status` int(2) DEFAULT NULL COMMENT '0关 1开',
                                         `skill_id` bigint(20) DEFAULT NULL COMMENT '技能id',
                                         PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;


--
-- 表结构初始化— `t_monitoring_report`
--

DROP TABLE IF EXISTS `t_monitoring_report`;

CREATE TABLE `t_monitoring_report` (
                                       `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                       `skill_id` bigint(20) DEFAULT NULL COMMENT '技能表id',
                                       `intent_id` bigint(20) DEFAULT NULL COMMENT '意图id',
                                       `url` varchar(255) DEFAULT NULL COMMENT '外部服务地址',
                                       `body` longtext COMMENT '外部服务请求体',
                                       `url_index` int(2) DEFAULT NULL COMMENT 'url在json模版中的位置',
                                       `result` int(2) DEFAULT NULL COMMENT '测试结果(0表示失败，1表示成功)',
                                       `reply` longtext COMMENT '外部服务回复',
                                       `latency` bigint(20) DEFAULT NULL COMMENT '耗时',
                                       `msg` varchar(255) DEFAULT NULL COMMENT '报错信息',
                                       `fail_count` int(2) DEFAULT NULL COMMENT '失败的次数',
                                       `fail_reason` int(2) DEFAULT NULL COMMENT '失败的原因 1 正常 2 外部地址响应时间超时 3 外部服务未回复 4 技能回复内容变更',
                                       PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;


--
-- 表结构初始化— `t_parser`
--

DROP TABLE IF EXISTS `t_parser`;

CREATE TABLE `t_parser` (
                            `id` bigint(20) NOT NULL AUTO_INCREMENT,
                            `zh_name` varchar(255)  DEFAULT '' COMMENT '技能id',
                            `eng_name` varchar(255)  DEFAULT '' COMMENT '解析器id',
                            `flag` int(2) DEFAULT '1',
                            `type` int(2) DEFAULT NULL COMMENT '1通用,2外部',
                            `user_id` varchar(255)  DEFAULT '' COMMENT '用户id',
                            `url` varchar(255)  DEFAULT '' COMMENT '解析器url',
                            `method` varchar(255) DEFAULT NULL,
                            PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;


--
-- 表结构初始化— `t_parser_status`
--

DROP TABLE IF EXISTS `t_parser_status`;

CREATE TABLE `t_parser_status` (
                                   `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                   `skill_id` bigint(20) DEFAULT NULL COMMENT '技能id',
                                   `parser_id` bigint(20) DEFAULT NULL COMMENT '解析器id',
                                   `flag` int(2) DEFAULT NULL,
                                   `status` int(2) DEFAULT '0' COMMENT '当前状态',
                                   PRIMARY KEY (`id`),
                                   KEY `idx_skill_id` (`skill_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;


--
-- 表结构初始化— `t_skill`
--

DROP TABLE IF EXISTS `t_skill`;

CREATE TABLE `t_skill` (
                           `id` bigint(20) NOT NULL AUTO_INCREMENT,
                           `type` int(2) DEFAULT NULL COMMENT '技能类型（1自定义）',
                           `scenario` int(2) DEFAULT NULL COMMENT '技能场景1手机（',
                           `name` varchar(255) DEFAULT NULL COMMENT '技能名称',
                           `welcome_verb` varchar(255) DEFAULT NULL COMMENT '欢迎语\n',
                           `farewell_verb` varchar(255) DEFAULT NULL COMMENT '结束语',
                           `flag` int(2) DEFAULT NULL,
                           `exec_name` varchar(255) DEFAULT NULL COMMENT '技能调用名',
                           `run_flag` int(4) DEFAULT NULL COMMENT '1 2345',
                           `user_id` varchar(255) DEFAULT '1',
                           `skill_desc` varchar(500) DEFAULT '' COMMENT '技能描述',
                           `developer_id` varchar(255) DEFAULT '' COMMENT '开发者id',
                           `newfunction_desc` varchar(500) DEFAULT '' COMMENT '新功能介绍',
                           `fork_id` bigint(20) DEFAULT '-1',
                           `run_time` date DEFAULT NULL COMMENT '上线时间',
                           `apply_run_time` datetime DEFAULT NULL COMMENT '申请上线时间',
                           `skill_id` bigint(20) DEFAULT NULL,
                           `category` int(2) DEFAULT NULL COMMENT '技能分类  1,社交通讯 2,音乐电台 3,视频播放 4,实用工具 5,美食外卖 6,网上购物 7,新闻阅读 8,教育学习 9,交通导航 10,旅游出行 11,医疗健康 12,生活娱乐 13,信息查询 14,其他',
                           `test_mode` tinyint(1) DEFAULT NULL,
                           `test_word` varchar(45) DEFAULT NULL,
                           `release_notes` varchar(255) DEFAULT NULL COMMENT '更改模块',
                           `health_status` int(4) DEFAULT NULL COMMENT '  null  -1  0  1 ',
                           `company_desc` varchar(255) DEFAULT NULL,
                           `original_query_flag` int(4) DEFAULT NULL COMMENT 'query: null | 0   query, 1  query ',
                           `corpus_generalization_level` varchar(20) DEFAULT NULL COMMENT 'æ„å›¾åŒ¹é…ç­‰çº§ å¼º strong ä¸­ medium å¼± weak',
                           PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;


--
-- 表结构初始化— `t_skill_approval`
--

DROP TABLE IF EXISTS `t_skill_approval`;

CREATE TABLE `t_skill_approval` (
                                    `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                    `skill_id` bigint(20) DEFAULT NULL COMMENT '技能id',
                                    `comment` varchar(500) DEFAULT '' COMMENT '备注',
                                    `flag` int(2) DEFAULT NULL,
                                    `operate_time` datetime DEFAULT NULL COMMENT '操作时间',
                                    `operate_type` int(4) DEFAULT NULL COMMENT '操作类型 1提交 2拒绝  3通过',
                                    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;


--
-- 表结构初始化— `t_skill_conflict_test`
--

DROP TABLE IF EXISTS `t_skill_conflict_test`;

CREATE TABLE `t_skill_conflict_test` (
                                         `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                         `skill_id` bigint(20) DEFAULT NULL COMMENT '技能ID',
                                         `type` varchar(255) DEFAULT NULL COMMENT '类型(0 单轮 1 多轮 2 其他)',
                                         `description` varchar(255) DEFAULT NULL COMMENT '说明',
                                         `corpus` varchar(255) DEFAULT NULL COMMENT '语料',
                                         `reply` varchar(255) DEFAULT NULL COMMENT '回复效果',
                                         PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;


--
-- 表结构初始化— `t_skill_conflict_test_eval`
--

DROP TABLE IF EXISTS `t_skill_conflict_test_eval`;

CREATE TABLE `t_skill_conflict_test_eval` (
                                              `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                              `skill_id` bigint(20) DEFAULT NULL COMMENT 'ID',
                                              `type` int(2) DEFAULT NULL COMMENT '(0  1  2  3)',
                                              `description` varchar(255) DEFAULT NULL,
                                              `corpus` varchar(255) DEFAULT NULL,
                                              `reply` varchar(255) DEFAULT NULL,
                                              PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;


--
-- 表结构初始化— `t_skill_function`
--

DROP TABLE IF EXISTS `t_skill_function`;

CREATE TABLE `t_skill_function` (
                                    `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                    `skill_id` bigint(20) DEFAULT NULL COMMENT '技能ID',
                                    `skill_function` varchar(255) DEFAULT NULL COMMENT '技能功能',
                                    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;


--
-- 表结构初始化— `t_skill_function_example`
--

DROP TABLE IF EXISTS `t_skill_function_example`;

CREATE TABLE `t_skill_function_example` (
                                            `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                            `skill_function_id` bigint(20) DEFAULT NULL COMMENT '技能功能ID',
                                            `skill_function_example` varchar(255) DEFAULT NULL COMMENT '技能功能示例问法',
                                            PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;


--
-- 表结构初始化— `t_skill_json`
--

DROP TABLE IF EXISTS `t_skill_json`;

CREATE TABLE `t_skill_json` (
                                `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                `skill_id` bigint(20) DEFAULT NULL,
                                `skill_json` longtext,
                                `flag` int(2) DEFAULT NULL,
                                `intent_id` bigint(20) DEFAULT NULL,
                                `default_flag` int(2) DEFAULT '0' COMMENT '默认意图标记01为默认',
                                PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;


--
-- 表结构初始化— `t_synonyms`
--

DROP TABLE IF EXISTS `t_synonyms`;

CREATE TABLE `t_synonyms` (
                              `id` bigint(20) NOT NULL AUTO_INCREMENT,
                              `entry_id` bigint(20) DEFAULT NULL COMMENT '词条id',
                              `name` varchar(255) DEFAULT NULL COMMENT '近义词',
                              `flag` int(2) DEFAULT NULL,
                              `wid` bigint(20) DEFAULT NULL COMMENT '解析器词条id',
                              `user_id` varchar(255) DEFAULT NULL,
                              `correct_flag` int(2) DEFAULT '0',
                              PRIMARY KEY (`id`),
                              KEY `index_synonyms` (`entry_id`),
                              KEY `index_user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;


--
-- 表结构初始化— `t_user_parser`
--

DROP TABLE IF EXISTS `t_user_parser`;

CREATE TABLE `t_user_parser` (
                                 `user_id` varchar(255) DEFAULT NULL COMMENT '用户id',
                                 `user_parser_id` varchar(255) DEFAULT NULL COMMENT '用户解析器id',
                                 `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                 PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;


--
-- 表结构初始化— `t_words`
--

DROP TABLE IF EXISTS `t_words`;

CREATE TABLE `t_words` (
                           `id` bigint(20) NOT NULL AUTO_INCREMENT,
                           `content` varchar(255) DEFAULT NULL COMMENT '话术内容',
                           `type` int(3) DEFAULT NULL COMMENT '话术类型0追问， 2确认',
                           `flag` int(2) DEFAULT NULL,
                           `slot_id` bigint(20) DEFAULT NULL COMMENT '槽位id',
                           `intent_id` bigint(20) DEFAULT NULL COMMENT '意图id',
                           PRIMARY KEY (`id`),
                           KEY `idx_slot_id` (`slot_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;

--
-- 表结构初始化— `tj_logs`
--

DROP TABLE IF EXISTS `tj_logs`;

CREATE TABLE `tj_logs` (
                           `id` bigint(20) NOT NULL AUTO_INCREMENT,
                           `createdate` datetime DEFAULT NULL,
                           `creater` varchar(255) DEFAULT NULL,
                           `mark` varchar(255) DEFAULT NULL,
                           `updatedate` datetime DEFAULT NULL,
                           `updater` varchar(255) DEFAULT NULL,
                           `ip` varchar(255) DEFAULT NULL COMMENT 'ip地址',
                           `params` text,
                           `v_url` varchar(5000) DEFAULT NULL COMMENT '访问路径',
                           PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

DROP TABLE IF EXISTS `t_config_resource`;
DROP TABLE IF EXISTS `t_developer`;
DROP TABLE IF EXISTS `t_dictionary`;
DROP TABLE IF EXISTS `t_dictionary_status`;
DROP TABLE IF EXISTS `t_entity_parser`;
DROP TABLE IF EXISTS `t_entry`;
DROP TABLE IF EXISTS `t_intent`;
DROP TABLE IF EXISTS `t_intent_response`;
DROP TABLE IF EXISTS `t_intent_slot`;
DROP TABLE IF EXISTS `t_material`;
DROP TABLE IF EXISTS `t_material_parse_status`;
DROP TABLE IF EXISTS `t_material_slot`;
DROP TABLE IF EXISTS `t_material_test`;
DROP TABLE IF EXISTS `t_model_parser`;
DROP TABLE IF EXISTS `t_model_parser_material`;
DROP TABLE IF EXISTS `t_model_parser_status`;
DROP TABLE IF EXISTS `t_monitoring_report`;
DROP TABLE IF EXISTS `t_parser`;
DROP TABLE IF EXISTS `t_parser_status`;
DROP TABLE IF EXISTS `t_skill`;
DROP TABLE IF EXISTS `t_skill_approval`;
DROP TABLE IF EXISTS `t_skill_conflict_test`;
DROP TABLE IF EXISTS `t_skill_conflict_test_eval`;
DROP TABLE IF EXISTS `t_skill_function`;
DROP TABLE IF EXISTS `t_skill_function_example`;
DROP TABLE IF EXISTS `t_skill_json`;
DROP TABLE IF EXISTS `t_synonyms`;
DROP TABLE IF EXISTS `t_user_parser`;
DROP TABLE IF EXISTS `t_words`;
DROP TABLE IF EXISTS `tj_logs`;
