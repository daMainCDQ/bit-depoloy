-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
alter table `t_skill` add column `status` tinyint(1) DEFAULT '1' COMMENT '开关状态1开 0关';

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
