-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

drop table if EXISTS `t_response_dimension`;
CREATE TABLE `t_response_dimension` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `response_id` bigint(20) NOT NULL COMMENT 't_intent_response 的id',
  `name` varchar(255) DEFAULT NULL COMMENT '维度name',
  PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

drop table if EXISTS `t_response_dimension_value`;
CREATE TABLE `t_response_dimension_value` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `response_dimension_id` bigint(20) NOT NULL COMMENT 't_response_dimension 的id',
  `name` varchar(255) DEFAULT NULL COMMENT '维度值name',
  PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
