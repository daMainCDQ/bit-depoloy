-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
USE `skill_platform`;

ALTER TABLE `skill_platform`.`t_response_dimension`
ADD COLUMN `type` TINYINT(4) NULL COMMENT '0代表维度1代表前置意图' AFTER `name`,
ADD COLUMN `compare_rule` VARCHAR(20) NULL COMMENT '计算规则 eq，neq 或 null' AFTER `type`;

UPDATE `skill_platform`.`t_response_dimension` SET `type` = 0;
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
