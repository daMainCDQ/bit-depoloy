-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
alter table t_intent_slot add column is_extend tinyint comment '槽位继承，1继承，0不继承' default 1;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
