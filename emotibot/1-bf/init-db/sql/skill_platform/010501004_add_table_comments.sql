-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE `t_config_resource` COMMENT '系统静态配置资源';
ALTER TABLE `t_developer` COMMENT '技能开发者信息表';
ALTER TABLE `t_dictionary` COMMENT '词典数据表';
ALTER TABLE `t_dictionary_status` COMMENT '词典状态表';
ALTER TABLE `t_entity_parser` COMMENT '意图解析器表';
ALTER TABLE `t_entry` COMMENT '字典词条表';
ALTER TABLE `t_intent` COMMENT '意图表';
ALTER TABLE `t_intent_response` COMMENT '问答小技能回复信息表';
ALTER TABLE `t_intent_slot` COMMENT '意图槽位表';
ALTER TABLE `t_intent_slot_relation` COMMENT '意图槽位关联表';
ALTER TABLE `t_material` COMMENT '意图语料表';
ALTER TABLE `t_material_parse_status` COMMENT '语料解析状态表';
ALTER TABLE `t_material_slot` COMMENT '语料槽位表';
ALTER TABLE `t_material_test` COMMENT '正例测试语料表';
ALTER TABLE `t_model_parser` COMMENT '模型解析器表';
ALTER TABLE `t_model_parser_material` COMMENT '模型解析器语料表';
ALTER TABLE `t_model_parser_status` COMMENT '模型解析器状态表';
ALTER TABLE `t_monitoring_report` COMMENT '监控报告表';
ALTER TABLE `t_parser` COMMENT '解析器信息表';
ALTER TABLE `t_parser_status` COMMENT '解析器状态表';
ALTER TABLE `t_resource` COMMENT '资源表';
ALTER TABLE `t_resource_answer` COMMENT '资源回复表';
ALTER TABLE `t_resource_slot` COMMENT '资源槽位表';
ALTER TABLE `t_resource_word` COMMENT '资源词条表';
ALTER TABLE `t_skill` COMMENT '技能信息表';
ALTER TABLE `t_skill_approval` COMMENT '技能审核信息表';
ALTER TABLE `t_skill_conflict_test` COMMENT '技能冲突测试表';
ALTER TABLE `t_skill_conflict_test_eval` COMMENT '技能评测语料表';
ALTER TABLE `t_skill_function` COMMENT '技能使用介绍表';
ALTER TABLE `t_skill_function_example` COMMENT '技能示例问法表';
ALTER TABLE `t_skill_json` COMMENT '技能场景json表';
ALTER TABLE `t_synonyms` COMMENT '同义词表';
ALTER TABLE `t_user_parser` COMMENT '用户解析器表';
ALTER TABLE `t_words` COMMENT '意图追问和确认话术表';
ALTER TABLE `tj_logs` COMMENT '接口调用日志信息表';


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
