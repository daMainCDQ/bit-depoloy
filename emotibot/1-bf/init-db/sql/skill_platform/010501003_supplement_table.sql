-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

--
-- 表结构初始化— `t_intent_slot_relation`
--

DROP TABLE IF EXISTS `t_intent_slot_relation`;

CREATE TABLE `t_intent_slot_relation` (
                                          `id` int unsigned auto_increment primary key,
                                          `intent_id` int unsigned NOT NULL COMMENT '意图表主键',
                                          `related_slots` varchar(1024) COMMENT '有关联的意图槽位',
                                          `relation_type` TINYINT(1) COMMENT '关联类型，互斥(1)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='意图槽位关联表';



--
-- 表结构初始化— `t_resource`
--

DROP TABLE IF EXISTS `t_resource`;

CREATE TABLE `t_resource` (
                              `id` int unsigned auto_increment primary key,
                              `skill_id` int unsigned COMMENT '技能ID',
                              `resource_name` varchar(50) COMMENT '资源名称',
                              `upload_date` date COMMENT '资源上次日期',
                              `resource_format` varchar(6) COMMENT '资源格式',
                              `resource_size` varchar(10) COMMENT '资源大小',
                              `resource_url` varchar(100) COMMENT '资源URL'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='资源表';


--
-- 表结构初始化— `t_resource_slot`
--

DROP TABLE IF EXISTS `t_resource_slot`;

CREATE TABLE `t_resource_slot` (
                                   `id` int unsigned auto_increment primary key,
                                   `resource_id` int unsigned COMMENT '资源ID',
                                   `slot_value` varchar(20) COMMENT '槽位值'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='资源槽位表';



--
-- 表结构初始化— `t_resource_word`
--

DROP TABLE IF EXISTS `t_resource_word`;

CREATE TABLE `t_resource_word` (
                                   `id` int unsigned auto_increment primary key,
                                   `resource_id` int unsigned COMMENT '资源ID',
                                   `resource_slot_id` int unsigned COMMENT '资源槽位ID',
                                   `words` varchar(20) COMMENT '词条'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='资源词条表';



--
-- 表结构初始化— `t_resource_answer`
--

DROP TABLE IF EXISTS `t_resource_answer`;

CREATE TABLE `t_resource_answer` (
                                     `id` int unsigned auto_increment primary key,
                                     `resource_id` int unsigned COMMENT '资源ID',
                                     `resource_words_id` int unsigned COMMENT '资源词条ID',
                                     `answer` varchar(2000) COMMENT '回复'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='资源回复表';


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE IF EXISTS `t_intent_slot_relation`;
DROP TABLE IF EXISTS `t_resource`;
DROP TABLE IF EXISTS `t_resource_slot`;
DROP TABLE IF EXISTS `t_resource_word`;
DROP TABLE IF EXISTS `t_resource_answer`;
