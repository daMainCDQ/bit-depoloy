-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
drop table if EXISTS `t_skill_parser`;
CREATE TABLE `t_skill_parser` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `skill_id` bigint(20) DEFAULT -1 NOT NULL COMMENT '技能id',
  `intent_id` bigint(20) DEFAULT -1 NOT NULL COMMENT '意图id',
  `parser_id` varchar(36) DEFAULT "" NOT NULL COMMENT 'parserLib解析器id',
  `slot_id` int(2) DEFAULT -1 NOT NULL COMMENT '解析器槽位id',
	`slot_name` varchar(36) DEFAULT "" NOT NULL COMMENT '解析器槽位英文name',
  `status` int(2) DEFAULT 1 NOT NULL COMMENT '当前状态:1-enable,0-disable',
	`parser_desc` varchar(255) DEFAULT "" NOT NULL COMMENT '解析器描述',
	`slot_desc` varchar(50) DEFAULT "" NOT NULL COMMENT '槽位描述',
	`type` int(2) DEFAULT 1 NOT NULL COMMENT '解析器类型',
	`parser_name` varchar(50) DEFAULT "" NOT NULL COMMENT '解析器中文名称',
	`slot_zh_name` varchar(50) DEFAULT "" NOT NULL COMMENT '解析器槽位中文名称',
	`create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx_intent_slot` (`intent_id`, `slot_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

alter table t_material_slot modify column parser_id varchar(50);

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
