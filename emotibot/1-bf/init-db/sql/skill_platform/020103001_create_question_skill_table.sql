-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
alter table t_skill_json add column task_id varchar(50);

alter table t_skill add column `question_round` varchar(20) DEFAULT 'default' COMMENT '答题技能的轮次,值为 default 时为用户上传的题目数';

alter table t_words add column `skill_id` bigint(20) DEFAULT null COMMENT '技能id，用于关联技能和话术配置';

drop table if EXISTS `t_skill_question`;
CREATE TABLE `t_skill_question` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `skill_id` bigint(20) DEFAULT NULL COMMENT '技能id',
  `question` varchar(255) DEFAULT NULL COMMENT '问题',
  `type` int(1) DEFAULT NULL COMMENT '问题类型(1-问答，2-选择题)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

drop table if EXISTS `t_skill_question_info`;
CREATE TABLE `t_skill_question_info` (
  `id`  bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `question_id` bigint(20) DEFAULT NULL COMMENT '关联的问题id',
  `content` varchar(255) DEFAULT NULL COMMENT '提示或答案',
  `content_type` int(1) NOT NULL COMMENT '当值为 1 时 content 为答案，具体取值 1:答案 2:相似答案 3:提示',
  `content_index` int(1) DEFAULT NULL COMMENT '答案顺序，正确答案为0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
