import pymysql
import os
import json

mysql_host = os.environ.get('INIT_MYSQL_HOST')
mysql_port = int(os.environ.get('INIT_MYSQL_PORT'))
mysql_user = os.environ.get('INIT_MYSQL_USER')
mysql_password = os.environ.get('INIT_MYSQL_PASSWORD')
mysql_db = 'skill_platform'

source_mysql_host = os.environ.get('INIT_MYSQL_HOST')
source_mysql_port = int(os.environ.get('INIT_MYSQL_PORT'))
source_mysql_user = os.environ.get('INIT_MYSQL_USER')
source_mysql_password = os.environ.get('INIT_MYSQL_PASSWORD')
source_mysql_db = 'skill_platform'


def handler_skill_json():
	skill_json_query()
	return True


def skill_json_query():
	query_skill_json_sql = """select skill_id,intent_id,skill_json from t_skill_json"""
	query_intent_slot_sql = """select analyzer from t_intent_slot where intent_id = '%s'"""
	old_skill_json = mysql_query_source_list(query_skill_json_sql)
	for i in old_skill_json:
		intent_id = i[1]
		skill_json = i[2]

		intent_slot = mysql_query_source_list(query_intent_slot_sql % (intent_id))
		slots = []
		for slot in intent_slot:
			slots.append(slot[0])

		value_json = json.loads(skill_json)
		nodes = value_json['taskScenario']['nodes']
		for node in nodes:
			if node['description'] == '动作节点':
				action_group_list = node['content']['action_group_list']
				flag = False
				reply_index = -1
				for index in range(len(action_group_list)):
					if flag:
						flag = False
						continue
					action_group = action_group_list[index]
					reply = ""
					reply_index = reply_index + 1
					reply_type = ""
					action_list = action_group['actionList']
					if action_list[0]['type'] == 'webhook':
						reply_type = 99
						method = action_list[0]['method']
						body = str(action_list[0]['body']).replace("\"", "\\\\\"").replace("\n","")
						url = action_list[0]['url']
						msg = action_group_list[index + 1]['actionList'][0]['msg']
						msg_obj = json.loads(msg)
						speak_text_str = msg_obj['speak']['text']
						show_type = msg_obj['header']['showType']
						if show_type == 'card':
							show_type = 'commonCard'
						reply = "{\"subType\":\"${respType}\",\"url\":\"${url}\",\"text\":\"${text}\",\"method\":\"${method}\",\"body\":\"${body}\"}".replace("${body}", change_json_key(body, slots)).replace("${url}", change_json_key(url, slots)).replace("${method}", method).replace("${text}", change_json_key(speak_text_str, slots)).replace("${respType}", show_type)
						flag = True
					else:
						msg = action_list[0]['msg']
						msg_obj = json.loads(msg)
						resp_type_str = msg_obj['respType']
						if resp_type_str == 'textResponse':
							reply_type = 0
							text = msg_obj['payload']
							reply = "{\"text\":\"${text}\"}".replace("${text}", change_json_key(text, slots))
						elif resp_type_str == 'commonResponse':
							sub_type = msg_obj['payload'][0]['subType']
							if sub_type == 'music':
								reply_type = 11
								payload = msg_obj['payload'][0]
								data = dict(payload['data'][0])
								title = ""
								if ('title' in data): title = data['title']
								source_name = ""
								if ('sourceName' in data): source_name = data['sourceName']
								source_icon = ""
								if ('sourceIcon' in data): source_icon = data['sourceIcon']
								image_url = ""
								if ('imageUrl' in data): image_url = data['imageUrl']
								audio_url = ""
								if ('audioUrl' in data): audio_url = data['audioUrl']
								author = ""
								if ('author' in data): author = data['author']
								album = ""
								if ('album' in data): album = data['album']
								image_name = ""
								if ('imageName' in data): image_name = data['imageName']
								card_icon_name = ""
								if ('cardIconName' in data): card_icon_name = data['cardIconName']
								audio_name = ""
								if ('audioName' in data): audio_name = data['audioName']
								reply = "{\"title\":\"${title}\",\"subType\":\"music\",\"sourceName\":\"${sourceName}\",\"sourceIcon\":\"${sourceIcon}\",\"imageUrl\":\"${imageUrl}\",\"imageName\":\"${imageName}\",\"cardIconName\":\"${cardIconName}\",\"author\":\"${author}\",\"audioUrl\":\"${audioUrl}\",\"audioName\":\"${audioName}\",\"album\":\"${album}\"}".replace("${title}", title).replace("${sourceName}", source_name).replace("${sourceIcon}", source_icon).replace("${imageUrl}", image_url).replace("${imageName}", image_name).replace("${cardIconName}", card_icon_name).replace("${author}", author).replace("${audioUrl}", audio_url).replace("${audioName}", audio_name).replace("${album}", album)
							elif sub_type == 'voice':
								reply_type = 1
								url = msg_obj['payload'][0]['value']
								name = ""
								if (len(msg_obj['payload'][0]['data']) > 0 and 'name' in msg_obj['payload'][0]['data'][0]): name = msg_obj['payload'][0]['data'][0]['name']
								reply = "{\"url\":\"${url}\",\"subType\":\"voice\",\"name\":\"${name}\"}".replace("${url}", url).replace("${name}", name)
							elif sub_type == 'image':
								reply_type = 1
								url = msg_obj['payload'][0]['value']
								name = ""
								if (len(msg_obj['payload'][0]['data']) > 0 and 'name' in msg_obj['payload'][0]['data'][0]): name = msg_obj['payload'][0]['data'][0]['name']
								reply = "{\"url\":\"${url}\",\"subType\":\"image\",\"name\":\"${name}\"}".replace("${url}", url).replace("${name}", name)
							elif sub_type == 'imageText':
								reply_type = 11
								payload = msg_obj['payload'][0]
								data = dict(payload['data'][0])
								title = ""
								if ('title' in data): title = data['title']
								source_name = ""
								if ('sourceName' in data): source_name = data['sourceName']
								source_icon = ""
								if ('sourceIcon' in data): source_icon = data['sourceIcon']
								image_url = ""
								if ('imageUrl' in data): image_url = data['imageUrl']
								image_name = ""
								if ('imageName' in data): image_name = data['imageName']
								card_icon_name = ""
								if ('cardIconName' in data): card_icon_name = data['cardIconName']
								link_url = ""
								if ('linkUrl' in data): link_url = data['linkUrl']
								content = ""
								if ('content' in data): content = data['content']
								reply = "{\"title\":\"${title}\",\"subType\":\"imageText\",\"sourceName\":\"${sourceName}\",\"sourceIcon\":\"${sourceIcon}\",\"linkUrl\":\"${linkUrl}\",\"imageUrl\":\"${imageUrl}\",\"imageName\":\"${imageName}\",\"content\":\"${content}\",\"cardIconName\":\"${cardIconName}\"}".replace("${title}", title).replace("${sourceName}", source_name).replace("${sourceIcon}",source_icon).replace("${imageUrl}", image_url).replace("${imageName}", image_name).replace("${cardIconName}", card_icon_name).replace("${content}", content).replace("${linkUrl}", link_url)

					insert_sql = """INSERT INTO t_intent_response(intent_id,reply,reply_type,reply_index) VALUES('%s','%s','%s','%s')""" % (intent_id, reply, str(reply_type), reply_index)
					response_id = mysql_insert_target(insert_sql)
					add_intent_response_condition(action_group, response_id, intent_id)
				break


def add_intent_response_condition(action_group, response_id, intent_id):
	for condtion in action_group['conditionList'][0]:
		function_name = condtion['functions'][0]['function_name']
		key = condtion['functions'][0]['content'][0]['key']
		val = condtion['functions'][0]['content'][0]['val']
		compare = condtion['functions'][0]['content'][0]['compare']
		insert_sql = """INSERT INTO t_intent_response_condition(intent_id,response_id,function_name,`key`,val,compare_rule) VALUES('%s','%s','%s','%s','%s','%s')""" % (intent_id, response_id, function_name, key, val, compare)
		mysql_insert_target(insert_sql)


def change_json_key(string, slots):
	for slot in slots:
		string = string.replace("$global{" + slot + "}", "$" + slot)
	return string


def mysql_insert_target(sql):
	id = 0
	db = pymysql.connect(mysql_host, mysql_user, mysql_password, mysql_db, mysql_port)
	cursor = db.cursor()
	try:
		cursor.execute(sql)
		id = cursor.lastrowid
		db.commit()
		db.close()
		return id
	except Exception as e:
		db.rollback()
		print(e)
		return id


def mysql_query_source_list(query_sql):
	db = pymysql.connect(source_mysql_host, source_mysql_user, source_mysql_password, source_mysql_db,
						 source_mysql_port)
	cursor = db.cursor()
	try:
		cursor.execute(query_sql)
		ret = cursor.fetchall()
		return ret
	except Exception as e:
		print(e)
	return None


if __name__ == '__main__':
	print('start handler skill json to intent response migrate')
	handler_skill_json()
	print('end handler skill json to intent response migrate')
