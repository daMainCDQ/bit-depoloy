-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
alter table t_skill_parser modify column slot_desc varchar (255);

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
