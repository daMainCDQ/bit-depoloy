-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
INSERT INTO `t_parser`(`id`, `zh_name`, `eng_name`, `flag`, `type`, `user_id`, `url`) VALUES (1, '金钱', 'MONEY', 1, 1, NULL, NULL);
INSERT INTO `t_parser`(`id`, `zh_name`, `eng_name`, `flag`, `type`, `user_id`, `url`) VALUES (2, '时间', 'DATE_TIME', 1, 1, NULL, NULL);
INSERT INTO `t_parser`(`id`, `zh_name`, `eng_name`, `flag`, `type`, `user_id`, `url`) VALUES (3, '地址', 'ADDRESS', 1, 1, NULL, NULL);
INSERT INTO `t_parser`(`id`, `zh_name`, `eng_name`, `flag`, `type`, `user_id`, `url`) VALUES (4, '人数', 'PERSON_NUMBER', 1, 1, NULL, NULL);
INSERT INTO `t_parser`(`id`, `zh_name`, `eng_name`, `flag`, `type`, `user_id`, `url`) VALUES (5, '电话', 'PHONE_NUMBER', 1, 1, NULL, NULL);
INSERT INTO `t_parser`(`id`, `zh_name`, `eng_name`, `flag`, `type`, `user_id`, `url`) VALUES (6, '姓氏', 'SURNAME', 1, 1, NULL, NULL);
INSERT INTO `t_parser`(`id`, `zh_name`, `eng_name`, `flag`, `type`, `user_id`, `url`) VALUES (7, '人名', 'PERSON_NAME', 1, 1, NULL, NULL);
INSERT INTO `t_parser`(`id`, `zh_name`, `eng_name`, `flag`, `type`, `user_id`, `url`) VALUES (8, '数量', 'QUANTITY', 1, 1, NULL, NULL);
INSERT INTO `t_parser`(`id`, `zh_name`, `eng_name`, `flag`, `type`, `user_id`, `url`) VALUES (9, '国内出发与到达', 'FROM_TO', 1, 1, NULL, NULL);

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
delete from `t_parser` where id in (1,2,3,4,5,6,7,8,9);