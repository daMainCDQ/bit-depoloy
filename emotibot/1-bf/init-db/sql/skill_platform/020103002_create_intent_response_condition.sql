-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
drop table if EXISTS `t_intent_response_condition`;
CREATE TABLE `t_intent_response_condition` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `intent_id` bigint(20) NOT NULL COMMENT '意图id',
  `response_id` bigint(20) NOT NULL COMMENT '意图回复Id',
  `function_name` varchar(20) NOT NULL COMMENT '槽位过滤方法(key_val_match，contain_key，not_contain_key)',
  `key` varchar(255) NOT NULL COMMENT '需要过滤的槽位(页面选择的第一个槽位)',
  `val` varchar(255) DEFAULT NULL COMMENT '在function_name为key_val_match时有值，表示过滤的具体的值',
  `compare_rule` varchar(20) DEFAULT NULL COMMENT '比较方式(可能值 >，<，==，!= 分别对应 大于，小于，等于，不等于)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
