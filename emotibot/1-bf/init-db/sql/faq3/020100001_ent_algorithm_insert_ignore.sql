-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
START TRANSACTION;
 
 
 INSERT ignore INTO `ent_ai_algorithm` VALUES 
 (24,'fasttext_zoo_ver_c',1,b'1','char','sean team','{\"lr\": 0.8, \"epoch\": 150, \"ngram\": 2, \"negsample\": 50, \"topic\" : \"http://simple-ft-service:8895/ft_stdq_topic_map\", \"feiyewu_rate\":1.0, \"feiyewu_topic_rate\":0.3, \"pretrain_model\":\"vip_16w\"}','http://simple-ft-service:8895/ft_predict','http://simple-ft-service:8895/ft_train','http://simple-ft-service:8895/ft_batch_predict','http://simple-ft-service:8895/ft_delete','2018-02-28 18:01:18',1,'cn'),
 (25,'fasttext_zoo_ver_s',1,b'1','stroke','sean_team','{\"lr\": 0.6, \"epoch\": 80, \"ngram\": 6, \"negsample\": 50, \"topic\" : \"http://172.16.101.66:8896/ft_stdq_topic_map\"}','http://172.16.101.66:8896/ft_predict','http://172.16.101.66:8896/ft_train','http://172.16.101.66:8896/ft_batch_predict','http://172.16.101.66:8896/ft_delete','2018-02-28 18:03:32',1,'cn'),
 (37,'simple_lr',1,b'1','>建议阀值 50','林志豪','{}','http://10.0.0.23:8898/predict','http://10.0.0.23:8898/train','http://10.0.0.23:8898/batch_predict','http://10.0.0.23:8898/delete','2018-04-09 13:40:51',1,'cn'),
 (38,'志豪意图模型',2,b'1','志豪开发的通用的意图训练算法','林志豪','{}','http://172.16.101.66:8899/predict','http://172.16.101.66:8899/train','http://172.16.101.66:8899/batch_predict','http://172.16.101.66:8899/delete','2018-04-28 13:34:52',1,'cn'),
(39,'北京标准句向量训练算法',4,b'1','北京永宁组的句向量模型>，专为bf打造','周洪杰','{\"Epochs\" : 3}','http://172.16.101.66:8900/similarity_multi','http://172.16.101.66:8900/train','http://172.16.101.66:8900/test','http://172.16.101.66:8900/delete','2018-05-05 09:24:21',1,'cn'),
(46,'英文faq',1,b'1','英文faq测试','yunze','{\"lr\": 0.7, \"whitelist\":\"\", \"epoch\": 100, \"ngram\": 2, \"negsample\": 30, \"topic\" : \"http://172.17.0.1:8895/ft_stdq_topic_map\",\"feiyewu_rate\":0, \"feiyewu_topic_rate\":0,\"lang\":\"en\",\"pretrain_model\": \"all_poc\"}','http://172.17.0.1:8895/ft_predict','http://172.17.0.1:8895/ft_train','http://172.17.0.1:8895/ft_batch_predict','http://172.17.0.1:8895/ft_delete','2019-06-19 14:46:45',1,'cn'),
(51,'英文_faq',1,b'1','英文faq测试','sean team','{\"lr\": 0.7, \"whitelist\":\"\", \"epoch\": 100, \"ngram\": 2, \"negsample\": 30, \"topic\" : \"http://172.17.0.1:8895/ft_stdq_topic_map\", \"feiyewu_rate\":0, \"feiyewu_topic_rate\":0,\"lang\":\"en\",\"pretrain_model\": \"all_poc\",\"rectify_base_ratio\":0.8}','http://172.17.0.1:8895/ft_predict','http://172.17.0.1:8895/ft_train','http://172.17.0.1:8895/ft_batch_predict','http://172.17.0.1:8895/ft_delete','2019-06-19 18:54:27',1,'cn'),
(52,'English',1,b'1','test english','yunze','{\"lr\": 0.7, \"whitelist\":\"\", \"epoch\": 100, \"ngram\": 2, \"negsample\": 30, \"topic\" : \"http://172.17.0.1:8895/ft_stdq_topic_map\",\"feiyewu_rate\":0, \"feiyewu_topic_rate\":0,\"lang\":\"en\",\"pretrain_model\": \"all_poc\"}','http://172.17.0.1:8895/ft_predict','http://172.17.0.1:8895/ft_ train','http://172.17.0.1:8895/ft_batch_predict','http://172.17.0.1:8895/ft_delete','2019-06-25 15:02:30',1,'cn'),
(53,'英文_faq_算法测试',1,b'1','英文faq测试','sean team','{\"lr\": 0.7, \"whitelist\":\"\", \"epoch\": 100, \"ngram\": 2, \"negsample\": 30, \"topic\" : \"http://simple-ft-service:8895/ft_stdq_topic_map\", \"feiyewu_rate\":0, \"feiyewu_topic_rate\":0,\"lang\":\"en\",\"pretrain_model\": \"all_poc\",\"rectify_base_ratio\":0.8}','http://simple-ft-service:8895/ft_predict','http://simple-ft-service:8895/ft_train','http://simple-ft-service:8895/ft_batch_predict','http://simple-ft-service:8895/ft_delete','2019-09-02 19:57:23',1,'en');


COMMIT;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back