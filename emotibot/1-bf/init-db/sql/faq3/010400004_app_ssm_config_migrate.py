import pymysql
import json
import os
import time


source_mysql_host = os.environ.get('INIT_MYSQL_HOST')
source_mysql_port = int(os.environ.get('INIT_MYSQL_PORT'))
source_mysql_user = os.environ.get('INIT_MYSQL_USER')
source_mysql_password = os.environ.get('INIT_MYSQL_PASSWORD')
source_mysql_db = 'emotibot'


mysql_host = os.environ.get('INIT_MYSQL_HOST')
mysql_port = int(os.environ.get('INIT_MYSQL_PORT'))
mysql_user = os.environ.get('INIT_MYSQL_USER')
mysql_password = os.environ.get('INIT_MYSQL_PASSWORD')
mysql_db = 'faq3'




def handler_ml_aglo():
    """更新ent_config表的ml_aglo_prd值为 ent_config表的ml_aglo值
       更新ent_config_appid_customization 表的ml_aglo_prd，值为相同app ID的 ml_aglo值

    """
    query_source_mlalgo_sql = """select name,module,value,description,zhtw,enus from ent_config where name = 'ml_algo' """

    source_db = pymysql.connect(host = source_mysql_host,port = source_mysql_port, user = source_mysql_user, passwd = source_mysql_password, db = source_mysql_db)
    cursor = source_db.cursor()

    cursor.execute(query_source_mlalgo_sql)
    source_mlalgo = cursor.fetchone()

    # insert_target_mlalgo_prd_sql = """insert into ent_config (name,module,value,description,zhtw,enus) values ('%s','%s','%s','%s','%s','%s')
    #                             """%('ml_algo_prd',source_mlalgo[1],source_mlalgo[2],source_mlalgo[3],source_mlalgo[4],source_mlalgo[5])
    update_target_mlalgo_prd_sql = """update ent_config set value = '%s' where name = 'ml_algo_prd'""" % (
    source_mlalgo[2])

    target_db = pymysql.connect(host = mysql_host,port = mysql_port, user =mysql_user, passwd =mysql_password, db = mysql_db)
    target_cursor = target_db.cursor()
    target_cursor.execute(update_target_mlalgo_prd_sql)
    target_db.commit()

    ml_algo_query_sql = """select name,app_id,value from ent_config_appid_customization where name = 'ml_algo' """
    ml_algo_cursor = source_db.cursor()
    ml_algo_cursor.execute(ml_algo_query_sql)
    ml_algo_list = ml_algo_cursor.fetchall()

    target_cursor = target_db.cursor()
    for i in ml_algo_list:
        name = i[0]
        app = i[1]
        value = i[2]
        insert_ml_algo_prd_sql = """insert ignore into ent_config_appid_customization (name,app_id,value) values ('ml_algo_prd',%s,%s)"""
        insert_ml_algo_prd_condition = (app, value)
        target_cursor.execute(insert_ml_algo_prd_sql,insert_ml_algo_prd_condition)

    target_db.commit()

    source_db.close()
    target_db.close()

    return


def handler_app_ssm_config(app_id):
    query_ssm_config_sql = """select name,module,value,description,zhtw,enus from ent_config where name = 'ssm_config'"""
    db = pymysql.connect(host = mysql_host,port = mysql_port,user= mysql_user, passwd = mysql_password, db = mysql_db)
    cursor = db.cursor()
    cursor.execute(query_ssm_config_sql)
    ret = cursor.fetchone()
    insert_ssm_config_rc_sql = """insert ignore into ent_config (name,module,value,description,zhtw,enus) values ('%s','%s','%s','%s','%s','%s')""" \
                               % ('ssm_config_rc', ret[1], ret[2], ret[3], ret[4], ret[5])
    update_ret = mysql_update_target(insert_ssm_config_rc_sql)
    if not update_ret:
        print('insert ssm_config_rc fail')
        return

    if app_id is None:
        query_old_app_ssmconfig_sql = """select name,app_id,value from ent_config_appid_customization where name = 'ssm_config' """
    else:
        query_old_app_ssmconfig_sql = """select name,app_id,value from ent_config_appid_customization where app_id = '%s' and name = 'ssm_config'""" % (
        app_id)
    old_app_configs = mysql_query_source_list(query_old_app_ssmconfig_sql)
    for i in old_app_configs:
        app = i[1]
        value = i[2]
        value_json = json.loads(value)
        for item in value_json['items']:
            if item['name'] == 'rank':
                for di in item['value']:
                    if di['source'] == 'qq':
                        for qqdi in di['dependency']:
                            if qqdi['name'] == 'solr':
                                qqdi['name'] = 'es'
            if item['name'] == 'dependency':
                for vi in item['value']:
                    if vi['name'] == 'solr':
                        vi['name'] = 'es'
                        vi['clazz'] = 'SSMDependencyES'
                        vi['url'] = '@es_url'
                        for ri in vi['result']:
                            if ri['source'] == 'solr':
                                ri['source'] = 'es'

                    if vi['name'] == 'se':
                        vi['parameters']['match_q'][0] = '@es[d]/matchQuestion'
                        vi['result'][0]['answer'] = '@es[d]/answer'
                        vi['result'][0]['matchQuestion'] = '@es[d]/matchQuestion'

                    if vi['name'] == 'w2v':
                        vi['parameters'][0]['tar'][0]['tar_kw'] = '@es[d]/keywords:~joinByComma'
                        vi['parameters'][0]['tar'][0]['tar_seg'] = '@es[d]/tokens:~joinByComma'
                        vi['result'][0]['answer'] = '@es[d]/answer'
                        vi['result'][0]['matchQuestion'] = '@es[d]/matchQuestion'

        update_app_ssmconfig_sql = """update ent_config_appid_customization set value = '%s' where app_id = '%s' and name = 'ssm_config'""" % (
        json.dumps(value_json), str(app))
        insert_app_ssmconfig_rc_sql = """insert ignore into ent_config_appid_customization (name,app_id,value) values ('%s','%s','%s')""" % (
        'ssm_config_rc', str(app), json.dumps(value_json))

        update_ret = mysql_update_target(update_app_ssmconfig_sql)
        if not update_ret:
            print('update app_id ssm_config: %s fail' % (str(app)))
        else:
            print('update app_id ssm_config success : %s' % (str(app)))

        update_ret = mysql_update_target(insert_app_ssmconfig_rc_sql)
        if not update_ret:
            print('insert app_id ssm_config_rc : %s fail' % (str(app)))
        else:
            print('insert app_id ssm_config success : %s' % (str(app)))
    return


def mysql_update_target(sql):
    db = pymysql.connect(host = mysql_host,port = mysql_port, user = mysql_user,passwd= mysql_password,db= mysql_db)
    cursor = db.cursor()
    try:
        cursor.execute(sql)
        db.commit()
        db.close()
        return True
    except Exception as  e:
        db.rollback()
        print(e)
        return False


def mysql_query_source_list(query_sql):
    db = pymysql.connect(host = source_mysql_host,port = source_mysql_port, user = source_mysql_user, passwd = source_mysql_password, db = source_mysql_db)
    cursor = db.cursor()
    try:
        cursor.execute(query_sql)
        ret = cursor.fetchall()
        return ret
    except Exception as e:
        print(e)
    return None


def handler_upload_history():
    source_upload_list_sql = """select user_id,app_id,is_part,type,file_path,rows,valid_rows,date_time,done_rows from tbl_upload_corpus_history"""
    source_upload_list = mysql_query_source_list(source_upload_list_sql)
    source_cnt = 0
    sq_ans = 0
    lq_sq = 0
    tq = 0
    if source_upload_list:
        target_db = pymysql.connect(host = mysql_host,port = mysql_port, user = mysql_user, passwd=mysql_password,db= mysql_db)

        for i in source_upload_list:
            source_cnt += 1
            user_id = i[0]
            app_id = i[1]
            is_part = i[2]
            is_part = '1' if is_part == b'\x01' else '2'
            type = i[3]
            file_path = i[4]
            file_name = file_path
            if len(file_path) > 60: file_name = file_path[0:50]
            file_path = str(app_id) + '/' + file_path
            rows = i[5]
            valid_rows = i[6]
            done_rows = i[8]
            insert_sql = ''
            insert_args = ()
            if type == 'SQ-Answer' or type == 'SQ-Answer-New':
                sq_ans += 1
                insert_sql = """insert into tbl_upload_sq_history (user_id,app_id,file_path,rows,valid_rows,finish_rows,create_user,file_name,upload_type)
                            values (%s,%s,%s,%s,%s,%s,%s,%s,%s) """
                insert_args = (user_id, app_id, file_path, rows, valid_rows, done_rows, user_id, file_name, is_part)
            elif type == 'LQ-SQ':
                lq_sq += 1
                insert_sql = """insert into tbl_upload_corpus_history (user_id,app_id,file_path,rows,valid_rows,finish_rows,create_user,file_name,upload_type)
                    values (%s,%s,%s,%s,%s,%s,%s,%s,%s) """
                insert_args = ( user_id, app_id, file_path, rows, valid_rows, done_rows, user_id, file_name, is_part)
            elif type == 'TEST_CASE':
                tq += 1
                insert_sql = """insert into tbl_upload_testset_history (user_id,app_id,file_path,rows,valid_rows,finish_rows,create_user)
                    values (%s,%s,%s,%s,%s,%s,%s) """
                insert_args = (user_id, app_id, file_path, rows, valid_rows, done_rows, user_id)

            if insert_sql == '': continue
            target_cursor = target_db.cursor()
            print(insert_sql)
            target_cursor.execute(insert_sql,insert_args)
            target_cursor.execute('select last_insert_id();')
            last_insert = target_cursor.fetchone()
            target_db.commit()
            if not last_insert:
                print('error on file_path :%s' % (file_path))

        print('source cnt : %s' % (source_cnt))
        print('sq-ans cnt : %s' % (sq_ans))
        print('lq-sq cnt : %s' % (lq_sq))
        print('tq cnt : %s' % (tq))

    return




if __name__ == '__main__':
    app_id = None
    print('start handler handler_ml_aglo migrate')
    handler_ml_aglo()
    print('end handler handler_ml_aglo migrate')

    print('start handler app ssm config migrate')
    handler_app_ssm_config(app_id)
    print('start handler app ssm config migrate')

    print('start handler_upload_history migrate')
    handler_upload_history()
    print('end handler_upload_history migrate')



