-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

alter table faq3.tbl_ans add `period_type` smallint default 0 COMMENT '周期类型0:默认，1:每天';
alter table faq3.tbl_ans_online add `period_type` smallint default 0 COMMENT '周期类型0:默认，1:每天';


-- +migrate Down
alter table faq3.tbl_ans drop `period_type` ;
alter table faq3.tbl_ans_online drop `period_type` ;
