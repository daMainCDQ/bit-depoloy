-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

create table if not exists tbl_context_corpus
(
  id          int auto_increment
    primary key,
  appid       varchar(100)                        not null,
  stdq        varchar(120)                        not null,
  entity      varchar(120)                        not null,
  context     varchar(120)                        not null,
  status      int                                 not null,
  create_time timestamp default CURRENT_TIMESTAMP not null,
  update_time timestamp default CURRENT_TIMESTAMP not null
);
-- +migrate Down