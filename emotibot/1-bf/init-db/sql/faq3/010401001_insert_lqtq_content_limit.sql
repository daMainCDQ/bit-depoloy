-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
START TRANSACTION;
INSERT IGNORE INTO faq3.`ent_config` VALUES  
   ('upload_lq_content_limit','helper','120',b'1','语料长度限制',NULL,NULL),
   ('testset_content_limit','helper','120',b'1','测试题长度限制',NULL,NULL);
update IGNORE faq3.`ent_config` set value = '120' where name = 'upload_sq_content_limit';
COMMIT;

 
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
delete from faq3.`ent_config` where name in ('upload_lq_content_limit','testset_content_limit');
update IGNORE faq3.`ent_config` set value = '50' where name = 'upload_sq_content_limit';
