-- +migrate Up
CREATE TABLE IF NOT EXISTS `faq3`.`tbl_corpus_indexed` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'auto increment id',
  `app_id` varchar(64) NOT NULL DEFAULT '' COMMENT '机器人ID',
  `corpus` varchar(2000) NOT NULL COMMENT '语料',
  `std_q_content` varchar(2000) NOT NULL COMMENT '标准问',
  `sq_id` bigint(20) NOT NULL COMMENT '标准问ID',
  `index_name` varchar(40) NOT NULL COMMENT '标准问及语料索引号',
  PRIMARY KEY (`id`),
  KEY `app_id_index` (`app_id`,`index_name`,`corpus`(100)) USING BTREE,
  KEY `app_id_sq_index` (`app_id`,`index_name`,`std_q_content`(100)) USING BTREE,
  KEY `app_id_sq_id` (`app_id`,`index_name`,`sq_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='训练发布用的语料表';

-- +migrate Down
DROP TABLE `faq3`.`tbl_corpus_indexed`;
