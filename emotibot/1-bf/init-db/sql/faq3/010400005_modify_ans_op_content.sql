-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
alter table faq3.tbl_ans_audit_op modify `origin_content` mediumtext COMMENT '答案修改前数据';
alter table faq3.tbl_ans_audit_op modify `modify_content` mediumtext COMMENT '答案修改后content';


-- +migrate Down
alter table faq3.tbl_ans_audit_op modify `origin_content` varchar(2000) NOT NULL DEFAULT '' COMMENT '答案修改前数据';
alter table faq3.tbl_ans_audit_op modify `modify_content` varchar(2000) NOT NULL DEFAULT '' COMMENT '答案修改后content';
