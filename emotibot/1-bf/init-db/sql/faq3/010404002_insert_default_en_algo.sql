-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
START TRANSACTION;
INSERT IGNORE INTO `faq3`.`ent_ai_algorithm` (`name`, `kind`, `enabled`, `description`, `provider`, `default_config`, `predict_url`, `train_url`, `test_url`, `remove_url`, `is_default`,`lang`,`create_datetime`)
VALUES ('英文_faq_算法测试', '1', b'1', '英文faq测试', 'sean team',
'{\"lr\": 0.7, \"whitelist\":\"\", \"epoch\": 100, \"ngram\": 2, \"negsample\": 30, \"topic\" : \"http://simple-ft-service:8895/ft_stdq_topic_map\", \"feiyewu_rate\":0, \"feiyewu_topic_rate\":0,\"lang\":\"en\",\"pretrain_model\": \"all_poc\",\"rectify_base_ratio\":0.8}',
 'http://simple-ft-service:8895/ft_predict',
 'http://simple-ft-service:8895/ft_train',
 'http://simple-ft-service:8895/ft_batch_predict',
 'http://simple-ft-service:8895/ft_delete',
 1,'en',
 now());
COMMIT;


-- +migrate Down
delete from faq3.`ent_ai_algorithm` where name = '英文_faq_算法测试';

