

-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
START TRANSACTION;


insert IGNORE into faq3.ent_config select * from emotibot.ent_config;
 INSERT IGNORE INTO `ent_config` VALUES   
('ml_algo_prd','default','[\n    {\n        \"algo\": 24,\n        \"kind\": 1,\n        \"model\": \"unknown_20190228150726\",\n        \"solution\": null,\n        \"config\": \"{\\\"lr\\\": 0.8, \\\"epoch\\\": 150, \\\"ngram\\\": 2, \\\"negsample\\\": 50, \\\"topic\\\" : \\\"http://simple-ft-service:8895/ft_stdq_topic_map\\\", \\\"feiyewu_rate\\\":1.0, \\\"feiyewu_topic_rate\\\":0.3, \\\"pretrain_model\\\":\\\"vip_16w\\\"}\"\n    },\n    {\n        \"algo\": 38,\n        \"kind\": 2,\n        \"model\": \"\",\n        \"solution\": 0\n    },\n    {\n        \"algo\": 0,\n        \"kind\": 3,\n        \"model\": \"\",\n        \"solution\": 0\n    },\n    {\n        \"algo\": 4,\n        \"kind\": 39,\n        \"model\": \"\",\n        \"solution\": 0\n    }\n]',b'1','algo_ml_prd',NULL,NULL),
('related_sq_num_limit','default','6',b'1','相关问数量',NULL,NULL),
('upload_lq_max_file_rows','helper','999999',b'1','语料文件大小',NULL,NULL),
('upload_lq_max_file_size','helper','1000',b'1','语料行数',NULL,NULL),
('upload_sq_max_file_rows','helper','999999',b'1','标准问文件行数',NULL,NULL),
('upload_sq_max_file_size','helper','1000',b'1','标准问文件大小',NULL,NULL),
('upload_tq_max_file_rows','helper','999999',b'1','测试题文件行数',NULL,NULL),
('upload_sq_content_limit','helper','50',b'1','标准问长度限制',NULL,NULL),
('upload_tq_max_file_size','helper','1000',b'1','测试题文件大写',NULL,NULL);

update faq3.ent_config set value = '{
	"items" : [
		{
			"name" : "candidate",
			"value" : 5
		},
		{
			"name" : "seg_url",
			"value" : "http://snlu:13901"
		},
		{
			"name" : "rank",
			"value" : [
				{
					"dependency" : [
						{
							"name" : "ml",
							"weight" : 1
						}
					],
					"order" : 1,
					"source" : "ml",
					"threadholds" : 97
				},
				{
					"dependency" : [
						{
							"name" : "se",
							"weight" : 0.300000
						},
						{
							"name" : "w2v",
							"weight" : 0.600000
						},
						{
							"name" : "es",
							"weight" : 0.100000
						}
					],
					"order" : 2,
					"source" : "qq",
					"threadholds" : 85
				}
			]
		},
		{
			"name" : "dependency",
			"value" : [
				{
					"candidate" : 30,
					"clazz" : "SSMDependencyES",
					"enabled" : true,
					"method" : "get",
					"name" : "es",
					"order" : 1,
					"parameters" : "p",
					"result" : [
						{
							"answer" : "@[d]/answer_list[0]",
							"keywords" : "@[d]/keywords",
							"matchQuestion" : "@[d]/question",
							"question_id" : "@[d]/question_id:int",
							"score" : "@[d]/score:~toPercent",
							"source" : "es",
							"stop_words" : "@[d]/stop_words",
							"tokens" : "@[d]/tokens"
						}
					],
					"timeout" : 60000,
					"url" : "@es_url"
				},
				{
					"candidate" : 5,
					"enabled" : true,
					"method" : "POST",
					"name" : "ml",
					"order" : 2,
					"parameters" : {
						"candidate" : "@candidate",
						"data" : "@Text",
						"model" : "@model"
					},
					"result" : [
						{
							"answer" : "@result/predict",
							"matchQuestion" : "@result/predict",
							"score" : "@result/score",
							"source" : "ML"
						}
					],
					"timeout" : 60000,
					"url" : "@model_predict_url"
				},
				{
					"enabled" : true,
					"formData" : false,
					"method" : "POST",
					"name" : "se",
					"order" : 2,
					"parameters" : {
						"match_q" : [
							"@es[d]/matchQuestion"
						],
						"user_q" : "@Text"
					},
					"result" : [
						{
							"answer" : "@es[d]/answer",
							"matchQuestion" : "@es[d]/matchQuestion",
							"score" : "@Msg/{d}:~toPercent",
							"source" : "SE"
						}
					],
					"timeout" : 60000,
					"url" : "http://simple-ft-service:8895/similar"
				},
				{
					"enabled" : true,
					"formData" : false,
					"method" : "POST",
					"name" : "w2v",
					"order" : 2,
					"parameters" : [
						{
							"src" : {
								"src_kw" : "@nlp/key_words:~joinByComma",
								"src_seg" : "@nlp/tokens:~joinByComma",
								"stoplist" : "@nlp/stop_words:~joinByComma"
							},
							"tar" : [
								{
									"id" : "{d}",
									"tar_kw" : "@es[d]/keywords:~joinByComma",
									"tar_seg" : "@es[d]/tokens:~joinByComma"
								}
							]
						}
					],
					"result" : [
						{
							"answer" : "@es[d]/answer",
							"matchQuestion" : "@es[d]/matchQuestion",
							"score" : "@scores/{d}:~toPercent",
							"source" : "w2v"
						}
					],
					"timeout" : 60000,
					"url" : "http://rc-word2vec:11501/partial_w2v"
				},
				{
                    "enabled":false,
                    "clazz":"SSMDependencyMLContext",
                    "method":"POST",
                    "name":"topic",
                    "order":1,
                    "parameters":{
                        "model":"@model"
                    },
                    "result":"@result",
                    "timeout":60000,
                    "url":"http://simple-ft-service:8895/ft_stdq_topic_map"
                }
			]
		}
	]
}' where name = 'ssm_config';


insert IGNORE into faq3.ent_config_appid_customization select * from emotibot.ent_config_appid_customization;


COMMIT;


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
TRUNCATE TABLE `ent_config`;
TRUNCATE TABLE `ent_config_appid_customization`;

