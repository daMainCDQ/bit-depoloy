
-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

START TRANSACTION;
DROP TABLE IF EXISTS `ent_ai_algorithm`;
CREATE TABLE `ent_ai_algorithm` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(45) NOT NULL COMMENT '算法名称',
  `kind` int(11) NOT NULL COMMENT '算法类型',
  `enabled` bit(1) NOT NULL DEFAULT b'1' COMMENT '是否可用',
  `description` varchar(45) NOT NULL COMMENT '算法描述',
  `provider` varchar(45) NOT NULL COMMENT '算法提供者',
  `default_config` mediumtext NOT NULL COMMENT '预设设定值',
  `predict_url` varchar(200) NOT NULL COMMENT '预测用网路位址',
  `train_url` varchar(200) NOT NULL COMMENT '训练用网路位址',
  `test_url` varchar(200) NOT NULL COMMENT '测试用网路位址',
  `remove_url` varchar(200) NOT NULL DEFAULT 'n/a' COMMENT '移除用网路位址',
  `create_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci COMMENT='AI 算法库。可以包含 sentence embeding 学习算法， ml classify 算法， intent 训练算法等。';

DROP TABLE IF EXISTS `ent_config`;
CREATE TABLE `ent_config` (
  `name` varchar(50) NOT NULL COMMENT '配置名称',
  `module` varchar(45) NOT NULL DEFAULT 'default' COMMENT '配置模块',
  `value` varchar(8000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '配置值',
  `enabled` bit(1) DEFAULT b'1' COMMENT '是否启用',
  `description` varchar(500) DEFAULT NULL COMMENT '描述',
  `zhtw` varchar(4000) DEFAULT NULL COMMENT '台湾繁体中文',
  `enus` varchar(4000) DEFAULT NULL COMMENT '英文美国',
  PRIMARY KEY (`name`),
  KEY `idx_name_module` (`name`(20),`module`(20))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci COMMENT='配置表';



DROP TABLE IF EXISTS `ent_config_appid_customization`;
CREATE TABLE `ent_config_appid_customization` (
  `name` varchar(50) NOT NULL COMMENT '配置名称',
  `app_id` varchar(50) NOT NULL COMMENT '机器人id',
  `value` varchar(8000) NOT NULL COMMENT '配置值',
  PRIMARY KEY (`name`,`app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci COMMENT='机器人定制化配置表';


DROP TABLE IF EXISTS `tbl_ai_solution`;
CREATE TABLE `tbl_ai_solution` (
  `id` int(11) NOT NULL AUTO_INCREMENT comment '主键ID',
  `name` varchar(50) DEFAULT NULL comment '解决方案名称',
  `enabled` bit(1) NOT NULL DEFAULT b'1' comment '是否有效',
  `description` varchar(200) NOT NULL comment '描叙',
  `ai_algorithm_id` int(11) NOT NULL comment '算法ID',
  `config` mediumtext comment '配置',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ai_solution_name` (`name`,`ai_algorithm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci comment='解决方案';

DROP TABLE IF EXISTS `tbl_ans`;
CREATE TABLE `tbl_ans` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'auto increment id',
  `app_id` varchar(64) NOT NULL DEFAULT '' COMMENT '机器人ID',
  `sq_id` bigint(11) NOT NULL DEFAULT '0' COMMENT '标准问id',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '状态0（未变动）1（修改待训练）2（修改待发布）-1（删除待训练）-2（删除待发布） ',
  `dimension` varchar(64) NOT NULL DEFAULT '' COMMENT '维度id拼接',
  `content` mediumtext COMMENT '答案',
  `create_user` varchar(64) NOT NULL DEFAULT '' COMMENT '创建用户',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_user` varchar(64) NOT NULL DEFAULT '' COMMENT '更新用户',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `start_time` datetime DEFAULT NULL COMMENT '答案生效时间',
  `end_time` datetime DEFAULT NULL COMMENT '答案失效时间',
  PRIMARY KEY (`id`),
  KEY `idx_sqid` (`sq_id`),
  KEY `idx_app` (`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci COMMENT='答案表';

DROP TABLE IF EXISTS `tbl_ans_related_sq`;
CREATE TABLE `tbl_ans_related_sq` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'auto increment id',
  `app_id` varchar(64) NOT NULL DEFAULT '' COMMENT '机器人ID',
  `ans_id` bigint(11) NOT NULL DEFAULT '0' COMMENT '答案id',
  `sq_id` bigint(11) NOT NULL DEFAULT '0' COMMENT '标准问id',
  `create_user` varchar(64) NOT NULL DEFAULT '' COMMENT '创建用户',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_user` varchar(64) NOT NULL DEFAULT '' COMMENT '更新用户',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_app_ansid` (`app_id`,`ans_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci COMMENT='答案与关联标准问的对照表';


DROP TABLE IF EXISTS `tbl_ans_tag`;
CREATE TABLE `tbl_ans_tag` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'auto increment id',
  `ans_id` bigint(11) DEFAULT '0' COMMENT '回答ID',
  `tag_id` bigint(11) DEFAULT '0' COMMENT '标签ID',
  PRIMARY KEY (`id`),
  KEY `idx_ansid` (`ans_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci COMMENT='回答对应的标签';


DROP TABLE IF EXISTS `tbl_corpus`;
CREATE TABLE `tbl_corpus` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'auto increment id',
  `app_id` varchar(64) NOT NULL DEFAULT '' COMMENT '机器人ID',
  `sq_id` bigint(11) NOT NULL DEFAULT '0' COMMENT '标准问id',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '状态0（未变动）1（修改待训练）2（修改待发布）-1（删除待训练）-2（删除待发布） ',
  `content` varchar(2000) NOT NULL DEFAULT '' COMMENT '标准问内容',
  `create_user` varchar(64) NOT NULL DEFAULT '' COMMENT '创建用户',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_user` varchar(64) NOT NULL DEFAULT '' COMMENT '更新用户',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_sqid_cp` (`sq_id`),
  KEY `idx_app` (`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci COMMENT='语料';


DROP TABLE IF EXISTS `tbl_corpus_audit_op`;
CREATE TABLE `tbl_corpus_audit_op` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'auto increment id',
  `app_id` varchar(64) NOT NULL DEFAULT '' COMMENT '机器人ID',
  `sq_id` bigint(11) NOT NULL DEFAULT '0' COMMENT '标准问id',
  `corpus_id` bigint(11) NOT NULL DEFAULT '0' COMMENT '语料id',
  `op_type` int(3) NOT NULL DEFAULT '0' COMMENT '1增量，2全量导入，3新增，4删除，5修改',
  `status` int(3) NOT NULL DEFAULT '0' COMMENT '1可驳回，2已驳回',
  `origin_data` varchar(2000) NOT NULL DEFAULT '' COMMENT '语料修改前数据',
  `origin_status` int(11) NOT NULL DEFAULT '0' COMMENT '语料修改前数据',
  `modified_data` varchar(2000) NOT NULL DEFAULT '' comment '修改后数据',
  `remark` varchar(4096) DEFAULT '' comment '描述，上传时保存文件地址',
  `release_version` varchar(32) NOT NULL DEFAULT '' COMMENT '发布版本',
  `create_user` varchar(64) NOT NULL DEFAULT '' COMMENT '创建用户',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `audit_user` varchar(64) NOT NULL DEFAULT '' COMMENT '更新用户',
  `audit_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `readed` int(11) NOT NULL DEFAULT '0' COMMENT '是否阅读，1：阅读，0:未阅读',
  `read_user` varchar(64) NOT NULL DEFAULT '' COMMENT '阅读用户',
  PRIMARY KEY (`id`),
  KEY `idx_app` (`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci COMMENT='语料';


DROP TABLE IF EXISTS `tbl_folder`;
CREATE TABLE `tbl_folder` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'auto increment id',
  `app_id` varchar(64) NOT NULL DEFAULT '' COMMENT '机器人ID',
  `parent_folder_id` bigint(11) NOT NULL DEFAULT '0' COMMENT '父目录id',
  `name` varchar(64) NOT NULL DEFAULT '' COMMENT '目录名',
  `fullname` varchar(128) NOT NULL DEFAULT '' COMMENT '目录全路径名',
  `level` int(11) NOT NULL DEFAULT '0' COMMENT '目录层级，根目录为0',
  `create_user` varchar(64) NOT NULL DEFAULT '' COMMENT '创建用户',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_user` varchar(64) NOT NULL DEFAULT '' COMMENT '更新用户',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_app` (`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci COMMENT='目录';


DROP TABLE IF EXISTS `tbl_ml_test_history`;
CREATE TABLE `tbl_ml_test_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_id` varchar(45)  NOT NULL default '' comment '机器人ID',
  `create_user` varchar(64) NOT NULL DEFAULT '' COMMENT '创建用户',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `status` int(11) NOT NULL DEFAULT '0' comment '状态100:完成',
  `train_history_id` int(11) DEFAULT NULL comment '训练ID',
  `test_count` int(11) NOT NULL DEFAULT '0' comment '测试行数',
  `finish_cnt` int(11) DEFAULT '0' comment ' 完成行数',
  `accurate_cnt` int(11) NOT NULL DEFAULT '0' comment '准确数',
  `test_accurate` int(11) DEFAULT '0' comment '准确率',
  `file_path` varchar(4096) DEFAULT '' comment '文件地址',
  PRIMARY KEY (`id`),
  KEY `idx_app` (`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci COMMENT='测试记录表';


DROP TABLE IF EXISTS `tbl_ml_test_result`;
CREATE TABLE `tbl_ml_test_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT comment '主键ID',
  `app_id` varchar(45) DEFAULT NULL comment '机器人ID',
  `ml_test_history_id` int(11) NOT NULL comment '测试记录ID',
  `testset_id` bigint(11) unsigned NOT NULL  comment '测试集ID',
  `question` varchar(4000) DEFAULT NULL comment '问题',
  `sq_id` bigint(11) NOT NULL DEFAULT '0' comment '标准问ID',
  `sq_content` varchar(2000) NOT NULL DEFAULT '' COMMENT '标准问内容',
  `matched_sq` varchar(2000) DEFAULT NULL comment '匹配的标准问',
  `is_correct` tinyint(4) DEFAULT NULL COMMENT '1 成功 0 失败',
  `score` float DEFAULT NULL comment '得分',
  `source` varchar(45) DEFAULT NULL comment '打分来源',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_app` (`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci COMMENT='测试结果详情表';


DROP TABLE IF EXISTS `tbl_ml_train_history`;
CREATE TABLE `tbl_ml_train_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT comment '主键ID',
  `app_id` varchar(50) NOT NULL comment '机器人ID',
  `status` int(11) NOT NULL DEFAULT '0' comment '状态100完成',
  `model_id` varchar(200) DEFAULT NULL comment '模型ID',
  `result` longtext comment '返回结果',
  `create_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP comment '创建时间',
  `update_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP comment '更新时间',
  `algo_kind_id` int(11) NOT NULL comment '算法类型',
  `algo_id` int(11) NOT NULL comment '算法ID',
  `algo_solution_id` int(11) DEFAULT NULL comment '解决方案ID',
  `train_url` varchar(4000) NOT NULL comment '训练URL',
  `config` longtext NOT NULL comment '训练配置',
  `train_type` tinyint(4) DEFAULT '0' COMMENT '0:config训练，1:platform训练',
  `is_released` tinyint(4) DEFAULT '0' COMMENT '0:未发布，1:已发布',
  `create_user` varchar(64) DEFAULT '' COMMENT '创建用户',
  `release_file_path` varchar(4096) DEFAULT '' COMMENT '发布保存zip文件地址',
  `release_user` varchar(64) DEFAULT '' COMMENT '创建用户',
  PRIMARY KEY (`id`),
  KEY `idx_app` (`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci;


DROP TABLE IF EXISTS `tbl_sq`;
CREATE TABLE `tbl_sq` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'auto increment id',
  `app_id` varchar(64) NOT NULL DEFAULT '' COMMENT '机器人ID',
  `folder_id` bigint(11) NOT NULL DEFAULT '0' COMMENT '目录id',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '状态0（未变动）1（修改待训练）2（修改待发布）-1（删除待训练）-2（删除待发布） ',
  `content` varchar(2000) NOT NULL DEFAULT '' COMMENT '标准问内容',
  `corpus_cnt` int(11) NOT NULL DEFAULT '0' COMMENT '统计标准问语料数',
  `accurate_rate` int(11) DEFAULT NULL COMMENT '统计标准问准确率，千分之',
  `create_user` varchar(64) NOT NULL DEFAULT '' COMMENT '创建用户',
  `create_time` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `update_user` varchar(64) NOT NULL DEFAULT '' COMMENT '更新用户',
  `update_time` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_appid_sq` (`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci COMMENT='标准问';


DROP TABLE IF EXISTS `tbl_sq_op`;
CREATE TABLE `tbl_sq_op` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'auto increment id',
  `app_id` varchar(64) NOT NULL DEFAULT '' COMMENT '机器人ID',
  `sq_id` bigint(11) NOT NULL DEFAULT '0' COMMENT '标准问id',
  `op_type` int(3) NOT NULL DEFAULT '0' COMMENT '1新增，2导入，3删除，4修改问，5修改标签，6修改目录',
  `origin_data` varchar(2000) NOT NULL DEFAULT '' COMMENT '标准问修改前数据',
  `origin_status` int(11) NOT NULL DEFAULT '0' COMMENT '标准问修改前数据',
  `modified_data` varchar(2000) NOT NULL DEFAULT '',
  `remark` varchar(4096) DEFAULT '' COMMENT '下载详情文件地址',
  `release_version` varchar(32) NOT NULL DEFAULT '' COMMENT '发布版本',
  `create_user` varchar(64) NOT NULL DEFAULT '' COMMENT '创建用户',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `audit_user` varchar(64) NOT NULL DEFAULT '' COMMENT '更新用户',
  `audit_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `readed` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否阅读，1：阅读，0:未阅读',
  `read_user` varchar(64) NOT NULL DEFAULT '' COMMENT '阅读用户',
  `status` int(3) NOT NULL DEFAULT '0' COMMENT ' 0:default, 3,disable',
  PRIMARY KEY (`id`),
  KEY `idx_app` (`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci COMMENT='标准问操作记录表';


DROP TABLE IF EXISTS `tbl_sq_tag`;
CREATE TABLE `tbl_sq_tag` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'auto increment id',
  `sq_id` bigint(11) DEFAULT '0' COMMENT '标准问ID',
  `tag_id` bigint(11) DEFAULT '0' COMMENT '标签ID',
  PRIMARY KEY (`id`),
  KEY `idx_sqid` (`sq_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci COMMENT='标准问对应的标签';


DROP TABLE IF EXISTS `tbl_testset`;
CREATE TABLE `tbl_testset` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'auto increment id',
  `app_id` varchar(64) NOT NULL DEFAULT '' COMMENT '机器人ID',
  `sq_id` bigint(11) NOT NULL DEFAULT '0' COMMENT '标准问id',
  `content` varchar(2000) NOT NULL DEFAULT '' COMMENT '测试题内容',
  `create_user` varchar(64) NOT NULL DEFAULT '' COMMENT '创建用户',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_user` varchar(64) NOT NULL DEFAULT '' COMMENT '更新用户',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_appid_sqid` (`app_id`,`sq_id`),
  KEY `idx_sqid` (`sq_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci COMMENT='测试集';


DROP TABLE IF EXISTS `tbl_upload_corpus_history`;
CREATE TABLE `tbl_upload_corpus_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT comment '主键ID',
  `user_id` varchar(64) NOT NULL DEFAULT '' comment '用户ID',
  `app_id` varchar(64) NOT NULL DEFAULT '' comment '机器人ID',
  `file_path` varchar(4096) DEFAULT '' comment '文件路径',
  `rows` int(11) DEFAULT '0' comment '总行数',
  `valid_rows` int(11) DEFAULT '0' comment '有效行数',
  `finish_rows` int(11) DEFAULT '0' comment '完成行数',
  `comment` varchar(100) NOT NULL DEFAULT '' COMMENT '说明',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP comment '创建时间',
  `create_user` varchar(64) NOT NULL DEFAULT '' COMMENT '创建用户',
  `upload_detail_file_path` varchar(4096) DEFAULT '' COMMENT '下载详情文件地址',
  `file_name` varchar(60) DEFAULT '' COMMENT '上传文件名',
  `upload_type` varchar(2) DEFAULT '' COMMENT '1:增量，2:全量',
  PRIMARY KEY (`id`),
  KEY `idx_app` (`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci COMMENT='语料上传历史记录表';


DROP TABLE IF EXISTS `tbl_upload_sq_history`;
CREATE TABLE `tbl_upload_sq_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT comment '主键ID',
  `user_id` varchar(64) NOT NULL DEFAULT '' comment '用户ID',
  `app_id` varchar(64) NOT NULL DEFAULT '' comment '机器人ID',
  `file_path` varchar(4096) DEFAULT '' comment '文件地址',
  `rows` int(11) DEFAULT '0' comment '总行数',
  `valid_rows` int(11) DEFAULT '0' comment '有效行数',
  `finish_rows` int(11) DEFAULT '0' comment '完成行数',
  `comment` varchar(100) NOT NULL DEFAULT '' COMMENT '说明',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP comment '创建时间',
  `create_user` varchar(64) NOT NULL DEFAULT '' COMMENT '创建用户',
  `file_name` varchar(60) DEFAULT '' COMMENT '上传文件名',
  `upload_type` varchar(2) DEFAULT '' COMMENT '1:增量，2:全量',
  `upload_detail_file_path` varchar(4096) DEFAULT '' COMMENT '下载详情文件地址',
  PRIMARY KEY (`id`),
  KEY `idx_app` (`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci COMMENT='标准问答案上传历史记录表';


DROP TABLE IF EXISTS `tbl_upload_testset_history`;
CREATE TABLE `tbl_upload_testset_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT comment '主键ID',
  `user_id` varchar(64) NOT NULL DEFAULT '' comment '用户ID',
  `app_id` varchar(64) NOT NULL DEFAULT '' comment '机器人ID',
  `file_path` varchar(4096) DEFAULT '' comment '文件路径',
  `rows` int(11) DEFAULT '0' comment '总行数',
  `valid_rows` int(11) DEFAULT '0' comment '有效行数',
  `finish_rows` int(11) DEFAULT '0' comment '完成行数',
  `comment` varchar(100) NOT NULL DEFAULT '' COMMENT '说明',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `create_user` varchar(64) NOT NULL DEFAULT '' COMMENT '创建用户',
  `file_name` varchar(60) DEFAULT '' COMMENT '上传文件名',
  `upload_type` varchar(2) DEFAULT '' COMMENT '1:增量，2:全量',
  `upload_detail_file_path` varchar(4096) DEFAULT '' COMMENT '下载详情文件地址',
  PRIMARY KEY (`id`),
  KEY `idx_app` (`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci COMMENT='语料测试集历史记录表';


DROP TABLE IF EXISTS `tbl_ans_audit_op`;
CREATE TABLE `tbl_ans_audit_op` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'auto increment id',
  `app_id` varchar(64) NOT NULL DEFAULT '' COMMENT '机器人ID',
  `sq_id` bigint(11) NOT NULL DEFAULT '0' COMMENT '标准问id',
  `ans_id` bigint(11) NOT NULL DEFAULT '0' COMMENT '答案id',
  `op_type` int(3) NOT NULL DEFAULT '0' COMMENT '1增量，2全量导入，3新增，4删除，5修改',
  `status` int(3) NOT NULL DEFAULT '0' COMMENT '1可驳回，2已驳回,3失效',
  `origin_content` varchar(2000) NOT NULL DEFAULT '' COMMENT '答案修改前数据',
  `origin_status` int(11) NOT NULL DEFAULT '0' COMMENT '答案修改前状态',
  `origin_info` varchar(2000) NOT NULL DEFAULT '' COMMENT '答案附加信息json格式，包括维度，失效生效时间，tags,rqs',
  `modify_content` varchar(2000) NOT NULL DEFAULT '' COMMENT '答案修改后content',
  `modify_info` varchar(2000) NOT NULL DEFAULT '' COMMENT '修改后答案附加信息json格式',
  `remark` varchar(4096) DEFAULT '' comment '描述',
  `release_version` varchar(32) NOT NULL DEFAULT '' COMMENT '发布版本',
  `create_user` varchar(64) NOT NULL DEFAULT '' COMMENT '创建用户',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `audit_user` varchar(64) NOT NULL DEFAULT '' COMMENT '更新用户',
  `audit_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `readed` int(11) NOT NULL DEFAULT '0' COMMENT '是否阅读，1：阅读，0:未阅读',
  `read_user` varchar(64) NOT NULL DEFAULT '' COMMENT '阅读用户',
  PRIMARY KEY (`id`),
  KEY `idx_app` (`app_id`),
  KEY `idx_ansid_cp` (`ans_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci COMMENT='答案审核记录表'
;

DROP TABLE IF EXISTS `tbl_ans_online`;
CREATE TABLE `tbl_ans_online` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'auto increment id',
  `ans_id` bigint(11) NOT NULL  COMMENT '标准问id',
  `app_id` varchar(64) NOT NULL  COMMENT '机器人ID',
  `sq_id` bigint(11) NOT NULL  COMMENT '标准问id',
  `dimension` varchar(64) NOT NULL DEFAULT '' COMMENT '维度id拼接',
  `content` mediumtext COMMENT '答案',
  `tags` varchar(2000) COMMENT '标签 id list,split by ,',
  `rqs` varchar(2000) COMMENT '相关问 id list,split by ,',
  `create_user` varchar(64) NOT NULL DEFAULT '' COMMENT '创建用户',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_user` varchar(64) NOT NULL DEFAULT '' COMMENT '更新用户',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `start_time` datetime DEFAULT NULL COMMENT '答案生效时间',
  `end_time` datetime DEFAULT NULL COMMENT '答案失效时间',
  PRIMARY KEY (`id`),
  KEY `idx_sqid_cp` (`sq_id`),
  KEY `idx_ansid_cp` (`ans_id`),
  KEY `idx_app` (`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci COMMENT='答案online表';


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `ent_ai_algorithm`;
DROP TABLE `ent_config`;
DROP TABLE `ent_config_appid_customization`;
DROP TABLE `tbl_ai_solution`;
DROP TABLE `tbl_ans`;
DROP TABLE `tbl_ans_related_sq`;
DROP TABLE `tbl_ans_tag`;
DROP TABLE `tbl_corpus`;
DROP TABLE `tbl_corpus_audit_op`;
DROP TABLE `tbl_folder`;
DROP TABLE `tbl_ml_test_history`;
DROP TABLE `tbl_ml_test_result`;
DROP TABLE `tbl_ml_train_history`;
DROP TABLE `tbl_sq`;
DROP TABLE `tbl_sq_op`;
DROP TABLE `tbl_sq_tag`;
DROP TABLE `tbl_testset`;
DROP TABLE `tbl_upload_corpus_history`;
DROP TABLE `tbl_upload_sq_history`;
DROP TABLE `tbl_upload_testset_history`;
DROP TABLE `tbl_ans_audit_op`;
DROP TABLE `tbl_ans_online`;


