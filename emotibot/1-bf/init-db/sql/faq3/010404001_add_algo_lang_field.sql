-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
START TRANSACTION;
alter table faq3.ent_ai_algorithm add `is_default` smallint default 1  COMMENT '是否是该类算法默认算法 1:是，2:否';
alter table faq3.ent_ai_algorithm add `lang` varchar(6) default 'cn' COMMENT '算法语言，默认中文,cn:中文,en:英文';
COMMIT;
 

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
alter table faq3.tbl_ans_audit_op drop `is_default` ;
alter table faq3.tbl_ans_audit_op drop `lang` ;
