import pymysql
import json
import os
import time

source_mysql_host = os.environ.get('INIT_MYSQL_HOST')
source_mysql_port = int(os.environ.get('INIT_MYSQL_PORT'))
source_mysql_user = os.environ.get('INIT_MYSQL_USER')
source_mysql_password = os.environ.get('INIT_MYSQL_PASSWORD')
source_mysql_db = 'faq3'

mysql_host = os.environ.get('INIT_MYSQL_HOST')
mysql_port = int(os.environ.get('INIT_MYSQL_PORT'))
mysql_user = os.environ.get('INIT_MYSQL_USER')
mysql_password = os.environ.get('INIT_MYSQL_PASSWORD')
mysql_db = 'faq3'


def update_ssm_config_template(old_app_configs):
	for i in old_app_configs:
		value = i[1]
		value_json = json.loads(value)
		for item in value_json['items']:
			if item['name'] == 'dependency':
				for vi in item['value']:
					if vi['name'] == 'es':
						for ri in vi['result']:
							ri['std_q_id'] = "@[d]/std_q_id"
							ri["module"] = "@[d]/module"
							ri["doc_id"] = "@[d]/doc_id"

		update_app_ssmconfig_sql = """update ent_config set value = '%s' where name = 'ssm_config'""" % (
			json.dumps(value_json))
		insert_app_ssmconfig_rc_sql = """update ent_config set value = '%s' where name = 'ssm_config_rc'""" % (
			json.dumps(value_json))

		update_ret = mysql_update_target(update_app_ssmconfig_sql)
		if not update_ret:
			print('update ssm_config fail ')
		else:
			print('update ssm_config success ')

		update_ret = mysql_update_target(insert_app_ssmconfig_rc_sql)
		if not update_ret:
			print('update ssm_config_rc fail ')
		else:
			print('update ssm_config_rc success ')


def handler_app_ssm_config_template():
	"""
	更新ssm_config模板
	"""
	query_old_app_ssmconfig_sql = """select name,value from ent_config where name = 'ssm_config' """
	old_app_configs = mysql_query_source_list(query_old_app_ssmconfig_sql)
	update_ssm_config_template(old_app_configs)
	return


def handler_app_ssm_config_customization(app_id):
	"""
	更新每个机器人配置
	"""
	if app_id is None:
		query_old_app_ssmconfig_sql = """select name,app_id,value from ent_config_appid_customization where name = 'ssm_config' """
	else:
		query_old_app_ssmconfig_sql = """select name,app_id,value from ent_config_appid_customization where app_id = '%s' and name = 'ssm_config'""" % (
			app_id)
	old_app_configs = mysql_query_source_list(query_old_app_ssmconfig_sql)
	for i in old_app_configs:
		app = i[1]
		value = i[2]
		value_json = json.loads(value)
		for item in value_json['items']:
			if item['name'] == 'dependency':
				for vi in item['value']:
					if vi['name'] == 'es':
						for ri in vi['result']:
							ri['std_q_id'] = "@[d]/std_q_id"
							ri["module"] = "@[d]/module"
							ri["doc_id"] = "@[d]/doc_id"

		update_app_ssmconfig_sql = """update ent_config_appid_customization set value = '%s' where app_id = '%s' and name = 'ssm_config'""" % (
			json.dumps(value_json), str(app))
		update_app_ssmconfig_rc_sql = """update ent_config_appid_customization set value = '%s' where app_id = '%s' and name = 'ssm_config_rc'""" % (
			json.dumps(value_json), str(app))

		update_ret = mysql_update_target(update_app_ssmconfig_sql)
		if not update_ret:
			print('update app_id ssm_config: %s fail' % (str(app)))
		else:
			print('update app_id ssm_config success: %s' % (str(app)))

		update_ret = mysql_update_target(update_app_ssmconfig_rc_sql)
		if not update_ret:
			print('update app_id ssm_config_rc : %s fail' % (str(app)))
		else:
			print('update app_id ssm_config_rc success: %s' % (str(app)))
	return


def mysql_update_target(sql):
	db = pymysql.connect(host=mysql_host, port=mysql_port, user=mysql_user, passwd=mysql_password, db=mysql_db)
	cursor = db.cursor()
	try:
		cursor.execute(sql)
		db.commit()
		db.close()
		return True
	except Exception as  e:
		db.rollback()
		print(e)
		return False


def mysql_query_source_list(query_sql):
	db = pymysql.connect(host=source_mysql_host, port=source_mysql_port, user=source_mysql_user,
	                     passwd=source_mysql_password, db=source_mysql_db)
	cursor = db.cursor()
	try:
		cursor.execute(query_sql)
		ret = cursor.fetchall()
		return ret
	except Exception as e:
		print(e)
	return None


if __name__ == '__main__':
	app_id = None

	print('start handler ssm config template migrate')
	handler_app_ssm_config_template()

	print('start handler app ssm config migrate')
	handler_app_ssm_config_customization(app_id)
