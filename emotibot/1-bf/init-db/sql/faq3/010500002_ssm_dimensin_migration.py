import json
import logging
import os
import sys
import time
import pymysql

mysql_host = os.environ.get('INIT_MYSQL_HOST', '172.16.101.116')
mysql_port = int(os.environ.get('INIT_MYSQL_PORT', '3306'))
mysql_user = os.environ.get('INIT_MYSQL_USER', 'root')
mysql_password = os.environ.get('INIT_MYSQL_PASSWORD', 'password')
mysql_db = 'faq3'
mysql_emotibot_db = 'emotibot'






def handler_dimension_transform(app_id=None):
    try:
        db = pymysql.connect(host=mysql_host, port=mysql_port, user=mysql_user, passwd=mysql_password, db=mysql_db)
        cursor = db.cursor()
        filter_sql = ''
        if app_id is not None:
            filter_sql = ' where app_id in ( "system", "' + app_id + '")'
        cursor.execute('select id,code,name,type,app_id from ' + mysql_emotibot_db + '.tags ' + filter_sql)
        ret = cursor.fetchall()
        system_tag = {}  # code=>id
        app_code_id = {}  # appid => {旧id -> 新ID}
        for i in ret:
            id = i[0]
            code = i[1]
            app = i[4]
            if app == 'system': system_tag[code] = id
        for i in ret:
            id = i[0]
            code = i[1]
            app = i[4]
            if i[4] != 'system':
                if app not in app_code_id: app_code_id[app] = {}
                if code not in system_tag:
                    print('appid code not in system tag:' + str(code))
                    continue
                old_id = system_tag[code]
                app_code_id[app][old_id] = id
        print(app_code_id)

        # update tbl_ans
        if app_id is None:
            cursor.execute('select  id,dimension,app_id from ' + mysql_db + '.tbl_ans where length(dimension) > 0')
        else:
            cursor.execute(
                'select id,dimension,app_id from ' + mysql_db + '.tbl_ans where length(dimension) > 0 and  app_id = %s',
                (str(app_id),))
        ans_list = cursor.fetchall()
        ans_update_cnt = 0
        for i in ans_list:
            id = i[0]
            dim = i[1]
            appi = i[2]
            if appi not in app_code_id:
                continue
            dims = dim.split(',')
            if len(dims) > 0:
                need_update_dimn = [di for di in dims if di != ''  and  int(di) in app_code_id[appi]]
                if len(need_update_dimn) <= 0: continue
                dims = [int(di) if int(di) not in app_code_id[appi] else app_code_id[appi][int(di)] for di in dims if di != '']
                ndim = ',' + ','.join([str(x) for x in dims]) + ','
                cursor.execute('update ' + mysql_db + '.tbl_ans set dimension = %s where id = %s', (ndim, id))
                ans_update_cnt += 1

        print('update ans dimension cnt:%s' % (ans_update_cnt))

        # update tbl_ans_online
        if app_id is None:
            cursor.execute(
                'select  id,dimension,app_id from ' + mysql_db + '.tbl_ans_online where length(dimension) > 0')
        else:
            cursor.execute(
                'select id,dimension,app_id from ' + mysql_db + '.tbl_ans_online where length(dimension) > 0 and  app_id = %s',
                (str(app_id),))
        ans_online_list = cursor.fetchall()
        update_ans_online_cnt = 0
        for i in ans_online_list:
            id = i[0]
            dim = i[1]
            appi = i[2]
            if appi not in app_code_id:
                continue
            dims = dim.split(',')
            if len(dims) > 0:
                need_update_dimn = [di for di in dims if di != '' and int(di) in app_code_id[appi]]
                if len(need_update_dimn) <= 0: continue
                dims = [int(di) if int(di) not in app_code_id[appi] else app_code_id[appi][int(di)] for di in dims if di != '']
                ndim = ',' + ','.join([str(x) for x in dims]) + ','
                cursor.execute('update ' + mysql_db + '.tbl_ans_online set dimension = %s where id = %s', (ndim, id))
                update_ans_online_cnt += 1


        #update ans_andit_op
        if app_id is None:
            cursor.execute('select id,origin_info,modify_info,app_id from '+mysql_db+'.tbl_ans_audit_op')
        else:
            cursor.execute('select id,origin_info,modify_info,app_id from ' + mysql_db + '.tbl_ans_audit_op where app_id = %s',(str(app_id),))
        ans_op_list = cursor.fetchall()
        update_ans_op_cnt = 0
        for i in ans_op_list:
            id = i[0]
            origin_info = i[1]
            modify_info = i[2]
            appi = i[3]

            if appi not in app_code_id:
                continue
            if origin_info is not None and len(origin_info) > 0:
                origin_json = json.loads(origin_info)
                if 'dimension' in origin_json and len(origin_json['dimension']) > 0:
                    dims = origin_json['dimension'].split(',')
                    if len(dims) > 0:
                        need_update_dimn = [di for di in dims if di != ''  and int(di) in app_code_id[appi]]
                        if len(need_update_dimn) > 0:
                            dims = [int(di) if int(di) not in app_code_id[appi] else app_code_id[appi][int(di)] for di
                                    in dims if di != '']
                            ndim = ',' + ','.join([str(x) for x in dims]) + ','
                            origin_json['dimension'] = ndim
                            update_ans_op_cnt += 1
                            cursor.execute('update ' + mysql_db + '.tbl_ans_audit_op set origin_info = %s where id = %s',
                                       (json.dumps(origin_json), id))


            if modify_info is not None and len(modify_info) > 0:
                modify_json = json.loads(modify_info)
                if 'dimension' in modify_json and len(modify_json['dimension'])>0:
                    dims = modify_json['dimension'].split(',')
                    if len(dims) > 0:
                        need_update_dimn = [di for di in dims if di != '' and int(di) in app_code_id[appi]]
                        if len(need_update_dimn) > 0:
                            dims = [int(di) if int(di) not in app_code_id[appi] else app_code_id[appi][int(di)] for di
                                    in
                                    dims if di != '']
                            ndim = ',' + ','.join([str(x) for x in dims]) + ','
                            modify_json['dimension'] = ndim
                            update_ans_op_cnt += 1
                            cursor.execute(
                                'update ' + mysql_db + '.tbl_ans_audit_op set modify_info = %s where id = %s',
                                (json.dumps(modify_json), id))
        print('update ans op cnt :%s'%(update_ans_op_cnt))

        db.commit()
        db.close()
        print('update update_ans_online_cnt:%s' % (update_ans_online_cnt))

    except Exception as e:
        logging.exception(e)
        if db is not None: db.rollback()



def handler_all(app_id):
    """

    """

    handler_dimension_transform(app_id=app_id)
    return


if __name__ == '__main__':
    app_id = None
    print('mysql target server : %s, database : %s' % (mysql_host, mysql_db))
    start_time = time.time()

    try:
        handler_all(app_id)
    except Exception as err:
        print("error, skip this migration.")
        print(err)
        sys.exit()
    print('handler all cost time:%s' % (time.time() - start_time))


