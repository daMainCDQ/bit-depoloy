-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

alter table faq3.tbl_ans add COLUMN `parsers` varchar(2000) NULL COMMENT '答案相关解析器';
alter table faq3.tbl_ans_online add COLUMN `parsers` varchar(2000) NULL COMMENT '答案相关解析器';

-- +migrate Down
alter table faq3.tbl_ans drop `parsers` ;
alter table faq3.tbl_ans_online drop `parsers` ;