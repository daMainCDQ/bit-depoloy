-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
alter table faq3.tbl_ans modify `dimension` varchar(1024) COMMENT '维度id拼接';
alter table faq3.tbl_ans_online modify `dimension` varchar(1024) COMMENT '维度id拼接';


-- +migrate Down
alter table faq3.tbl_ans modify `dimension` varchar(60) COMMENT '维度id拼接';
alter table faq3.tbl_ans_online modify `dimension` varchar(60) COMMENT '维度id拼接';
