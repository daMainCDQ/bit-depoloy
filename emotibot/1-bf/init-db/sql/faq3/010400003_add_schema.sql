-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
alter table `tbl_ml_train_history` add `is_deleted` tinyint(4) DEFAULT 0 COMMENT '0:未删除，1:已删除';


-- +migrate Down
ALTER TABLE `tbl_ml_train_history` DROP `is_deleted`;