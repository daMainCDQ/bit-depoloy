-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied


START TRANSACTION;
USE `chat`;

alter table tbl_question  add    `topic` varchar(50) DEFAULT '' COMMENT '话题';


COMMIT;


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

alter table tbl_question  drop   `topic`;

