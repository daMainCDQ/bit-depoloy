-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied


USE `chat`;

ALTER  TABLE tbl_question ADD  INDEX idx_app (app_id);
ALTER  TABLE tbl_question ADD  INDEX idx_group_app (group_id,app_id);


ALTER TABLE tbl_answer ADD INDEX idx_app_group (group_id,app_id);
ALTER TABLE tbl_answer ADD INDEX idx_app (app_id);



-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

drop index idx_app on tbl_question;
drop index idx_group_app on tbl_question;

drop index idx_app on tbl_answer;
drop index idx_app_group on tbl_answer;

