
-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied



START TRANSACTION;

CREATE DATABASE IF NOT EXISTS `chat` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `chat`;

CREATE TABLE `tbl_question` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'auto increment id',
  `group_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '状态0（未变动）1插入，2 修改（非新增）-1 删除 ',
  `content` varchar(2000) NOT NULL DEFAULT '' COMMENT '标准问内容',
  `group_cnt` int(11) NOT NULL DEFAULT '0' COMMENT '统计标准问语料数',
  `create_user` varchar(64) NOT NULL DEFAULT '' COMMENT '创建用户',
  `create_time` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `update_user` varchar(64) NOT NULL DEFAULT '' COMMENT '更新用户',
  `update_time` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  `style_type` smallint(6) DEFAULT '0' COMMENT '风格',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=348 COMMENT='用户问';


CREATE TABLE `tbl_answer` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'auto increment id',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '状态0（未变动）1（）2 修改-1 删除',
  `dimension` varchar(64) DEFAULT NULL COMMENT '维度id拼接',
  `content` mediumtext COMMENT '答案',
  `create_user` varchar(64) NOT NULL DEFAULT '' COMMENT '创建用户',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_user` varchar(64) NOT NULL DEFAULT '' COMMENT '更新用户',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `start_time` datetime DEFAULT NULL COMMENT '答案生效时间',
  `end_time` datetime DEFAULT NULL COMMENT '答案失效时间',
  `style_type` smallint(6) DEFAULT '0' COMMENT '风格',
  `group_id` bigint(11) NOT NULL COMMENT '组ID',
  `period_type` smallint(6) DEFAULT '0' COMMENT '0,1:每天2，每月，3每年',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=146  COMMENT='答案表';

CREATE TABLE `tbl_upload_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(64) NOT NULL DEFAULT '',
  `file_path` varchar(4096) DEFAULT '',
  `rows` int(11) DEFAULT '0',
  `valid_rows` int(11) DEFAULT '0',
  `finish_rows` int(11) DEFAULT '0',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `file_name` varchar(60) DEFAULT '' COMMENT '上传文件名',
  `type` smallint(6) DEFAULT NULL COMMENT '0:全量，1:增量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15  COMMENT='上传历史记录表';

COMMIT;


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

DROP TABLE `tbl_question`;
DROP TABLE `tbl_answer`;
DROP TABLE `tbl_upload_history`;