-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied


START TRANSACTION;
USE `chat`;

alter table tbl_question  add    `app_id` varchar(50) NOT NULL DEFAULT '' COMMENT '机器人id';
alter table tbl_answer  add    `app_id` varchar(50) NOT NULL DEFAULT '' COMMENT '机器人id';
alter table tbl_upload_history  add    `app_id` varchar(50) NOT NULL DEFAULT '' COMMENT '机器人id';

alter table tbl_question  add    `mark_question` varchar(2000) NOT NULL DEFAULT '' COMMENT '标准问内容';

COMMIT;


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

alter table tbl_question  drop    `app_id`;
alter table tbl_answer  drop    `app_id`;
alter table tbl_upload_history  drop    `app_id`;

alter table tbl_question  drop    `mark_question`;
