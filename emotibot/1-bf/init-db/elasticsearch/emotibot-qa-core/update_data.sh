#!/bin/bash
set -e
if [[ ${#INIT_ES_HOST} == 0 ]]; then
	INIT_ES_HOST=172.17.0.1
fi
if [[ ${#INIT_ES_PORT} == 0 ]]; then
	INIT_ES_PORT=9200
fi
if [[ ${#INIT_ES_USER} == 0 ]]; then
	INIT_ES_USER=emotibot
fi
if [[ ${#INIT_ES_PASS} == 0 ]]; then
	INIT_ES_PASS=password
fi

INDEX_URL="http://$INIT_ES_USER:$INIT_ES_PASS@$INIT_ES_HOST:$INIT_ES_PORT/emotibot-qa-core"

#import data
tar Jxvf emotibot-qa-core.tar.xz

# for testing
if [[ ${#INIT_ES_QA_SIZE} != 0 ]]; then
    tail -n $INIT_ES_QA_SIZE emotibot-qa-core &> emotibot-qa-core-2
    mv emotibot-qa-core-2 emotibot-qa-core
fi
# delete all data
curl -X POST \
  $INDEX_URL/_delete_by_query \
  -H 'Content-Type: application/json' \
  -d '{
    "query": {
    	"term":{
    		"app_id":"system"
    	}
    }
}'

if [[ $? -ne 0 ]]; then
    echo "Failed to remove $INIT_ES_HOST:$INIT_ES_PORT data"
    exit -1
fi

elasticdump \
  --input=emotibot-qa-core \
  --output=$INDEX_URL \
  --limit=10000
