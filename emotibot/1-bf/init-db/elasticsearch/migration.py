
import requests
import os
import time
import json
import sys
import re

def insert_record(esUrl, template_ver, qaCoreVer):
    t = time.time()
    requests.post(esUrl + "/_doc", json={"date": int(t), "template_ver": template_ver, "es_emotibot_qa_ver": qaCoreVer})

def find_last_template_ver(esUrl):
    json = {
        "size": 1,
        "query": {
            "bool": {
                "filter": {
                    "exists": {
                        "field": "template_ver"
                    }
                }
            }
        },
        "sort": {
            "template_ver.keyword": "desc"
        }
    }
    r = requests.post(esUrl + "/_search", json=json)
    try:
        return int(r.json()['hits']['hits'][0]['sort'][0][:9])
    except :
        return 0

def find_last_emotibot_qa_core_ver(esUrl):
    json = {
        "size": 1,
        "query": {
            "bool": {
                "filter": {
                    "exists": {
                        "field": "es_emotibot_qa_ver"
                    }
                }
            }
        },
        "sort": {
            "es_emotibot_qa_ver.keyword": "desc"
        }
    }
    r = requests.post(esUrl + "/_search", json=json)
    try:
        return int(r.json()['hits']['hits'][0]['sort'][0][:9])
    except :
        return 0
def get_migration_emotibot_qa_core_name():
    fo = open('emotibot-qa-core/args.env', "r")
    tmp = fo.read().split("/")
    fo.close()
    return tmp[len(tmp)-1][:-1]

def put_index_template(esUrl, fileName):
    index_template = os.path.basename(fileName)[10: -5]
    fo = open(fileName, "rb")
    template_str = fo.read()
    template_json = json.loads(template_str)
    print("[put]: ES_URL/_template/" + index_template)
    headers = {'Content-Type' : 'application/json'}
    r = requests.put(esUrl + "_template/" + index_template, json=template_json)
    print("[result]: " +  str(r.json()))
    fo.close()

def main():
    esHost = os.environ.get('INIT_ES_HOST')
    esPort = os.environ.get('INIT_ES_PORT')
    esUser = os.environ.get('INIT_ES_USER')
    esPass = os.environ.get('INIT_ES_PASS')
    esMigrationIndex = "migration"
    esURL = "http://" + esUser + ":" + esPass + "@" + esHost + ":" + esPort + "/"
    print("[ES]: http://" + esHost + ":" + esPort)
    pattern = re.compile("^[0-9]{9}\w*.(py|sh|json)$")
    # =================================
    # template migration
    # =================================
    lastVersion = find_last_template_ver(esURL + esMigrationIndex)
    migrationNames = sorted(os.listdir('migration'))
    for migrationName in migrationNames :
        if pattern.match(migrationName) and int(migrationName[:9]) > lastVersion:
            if migrationName.endswith(".json"):
                put_index_template(esURL, 'migration/' + migrationName)
            elif migrationName.endswith(".sh"):
                print('Execute script: migration/' + migrationName)
                out = os.system('cd migration && bash ' + migrationName)
                if out != 0:
                    print ('Failed to execute script migration/' + migrationName)
                    sys.exit(-1)
            elif migrationName.endswith(".py"):
                print('Execute script: migration/' + migrationName)
                out = os.system('cd migration && python -u ' + migrationName)
                if out != 0:
                    print ('Failed to execute script migration/' + migrationName)
                    sys.exit(-1)
            insert_record(esURL + esMigrationIndex, migrationName, None)
        sys.stdout.flush()


    # =================================
    # emotibot-qa-core migration
    # =================================
    lastVersion = find_last_emotibot_qa_core_ver(esURL + esMigrationIndex)
    migrationQACoreFileName = get_migration_emotibot_qa_core_name()
    migrationVersion = int(migrationQACoreFileName[:9])
    if migrationVersion > lastVersion:
        print('Execute script: emotibot-qa-core/update_data.sh')
        out = os.system('cd emotibot-qa-core && bash update_data.sh')
        if out != 0:
            print ('Failed to execute script emotibot-qa-core/update_data.sh')
            sys.exit(-1)
        insert_record(esURL + esMigrationIndex, None, migrationQACoreFileName)

    print("Migration Done")

if __name__ == "__main__":
    main()