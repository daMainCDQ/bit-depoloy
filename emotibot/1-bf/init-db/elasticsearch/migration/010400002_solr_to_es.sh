#!/bin/bash


export ES_HOST=$INIT_ES_HOST
export ES_PORT=$INIT_ES_PORT
export SOLR_HOST=$INIT_SOLR_HOST
export SOLR_PORT=$INIT_SOLR_PORT
export LOG_LEVEL="ERROR"

i=1
while [[ i -le 3 ]] ;
do
  curl -sS $SOLR_HOST":"$SOLR_PORT &> /dev/null
  if [[ $? == 0 ]]; then
    solr-es-migration
    exit 0
  fi
  i=$((i+1));
  echo "Try to wait solr: $SOLR_HOST:$SOLR_PORT"
  sleep 2
done

echo "Not Found solr service, skip to execute solr-es-migration"
exit 0

