# -*- coding: UTF-8 -*- 
import os
import sys
import ui_menu
import json

DEPTH = 8

current_py_version = sys.version_info.major


# 获取目录下所有文件
def get_json_file(file_dir, suffix):
    list_json = []
    for root, dirs, files in os.walk(file_dir):
        # print(root) #当前目录路径
        # print(dirs) #当前路径下所有子目录
        # print(files) #当前路径下所有非目录子文件
        for file in files:
            if file.endswith(suffix):
                list_json.append(os.path.join(root, file))
    return list_json


def unicode_convert(json):
    if current_py_version >= 3:
        return json
    if isinstance(json, dict):
        return {unicode_convert(key): unicode_convert(value) for key, value in json.items()}
    elif isinstance(json, list):
        return [unicode_convert(element) for element in json]
    elif isinstance(json, unicode):
        return json.encode('utf-8')
    else:
        return json


# load file as json
def load_json_obj(file):
    with open(file) as f:
        json_obj = json.load(f, encoding='utf-8')
    return unicode_convert(json_obj)


# aggregate all dependency objects
def aggregate_dependencies(json_obj):
    all_dependencies = []
    if isinstance(json_obj, dict):
        for key, list_value in json_obj.items():
            if isinstance(list_value, list):
                for each in list_value:
                    all_dependencies.append(each)
    return all_dependencies


# retrieve all depends
def fetch_dependencies(parent, json_list):
    all_dependency = set()
    skip_dependency = set()
    ui_codes = set()
    if isinstance(json_list, list):
        for module in json_list:
            if module['name'].upper() == parent.upper():
                all_dependency.add(parent)
                if module.get('ui_codes'):
                    ui_codes = ui_codes.union(set(module['ui_codes']))
                if 'skip' in module:
                    if module['skip'] is True:
                        skip_dependency.add(module['name'])
                for sub_module in module['dependency']:
                    all_dependency.add(sub_module['name'])
    return all_dependency, skip_dependency, ui_codes


def get_dependeny_objs(file):
    return aggregate_dependencies(load_json_obj(file))


def get_dependencies_objs(files):
    all_dependencies = []
    for each in files:
        all_dependencies.extend(get_dependeny_objs(each))
    return all_dependencies


def get_all_options(dependencies):
    names = set()
    for dependency in dependencies:
        names.add(dependency.get('name'))
    return names


def dump2file(path, data):
    with open(path, "w") as f:
        f.write(data)


def append2file(path, data):
    with open(path, "a+") as f:
        f.write(data)


def generate_exclude_modules(filepath, include_modules):
    module_equals_map = ui_menu.module_equals
    for key in module_equals_map:
        module_equals_map[key].append(key)

    # 统一转为小写
    module_equals_map = {
        key.lower(): [
            value.lower()
            for value in module_equals_map[key]
        ]
        for key in module_equals_map}
    include_modules = [
        include_module.lower()
        for include_module in include_modules
    ]

    exclude_modules = []

    for module, module_alias in module_equals_map.items():
        is_include = len(list(filter(lambda x: x in module_alias, include_modules))) > 0
        if is_include is False:
            exclude_modules.append(module)

    content = "EXCLUDE_UI_MODULES='{}'".format(json.dumps(exclude_modules, ensure_ascii=False))
    with open(filepath, "w") as file:
        file.write(content)
        file.close()
