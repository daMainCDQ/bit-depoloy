# -*- coding: UTF-8 -*- 

import gen_common as common
import sys
import os

DEPTH = 8
RESULT_FILE_NAME = ".module_output.csv"
RESULT_FILE = RESULT_FILE_NAME
DEBUG_FILE_NAME = ".debug_output.log"

CONF_DEPENDENCY = "module.json"
EXCLUDE_UI_MODULES_ENV = ".ui-mini.dev"
CURRENT_DIR = os.path.dirname(os.path.os.path.realpath(__file__))
FILE_OUTPUT_PRE_PATH = os.path.join(CURRENT_DIR, '../../')
CONF_DIR = os.path.join(CURRENT_DIR, "../")
DEBUG_FILE = os.path.join(CURRENT_DIR, DEBUG_FILE_NAME)

input_args = sys.argv
dependency_files = common.get_json_file(CONF_DIR, CONF_DEPENDENCY)
depend_obj = common.get_dependencies_objs(dependency_files)
optional_names = common.get_all_options(depend_obj)
if type(input_args) == list and len(input_args) > 1:
    input_args.pop(0)
    illegal_args = set()
    for arg in input_args:
        if arg not in optional_names:
            illegal_args.add(arg)
    if len(illegal_args) > 0:
        print("error: argument 【{}】 not supported".format(", ".join(illegal_args)))
        sys.exit(-1)

else:
    print("error: required argument not found")
    sys.exit(-1)

debug_info = "find module dependency files ==>" + ','.join(dependency_files)
common.dump2file(DEBUG_FILE, debug_info)
# debug_info= "\nrequired modules ==>"+ ','.join(modules.MODULES)
debug_info = "\nrequired modules ==>" + ','.join(input_args)
common.append2file(DEBUG_FILE, debug_info)
all_sub_modules = set()
all_skip_modules = set()
used_ui_codes = set()
ancestors = input_args
for i in range(1, DEPTH + 1):  # DEPTH is to avoid circular dependency
    common.append2file(DEBUG_FILE, ('\nRound ' + str(i)))
    if len(ancestors) > 0:
        children = set()
        for ancestor in ancestors:
            cur_dependency, skip_dependency, ui_codes = common.fetch_dependencies(ancestor, depend_obj)
            used_ui_codes = used_ui_codes.union(ui_codes)
            common.append2file(DEBUG_FILE, ('\n\t' + ancestor + " depends on ==>" + ','.join(cur_dependency)))
            children.update(cur_dependency)
            all_sub_modules.update(cur_dependency)
            all_skip_modules.update(skip_dependency)
        ancestors = children
        common.append2file(DEBUG_FILE, ("\n\tnext ==>" + ','.join(children)))
for each in all_skip_modules:
    all_sub_modules.discard(each)
common.append2file(DEBUG_FILE,
                   ("\n\nall dependencies are ===>" + str(len(all_sub_modules)) + '个' + ','.join(all_sub_modules)))
common.dump2file(os.path.join(FILE_OUTPUT_PRE_PATH, RESULT_FILE), ','.join(all_sub_modules))
print('used ui menus: ' + ','.join(used_ui_codes))
common.generate_exclude_modules(os.path.join(FILE_OUTPUT_PRE_PATH, EXCLUDE_UI_MODULES_ENV), used_ui_codes)
print('done. Find ' + str(len(all_sub_modules)) + ' modules at output file ' + RESULT_FILE_NAME +
      "\nDetails see debug file " + DEBUG_FILE_NAME)
sys.exit(0)
