执行步骤：
1. 在modules.py里设置需要安装的功能。
2. 执行sh generate.sh 
  输出: 1. module_output.csv 是要安装的docker服务列表
        2. ui-mini.dev 是要隐藏的菜单文件。
注: 部署脚本会按照module_output.csv启动需要的服务。 ui-mini.dev 里的参数会传入到admin-auth模块，隐藏不需要显示的菜单。


其余文件详情：

1. gen_module.py
主运行文件，执行即可生成结果。 扫描CONF_DIR 下所有的目录的json文件, 输出结果文件csv结果文件 RESULT_FILE

2. gen_common.py
为基础公用解析及文件写入函数。

3. _module.json结尾的文件是依赖关系。 skip不作为输出模块。

4.ui_menu.py 设置了菜单code和功能模块的应对关系。 没有在modules.py里的模块，相应的菜单code都会被exclude。

5. 如果要新加独立产品模块部署， 例如质检，可新建目录PRODUCT_QIC, 目录下新建json文件 qic_module.json。（注意要以module.json作为后缀，不然扫描不到）

|SOLUTIONS|电话机器人
|PLATFORM|机器人平台
|PRODUCT|SSM
|FRAMEWORK|mlp
|MODEL|NER， ASR
|TOOL|License,媒体库
|INFRA|loadbalance

1. dependency.json
   以下为参考。Key分为 product, framework, model, tool，为模块层级
   每个item有name，dependency 分为layer_name（层级名）和name（模块名）
   {
    "product": [{
        "name": "faq",
        "dependency": [{
                "layer_name": "model",
                "name": "ssm-ui"
            },
            {
                "layer_name": "model",
                "name": "ssm-dac"
            },
            {
                "layer_name": "model",
                "name": "faq-module"
            }
        ]
    }], 
    "framework": [{
        "name": "mlp",
        "dependency": [
            {
                "layer_name": "model",
                "name": "ml-platform-online"
            },
            {
                "layer_name": "model",
                "name": "ml-platform-offline"
            }
        ]
    }
],
    "model":
        [{
                "name": "faq-module",
                "depenency":
                    [{
                            "layer_name": "model",
                            "name": "ssm-dac"
                        },
                        {
                            "layer_name": "tool",
                            "name": "simple-ft-service"
                        },
                        {
                            "layer_name": "model",
                            "name": "rc-word2vec"
                        },
                        {
                            "layer_name": "model",
                            "name": "ssm-ui"
                        }
                    ]
            }, 
            {
                "name": "ssm-dac",
                "dependency":
                    [{
                        "layer_name": "tool",
                        "name": "snlu"
                    }, {
                        "layer_name": "tool",
                        "name": "simple-ft-service"
                    }, {
                        "layer_name": "model",
                        "name": "faq-module"
                    }]
            }, 
            {
                "name": "ssm-ui",
                "dependency": 
                    [{
                        "layer_name": "model",
                        "name": "ssm-dac"
                    }, {
                        "layer_name": "model",
                        "name": "admin-ui"
                    }]
            },
            {
                "name": "admin-service",
                "dependency": []
            },
            {
                "name": "rc-word2vec",
                "dependency": [{
                    "layer_name": "tool",
                    "name": "snlu"
                }]
            }
   ],
    "tool":
        [{
                "name": "snlu",
                "dependency": []
            },
            {
                "name": "simple-ft-service",
                "dependency": []
            }
        ]
   }
   2. custom_config.json为env变量设置
   source为env和module
   env代表覆盖dev.env中参数
   module代表覆盖yaml中模块环境变量
 [
    {
        "source": "env",
        "key_name": "SNLU_NUM_WORKER_THREAD",
        "value": "10",
        "description": "snlu thread num"
    }, 
    { 
        "source": "env",
          "key_name": "SNLU_JVM_OPTS",
        "value": "-Xms2g -Xmx8g",
        "description": "snlu java heap "
    },
    {
        "source": "module",
        "key_name": "rc-word2vec",
        "environment_key": "XEONKG_JVM_OPTS",
        "value": "-Xms1g -Xmx2g",
        "description": "xeon-kg heap "
}
]
