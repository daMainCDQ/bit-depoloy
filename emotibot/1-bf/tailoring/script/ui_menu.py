# -*- coding: UTF-8 -*-
menus = [
    {
        "code": "statistic_dash",
        "name": "数据总览"
    },
    {
        "code": "ccs_config",
        "name": "对话管理"
    },
    {
        "code": "ssm3",
        "name": "问答管理"
    },
    {
        "code": "task_engine",
        "name": "任务引擎"
    },
    {
        "code": "skills",
        "name": "机器人技能"
    },
    {
        "code": "intent_manage",
        "name": "意图引擎"
    },
    {
        "code": "personas",
        "name": "用户画像"
    },
    {
        "code": "emotion_manage",
        "name": "情绪引擎"
    },
    {
        "code": "kg_data_manage",
        "name": "知识推理引擎"
    },
    {
        "code": "robot_chat",
        "name": "机器人聊天"
    },
    {
        "code": "ner_factory",
        "name": "文本解析器"
    },
    {
        "code": "wordbank",
        "name": "词库"
    },
    {
        "code": "material_library",
        "name": "多媒体库"
    },
    {
        "code": "statistic_daily",
        "name": "对话数据"
    },
    {
        "code": "robot_config",
        "name": "机器人设置"
    },
    {
        "code": "mlp",
        "name": "机器学习平台"
    },
    {
        "code": "ai_tools",
        "name": "AI工具"
    }
]

module_equals = {
    "ssm3": ["faq", "faq-lite"],
    "task_engine": ["taskengine", "te"],
    "kg_data_manage": ["kg"],
    "intent_manage": ["intent"],
    "ner_factory": ["ner"],
    "robot_chat": ["chat"],
    "skills": ["skill"],
    "ccs_config": ["ccs"]
}

for menu in menus:
    code = menu['code']
    if code in module_equals:
        module_equals[code].append(code)
    else:
        module_equals[code] = [code]

# - ADMIN_AUTH_EXCLUDE_MODULES=["task_engine"]
# http://172.16.103.9/auth/v3/modules
# 任意环境打这个接口~
