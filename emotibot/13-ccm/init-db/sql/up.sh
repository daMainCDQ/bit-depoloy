#!/bin/bash
set -e 


DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
env=$1

until /usr/bin/mysql \
  -h "${INIT_MYSQL_HOST}" \
  -P "${INIT_MYSQL_PORT}" \
  --user=${INIT_MYSQL_USER} \
  --password=${INIT_MYSQL_PASSWORD} \
  --execute="SELECT 1;" \
  &>/dev/null; do
    echo "wait mysql"
    sleep 3
done

bash -c "$DIR/make.sh $env"

grep -v '^#' "$DIR/DB_LIST" | while IFS= read -r DB_NAME
do
    cmd="sql-migrate up -config=$DIR/$DB_NAME.yml --env $env "
    echo $cmd
    eval $cmd
done
