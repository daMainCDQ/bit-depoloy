-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
USE `ccm`;

START TRANSACTION;

-- ----------------------------
-- Table structure for ccm_balance
-- ----------------------------
create TABLE `ccm_balance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `code` varchar(50) NOT NULL COMMENT '分流策略编码',
  `name` varchar(50) NOT NULL COMMENT '分流策略名称',
  `created_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新时间',
  `active` tinyint(1) DEFAULT 1 COMMENT '是否激活：0-废弃；1-激活',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='分流策略表';

-- ----------------------------
-- Table structure for ccm_mapping_service
-- ----------------------------
create TABLE `ccm_mapping_service` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `mapping_id` bigint(20) NOT NULL COMMENT '映射条件id',
  `service_id` bigint(20) NOT NULL COMMENT '对应服务Id',
  `ratio` tinyint(5) DEFAULT 100 COMMENT '比例',
  PRIMARY KEY (`id`),
  INDEX `mappingid_index` (mapping_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='策略服务映射表';

INSERT INTO `ccm_balance`(`id`, `code`, `name`, `created_time`, `updated_time`, `active`) VALUES (1, 'ALL_LOAD_BALANCE', '全流量', '2020-07-14 17:09:48', '2020-07-14 17:09:48', 1);
INSERT INTO `ccm_balance`(`id`, `code`, `name`, `created_time`, `updated_time`, `active`) VALUES (2, 'WEIGHTED_LOAD_BALANCE', '分流量', '2020-07-14 17:09:48', '2020-07-14 17:09:48', 1);


insert into ccm_mapping_service(mapping_id,service_id) select id,service_id from ccm_mapping;

alter table ccm_mapping drop column `service_id`;
alter table ccm_mapping drop column `service_type`;
alter table ccm_mapping add `balance_id` bigint(20) NOT NULL COMMENT '分流策略Id';
alter table ccm_config add `service_flow` varchar(50) DEFAULT 'ASR,ROBOT,TTS' COMMENT '服务调用流程';

update ccm_mapping set balance_id = 1;
update ccm_config set service_flow = 'ASR,ROBOT,TTS';

commit;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back