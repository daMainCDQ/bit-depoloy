-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
USE `ccm`;

START TRANSACTION;

CREATE TABLE IF NOT EXISTS `ccm`.`ccm_line_record` (
  `id` int NOT NULL AUTO_INCREMENT,
  `session_id` varchar(50) DEFAULT NULL COMMENT '外部会话ID',
  `app_id` varchar(100) DEFAULT NULL,
  `channel` varchar(20) DEFAULT NULL,
  `start_time` varchar(20) DEFAULT NULL,
  `end_time` varchar(20) DEFAULT NULL,
  `user_id` varchar(100) DEFAULT NULL,
  `user_name` varchar(200) DEFAULT NULL,
  `staff_start_time` varchar(20) DEFAULT NULL,
  `staff_id` varchar(100) DEFAULT NULL,
  `staff_name` varchar(200) DEFAULT NULL,
  `close_type` varchar(20) DEFAULT NULL COMMENT '会话关闭类型',
  `satisfaction` varchar(20) DEFAULT NULL,
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `page_id` varchar(50) DEFAULT NULL,
  `enter_id` varchar(50) DEFAULT NULL,
  INDEX  (`session_id`),
  INDEX  (`user_id`),
  INDEX  (`start_time`),
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `ccm`.`ccm_line_record_detail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `json_info` json comment 'json数据',
  `line_id` varchar(50) NOT NULL,
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` varchar(100) COMMENT '用户ID',
  `content` varchar(1000)  COMMENT '发送/响应内容',
  `datas` varchar(2000)  COMMENT '回复扩展信息',
  `match_question` varchar(400) COMMENT '标准问',
  `role` varchar(20) COMMENT '发送方',
  `type` varchar(50) COMMENT '消息类型',
  `channel` varchar(50) COMMENT '渠道',
  `module` varchar(50) COMMENT '命中模块',
  `uuid` varchar(50) COMMENT '对话ID',
  INDEX  (`line_id`),
  INDEX  (`user_id`),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `ccm`.`ccm_feedback_record` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键 自增',
  `session_id` varchar(50) DEFAULT NULL COMMENT '会话ID',
  `user_id` varchar(100) DEFAULT NULL COMMENT '用户ID',
  `uuid` varchar(100) DEFAULT NULL COMMENT '对话唯一标识ID',
  `content` varchar(20) DEFAULT NULL COMMENT '反馈类型',
  `feedback_time` varchar(20) DEFAULT NULL COMMENT '反馈时间 YYYY-MM-DD HH:mm:ss',
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '记录创建时间',
  INDEX  (`session_id`),
  INDEX  (`user_id`),
  INDEX  (`uuid`),
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `ccm`.`ccm_human_record_detail` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键 自增',
  `session_id` varchar(50) DEFAULT NULL COMMENT '会话ID',
  `user_id` varchar(100) DEFAULT NULL COMMENT '用户ID',
  `type` INT DEFAULT NULL COMMENT '转人工类型',
  `human_time` varchar(20) DEFAULT NULL COMMENT '转人工时间 YYYY-MM-DD HH:mm:ss',
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '记录创建时间',
  INDEX  (`session_id`),
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


create unique index index_t_feedback_record_uuid on `ccm`.`ccm_feedback_record`(uuid);


-- +migrate Down
drop index index_t_feedback_record_uuid on `ccm`.`ccm_feedback_record`;


commit;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back