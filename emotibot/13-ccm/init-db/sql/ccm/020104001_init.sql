-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
USE `ccm`;

START TRANSACTION;
-- ----------------------------
-- Table structure for ccm_config
-- ----------------------------
CREATE TABLE `ccm_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) DEFAULT NULL COMMENT '配置中文名称',
  `config_id` varchar(50) NOT NULL COMMENT '配置规则ID',
  `created_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新时间',
  `active` tinyint(1) DEFAULT NULL COMMENT '是否激活',
  `enterprise_id` varchar(50) DEFAULT NULL COMMENT '企业Id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='配置策略表';

-- ----------------------------
-- Table structure for ccm_mapping
-- ----------------------------
CREATE TABLE `ccm_mapping` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `priority` int(11) DEFAULT NULL COMMENT '优先级',
  `service_id` bigint(20) NOT NULL COMMENT '对应服务Id',
  `service_type` varchar(20) NOT NULL COMMENT '类型',
  `config_id` varchar(50) DEFAULT NULL COMMENT '对应的中控配置id',
  `mapping_list_id` bigint(20) DEFAULT NULL COMMENT '对应的映射列表配置id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='配置策略映射表';

-- ----------------------------
-- Table structure for ccm_mapping_condition
-- ----------------------------
CREATE TABLE `ccm_mapping_condition` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) NOT NULL COMMENT '条件名称',
  `rule` varchar(5) DEFAULT NULL COMMENT '过滤条件 等于 eq 不等于 ne',
  `mapping_id` bigint(20) NOT NULL COMMENT '映射条件id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='配置策略映射条件表';

-- ----------------------------
-- Table structure for ccm_mapping_condition_item
-- ----------------------------
CREATE TABLE `ccm_mapping_condition_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `mapping_condition_id` bigint(20) NOT NULL COMMENT '映射条件表Id',
  `item_value` varchar(255) NOT NULL COMMENT '过滤条件具体的值',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='配置策略映射条件元素表';

-- ----------------------------
-- Table structure for ccm_mapping_list
-- ----------------------------
CREATE TABLE `ccm_mapping_list` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) DEFAULT NULL COMMENT '策略名称',
  `index` int(11) DEFAULT NULL COMMENT '调用顺序',
  `service_type` varchar(255) DEFAULT NULL COMMENT '服务类型 ASR/TTS/IM/ROBOT',
  `config_id` bigint(20) NOT NULL COMMENT '对应的中控配置id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for ccm_service
-- ----------------------------
CREATE TABLE `ccm_service` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) DEFAULT NULL COMMENT '服务中文名',
  `service_type` varchar(20) NOT NULL COMMENT '类型',
  `url` varchar(255) DEFAULT NULL COMMENT '服务地址',
  `supplier` varchar(20) DEFAULT NULL COMMENT '提供服的厂商（Ali,emotibot,ifly）',
  `protocol_type` varchar(10) DEFAULT NULL COMMENT '协议类型',
  `created_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新时间',
  `enterprise_id` varchar(50) DEFAULT NULL COMMENT '企业Id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='配置策略服务表';

-- ----------------------------
-- Table structure for ccm_service_invoke_log
-- ----------------------------
CREATE TABLE `ccm_service_invoke_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `unique_id` varchar(50) DEFAULT NULL COMMENT '会话唯一Id',
  `user_id` varchar(50) DEFAULT NULL COMMENT '用户Id',
  `log_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '日志记录时间',
  `duration` int(11) DEFAULT NULL COMMENT '耗时(单位毫秒ms)',
  `service_type` varchar(20) DEFAULT NULL COMMENT '服务类型（TTS/ASR/IM...）',
  `service_url` varchar(255) DEFAULT NULL COMMENT '服务地址',
  `result` varchar(20) DEFAULT NULL COMMENT '调用结果（success/fail）',
  `enterprise_id` varchar(50) DEFAULT NULL COMMENT '服务归属企业Id',
  PRIMARY KEY (`id`),
  KEY `idx_unique_id` (`unique_id`),
  KEY `idx_start_time` (`log_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for ccm_service_param
-- ----------------------------
CREATE TABLE `ccm_service_param` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `service_id` bigint(20) NOT NULL COMMENT '对应服务Id',
  `param_name` varchar(255) NOT NULL COMMENT '参数名称',
  `param_value` varchar(255) DEFAULT NULL COMMENT '参数值',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='配置策略服务配置表';

COMMIT;