package utils

import (
	"fmt"
	"strings"

	"emotibot.com/solr-es-migration/data"
	"emotibot.com/solr-es-migration/elasticsearch"
	"emotibot.com/solr-es-migration/logger"
	"emotibot.com/solr-es-migration/solr"
)

func SolrToESQACore(docs []*solr.QACoreDoc, collection string,
	database string) ([]*elasticsearch.QACoreDoc, error) {

    var moduleNameMap = map[string]string {
        "editorial": "editorial",
        "robot_profile": "robot",
        "robot": "robot_custom",
        "other": "editorial_custom",
    }
    // metadata is [appid, module, domain]
	var metadata []string
	if collection == data.COLLECTION_MERGE_6_25 {
        if strings.HasPrefix(database, "domain_chat_") {
            metadata = []string{"system", "editorial_domain", strings.Replace(database, "domain_chat_", "", 1)}
        } else {
            newDatabase, exist := moduleNameMap[database]
            if exist {
		        metadata = []string{"system", newDatabase, ""}
            } else {
		        metadata = []string{"system", database, ""}
            }
        }
	} else {
        tmpMetadata := strings.Split(database, "_")
        newDatabase, exist := moduleNameMap[tmpMetadata[1]]
        if exist {
            metadata = []string{tmpMetadata[0], newDatabase, ""}
        } else {
            metadata = []string{tmpMetadata[0], strings.Replace(tmpMetadata[1], "#", "_", -1), ""}
        }
	}
    if len(metadata) != 3 {
		return nil, fmt.Errorf("Invalid database schema: %s", database)
	}

	logger.Info.Printf("Converting %d Solr documents to Elasticsearch format", len(docs))
	esDocs := []*elasticsearch.QACoreDoc{}

	for _, doc := range docs {
		docId := doc.ID
        if metadata[1] == "robot_custom" {
			docId = strings.Replace(docId, "robot", "robot_custom", -1)
		}
		if metadata[1] == "editorial_custom" {
			docId = strings.Replace(docId, "other", "editorial_custom", -1)
		}
		var answers []*elasticsearch.Answer

		if doc.RelatedSentences != nil {
			answers = make([]*elasticsearch.Answer, len(doc.RelatedSentences))

			for i, relatedSentence := range doc.RelatedSentences {
				if relatedSentence.Answer != "" {
					answer := elasticsearch.Answer{}
					answer.Sentence = relatedSentence.Answer
					answer.Creator = relatedSentence.Creator

					if relatedSentence.CU != nil {
						emotions := []*elasticsearch.Emotion{}
						for _, e := range relatedSentence.CU.Emotion.Res {
							emotion := elasticsearch.Emotion{
								Label: e.Item,
								Score: e.Score,
							}
							emotions = append(emotions, &emotion)
						}
						answer.Emotions = emotions

						topics := []*elasticsearch.Topic{}
						for _, t := range relatedSentence.CU.Topic.Res {
							topic := elasticsearch.Topic{
								Label: t.Item,
								Score: t.Score,
							}
							topics = append(topics, &topic)
						}
						answer.Topics = topics

						speechActs := []*elasticsearch.SpeechAct{}
						for _, s := range relatedSentence.CU.SpeechAct.Res {
							speechAct := elasticsearch.SpeechAct{
								Label: s.Item,
								Score: s.Score,
							}
							speechActs = append(speechActs, &speechAct)
						}
						answer.SpeechActs = speechActs
					}

					answers[i] = &answer
				}
			}
		}

		var autofillEnabled bool
		if doc.AutofillEnabled == nil {
			autofillEnabled = true
		} else {
			autofillEnabled = *doc.AutofillEnabled
		}

		esDoc := elasticsearch.QACoreDoc{
			DocID:           docId,
			AppID:           metadata[0],
			Module:          metadata[1],
			Domain:          metadata[2],
			Answers:         answers,
			Sentence:        doc.Sentence,
			SentenceOrig:    doc.SentenceOrig,
			SentenceType:    doc.SentenceType,
			SentencePos:     doc.SentencePos,
			SentenceKeywords:doc.SentenceKeywords,
			Source:          doc.Source,
			AutofillEnabled: autofillEnabled,
		}

		sentenceCU := doc.SentenceCU
		if sentenceCU != nil {
			emotions := []*elasticsearch.Emotion{}
			for _, e := range sentenceCU.Emotion.Res {
				emotion := elasticsearch.Emotion{
					Label: e.Item,
					Score: e.Score,
				}
				emotions = append(emotions, &emotion)
			}
			esDoc.Emotions = emotions

			topics := []*elasticsearch.Topic{}
			for _, t := range sentenceCU.Topic.Res {
				topic := elasticsearch.Topic{
					Label: t.Item,
					Score: t.Score,
				}
				topics = append(topics, &topic)
			}
			esDoc.Topics = topics

			speechActs := []*elasticsearch.SpeechAct{}
			for _, s := range sentenceCU.SpeechAct.Res {
				speechAct := elasticsearch.SpeechAct{
					Label: s.Item,
					Score: s.Score,
				}
				speechActs = append(speechActs, &speechAct)
			}
			esDoc.SpeechActs = speechActs
		}

		logger.Debug.Printf("Converted document: %+v\n", esDoc)

		esDocs = append(esDocs, &esDoc)
	}

	return esDocs, nil
}
