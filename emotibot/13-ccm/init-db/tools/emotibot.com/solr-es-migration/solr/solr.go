package solr

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"

	"emotibot.com/solr-es-migration/data"
	"emotibot.com/solr-es-migration/logger"
)

var solrBaseURL string

func Setup() error {
	logger.Info.Println("Initializing Solr client...")
	return initSolr()
}

func initSolr() error {
	solrHost := os.Getenv("SOLR_HOST")
	solrPort := os.Getenv("SOLR_PORT")

	if solrHost == "" {
		logger.Error.Println("SOLR_HOST env not specified")
		return data.ErrMissingEnv
	}

	if solrHost == "" {
		logger.Error.Println("SOLR_PORT env not specified")
		return data.ErrMissingEnv
	}

	logger.Info.Printf("SOLR_HOST: %s, SOLR_PORT: %s", solrHost, solrPort)
	solrBaseURL = fmt.Sprintf("http://%s:%s/solr", solrHost, solrPort)

	return nil
}

func GetDatabaseTerms(collection string) ([]string, error) {
	logger.Info.Printf("Getting database terms of collection: %s", collection)

	v := url.Values{}
	v.Set("fl", "database")
	v.Set("wt", "json")
	v.Set("numTerms", "99999")

	solrURL := fmt.Sprintf("%s/%s/admin/luke?%s", solrBaseURL, collection, v.Encode())
	logger.Debug.Println(solrURL)

	client := http.Client{}
	resp, err := client.Get(solrURL)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	lukeResp := LukeResponse{}

	jsonBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(jsonBody, &lukeResp)
	if err != nil {
		return nil, err
	}

	terms := make([]string, lukeResp.Fields.Database.Distinct)

	for i, term := range lukeResp.Fields.Database.TopTerms {
		if i%2 == 0 {
			terms[i/2] = term.(string)
		}
	}

	return terms, nil
}

func DumpAllData(collection string, database string, cursorMark string,
	limit int) (docs []*QACoreDoc, nextCursorMark string, err error) {
	logger.Info.Printf("Dumping database: %s data", database)

	query := fmt.Sprintf("database:%s AND !type:\"a\"", database)

	v := url.Values{}
	v.Set("q", query)
	v.Set("sort", "id desc")
	v.Set("rows", "10000")
	v.Set("wt", "json")
	v.Set("cursorMark", cursorMark)

	docs = []*QACoreDoc{}

	for {
		solrURL := fmt.Sprintf("%s/%s/select?%s", solrBaseURL, collection, v.Encode())
		logger.Debug.Printf("Solr request url: %s", solrURL)

		resp, err := http.Get(solrURL)
		if err != nil {
			logger.Error.Println(err.Error())
			return nil, "", err
		}
		defer resp.Body.Close()

		jsonBody, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, "", err
		}

		var cursorResp CursorResponse
		err = json.Unmarshal(jsonBody, &cursorResp)
		if err != nil {
			return nil, "", err
		}

		for _, doc := range cursorResp.Response.Docs {
			logger.Debug.Printf("doc: %+v\n", doc)
			docs = append(docs, doc)
		}

		if cursorResp.NextCursorMark == v.Get("cursorMark") {
			// Reach the end of cursor.
			return docs, "", nil
		}

		if len(docs) >= limit {
			// Reach the limitation.
			return docs, cursorResp.NextCursorMark, nil
		}

		v.Set("cursorMark", cursorResp.NextCursorMark)
	}
}
