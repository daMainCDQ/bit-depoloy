import requests
import os
import io
import time
import json
import sys
import re
from minio import Minio
from minio.error import (ResponseError, BucketAlreadyOwnedByYou, BucketAlreadyExists, NoSuchKey)

def insert_record(minioUrl, accessKey, secretKey, secure, bucketName, versionFileName, scriptFileName):
    t = time.time()
    minioClient = Minio(minioUrl, access_key=accessKey, secret_key=secretKey, secure=secure)
    insertJson = []
    try:
        versionFile = minioClient.get_object(bucketName, versionFileName)
        insertJson = json.loads(versionFile.data)
    except NoSuchKey as err:
        print("noSuchkey")
    except ResponseError as err:
        print(err)
        raise err
    
    try:
        insertJson.append({"date": int(t), "script_ver": scriptFileName})
        data = json.dumps(insertJson)
        f = io.BytesIO(data.encode('utf-8'))
        minioClient.put_object(bucketName, versionFileName, f, len(data))
        f.close()
    except ResponseError as err:
        print(err)

def find_last_script_ver(minioUrl, accessKey, secretKey, secure, bucketName, versionFileName):
    try:
        minioClient = Minio(minioUrl, access_key=accessKey, secret_key=secretKey, secure=secure)
        versionFile = minioClient.get_object(bucketName, versionFileName)
        insertJson = json.loads(versionFile.data)
        return int(insertJson[len(insertJson) - 1]['script_ver'][:9])
    except NoSuchKey as err:
        return 0
    except ResponseError as err:
        print(err)
        raise


def main():
    minioHost = os.environ.get('INIT_MINIO_HOST')
    minioPort = os.environ.get('INIT_MINIO_PORT')
    accessKey = os.environ.get('INIT_MINIO_ACCESS_KEY')
    secretKey = os.environ.get('INIT_MINIO_SECRET_KEY')
    secureStr = os.environ.get('INIT_MINIO_SECURE')
    bucketName = os.environ.get('INIT_MINIO_BUCKET_MIGRATION')
    secure = False
    if 'true' == secureStr.lower():
        secure = True
    minioUrl = minioHost + ":" + minioPort
    
    versionFile = 'module_version'

    lastVersion = find_last_script_ver(minioUrl, accessKey, secretKey, secure, bucketName, versionFile)

    migrationNames = sorted(os.listdir('migration'))
    pattern = re.compile("^[0-9]{9}\w*.(py|sh)$")

    for migrationName in migrationNames :
        if pattern.match(migrationName) and int(migrationName[:9]) > lastVersion:
            if migrationName.endswith('.py'):
                print('Execute python: migration/' + migrationName)
                out = os.system('cd migration && python -u ' + migrationName)
                if out != 0:
                    print ('Failed to execute script migration/' + migrationName)
                    sys.exit(-1)
            elif migrationName.endswith('.sh'):
                print('Execute script: migration/' + migrationName)
                out = os.system('cd migration && bash ' + migrationName)
                if out != 0:
                    print ('Failed to execute script migration/' + migrationName)
                    sys.exit(-1)
            insert_record(minioUrl, accessKey, secretKey, secure, bucketName, versionFile, migrationName)
        sys.stdout.flush()
    print("Migration Done")
if __name__ == "__main__":
    main()
