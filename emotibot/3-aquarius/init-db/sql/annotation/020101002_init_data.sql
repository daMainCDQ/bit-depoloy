-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

USE `annotation`;

START TRANSACTION;
insert IGNORE into annotation.role(`id`,`role_name`,`description`) values (6,'标注员','annotator');
insert IGNORE into annotation.role(`id`,`role_name`,`description`) values (7,'质检员','inspector');
insert IGNORE into annotation.role(`id`,`role_name`,`description`) values (8,'管理员','manager');
insert IGNORE into annotation.role(`id`,`role_name`,`description`) values (9,'抽检员','sampler');


insert into api_resource(`id`,`name`,`to_module`,`to_route`,`api_url`,`cmd`) values (3,'标注任务','annotation','tasks','','list');
insert into api_resource(`id`,`name`,`to_module`,`to_route`,`api_url`,`cmd`) values (4,'标签管理','annotation','rulelist','','list');

insert into api_resource(`id`,`name`,`to_module`,`to_route`,`api_url`,`cmd`) values (5,'人员总览','annotation','personnel','','list');
insert into api_resource(`id`,`name`,`to_module`,`to_route`,`api_url`,`cmd`) values (6,'文本管理','annotation','textcenter','','list');
insert into api_resource(`id`,`name`,`to_module`,`to_route`,`api_url`,`cmd`) values (7,'任务管理','annotation','taskcenter','','list');
insert into api_resource(`id`,`name`,`to_module`,`to_route`,`api_url`,`cmd`) values (8,'抽检任务','annotation','samplecenter','','list');
insert into api_resource(`id`,`name`,`to_module`,`to_route`,`api_url`,`cmd`) values (9,'质检任务','annotation','qccenter','','list');
insert into api_resource(`id`,`name`,`to_module`,`to_route`,`api_url`,`cmd`) values (10,'标注任务','annotation','annocenter','','list');
insert into api_resource(`id`,`name`,`to_module`,`to_route`,`api_url`,`cmd`) values (11,'数据统计','annotation','statistics','','list');


insert into role_resource(`role_id`,`resource_id`) values (6,10);
insert into role_resource(`role_id`,`resource_id`) values (7,9);
insert into role_resource(`role_id`,`resource_id`) values (9,8);

insert into role_resource(`role_id`,`resource_id`) values (8,5);
insert into role_resource(`role_id`,`resource_id`) values (8,6);
insert into role_resource(`role_id`,`resource_id`) values (8,7);
insert into role_resource(`role_id`,`resource_id`) values (8,11);



insert into enterprise(`org_id`,`uuid`,`name`,`parent_id`,`status`) values (1,'bb3e3925f0ad11e7bd860242ac120003','emotibot',0,1);
insert into user(`user_id`,`user_name`) values (1,'csbotadmin');
insert into user_role_ent(`user_id`,`ent_id`,`role_id`) values(1,1,8);
COMMIT;


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
TRUNCATE TABLE annotation.role;
TRUNCATE TABLE annotation.api_resource;
TRUNCATE TABLE annotation.role_resource;
TRUNCATE TABLE annotation.enterprise;
TRUNCATE TABLE annotation.user;
TRUNCATE TABLE annotation.user_role_ent;
