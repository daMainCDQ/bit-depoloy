-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

alter table annotation.enterprise modify `name` varchar(200) NOT NULL;
alter table annotation.user modify `user_name` varchar(200) NOT NULL;


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back

