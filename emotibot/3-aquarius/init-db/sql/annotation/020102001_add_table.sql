-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

USE `annotation`;

START TRANSACTION;
DROP TABLE IF EXISTS `rule_item_rel`;
CREATE TABLE `rule_item_rel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rule_id` int(11) NOT NULL,
  `rel_name` varchar(45) NOT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `source_rule_item_id` int(11) DEFAULT NULL,
  `target_rule_item_id` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=utf8mb4 comment '关系抽取';


COMMIT;


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE annotation.rule_item_rel;

