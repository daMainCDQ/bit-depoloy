-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied

CREATE DATABASE IF NOT EXISTS `annotation` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `annotation`;

START TRANSACTION;
DROP TABLE IF EXISTS `api_resource`;
CREATE TABLE `api_resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `to_module` varchar(45) NOT NULL,
  `api_url` varchar(100) DEFAULT NULL,
  `to_route` varchar(100) DEFAULT NULL,
  `cmd` varchar(100) DEFAULT NULL,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;



DROP TABLE IF EXISTS `check_task`;
CREATE TABLE `check_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label_task_id` int(11) DEFAULT NULL,
  `task_id` int(11) DEFAULT NULL,
  `task_item_id` int(11) DEFAULT NULL,
  `check_user_id` int(11) DEFAULT NULL COMMENT '标注者',
  `check_status` smallint(6) DEFAULT '0' COMMENT '抽检状态 1:不合格，2:合格',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `classify_item`;
CREATE TABLE `classify_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rule_id` int(11) NOT NULL,
  `classify_name` varchar(45) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `create_user` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=utf8mb4;



DROP TABLE IF EXISTS `enterprise`;

CREATE TABLE `enterprise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `org_id` int(11) NOT NULL,
  `uuid` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0:disable,1:active',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `org_id` (`org_id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `enterprise_role`;

CREATE TABLE `enterprise_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enterprise_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `enterprise_user`;

CREATE TABLE `enterprise_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enterprise_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(45) NOT NULL,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `label_task`;

CREATE TABLE `label_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL,
  `task_item_id` int(11) DEFAULT NULL,
  `label_status` smallint(6) DEFAULT '0' COMMENT '标注状态',
  `label_user_id` int(11) DEFAULT NULL COMMENT '标注者',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `audit_user_id` int(11) DEFAULT NULL COMMENT '质检者',
  `audit_status` smallint(6) DEFAULT '0' COMMENT '质检状态',
  `audit_comment` varchar(45) DEFAULT NULL,
  `label_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `audit_time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `task_item_id` (`task_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=250 DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(45) NOT NULL,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `description` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `role_resource`;

CREATE TABLE `role_resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `resource_id` int(11) NOT NULL,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `rule`;

CREATE TABLE `rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `create_user` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `dep_id` int(11) DEFAULT NULL,
  `description` varchar(300) DEFAULT NULL,
   rule_type smallint default 1 comment '规则类型1：普通2相似度',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `rule_item`;

CREATE TABLE `rule_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rule_id` int(11) unsigned NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `contraint` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL COMMENT 'single,array',
  `code` varchar(45) DEFAULT NULL COMMENT 'label.text,label.start,label.end',
  `format` varchar(45) DEFAULT NULL COMMENT 'INT,FLOAT,STRING,TIME',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `color` varchar(30) DEFAULT NULL COMMENT '颜色',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=200 DEFAULT CHARSET=utf8mb4;



DROP TABLE IF EXISTS `rule_item_result`;

CREATE TABLE `rule_item_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label_task_id` int(11) DEFAULT NULL,
  `json_value` text,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `task_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `label_task_id` (`label_task_id`)
) ENGINE=InnoDB AUTO_INCREMENT=186 DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `spot_check`;

CREATE TABLE `spot_check` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL,
  `type` smallint(6) DEFAULT '0' COMMENT '抽检类型，1:按百分比例抽，2:按数量抽检',
  `status` smallint(6) DEFAULT '0' COMMENT '抽检状态0: 未开始  1：未完成，2，完成',
  `num` int(11) DEFAULT NULL,
  `pass_rate` int(11) DEFAULT NULL COMMENT '合格率千分率',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `task`;

CREATE TABLE `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `status` smallint(6) DEFAULT '0' COMMENT '状态',
  `rule_id` int(11) DEFAULT NULL,
  `create_user` varchar(45) DEFAULT NULL,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `description` varchar(100) DEFAULT NULL,
  `test_id` int(11) DEFAULT NULL,
  `dep_id` int(11) DEFAULT NULL,
  `create_user_name` varchar(100) DEFAULT NULL,
  `dep_name` varchar(100) DEFAULT NULL,
  `rule_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `task_audit_assign`;

CREATE TABLE `task_audit_assign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `task_check_assign`;

CREATE TABLE `task_check_assign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `task_item`;

CREATE TABLE `task_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL,
  `file_location` varchar(100) DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `create_user` varchar(45) DEFAULT NULL,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `type` varchar(45) CHARACTER SET ascii DEFAULT NULL COMMENT 'File,String',
  `content` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=274 DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `task_label_assign`;

CREATE TABLE `task_label_assign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(45) NOT NULL,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3436 DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `user_role_ent`;

CREATE TABLE `user_role_ent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `ent_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7061 DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `user_stat`;
CREATE TABLE `user_stat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `reset_cnt` int(11) NOT NULL,
  `onepass_cnt` int(11) NOT NULL,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8mb4;


COMMIT;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `api_resource`;
DROP TABLE `check_task`;
DROP TABLE `classify_item`;
DROP TABLE `enterprise`;
DROP TABLE `enterprise_role`;
DROP TABLE `enterprise_user`;
DROP TABLE `label_task`;
DROP TABLE `role`;
DROP TABLE `role_resource`;
DROP TABLE `rule_item_result`;
DROP TABLE `rule_item`;
DROP TABLE `rule`;
DROP TABLE `spot_check`;
DROP TABLE `task_audit_assign`;
DROP TABLE `task`;
DROP TABLE `task_check_assign`;
DROP TABLE `task_item`;
DROP TABLE `task_label_assign`;
DROP TABLE `user_role_ent`;
DROP TABLE `user_stat`;
DROP TABLE `user`;
