-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
alter table rule_item_result  modify column json_value mediumtext ;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
alter table rule_item_result  modify column json_value text ;