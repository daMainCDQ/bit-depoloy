-- +migrate Up
-- ----------------------------
-- backup intent table
-- ----------------------------
RENAME TABLE knowledge TO knowledge_v1, 
health_check TO health_check_v1, 
health_check_task TO health_check_task_v1, 
cluster_task TO cluster_task_v1, 
knowledge_history TO knowledge_history_v1;

-- ----------------------------
-- Table structure for cluster_task
-- ----------------------------
CREATE TABLE `cluster_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `task_id` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '检查任务标示',
  `status` int(11) NOT NULL COMMENT '状态（0初始化） （1聚类中）（2已聚类）（-1失败）（-2已删除）',
  `message` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '检查任务讯息',
  `remain_time` int(11) DEFAULT '0' COMMENT '所需时间',
  `total_count` int(11) DEFAULT '0' COMMENT '聚类条数',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `app_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '机器人ID',
  `dataset_id` varchar(64) DEFAULT NULL COMMENT '数据集ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='聚类任务表';


-- ----------------------------
-- Table structure for health_check
-- ----------------------------
CREATE TABLE `health_check` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `app_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '机器人ID',
  `module_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '模块ID',
  `is_needHealthCheck` tinyint(4) NOT NULL COMMENT '是否需要进行健康检查 1需要，0不需要',
  `dataset_id` varchar(64) DEFAULT NULL COMMENT '数据集ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COMMENT='健康检查表';

-- ----------------------------
-- Table structure for health_check_task
-- ----------------------------
CREATE TABLE `health_check_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `task_id` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '检查任务标示',
  `type` tinyint(4) NOT NULL COMMENT '检查类型：1冲突检查',
  `check_result` tinyint(4) NOT NULL DEFAULT '2' COMMENT '检查类型：1健康 2不健康',
  `module_id` tinyint(4) NOT NULL COMMENT '1 全部 2 FAQ 3 KG 4 TE 5 NLU',
  `check_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '检查状态：1进行中 2已完成',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `app_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '机器人ID',
  `dataset_id` varchar(64) DEFAULT NULL COMMENT '数据集ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COMMENT='健康检查任务表';

-- ----------------------------
-- Table structure for knowledge
-- ----------------------------
CREATE TABLE `knowledge` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '名称',
  `module_id` int(11) NOT NULL COMMENT '模块ID，唯一',
  `category_id` int(11) NOT NULL COMMENT '类别ID，只能挂一个',
  `corpus_counts` int(11) DEFAULT NULL COMMENT '语料条数',
  `status` int(11) NOT NULL COMMENT '状态 （0未变动）（1修改待训练）（2训练待发布）（-1删除待训练）（-2删除待发布）',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `app_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '机器人ID',
  `task_id` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '上传任务标识',
  `dataset_id` varchar(64) DEFAULT NULL COMMENT '数据集ID',
  PRIMARY KEY (`id`),
  KEY `name_moduleid_app_index` (`name`,`module_id`,`app_id`),
  KEY `name_app_index` (`name`,`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=593 DEFAULT CHARSET=utf8mb4 COMMENT='知识表';

-- ----------------------------
-- Table structure for knowledge_history
-- ----------------------------
CREATE TABLE `knowledge_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '名称',
  `module_id` int(11) NOT NULL COMMENT '模块ID，唯一',
  `category_id` int(11) NOT NULL COMMENT '类别ID，只能挂一个',
  `corpus_counts` int(11) DEFAULT NULL COMMENT '语料条数',
  `status` int(11) NOT NULL COMMENT '状态 （0未变动）（1修改待训练）（2训练待发布）（-1删除待训练）（-2删除待发布） ',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `app_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '机器人ID',
  `task_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '上传任务ID',
  `dataset_share_id` varchar(64) DEFAULT NULL COMMENT '共享数据集ID',
  PRIMARY KEY (`id`),
  KEY `name_moduleid_app_index` (`name`,`module_id`,`app_id`),
  KEY `name_app_index` (`name`,`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=179 DEFAULT CHARSET=utf8mb4 COMMENT='历史知识表';

-- ----------------------------
-- copy data to backup table
-- ----------------------------
SET AUTOCOMMIT=0;

INSERT INTO `aquarius`.`knowledge`(`id`, `name`, `module_id`, `category_id`, `corpus_counts`, `status`, `create_time`, `update_time`, `app_id`, `task_id`) 
SELECT `id`, `name`, `module_id`, `category_id`, `corpus_counts`, `status`, `create_time`, `update_time`, `app_id`, `task_id` 
FROM `knowledge_v1`;

INSERT INTO `aquarius`.`knowledge_history`(`id`, `name`, `module_id`, `category_id`, `corpus_counts`, `status`, `create_time`, `update_time`, `app_id`, `task_id`) 
SELECT `id`, `name`, `module_id`, `category_id`, `corpus_counts`, `status`, `create_time`, `update_time`, `app_id`, `task_id` 
FROM `knowledge_history_v1`;

INSERT INTO `aquarius`.`cluster_task`(`id`, `task_id`, `status`, `message`, `remain_time`, `total_count`, `create_time`, `update_time`, `app_id`) 
SELECT `id`, `task_id`, `status`, `message`, `remain_time`, `total_count`, `create_time`, `update_time`, `app_id` 
FROM `cluster_task_v1`;

INSERT INTO `aquarius`.`health_check`(`id`, `app_id`, `module_id`, `is_needHealthCheck`) 
SELECT `id`, `app_id`, `module_id`, `is_needHealthCheck` 
FROM `health_check_v1`;

INSERT INTO `aquarius`.`health_check_task`(`id`, `task_id`, `type`, `check_result`, `module_id`, `check_status`, `create_time`, `update_time`, `app_id`) 
SELECT `id`, `task_id`, `type`, `check_result`, `module_id`, `check_status`, `create_time`, `update_time`, `app_id` 
FROM `health_check_task_v1`;

SET AUTOCOMMIT=1;

-- ----------------------------
-- Table structure for dataset
-- ----------------------------
DROP TABLE IF EXISTS `dataset`;
CREATE TABLE `dataset` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `dataset_id` varchar(64) NOT NULL COMMENT '数据集ID，逻辑主键，唯一索引',
  `name` varchar(256) NOT NULL COMMENT '数据集名称',
  `enterprise_id` varchar(64) NOT NULL COMMENT '企业ID',
  `description` varchar(256) COMMENT '数据集描述',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `dataset_id` (`dataset_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='标注平台-短文本2.0新增表1 ：数据集表';

-- ----------------------------
-- Table structure for dataset_share
-- ----------------------------
DROP TABLE IF EXISTS `dataset_share`;
CREATE TABLE `dataset_share` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `dataset_share_id` varchar(64) NOT NULL COMMENT '共享数据集ID，逻辑主键，唯一索引',
  `name` varchar(256) NOT NULL COMMENT '共享数据集名称',
  `enterprise_id` varchar(64) NOT NULL COMMENT '企业ID',
	`on_off` tinyint(2) NOT NULL DEFAULT 0 COMMENT '共享数据集开关：0-关；1-开；默认关',
  `type` tinyint(2) NOT NULL DEFAULT 0 COMMENT '共享数据集类型：0-所有类型；1-系统预设；2-自定义；默认预设', 
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `dataset_share_id` (`dataset_share_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='标注平台-短文本2.0新增表2 ：共享数据集表';

-- ----------------------------
-- Table structure for data_field
-- ----------------------------
DROP TABLE IF EXISTS `data_field`;
CREATE TABLE `data_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键，领域ID',
  `parent_id` int(11) NOT NULL COMMENT '领域父ID',
  `name` varchar(256) NOT NULL COMMENT '领域名称',
  `enterprise_id` varchar(64) NOT NULL COMMENT '企业ID',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='标注平台-短文本2.0新增表3 ：领域表';

-- ----------------------------
-- Table structure for dataset_field
-- ----------------------------
DROP TABLE IF EXISTS `dataset_field`;
CREATE TABLE `dataset_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键，领域ID',
  `data_field_id` int(11) NOT NULL COMMENT '领域ID',
  `dataset_id` varchar(64) NOT NULL COMMENT '数据集ID',
  `enterprise_id` varchar(64) NOT NULL COMMENT '企业ID',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='标注平台-短文本2.0新增表4 ：领域-数据集关系表';

-- ----------------------------
-- Table structure for dataset_field_share
-- ----------------------------
DROP TABLE IF EXISTS `dataset_field_share`;
CREATE TABLE `dataset_field_share` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键，领域ID',
  `data_field_id` int(11) NOT NULL COMMENT '领域ID',
  `dataset_share_id` varchar(64) NOT NULL COMMENT '数据集ID',
  `enterprise_id` varchar(64) NOT NULL COMMENT '企业ID',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='标注平台-短文本2.0新增表5 ：领域-共享数据集关系表';

-- ----------------------------
-- init data
-- ----------------------------

DELETE FROM dataset WHERE id = 1;

INSERT INTO dataset(`id`, `dataset_id`, `name`, `enterprise_id`, `description`, `create_time`, `update_time`) VALUES
(1, 'bb3e3925f0ad11e7bd860242ac120003', '示例数据集', 'bb3e3925f0ad11e7bd860242ac120003', '原有数据升级后的数据文件', '2020-03-18 21:12:34', '2020-03-18 21:12:51');

DELETE FROM dataset_share WHERE id = 1;

INSERT INTO dataset_share(`id`, `dataset_share_id`, `name`, `enterprise_id`, `on_off`, `type`, `create_time`, `update_time`)
VALUES (1, 'bb3e3925f0ad11e7bd860242ac120003', '行业历史数据', 'bb3e3925f0ad11e7bd860242ac120003', 1, 1, '2020-03-20 11:34:16', '2020-03-23 08:55:52');

update knowledge set `dataset_id`='bb3e3925f0ad11e7bd860242ac120003' where dataset_id is null;
update health_check set `dataset_id`='bb3e3925f0ad11e7bd860242ac120003' where dataset_id is null;
update health_check_task set `dataset_id`='bb3e3925f0ad11e7bd860242ac120003' where dataset_id is null;
update cluster_task set `dataset_id`='bb3e3925f0ad11e7bd860242ac120003' where dataset_id is null;
  
update knowledge_history set `dataset_share_id`='bb3e3925f0ad11e7bd860242ac120003' where dataset_share_id is null;

UPDATE `module` SET `module_id` = 3, `name` = '知识图谱', `app_id` = 'remove' WHERE `id` = 7;


-- +migrate Down
UNLOCK TABLES;
DROP TABLE `dataset`;
DROP TABLE `dataset_share`;
DROP TABLE `data_field`;
DROP TABLE `dataset_field`;
DROP TABLE `dataset_field_share`;

DROP TABLE `knowledge`;
DROP TABLE `health_check`;
DROP TABLE `health_check_task`;
DROP TABLE `cluster_task`;
DROP TABLE `knowledge_history`;

RENAME TABLE knowledge_v1 TO knowledge, 
health_check_v1 TO health_check, 
health_check_task_v1 TO health_check_task, 
cluster_task_v1 TO cluster_task, 
knowledge_history_v1 TO knowledge_history;





















