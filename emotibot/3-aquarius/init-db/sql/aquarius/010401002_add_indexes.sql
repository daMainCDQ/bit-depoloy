-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
alter table corpus add index name_knowledgeid_index (name, knowledge_id);
alter table corpus add index knowledgeid_index (knowledge_id);
alter table knowledge add index name_moduleid_app_index (name,module_id,app_id);
alter table knowledge add index name_app_index (name,app_id);
alter table corpus_history add index name_knowledgeid_index (name, knowledge_id);
alter table corpus_history add index knowledgeid_index (knowledge_id);
alter table knowledge_history add index name_moduleid_app_index (name,module_id,app_id);
alter table knowledge_history add index name_app_index (name,app_id);

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
alter table corpus drop index name_knowledgeid_index;
alter table corpus drop index knowledgeid_index;
alter table knowledge drop index name_moduleid_app_index;
alter table knowledge drop index name_app_index;
alter table corpus_history drop index name_knowledgeid_index;
alter table corpus_history drop index knowledgeid_index;
alter table knowledge_history drop index name_moduleid_app_index;
alter table knowledge_history drop index name_app_index;
