-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE DATABASE IF NOT EXISTS `aquarius` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `aquarius`;

START TRANSACTION;

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `app_id` varchar(50) COLLATE utf8mb4_bin NOT NULL COMMENT '机器人ID',
  `name` varchar(225) COLLATE utf8mb4_bin NOT NULL COMMENT '类别名',
  `pid` int(11) NOT NULL COMMENT '顶级类目，0代表顶级目录',
  `desc` varchar(225) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '行业说明',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `task_id` varchar(225) COLLATE utf8mb4_bin NOT NULL COMMENT '上传标识',
  PRIMARY KEY (`id`)
) COMMENT='目录类别表';

-- ----------------------------
-- Table structure for category_history
-- ----------------------------
DROP TABLE IF EXISTS `category_history`;
CREATE TABLE `category_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `app_id` varchar(50) COLLATE utf8mb4_bin NOT NULL COMMENT '机器人ID',
  `name` varchar(225) COLLATE utf8mb4_bin NOT NULL COMMENT '类别名',
  `pid` int(11) NOT NULL COMMENT '顶级类目，0代表顶级目录',
  `desc` varchar(225) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '行业说明',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '行业编码',
  PRIMARY KEY (`id`)
) COMMENT='行业类别表';

-- ----------------------------
-- Table structure for cluster_task
-- ----------------------------
DROP TABLE IF EXISTS `cluster_task`;
CREATE TABLE `cluster_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `task_id` varchar(225) COLLATE utf8mb4_bin NOT NULL COMMENT '检查任务标示',
  `status` int(11) NOT NULL COMMENT '状态（0初始化） （1聚类中）（2已聚类）（-1失败）（-2已删除）',
  `message` varchar(225) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '检查任务讯息',
  `remain_time` int(11) DEFAULT 0 COMMENT '所需时间',
  `total_count` int(11) DEFAULT 0 COMMENT '聚类条数',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `app_id` varchar(50) COLLATE utf8mb4_bin NOT NULL COMMENT '机器人ID',
  PRIMARY KEY (`id`)
) COMMENT='聚类任务表';

-- ----------------------------
-- Table structure for conflict
-- ----------------------------
DROP TABLE IF EXISTS `conflict`;
CREATE TABLE `conflict` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `health_id` int(11) NOT NULL COMMENT '健康检查ID',
  `corpus_id` int(11) NOT NULL COMMENT '语料ID',
  `conflict_set_id` int(11) NOT NULL COMMENT '冲突语料组合',
  `operation_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '处理状态：0未处理 1已标注 2已修改 3已删除',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `app_id` varchar(50) COLLATE utf8mb4_bin NOT NULL COMMENT '机器人ID',
  PRIMARY KEY (`id`)
) COMMENT='冲突语料表';

-- ----------------------------
-- Table structure for corpus
-- ----------------------------
DROP TABLE IF EXISTS `corpus`;
CREATE TABLE `corpus` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
 `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '名称',
  `knowledge_id` int(11) NOT NULL COMMENT '知识ID，唯一',
  `status` int(11) NOT NULL COMMENT '状态',
  `is_positive` tinyint(11) NOT NULL COMMENT '是否正例',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `app_id` varchar(50) COLLATE utf8mb4_bin NOT NULL COMMENT '机器人ID',
  PRIMARY KEY (`id`)
) COMMENT='语料表';

-- ----------------------------
-- Table structure for corpus_history
-- ----------------------------
DROP TABLE IF EXISTS `corpus_history`;
CREATE TABLE `corpus_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '名称',
  `knowledge_id` int(11) NOT NULL COMMENT '知识ID，唯一',
  `status` int(11) NOT NULL COMMENT '状态',
  `is_positive` int(11) NOT NULL COMMENT '是否正例',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `app_id` varchar(50) COLLATE utf8mb4_bin NOT NULL COMMENT '机器人ID',
  PRIMARY KEY (`id`)
) COMMENT='历史语料表';

-- ----------------------------
-- Table structure for health_check
-- ----------------------------
DROP TABLE IF EXISTS `health_check`;
CREATE TABLE `health_check` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `app_id` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '机器人ID',
  `module_id` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '模块ID',
  `is_needHealthCheck` tinyint(4) NOT NULL COMMENT '是否需要进行健康检查 1需要，0不需要',
  PRIMARY KEY (`id`)
) COMMENT='健康检查表';

-- ----------------------------
-- Table structure for health_check_task
-- ----------------------------
DROP TABLE IF EXISTS `health_check_task`;
CREATE TABLE `health_check_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `task_id` varchar(225) COLLATE utf8mb4_bin NOT NULL COMMENT '检查任务标示',
  `type` tinyint(4) NOT NULL COMMENT '检查类型：1冲突检查',
  `check_result` tinyint(4) NOT NULL DEFAULT '2' COMMENT '检查类型：1健康 2不健康',
  `module_id` tinyint(4) NOT NULL COMMENT '1 全部 2 FAQ 3 KG 4 TE 5 NLU',
  `check_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '检查状态：1进行中 2已完成',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `app_id` varchar(50) COLLATE utf8mb4_bin NOT NULL COMMENT '机器人ID',
  PRIMARY KEY (`id`)
) COMMENT='健康检查任务表';

-- ----------------------------
-- Table structure for knowledge
-- ----------------------------
DROP TABLE IF EXISTS `knowledge`;
CREATE TABLE `knowledge` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '名称',
  `module_id` int(11) NOT NULL COMMENT '模块ID，唯一',
  `category_id` int(11) NOT NULL COMMENT '类别ID，只能挂一个',
  `corpus_counts` int(11) DEFAULT NULL COMMENT '语料条数',
  `status` int(11) NOT NULL COMMENT '状态 （0未变动）（1修改待训练）（2训练待发布）（-1删除待训练）（-2删除待发布）',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `app_id` varchar(50) COLLATE utf8mb4_bin NOT NULL COMMENT '机器人ID',
  `task_id` varchar(225) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '上传任务标识',
  PRIMARY KEY (`id`)
) COMMENT='知识表';

-- ----------------------------
-- Table structure for knowledge_history
-- ----------------------------
DROP TABLE IF EXISTS `knowledge_history`;
CREATE TABLE `knowledge_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
 `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '名称',
  `module_id` int(11) NOT NULL COMMENT '模块ID，唯一',
  `category_id` int(11) NOT NULL COMMENT '类别ID，只能挂一个',
  `corpus_counts` int(11) DEFAULT NULL COMMENT '语料条数',
  `status` int(11) NOT NULL COMMENT '状态 （0未变动）（1修改待训练）（2训练待发布）（-1删除待训练）（-2删除待发布） ',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `app_id` varchar(50) COLLATE utf8mb4_bin NOT NULL COMMENT '机器人ID',
  `task_id` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '上传任务ID',
  PRIMARY KEY (`id`)
) COMMENT='历史知识表';

-- ----------------------------
-- Table structure for log_cluster
-- ----------------------------
DROP TABLE IF EXISTS `log_cluster`;
CREATE TABLE `log_cluster` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `cluster_task_id` int(11) NOT NULL COMMENT '聚类任务ID',
  `center_sentence` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '中心句',
  `tags` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '聚类标签',
  `log_corpus_ids` mediumtext COLLATE utf8mb4_bin NOT NULL COMMENT '日志语料IDS',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `app_id` varchar(50) COLLATE utf8mb4_bin NOT NULL COMMENT '机器人ID',
  PRIMARY KEY (`id`)
) COMMENT='日志聚类表';

-- ----------------------------
-- Table structure for log_corpus
-- ----------------------------
DROP TABLE IF EXISTS `log_corpus`;
CREATE TABLE `log_corpus` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '名称',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '状态 （0未聚类）（1已聚类）（2已标注）（-1已忽略） （-2已删除）',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `app_id` varchar(50) COLLATE utf8mb4_bin NOT NULL COMMENT '机器人ID',
  PRIMARY KEY (`id`)
) COMMENT='日志语料表';

-- ----------------------------
-- Table structure for module
-- ----------------------------
DROP TABLE IF EXISTS `module`;
CREATE TABLE `module` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `module_id` tinyint(4) NOT NULL COMMENT '1全部知识 2FAQ 3KG 4INTENT',
  `name` varchar(50) COLLATE utf8mb4_bin NOT NULL COMMENT '模型名称',
  `app_id` varchar(50) COLLATE utf8mb4_bin NOT NULL COMMENT '机器人ID',
  PRIMARY KEY (`id`)
) COMMENT='模块表';

-- ----------------------------
-- Table structure for train_log
-- ----------------------------
DROP TABLE IF EXISTS `train_log`;
CREATE TABLE `train_log` (
  `id` int(11) NOT NULL COMMENT '自增ID ',
  `knowledge_count` int(11) DEFAULT NULL COMMENT '知识数量',
  `corpus_count` int(11) DEFAULT NULL COMMENT '语料数量',
  `train_time` timestamp(6) NULL DEFAULT NULL COMMENT '训练时间',
  `train_result` tinyint(4) DEFAULT NULL COMMENT '训练结果',
  `is_check` tinyint(4) NOT NULL COMMENT '是否已健康检查（0是，1否）',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `app_id` varchar(50) COLLATE utf8mb4_bin NOT NULL COMMENT '机器人ID',
  `module_id` tinyint(4) NOT NULL COMMENT '模块ID，唯一',
  PRIMARY KEY (`id`)
) COMMENT='训练日志表';

-- ----------------------------
-- Table structure for upload_history
-- ----------------------------
DROP TABLE IF EXISTS `upload_history`;
CREATE TABLE `upload_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `user_id` varchar(64) NOT NULL DEFAULT '' COMMENT '用户ID',
  `app_id` varchar(64) NOT NULL DEFAULT '' COMMENT '机器人ID',
  `file_path` varchar(4096) NOT NULL DEFAULT '' COMMENT '文件地址',
  `rows` int(11) NOT NULL DEFAULT '0' COMMENT '总行数',
  `valid_rows` int(11) NOT NULL DEFAULT '0' COMMENT '有效行数',
  `finish_rows` int(11) NOT NULL DEFAULT '0' COMMENT '完成行数',
  `comment` varchar(100) NOT NULL DEFAULT '' COMMENT '说明',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `file_name` varchar(60) NOT NULL DEFAULT '' COMMENT '上传文件名',
  `upload_type` int(2) NOT NULL COMMENT '1:增量，2:全量',
  `upload_detail_file_path` varchar(4096) NOT NULL DEFAULT '' COMMENT '下载详情文件地址',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_app` (`app_id`) USING BTREE
) COMMENT='标准问答案上传历史记录表';

#### 初始化数据 ######################

INSERT INTO `module` VALUES (1, 1, '全部知识', 'AQBOT');
INSERT INTO `module` VALUES (2, 2, '标准问题', 'AQBOT');
INSERT INTO `module` VALUES (3, 3, '知识图谱', 'AQBOT');
INSERT INTO `module` VALUES (4, 4, '意图', 'AQBOT');
INSERT INTO `module` VALUES (5, 1, '全部知识', 'csbot');
INSERT INTO `module` VALUES (6, 2, '标准问题', 'csbot');
INSERT INTO `module` VALUES (7, 3, '知识图谱', 'csbot');
INSERT INTO `module` VALUES (8, 4, '意图', 'csbot');
COMMIT;

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back


