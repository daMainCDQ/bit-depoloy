-- +migrate Up
-- ----------------------------
-- backup intent table
-- ----------------------------
RENAME TABLE log_corpus TO log_corpus_v1;

-- ----------------------------
-- Table structure for log_corpus
-- ----------------------------
CREATE TABLE `log_corpus` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '名称',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '状态 （0未聚类）（1已聚类）（2已标注）（-1已忽略） （-2已删除）',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `app_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '机器人ID',
  `dataset_id` varchar(64) DEFAULT NULL COMMENT '数据集ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6944 DEFAULT CHARSET=utf8mb4 COMMENT='日志语料表';

INSERT INTO `aquarius`.`log_corpus`(`id`, `name`, `status`, `create_time`, `update_time`, `app_id`) 
SELECT `id`, `name`, `status`, `create_time`, `update_time`, `app_id`
FROM `log_corpus_v1`;

update `aquarius`.`log_corpus` set `dataset_id`='bb3e3925f0ad11e7bd860242ac120003' where dataset_id is null;

-- +migrate Down
DROP TABLE `log_corpus`;
RENAME TABLE log_corpus_v1 TO log_corpus;