#!/bin/bash


if [[ ${#INIT_ES_HOST} == 0 ]]; then
	INIT_ES_HOST=172.17.0.1
fi
if [[ ${#INIT_ES_PORT} == 0 ]]; then
	INIT_ES_PORT=9200
fi

INDEX_URL="http://$INIT_ES_HOST:$INIT_ES_PORT/emotibot-qa-core"

# wait elasticsearch
until curl -sS "$INIT_ES_HOST:$INIT_ES_PORT" &> /dev/null; do echo "wait elasticsearch" && sleep 3; done

python -u migration.py
